# POC - Prova de conceito de redirecionamento

## Setup

- Tem dois projetos e ambos precisam rodar simultaneamente.
  - Projeto Antigo: **ANTIGO_planner_front** rodando na porta **3000**
  - Projeto Novo: **NOVO_planner_front** rodando na porta **3001**

- Cada um precisa rodar em uma versão node diferente:
  - Projeto Antigo: rodar node na versão 12 ou 14, eu rodei na versão **14.16.1**
  - Projeto Novo: rodar node na versão 16, eu rodei na versão **16.8.0**

- Para começar pelo login, limpe o local storage da url do antigo

## Comportamento

- Quando o usuário acessar o antigo, quando não logado ele é redirecionado pro novo, para logar lá!
- Na página home foi adicionado um link direto na parte superior para o novo com a página de teste de planos e sonhos, como exemplo.
- Se clicar no link original do planos e sonhos no antigo na esquerda também é redirecionado para essa tela, porém com um bem pequeno delay.
- Os redirecionamentos estão funcionando bem, mas se atente ao que é acessado primeiro, se no caso do antigo e o usuário não tiver a autenticação ele é redirecionado para o novo
- A partir do novo, na url do novo, graças ao rewrites do next ele busca primeiro se tem a página pronta no novo, tendo, a serve, não tendo, abre o antigo com a url do novo
- quando abre uma tela que está no antigo com rewrites para o novo, é possivel compartilhar tudo, porém é preciso fazer uma modificação simples no antigo para redirecionar os links para a url pronta no novo...

## Resultado

- De acordo com meus testes **está validado** a POC de redirecionamento.

[x] Criar projeto para POC

[x] Fazer redirecionamento velho->novo (quando não logado no antigo ele redireciona pra logar no novo)

[x] Validar recuperação de dados do usuário (velho -> novo) (dados recuperados sem agravar problemas no antigo servindo o novo como base do login)

[x] Fazer redirecionamento novo->velho

[x] Validar recuperação de dados do usuário (novo -> velho)

Critério de aceite: Fazer o redirecionamento (vice-versa) e conseguir recuperar os dados do usuário no novo e manter ele logado no antigo.