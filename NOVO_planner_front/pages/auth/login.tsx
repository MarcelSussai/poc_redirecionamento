import axios from 'axios'
import { useState, useEffect } from 'react'
import { useRouter } from 'next/router'
import {
  Container01,
  Container02,
  LabelInput01,
  TextInput01,
  InputWrapper,
  EnterButton, 
  Container03,
  TitleLoginPOC,
  SubtitleLoginPOC
} from '../../src/styles/Main_Styled'

const base_url = 'https://api.qa.meuvista.com/api/'
const config_chamada_api = {
  headers: {
    "Access-Control-Allow-Origin": "*",
    "Access-Control-Allow-Headers": "Authorization",
    "Access-Control-Allow-Methods": "GET, POST, OPTIONS, PUT, PATCH, DELETE",
  },
};
const setLocalStorage = (tag, dado) => localStorage.setItem(tag, dado)

const InputDefault = ({textLabel, changeFunction, isPass}) => (
  <InputWrapper>

    <LabelInput01>{textLabel}</LabelInput01>
    <TextInput01 type={isPass ? 'password' : 'text'} onChange={changeFunction}/>

  </InputWrapper>
)

const Login = () => {

  const [email, setEmail] = useState('')
  const [senha, setSenha] = useState('')
  const [exibir, setExibir] = useState(false)
  const router = useRouter()

  const onChangeEmail = (e) => setEmail(e.target.value)
  const onChangeSenha = (e) => setSenha(e.target.value)

  useEffect(() => {
    if(localStorage.getItem("tokenAcesso") && localStorage.getItem("access-token")) {
      console.log('LOGADO')
      router.push('/admin/index')
    } else {
      console.log('NÃO LOGADO')
      setExibir(true)
    }
  }, [])

  const getLogin = (url, dados) => {
    axios.post(url, dados).then((res) => {
      // console.log('LOGIN, chamada 02')
      // console.log(res)
      if (res.data.success === true) {
        setLocalStorage("usuario-email", email)
        setLocalStorage("access-token", res.data.accessToken)
        if (res.data.empresaId != null) { setLocalStorage("empresa-id", res.data.empresaId) }
        if (res.data.familiaId != null) { setLocalStorage("familia-id", res.data.familiaId) }
        if (res.data.pessoaId != null) { setLocalStorage("pessoa-id", res.data.pessoaId) }
        if (res.data.planejadorId != null) { setLocalStorage("planejador-id", res.data.planejadorId) }
        if (res.data.usuarioId != null) { setLocalStorage("usuario-id", res.data.usuarioId) }
        if (res.data.planejadorPrincipal != null && res.data.planejadorPrincipal === true) {
          setLocalStorage("planejador-principal", "1")
        }
        setLocalStorage("tipo-usuario", res.data.tipoUsuario);
        setLocalStorage("isFlaggedToUseBankIntegration", res.data.isFlaggedToUseBankIntegration);
        
        router.push('/admin/index')
      } else { console.log(res.data.exception) }
    })
  }

  const getTokenAcesso = (url, dados1, dados2, config) => {
    axios.post(url, dados1, config).then((res) => {
      console.log('TOKEN ACESSO, chamada 01')
      console.log(res)
      if(res.status === 200) {
        if(res.data.code === 200) {
          setLocalStorage("tokenAcesso", res.data.apiToken)
          console.log('SUCESSO: Login Válido')
          getLogin(base_url + res.data.apiToken + '/auth', dados2)
        } else {
          setLocalStorage("erroLogin", "true")
          console.log('ERRO: Login Inválido')
        }
      } else {
        setLocalStorage("erroLogin", "true")
        console.log('ERRO: Problemas Técnicos')
      }
    })
  }


  const submitLogin = () => {

    const url01 = base_url + 'credencial'
    const dados01 = {
      email: email,
      senha: senha,
    }
    const dados02 = {
      email: email,
      senha: senha,
      nomeUsuario: null
    }

    getTokenAcesso(url01, dados01, dados02, config_chamada_api)
  
  }

  return (
    <>
      {
        exibir && 
          <Container01>

          <Container03>
            <TitleLoginPOC> {`Prova de Conceito (POC)`} </TitleLoginPOC>
            <SubtitleLoginPOC>
              {`Projeto Novo, tela de exemplo de login, redireciona com a url do novo para o antigo, usando rewrite`}
            </SubtitleLoginPOC>
          </Container03>

          <Container02>
            <InputDefault isPass={false} changeFunction={onChangeEmail} textLabel={`Email`} />
            <InputDefault isPass={true} changeFunction={onChangeSenha} textLabel={`Senha`} />
            <EnterButton onClick={submitLogin}>{`Entrar`}</EnterButton>
          </Container02>

        </Container01>
      }
    </>
  )
}



export default Login