
import Head from '../src/components/head'
import Header from '../src/components/header';
import { Main } from '../src/styles/Main_Styled';
import Link from 'next/link'
import { useEffect } from 'react';
import { useRouter } from 'next/router';

// admin/planos-sonhos

const Home = () => {
  const router = useRouter()
  useEffect(() => {
    // router.push('/admin/index')
    if(localStorage.getItem("tokenAcesso") && localStorage.getItem("access-token")) {
      router.push('/admin/index')
    } else {
      router.push('/auth/login')
    }
  }, [])

  return (
  <>
    <Head title="Home" />
    {/* <Header /> */}
    <Main>
      {/* <h1>Titulo Corpo</h1>
      <Link href="/admin/planos-sonhos">
        <a>TESTE</a>
      </Link> */}
    </Main>
  </>
);

}

export default Home;