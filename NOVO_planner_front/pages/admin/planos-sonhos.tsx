import Link from 'next/link'
import { useEffect, useState } from 'react'
import { useRouter } from 'next/router'
import { Container01, LinkToTest } from "../../src/styles/Main_Styled"


const PlanosSonhos = () => {
  const [exibir, setExibir] = useState(false)

  const router = useRouter()
  useEffect(() => {
    if(localStorage.getItem("tokenAcesso") && localStorage.getItem("access-token")) {
      console.log('LOGADO')
      setExibir(true)
    } else {
      router.push('/auth/login')
    }
  }, [])

  return (
    <>
      {
        exibir &&
        <Container01>
          <h1>TESTE</h1>
          <Link href="/admin/index">
            <LinkToTest>HOME</LinkToTest>
          </Link>
        </Container01>
      }
    </>
  )
}

export default PlanosSonhos