import { createGlobalStyle } from 'styled-components';

export const GlobalStyle = createGlobalStyle`
  *, *::before, *::after {
    box-sizing: border-box;
  }

  ul[class], ol[class] {
    padding: 0;
  }

  body, h1, h2, h3, h4, p, ul[class], ol[class], li, figure, figcaption, blockquote, dl, dd {
    margin: 0;
  }

  html::-webkit-scrollbar {
    
  }
  html::-webkit-scrollbar-button  {
    
  }
  html::-webkit-scrollbar-track  {
    
  }
  html::-webkit-scrollbar-track-piece {
    
  }
  html::-webkit-scrollbar-thumb {
    
  }

  html {
    scroll-behavior: smooth;
    font-size: 16px;
    overflow-x: hidden;
  }

  body {
    min-height: 100vh;
    padding: 0;
    line-height: 1.6;
    overflow-x: hidden;
  }

  ul[class], ol[class] {
    list-style: none;
  }

  a:not([class]) {
    -webkit-text-decoration-skip: ink;
            text-decoration-skip-ink: auto;
  }

  img {
    max-width: 100%;
    display: block;
  }

  input, button, textarea, select {
    font: inherit;
  }

  @media (prefers-reduced-motion: reduce) {
    * {
      animation-duration: 0.01ms !important;
      animation-iteration-count: 1 !important;
      transition-duration: 0.01ms !important;
      scroll-behavior: auto !important;
    }
  }
`

export const theme = {
  colors: {
    primary: '#1d1d1f',
    grays: {
      gray01: '#323232',
      gray02: '#646464',
      gray03: '#a8a8a8',
      gray04: '#c4c4c4',
      gray05: "#d8d8d8",
      gray06: "#e4e4e4",
      gray07: "#f0f0f0",
      gray08: "#fafafa",
    },
    blues: {
      blue01: '#3232c4',
      blue02: '#5757d4',
      blue03: '#9696e4',
      blue04: '#eaeafa',
      blue05: '#28289d',
      blue06: '#191962',
      blue07: '#0f0f3b',
      blue08: '#050514',
    },
    greens: {
      green01: '#32FF32',
      green02: '#70ff70',
      green03: '#99ff99',
      green04: '#d6ffd6',
      green05: '#00d500',
      green06: '#009900',
      green07: '#005c00',
      green08: '#001f00',
    },
    reds: {
      red01: '#ff4848',
    }
  },
}