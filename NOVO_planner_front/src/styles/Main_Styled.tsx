
import styled from 'styled-components';

export const Main = styled.main`
  display: flex;
  flex-direction: column;
  padding: 8px;
`

export const Container01 = styled.div`
  width: 100vw;
  height: 100vh;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  gap: 8px;
`

export const Container02 = styled.div`
  width: 320px;
  display: flex;
  flex-direction: column;
  gap: 16px;
  background-color: ${({theme}) => theme.colors.grays.gray07};
  border: solid 1px ${({theme}) => theme.colors.grays.gray05};
  border-radius: 16px;
  padding: 16px;
  
`

export const Container03 = styled.div`
  max-width: 320px;
  display: flex;
  flex-direction: column;
  justify-content: center;
  gap: 16px;
`

export const LinkToTest = styled.a`
  font-size: 18px;
  padding: 8px;
  margin: 8px;
  color: ${({theme}) => theme.colors.primary};
  transition: all .3s ease-in-out;
  cursor: pointer;
  &:hover {
    color: ${({theme}) => theme.colors.reds.red01};
    background-color: ${({theme}) => theme.colors.grays.gray04} ;
  }
`

export const InputWrapper = styled.div`
  display: flex;
  flex-direction: column;
`

export const TextInput01 = styled.input`
  padding: 8px;
  border: 1px solid ${({theme}) => theme.colors.grays.gray03};
  outline: 0;
  border-radius: 8px;
  box-shadow: 2px 2px 8px #64646464;
  font-family: sans-serif;
  font-size: 16px;
  color: ${({theme}) => theme.colors.grays.gray02};
`

export const LabelInput01 = styled.label`
  font-family: sans-serif;
  font-size: 16px;
  margin-left: 8px;
  color: ${({theme}) => theme.colors.grays.gray02};
`

export const EnterButton = styled.button`
  margin-top: 32px;
  outline: 0;
  border: 2px solid ${({theme}) => theme.colors.blues.blue06};
  background-color: ${({theme}) => theme.colors.blues.blue04};
  color: ${({theme}) => theme.colors.blues.blue06};
  border-radius: 8px;
  padding: 8px;
  font-family: sans-serif ;
  font-size: 20px;
  font-weight: 700;
  width: 120px;
  align-self: flex-end;
  transition: all .3s ease-in-out;
  cursor: pointer;
  
  &:hover {
    border-color: ${({theme}) => theme.colors.greens.green06};
    background-color: ${({theme}) => theme.colors.greens.green04};
    color: ${({theme}) => theme.colors.greens.green06};
  }
`

export const TitleLoginPOC = styled.h1`
  max-width: 320px;
  text-align: center;
  font-family: sans-serif ;
  font-size: 24px;
  font-weight: 800;
`

export const SubtitleLoginPOC = styled.h2`
  max-width: 320px;
  text-align: center;
  font-family: sans-serif ;
  font-size: 20px;
  font-weight: 500;
  margin-bottom: 16px;
`