module.exports = {
  async rewrites() {
    return [
      {
        source: '/:path*',
        destination: '/:path*',
      },
      {
        source: '/:path*',
        destination: 'http://localhost:3000/:path*',
      },
      // {
      //   source: '/',
      //   destination: 'http://localhost:3000/',
      // },
    ]
  },
}