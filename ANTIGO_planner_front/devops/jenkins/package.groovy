// This generates static builds and stores it in S3 for later deployment
pipeline {
    agent any
    tools { nodejs 'V12.22.7' }

    stages {
        stage('CleanWorkspace') {
            steps {
                timeout(time: 1, unit: 'MINUTES') {
                    cleanWs()
                }
            }
        }

        stage('Checkout') {
            steps {
                timeout(time: 5, unit: 'MINUTES') {
                    checkoutFromGit()
                }
            }
        }

        stage('Install dependencies') {
            steps {
                timeout(time: 7, unit: 'MINUTES') {
                    echo 'instalando dependencias'
                    sh 'yarn'
                }
            }
        }

        stage('Lint') {
            steps {
                timeout(time: 5, unit: 'MINUTES') {
                    echo 'Linting code'
                    sh 'yarn lint'
                }
            }
        }

        stage('Test') {
            steps {
                timeout(time: 3, unit: 'MINUTES') {
                    echo 'executando testes'
                    sh 'yarn test'
                }
            }
        }

        stage('Build QA') {
            steps {
                timeout(time: 5, unit: 'MINUTES') {
                    echo 'buildando'
                    sh 'yarn build:qa'
                }
            }
        }

        stage('Save QA Build') {
            steps {
                timeout(time: 3, unit: 'MINUTES') {
                    echo 'movendo arquivos de QA'
                    s3Upload(
                        bucket: 'vista-releases',
                        file:'./build/',
                        path:"frontend/$BUILD_NUMBER/qa/"
                    )
                }
            }
        }

        stage('Build PROD') {
            steps {
                timeout(time: 5, unit: 'MINUTES') {
                    echo 'buildando'
                    sh 'yarn build:production'
                }
            }
        }

        stage('Save PROD Build') {
            steps {
                timeout(time: 3, unit: 'MINUTES') {
                    echo 'movendo arquivos de PROD'
                    s3Upload(
                        bucket: 'vista-releases',
                        file:'./build/',
                        path:"frontend/$BUILD_NUMBER/prod/"
                    )
                }
            }
        }
    }
}

def checkoutFromGit() {
    if (branch.startsWith('origin/')) {
        def reg = ~/^origin\//
        branch = branch - reg
    }
    git branch: branch, credentialsId: 'git-lsreis', url: 'https://bitbucket.org/luizfernandomsch/planner_front.git'
}
