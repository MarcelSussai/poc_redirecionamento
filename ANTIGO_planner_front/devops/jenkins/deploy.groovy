// This gets an already generated static deployment from S3 and deploys it to the production S3 bucket
def envValues = [
    qa: [
        sourceBucket: 'qa.app.meuvista.com',
        distributionId: 'E1CGFR6CS290J7',
        awsProfile: 'jenkins-qa'
    ],
    prod: [
        sourceBucket: 'prod.app.meuvista.com',
        distributionId: 'E3HMYKT7L44LA7',
        awsProfile: 'jenkins-prod'
    ]
]
def sourceBucket
def distributionId
def awsProfile

pipeline {
    agent any

    stages {
        stage('Validate environment variables') {
            steps {
                script {
                    noValue = 'noValue'
                    values = envValues["$ENVIRONMENT"] ?: noValue
                    if (values == noValue) {
                        msg = "Aborting the build. Missing environment values. Check the script."
                        currentBuild.result = 'ABORTED'
                        echo msg
                        error(msg)
                    }
                    sourceBucket = values.sourceBucket
                    distributionId = values.distributionId
                    awsProfile = values.awsProfile
                }
            }
        }

        stage('Deleting old deployment') {
            steps {
                script {
                    result = sh(
                        script: "aws s3 rm s3://$sourceBucket --recursive",
                        returnStdout: true
                    ).trim()
                    echo result
                }
            }
        }

        stage('Transfer new files') {
            steps {
                script {
                    versions = sh(
                        script: "aws s3 cp s3://vista-releases/frontend/$VERSION_NUMBER/$ENVIRONMENT/ s3://$sourceBucket --recursive --acl public-read",
                        returnStdout: true
                    ).trim()
                    echo versions
                }
            }
        }

        stage('Invalidate Cloudfront distribution cache') {
            steps {
                // I could not find how to allow cross-account cloudfront cache invalidations
                // So I'm switching credentials to be able to run the invalidation
                // TODO: Check if there is really no other way and try to standardize authentication method
                withCredentials([[
                    $class: 'AmazonWebServicesCredentialsBinding',
                    credentialsId: "$awsProfile",
                    accessKeyVariable: 'AWS_ACCESS_KEY_ID',
                    secretKeyVariable: 'AWS_SECRET_ACCESS_KEY'
                ]]) {
                    script {
                        invalidationResult = sh(
                            script: "aws cloudfront create-invalidation --distribution $distributionId --paths \"/*\"", 
                            returnStdout: true
                        ).trim()
                        echo invalidationResult
                    }
                }
            }
        }
    }
}
