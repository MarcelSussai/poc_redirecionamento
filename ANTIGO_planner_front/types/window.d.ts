import NewRelicBrowser from 'new-relic-browser'

declare global {
  interface Window {
    // This is defined in the index.html by new relic's initialization script
    newrelic: typeof NewRelicBrowser
  }
}

export {}