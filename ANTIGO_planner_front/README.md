# Vista Frontend

This is the web frontend used by Vista. It is a [React](https://reactjs.org/) app originaly built using Javascript, but currently being migrated to [Typescript](https://www.typescriptlang.org/)

## Development

### Environment

- The NodeJS version to be used is specified at `.nvmrc` file in the project root. [More about NVM](https://github.com/nvm-sh/nvm)
- [Yarn](https://yarnpkg.com/) should be used instead of NPM

### Contributing guidelines

New development should follow these rules:

- Must use Typescript instead of Javascript
- Must style components using [Styled-Components](https://styled-components.com/)
- Must have related tests
- Abbreviations should be avoided (better explained [here](https://developers.google.com/style/abbreviations#long-and-short-versions))
- This repository uses [git flow](https://nvie.com/posts/a-successful-git-branching-model/) as branching model
    - New feature branches should start with `feature/*my-branch-name*` and should be created off the `develop` branch.
- Avoid redundant comments ([chapter 4 of Clean Code, by Robert C. Martin](https://enos.itcollege.ee/~jpoial/oop/naited/Clean%20Code.pdf))
- [Yarn](https://yarnpkg.com/) should be used instead of NPM
- All new code must be buildable in [Jenkins](https://jenkins.meuvista.com/job/planner-frontend-package/)

## Monitoring

### New Relic

The index.html file has a [New Relic](https://newrelic.com/) [script](https://docs.newrelic.com/docs/browser/browser-monitoring/installation/install-browser-monitoring-agent/#copy-paste-app) at it's top. This allows errors to be transmited to New Relic, where they can be analysed. 

- [QA environment link](https://onenr.io/0oqQakr60R1)
- [Prod environment link](https://onenr.io/01Owvx5a1jv)

It is possible to use the `JS errors` or the `Errors Inbox` tabs to check errors out. It is also important to note the time range being viewed at the top right of the page.

![Errors in New Relic](./docs/images/newrelic.png)

It is also possible to check each error custom attributes:

![Error detail](./docs/images/newrelic_errorCustomAttributes.png)

### Source maps

The source maps can be downloaded from S3 deployment buckets:

- Prod: `arn:aws:s3:::prod.app.meuvista.com` (account 430537850870)
- QA: `arn:aws:s3:::qa.app.meuvista.com` (account 759041471414)

Then, the source maps files can be found in `static/js/*.js.map`:

![Source maps](./docs/images/s3_sourceMaps.png)

## Linter

This project uses [ESLint](https://eslint.org/) to standarize the project files. We have started using it since we migrated to Typescript, so currently we only run ESLint against TS files. It is possible to run the linter locally using `yarn lint`. Linting is part of the build proccess, so new errors cannot be added.