import axios from "axios";
import { confirmAlert } from "react-confirm-alert";

const baseApiUrl = process.env.REACT_APP_API_BASE_URL;
global.server_api_new = baseApiUrl;

global.apiToken = "";
global.termoId = "8a6b8fce-7340-4170-9f60-dd674fe6b4d4";
global.email = "";
global.senha = "";

global.idSonhoAposentadoria = 119;

global.spinnerActive = true;
global.spinnerMessage = "Processando...";
global.estados = [
  "AC",
  "AL",
  "AP",
  "AM",
  "BA",
  "CE",
  "DF",
  "ES",
  "GO",
  "MA",
  "MT",
  "MS",
  "MG",
  "PA",
  "PB",
  "PR",
  "PE",
  "PI",
  "RJ",
  "RN",
  "RS",
  "RO",
  "RR",
  "SC",
  "SP",
  "SE",
  "TO",
];
global.estadoCivil = ["Solteiro", "Casado", "Divorciado", "Viúvo"];
global.tipoEmpresa = ["PessoaFisica", "PessoaJuridica"];
global.tipoUsuario = ["Admin", "Planejador", "Pessoa"];
//global.tipoRecorrencia = [ 'Mensal', 'Bimestral', 'Trimestral', 'Semestral', 'Anual' ];
global.tipoRecorrencia = ["Mensal", "Avulso"];
global.tipoOrcamento = ["Receita", "Despesa", "Investimento", "Divida"];
global.tipoMeioPagamento = [
  "Dinheiro",
  "Pré-pago / Vale / Ticket",
  "Cartão Crédito",
  "Instituição Financeira",
];
global.statusDivida = ["Aberto", "Atrasado", "Pago"];
global.statusSonho = ["Não Iniciado", "Em Progresso", "Pausado", "Concluído"];
global.tipoPatrimonio = [
  "Imóvel",
  "Veículo",
  "Empresa",
  "Investimento",
  "Outros",
];
global.bandeira = ["Visa", "Master", "Elo", "Amex", "Diners", "Hiper"];
global.frequenciaCategoria = ["Mensal", "Anual"];
global.grupoDeCategoria = [
  "Alimentação",
  "Educação",
  "Lazer",
  "Moradia",
  "Transporte",
  "Receitas",
  "Investimento",
  "Dívida",
  "Saúde",
  "Outros",
  "Pessoal",
  "Viagens",
  "Bem Estar",
  "Trabalho",
  "Impostos",
  "Integração Olivia"
];
global.statusDaCategoria = ["Ativa", "Historica"];
global.indexador = ["TR", "PRE", "CDI", "OUTRO"];
global.amortizacao = ["SAC", "PRICE", "OUTRO"];
global.statusFamilia = ["Aberto", "Pago", "Atrasado"];

global.proRata7Dias = true;

global.grupoCategoriaSonho = {
  "-1": "Outros",
  1: "Viagens",
  2: "Aventuras",
  3: "Pessoal",
  4: "Esportes",
  5: "Carreira / Educação",
  6: "Família",
  7: "Saúde / Bem Estar",
  8: "Generosidade",
  9: "Compras",
  10: "Aposentadoria",
};

global.tipoInvestimento = {
  1: "Fundos",
  2: "Previdência",
  6: "Renda Fixa Pré-fixado",
  3: "Renda Fixa Pós-fixado",
  4: "Tesouro Direto",
  5: "Poupança",
  8: "Ações, FII, ETFs",
  9: "Debêntures",
  10: "Moedas",
  99: "Produto Customizado",
  //7: 'Bitcoin',
};

global.naturezaFundo = { 1: "Fundo", 2: "Previdência" };
global.metodologiaDePreco = { 1: "Por Valor", 2: "Por Variação" };
global.tipoRetirada = [
  { value: 1, label: "Resgate" },
  { value: 3, label: "Come Cotas" },
];
global.operacao = [
  { value: 0, label: "Aplicação" },
  { value: 1, label: "Resgate" },
  { value: 2, label: "Saldo Inicial" },
];
global.operacao2 = ["Aplicação", "Resgate", "Saldo Inicial"];

global.taxa = 0.4;
global.inflacao = 4;
global.planosCarteiras = [1, 2];
global.planosCarteiras2 = { 1: "Plano Individual", 2: "Plano Família" };
global.carenciaCobranca = 7;

global.tipoConsolidacao = {
  1: "Desde o Início",
  2: "Ano Corrente",
  3: "Últimos 12 Meses",
};
global.tipoGrafico = { 1: "Geral", 2: "Rentabilidade" };

global.spinnerShow = function ($) {
  var html = $("html"); //IE7 won't have that
  html.css("overflow", "hidden");
  window.scrollTo(0, 0);
  $("._loading_overlay_wrapper").show();
};

global.showModal = (skyLightModal) => {
  if (typeof window.scrollTo === "function") {
    window.scrollTo(0, 0);
  }
  skyLightModal.show();
};

global.spinnerHide = function ($, scrollPosition) {
  $("._loading_overlay_wrapper").hide();
  $("html").css("overflow", "auto");
  window.scrollTo(scrollPosition[0], scrollPosition[1]);
};

global.logarErroDeRequisicao = function (error) {
  // Error 😨
  if (error.response) {
    /*
     * The request was made and the server responded with a
     * status code that falls out of the range of 2xx
     */
    console.log(error.response.data);
    console.log(error.response.status);
    console.log(error.response.headers);
  } else if (error.request) {
    /*
     * The request was made but no response was received, `error.request`
     * is an instance of XMLHttpRequest in the browser and an instance
     * of http.ClientRequest in Node.js
     */
    console.log(error.request);
  } else {
    // Something happened in setting up the request and triggered an Error
    console.log("Error", error.message);
  }

  console.log(error.config);
};

global.maskCurrency = function (e) {
  var value = e.target.value;
  var formattedValue = value;

  if (value !== "") {
    value = value.replace(/\D/g, "");
    value = parseFloat(parseInt(value) / 100);

    formattedValue = value
      .toLocaleString("pt-BR", { style: "currency", currency: "BRL" })
      .replace("R$", "");
  }

  e.target.value = formattedValue;
};

global.ajudaPlanejador = {
  "/admin/index": [
    "Como funciona a cobrança no Vista?|O Vista é uma <b>plataforma tecnológica</b> para o planejador financeiro atender seus clientes.<br><br>O cliente é do Planejador, e não do Vista, portanto, o contrato entre o planejador e o cliente deve ser feito entre as partes.<br><br>Oferecemos 7 dias como período de desistência. Portanto todo novo cliente poderá ser cancelado em até 7 dias sem gerar custos. A partir do 8º dia, será cobrado o valor desde o início.<br><br>O planejador poderá escolher entre os dois planos:<br><br><b>• Plano Individual:</b> 49,90 por família<br><b>• Plano Família:</b> 59,90 por família",
    "Como funciona a cobrança no Vista?|<b>Mais sobre os Planos</b><br><br><b>Plano Individual:</b> ideal para solteiros, clientes que não possuem investimentos ou casais que possuem 1 carteira de investimentos (Ex: o trabalho é feito com uma família, mas somente um dos cônjuges possui investimentos no seu nome). <br><br><b>Plano Família:</b> ideal para famílias que possuem 2 carteiras de investimentos (Ex: o trabalho é feito com uma família, sendo que cada um dos cônjuges possui investimentos no seu nome). <br><br>* Os planos podem ser modificados a qualquer momento.",
    "Como funciona a cobrança no Vista?|<b>Qual o dia da cobrança?</b><br><br>Todo dia 10 de cada mês subsequente ao cadastramento do cliente, o planejador pagará a sua fatura mensal referente aos seus clientes, de acordo com o plano escolhido. O pagamento pode ser feito através do cartão de crédito ou débito. Na primeira cobrança o planejador receberá um link no seu e-mail para cadastrar o cartão de crédito ou débito, e aquele cartão ficará salvo para as próximas faturas.<br><br>Ex: Planejador tem 3 clientes no Plano Individual de 49,90 (total de 149,70) e 4 clientes no Plano Família de 59,90 (239,60). Além disso, o planejador utiliza o gateway de pagamento de 59,90.<br><br>Total da Fatura = 149,70 + 239,60 + 59,90 = 449,20.<br><br>Esse valor será cobrado no cartão de crédito/débito cadastrado do planejador.<br><br><b>Importante:</b> O valor cobrado pelo cliente será pro-rata. Ou seja, caso o planejador cadastre o cliente durante o mês (ou cancele o cliente durante o mês), ele irá pagar o valor proporcional do cliente na próxima fatura.",
  ],
  "/admin/clientes": [
    "Como funciona a cobrança no Vista?|O Vista é uma <b>plataforma tecnológica</b> para o planejador financeiro atender seus clientes.<br><br>O cliente é do Planejador, e não do Vista, portanto, o contrato entre o planejador e o cliente deve ser feito entre as partes.<br><br>Oferecemos 7 dias como período de desistência. Portanto todo novo cliente poderá ser cancelado em até 7 dias sem gerar custos. A partir do 8º dia, será cobrado o valor desde o início.<br><br>O planejador poderá escolher entre os dois planos:<br><br><b>• Plano Individual:</b> 49,90 por família<br><b>• Plano Família:</b> 59,90 por família",
    "Como funciona a cobrança no Vista?|<b>Mais sobre os Planos</b><br><br><b>Plano Individual:</b> ideal para solteiros, clientes que não possuem investimentos ou casais que possuem 1 carteira de investimentos (Ex: o trabalho é feito com uma família, mas somente um dos cônjuges possui investimentos no seu nome). <br><br><b>Plano Família:</b> ideal para famílias que possuem 2 carteiras de investimentos (Ex: o trabalho é feito com uma família, sendo que cada um dos cônjuges possui investimentos no seu nome). <br><br>* Os planos podem ser modificados a qualquer momento.",
    "Como funciona a cobrança no Vista?|<b>Qual o dia da cobrança?</b><br><br>Todo dia 10 de cada mês subsequente ao cadastramento do cliente, o planejador pagará a sua fatura mensal referente aos seus clientes, de acordo com o plano escolhido. O pagamento pode ser feito através do cartão de crédito ou débito. Na primeira cobrança o planejador receberá um link no seu e-mail para cadastrar o cartão de crédito ou débito, e aquele cartão ficará salvo para as próximas faturas.<br><br>Ex: Planejador tem 3 clientes no Plano Individual de 49,90 (total de 149,70) e 4 clientes no Plano Família de 59,90 (239,60). Além disso, o planejador utiliza o gateway de pagamento de 59,90.<br><br>Total da Fatura = 149,70 + 239,60 + 59,90 = 449,20.<br><br>Esse valor será cobrado no cartão de crédito/débito cadastrado do planejador.<br><br><b>Importante:</b> O valor cobrado pelo cliente será pro-rata. Ou seja, caso o planejador cadastre o cliente durante o mês (ou cancele o cliente durante o mês), ele irá pagar o valor proporcional do cliente na próxima fatura.",
  ],
  "/admin/usuarios": [
    "Como funciona a cobrança no Vista?|O Vista é uma <b>plataforma tecnológica</b> para o planejador financeiro atender seus clientes.<br><br>O cliente é do Planejador, e não do Vista, portanto, o contrato entre o planejador e o cliente deve ser feito entre as partes.<br><br>Oferecemos 7 dias como período de desistência. Portanto todo novo cliente poderá ser cancelado em até 7 dias sem gerar custos. A partir do 8º dia, será cobrado o valor desde o início.<br><br>O planejador poderá escolher entre os dois planos:<br><br><b>• Plano Individual:</b> 49,90 por família<br><b>• Plano Família:</b> 59,90 por família",
    "Como funciona a cobrança no Vista?|<b>Mais sobre os Planos</b><br><br><b>Plano Individual:</b> ideal para solteiros, clientes que não possuem investimentos ou casais que possuem 1 carteira de investimentos (Ex: o trabalho é feito com uma família, mas somente um dos cônjuges possui investimentos no seu nome). <br><br><b>Plano Família:</b> ideal para famílias que possuem 2 carteiras de investimentos (Ex: o trabalho é feito com uma família, sendo que cada um dos cônjuges possui investimentos no seu nome). <br><br>* Os planos podem ser modificados a qualquer momento.",
    "Como funciona a cobrança no Vista?|<b>Qual o dia da cobrança?</b><br><br>Todo dia 10 de cada mês subsequente ao cadastramento do cliente, o planejador pagará a sua fatura mensal referente aos seus clientes, de acordo com o plano escolhido. O pagamento pode ser feito através do cartão de crédito ou débito. Na primeira cobrança o planejador receberá um link no seu e-mail para cadastrar o cartão de crédito ou débito, e aquele cartão ficará salvo para as próximas faturas.<br><br>Ex: Planejador tem 3 clientes no Plano Individual de 49,90 (total de 149,70) e 4 clientes no Plano Família de 59,90 (239,60). Além disso, o planejador utiliza o gateway de pagamento de 59,90.<br><br>Total da Fatura = 149,70 + 239,60 + 59,90 = 449,20.<br><br>Esse valor será cobrado no cartão de crédito/débito cadastrado do planejador.<br><br><b>Importante:</b> O valor cobrado pelo cliente será pro-rata. Ou seja, caso o planejador cadastre o cliente durante o mês (ou cancele o cliente durante o mês), ele irá pagar o valor proporcional do cliente na próxima fatura.",
  ],
};

global.ajuda = {
  "/admin/index": [
    "Home|<b>Bem vindo(a) ao Vista!</b><br><br>Nesta tela você verá algumas informações resumidas sobre a sua saúde financeira.<br>No gráfico abaixo você poderá comparar os seus investimentos, receitas, despesas e dívidas que foram realizados ao longo do prazo escolhido.<br><br>Por padrão este prazo será o mês atual.",
    "Home|<b>Clientes</b><br><br>Seus clientes cadastrados na plataforma representa uma família e cada usuário, são os membros desta família que terão acesso ao VISTA.<br><br> Ex: Cliente: Família Silva, Usuário: José; Usuário: Maria.",
    "Home|<b>Lançamentos do dia</b><br><br>Aqui você verá os seus lançamentos do dia. Isso vai te ajudar a se manter atualizado e ver seu último registro.",
    "Home|<b>Quadro Realizado x Estimado</b><br><br>Neste quadro estão os valores realizados e suas estimativas.<br>Compare suas estimativas com o que foi realizado, tenha atenção nos valores que superam suas estimativas, eles ficarão vermelho.",
    "Home|<b>Gráfico Pizza</b><br><br>Este gráfico demonstra percentualmente, a divisão dos seus gastos por orçamento.<br>Veja quais os orçamentos que mais ocupam espaço nas suas finanças.",
    "Home|<b>Gastos por Categoria</b><br><br>Os gastos por categoria estão em ordem dos seus maiores gastos realizados ao longo do mês. Veja para onde está indo o seu dinheiro.",
  ],
  "/admin/planos-sonhos": [
    "Planos e Sonhos|Aqui é o coração de todo o planejamento. Desejamos que você conquiste e festeje o maior número possível de sonhos e planos.<br><br></br>Sonhos são todos os seus desejos, tudo aquilo que pensa um dia em realizar. Não deixe de registrar aqui os seus sonhos já realizados!<br><br></br>Já os planos são os sonhos que passam a ter um planejamento financeiro, e a partir dai inserimos informações como prazo, valor, patrimônio alocado e outros dados. Queremos te ajudar a conquistar seus sonhos, mas primeiro vamos planejar.",
    "Planos e Sonhos|Todos os planos têm uma barra de status, que irá aumentando o percentual a medida em que você destina patrimônio para a realização deste. Ao chegar nos 100% quer dizer que você possui o patrimônio almejado para isso, mas não necessariamente já realizou o plano.",
  ],
  "/admin/gestao-planos-sonhos": [
    "Gestão de Planos|Nesta tela você conseguirá visualizar os dados de todos os planos, bem como o valor total necessário que você deve investir mensalmente para alcançar os seus objetivos.",
    "Gestão de Planos|Na gestão de sonhos você pode alterar os campos como taxa, prazo, valor presente, inflação e parcela. Faça o teste mudando os parâmetros e ajustando mais próximo da sua realidade.",
    "Gestão de Planos|Tente chegar em um valor de parcela mensal próximo ao que você consegue investir para alcançar seus planos.",
    "Gestão de Planos|O valor acumulado não pode ser alterado nesta tela, apenas na destinação de patrimônio, onde atribuímos o patrimônio acumulado aos seus planos.",
    "Gestão de Planos|Ao salvar, os valores serão atualizados em nosso banco de dados.",
  ],
  "/admin/destinacao-patrimonio": [
    "Destinação de Patrimônio|Aqui você pode destinar um patrimônio a um objetivo específico. Como por exemplo atribuir uma previdência privada ao plano da aposentadoria, ou a venda de um imóvel para a compra de outra casa.",
    "Destinação de Patrimônio|O mais interessante é que você pode atribuir percentualmente um patrimônio para vários objetivos. Ex. Um fundo de investimentos pode ser 50% para uma viagem especial e outros 50% como uma reserva financeira.",
    "Destinação de Patrimônio|Alinhar seu patrimônio para os seus planos deixará você mais perto das suas conquistas e trará mais clareza para o objetivo do seu dinheiro.",
    "Destinação de Patrimônio|Os valores atribuídos serão somados ao patrimônio acumulado dos seus sonhos.",
  ],
  "/admin/patrimonio": [
    "Patrimônio|Nesta tela você vai inserir todo o seu patrimônio, seja financeiro ou imobilizado. Aqui será toda a visão dos seus bens.",
    "Patrimônio|Abaixo encontram-se alguns gráficos da evolução dos seus investimentos. Grande parte dos ativos financeiros estão vinculados com inteligência para atualizar automaticamente.",
    "Patrimônio|Cadastre todos os seus investimentos financeiros. Quando fizer novos aportes, basta entrar em “Novo Lançamento” no topo da página, escolher a aba “Investimentos” e escolher o ativo que deseja investir. Automaticamente o sistema reconhecerá o seu aporte.",
  ],
  "/admin/destinacao-investimentos": [
    "Destinação de Investimentos|Aqui você irá determinar para onde seus investimentos mensais serão destinados. Ao realizar o investimento do mês ele será distribuído na proporção que você determinar para cada um dos seus planos.",
    "Destinação de Investimentos|Ex: Se você determinar 30% de um fundo de ações para a aposentadoria e outros 70% para a compra de uma casa, ao alocar qualquer valor neste fundo a inteligência do Vista distribuirá nesta proporção aos seus planos.",
  ],
  "/admin/dividas": [
    "Cadastro de Dívida|O cadastro de dívidas te ajuda a organizar melhor os seus passivos financeiros.",
    "Cadastro de Dívida|Poderão haver divergências dos valores da sua dívida real, uma vez que cada dívida tem peculiaridades, ajustes e características diversas. Portanto, atualizar os valores sempre que possível.",
  ],
  "/admin/orcamento": [
    "Orçamento|O orçamento é toda a estrutura do seu fluxo de caixa. Aqui você poderá criar e excluir categorias e determinar estimativas; bem como uma série de informações para sua análise. Clique em “x” e personalize suas informações que deseja enxergar.",
    "Orçamento|<b>Orçamento</b>: São centro de custos, como Adulto, Família, Imóvel, Carro, Investimentos.<br><b>Categoria</b>: São os itens dentro de cada orçamento, como condomínio, aluguel, etc (No caso do orçamento Imóvel).<br><b>Estimado</b>: É o valor você estima de receitas e despesas. Os valores de investimentos e dívidas são automáticos e não editáveis por aqui.<br><b>Meio de pagamento padrão</b>: escolha os meios de pagamento que você utiliza para facilitar nos seus lançamentos.<br><b>Quantidade</b>: Quantos gastos houveram nesta categoria.<br><b>Frequência</b>: Poderá ser mensal (frequente) ou anual (gastos esporádicos).",
    "Orçamento|O orçamento também é uma ferramenta de análise do seu fluxo de caixa; classifique e veja os seus maiores gastos, qual é a média, as estimativas, etc.",
  ],
  "/admin/extrato": [
    "Extrato de Lançamentos|O extrato te dará uma visão de todos os seus lançamentos. Por aqui também será possível editar e apagar seus lançamentos.",
    "Extrato de Lançamentos|Veja quais os seus gastos ou receitas estão parcelados.",
  ],
  "/admin/equilibrio-financeiro": [
    "Equilíbrio Financeiro|Nesta tela você terá informações preciosas do seu Planejamento Financeiro.",
    "Equilíbrio Financeiro|Determine metas e objetivos gerais, veja como está a sua evolução e quanto dinheiro está economizando e ganhando.",
  ],
  "/admin/aposentadoria": [
    "Plano de Independência Financeira|Nesta tela você terá informações preciosas do seu Planejamento Financeiro.",
  ],
  "/admin/integracao-bancaria": [
    "Integração Bancária|<strong>Bem vindo ao Open Finance do Vista!</strong><br/><br/><br/>Aqui você ira conectar suas contas bancárias para obtermos as informações das suas movimentações. O acesso é apenas às informações e não é possível executar nenhuma outra atividade em suas contas.",
    "Integração Bancária|Respeitamos a sua privacidade e dados. Todas as informações são criptografadas, assim como nos bancos os seus dados não são (e nunca serão) compartilhados ou vendidos à terceiros.",
    "Integração Bancária|Uma das partes mais importantes do planejamento financeiro, porém que demanda um certo tempo, é anotar suas receitas e despesas. Nós iremos facilitar isso para você!",
    "Integração Bancária|Depois que você conectar as suas contas, você precisará vincular os meios de pagamento do seu banco com aqueles cadastrados no Vista. Para isso, acesse a aba “integração bancária”, encontre a integração conectada e faça a destinação.",
    "Integração Bancária|Depois de conectar as suas contas e vincular aos meios de pagamento, vamos buscar todas as transações de cartão de crédito e conta corrente a partir de hoje, mas você terá que categorizar (identificar) estas movimentações.",
    "Integração Bancária|Para isso, entre no seu extrato e categorize suas movimentações, ou seja, determine o “orçamento” e “categoria” mais adequado para esta movimentação. Todas as movimentações podem ser editadas (inclusive excluídas).",
    "Integração Bancária|Caso você receba uma mensagem do seu banco informando sobre o nosso acesso, não se preocupe, é uma mensagem padrão e de segurança de alguns bancos. Caso tenha qualquer dúvida, entre em contato com o seu planejador financeiro.",
    "Integração Bancária|Bom planejamento!!!<br/><br/><br/>Aqui seus dados estão seguros e queremos te ajudar ao máximo para facilitar o seu planejamento financeiro.",
  ],
};

global.mostrarAjudaDaPaginaAutomaticamente = function () {
  //Pega id da pagina
  var idPagina = window.location.pathname;

  //Busca o cookie
  var cookie = localStorage.getItem(idPagina);

  //Se nao foi aberto ainda
  if (cookie === null || cookie !== "ok") {
    //Abrir ajuda daquela pagina
    var botao = document.getElementById("abrir-ajuda");

    //Se existe
    if (botao != null) {
      //Simula o click
      window.scrollTo(0, 0);
      botao.click();
    }
  }
};

global.getCredential = function () {
  localStorage.setItem("naoLembrarAtzApp", false);

  var url = global.server_api_new + "credencial";

  var dados = {
    email: global.email,
    senha: global.senha,
  };

  var config = {
    headers: {
      "Access-Control-Allow-Origin": "*",
      "Access-Control-Allow-Headers": "Authorization",
      "Access-Control-Allow-Methods": "GET, POST, OPTIONS, PUT, PATCH, DELETE",
    },
  };

  axios.post(url, dados, config).then((res) => {
    //console.log(res);
    try {
      if (res.status === 200) {
        if (res.data.code === 200) {
          localStorage.setItem("tokenAcesso", res.data.apiToken);
          this.apiToken = res.data.apiToken;
        } else {
          localStorage.setItem("erroLogin", "true");
          const options = {
            title: "Falha no Login",
            message: "Usuário ou senha inválidos.",
            buttons: [
              {
                label: "Ok",
              },
            ],
          };
          confirmAlert(options);
        }
      } else {
        localStorage.setItem("erroLogin", "true");
        const options = {
          title: "Problemas técnicos",
          message:
            "No momento estamos com problemas técnicos. Tente novamente mais tarde.",
          buttons: [
            {
              label: "Ok",
            },
          ],
        };
        confirmAlert(options);
      }
    } catch (error) {
      console.log(error);
    }
  });
};

global.mostrarAjudaDaPagina = function (vThis, userType) {
  var idPagina = window.location.pathname;
  console.log(idPagina);

  var ajudaDaPagina = global.ajuda[idPagina];
  var ajudaDaPaginaPlanejador = global.ajudaPlanejador[idPagina];

  var familiaId = localStorage.getItem("familia-id");

  if (userType === 1 && familiaId === null && ajudaDaPaginaPlanejador != null) {
    ajudaDaPagina = ajudaDaPaginaPlanejador;
  }

  var modal = document.getElementById("ajuda-modal");

  if (modal != null && ajudaDaPagina != null && vThis != null) {
    window.scrollTo(0, 0);
    vThis.setState({ qtdPassos: ajudaDaPagina.length });

    var conteudoHtml = document.getElementById("ajuda-conteudo");
    if (conteudoHtml != null) {
      conteudoHtml.innerHTML = "";
    }

    ajudaDaPagina.forEach((item, idx) => {
      var partes = item.split("|");
      var titulo = partes[0];
      var conteudo = partes[1];

      var tituloHtml = document.getElementById("ajuda-titulo");
      if (tituloHtml != null) {
        tituloHtml.innerHTML = titulo;
      }

      var conteudoHtml = document.getElementById("ajuda-conteudo");
      if (conteudoHtml != null) {
        if (idx === 0)
          conteudoHtml.innerHTML +=
            '<div lg="12" xl="12" id="passo_' +
            (idx + 1) +
            '" class="passos">' +
            conteudo +
            "</div>";
        else
          conteudoHtml.innerHTML +=
            '<div lg="12" xl="12" id="passo_' +
            (idx + 1) +
            '" class="passos" style="display: none">' +
            conteudo +
            "</div>";

        // if(idPagina == '/admin/aposentadoria'){
        //   conteudoHtml.innerHTML +=
        //   '<div class="container py-5" style="min-height: 600px" id="video"><div class="row justify-content-center" style="width: 100%; height: 100%">' +
        //     '<iframe src="https://player.vimeo.com/video/518358268" style="width: 100%; height: 100%"' +
        //     'allow="fullscreen" allowFullScreen  title="vídeo selecionado"></iframe>' +
        //     "</div></div>";
        // }
      }
    });

    vThis.ajudaModal.show();

    //Seta o cookie
    localStorage.setItem(idPagina, "ok");
  }
};

global.textLabels = {
  body: {
    noMatch: "Nenhum registro encontrado",
    toolTip: "Ordenar",
    columnHeaderTooltip: (column) => `Ordenar por ${column.label}`,
  },
  pagination: {
    next: "Próxima",
    previous: "Anterior",
    rowsPerPage: "Registros por página:",
    displayRows: "de",
  },
  toolbar: {
    search: "Procurar",
    downloadCsv: "Baixar CSV",
    print: "Imprimir",
    viewColumns: "Ver Colunas",
    filterTable: "Filtrar Tabela",
  },
  filter: {
    all: "Todos",
    title: "FILTROS",
    reset: "LIMPAR",
  },
  viewColumns: {
    title: "Mostrar Colunas",
    titleAria: "Mostrar/Esconder colunas",
  },
  selectedRows: {
    text: "registro(s) selecionado(s)",
    delete: "Remover",
    deleteAria: "Remover registros selecionados",
  },
};

global.diferencaEntreDatasEmDias = function (dataInicio, dataFim) {
  var diffMilissegundos = dataFim - dataInicio;
  var diffSegundos = diffMilissegundos / 1000;
  var diffMinutos = diffSegundos / 60;
  var diffHoras = diffMinutos / 60;
  var diffDias = diffHoras / 24;
  return diffDias;
};

global.retornoMes = (mesExt) => {
  if (mesExt === "Jan") {
    return "01";
  } else if (mesExt === "Fev") {
    return "02";
  } else if (mesExt === "Mar") {
    return "03";
  } else if (mesExt === "Abr") {
    return "04";
  } else if (mesExt === "Mai") {
    return "05";
  } else if (mesExt === "Jun") {
    return "06";
  } else if (mesExt === "Jul") {
    return "07";
  } else if (mesExt === "Ago") {
    return "08";
  } else if (mesExt === "Set") {
    return "09";
  } else if (mesExt === "Out") {
    return "10";
  } else if (mesExt === "Nov") {
    return "11";
  } else if (mesExt === "Dez") {
    return "12";
  }
};

global.convertToFloat = (dado) => {
  var temp = dado + "";
  if (temp.includes(",")) {
    return parseFloat(
      temp.trim().replaceAll("R$", "").replaceAll(".", "").replaceAll(",", ".")
    );
  } else {
    return parseFloat(temp.trim().replaceAll("R$", ""));
  }
};
