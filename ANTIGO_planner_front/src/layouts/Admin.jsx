import React from "react";
import { Route, Switch } from "react-router-dom";
import { Container } from "reactstrap";
import AdminNavbar from "../components/Navbars/AdminNavbar.jsx";
import AdminFooter from "../components/Footers/AdminFooter.jsx";
import Sidebar from "../components/Sidebar/Sidebar.jsx";
import LoadingOverlay from "react-loading-overlay";
import routes from "../routes.tsx";
import rightBarTopRoutes from "../rightBarTopRoutes.js";
import "../assets/css/spinner-aux.css";

class Admin extends React.Component {
  componentDidUpdate(e) {
    document.documentElement.scrollTop = 0;
    document.scrollingElement.scrollTop = 0;
    this.refs.mainContent.scrollTop = 0;
  }
  getRoutes = (routes) => {
    if (routes == null) {
      return <></>;
    }

    return routes.map((prop, key) => {
      if (prop.layout === "/admin") {
        //console.log(prop.layout,prop.path);
        return (
          <Route
            path={prop.layout + prop.path}
            component={prop.component}
            key={key}
          />
        );
      } else {
        return null;
      }
    });
  };
  getBrandText = (path) => {
    for (let i = 0; i < routes.length; i++) {
      if (
        this.props.location.pathname.indexOf(
          routes[i].layout + routes[i].path
        ) !== -1
      ) {
        return routes[i].name;
      }
    }
    return "Brand";
  };
  render() {
    return (
      <>
        <LoadingOverlay
          active={global.spinnerActive}
          spinner
          text={global.spinnerMessage}
        ></LoadingOverlay>
        <Sidebar
          {...this.props}
          routes={routes}
          logo={{
            innerLink: "/admin/index",
            imgSrc: require("../assets/img/brand/logo-nova-branca.png"),
            imgAlt: "...",
          }}
          id="sidenav-main"
          position="fixed-left"
        />
        <div className="main-content" ref="mainContent">
          <AdminNavbar
            {...this.props}
            brandText={this.getBrandText(this.props.location.pathname)}
            rightBarTopRoutes={rightBarTopRoutes}
          />
          <Switch>{this.getRoutes(routes)}</Switch>
          <Container fluid>
            <AdminFooter />
          </Container>
        </div>
      </>
    );
  }
}

export default Admin;
