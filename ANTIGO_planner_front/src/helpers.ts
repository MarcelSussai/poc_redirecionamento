const isAbsent = (value: unknown): value is null | undefined =>
  value === null || value === undefined

const isPresent = <T>(value: T): value is NonNullable<T> => !isAbsent(value)

export { isAbsent, isPresent }