export default {
  cyan: '144, 228, 228',
  lightGreen: '102, 209, 209',
  lightBlue: '91, 191, 229',
  blue: '61, 148, 182',
  darkBlue: '48, 102, 151',
  highLightGray: '205, 205, 205',
  gray: '245, 245, 245',
  white: '255, 255, 255'
}