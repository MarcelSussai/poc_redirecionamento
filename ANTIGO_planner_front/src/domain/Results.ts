export interface Result {
  success: boolean,
  errorMsg: string,
  totalResults: number,
}

export interface SingleResult<T> extends Result {
  singleResult: T,
}

export interface ManyResults<T> extends Result {
  results: T[],
}