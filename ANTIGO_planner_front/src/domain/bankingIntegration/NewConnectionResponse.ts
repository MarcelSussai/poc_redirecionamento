export interface NewConnectionResponse {
  token: string;
  url: string;
  success: boolean;
}