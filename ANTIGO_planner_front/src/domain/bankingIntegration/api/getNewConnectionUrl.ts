import { postWithToken } from '../../../webClient'
import { NewConnectionResponse } from '../NewConnectionResponse'

export const getNewConnectionUrl = async (personId: number) => {
  const { data: response, status } = await postWithToken<NewConnectionResponse, null>(`bankintegration/pessoa/${personId}`)
  if (status !== 200 || !response.success) {
    const msg = `Error getting new bob connection url for person ${personId} - ${status}`;
    console.error(msg);
    throw new Error(msg);
  }
  return response;
}