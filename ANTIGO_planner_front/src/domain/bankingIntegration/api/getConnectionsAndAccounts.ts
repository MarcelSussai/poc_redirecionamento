import { getWithToken } from '../../../webClient'
import { Connection } from '../Connection'

export interface Params {
  personId: number
}
export interface ApiResponse {
  success: boolean,
  conexoes: Connection[]
}

export const getConnectionsAndAccounts = async (personId: number) => {
  const { status, data } = await getWithToken<ApiResponse>(`bankintegration/obterconexoesecontas/${personId}`)
  if (status !== 200) {
    const msg = `Error getting ${personId} connections and accounts - ${status}`;
    console.error(msg);
    throw new Error(msg);
  }
  return data.conexoes;
}