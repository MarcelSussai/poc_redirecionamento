import { deleteWithToken } from '../../../webClient'

export interface Params {
  connectionId: String
}

// TODO: Error handling
export const deleteConnection = async ({ connectionId }: Params) => {
  const { status } = await deleteWithToken(`bankIntegration/connection/${connectionId}`)
  if (status !== 200) {
    console.error(`Deleting connection ${connectionId}`)
  }
}