export interface Connection {
    id: string;
    conexaoNome: string;
    ultimoSucesso: Date;
    qtdContasCards: number;
    status: string;
    acaoRequerida: boolean;
    urlAcao?: any;
    contas: Account[];
}

export interface Account {
  id: string;
  descricao: string;
  meioPagamento?: any;
  meioPagamentoId: number;
}