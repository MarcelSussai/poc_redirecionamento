import { postWithToken } from '../../../webClient'
import { MeioPagamento } from '../meioPagamento'
import { ManyResults } from '../../Results'

// Todo: type filters when necessary
interface Filter {}

const fetchByFamily = async (familyId: number, filter: Filter) => {
  const { data: { results, success, errorMsg }, status } = await postWithToken<ManyResults<MeioPagamento>, Filter>(`meioPagamento/familia/${familyId}/filtro`, filter)
  if (!success || status !== 200) {
    console.error(`Error getting MeiosPagamento from family ${familyId}: ${errorMsg}`)
    console.error("Returning empty list list of MeiosPagamento")
    return []
  }
  return results
}

export const fetchAllByFamily = (familyId: number) => fetchByFamily(familyId, {})