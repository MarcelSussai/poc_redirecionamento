import { deleteWithToken, putWithToken } from '../../../webClient'

export interface AssociateParams {
  connectionId: string, 
  accountId: string, 
  meioPagamentoId: number
}

export interface DeassociateParams {
  connectionId: string, 
  accountId: string
}

export const associateBankIntegrationAccountToMeioPagamento = async ({
  connectionId,
  accountId,
  meioPagamentoId
}: AssociateParams) => {
  const { status } = await putWithToken(`bankintegration/connection/${connectionId}/${accountId}/${meioPagamentoId}`)
  if (status !== 200) {
    console.error(`Error associating ${connectionId}:${accountId} to meio pagamento ${meioPagamentoId} - ${status}`)
  }
}

export const removeBankIntegrationAccountAssociationToMeioPagamento = async ({
  connectionId,
  accountId
}: DeassociateParams) => {
  const { status } = await deleteWithToken(`bankintegration/connection/${connectionId}/${accountId}/meioPagamentoId`)
  if (status !== 200) {
    console.error(`Error removing association ${connectionId}:${accountId} - ${status}`)
  }
}