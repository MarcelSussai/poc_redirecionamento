export interface MeioPagamento {
  id: number;
  dono: Dono;
  nome: string;
  tipoMeioPagamento: number;
  bandeira?: number;
  diaVencimento?: number;
  banco: Banco;
  agencia: string;
  conta: string;
  bandeiraPrePago?: any;
  lancamentos?: any;
  dividas?: any;
  empresa?: any;
  familia?: any;
  dataCriacao: Date;
  usuarioCriacao?: any;
  dataUltimaAtualizacao: Date;
  usuarioUltimaAtualizacao?: any;
  ativo: number;
}

export interface Dono {
  nomeUsuario?: any;
  nome: string;
  sobrenome: string;
  documento: string;
  celular: string;
  dataNascimento: Date;
  estadoCivil: number;
  telefone?: any;
  idContaFinanceira?: any;
  temCarteira: number;
  usuario?: any;
  chaveBob?: any;
  empresa?: any;
  familia?: any;
  id: number;
  dataCriacao: Date;
  usuarioCriacao?: any;
  dataUltimaAtualizacao: Date;
  usuarioUltimaAtualizacao?: any;
  ativo: number;
}

export interface Banco {
  nome: string;
  codigoComp: string;
  ispb: string;
  id: number;
  dataCriacao: Date;
  usuarioCriacao?: any;
  dataUltimaAtualizacao: Date;
  usuarioUltimaAtualizacao?: any;
  ativo: number;
}

