import { getWithToken } from '../../../webClient'
import { Pessoa } from '../Pessoa'
import { SingleResult } from '../../Results'

interface Params {
  personId: number;
}

export const getPessoaById = async ({ personId }: Params) => {
  const { 
    data: { 
      singleResult: pessoa,
      success,
      errorMsg,
    } = {}, 
    status 
  } = await getWithToken<SingleResult<Pessoa>>(`pessoa/${personId}`)
  if (!success || status !== 200) {
    const msg = `Error getting Pessoa ${personId}: ${errorMsg}`
    console.error(msg)
    throw new Error(msg)
  }
  return pessoa;
}
