export interface Pessoa {
  id: number;
  nome: string;
  sobrenome: string;
  documento: string;
  celular: string;
  nomeUsuario?: string;
  dataNascimento: string;
  estadoCivil: number;
  temCarteira: number;
  chaveBob: string;
  estaHabilitadoBob: boolean;
  dataCriacao: string;
  dataUltimaAtualizacao: string;
  ativo: number;
}
