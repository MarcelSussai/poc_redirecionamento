export interface Family {
  nome: string;
  endereco: string;
  complemento: string;
  pais: string;
  cidade: string;
  estado: number;
  cep: string;
  valorPago: number;
  inicioContrato: string;
  fimContrato: string;
  recorrencia: number;
  statusFamilia: number;
  quantidadeCarteiras: number;
  meiosPagamentos?: any;
  id: number;
  dataCriacao: string;
  dataUltimaAtualizacao: string;
  ativo: number;
}
