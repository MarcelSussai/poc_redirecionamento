import { getWithToken } from '../../../webClient'
import { SingleResult } from '../../Results'
import { Family } from '../Family'

export interface Params {
  familyId: number
}

export const getFamilyById = async ({ familyId }: Params) => {
  const { 
    data: { 
      singleResult: family,
      success,
      errorMsg,
    } = {}, 
    status 
  } = await getWithToken<SingleResult<Family>>(`familia/${familyId}`)
  if (!success || status !== 200) {
    const msg = `Error getting family ${familyId}: ${errorMsg}`
    console.error(msg)
    throw new Error(msg)
  }
  return family;
}