import React from "react";
import axios from 'axios';

class OrcamentoService extends React.Component 
{
    buscarOrcamentoPorFamiliaFiltro = (token, familiaId, filtro, callback) => 
    {
        //var url = global.server_api + 'api/orcamento/familia/' + familiaId + '/filtro';
        var url = global.server_api_new + global.apiToken + '/orcamento/familia/' + familiaId + '/filtro';

        var config = {
            headers: {
                'Authorization': "bearer " + token,
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Headers": "Authorization", 
                "Access-Control-Allow-Methods": "GET, POST, OPTIONS, PUT, PATCH, DELETE" ,
            }
        };

        axios.post(url, filtro, config).then(res => 
        {
            if(callback != null)
            {
                callback(res.data);
            }
        });
    }

    buscarOrcamentoPorFamiliaTipoNome = (token, familiaId, nome, tipoOrcamento, callback) => 
    {
        var filtro = {
            filtro: nome,
            tipoOrcamento: tipoOrcamento
        };

        //var url = global.server_api + 'api/orcamento/familia/' + familiaId + '/nomeetipoorcamento';
        var url = global.server_api_new + global.apiToken + '/orcamento/familia/' + familiaId + '/nomeetipoorcamento';

        var config = {
            headers: {
                'Authorization': "bearer " + token,
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Headers": "Authorization", 
                "Access-Control-Allow-Methods": "GET, POST, OPTIONS, PUT, PATCH, DELETE" ,
            }
        };

        axios.post(url, filtro, config).then(res => 
        {
            if(callback != null)
            {
                callback(res.data);
            }
        });
    }

    salvarOrcamento = (token, item, callback) =>
    {
        //var url = global.server_api + 'api/orcamento';
        var url = global.server_api_new + global.apiToken + '/orcamento';
    
        var config = 
        {
            headers: {
                'Authorization': "bearer " + token,
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Headers": "Authorization", 
                "Access-Control-Allow-Methods": "GET, POST, OPTIONS, PUT, PATCH, DELETE" ,
            }
        };
        
        axios.post(url, item, config).then(res => 
        {
            if(callback){
                callback(res.data);
            }
        });
    }
}

export default OrcamentoService;