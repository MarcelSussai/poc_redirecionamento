import React from "react";
import axios from 'axios';

class PlanejadorService extends React.Component 
{
    getDadosPlanejador(token, planejadorId, callback) 
    {
        //var url = `${global.server_api}api/planejador/${planejadorId}`;
        var url = `${global.server_api_new}${global.apiToken}/planejador/${planejadorId}`;

        var config = {
            headers: {
                'Authorization': "bearer " + token,
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Headers": "Authorization", 
                "Access-Control-Allow-Methods": "GET, POST, OPTIONS, PUT, PATCH, DELETE" ,
            }
        };

        axios.get(url, config).then(res => 
        {
                if(callback != null)
                {
                    callback(res.data);
                }
        });
    }
}

export default PlanejadorService;