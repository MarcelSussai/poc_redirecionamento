import React from "react";
import axios from 'axios';

class WizardService extends React.Component 
{
    configuracaoIniciada = (token, familiaId, callback) => 
    {
        //var url = global.server_api + 'api/wizard/' + familiaId;
        var url = global.server_api_new + global.apiToken + '/wizard/' + familiaId;
        
        var config = {
            headers: {
                'Authorization': "bearer " + token,
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Headers": "Authorization", 
                "Access-Control-Allow-Methods": "GET, POST, OPTIONS, PUT, PATCH, DELETE" ,
            }
        };

        axios.post(url, {}, config).then(res => {
            if(callback)
            {
                callback(res.data);
            }
        });
    }

    limparConfiguracaoRealizada = (token, planejadorId, familiaId, callback) => 
    {    
        //var url = global.server_api + 'api/wizard/' + familiaId;
        var url = global.server_api_new + global.apiToken + '/wizard/' + familiaId;

        var config = {
            headers: {
                'Authorization': "bearer " + token,
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Headers": "Authorization", 
                "Access-Control-Allow-Methods": "GET, POST, OPTIONS, PUT, PATCH, DELETE" ,
            }
        };

        var dadosRequisicao = {
            planejadorId: planejadorId
        };

        axios.put(url, dadosRequisicao, config).then(res => {
            if(callback){
                callback(res.data)
            }
        });
    }

    buscarStatusDeConfiguracao = (token, familiaId, callback) => {

        //var url = global.server_api + 'api/wizard/' + familiaId;
        var url = global.server_api_new + global.apiToken + '/wizard/' + familiaId;

        var config = {
            headers: {
                'Authorization': "bearer " + token,
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Headers": "Authorization", 
                "Access-Control-Allow-Methods": "GET, POST, OPTIONS, PUT, PATCH, DELETE" ,
            }
        };

        axios.get(url, config).then(res => {
            if(callback){
                callback(res.data);
            }
        });
    }
}

export default WizardService;