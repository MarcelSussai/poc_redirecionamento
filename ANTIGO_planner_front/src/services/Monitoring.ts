interface Tags {
    [key: string]: string | undefined
}

const isError = (err: unknown): err is Error => err instanceof Error;

const noticeError = (err: Error, tags?: Tags) => window.newrelic.noticeError(err, tags)

const noticeUnknownError = (err: unknown, tags?: Tags) => {
    if (isError(err)) {
        noticeError(err, tags)
    } else {
        // TODO: Improve error handling
        console.warn('Received weird error:', err, tags)
        noticeError(err as Error, tags)
    }
}

export { noticeError, noticeUnknownError }
export type { Tags }