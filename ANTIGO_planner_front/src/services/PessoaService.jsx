import React from "react";
import axios from 'axios';

class PessoaService extends React.Component 
{
    buscarPessoasPorFamilia = (token, familiaId, filtro, callback) => 
    {
        //var url = global.server_api + 'api/Pessoa/familia/' + familiaId + "/filtro/";
        var url = global.server_api_new + global.apiToken + '/Pessoa/familia/' + familiaId + "/filtro/";

        var config = {
            headers: {
                'Authorization': "bearer " + token,
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Headers": "Authorization", 
                "Access-Control-Allow-Methods": "GET, POST, OPTIONS, PUT, PATCH, DELETE" ,
            }
        };

        axios.post(url, filtro, config).then(res => 
        {
            if(callback != null)
            {
                callback(res.data);
            }
        });
    }

    salvarPessoa = (token, item, callback) =>
    {
        //var url = global.server_api + 'api/pessoa';
        var url = global.server_api_new + global.apiToken + '/pessoa';

        var config = {
            headers: {
                'Authorization': "bearer " + token,
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Headers": "Authorization", 
                "Access-Control-Allow-Methods": "GET, POST, OPTIONS, PUT, PATCH, DELETE" ,
            }
        };

        item.TemCarteira = (item.TemCarteira === true) ? 1 : 0;

        axios.post(url, item, config).then(res => 
        {
            if(callback){
                callback(res.data);
            }
        });
    }
}

export default PessoaService;