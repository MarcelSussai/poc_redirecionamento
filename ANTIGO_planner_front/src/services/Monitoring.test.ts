import { noticeError, noticeUnknownError } from './Monitoring'

window.newrelic = {}
window.newrelic.noticeError = jest.fn()

describe('Monitoring', () => {
    describe('noticeError', () => {
        it('should call new relic integration', () => {
            const err = new Error('test error')
            noticeError(err)
            expect(window.newrelic.noticeError).toHaveBeenCalledWith(err, undefined)
        })

        it('should forward tags to new relic', () => {
            const err = new Error('test error')
            const tags = { some: 'tag', values: 'to test' }
            noticeError(err, tags)
            expect(window.newrelic.noticeError).toHaveBeenCalledWith(err, tags)
        })
    })

    describe('noticeUnknownError', () => {
        jest.spyOn(global.console, 'warn').mockImplementation()
        beforeEach(() => console.warn.mockReset())

        it('should console warn and call noticeError if non error value is received', () => {
            const nonErrorValue = { custom: 'object' }
            noticeUnknownError(nonErrorValue)
            expect(console.warn).toHaveBeenCalledTimes(1)
            expect(window.newrelic.noticeError).toHaveBeenCalledWith(nonErrorValue, undefined)
        })

        it('should just call noticeError when error value', () => {
            const error = new Error('my test error')
            noticeUnknownError(error)
            expect(console.warn).not.toHaveBeenCalled()
            expect(window.newrelic.noticeError).toHaveBeenCalledWith(error, undefined)
        })
    })
})