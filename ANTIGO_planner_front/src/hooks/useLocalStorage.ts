import { useCallback, useEffect, useRef, useState } from 'react'

const debounceTime = process.env.NODE_ENV === 'test' ? 0 : 500

const useLocalStorage = (key: string, defaultValue = '') => {
  // Inicializa valor com o valor atual do storage ou o valor default
  const [value, setValue] = useState<string | null>(
    localStorage.getItem(key) || defaultValue
  )
  const isFirstRun = useRef(true)

  // Se a chave mudar, mudamos o valor
  useEffect(() => setValue(localStorage.getItem(key)), [key])

  // Resposta ao evento de atualização de valor diretamento no localstorage
  useEffect(() => {
    const onStorageChange = (event: StorageEvent) => {
      if (event.key !== key || event.newValue === value) return

      setValue(event.newValue)
    }

    window.addEventListener('storage', onStorageChange)
    return () => window.removeEventListener('storage', onStorageChange)
  }, [key, value])

  // Timer para reagir a mudança de estado e mudar o valor no local storage
  useEffect(() => {
    const dispatchStorageChange = (newValue: string | null) =>
      window.dispatchEvent(new StorageEvent('storage', { key, newValue }))

    if (isFirstRun.current) {
      isFirstRun.current = false
      return undefined
    }

    const timer = setTimeout(() => {
      if (value === null || value === undefined) {
        localStorage.removeItem(key)
        dispatchStorageChange(null)
      } else if (value !== localStorage.getItem(key)) {
        localStorage.setItem(key, value)
        dispatchStorageChange(value)
      }
    }, debounceTime)

    return () => clearTimeout(timer)
  }, [key, value])

  // helper
  const removeValue = useCallback(() => setValue(null), [])

  return { value, setValue, removeValue }
}

export { useLocalStorage }
