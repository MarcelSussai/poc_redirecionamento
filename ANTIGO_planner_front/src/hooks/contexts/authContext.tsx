import React, { createContext, FunctionComponent } from "react";
import { Redirect } from "react-router";
import { useAccessToken } from "../useAccessToken";
import { loginPath } from "../../routes"
import { useApiToken } from "../useApiToken";

interface ContextValue {
  accessToken: string;
  apiToken: string;
}

interface Props {}

const Context = createContext<ContextValue | undefined>(undefined)

const Provider: FunctionComponent<Props> = ({ children }) => {
  const { token: accessToken } = useAccessToken()
  const { token: apiToken } = useApiToken()
  if (!accessToken || !apiToken) {
    console.log(`Redirecting user to login page`)
    return <Redirect to={loginPath} />
  }

  return (
    <Context.Provider value={{ accessToken, apiToken }}>
      {children}
    </Context.Provider>
  )
}

export {
  Provider as AuthProvider,
  Context as AuthContext,
}

export type {
  Props as AuthProviderProps,
  ContextValue as AuthContextValue,
}