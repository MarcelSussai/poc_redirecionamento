import React, { createContext, FunctionComponent } from "react";
import { Pessoa } from "../../domain/pessoa/Pessoa";
import { useCurrentPessoaQuery } from '../../queries/pessoa/queries'

interface ContextValue {
  isLoadingPessoa: boolean,
  isErrorPessoa: boolean;
  pessoaError: Error | null;
  personId: number;
  pessoa: Pessoa | undefined
}

interface Props {
  personId: number;
}

const Context = createContext<ContextValue | undefined>(undefined)

const Provider: FunctionComponent<Props> = ({ children, personId }) => {
  const { isLoading, isError, data: pessoa, error: pessoaError } = useCurrentPessoaQuery(personId);
  return (
    <Context.Provider value={{ 
        isLoadingPessoa: isLoading, 
        isErrorPessoa: isError,
        pessoa,
        personId,
        pessoaError
      }}>
      {children}
    </Context.Provider>
  )
}

export {
  Provider as CurrentPersonProvider,
  Context as CurrentPersonContext,
}

export type {
  Props as CurrentPersonProviderProps,
  ContextValue as CurrentPersonContextValue,
}