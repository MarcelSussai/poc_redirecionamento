import React, { createContext, FunctionComponent } from "react";
import { Family } from "../../domain/family/Family";
import { useCurrentFamilyQuery } from '../../queries/family/queries'

interface ContextValue {
  isLoadingFamily: boolean,
  isErrorFamily: boolean;
  familyId: number;
  family: Family | undefined,
  familyError: Error | null
}

interface Props {
  familyId: number;
}

const Context = createContext<ContextValue | undefined>(undefined)

const Provider: FunctionComponent<Props> = ({ children, familyId }) => {
  const { 
    isLoading: isLoadingFamily, 
    isError: isErrorFamily, 
    data: family, 
    error: familyError 
  } = useCurrentFamilyQuery(familyId);
  return (
    <Context.Provider value={{
      isLoadingFamily,
      isErrorFamily,
      family,
      familyId,
      familyError
    }}>
      {children}
    </Context.Provider>
  )
}

export {
  Provider as CurrentFamilyProvider,
  Context as CurrentFamilyContext,
}

export type {
  Props as CurrentFamilyProviderProps,
  ContextValue as CurrentFamilyContextValue,
}