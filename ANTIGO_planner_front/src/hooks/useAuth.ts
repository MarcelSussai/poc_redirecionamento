import { AuthContext } from "./contexts/authContext"
import { useRequiredContext } from "./useRequiredContext"

export const useRequiredAuth = () =>
  useRequiredContext({ Context: AuthContext, name: 'auth'})