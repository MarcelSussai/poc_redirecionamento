import { isAbsent } from '../helpers'
import { useLocalStorage } from './useLocalStorage'

export const useCurrentPersonId = () => {
  const { value: personIdStr } = useLocalStorage("pessoa-id")
  if (isAbsent(personIdStr) ) throw new Error("Person id not found")
  return { personId: parseInt(personIdStr) }
}