import { CurrentPersonContext } from "./contexts/currentPersonContext"
import { useRequiredContext } from "./useRequiredContext"

export const useCurrentPessoa = () => 
  useRequiredContext({ Context: CurrentPersonContext, name: 'currentPerson' })