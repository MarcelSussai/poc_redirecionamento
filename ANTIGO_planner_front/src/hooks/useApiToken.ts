import { useLocalStorage } from "./useLocalStorage"

const useApiToken = () => {
  const { value: token } = useLocalStorage("tokenAcesso")
  return { token }
}

export { useApiToken }