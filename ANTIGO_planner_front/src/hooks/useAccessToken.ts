import { useLocalStorage } from "./useLocalStorage"

const useAccessToken = () => {
  const { value: token } = useLocalStorage("access-token")
  return { token }
}

export { useAccessToken }