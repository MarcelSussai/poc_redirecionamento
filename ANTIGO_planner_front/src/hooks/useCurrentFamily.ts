import { CurrentFamilyContext } from "./contexts/currentFamilyContext"
import { useRequiredContext } from "./useRequiredContext"

export const useCurrentFamily = () => 
  useRequiredContext({ Context: CurrentFamilyContext, name: 'currentFamily' })