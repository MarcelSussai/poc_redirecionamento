import { isAbsent } from '../helpers'
import { useLocalStorage } from './useLocalStorage'

export const useCurrentFamilyId = () => {
  const { value: familyIdStr } = useLocalStorage("familia-id")
  if (isAbsent(familyIdStr) ) throw new Error("Family id not found")
  return { familyId: parseInt(familyIdStr) }
}