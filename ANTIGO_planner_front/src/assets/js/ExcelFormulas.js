var ExcelFormulas = {

    PMT: function(rate, nper, pv, fv, type) {
        if (!fv) fv = 0;
        if (!type) type = 0;

        if (rate === 0) return -(pv + fv) / nper;

        rate = rate / 100;

        var pvif = Math.pow(1 + rate, nper);
        var pmt = rate / (pvif - 1) * -((pv * pvif) + fv);

        if (type === 1) {
            pmt /= (1 + rate);
        };

        if(pmt > 0) {
            return 0;
        }

        return pmt;
    },

    FV: function(rate, periods, value) {

        var result;

        if (rate === 0) {
            result = value;
        } else {
            var term = Math.pow( ( 1 + (rate/100) ), periods);
            result = value * term;
        }

        return result;
    },

    PV: function(rate, periods, value) {

        var result;

        if (rate === 0) {
            result = value;
        } else {
            var term = Math.pow( ( 1 + (rate/100) ), periods);
            result = value / term;
        }

        return result;
    },
    
    NPER: function(rate, payment, present, future, type) {
        // Initialize type
        //var type = (typeof type === 'undefined') ? 0 : type;

        // Initialize future value
        //var future = (typeof future === 'undefined') ? 0 : future;

        // Evaluate rate and periods (TODO: replace with secure expression evaluator)
        rate = rate/100;

        // Return number of periods
        var num = payment * (1 + rate * type) - future * rate;
        var den = (present * rate + payment * (1 + rate * type));
        return Math.log(num / den) / Math.log(1 + rate);
    },

    RATE: function(periods, payment, present, future, type, guess) {
        guess = (guess === undefined) ? 0.01 : guess;
        future = (future === undefined) ? 0 : future;
        type = (type === undefined) ? 0 : type;
      
        // Set maximum epsilon for end of iteration
        var epsMax = 1e-10;
      
        // Set maximum number of iterations
        var iterMax = 10;
      
        // Implement Newton's method
        var y, y0, y1, x0, x1 = 0,f = 0,i = 0;
        var rate = guess;
        if (Math.abs(rate) < epsMax) {
            y = present * (1 + periods * rate) + payment * (1 + rate * type) * periods + future;
        } else {
            f = Math.exp(periods * Math.log(1 + rate));
            y = present * f + payment * (1 / rate + type) * (f - 1) + future;
        }
        y0 = present + payment * periods + future;
        y1 = present * f + payment * (1 / rate + type) * (f - 1) + future;
        i = x0 = 0;
        x1 = rate;
        while ((Math.abs(y0 - y1) > epsMax) && (i < iterMax)) {
            rate = (y1 * x0 - y0 * x1) / (y1 - y0);
            x0 = x1;
            x1 = rate;
            if (Math.abs(rate) < epsMax) {
                y = present * (1 + periods * rate) + payment * (1 + rate * type) * periods + future;
            } else {
                f = Math.exp(periods * Math.log(1 + rate));
                y = present * f + payment * (1 / rate + type) * (f - 1) + future;
            }
            y0 = y1;
            y1 = y;
            ++i;
        }
        return rate;
    }
    
};

export default ExcelFormulas;