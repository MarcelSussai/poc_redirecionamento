import React from "react";
import PassoCadastro from "./components/PassoCadastro";
import PassoCarros from "./components/PassoCarros";
import PassoFilhos from "./components/PassoFilhos";
import PassoFinalizarEtapa from "./components/PassoFinalizarEtapa";
import GestaoOrcamento from "./components/GestaoOrcamento";
import PassoContaBanco from "./components/PassoContaBanco";
import PassoImoveis from "./components/PassoImoveis";
import PassoCartao from "./components/PassoCartao";
import PassoFinal from "./components/PassoFinal";
import Steps from "./components/steps/Steps";
import { confirmAlert } from "react-confirm-alert";

import "./components/steps/Steps.css";

import WizardService from "../../../services/WizardService";

class Wizard extends React.Component {
  stepEnd = false;
  masks = {
    cpf: "111.111.111-11",
    telefone: "(11) 1111-1111",
    celular: "(11) 11111-1111",
    data: "11/11/1111",
    dataVencimento: "11/1111",
  };

  constructor(props) {
    super(props);
    var set = this.getParameterByName("set");
    var step = this.getParameterByName("step");
    var open = this.getParameterByName("open");
    this.state = {
      step: isNaN(parseInt(step)) ? 0 : parseInt(step),
      set: isNaN(parseInt(set)) ? 0 : parseInt(set),
      open: isNaN(parseInt(open)) ? 0 : parseInt(open),
    };
    this.wizardService = new WizardService();
  }

  getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[[]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
      results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return "";
    return decodeURIComponent(results[2].replace(/\+/g, " "));
  }

  componentDidMount() {
    this.setState(
      {
        accessToken: localStorage.getItem("access-token"),
        empresaId: localStorage.getItem("empresa-id"),
        familiaId: localStorage.getItem("familia-id"),
        planejadorId: localStorage.getItem("planejador-id"),
        planejadorPrincipal: localStorage.getItem("planejador-principal"),
        tipoUsuario: localStorage.getItem("tipo-usuario"),
      },
      function () {
        if (this.state.accessToken == null) {
          window.location.href = "/auth/login";
          return;
        }

        this.getWizard();
      }
    );
  }

  getWizard = () => {
    let $this = this;
    this.wizardService.buscarStatusDeConfiguracao(
      this.state.accessToken,
      this.state.familiaId,
      function (data) {
        if (data.success && data.singleResult != null) {
          const options = {
            title: "Deseja Reiniciar a Configuração Inicial?",
            message:
              "Verificamos que você já executou a Configuração Inicial. Como deseja prosseguir?",
            buttons: [
              {
                label: "Voltar para página inicial",
                onClick: () => {
                  window.location.href = "/admin/index";
                },
              },
              {
                label: "Continuar sem limpar",
              },
              {
                label:
                  "Recomeçar (limpar usuários, orçamentos, bancos e cartões)",
                onClick: () => {
                  $this.wizardService.limparConfiguracaoRealizada(
                    $this.state.accessToken,
                    $this.state.planejadorId,
                    $this.state.familiaId,
                    function () {
                      window.location.reload();
                    }
                  );
                },
              },
            ],
          };

          confirmAlert(options);
        }
      }
    );
  };

  stepBack = () => {
    this.setState((state, props) => {
      var step = state.step;
      var set = state.set;
      --step;
      if (step < 0) {
        if (set === 1) {
          set = 0;
          step = 3;
        } else {
          step = 0;
        }
      }
      console.log(set, step);
      return {
        step: step,
        set: set,
      };
    });
  };

  setVisibleStepSet1() {
    switch (this.state.step) {
      case 0:
        return (
          <PassoCadastro
            masks={this.masks}
            stepEnd={this.stepEnd}
            onCancel={() => {
              window.location.href = window.location.origin + "/home";
            }}
            onContinue={() => {
              this.stepEnd = true;
              window.history.pushState({}, "Vista", "?set=0&step=0&open=0");
              this.setState({ step: 1 });
            }}
            onContinueFilho={() => {
              this.stepEnd = true;
              this.setState({ step: 4 });
            }}
          />
        );
      case 1:
        return (
          <PassoCarros
            masks={this.masks}
            onCancel={() => {
              window.location.href = window.location.origin + "/home";
            }}
            onContinue={() => {
              window.history.pushState({}, "Vista", "?set=0&step=1&open=0");
              this.setState({ step: 2 });
            }}
          />
        );
      case 2:
        return (
          <PassoImoveis
            masks={this.masks}
            onCancel={() => {
              window.location.href = window.location.origin + "/home";
            }}
            onContinue={() => {
              this.setState({ step: 3 });
              window.history.pushState({}, "Vista", "?set=0&step=2&open=0");
            }}
          />
        );
      case 3:
        return (
          <PassoFinalizarEtapa
            masks={this.masks}
            onCancel={() => {
              window.location.href = window.location.origin + "/home";
            }}
            onContinue={() => {
              this.setState({ step: 0, set: 1 });
              delete localStorage["stepEnd"];
              window.history.pushState({}, "Vista", "?set=0&step=3&open=0");
            }}
          />
        );
      case 4:
        return (
          <PassoFilhos
            masks={this.masks}
            onCancel={() => {
              window.location.href = window.location.origin + "/home";
            }}
            onContinue={() => {
              this.stepEnd = false;
              this.setState({ step: 0 });
            }}
          />
        );
      default:
        break;
    }
  }

  setVisibleStepSet2() {
    switch (this.state.step) {
      case 0:
        return (
          <GestaoOrcamento
            masks={this.masks}
            open={this.state.open}
            onCancel={() => {
              window.location.href = window.location.origin + "/home";
            }}
            onContinue={() => {
              window.history.pushState({}, "Vista", "?set=1&step=0");
              this.setState({ step: 1 });
            }}
          />
        );
      case 1:
        return (
          <PassoContaBanco
            masks={this.masks}
            onCancel={() => {
              window.location.href = window.location.origin + "/home";
            }}
            onContinue={() => {
              window.history.pushState({}, "Vista", "?set=1&step=1&open=0");
              this.setState({ step: 2 });
            }}
          />
        );
      case 2:
        return (
          <PassoCartao
            masks={this.masks}
            onCancel={() => {
              window.location.href = window.location.origin + "/home";
            }}
            onContinue={() => {
              window.history.pushState({}, "Vista", "?set=1&step=2&open=0");
              this.setState({ step: 3 });
            }}
          />
        );

      case 3:
        //Marca wizard como já executado
        this.wizardService.configuracaoIniciada(
          this.state.accessToken,
          this.state.familiaId
        );
        return (
          <PassoFinal
            masks={this.masks}
            onContinue={() => {
              window.history.pushState({}, "Vista", "?set=1&step=3&open=0");
              window.location.href = window.location.origin + "/home";
            }}
          />
        );
      default:
        break;
    }
  }

  render() {
    return (
      <div style={{ padding: "2rem 1rem" }}>
        <div style={{ paddingBottom: "2rem" }}>
          <img
            src={require("../../../assets/img/theme/icone-calendario.png")}
            alt="Meios de Pagamento"
            className="before-title-img"
          />
          <h3 className="mb-0 chart-title">Configuração Inicial</h3>
        </div>
        <div>
          <div style={{ paddingBottom: "2rem" }}>
            <Steps
              onStepClick={(i) => {
                if (i === 0) {
                  this.stepEnd = true;
                }
                this.setState({ step: i });
              }}
              backPressed={this.stepBack}
              step={this.state.step}
              set={this.state.set}
            />
          </div>
          {(() => {
            switch (this.state.set) {
              case 0:
                return this.setVisibleStepSet1();
              case 1:
                return this.setVisibleStepSet2();
              default:
                break;
            }
          })()}
        </div>
      </div>
    );
  }
}

export default Wizard;
