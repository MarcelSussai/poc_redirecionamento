import React from "react";
import { Row, Col, NavLink, NavItem, Input } from "reactstrap";
import $ from "jquery";
import OrcamentoService from "../../../../services/OrcamentoService";
import "./steps/Steps.css";

class PassoFilhos extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      accessToken: localStorage.getItem("access-token"),
      empresaId: localStorage.getItem("empresa-id"),
      familiaId: localStorage.getItem("familia-id"),
      planejadorId: localStorage.getItem("planejador-id"),
      tipoUsuario: localStorage.getItem("tipo-usuario"),
      numberOfChildren: null,
      members: 0,
      singleFirstName: "",
      children: [],
    };

    this.orcamentoService = new OrcamentoService();
  }

  fabricarQuantidadeFilhos() {
    var array = [];

    for (var i = 1; i < 6; i++) {
      var fun = function () {
        if (this.i > 0) {
          this.$this.setState({ numberOfChildren: this.i });
        }
      }.bind({ i: i, $this: this });
      array.push(
        <NavItem
          className="general-button dark"
          style={{ float: "left", marginLeft: "5px" }}
        >
          <NavLink
            className="nav-link-icon aux-button"
            to="/admin/gestao-planos-sonhos"
            onClick={fun}
          >
            <span className="nav-link-inner--text inner">{i}</span>
          </NavLink>
        </NavItem>
      );
    }
    return array;
  }

  onChangeInput = (e) => {
    var i = e.currentTarget.id.split("-")[2];
    var value = e.target.value;
    this.setState((state) => {
      state.children[i] = value;
    });
  };

  onContinue = (e) => {
    this.recursiveRequestChildrenRegister(this.state.children, 0);
  };

  recursiveRequestChildrenRegister = (children, i) => {
    //Primeiro filho da lista
    if (i === 0 && children.length > 0) {
      global.spinnerShow($);
    }

    //Recursao terminou
    if (children.length === i) {
      var currentScroll = [
        document.documentElement.scrollLeft || document.body.scrollLeft,
        document.documentElement.scrollTop || document.body.scrollTop,
      ];
      global.spinnerHide($, currentScroll);

      this.props.onContinue();

      return;
    }

    var child = children[i];

    let $this = this;

    this.orcamentoService.salvarOrcamento(
      this.state.accessToken,
      {
        Nome: child,
        TipoOrcamento: 1,
        FamiliaId: parseInt(this.state.familiaId),
        EmpresaId: parseInt(this.state.empresaId),
        PlanejadorId: this.state.planejadorId,
        Suspenso: 0,
        OrcamentoModelo: 5,
      },
      function (data) {
        if (data.success === true) {
          $this.recursiveRequestChildrenRegister(children, ++i);
        } else {
          $this.recursiveRequestChildrenRegister(children, ++i);
        }
      }
    );
  };

  editarOrcamento() {
    var array = [];
    for (var i = 0; i < this.state.numberOfChildren; i++) {
      array.push(
        <Col style={{ textAlign: "left" }} lg="4" xl="4">
          <label>Filho {i + 1} *</label>
          <Input id={"id-car-" + i} onChange={this.onChangeInput} type="text" />
        </Col>
      );
    }

    return (
      <div>
        <Row style={{ marginBottom: "1rem" }}>{array}</Row>
      </div>
    );
  }

  render() {
    return (
      <div className="FadeIn" style={{ textAlign: "center" }}>
        <div style={{ width: "100%", textAlign: "center" }}>
          <h4
            style={{
              paddingBottom: "1.5rem",
              fontSize: "1.1rem",
              fontWeight: "100",
            }}
          >
            <font style={{ fontWeight: "900" }}>Quantos filhos</font> a família
            possui?
          </h4>
          <div
            className={this.state.numberOfChildren != null ? "FadeOut" : ""}
            style={{ display: "inline-block" }}
          >
            {this.fabricarQuantidadeFilhos()}
          </div>
          <div
            className={
              this.state.numberOfChildren != null &&
              this.state.numberOfChildren > 0
                ? "FadeIn"
                : "Invisible"
            }
          >
            {this.editarOrcamento()}
          </div>
        </div>
        <div style={{ marginTop: ".25rem" }}>
          <button
            className="featured-button"
            onClick={this.props.onCancel}
            style={{
              backgroundImage:
                "url(" +
                require("../../../../assets/img/theme/botao-destaque.png") +
                ")",
            }}
          >
            Cancelar
          </button>
          <button
            className={
              "featured-button " +
              (this.state.numberOfChildren != null &&
              this.state.numberOfChildren > 0
                ? "FadeIn"
                : "Invisible")
            }
            onClick={this.onContinue}
            style={{
              backgroundImage:
                "url(" +
                require("../../../../assets/img/theme/botao-destaque.png") +
                ")",
              marginLeft: ".25rem",
            }}
          >
            Salvar
          </button>
        </div>
      </div>
    );
  }
}

export default PassoFilhos;
