import React from "react";
import Select from "react-select";
import $ from "jquery";
// import { confirmAlert } from 'react-confirm-alert';
import "./steps/Steps.css";

import OrcamentoService from "../../../../services/OrcamentoService";

import { Row, Col, NavLink, NavItem, Input } from "reactstrap";

class PassosImoveis extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      accessToken: localStorage.getItem("access-token"),
      empresaId: localStorage.getItem("empresa-id"),
      familiaId: localStorage.getItem("familia-id"),
      planejadorId: localStorage.getItem("planejador-id"),
      tipoUsuario: localStorage.getItem("tipo-usuario"),
      numberOfimoveis: null,
      imoveis: [],
    };
    this.orcamentoService = new OrcamentoService();
  }

  fabricarQuantidadeImoveis() {
    var array = [];
    for (var i = 0; i < 6; i++) {
      if (i === 0)
        // Para pular o número 0
        continue;
      var fun = function () {
        this.$this.setState({ numberOfimoveis: this.i });
      }.bind({ i: i, $this: this });
      array.push(
        <NavItem
          className="general-button dark"
          style={{ float: "left", marginLeft: "5px" }}
        >
          <NavLink
            className="nav-link-icon aux-button"
            to="/admin/gestao-planos-sonhos"
            onClick={fun}
          >
            <span className="nav-link-inner--text inner">{i}</span>
          </NavLink>
        </NavItem>
      );
    }
    return array;
  }

  onChangeInput = (e) => {
    var i = e.currentTarget.id.split("-")[2];
    var value = e.target.value;
    this.setState((state) => {
      state.imoveis[i] = value;
    });
  };

  onContinue = () => {
    this.recursiveRequestImovelRegister(this.state.imoveis, 0);
  };

  recursiveRequestImovelRegister = (imoveis, i) => {
    if (imoveis.length === i) {
      // Recursao terminou
      var currentScroll = [
        document.documentElement.scrollLeft || document.body.scrollLeft,
        document.documentElement.scrollTop || document.body.scrollTop,
      ];
      global.spinnerHide($, currentScroll);
      this.props.onContinue();
      return;
    }

    global.spinnerShow($);

    var imovel = imoveis[i];

    let $this = this;

    this.orcamentoService.salvarOrcamento(
      this.state.accessToken,
      {
        Nome: imovel,
        TipoOrcamento: 1,
        FamiliaId: parseInt(this.state.familiaId),
        EmpresaId: parseInt(this.state.empresaId),
        PlanejadorId: this.state.planejadorId,
        Suspenso: 0,
        OrcamentoModelo: 7,
      },
      function () {
        $this.recursiveRequestImovelRegister(imoveis, ++i);
      }
    );
  };

  /** Used in redner */
  editarOrcamento() {
    var array = [];
    for (var i = 0; i < this.state.numberOfimoveis; i++) {
      array.push(
        <Col style={{ textAlign: "left" }} lg="4" xl="4">
          <label>Imóvel {i + 1} *</label>
          <Input id={"id-car-" + i} onChange={this.onChangeInput} type="text" />
        </Col>
      );
    }

    return (
      <div>
        <Row style={{ marginBottom: "1rem" }}>{array}</Row>
      </div>
    );
  }

  /** Used in redner */
  montarDropDown = (itens, id, changeMethod, selectedValue, type) => {
    return (
      <Select
        id={id}
        onChange={changeMethod}
        className="select-component"
        options={itens}
        defaultValue={itens != null ? itens[0] : ""}
        placeholder="Selecione..."
        value={
          itens != null && selectedValue != null
            ? itens.find((op) => {
                if (type === "enum") {
                  return (
                    (op.value != null ? op.value.toString() : null) ===
                    (selectedValue != null ? selectedValue.toString() : null)
                  );
                } else if (op.value != null && selectedValue != null) {
                  return op.value.id === selectedValue.id;
                }
                return false;
              })
            : null
        }
      />
    );
  };

  render() {
    return (
      <div className="FadeIn" style={{ textAlign: "center" }}>
        <div style={{ width: "100%", textAlign: "center" }}>
          <h4
            style={{
              paddingBottom: "1.5rem",
              fontSize: "1.1rem",
              fontWeight: "100",
            }}
          >
            <font style={{ fontWeight: "900" }}>Quantos imóveis</font> a família
            possui, sendo responsável pelos custos deste imóvel?
          </h4>
          <div
            className={this.state.numberOfimoveis != null ? "FadeOut" : ""}
            style={{ display: "inline-block" }}
          >
            {this.fabricarQuantidadeImoveis()}
          </div>
          <div
            className={
              this.state.numberOfimoveis != null &&
              this.state.numberOfimoveis > 0
                ? "FadeIn"
                : "Invisible"
            }
          >
            {this.editarOrcamento()}
          </div>
        </div>
        <div style={{ marginTop: ".25rem" }}>
          <button
            className="featured-button"
            onClick={this.props.onCancel}
            style={{
              backgroundImage:
                "url(" +
                require("../../../../assets/img/theme/botao-destaque.png") +
                ")",
            }}
          >
            Cancelar
          </button>
          <button
            className="featured-button"
            onClick={this.props.onContinue}
            style={{
              backgroundImage:
                "url(" +
                require("../../../../assets/img/theme/botao-destaque.png") +
                ")",
              marginLeft: ".25rem",
            }}
          >
            Pular Passo
          </button>
          <button
            className={
              "featured-button " +
              (this.state.numberOfimoveis != null &&
              this.state.numberOfimoveis > 0
                ? "FadeIn"
                : "Invisible")
            }
            onClick={this.onContinue}
            style={{
              backgroundImage:
                "url(" +
                require("../../../../assets/img/theme/botao-destaque.png") +
                ")",
              marginLeft: ".25rem",
            }}
          >
            Salvar
          </button>
        </div>
      </div>
    );
  }
}

export default PassosImoveis;
