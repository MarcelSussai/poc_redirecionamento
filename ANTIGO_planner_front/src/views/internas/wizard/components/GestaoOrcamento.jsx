import React from "react";
import Lancamentos from "../../Orcamento";
import "./steps/Steps.css";
import $ from "jquery";

class GestaoOrcamento extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      orcamentoVisible: this.props.open === 1,
    };
  }

  putNavBack() {
    $(".navbar-top").addClass("ZIndexBack");
  }

  putNavFront() {
    $(".navbar-top").removeClass("ZIndexBack");
  }

  showOrcamento() {
    if (!this.state.orcamentoVisible) {
      return (
        <div className="FadeIn" style={{ textAlign: "Center" }}>
          <i style={{ fontSize: "6rem" }} className="fas fa-chart-bar"></i>
          <h4
            style={{
              marginTop: "1.35rem",
              marginBottom: "0",
              fontSize: "1.2rem",
              fontWeight: "100",
            }}
          >
            <font style={{ fontWeight: "900" }}>Você quer editar</font> agora as
            estimativas de despesas e receitas?
          </h4>
          <p style={{ marginBottom: "1.35rem" }}>
            Você poderá fazer isso a qualquer momento no menu de gestão de
            orçamento.
          </p>
          <button
            id="salvar-lancamento"
            className="featured-button"
            onClick={() => {
              this.setState({ orcamentoVisible: true });
              window.history.pushState({}, "Vista", "?set=1&step=0&open=1");
            }}
            style={{
              marginRight: "5px",
              backgroundImage:
                "url(" +
                require("../../../../assets/img/theme/botao-destaque.png") +
                ")",
            }}
          >
            Sim
          </button>
          <button
            id="salvar-lancamento"
            className="featured-button"
            onClick={this.props.onContinue}
            style={{
              backgroundImage:
                "url(" +
                require("../../../../assets/img/theme/botao-destaque.png") +
                ")",
            }}
          >
            Próximo Passo
          </button>
        </div>
      );
    } else {
      return (
        <div className="FadeIn">
          <Lancamentos
            onSteps={true}
            putNavBack={this.putNavBack}
            putNavFront={this.putNavFront}
            onContinue={this.props.onContinue}
          />
        </div>
      );
    }
  }

  render = this.showOrcamento;
}

export default GestaoOrcamento;
