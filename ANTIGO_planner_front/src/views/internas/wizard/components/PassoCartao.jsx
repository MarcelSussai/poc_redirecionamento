import React from "react";
import Select from "react-select";
import axios from "axios";
import $ from "jquery";
import SkyLight from "react-skylight";
import { confirmAlert } from "react-confirm-alert";
import { RadioGroup, Radio } from "react-radio-group";

import { Col, Row, Input } from "reactstrap";

class PassoCartao extends React.Component {
  constructor(props) {
    super(props);
    var bandeiras = [{ value: null, label: "Selecione..." }];

    global.bandeira.forEach(function (value, index) {
      bandeiras.push({
        value: index,
        label: value,
        image: (value + ".png").toLowerCase(),
      });
    });
    this.state = {
      accessToken: localStorage.getItem("access-token"),
      empresaId: localStorage.getItem("empresa-id"),
      familiaId: localStorage.getItem("familia-id"),
      donosCombo: [],
      comboBancos: [],
      bandeiras: bandeiras,
    };
  }

  putNavBack() {
    $(".navbar-top").addClass("ZIndexBack");
  }

  putNavFront() {
    $(".navbar-top").removeClass("ZIndexBack");
  }

  componentDidMount() {
    //var url = global.server_api + 'api/banco/filtro';
    var url = global.server_api_new + global.apiToken + "/banco/filtro";

    var filtro = {
      Filtro: this.state.textoDigitado,
      Ordenacao: this.state.sortField,
    };

    var config = {
      headers: {
        Authorization: "bearer " + this.state.accessToken,
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Headers": "Authorization",
        "Access-Control-Allow-Methods":
          "GET, POST, OPTIONS, PUT, PATCH, DELETE",
      },
    };

    axios.post(url, filtro, config).then((res) => {
      const bancos = res.data.results;
      const comboBancos = this.state.comboBancos;
      bancos.forEach((value) => {
        comboBancos.push({
          value: value.id,
          label: value.nome + " " + value.codigoComp,
        });
      });
      this.setState({ bancos, comboBancos });
    });
    this.atualizarDonos();
  }

  mudarBandeira = (selectedOption) => {
    const bandeira = selectedOption;
    this.setState({ bandeira });
  };

  atualizarDonos = () => {
    //var url = global.server_api + 'api/pessoa/familia/'+this.state.familiaId+'/filtro';
    var url =
      global.server_api_new +
      global.apiToken +
      "/pessoa/familia/" +
      this.state.familiaId +
      "/filtro";

    var config = {
      headers: {
        Authorization: "bearer " + this.state.accessToken,
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Headers": "Authorization",
        "Access-Control-Allow-Methods":
          "GET, POST, OPTIONS, PUT, PATCH, DELETE",
      },
    };

    axios.post(url, {}, config).then((res) => {
      const donos = res.data.results;
      var donosCombo = [
        {
          label: "Selecione...",
          value: null,
        },
      ];

      if (donos != null) {
        donos.forEach((mp) => {
          donosCombo.push({
            value: mp,
            label: mp.nome + " " + mp.sobrenome,
          });
        });
      }

      this.setState({ donos });
      this.setState({ donosCombo });
    });
  };

  montarDropDown = (itens, id, changeMethod, selectedValue, type) => (
    <Select
      id={id}
      onChange={changeMethod}
      className="select-component"
      options={itens}
      value={
        itens != null && selectedValue != null
          ? itens.find((op) => {
              return op.value === selectedValue;
            })
          : null
      }
      placeholder="Selecione..."
    />
  );

  onContinue = () => {
    var item = {
      nome: this.state.nome,
      tipoMeioPagamento: 2,
      bancoId: this.state.bancoSelecionado,
      pessoaId: this.state.dono != null ? this.state.dono.id : null,
      bandeira: this.state.bandeira,
      bandeiraPrePago: null,
      diaVencimento: this.state.diaVencimento,
      agencia: this.state.agencia,
      conta: this.state.conta,
      familiaId:
        this.state.familiaId != null ? parseInt(this.state.familiaId) : null,
      empresaId:
        this.state.empresaId != null ? parseInt(this.state.empresaId) : null,
    };

    //var url = global.server_api + 'api/meioPagamento' + (this.state.id != null ? "/" + this.state.id: "");
    var url =
      global.server_api_new +
      global.apiToken +
      "/meioPagamento" +
      (this.state.id != null ? "/" + this.state.id : "");

    var config = {
      headers: {
        Authorization: "bearer " + this.state.accessToken,
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Headers": "Authorization",
        "Access-Control-Allow-Methods":
          "GET, POST, OPTIONS, PUT, PATCH, DELETE",
      },
    };

    var currentScroll = [
      document.documentElement.scrollLeft || document.body.scrollLeft,
      document.documentElement.scrollTop || document.body.scrollTop,
    ];

    global.spinnerShow($);
    console.log(item);
    axios.post(url, item, config).then((res) => {
      global.spinnerHide($, currentScroll);
      if (res.data.success === true) {
        this.putNavBack();
        this.limparCampos();
        global.showModal(this.modal);
      } else {
        console.log(res.data.exception);
        const options = {
          title: "Erro ao Salvar Registro",
          message: res.data.exception.Message,
          buttons: [
            {
              label: "Ok",
            },
          ],
        };

        confirmAlert(options);
      }
    });
  };

  limparCampos = () => {
    this.setState({
      nome: "",
      bancoSelecionado: null,
      pessoaId: null,
      bandeira: 0,
      diaVencimento: "",
      agencia: "",
      dono: null,
      conta: "",
    });
  };

  mostraOpcoesBandeiras = () => {
    return this.state.bandeiras.map((item, key) => {
      if (item.image == null) return <></>;

      return (
        <div
          key={key}
          style={{ float: "left", marginRight: "5px", marginBottom: "10px" }}
        >
          <Radio value={item.value} />
          <img
            src={require("../../../../assets/img/theme/" + item.image)}
            alt={item.label}
            style={{ width: "30px", marginLeft: "5px" }}
          />
        </div>
      );
    });
  };

  render = () => {
    return (
      <div className="FadeIn">
        <SkyLight
          afterClose={() => {
            this.putNavFront();
          }}
          ref={(ref) => (this.modal = ref)}
          title={
            <>
              <div className="pop-up-title">
                <h2 id="ajuda-titulo">Cadastrar Outro Cartão de Crédito?</h2>
              </div>
            </>
          }
        >
          <p>
            Deseja cadastrar outro cartão de crédito? Você também poderá
            cadastrar outro mais tarde.
          </p>
          <button
            id="salvar-lancamento"
            className="featured-button"
            onClick={() => {
              this.putNavFront();
              this.modal.hide();
            }}
            style={{
              marginRight: "5px",
              backgroundImage:
                "url(" +
                require("../../../../assets/img/theme/botao-destaque.png") +
                ")",
            }}
          >
            Novo Cartão
          </button>
          <button
            id="salvar-lancamento"
            className="featured-button"
            onClick={this.props.onContinue}
            style={{
              marginRight: "5px",
              backgroundImage:
                "url(" +
                require("../../../../assets/img/theme/botao-destaque.png") +
                ")",
            }}
          >
            Próximo Passo
          </button>
        </SkyLight>

        <h4
          style={{
            paddingBottom: "0",
            fontSize: "1.1rem",
            fontWeight: "100",
            textAlign: "center",
          }}
        >
          <font style={{ fontWeight: "900" }}>Quais cartões de crédito</font>{" "}
          que você usa?
        </h4>
        <p
          style={{ maring: "0", marginBottom: "1.35rem", textAlign: "center" }}
        >
          Lembre-se de colocar um nome em que você identificará facilmente.
        </p>
        <Row>
          <Col lg="4" xl="4">
            <div>
              <label>Identificador do cartão *</label>
              <Input
                value={this.state.nome}
                onChange={(e) => this.setState({ nome: e.target.value })}
                type="text"
              />
            </div>
          </Col>
          <Col lg="4" xl="4">
            <div>
              <label>Dono *</label>
              <Select
                onChange={(e) => this.setState({ dono: e.value })}
                className="select-dono"
                options={this.state.donosCombo}
                value={
                  this.state.donosCombo != null && this.state.dono != null
                    ? this.state.donosCombo.find((op) => {
                        return op.value === this.state.dono;
                      })
                    : null
                }
                placeholder="Selecione..."
              />
            </div>
          </Col>
          <Col lg="4" xl="4" className="cartaocredito">
            <div>
              <label>Bandeira *</label>
              <RadioGroup
                name="bandeiras"
                selectedValue={this.state.bandeira}
                onChange={this.mudarBandeira}
              >
                {this.mostraOpcoesBandeiras()}
              </RadioGroup>
            </div>
          </Col>
          <Col lg="4" xl="4">
            <label style={{ maring: "0px !important", textAlign: "left" }}>
              Banco Emissor
            </label>
            <Select
              onChange={(e) => {
                this.setState({ bancoSelecionado: e.value });
              }}
              value={
                this.state.comboBancos != null &&
                this.state.bancoSelecionado != null
                  ? this.state.comboBancos.find((op) => {
                      return op.value === this.state.bancoSelecionado;
                    })
                  : null
              }
              options={this.state.comboBancos}
              className="select-component"
            />
          </Col>
          <Col lg="4" xl="4">
            <div>
              <label>Dia de vencimento da fatura</label>
              <Input
                value={this.state.diaVencimento}
                onChange={(e) =>
                  this.setState({ diaVencimento: e.target.value })
                }
                mask={this.props.masks["dataVencimento"]}
                className="form-control"
              />
            </div>
          </Col>
          <Col lg="12" xl="12">
            <div
              style={{ width: "100%", textAlign: "center", marginTop: "1rem" }}
            >
              <button
                id="salvar-lancamento"
                className="featured-button"
                onClick={this.props.onCancel}
                style={{
                  marginRight: "5px",
                  backgroundImage:
                    "url(" +
                    require("../../../../assets/img/theme/botao-destaque.png") +
                    ")",
                }}
              >
                Cancelar
              </button>
              <button
                id="salvar-lancamento"
                className="featured-button"
                onClick={this.onContinue}
                style={{
                  backgroundImage:
                    "url(" +
                    require("../../../../assets/img/theme/botao-destaque.png") +
                    ")",
                }}
              >
                Continuar
              </button>
            </div>
          </Col>
        </Row>
      </div>
    );
  };
}

export default PassoCartao;
