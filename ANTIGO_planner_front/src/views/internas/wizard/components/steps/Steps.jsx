import React from 'react';
import './Steps.css';
import $ from 'jquery';

class Steps extends React.Component {

    componentDidMount() {
        $(window).scroll((e) => {
            var $this = $(window);
            if ($this.scrollTop() <= 60) {
                $(".Steps").removeClass("OpacityOut");
            } else {
                $(".Steps").addClass("OpacityOut");
            }
        });
    }

    confirarConjuntos() {
        switch(this.props.set) {
            case 0:
                return this.configurarPrimeiroConjunto();
            case 1:
                return this.configurarSegundoConjunto();
            default:
                return this.configurarPrimeiroConjunto();
        }
    }

    configurarSegundoConjunto() {
        var icons = ["fas fa-chart-bar", "fas fa-piggy-bank", "fas fa-credit-card", "fas fa-check"];
        return this.fabricarConjunto(icons);
    }

    configurarPrimeiroConjunto() {
        var icons = ["fas fa-clipboard-list", "fas fa-car-side", "fas fa-home", "far fa-thumbs-up"];
        return this.fabricarConjunto(icons);
    }

    fabricarConjunto(icons) {
        var array = []
        for(var i = 0; i < icons.length; i++) {
            var status = ""
            if(this.props.step < i) {
                status = "";
            } else if(this.props.step === i) {
                status = "active";
            } else {
                status = "fixed";
            }
            if(i === 0) {
                array.push(this.fabricarCirculo(i, icons[i], status));
            } else {
                array.push(this.fabricarLinha(status));
                array.push(this.fabricarCirculo(i, icons[i], status));
            }
        }
        return(
        <div className="Steps">
            <i style={{    position: "inherit", cursor: "pointer",
                            bottom: "1.25rem",
                            right: "1rem",
                            fontSize: "1.8rem" }}  onClick={this.props.backPressed} class="fas fa-caret-left"></i>
            {array}
        </div>  
        );
    }

    fabricarCirculo(i, icon, status) {
        return(                
            <div onClick={() => {
                this.props.onStepClick(i);
            }}className={"CircleHolder " + status }>
                <div className={ "Circle " + status}>
                    <i class={icon}></i>
                </div>
            </div>
        );
    }

    fabricarLinha(status) {
        return (
        <div className="Line">
            <div className={"LineFiller " + status}></div>
        </div>
        );
    }

    render() { 
        return (
            <div>
                { this.confirarConjuntos() }
            </div>
        );
    }
}

export default Steps;