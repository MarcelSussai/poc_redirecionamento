import React from "react";
import "./steps/Steps.css";

class PassoFinal extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      numberOfCars: null,
    };
  }

  render() {
    return (
      <div className="FadeIn" style={{ textAlign: "Center" }}>
        <i style={{ fontSize: "6rem" }} className="fas fa-check"></i>
        <h4 style={{ fontSize: "1.2rem", fontWeight: "100" }}>
          Ótimo! Você realizou a sua configuração inicial.{" "}
          <font style={{ fontSize: "1.5rem", fontWeight: "900" }}>
            Bem Vindo ao Vista!
          </font>
        </h4>
        <p style={{ marginBottom: "1.2rem" }}>
          Outras informações e edições ainda serão solicitadas, mas desde já,
          você poderá usar o sistema.
        </p>
        <button
          id="salvar-lancamento"
          className="featured-button"
          onClick={this.props.onContinue}
          style={{
            backgroundImage:
              "url(" +
              require("../../../../assets/img/theme/botao-destaque.png") +
              ")",
          }}
        >
          Começar
        </button>
      </div>
    );
  }
}

// "far fa-thumbs-up"  Legal!! Construímos uma estrutura padrão para o seu orçamento. A qualquer momento você poderá editá-la.

export default PassoFinal;
