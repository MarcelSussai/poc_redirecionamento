import React from "react";
import Select from "react-select";
import SkyLight from "react-skylight";
import axios from "axios";
import "./steps/Steps.css";
import $ from "jquery";
import { confirmAlert } from "react-confirm-alert";

import { Col, Row, Input } from "reactstrap";

class PassoContaBanco extends React.Component {
  constructor(props) {
    super(props);
    var comboBancos = [{ value: null, label: "Selecione..." }];
    this.state = {
      cadastrosConcluidos: 0,
      comboBancos: comboBancos,
      bancoSelecionado: null,
      conta: "00000",
      agencia: "000000",
      nome: "", //A PEDIDO DO LUIZ 05/05/2020 10h57
      accessToken: localStorage.getItem("access-token"),
      empresaId: localStorage.getItem("empresa-id"),
      familiaId: localStorage.getItem("familia-id"),
    };
  }

  componentDidMount() {
    this.setState({
      accessToken: localStorage.getItem("access-token"),
      empresaId: localStorage.getItem("empresa-id"),
      familiaId: localStorage.getItem("familia-id"),
    });
    this.getData();
  }

  getData() {
    //var url = global.server_api + 'api/banco/filtro';
    var url = global.server_api_new + global.apiToken + "/banco/filtro";

    var filtro = {
      Filtro: this.state.textoDigitado,
      Ordenacao: this.state.sortField,
    };

    var config = {
      headers: {
        Authorization: "bearer " + this.state.accessToken,
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Headers": "Authorization",
        "Access-Control-Allow-Methods":
          "GET, POST, OPTIONS, PUT, PATCH, DELETE",
      },
    };

    axios.post(url, filtro, config).then((res) => {
      const bancos = res.data.results;
      const comboBancos = this.state.comboBancos;
      bancos.forEach((value) => {
        comboBancos.push({
          value: value.id,
          label: value.nome + " " + value.codigoComp,
        });
      });
      this.setState({ bancos, comboBancos });
    });
    this.atualizarDonos();
  }

  onContinue = () => {
    var item = {
      nome: this.state.nome,
      tipoMeioPagamento: 3,
      bancoId: this.state.bancoSelecionado,
      pessoaId: this.state.dono != null ? this.state.dono.id : null,
      bandeira: null,
      bandeiraPrePago: null,
      diaVencimento: null,
      agencia: this.state.agencia,
      conta: this.state.conta,
      familiaId:
        this.state.familiaId != null ? parseInt(this.state.familiaId) : null,
      empresaId:
        this.state.empresaId != null ? parseInt(this.state.empresaId) : null,
    };

    //var url = global.server_api + 'api/meioPagamento' + (this.state.id != null ? "/" + this.state.id: "");
    var url =
      global.server_api_new +
      global.apiToken +
      "/meioPagamento" +
      (this.state.id != null ? "/" + this.state.id : "");

    var config = {
      headers: {
        Authorization: "bearer " + this.state.accessToken,
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Headers": "Authorization",
        "Access-Control-Allow-Methods":
          "GET, POST, OPTIONS, PUT, PATCH, DELETE",
      },
    };

    var currentScroll = [
      document.documentElement.scrollLeft || document.body.scrollLeft,
      document.documentElement.scrollTop || document.body.scrollTop,
    ];

    global.spinnerShow($);
    console.log(item);
    axios.post(url, item, config).then((res) => {
      global.spinnerHide($, currentScroll);
      if (res.data.success === true) {
        this.setState(function (state) {
          ++state.cadastrosConcluidos;
        });
        global.showModal(this.modal);
      } else {
        console.log(res.data.exception);
        const options = {
          title: "Erro ao Salvar Registro",
          message: res.data.exception.Message,
          buttons: [
            {
              label: "Ok",
            },
          ],
        };

        confirmAlert(options);
      }
    });
  };

  montarDropDown = (itens, id, changeMethod, selectedValue, type) => (
    <Select
      id={id}
      onChange={changeMethod}
      className="select-component"
      options={itens}
      placeholder="Selecione..."
    />
  );

  atualizarDonos = () => {
    //var url = global.server_api + 'api/pessoa/familia/'+this.state.familiaId+'/filtro';
    var url =
      global.server_api_new +
      global.apiToken +
      "/pessoa/familia/" +
      this.state.familiaId +
      "/filtro";

    var config = {
      headers: {
        Authorization: "bearer " + this.state.accessToken,
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Headers": "Authorization",
        "Access-Control-Allow-Methods":
          "GET, POST, OPTIONS, PUT, PATCH, DELETE",
      },
    };

    axios.post(url, {}, config).then((res) => {
      const donos = res.data.results;
      var donosCombo = [
        {
          label: "Selecione...",
          value: null,
        },
      ];

      if (donos != null) {
        donos.forEach((mp) => {
          donosCombo.push({
            value: mp,
            label: mp.nome + " " + mp.sobrenome,
          });
        });
      }

      this.setState({ donos });
      this.setState({ donosCombo });
    });
  };

  render() {
    return (
      <>
        <div
          className="FadeIn"
          style={{ textAlign: "Center", position: "relative", zIndex: "99999" }}
        >
          <h4
            style={{
              marginTop: "1.35rem",
              marginBottom: "0",
              fontSize: "1.2rem",
              fontWeight: "100",
            }}
          >
            Agora vamos <font style={{ fontWeight: "900" }}>cadastrar</font> os
            bancos que você utiliza?
          </h4>
          <p style={{ maring: "0", marginBottom: "1.35rem" }}>
            Não se preocupe, não vamos pedir senhas e nem dados sigilosos da sua
            conta ou cartão. Será apenas um registro para que você possa
            identificar.
          </p>
          <Row>
            <Col
              lg="4"
              xl="4"
              styles={{
                zIndex: 999999,
              }}
            >
              <label style={{ maring: "0px !important", textAlign: "left" }}>
                Em que banco você tem conta?
              </label>
              <Select
                onChange={(e) => {
                  try {
                    this.setState({
                      bancoSelecionado: e.value,
                      codigoBanco: e.label.match(/\d+/)[0],
                    });
                  } catch (e) {
                    this.setState({ bancoSelecionado: null, codigoBanco: "" });
                  } finally {
                    //this.setState({nome: this.state.bancoSelecionado + " | " + this.state.agencia + " | " + this.state.conta});
                  }
                }}
                options={this.state.comboBancos}
                className="select-component"
              />
            </Col>
            <Col lg="4" xl="4">
              <div
                className={
                  this.state.bancoSelecionado != null ? "FadeIn" : "Invisible"
                }
              >
                <label>Agência</label>
                <Input
                  value={this.state.agencia}
                  onChange={(e) => {
                    this.setState({ agencia: e.target.value });
                    // if(this.state.conta)
                    //     this.setState({ nome: this.state.codigoBanco + " | " + e.target.value + " | " + this.state.conta})
                    // else
                    //     this.setState({ nome: e.target.value})
                    //A PEDIDO DO LUIZ 05/05/2020 10h57
                  }}
                  type="text"
                />
              </div>
            </Col>
            <Col lg="4" xl="4">
              <div
                className={
                  this.state.bancoSelecionado != null ? "FadeIn" : "Invisible"
                }
              >
                <label>Conta</label>
                <Input
                  value={this.state.conta}
                  onChange={(e) => {
                    this.setState({ conta: e.target.value });
                    // if(this.state.agencia)
                    //     this.setState({ nome: this.state.codigoBanco + " | " + this.state.agencia + " | " + e.target.value })
                    // else
                    //     this.setState({ nome: e.target.value})
                    //A PEDIDO DO LUIZ 05/05/2020 10h57
                  }}
                  type="text"
                />
              </div>
            </Col>
            <Col lg="4" xl="4">
              <div
                className={
                  this.state.bancoSelecionado != null ? "FadeIn" : "Invisible"
                }
              >
                <label>Descrição banco *</label>
                <Input
                  onChange={(e) => this.setState({ nome: e.target.value })}
                  // placeholder={
                  // (() => {
                  //     if(this.state.agencia && this.state.conta)
                  //         return this.state.codigoBanco + " | " + this.state.agencia + " | " + this.state.conta
                  //     else if(this.state.agencia)
                  //         return this.state.codigoBanco + " | " + this.state.agencia
                  //     else if(this.state.conta)
                  //         return this.state.codigoBanco + " | " +     this.state.conta
                  // })()}
                  //A PEDIDO DO LUIZ 05/05/2020 10h57
                  type="text"
                />
              </div>
            </Col>
            <Col lg="4" xl="4">
              <div
                className={
                  this.state.bancoSelecionado != null ? "FadeIn" : "Invisible"
                }
                style={{ position: "relative", zIndex: 99999999 }}
              >
                <label>Dono dessa conta *</label>
                {this.montarDropDown(
                  this.state.donosCombo,
                  "select-dono",
                  (e) => this.setState({ dono: e.value }),
                  this.state.dono,
                  "model"
                )}
              </div>
            </Col>
          </Row>
          <div style={{ marginTop: "10px" }}>
            <button
              id="salvar-lancamento"
              className={
                "featured-button " +
                (this.state.bancoSelecionado != null ? "FadeIn" : "Invisible")
              }
              onClick={this.props.onCancel}
              style={{
                paddingTop: "1.35rem !important",
                backgroundImage:
                  "url(" +
                  require("../../../../assets/img/theme/botao-destaque.png") +
                  ")",
              }}
            >
              Cancelar
            </button>
            <button
              id="salvar-lancamento"
              className={
                "featured-button " +
                (this.state.bancoSelecionado != null &&
                this.state.cadastrosConcluidos >= 1
                  ? "FadeIn"
                  : "Invisible")
              }
              onClick={this.props.onContinue}
              style={{
                backgroundImage:
                  "url(" +
                  require("../../../../assets/img/theme/botao-destaque.png") +
                  ")",
                paddingTop: "1.35rem !important",
                marginLeft: "5px",
              }}
            >
              Pular Passo
            </button>
            <button
              id="salvar-lancamento"
              className={
                "featured-button " +
                (this.state.bancoSelecionado != null ? "FadeIn" : "Invisible")
              }
              onClick={this.onContinue}
              style={{
                backgroundImage:
                  "url(" +
                  require("../../../../assets/img/theme/botao-destaque.png") +
                  ")",
                paddingTop: "1.35rem !important",
                marginLeft: "5px",
              }}
            >
              Salvar
            </button>
          </div>
        </div>
        <div>
          <SkyLight
            ref={(ref) => (this.modal = ref)}
            title={
              <>
                <div className="pop-up-title">
                  <h2 id="ajuda-titulo">Cadastrar Outra Conta?</h2>
                </div>
              </>
            }
          >
            <p>
              Deseja cadastrar outra conta? Você também poderá cadastrar outra
              mais tarde.
            </p>
            <button
              id="salvar-lancamento"
              className="featured-button"
              onClick={() => {
                this.setState({ agencia: "", conta: "", nome: "" });
                this.modal.hide();
              }}
              style={{
                marginRight: "5px",
                backgroundImage:
                  "url(" +
                  require("../../../../assets/img/theme/botao-destaque.png") +
                  ")",
              }}
            >
              Nova Conta
            </button>
            <button
              id="salvar-lancamento"
              className="featured-button"
              onClick={() => {
                this.props.onContinue();
              }}
              style={{
                marginRight: "5px",
                backgroundImage:
                  "url(" +
                  require("../../../../assets/img/theme/botao-destaque.png") +
                  ")",
              }}
            >
              Próximo Passo
            </button>
          </SkyLight>
        </div>
      </>
    );
  }
}

export default PassoContaBanco;
