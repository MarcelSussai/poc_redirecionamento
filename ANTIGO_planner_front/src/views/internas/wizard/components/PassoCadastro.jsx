import React from "react";
import MaskedInput from "react-maskedinput";
import $ from "jquery";
import { confirmAlert } from "react-confirm-alert";
import Select from "react-select";
import Switch from "react-switch";
import SkyLight from "react-skylight";
import "./steps/Steps.css";

import Pessoas from "../../Pessoas";
import OrcamentoService from "../../../../services/OrcamentoService";
import PessoaService from "../../../../services/PessoaService";

import { Col, Row, Input } from "reactstrap";

class PassoCadastro extends React.Component {
  pessoasSalvas = [];

  tipoOrcamento = {
    receita: 0,
    despesa: 1,
  };

  orcamentoPadrao = {
    adultoCasado: 1,
    adultoSolteiro: 14,
    filhos: 5,
    familia: 6,
    receita: 2,
  };

  constructor(props) {
    super(props);
    this.ready = [];

    this.pessoaVazia = {
      Id: null,
      Nome: null,
      Sobrenome: null,
      Documento: null,
      EstadoCivil: null,
      Endereco: null,
      Complemento: null,
      DataNascimento: null,
      Pais: null,
      Cidade: null,
      Estado: null,
      CEP: null,
      Celular: null,
      Telefone: null,
      UsuarioDTO: {
        Senha: null,
        NovaSenha: null,
        Email: null,
        Tipo: 2,
      },
      FamiliaId: parseInt(localStorage.getItem("familia-id")),
      EmpresaId: parseInt(localStorage.getItem("empresa-id")),
      PlanejadorId: parseInt(localStorage.getItem("planejador-id")),
      TemCarteira: null,
      IdContaFinanceira: null,
      tipoDePessoa: null,
      salvaNoWizard: false,
    };

    this.orcamentoVazio = {
      Nome: null,
      TipoOrcamento: null,
      FamiliaId: parseInt(localStorage.getItem("familia-id")),
      EmpresaId: parseInt(localStorage.getItem("empresa-id")),
      PlanejadorId: parseInt(localStorage.getItem("planejador-id")),
      Suspenso: 0,
      OrcamentoModelo: null,
    };

    this.state = {
      accessToken: localStorage.getItem("access-token"),
      tipoUsuario: parseInt(localStorage.getItem("tipo-usuario")),
      familiaId: parseInt(localStorage.getItem("familia-id")),
      planejadorId: localStorage.getItem("planejador-id"),
      item: this.pessoaVazia,
      estadoCivilCombo: [],
      members: [],
      temCarteira: null,
      quantidadeTotalDePessoas: 0,
      quantidadeDePessoas: 0,
      quantidadeDeAdultos: 0,
      pessoasVisivel: true,
    };

    this.orcamentoService = new OrcamentoService();
    this.pessoaService = new PessoaService();
  }

  componentDidMount() {
    var estadoCivilCombo = [{ value: null, label: "Selecione..." }];

    global.estadoCivil.forEach(function (value, index) {
      estadoCivilCombo.push({
        value: index,
        label: value,
      });
    });

    if (this.props.stepEnd) {
      global.showModal(this.modalPessoas);
    }

    this.setState({ estadoCivilCombo });

    this.buscarPessoas();
  }

  beforeContinue = () => {
    //Nenhuma pessoa existente
    if (this.pessoasSalvas == null || this.pessoasSalvas.length === 0) {
      //Continuar processo
      this.props.onContinue();
      return;
    }

    //Filtra pessoas cadastradas agora
    var pessoasSalvasAgora = this.pessoasSalvas.filter(function (item) {
      return item.salvaNoWizard === true;
    });

    //Nenhuma pessoa cadastrada agora
    if (pessoasSalvasAgora == null || pessoasSalvasAgora.length === 0) {
      //Continuar processo
      this.props.onContinue();
      return;
    }

    this.onContinue(pessoasSalvasAgora, 0);
  };

  onContinue = (pessoasSalvas, index) => {
    let $this = this;

    //Se ainda é um index valido
    if (index < pessoasSalvas.length) {
      var pessoa = pessoasSalvas[index];

      var orcamentoPadrao = null;

      //Filhos
      if (pessoa.tipoDePessoa !== 1) {
        orcamentoPadrao = this.orcamentoPadrao.filhos;
      }
      //Todas as pessoas foram cadastradas agora
      else if (
        this.state.quantidadeTotalDePessoas === this.state.quantidadeDePessoas
      ) {
        orcamentoPadrao =
          this.state.quantidadeDeAdultos > 1
            ? this.orcamentoPadrao.adultoCasado //Marca perfil como casado
            : this.orcamentoPadrao.adultoSolteiro; //Marca perfil como solteiro
      }
      //Tinha pessoas cadastradas anteriormente, re-execução,
      //nao da para saber o perfil, marca como casado
      else {
        orcamentoPadrao = this.orcamentoPadrao.adultoCasado;
      }

      console.log("orcamentoPadrao", orcamentoPadrao);

      //Nome do orçamento de despesa
      var nomeOrcamento = pessoa.Nome;

      //Cria orcamento de despesa se nao existe
      this.salvarOrcamento(
        nomeOrcamento,
        this.tipoOrcamento.despesa,
        orcamentoPadrao,
        function (data) {
          if (data.success === true) {
            //Nome do orçamento de receita
            nomeOrcamento = "Receitas " + pessoa.Nome;

            $this.salvarOrcamento(
              nomeOrcamento,
              $this.tipoOrcamento.receita,
              $this.orcamentoPadrao.receita,
              function (data2) {
                if (data2.success === false) {
                  console.log(data.exception);
                  const options = {
                    title: "Erro ao salvar orçamento de receita",
                    message: data.exception.Message,
                    buttons: [
                      {
                        label: "Ok",
                      },
                    ],
                  };

                  confirmAlert(options);
                  return;
                }

                // continua salvamento recursivo do proximo item
                index++;
                $this.onContinue(pessoasSalvas, index);
              }
            );
          } else {
            console.log(data.exception);
            const options = {
              title: "Erro ao salvar orçamento de despesa",
              message: data.exception.Message,
              buttons: [
                {
                  label: "Ok",
                },
              ],
            };

            confirmAlert(options);
          }
        }
      );
    } else {
      //Se quantidade de pessoas maior que 1, cria orcamento familia
      if (this.state.quantidadeDePessoas > 1) {
        nomeOrcamento = "Família";

        this.salvarOrcamento(
          nomeOrcamento,
          this.tipoOrcamento.despesa,
          this.orcamentoPadrao.familia,
          function () {
            //Continuar processo do wizard
            $this.props.onContinue();
          }
        );
      } else {
        //Continuar processo do wizard
        this.props.onContinue();
      }
    }
  };

  onChangeItem(value, fieldName) {
    var item = { ...this.state.item };
    item[fieldName] = value;
    this.setState({ item });
  }

  onChangeUsuarioDto(value, fieldName) {
    var item = { ...this.state.item };
    var UsuarioDTO = { ...this.state.item.UsuarioDTO };
    UsuarioDTO[fieldName] = value;
    item.UsuarioDTO = UsuarioDTO;
    this.setState({ item: item });
  }

  onChangeUsuarioDtoSenha(value) {
    var item = { ...this.state.item };
    var UsuarioDTO = { ...this.state.item.UsuarioDTO };
    UsuarioDTO["Senha"] = value;
    UsuarioDTO["NovaSenha"] = value;
    item.UsuarioDTO = UsuarioDTO;
    this.setState({ item: item });
  }

  onSelect(value) {
    if (value === 2) {
      this.props.onContinueFilho();

      return;
    }

    var item = this.state.item;

    item.tipoDePessoa = value;

    this.setState({ item: item });
  }

  limparSelecao = () => {
    this.setState({
      item: this.pessoaVazia,
    });
  };

  buscarPessoas = () => {
    var filtro = {};

    let $this = this;

    this.pessoaService.buscarPessoasPorFamilia(
      this.state.accessToken,
      this.state.familiaId,
      filtro,
      function (data) {
        $this.pessoasSalvas = data.results;
        var total = data.results == null ? 0 : data.results.length;
        $this.setState({ quantidadeTotalDePessoas: total });
      }
    );
  };

  salvarPessoa = () => {
    this.setState({ pessoasVisivel: false });

    var currentScroll = [
      document.documentElement.scrollLeft || document.body.scrollLeft,
      document.documentElement.scrollTop || document.body.scrollTop,
    ];

    global.spinnerShow($);

    var item = this.state.item;

    let $this = this;

    this.pessoaService.salvarPessoa(
      this.state.accessToken,
      item,
      function (data) {
        global.spinnerHide($, currentScroll);

        $this.setState({ pessoasVisivel: true });

        if (data.success === true) {
          var pessoaId = data.singleResult.id;

          item.id = pessoaId;
          item.salvaNoWizard = true;

          $this.pessoasSalvas.push(item);

          var quantidadeDePessoas = $this.state.quantidadeDePessoas;
          var quantidadeDeAdultos = $this.state.quantidadeDeAdultos;
          var quantidadeTotalDePessoas = $this.state.quantidadeTotalDePessoas;

          quantidadeDePessoas++;
          quantidadeTotalDePessoas++;

          if ($this.state.item.tipoDePessoa === 1) {
            quantidadeDeAdultos++;
          }

          $this.setState({
            quantidadeDeAdultos: quantidadeDeAdultos,
            quantidadeDePessoas: quantidadeDePessoas,
            quantidadeTotalDePessoas: quantidadeTotalDePessoas,
          });

          $this.limparSelecao();

          global.showModal($this.modal);
        } else {
          console.log(data.exception);
          const options = {
            title: "Erro ao salvar usuário",
            message: data.exception.Message,
            buttons: [
              {
                label: "Ok",
              },
            ],
          };

          confirmAlert(options);
        }
      }
    );
  };

  mostrarCampoPossuiInvestimentos = () => {
    if (this.state.tipoUsuario === 1) {
      return [
        <Col lg="12" xl="12">
          <label>Possui investimentos?</label>
          <br />
          <Switch
            onChange={(checked) => this.onChangeItem(checked, "TemCarteira")}
            checked={this.state.item.TemCarteira}
          />
        </Col>,
        <Col lg="12" xl="12">
          <i style={{ fontSize: ".9rem" }}>
            Obs.: Se o usuário que está sendo cadastrado possuir investimentos
            no seu nome, marcar "Possui investimentos".
          </i>
        </Col>,
      ];
    } else {
      return <></>;
    }
  };

  mostrarTipoDePessoaOuDadosDaPessoa() {
    if (this.state.item.tipoDePessoa !== null) {
      return (
        <div className="FadeIn">
          <Row>
            <Col lg="4" xl="4">
              <div>
                <label>Nome *</label>
                <Input
                  onChange={(e) => this.onChangeItem(e.target.value, "Nome")}
                  type="text"
                />
              </div>
            </Col>
            <Col lg="4" xl="4">
              <div>
                <label>Sobrenome *</label>
                <Input
                  onChange={(e) =>
                    this.onChangeItem(e.target.value, "Sobrenome")
                  }
                  type="text"
                />
              </div>
            </Col>
            <Col lg="4" xl="4">
              <div>
                <label>CPF *</label>
                <MaskedInput
                  mask={this.props.masks["cpf"]}
                  name="documento"
                  onChange={(e) =>
                    this.onChangeItem(e.target.value, "Documento")
                  }
                  className="form-control"
                />
              </div>
            </Col>
            <Col lg="4" xl="4">
              <div>
                <label>Estado Civil *</label>
                <Select
                  className="select-component"
                  options={this.state.estadoCivilCombo}
                  onChange={(e) => this.onChangeItem(e.value, "EstadoCivil")}
                  value={
                    this.state.item.EstadoCivil == null
                      ? this.state.estadoCivilCombo[0]
                      : this.state.estadoCivilCombo[
                          this.state.item.EstadoCivil + 1
                        ]
                  }
                />
              </div>
            </Col>
            <Col lg="4" xl="4">
              <div>
                <label>Data de nascimento *</label>
                <MaskedInput
                  mask={this.props.masks["data"]}
                  onChange={(e) =>
                    this.onChangeItem(e.target.value, "DataNascimento")
                  }
                  name="dataNascimento"
                  className="form-control"
                />
              </div>
            </Col>
            <Col lg="4" xl="4">
              <div>
                <label>Celular *</label>
                <MaskedInput
                  mask={this.props.masks["celular"]}
                  name="Celular"
                  onChange={(e) => this.onChangeItem(e.target.value, "Celular")}
                  className="form-control"
                />
              </div>
            </Col>
            <Col lg="4" xl="4">
              <div>
                <label>Telefone</label>
                <MaskedInput
                  name="telefone"
                  mask={this.props.masks["telefone"]}
                  onChange={(e) =>
                    this.onChangeItem(e.target.value, "Telefone")
                  }
                  className="form-control"
                />
              </div>
            </Col>
            <Col lg="4" xl="4">
              <div>
                <label>E-mail *</label>
                <Input
                  type="email"
                  onChange={(e) =>
                    this.onChangeUsuarioDto(e.target.value, "Email")
                  }
                />
              </div>
            </Col>
            <Col lg="4" xl="4">
              <div>
                <label>Senha *</label>
                <Input
                  type="password"
                  onChange={(e) => {
                    this.onChangeUsuarioDtoSenha(e.target.value);
                  }}
                />
              </div>
            </Col>
            {this.mostrarCampoPossuiInvestimentos()}
            <Col lg="12" xl="12">
              <div
                style={{
                  width: "100%",
                  paddingTop: "15px",
                  textAlign: "center",
                }}
              >
                <button
                  className="featured-button"
                  onClick={this.props.onCancel}
                  style={{
                    marginRight: "5px",
                    backgroundImage:
                      "url(" +
                      require("../../../../assets/img/theme/botao-destaque.png") +
                      ")",
                  }}
                >
                  Cancelar
                </button>
                <button
                  className="featured-button"
                  onClick={() => {
                    this.beforeContinue();
                  }}
                  style={{
                    backgroundImage:
                      "url(" +
                      require("../../../../assets/img/theme/botao-destaque.png") +
                      ")",
                  }}
                >
                  Pular Passo
                </button>
                <button
                  id="salvar-lancamento"
                  className="featured-button"
                  onClick={() => {
                    //Salva pessoa e muda quantidade de pessoas/adultos se der certo
                    this.salvarPessoa();
                  }}
                  style={{
                    marginLeft: "5px",
                    backgroundImage:
                      "url(" +
                      require("../../../../assets/img/theme/botao-destaque.png") +
                      ")",
                  }}
                >
                  Salvar
                </button>
              </div>
            </Col>
          </Row>
        </div>
      );
    } else {
      return (
        <div className="Holder FadeIn">
          <div className="Center">
            <div className="Option" onClick={() => this.onSelect(1)}>
              <i class="fas fa-male"></i>
              <p>Adulto</p>
            </div>
            <div className="Option" onClick={() => this.onSelect(2)}>
              <i class="fas fa-child"></i>
              <p>Jovem/Criança</p>
            </div>
          </div>
          <div className="Center" style={{ bottom: "-50px" }}>
            <button
              className="featured-button"
              onClick={() => {
                this.beforeContinue();
              }}
              style={{
                backgroundImage:
                  "url(" +
                  require("../../../../assets/img/theme/botao-destaque.png") +
                  ")",
              }}
            >
              Pular Passo
            </button>
          </div>
        </div>
      );
    }
  }

  salvarOrcamento = (nome, tipoOrcamento, modelo, callback) => {
    var item = this.orcamentoVazio;

    item["Nome"] = nome;
    item["TipoOrcamento"] = tipoOrcamento;
    item["OrcamentoModelo"] = modelo;

    item["FamiliaId"] = this.state.familiaId;
    item["EmpresaId"] = this.state.empresaId;

    if (this.state.planejadorId != null)
      item["PlanejadorId"] = this.state.planejadorId;

    var currentScroll = [
      document.documentElement.scrollLeft || document.body.scrollLeft,
      document.documentElement.scrollTop || document.body.scrollTop,
    ];

    global.spinnerShow($);

    let $this = this;

    this.orcamentoService.buscarOrcamentoPorFamiliaTipoNome(
      this.state.accessToken,
      this.state.familiaId,
      nome,
      tipoOrcamento,
      function (data) {
        if (data.success === true) {
          if (data.results == null || data.results.length === 0) {
            $this.orcamentoService.salvarOrcamento(
              $this.state.accessToken,
              item,
              function (data2) {
                //Sucesso, cadastrou agora
                global.spinnerHide($, currentScroll);

                if (data2.success) {
                  if (callback) {
                    callback(data2);
                  }
                } else {
                  console.log("Erro ao salvar orçamento", data.exception);

                  const options = {
                    title: "Erro ao salvar orçamento",
                    message: data2.exception.Message,
                    buttons: [
                      {
                        label: "Ok",
                      },
                    ],
                  };
                  confirmAlert(options);
                }
              }
            );
          }
          //Caso de ucesso, mas já cadastrado o orçamento
          else {
            console.log("Orçamento [" + nome + "] já existe, passando adiante");

            global.spinnerHide($, currentScroll);

            if (callback) {
              callback(data);
            }
          }
        }
        //Caso de erro na consulta
        else {
          global.spinnerHide($, currentScroll);

          console.log("Erro ao consultar orçamento", data.exception);

          const options = {
            title: "Erro ao consultar existência do orçamento",
            message: data.exception.Message,
            buttons: [
              {
                label: "Ok",
              },
            ],
          };
          confirmAlert(options);
        }
      }
    );
  };

  render() {
    return (
      <div>
        <SkyLight
          ref={(ref) => (this.modalPessoas = ref)}
          title={
            <>
              <div className="pop-up-title">
                <h2 id="ajuda-titulo">Usuários já Cadastrados</h2>
              </div>
            </>
          }
        >
          {this.state.pessoasVisivel === true ? (
            <Pessoas match={{ params: false }} onSteps={true} />
          ) : (
            <></>
          )}
        </SkyLight>
        <SkyLight
          ref={(ref) => (this.modal = ref)}
          title={
            <>
              <div className="pop-up-title">
                <h2 id="ajuda-titulo">Cadastrar Outro Usuário?</h2>
              </div>
            </>
          }
        >
          <p>
            Deseja cadastrar outro usuário? Você também poderá cadastrar outro
            mais tarde.
          </p>
          <button
            id="salvar-lancamento"
            className="featured-button"
            onClick={() => {
              this.onSelect(null);
              this.modal.hide();
            }}
            style={{
              marginRight: "5px",
              backgroundImage:
                "url(" +
                require("../../../../assets/img/theme/botao-destaque.png") +
                ")",
            }}
          >
            Novo Usuário
          </button>
          <button
            id="salvar-lancamento"
            className="featured-button"
            onClick={() => {
              this.beforeContinue();
            }}
            style={{
              marginRight: "5px",
              backgroundImage:
                "url(" +
                require("../../../../assets/img/theme/botao-destaque.png") +
                ")",
            }}
          >
            Próximo Passo
          </button>
        </SkyLight>
        <h4
          style={{
            paddingBottom: "1.5rem",
            fontSize: "1.1rem",
            fontWeight: "100",
            textAlign: "center",
          }}
        >
          <font style={{ fontWeight: "900" }}>Por favor,</font> preencha os
          dados do novo membro da família abaixo ou{" "}
          <font
            style={{ color: "#2dce89", fontWeight: "900", cursor: "pointer" }}
            onClick={() => global.showModal(this.modalPessoas)}
          >
            clique aqui
          </font>{" "}
          para ver todos já cadastrados.
        </h4>
        {this.mostrarTipoDePessoaOuDadosDaPessoa()}
      </div>
    );
  }
}

export default PassoCadastro;
