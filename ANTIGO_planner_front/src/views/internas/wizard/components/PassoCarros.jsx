import React from "react";
import Select from "react-select";
import $ from "jquery";
import axios from "axios";
import { confirmAlert } from "react-confirm-alert";
import "./steps/Steps.css";

import OrcamentoService from "../../../../services/OrcamentoService";
import PessoaService from "../../../../services/PessoaService";

import { Row, Col, NavLink, NavItem, Input } from "reactstrap";

class PassoCarros extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      accessToken: localStorage.getItem("access-token"),
      empresaId: localStorage.getItem("empresa-id"),
      familiaId: localStorage.getItem("familia-id"),
      planejadorId: localStorage.getItem("planejador-id"),
      tipoUsuario: localStorage.getItem("tipo-usuario"),
      numberOfCars: null,
      members: 0,
      singleFirstName: "",
      cars: [],
    };

    this.pessoaService = new PessoaService();
    this.orcamentoService = new OrcamentoService();
  }

  componentDidMount() {
    var currentScroll = [
      document.documentElement.scrollLeft || document.body.scrollLeft,
      document.documentElement.scrollTop || document.body.scrollTop,
    ];

    global.spinnerShow($);

    let $this = this;

    //Busca pessoas, grava se for o unico
    this.pessoaService.buscarPessoasPorFamilia(
      this.state.accessToken,
      this.state.familiaId,
      {},
      function (data) {
        global.spinnerHide($, currentScroll);

        if (data.results != null && data.results.length === 1) {
          var singleFirstName = data.results[0].nome;
          $this.setState({
            members: data.results.length,
            singleFirstName: singleFirstName,
          });
        }
      }
    );
  }

  fabricarQuantidadeCarros() {
    var array = [];

    for (var i = 0; i < 6; i++) {
      var fun = function () {
        if (this.i > 0) {
          this.$this.setState({ numberOfCars: this.i });
        } else {
          console.log("fabricarQuantidadeCarros i = 0");
          this.$this.saveDefault();
        }
      }.bind({ i: i, $this: this });
      array.push(
        <NavItem
          className="general-button dark"
          style={{ float: "left", marginLeft: "5px" }}
        >
          <NavLink
            className="nav-link-icon aux-button"
            to="/admin/gestao-planos-sonhos"
            onClick={fun}
          >
            <span className="nav-link-inner--text inner">{i}</span>
          </NavLink>
        </NavItem>
      );
    }
    return array;
  }

  onChangeInput = (e) => {
    var i = e.currentTarget.id.split("-")[2];
    var value = e.target.value;
    this.setState((state) => {
      state.cars[i] = value;
    });
  };

  onContinue = (e) => {
    this.recursiveRequestCarRegister(this.state.cars, 0);
  };

  saveDefault = () => {
    let $this = this;

    global.spinnerShow($);

    var currentScroll = [
      document.documentElement.scrollLeft || document.body.scrollLeft,
      document.documentElement.scrollTop || document.body.scrollTop,
    ];

    this.orcamentoService.buscarOrcamentoPorFamiliaFiltro(
      this.state.accessToken,
      this.state.familiaId,
      {},
      function (data) {
        if (data.results !== null && data.results.length > 0) {
          var orcamentoFamilia = data.results.find((value) => {
            return (
              value.nome === "Família" ||
              ($this.state.members === 1 &&
                value.nome === $this.state.singleFirstName)
            );
          });

          if (orcamentoFamilia != null) {
            //var urlCategoria = global.server_api + 'api/categoria/familia/' + $this.state.familiaId + "/filtro";
            var urlCategoria =
              global.server_api_new +
              global.apiToken +
              "/categoria/familia/" +
              $this.state.familiaId +
              "/filtro";
            var config1 = {
              headers: { Authorization: "bearer " + $this.state.accessToken },
            };
            axios
              .post(urlCategoria, { IncluirOrcamentos: true }, config1)
              .then((res) => {
                if (res.data.results !== null && res.data.results.length > 0) {
                  var categoriaTransporte = res.data.results.find((value) => {
                    return value.nome === "Transporte";
                  });

                  if (categoriaTransporte != null) {
                    global.spinnerHide($, currentScroll);
                    $this.props.onContinue();
                  } else {
                    $this.salvarCategoriaTransporte(
                      "Transporte",
                      orcamentoFamilia.id /** Orcamento Família */,
                      {
                        success: () => {
                          global.spinnerHide($, currentScroll);
                          $this.props.onContinue();
                        },
                        error: (res) => {
                          global.spinnerHide($, currentScroll);
                          const options = {
                            title: "Erro ao Salvar Registro",
                            message: res.data.exception.Message,
                            buttons: [
                              {
                                label: "Ok",
                              },
                            ],
                          };

                          confirmAlert(options);
                        },
                      }
                    );
                  }
                } else {
                  global.spinnerHide($, currentScroll);
                }
              });
          } else {
            global.spinnerHide($, currentScroll);
            $this.props.onContinue();
          }
        } else {
          global.spinnerHide($, currentScroll);
          $this.props.onContinue();
        }
      }
    );
  };

  recursiveRequestCarRegister = (cars, i) => {
    //Primeiro carro da lista
    if (i === 0 && cars.length > 0) {
      global.spinnerShow($);
    } else if (i === 0 && cars.length === 0) {
      this.saveDefault();
    }

    //Recursao terminou
    if (cars.length === i) {
      var currentScroll = [
        document.documentElement.scrollLeft || document.body.scrollLeft,
        document.documentElement.scrollTop || document.body.scrollTop,
      ];
      global.spinnerHide($, currentScroll);
      this.props.onContinue();
      return;
    }

    var car = cars[i];

    let $this = this;

    this.orcamentoService.salvarOrcamento(
      this.state.accessToken,
      {
        Nome: car,
        TipoOrcamento: 1,
        FamiliaId: parseInt(this.state.familiaId),
        EmpresaId: parseInt(this.state.empresaId),
        PlanejadorId: this.state.planejadorId,
        Suspenso: 0,
        OrcamentoModelo: 4,
      },
      function (data) {
        if (data.success === true) {
          $this.recursiveRequestCarRegister(cars, ++i);
        } else {
          $this.recursiveRequestCarRegister(cars, ++i);
        }
      }
    );
  };

  salvarCategoriaTransporte(nome, orcamentoId, callback) {
    var item = {
      Nome: nome,
      Grupo: 4,
      Frequencia: 0,
      ValorEstimado: 0,
      OrcamentoId: orcamentoId,
      FamiliaId: parseInt(this.state.familiaId),
      EmpresaId: parseInt(this.state.empresaId),
      Suspenso: 0,
    };

    //var url = global.server_api + 'api/categoria/' + (this.state.id != null ? this.state.id : "");
    var url =
      global.server_api_new +
      global.apiToken +
      "/categoria/" +
      (this.state.id != null ? this.state.id : "");

    var config = {
      headers: {
        Authorization: "bearer " + this.state.accessToken,
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Headers": "Authorization",
        "Access-Control-Allow-Methods":
          "GET, POST, OPTIONS, PUT, PATCH, DELETE",
      },
    };

    axios.post(url, item, config).then((res) => {
      if (res.data.success === true) {
        callback.success();
      } else {
        callback.error(res);
      }
    });
  }

  /** Used in render */
  editarOrcamento() {
    var array = [];
    for (var i = 0; i < this.state.numberOfCars; i++) {
      array.push(
        <Col style={{ textAlign: "left" }} lg="4" xl="4">
          <label>Veículo {i + 1} *</label>
          <Input id={"id-car-" + i} onChange={this.onChangeInput} type="text" />
        </Col>
      );
    }

    return (
      <div>
        <Row style={{ marginBottom: "1rem" }}>{array}</Row>
      </div>
    );
  }

  /** Used in render */
  montarDropDown = (itens, id, changeMethod, selectedValue, type) => {
    return (
      <Select
        id={id}
        onChange={changeMethod}
        className="select-component"
        options={itens}
        defaultValue={itens != null ? itens[0] : ""}
        placeholder="Selecione..."
        value={
          itens != null && selectedValue != null
            ? itens.find((op) => {
                if (type === "enum") {
                  return (
                    (op.value != null ? op.value.toString() : null) ===
                    (selectedValue != null ? selectedValue.toString() : null)
                  );
                } else if (op.value != null && selectedValue != null) {
                  return op.value.id === selectedValue.id;
                }
                return false;
              })
            : null
        }
      />
    );
  };

  render() {
    return (
      <div className="FadeIn" style={{ textAlign: "center" }}>
        <div style={{ width: "100%", textAlign: "center" }}>
          <h4
            style={{
              paddingBottom: "1.5rem",
              fontSize: "1.1rem",
              fontWeight: "100",
            }}
          >
            <font style={{ fontWeight: "900" }}>Quantos veículos</font> a
            família possui?
          </h4>
          <div
            className={this.state.numberOfCars != null ? "FadeOut" : ""}
            style={{ display: "inline-block" }}
          >
            {this.fabricarQuantidadeCarros()}
          </div>
          <div
            className={
              this.state.numberOfCars != null && this.state.numberOfCars > 0
                ? "FadeIn"
                : "Invisible"
            }
          >
            {this.editarOrcamento()}
          </div>
        </div>
        <div style={{ marginTop: ".25rem" }}>
          <button
            className="featured-button"
            onClick={this.props.onCancel}
            style={{
              backgroundImage:
                "url(" +
                require("../../../../assets/img/theme/botao-destaque.png") +
                ")",
            }}
          >
            Cancelar
          </button>
          <button
            className="featured-button"
            onClick={this.props.onContinue}
            style={{
              backgroundImage:
                "url(" +
                require("../../../../assets/img/theme/botao-destaque.png") +
                ")",
              marginLeft: ".25rem",
            }}
          >
            Pular Passo
          </button>
          <button
            className={
              "featured-button " +
              (this.state.numberOfCars != null && this.state.numberOfCars > 0
                ? "FadeIn"
                : "Invisible")
            }
            onClick={this.onContinue}
            style={{
              backgroundImage:
                "url(" +
                require("../../../../assets/img/theme/botao-destaque.png") +
                ")",
              marginLeft: ".25rem",
            }}
          >
            Salvar
          </button>
        </div>
      </div>
    );
  }
}

export default PassoCarros;
