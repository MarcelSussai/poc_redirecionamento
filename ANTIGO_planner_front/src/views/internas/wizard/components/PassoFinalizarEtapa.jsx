import React from "react";

class PassoFinalizarEtapa extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      numberOfCars: null,
    };
  }

  render() {
    return (
      <div className="FadeIn" style={{ textAlign: "Center" }}>
        <i style={{ fontSize: "6rem" }} className="far fa-thumbs-up"></i>
        <h4
          style={{
            margin: "1.35rem",
            paddingBottom: "1.5rem",
            fontSize: "1.2rem",
            fontWeight: "100",
          }}
        >
          <font style={{ fontWeight: "900" }}>Legal!!</font> Construímos uma
          estrutura padrão para o seu orçamento. A qualquer momento você poderá
          editá-la.
        </h4>
        <button
          id="salvar-lancamento"
          className="featured-button"
          onClick={this.props.onCancel}
          style={{
            marginRight: "5px",
            backgroundImage:
              "url(" +
              require("../../../../assets/img/theme/botao-destaque.png") +
              ")",
          }}
        >
          Cancelar
        </button>
        <button
          id="salvar-lancamento"
          className="featured-button"
          onClick={this.props.onContinue}
          style={{
            backgroundImage:
              "url(" +
              require("../../../../assets/img/theme/botao-destaque.png") +
              ")",
          }}
        >
          Continuar
        </button>
      </div>
    );
  }
}

// "far fa-thumbs-up"  Legal!! Construímos uma estrutura padrão para o seu orçamento. A qualquer momento você poderá editá-la.

export default PassoFinalizarEtapa;
