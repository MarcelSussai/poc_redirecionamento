import React from "react";
import { Redirect, Link } from "react-router-dom";
import { CircularProgress, Typography } from "@material-ui/core";
import MUIDataTable from "mui-datatables";
import axios from "axios";
import $ from "jquery";
import SkyLight from "react-skylight";
import { confirmAlert } from "react-confirm-alert";
import MaskedInput from "react-maskedinput";
import Select from "react-select";
import Switch from "react-switch";
import { createMuiTheme, MuiThemeProvider } from "@material-ui/core/styles";
import Button from '../../components/Buttons'
import GoBackButton from '../../components/Buttons/GoBackButton'

import { Col, Row, Input, Card, CardHeader, NavLink } from "reactstrap";

import OrcamentoService from "../../services/OrcamentoService";

class Pessoas extends React.Component {
  orcamentoPadrao = {
    adultoCasado: 1,
    adultoSolteiro: 14,
    filhos: 5,
    familia: 6,
    receita: 2,
  };

  tipoOrcamento = {
    receita: 0,
    despesa: 1,
  };

  constructor(props) {
    super(props);

    this.state = {
      accessToken: localStorage.getItem("access-token"),
      empresaId: localStorage.getItem("empresa-id"),
      planejadorId: localStorage.getItem("planejador-id"),
      familiaId: localStorage.getItem("familia-id"),
      tipoUsuario: parseInt(localStorage.getItem("tipo-usuario")),
      page: 0,
      count: 1,
      numeroItensPorPagina: 15,
      data: [["Carregando Dados..."]],
      isLoading: false,
      pais: "Brasil",
      familiasCombo: [],
      estadoCivilCombo: [],
      direcaoCampo: {},
      usuarioCadastrado: "",
    };

    this.orcamentoVazio = {
      Nome: null,
      TipoOrcamento: null,
      FamiliaId: parseInt(localStorage.getItem("familia-id")),
      EmpresaId: parseInt(localStorage.getItem("empresa-id")),
      PlanejadorId: parseInt(localStorage.getItem("planejador-id")),
      Suspenso: 0,
      OrcamentoModelo: null,
    };

    this.orcamentoService = new OrcamentoService();
  }

  mascaras = {
    telefone: "(11) 1111-1111",
    celular: "(11) 1 1111-1111",
    cpf: "111.111.111-11",
    cnpj: "11.111.111/1111-11",
    data: "11/11/1111",
  };

  getMuiTheme = () =>
    createMuiTheme({
      overrides: {
        MuiTableRow: {
          root: {
            "&:nth-of-type(odd)": {
              backgroundColor: "#f8f9fe",
            },
          },
        },
        MUIDataTableSelectCell: {
          fixedHeaderCommon: {
            backgroundColor: "unset",
          },
        },
        MuiTableCell: {
          root: {
            fontFamily: "unset",
          },
        },
        MUIDataTableHeadCell: {
          fixedHeaderYAxis: {
            fontWeight: 700,
          },
        },
        MuiPaper: {
          root: {
            overflow: "hidden",
            margin: 0,
          },
        },
        MuiToolbar: {
          regular: {
            backgroundColor: "#e4eaf4",
          },
        },
        MUIDataTablePagination: {
          root: {
            "&:last-child": {
              margin: "0px 24px 0px 24px",
              padding: 0,
            },
          },
        },
      },
    });

  componentWillReceiveProps(nextProps) {
    if (nextProps.reload === true) {
      this.getData();
    }
  }

  componentDidMount() {
    const { open } = this.props.match.params;

    var estadoCivil = [{ value: null, label: "Selecione..." }];

    global.estadoCivil.forEach(function (value, index) {
      estadoCivil.push({
        value: index,
        label: value,
      });
    });

    this.setState({
      estadoCivilCombo: estadoCivil,
      labelFamilia: this.state.tipoUsuario === 2 ? "família" : "cliente",
      labelFamiliaMaiusculo:
        this.state.tipoUsuario === 2 ? "Família" : "Cliente",
      mostraColunaFamilia: this.state.tipoUsuario === 2 ? false : true,
    });

    this.getData(open);
    this.atualizarFamilias();

    global.mostrarAjudaDaPaginaAutomaticamente(this);
  }

  setRedirect = () => {
    this.setState({
      redirect: true,
    });
  };

  renderRedirect = () => {
    if (this.state.redirect) {
      return <Redirect to="/auth/login" />;
    }
  };

  getData = (open) => {
    if (this.state.interval != null) {
      clearInterval(this.state.interval);
    }

    if (this.state.accessToken == null) {
      this.setRedirect();
      return;
    }

    this.setState({ isLoading: true });

    //Admins
    //var url = global.server_api + 'api/Pessoa/filtro/';
    var url = global.server_api_new + global.apiToken + "/Pessoa/filtro/";

    //Planners
    if (this.state.planejadorId != null)
      //url = global.server_api + 'api/Pessoa/planejador/' + this.state.planejadorId + "/filtro/";
      url =
        global.server_api_new +
        global.apiToken +
        "/Pessoa/planejador/" +
        this.state.planejadorId +
        "/filtro/";

    //Pessoa da familia
    if (this.state.familiaId != null)
      //url = global.server_api + 'api/Pessoa/familia/' + this.state.familiaId + "/filtro/";
      url =
        global.server_api_new +
        global.apiToken +
        "/Pessoa/familia/" +
        this.state.familiaId +
        "/filtro/";

    var filtro = {
      Filtro: this.state.textoDigitado,
      Ordenacao: this.state.sortField != null ? this.state.sortField : "Nome",
      NumeroPagina:
        this.state.numeroPagina != null ? this.state.numeroPagina : 1,
      NumeroItensPorPagina:
        this.state.numeroItensPorPagina != null
          ? this.state.numeroItensPorPagina
          : 10,
      Documento: this.state.documentoFiltro,
      EstadoCivil: this.state.estadoCivilFiltro,
    };

    var config = {
      headers: {
        Authorization: "bearer " + this.state.accessToken,
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Headers": "Authorization",
        "Access-Control-Allow-Methods":
          "GET, POST, OPTIONS, PUT, PATCH, DELETE",
      },
    };

    var currentScroll = [
      document.documentElement.scrollLeft || document.body.scrollLeft,
      document.documentElement.scrollTop || document.body.scrollTop,
    ];

    global.spinnerShow($);

    axios.post(url, filtro, config).then((res) => {
      var data = [];

      if (res.data.results != null) {
        res.data.results.forEach(function (item) {
          var temp = item;

          temp.estadoCivilLabel = global.estadoCivil[item.estadoCivil];
          temp.tipo = 2;
          temp.nomeFamilia = item.familia != null ? item.familia.nome : "";
          temp.familiaId = item.familia != null ? item.familia.id : "";
          temp.estado = global.estados[item.estado];
          temp.temCarteira = item.temCarteira === 1;
          temp.email = item.usuario != null ? item.usuario.email : "";

          data.push(temp);
        });
      }

      const total = res.data.totalResults;
      this.setState(
        { data: data, isLoading: false, count: total },
        function () {
          global.spinnerHide($, currentScroll);
          if (open != null) {
            global.showModal(this.modalEditar);
          }
        }
      );
    });
  };

  getEmpresaFamilia = () => {
    var config = {
      headers: {
        Authorization: "bearer " + this.state.accessToken,
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Headers": "Authorization",
        "Access-Control-Allow-Methods":
          "GET, POST, OPTIONS, PUT, PATCH, DELETE",
      },
    };

    if (this.state.familiaId2 != null) {
      //var url = global.server_api + 'api/familia/' + this.state.familiaId2 + '/buscaidempresa';
      var url =
        global.server_api_new +
        global.apiToken +
        "/familia/" +
        this.state.familiaId2 +
        "/buscaidempresa";
      var $this = this;

      try {
        axios.get(url, config).then((res) => {
          var idEmpresa = res.data.singleResult;

          if (idEmpresa != null) {
            localStorage.setItem("empresa-id", idEmpresa);
            $this.setState({ empresaId: idEmpresa });
          }
        });
      } catch (ex) {
        console.log(ex);
      }
    }
  };

  atualizarFamilias = () => {
    //var url = global.server_api + 'api/familia/filtro';
    var url = global.server_api_new + global.apiToken + "/familia/filtro";

    if (this.state.planejadorId != null) {
      //url = global.server_api + 'api/familia/planejador/filtro/' + this.state.planejadorId;
      url =
        global.server_api_new +
        global.apiToken +
        "/familia/planejador/filtro/" +
        this.state.planejadorId;
    }

    var config = {
      headers: {
        Authorization: "bearer " + this.state.accessToken,
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Headers": "Authorization",
        "Access-Control-Allow-Methods":
          "GET, POST, OPTIONS, PUT, PATCH, DELETE",
      },
    };

    axios.post(url, { ApenasNome: true }, config).then((res) => {
      var familias = res.data.results;
      var familiasCombo = [];

      if (familias != null) {
        familias = familias.sort((item1, item2) => {
          const name1 = item1.nome.toUpperCase();
          const name2 = item2.nome.toUpperCase();

          let comparison = 0;

          if (name1 > name2) {
            comparison = 1;
          } else if (name1 < name2) {
            comparison = -1;
          }

          return comparison;
        });

        familias.forEach((mp) => {
          familiasCombo.push({
            value: mp,
            label: mp.nome,
          });
        });
      }

      this.setState({ familias });
      this.setState({ familiasCombo });
    });
  };

  filtroPorTexto = (valorDigitado) => {
    const textoDigitado = valorDigitado;

    this.setState({ textoDigitado }, function () {
      if (this.state.interval != null) {
        clearInterval(this.state.interval);
      }

      const $this = this;

      const interval = setInterval(function () {
        $this.getData();
      }, 1000);

      this.setState({ interval });
    });
  };

  editarRegistro = (rowData) => {
    var id = rowData[0];

    if (id == null) return;

    var items = this.state.data.filter(function (temp) {
      return temp.id === id;
    });

    if (items != null && items.length > 0) {
      var item = items[0];
      var moment = require("moment");
      var temp = null;
      var testDate = moment(item.dataNascimento);
      if (item.dataNascimento != null && testDate.isValid()) {
        temp = moment(item.dataNascimento).format("DD/MM/YYYY");
        temp = temp.replace(/\//g, "");
      }
      //Seta item edicao
      this.setState(
        {
          id: item.id,
          nome: item.nome,
          sobrenome: item.sobrenome,
          dataNascimento: temp,
          documento: item.documento,
          estadoCivil: item.estadoCivil,
          celular: item.celular,
          telefone: item.telefone,
          cep: item.cep,
          temCarteira: item.temCarteira,
          familiaId2: item.familiaId,
          email: item.usuario != null ? item.usuario.email : "",
          idContaFinanceira: item.idContaFinanceira,
          tipo: 2,
          editando: true,
        },
        function () {
          this.setState(
            {
              endereco: item.endereco,
              complemento: item.complemento,
              pais: item.pais != null ? item.pais : "Brasil",
              cidade: item.cidade,
              estado: item.estado,
            },
            function () {
              //Abre modal
              global.showModal(this.modalEditar);
            }
          );
        }
      );
    }
  };

  salvar = () => {
    var currentScroll = [
      document.documentElement.scrollLeft || document.body.scrollLeft,
      document.documentElement.scrollTop || document.body.scrollTop,
    ];

    global.spinnerShow($);

    var usuarioDto = {
      Senha: this.state.senha != null ? this.state.senha : "",
      NovaSenha: this.state.senha != null ? this.state.senha : "",
      Email: this.state.email,
      Tipo: 2, //0 = Admin , 1 = Planejador, 2 = Pessoa
    };

    var dataNascimento = this.state.dataNascimento;

    if (dataNascimento != null && dataNascimento.indexOf("/") === -1) {
      dataNascimento =
        dataNascimento.substr(0, 2) +
        "/" +
        dataNascimento.substr(2, 2) +
        "/" +
        dataNascimento.substr(4, 4);
    }

    var item = {
      Id: this.state.id,
      Nome: this.state.nome,
      Sobrenome: this.state.sobrenome,
      Documento: this.state.documento,
      EstadoCivil: this.state.estadoCivil,
      Endereco: this.state.endereco,
      Complemento: this.state.complemento,
      DataNascimento: dataNascimento,
      Pais: this.state.pais,
      Cidade: this.state.cidade,
      Estado: this.state.estado,
      CEP: this.state.cep,
      Celular: this.state.celular,
      Telefone: this.state.telefone,
      UsuarioDTO: usuarioDto,
      FamiliaId:
        this.state.familiaId != null
          ? this.state.familiaId
          : this.state.familiaId2,
      EmpresaId: this.state.empresaId,
      PlanejadorId: this.state.planejadorId,
      TemCarteira: this.state.temCarteira === true ? 1 : 0,
      IdContaFinanceira: this.state.idContaFinanceira,
    };

    //var url = global.server_api + 'api/pessoa/' + ((this.state.id != null) ? this.state.id : "");
    var url =
      global.server_api_new +
      global.apiToken +
      "/pessoa/" +
      (this.state.id != null ? this.state.id : "");

    var config = {
      headers: {
        Authorization: "bearer " + this.state.accessToken,
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Headers": "Authorization",
        "Access-Control-Allow-Methods":
          "GET, POST, OPTIONS, PUT, PATCH, DELETE",
      },
    };
    console.log(JSON.stringify(item));
    axios
      .post(url, item, config)
      .then((res) => {
        global.spinnerHide($, currentScroll);

        if (res.data.success === true) {
          this.limparSelecao();
          window.location.reload();
        } else {
          console.log(res.data.exception);
          const options = {
            title: "Erro ao salvar usuário",
            message: res.data.exception.Message,
            buttons: [
              {
                label: "Ok",
              },
            ],
          };

          confirmAlert(options);
        }
      })
      .catch((error) => {
        global.spinnerHide($, currentScroll);
        global.logarErroDeRequisicao(error);
      });
  };

  criarNovoUsuario = () => {
    var currentScroll = [
      document.documentElement.scrollLeft || document.body.scrollLeft,
      document.documentElement.scrollTop || document.body.scrollTop,
    ];

    global.spinnerShow($);

    var usuarioDto = {
      Senha: this.state.senha != null ? this.state.senha : "",
      NovaSenha: this.state.senha != null ? this.state.senha : "",
      Email: this.state.email,
      Tipo: 2, //0 = Admin , 1 = Planejador, 2 = Pessoa
    };

    var dataNascimento = this.state.dataNascimento;

    if (dataNascimento != null && dataNascimento.indexOf("/") === -1) {
      dataNascimento =
        dataNascimento.substr(0, 2) +
        "/" +
        dataNascimento.substr(2, 2) +
        "/" +
        dataNascimento.substr(4, 4);
    }

    var item = {
      Id: this.state.id,
      Nome: this.state.nome,
      Sobrenome: this.state.sobrenome,
      Documento: this.state.documento,
      EstadoCivil: this.state.estadoCivil,
      Endereco: this.state.endereco,
      Complemento: this.state.complemento,
      DataNascimento: dataNascimento,
      Pais: this.state.pais,
      Cidade: this.state.cidade,
      Estado: this.state.estado,
      CEP: this.state.cep,
      Celular: this.state.celular,
      Telefone: this.state.telefone,
      UsuarioDTO: usuarioDto,
      FamiliaId:
        this.state.familiaId != null
          ? this.state.familiaId
          : this.state.familiaId2,
      EmpresaId: this.state.empresaId,
      PlanejadorId: this.state.planejadorId,
      TemCarteira: this.state.temCarteira === true ? 1 : 0,
      IdContaFinanceira: this.state.idContaFinanceira,
    };

    //var url = global.server_api + 'api/pessoa/' + ((this.state.id != null) ? this.state.id : "");
    var url =
      global.server_api_new +
      global.apiToken +
      "/pessoa/" +
      (this.state.id != null ? this.state.id : "");

    var config = {
      headers: {
        Authorization: "bearer " + this.state.accessToken,
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Headers": "Authorization",
        "Access-Control-Allow-Methods":
          "GET, POST, OPTIONS, PUT, PATCH, DELETE",
      },
    };
    //console.log(item);
    axios
      .post(url, item, config)
      .then((res) => {
        global.spinnerHide($, currentScroll);

        if (res.data.success === true) {
          this.setState({ usuarioCadastrado: item.Nome });
          //this.limparSelecao();
          this.modalNovo.hide();
          global.showModal(this.modalOrcamento);
        } else {
          console.log(res.data.exception);
          const options = {
            title: "Erro ao salvar usuário",
            message: res.data.exception.Message,
            buttons: [
              {
                label: "Ok",
              },
            ],
          };

          confirmAlert(options);
        }
      })
      .catch((error) => {
        global.spinnerHide($, currentScroll);
        global.logarErroDeRequisicao(error);
      });
  };

  limparSelecao = () => {
    this.setState({
      id: null,
      nome: "",
      sobrenome: "",
      documento: "",
      estadoCivil: null,
      endereco: "",
      complemento: "",
      cidade: "",
      estado: "",
      cep: "",
      editando: null,
      celular: "",
      telefone: "",
      nomeUsuario: "",
      senha: "",
      email: "",
      dataNascimento: "",
      temCarteira: false,
      tipo: null,
      familiaId2: null,
    });
  };

  removerRegistros = (rowsDeleted) => {
    const options = {
      title: "Exclusão de Usuários",
      message:
        "Tem certeza que deseja excluir? Este procedimento é irreversível!",
      buttons: [
        {
          label: "Sim",
          onClick: () => {
            var ids = [];

            for (var key in rowsDeleted.data) {
              ids.push(this.state.data[rowsDeleted.data[key].dataIndex].id);
            }

            if (ids.length > 0) {
              //var url = global.server_api + 'api/pessoa/bulk/' + ids.join(",");
              var url =
                global.server_api_new +
                global.apiToken +
                "/pessoa/bulk/" +
                ids.join(",");

              var config = {
                headers: {
                  Authorization: "bearer " + this.state.accessToken,
                  "Access-Control-Allow-Origin": "*",
                  "Access-Control-Allow-Headers": "Authorization",
                  "Access-Control-Allow-Methods":
                    "GET, POST, OPTIONS, PUT, PATCH, DELETE",
                },
              };

              axios.delete(url, {}, config).then((res) => {
                if (res.data.success === true) {
                  this.getData();
                } else {
                  console.log(res.data.exception);
                  alert(
                    "Erro ao remover usuários. " + res.data.exception.Message
                  );
                }
              });
            }
          },
        },
        {
          label: "Não",
        },
      ],
    };

    confirmAlert(options);

    return false;
  };

  mudarFamilia = (selectedOption) => {
    const familia = selectedOption.value;
    this.setState({ familiaId2: familia.id }, function () {
      this.getEmpresaFamilia();
    });
  };

  _onChangeSelection = (selectedOption) => {
    var valor = selectedOption.value;
    this.setState({ estadoCivil: valor });
  };

  _onChange = (e) => {
    var nomeCampo = e.target.name;

    if (e.target.value.indexOf("_") > -1) {
      return;
    }

    this.setState({ [nomeCampo]: e.target.value });

    if (nomeCampo === "cep") {
      var cep = e.target.value;
      cep = cep.replace(".", "").replace("-", "");

      var cepUrl = "https://viacep.com.br/ws/" + cep + "/json";

      axios.get(cepUrl).then((res) => {
        if (nomeCampo === "cep") {
          this.atualizarDadosEndereco(res.data);
        }
      });
    }
  };

  atualizarDadosEndereco = (json) => {
    if (json != null) {
      this.setState({
        endereco: json.logradouro,
        cidade: json.localidade,
        estado: json.uf,
      });
    }
  };

  montarDropDown = (itens, id, changeMethod, selectedValue, type) => (
    <Select
      id={id}
      onChange={changeMethod}
      className="select-component"
      options={itens}
      defaultValue={itens != null ? itens[0] : ""}
      placeholder="Selecione..."
      value={
        itens != null && selectedValue != null
          ? itens.find((op) => {
              if (type === "enum") {
                return (
                  (op.value != null ? op.value.toString() : null) ===
                  (selectedValue != null ? selectedValue.toString() : null)
                );
              } else if (
                type === "entidade" &&
                op.value != null &&
                selectedValue != null
              ) {
                return op.value.id === selectedValue.id;
              } else {
                return (
                  (selectedValue != null ? parseInt(selectedValue) : null) ===
                  (op.value != null && op.value.id
                    ? parseInt(op.value.id)
                    : null)
                );
              }
            })
          : null
      }
    />
  );

  mostrarComboFamilia = () => {
    return (
      <Col lg="4" xl="4">
        <div>
          <label>Cliente *</label>
          {this.montarDropDown(
            this.state.familiasCombo,
            "select-orcamento",
            this.mudarFamilia,
            this.state.familiaId != null
              ? this.state.familiaId
              : this.state.familiaId2,
            "outro"
          )}
        </div>
      </Col>
    );
  };

  mostrarPopup = () => (
    <div>
      <SkyLight
        ref={(ref) => (this.modalEditar = ref)}
        transitionDuration={0}
        beforeOpen={this.preCarregarParaEdicao}
        afterClose={this.limparSelecao}
        title={
          <Row>
            <div className="pop-up-title">
              <i className="ni ni-check-bold" style={{ fontWeight: 700 }}></i>{" "}
              <h2>Editar informações do usuário</h2>
            </div>
          </Row>
        }
        dialogStyles={{ borderRadius: "1rem", padding: "2rem" }}
      >
        <Row>
          {this.state.familiaId == null && this.mostrarComboFamilia()}
          <Col lg="4" xl="4">
            <div>
              <label style={{ fontWeight: 600 }}>Nome *</label>
              <Input
                type="text"
                value={this.state.nome}
                onChange={(e) => this.setState({ nome: e.target.value })}
                style={{ paddingLeft: 24 }}
              />
            </div>
          </Col>
          <Col lg="4" xl="4">
            <div>
              <label style={{ fontWeight: 600 }}>Sobrenome *</label>
              <Input
                type="text"
                value={this.state.sobrenome}
                onChange={(e) => this.setState({ sobrenome: e.target.value })}
                style={{ paddingLeft: 24 }}
              />
            </div>
          </Col>
          <Col lg="4" xl="4">
            <div>
              <label style={{ fontWeight: 600 }}>CPF *</label>
              <MaskedInput
                name="documento"
                className="form-control"
                mask={this.mascaras.cpf}
                value={this.state.documento}
                onChange={this._onChange}
                style={{ paddingLeft: 24 }}
              />
            </div>
          </Col>
          <Col lg="4" xl="4">
            <div>
              <label style={{ fontWeight: 600 }}>Estado Civil *</label>
              <Select
                className="select-component"
                value={this.state.estadoCivilCombo.find((item) => {
                  return item.value === this.state.estadoCivil;
                })}
                onChange={this._onChangeSelection}
                options={this.state.estadoCivilCombo}
              />
            </div>
          </Col>
          <Col lg="4" xl="4">
            <div>
              <label style={{ fontWeight: 600 }}>Data de nascimento *</label>
              <MaskedInput
                name="dataNascimento"
                className="form-control"
                mask={this.mascaras.data}
                value={this.state.dataNascimento}
                onChange={this._onChange}
                style={{ paddingLeft: 24 }}
              />
            </div>
          </Col>
          <Col lg="4" xl="4">
            <div>
              <label style={{ fontWeight: 600 }}>Celular *</label>
              <MaskedInput
                name="celular"
                className="form-control"
                mask={this.mascaras.celular}
                value={this.state.celular}
                onChange={this._onChange}
                style={{ paddingLeft: 24 }}
              />
            </div>
          </Col>
          <Col lg="4" xl="4">
            <div>
              <label style={{ fontWeight: 600 }}>Telefone</label>
              <MaskedInput
                name="telefone"
                className="form-control"
                mask={this.mascaras.telefone}
                value={this.state.telefone}
                onChange={this._onChange}
                style={{ paddingLeft: 24 }}
              />
            </div>
          </Col>
          <Col lg="4" xl="4">
            <div>
              <label style={{ fontWeight: 600 }}>E-mail *</label>
              <Input
                type="email"
                value={this.state.email}
                onChange={(e) => this.setState({ email: e.target.value })}
                autocomplete="new-password"
                style={{ paddingLeft: 24 }}
              />
            </div>
          </Col>
          <Col lg="4" xl="4">
            <div>
              <label style={{ fontWeight: 600 }}>
                Senha {this.state.editando === true ? <></> : <>*</>}
              </label>
              <Input
                type="password"
                value={this.state.senha}
                onChange={(e) => this.setState({ senha: e.target.value })}
                autocomplete="new-password"
                style={{ paddingLeft: 24 }}
              />
            </div>
          </Col>
          {(() => {
            if (this.state.tipoUsuario === 1 || this.state.tipoUsuario === 0) {
              return (
                <div>
                  <Col lg="4" xl="4">
                    <div>
                      <label style={{ fontWeight: 600 }}>
                        Possui investimentos?
                      </label>
                      <Switch
                        onChange={(checked) => {
                          this.setState({ temCarteira: checked });
                        }}
                        checked={this.state.temCarteira}
                      />
                    </div>
                  </Col>
                  <Col lg="12" xl="12">
                    <i style={{ padding: "5px", fontSize: ".9rem" }}>
                      Obs.: Se o usuário possuir investimentos no seu nome,
                      marcar "Possui investimentos".
                    </i>
                  </Col>
                </div>
              );
            } else {
              return <></>;
            }
          })()}
          <Col
            lg="12"
            xl="12"
            style={{ display: "flex", justifyContent: "center" }}
          >
            <button
              className="featured-button"
              onClick={() => this.salvar()}
              style={{
                backgroundImage:
                  "url(" +
                  require("../../assets/img/theme/botao-destaque.png") +
                  ")",
                backgroundColor: "transparent",
                minWidth: 200,
                fontWeight: 600,
              }}
            >
              SALVAR
            </button>
          </Col>
        </Row>
      </SkyLight>
      <SkyLight
        ref={(ref) => (this.modalNovo = ref)}
        transitionDuration={0}
        //afterClose={this.limparSelecao}
        title={
          <Row>
            <div className="pop-up-title">
              <i className="ni ni-check-bold" style={{ fontWeight: 700 }}></i>{" "}
              <h2>Criar usuário</h2>
            </div>
          </Row>
        }
        dialogStyles={{ borderRadius: "1rem", padding: "2rem" }}
      >
        <Row>
          {this.state.familiaId == null && this.mostrarComboFamilia()}
          <Col lg="4" xl="4">
            <div>
              <label style={{ fontWeight: 600 }}>Nome *</label>
              <Input
                type="text"
                value={this.state.nome}
                onChange={(e) => this.setState({ nome: e.target.value })}
                style={{ paddingLeft: 24 }}
              />
            </div>
          </Col>
          <Col lg="4" xl="4">
            <div>
              <label style={{ fontWeight: 600 }}>Sobrenome *</label>
              <Input
                type="text"
                value={this.state.sobrenome}
                onChange={(e) => this.setState({ sobrenome: e.target.value })}
                style={{ paddingLeft: 24 }}
              />
            </div>
          </Col>
          <Col lg="4" xl="4">
            <div>
              <label style={{ fontWeight: 600 }}>CPF *</label>
              <MaskedInput
                name="documento"
                className="form-control"
                mask={this.mascaras.cpf}
                value={this.state.documento}
                onChange={this._onChange}
                style={{ paddingLeft: 24 }}
              />
            </div>
          </Col>
          <Col lg="4" xl="4">
            <div>
              <label style={{ fontWeight: 600 }}>Estado Civil *</label>
              <Select
                className="select-component"
                value={this.state.estadoCivilCombo.find((item) => {
                  return item.value === this.state.estadoCivil;
                })}
                onChange={this._onChangeSelection}
                options={this.state.estadoCivilCombo}
              />
            </div>
          </Col>
          <Col lg="4" xl="4">
            <div>
              <label style={{ fontWeight: 600 }}>Data de nascimento *</label>
              <MaskedInput
                name="dataNascimento"
                className="form-control"
                mask={this.mascaras.data}
                value={this.state.dataNascimento}
                onChange={this._onChange}
                style={{ paddingLeft: 24 }}
              />
            </div>
          </Col>
          <Col lg="4" xl="4">
            <div>
              <label style={{ fontWeight: 600 }}>Celular *</label>
              <MaskedInput
                name="celular"
                className="form-control"
                mask={this.mascaras.celular}
                value={this.state.celular}
                onChange={this._onChange}
                style={{ paddingLeft: 24 }}
              />
            </div>
          </Col>
          <Col lg="4" xl="4">
            <div>
              <label style={{ fontWeight: 600 }}>Telefone</label>
              <MaskedInput
                name="telefone"
                className="form-control"
                mask={this.mascaras.telefone}
                value={this.state.telefone}
                onChange={this._onChange}
                style={{ paddingLeft: 24 }}
              />
            </div>
          </Col>
          <Col lg="4" xl="4">
            <div>
              <label style={{ fontWeight: 600 }}>E-mail *</label>
              <Input
                type="email"
                value={this.state.email}
                onChange={(e) => this.setState({ email: e.target.value })}
                autocomplete="new-password"
                style={{ paddingLeft: 24 }}
              />
            </div>
          </Col>
          <Col lg="4" xl="4">
            <div>
              <label style={{ fontWeight: 600 }}>
                Senha {this.state.editando === true ? <></> : <>*</>}
              </label>
              <Input
                type="password"
                value={this.state.senha}
                onChange={(e) => this.setState({ senha: e.target.value })}
                autocomplete="new-password"
                style={{ paddingLeft: 24 }}
              />
            </div>
          </Col>
          {(() => {
            if (this.state.tipoUsuario === 0 || this.state.tipoUsuario === 1) {
              return (
                <div>
                  <Col lg="4" xl="4">
                    <div>
                      <label style={{ fontWeight: 600 }}>
                        Possui investimentos?
                      </label>
                      <Switch
                        onChange={(checked) => {
                          this.setState({ temCarteira: checked });
                        }}
                        checked={this.state.temCarteira}
                      />
                    </div>
                  </Col>
                  <Col lg="12" xl="12">
                    <i style={{ padding: "5px", fontSize: ".9rem" }}>
                      Obs.: Se o usuário que está sendo cadastrado possuir
                      investimentos no seu nome, marcar "Possui investimentos".
                    </i>
                  </Col>
                </div>
              );
            } else {
              return <></>;
            }
          })()}
          <Col
            lg="12"
            xl="12"
            style={{ display: "flex", justifyContent: "center" }}
          >
            <button
              className="featured-button"
              onClick={() => this.criarNovoUsuario()}
              style={{
                backgroundImage:
                  "url(" +
                  require("../../assets/img/theme/botao-destaque.png") +
                  ")",
                backgroundColor: "transparent",
                minWidth: 200,
                fontWeight: 600,
              }}
            >
              SALVAR
            </button>
          </Col>
        </Row>
      </SkyLight>
    </div>
  );

  mostrarPopupOrcamento = () => (
    <div>
      <SkyLight
        ref={(ref) => (this.modalOrcamento = ref)}
        transitionDuration={0}
        // beforeOpen={this.preCarregarParaEdicao}
        //afterClose={this.limparSelecao}
        title={
          <Row style={{ paddingBottom: 0 }}>
            <Col lg="12" xl="12">
              <div className="pop-up-title">
                <h2>Usuário criado com sucesso!</h2>
              </div>
            </Col>
          </Row>
        }
        dialogStyles={{ borderRadius: "1rem", padding: "2rem" }}
      >
        <Row style={{ paddingTop: 0 }}>
          {this.state.familiaId == null &&
            this.state.familiaId2 == null &&
            this.mostrarComboFamilia()}
          <Col lg="12" xl="12">
            <div>
              <p>
                Deseja criar orçamentos de receita e despesa para este usuário?
              </p>
            </div>
          </Col>

          <Col
            lg="12"
            xl="12"
            style={{ display: "flex", justifyContent: "center" }}
          >
            <button
              style={{ border: "none", background: "none" }}
              onClick={() => {
                this.modalOrcamento.hide();
                window.location.reload();
              }}
            >
              <NavLink className="nav-link-icon aux-button">
                <span className="nav-link-inner--text inner">
                  Não, criar depois
                </span>
              </NavLink>
            </button>
            <button
              id="salvar-lancamento"
              className="featured-button"
              onClick={() => {
                this.modalOrcamento.hide();
                global.showModal(this.modalTipoUsuario);
              }}
              style={{
                backgroundImage:
                  "url(" +
                  require("../../assets/img/theme/botao-destaque.png") +
                  ")",
                backgroundColor: "transparent",
                backgroundSize: "cover",
              }}
            >
              Sim, quero criar agora
            </button>
          </Col>
        </Row>
      </SkyLight>
      <SkyLight
        ref={(ref) => (this.modalTipoUsuario = ref)}
        transitionDuration={0}
        // beforeOpen={this.preCarregarParaEdicao}
        // afterClose={this.limparSelecao}
        title={
          <Row style={{ paddingBottom: 0 }}>
            <Col lg="12" xl="12">
              <div className="pop-up-title">
                <h2>Qual o perfil deste usuário?</h2>
              </div>
            </Col>
          </Row>
        }
        dialogStyles={{ borderRadius: "1rem", padding: "2rem" }}
      >
        <Row style={{ paddingTop: 0 }}>
          {this.state.familiaId == null &&
            this.state.familiaId2 == null &&
            this.mostrarComboFamilia()}
          <Col
            lg="12"
            xl="12"
            style={{ display: "flex", justifyContent: "center" }}
          >
            <button
              style={{ border: "none", background: "none" }}
              onClick={() => {
                this.modalTipoUsuario.hide();
                this.criarOrcamento(1);
              }}
            >
              <NavLink className="nav-link-icon aux-button" to="/">
                <span className="nav-link-inner--text inner">
                  Adulto solteiro
                </span>
              </NavLink>
            </button>
            <button
              style={{ border: "none", background: "none" }}
              onClick={() => {
                this.modalTipoUsuario.hide();
                this.criarOrcamento(14);
              }}
            >
              <NavLink className="nav-link-icon aux-button" to="/">
                <span className="nav-link-inner--text inner">
                  Adulto casado
                </span>
              </NavLink>
            </button>
            <button
              style={{ border: "none", background: "none" }}
              onClick={() => {
                this.modalTipoUsuario.hide();
                this.criarOrcamento(5);
              }}
            >
              <NavLink className="nav-link-icon aux-button" to="/">
                <span className="nav-link-inner--text inner">
                  Jovem ou criança
                </span>
              </NavLink>
            </button>
          </Col>
        </Row>
      </SkyLight>
      <SkyLight
        ref={(ref) => (this.modalVerOrcamentos = ref)}
        transitionDuration={0}
        title={
          <Row style={{ paddingBottom: 0 }}>
            <Col lg="12" xl="12">
              <div className="pop-up-title">
                <h2>
                  Pronto! Criamos uma estrutura padrão para o seu orçamento.{" "}
                  <br />
                  Deseja editar as estimativas de despesas e receitas?
                </h2>
              </div>
            </Col>
          </Row>
        }
        dialogStyles={{ borderRadius: "1rem", padding: "2rem" }}
      >
        <Row style={{ paddingTop: 0 }}>
          {this.state.familiaId == null &&
            this.state.familiaId2 == null &&
            this.mostrarComboFamilia()}

          <Col
            lg="12"
            xl="12"
            style={{ display: "flex", justifyContent: "center" }}
          >
            <button
              style={{ border: "none", background: "none" }}
              onClick={() => {
                this.modalVerOrcamentos.hide();
                window.location.reload();
              }}
            >
              <NavLink className="nav-link-icon aux-button" to="/">
                <span className="nav-link-inner--text inner">
                  Não, editar depois
                </span>
              </NavLink>
            </button>
            <Link
              to="/admin/orcamento"
              className="featured-button"
              style={{
                backgroundImage:
                  "url(" +
                  require("../../assets/img/theme/botao-destaque.png") +
                  ")",
                backgroundColor: "transparent",
                backgroundSize: "cover",
              }}
            >
              Sim, quero editar agora
            </Link>
          </Col>
        </Row>
      </SkyLight>
    </div>
  );

  criarOrcamento = (orcamentoPadrao) => {
    let $this = this;
    var nomeOrcamento = this.state.usuarioCadastrado;
    //console.log(nomeOrcamento)

    this.salvarOrcamento(
      nomeOrcamento,
      this.tipoOrcamento.despesa,
      orcamentoPadrao,
      function (data) {
        if (data.success === true) {
          //Nome do orçamento de receita
          nomeOrcamento = "Receitas " + nomeOrcamento;

          $this.salvarOrcamento(
            nomeOrcamento,
            $this.tipoOrcamento.receita,
            $this.orcamentoPadrao.receita,
            function (data2) {
              if (data2.success === false) {
                console.log(data.exception);
                const options = {
                  title: "Erro ao salvar orçamento de receita",
                  message: data.exception.Message,
                  buttons: [
                    {
                      label: "Ok",
                    },
                  ],
                };

                confirmAlert(options);
                return;
              }

              global.showModal($this.modalVerOrcamentos);
            }
          );
        } else {
          console.log(data.exception);
          const options = {
            title: "Erro ao salvar orçamento de despesa",
            message: data.exception.Message,
            buttons: [
              {
                label: "Ok",
              },
            ],
          };

          confirmAlert(options);
        }
      }
    );
  };

  salvarOrcamento = (nome, tipoOrcamento, modelo, callback) => {
    var item = this.orcamentoVazio;

    item["Nome"] = nome;
    item["TipoOrcamento"] = tipoOrcamento;
    item["OrcamentoModelo"] = modelo;

    item["FamiliaId"] =
      this.state.familiaId2 != null
        ? this.state.familiaId2
        : this.state.familiaId;
    item["EmpresaId"] = this.state.empresaId;

    if (this.state.planejadorId != null)
      item["PlanejadorId"] = this.state.planejadorId;

    var currentScroll = [
      document.documentElement.scrollLeft || document.body.scrollLeft,
      document.documentElement.scrollTop || document.body.scrollTop,
    ];

    global.spinnerShow($);

    let $this = this;

    this.orcamentoService.buscarOrcamentoPorFamiliaTipoNome(
      this.state.accessToken,
      this.state.familiaId,
      nome,
      tipoOrcamento,
      function (data) {
        if (data.success === true) {
          if (data.results == null || data.results.length === 0) {
            $this.orcamentoService.salvarOrcamento(
              $this.state.accessToken,
              item,
              function (data2) {
                //Sucesso, cadastrou agora
                global.spinnerHide($, currentScroll);

                if (data2.success) {
                  if (callback) {
                    callback(data2);
                  }
                } else {
                  console.log("Erro ao salvar orçamento", data.exception);

                  const options = {
                    title: "Erro ao salvar orçamento",
                    message: data2.exception.Message,
                    buttons: [
                      {
                        label: "Ok",
                      },
                    ],
                  };
                  confirmAlert(options);
                }
              }
            );
          }
          //Caso de ucesso, mas já cadastrado o orçamento
          else {
            console.log("Orçamento [" + nome + "] já existe, passando adiante");

            global.spinnerHide($, currentScroll);

            if (callback) {
              callback(data);
            }
          }
        }
        //Caso de erro na consulta
        else {
          global.spinnerHide($, currentScroll);

          console.log("Erro ao consultar orçamento", data.exception);

          const options = {
            title: "Erro ao consultar existência do orçamento",
            message: data.exception.Message,
            buttons: [
              {
                label: "Ok",
              },
            ],
          };
          confirmAlert(options);
        }
      }
    );
  };

  render() {
    const columns = [
      {
        name: "id",
        options: {
          display: false,
          viewColumns: false,
          filter: false,
        },
      },
      {
        name: "nomeFamilia",
        label: this.state.labelFamiliaMaiusculo,
        options: {
          display: this.state.mostraColunaFamilia,
          viewColumns: this.state.mostraColunaFamilia,
          filter: false,
        },
      },
      {
        name: "nome",
        label: "Nome",
        options: {
          display: true,
          viewColumns: true,
          filter: false,
        },
      },
      {
        name: "sobrenome",
        label: "Sobrenome",
        options: {
          display: true,
          viewColumns: true,
          filter: false,
        },
      },
      {
        name: "documento",
        label: "CPF",
        options: {
          display: true,
          viewColumns: true,
          filter: true,
        },
      },
      {
        name: "estadoCivilLabel",
        label: "Estado Civil",
        options: {
          display: true,
          viewColumns: true,
          filter: true,
        },
      },
      {
        name: "email",
        label: "E-mail",
        options: {
          display: true,
          viewColumns: true,
          filter: true,
        },
      },
      {
        name: "celular",
        label: "Celular",
        options: {
          display: true,
          viewColumns: true,
          filter: true,
        },
      },
      {
        name: "telefone",
        label: "Telefone",
        options: {
          display: false,
          viewColumns: true,
          filter: true,
        },
      },
    ];
    columns.forEach((item) => {
      if (item.name === this.state.colunaOrdenacao) {
        item.options.sortDirection = this.state.direcaoOrdenacao;
      }
    });

    const { data, page, count, isLoading, numeroItensPorPagina } = this.state;

    const options = {
      textLabels: global.textLabels,
      filter: true,
      filterType: "dropdown",
      responsive: "stacked",
      print: false,
      download: false,
      serverSide: true,
      searchText: this.state.textoDigitado,
      count: count,
      page: page,
      sort: true,
      selectableRows: "multiple",
      //selectableRowsOnClick: true,
      rowsPerPage: numeroItensPorPagina,
      onRowsDelete: (rowsDeleted) => {
        this.removerRegistros(rowsDeleted);
        return false;
      },
      onRowClick: (rowData, rowMeta) => {
        this.editarRegistro(rowData);
      },
      onFilterChange: (changedColumn, filterList) => {
        switch (changedColumn) {
          case "documento":
            const documentoFiltro =
              filterList[4][0] == null ? filterList[4] : filterList[4][0];

            this.setState({ documentoFiltro }, () => {
              this.getData();
            });
            break;
          case "estadoCivilLabel":
            var array2 = this.state.estadoCivilCombo.filter(function (item) {
              return (
                item.label === filterList[5] || item.label === filterList[5][0]
              );
            });

            const estadoCivilFiltro =
              array2 != null && array2.length === 1 ? array2[0].value : null;

            this.setState({ estadoCivilFiltro }, () => {
              this.getData();
            });
            break;
          default:
            this.setState(
              {
                documentoFiltro: null,
                estadoCivilFiltro: null,
              },
              function () {
                this.getData();
              }
            );
            break;
        }
      },
      onColumnSortChange: (changedColumn, direction) => {
        //ASC
        let order = "";
        let direcaoOrdenacao = "asc";

        if (this.state.colunaOrdenacao === changedColumn) {
          if (
            this.state.direcaoOrdenacao != null &&
            this.state.direcaoOrdenacao === "asc"
          ) {
            order = " Desc";
            direcaoOrdenacao = "desc";
          }
        }

        this.setState(
          {
            sortField: changedColumn + order,
            colunaOrdenacao: changedColumn,
            direcaoOrdenacao: direcaoOrdenacao,
            clickedColumn: changedColumn,
          },
          () => {
            this.getData();
          }
        );
      },
      onTableChange: (action, tableState) => {
        switch (action) {
          case "changePage":
            this.setState({ numeroPagina: tableState.page + 1 }, () => {
              this.getData();
            });
            break;
          case "search":
            this.filtroPorTexto(tableState.searchText);
            break;
          default:
            break;
        }
      },
    };

    return (
      <div style={{ padding: "0 5%", marginTop: 20 }}>
        <Row style={{ paddingLeft: 0 }}>
          <Col lg="12" xl="12">
            <Card style={{ border: "none" }}>
              <CardHeader className="border-0">
                <Row className="align-items-center" style={{ paddingLeft: 0 }}>
                  <div className="col">
                    <img
                      src={require("../../assets/img/theme/icone-calendario.png")}
                      alt="Meios de Pagamento"
                      className="before-title-img"
                    />
                    <h3 className="mb-0 chart-title">Usuários</h3>
                  </div>
                  {
                    //Só mostra o botão se nao tem onSteps
                    !this.props.onSteps ? (
                      <>
                        <GoBackButton />
                        <Button text="Novo Usuário" onClick={() => global.showModal(this.modalNovo)} />
                      </>
                    ) : (<></>)
                  }
                </Row>
              </CardHeader>
            </Card>
          </Col>
        </Row>
          <MuiThemeProvider theme={this.getMuiTheme()}>
            <MUIDataTable
              id="orcamento-table"
              title={
                <Typography>
                  {isLoading && (
                    <CircularProgress
                      size={24}
                      style={{ marginLeft: 15, position: "relative", top: 4 }}
                    />
                  )}
                </Typography>
              }
              data={data}
              columns={columns}
              options={options}
            />
          </MuiThemeProvider>
          {this.mostrarPopup()}
          {this.mostrarPopupOrcamento()}
          {this.renderRedirect()}
      </div>
    );
  }
}

export default Pessoas;
