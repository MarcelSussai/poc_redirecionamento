import * as React from "react";
import { Redirect } from "react-router-dom";
import FormControl from "@material-ui/core/FormControl";
import Input from "@material-ui/core/Input";
import Radio from "@material-ui/core/Radio";
import RadioGroup from "@material-ui/core/RadioGroup";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import FormLabel from "@material-ui/core/FormLabel";
import Select from "react-select";
import CurrencyInput from "react-currency-input";
import FormGroup from "@material-ui/core/FormGroup";
import Checkbox from "@material-ui/core/Checkbox";
import { Col, Row, Card, CardHeader, Button } from "reactstrap";
import ApexChart from "react-apexcharts";
import SkyLight from "react-skylight";
import axios from "axios";
import $ from "jquery";
import { confirmAlert } from "react-confirm-alert";
import Tabela from "./Components/Tabela";
import Tooltip from "@material-ui/core/Tooltip";
import { withStyles } from "@material-ui/core/styles";
import Link from "@material-ui/core/Link";
import ModalSonho from "./Components/ModalSonho";
import { getYear } from "date-fns";
import "react-datepicker/dist/react-datepicker.css";
import "./estilo.css";
import "../../../assets/css/register.css";
import "../../../assets/css/aposentadoria.css";

class Aposentadoria extends React.Component {
  state = {
    grPie_series: [75, 25],
    grPie_options: {
      chart: {
        width: 250,
        type: "pie",
      },
      legend: {
        position: "top",
      },
      colors: ["#0097a7", "#FFD600"],
      labels: ["Aporte Financeiro", "Juros"],
      responsive: [
        {
          breakpoint: 480,
          options: {
            chart: {
              width: 180,
            },
            legend: {
              position: "top",
            },
          },
        },
      ],
    },
    grCurvaMenor_options: {
      chart: {
        type: "area",
        height: 500,
      },
      dataLabels: {
        enabled: false,
      },
      stroke: {
        curve: "straight",
      },
      title: {
        text: "Aportes / Resgates",
        align: "left",
        style: {
          fontSize: "14px",
        },
      },
      xaxis: {
        type: "number",
        axisBorder: {
          show: false,
        },
        axisTicks: {
          show: false,
        },
        title: {
          text: "Idade",
        },
      },
      yaxis: {
        title: {
          text: "Aportes / Resgates",
        },
        tickAmount: 1,
        floating: false,

        labels: {
          offsetY: -7,
          offsetX: 0,
        },
        axisBorder: {
          show: false,
        },
        axisTicks: {
          show: false,
        },
        labels: {
          style: {
            colors: "#8e8da4",
          },
          offsetX: 0,
          formatter: function (val) {
            try {
              var numero = val.toFixed(2).split(".");
              numero[0] = "R$ " + numero[0].split(/(?=(?:...)*$)/).join(".");
              return numero.join(",");
            } catch {
              try {
                return val.toFixed(2);
              } catch {
                return val;
              }
            }
          },
        },
      },
      fill: {
        opacity: 0.5,
      },
      tooltip: {
        y: {
          format: "xxx.xxx.xxx,xx",
        },
        fixed: {
          enabled: false,
          position: "topRight",
        },
      },
      grid: {
        yaxis: {
          lines: {
            offsetX: -30,
          },
        },
        padding: {
          left: 20,
        },
      },
    },
    accessToken: localStorage.getItem("access-token"),
    empresaId: localStorage.getItem("empresa-id"),
    planejadorId: localStorage.getItem("planejador-id"),
    familiaId: localStorage.getItem("familia-id"),
    tipoUsuario: parseInt(localStorage.getItem("tipo-usuario")),
    pessoaId: 0,
    sonhoAposentadoria: null,
    patrimonioDestinado: 0,
    aposentadoriaSelecionada: -1,
    sonhoSelec_descricao: "",
    rentAcuAnualDefault: 6,
    rentUsfAnualDefault: 5,
    InflacaoAnualDefault: 3,
    rentAnualAcumulacao: 6,
    rentMensalAcumulacao: "",
    rentAnualUsofruto: 5,
    rentMensalUsofruto: "",
    taxaInflacao: 3,
    taxaInflacaoMensal: "",
    atualizarRentabInflacao: true,
    rowsTable: [],
    aporteExtra: [],
    selectOp: [{ value: -99, label: "Cadastrar Plano de Aposentadoria" }],
    txtIdadeFutura: null,
    txtIdadeInicial: null,
    txtIdadeUsofruto: null,
    txtGastosAtuais: null,
    txtGastosCorrigidos: null,
    txtPatrimonioDestinado: null,
    txtParcelaAposent: null,
    IsProcessed: false,
    Validado: false,
    valorFuturoState: 0,
    idAposDadoExtra: null,
    aporteResgate_Ano: null,
    aporteResgate_Mes: null,
    aporteResgate_Valor: null,
    aporteResgate_QtdeRepet: null,
    aporteResgate_EhAporte: "true",
    valorMetaPlano: 0,
  };
  constructor(props) {
    super(props);
  }
  selectAporteExtra = [
    { value: 1, label: "Jan" },
    { value: 2, label: "Fev" },
    { value: 3, label: "Mar" },
    { value: 4, label: "Abr" },
    { value: 5, label: "Mai" },
    { value: 6, label: "Jun" },
    { value: 7, label: "Jul" },
    { value: 8, label: "Ago" },
    { value: 9, label: "Set" },
    { value: 10, label: "Out" },
    { value: 11, label: "Nov" },
    { value: 12, label: "Dez" },
  ];

  componentDidMount() {
    if (this.state.accessToken === null) {
      this.setRedirect();
      return;
    }
    global.mostrarAjudaDaPaginaAutomaticamente(this);
    this.carregaDadosSelect();
    console.log(this.state.rentMensalUsofruto);
    if (this.state.rentMensalUsofruto === "") {
      this.setState({
        rentMensalAcumulacao: this.desinflarTaxa(5),
        rentMensalUsofruto: this.desinflarTaxa(4),
        taxaInflacaoMensal: this.desinflarTaxa(4.5),
      });
    }
  }

  ShowModalSonho = (show) => {
    this.setState({
      showModalSonho: show,
    });
  };

  setRedirect = () => {
    this.setState({
      redirect: true,
    });
  };

  renderRedirect = () => {
    if (this.state.redirect) {
      return <Redirect to="/auth/login" />;
    }
  };

  obterPatrimonioNuvem = () => {
    if (
      this.state.aposentadoriaSelecionada === -1 ||
      this.state.idAposDadoExtra == null
    ) {
      const options = {
        title: "Informação",
        message:
          "Por favor, você precisa salvar o plano antes de usar essa opção.",
        buttons: [
          {
            label: "Ok",
          },
        ],
      };
      confirmAlert(options);
    } else {
      var url =
        global.server_api_new +
        global.apiToken +
        "/aposentadoria/carregar/" +
        this.state.aposentadoriaSelecionada;
      var config = {
        headers: {
          Authorization: "bearer " + this.state.accessToken,
          "Access-Control-Allow-Origin": "*",
          "Access-Control-Allow-Headers": "Authorization",
          "Access-Control-Allow-Methods":
            "GET, POST, OPTIONS, PUT, PATCH, DELETE",
        },
      };
      axios.get(url, config).then((res) => {
        if (res.data.success === true || res.data.dadosCarga != null) {
          console.log("handleChange", res.data);
          if (res.data.dadosCarga != null) {
            this.setState({
              txtPatrimonioDestinado:
                res.data.destinacaoInicial == null ||
                res.data.destinacaoInicial == 0
                  ? "R$ 0.00"
                  : res.data.destinacaoInicial.toLocaleString("pt-BR", {
                      style: "currency",
                      currency: "BRL",
                    }),
            });
            //alert(this.state.txtParcelaAposent);
            //this.recalculo();
            this.ObterDadosAPIDetalhado();
          }
        }
      });
    }
  };

  obterGastoNuvem = () => {
    var url =
      global.server_api_new +
      global.apiToken +
      "/aposentadoria/gastomensal/" +
      this.state.familiaId;
    var config = {
      headers: {
        Authorization: "bearer " + this.state.accessToken,
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Headers": "Authorization",
        "Access-Control-Allow-Methods":
          "GET, POST, OPTIONS, PUT, PATCH, DELETE",
      },
    };
    axios.get(url, config).then((res) => {
      if (res.data.success === true) {
        console.log("obterGastoNuvem", res.data);
        this.setState({
          txtGastosAtuais: res.data.gastosAtuais,
        });
        this.ObterDadosAPIDetalhado();
      }
    });
  };

  handleChange = (selectedOption) => {
    console.log(selectedOption);
    const campoCalculado = selectedOption.value;
    this.setState({
      aposentadoriaSelecionada: campoCalculado,
      sonhoSelec_descricao: selectedOption.label,
    });
    if (selectedOption.value === -99) {
      this.ShowModalSonho(true);
      //this.props.history.push("/admin/planos-sonhos");
    } else {
      var url =
        global.server_api_new +
        global.apiToken +
        "/aposentadoria/carregar/" +
        campoCalculado;
      var config = {
        headers: {
          Authorization: "bearer " + this.state.accessToken,
          "Access-Control-Allow-Origin": "*",
          "Access-Control-Allow-Headers": "Authorization",
          "Access-Control-Allow-Methods":
            "GET, POST, OPTIONS, PUT, PATCH, DELETE",
        },
      };
      axios.get(url, config).then((res) => {
        if (res.data.success === true || res.data.dadosCarga != null) {
          console.log("handleChange", res.data);
          if (res.data.dadosCarga != null) {
            this.setState({
              txtIdadeInicial: res.data.dadosCarga.idadeInicial,
              txtIdadeFutura: res.data.dadosCarga.idadeAposentadoria,
              txtIdadeUsofruto: res.data.dadosCarga.idadeMaxUsofruto,
              txtGastosAtuais: res.data.dadosCarga.gastosAtuais,
              txtGastosCorrigidos: this.calcularGastoCorrigido(
                res.data.dadosCarga.gastosAtuais,
                res.data.dadosCarga.idadeInicial,
                res.data.dadosCarga.idadeAposentadoria,
                res.data.dadosCarga.inflacaoAnual
              ),
              txtParcelaAposent: res.data.dadosCarga.investimentoAposentadoria,
              txtPatrimonioDestinado:
                res.data.dadosCarga.destinacaoInicial == null ||
                res.data.dadosCarga.destinacaoInicial == 0
                  ? "R$ 0.00"
                  : res.data.dadosCarga.destinacaoInicial.toLocaleString(
                      "pt-BR",
                      { style: "currency", currency: "BRL" }
                    ),
              rentAnualAcumulacao: res.data.dadosCarga.rentabilidadeAnualAcum,
              rentMensalAcumulacao: this.desinflarTaxa(
                res.data.dadosCarga.rentabilidadeAnualAcum
              ),
              rentAnualUsofruto: res.data.dadosCarga.rentabilidadeAnualUsufruto,
              rentMensalUsofruto: this.desinflarTaxa(
                res.data.dadosCarga.rentabilidadeAnualUsufruto
              ),
              taxaInflacao: res.data.dadosCarga.inflacaoAnual,
              taxaInflacaoMensal: this.desinflarTaxa(
                res.data.dadosCarga.inflacaoAnual
              ),
              atualizarRentabInflacao:
                res.data.dadosCarga.atualizarAporteAnualmenteInflacao,
              idAposDadoExtra: res.data.dadosCarga.id,
            });
            //alert(this.state.txtParcelaAposent);
            this.recalculo();
            this.ObterDadosAPIDetalhado();
          } else {
            this.setState({
              txtIdadeInicial: "",
              txtIdadeFutura: "",
              txtIdadeUsofruto: "",
              txtGastosAtuais: res.data.gastosAtuais,
              txtGastosCorrigidos: null,
              txtParcelaAposent: null,
              txtPatrimonioDestinado: res.data.destinacaoInicial,
              rentAnualAcumulacao: this.state.rentAcuAnualDefault,
              rentMensalAcumulacao: this.desinflarTaxa(
                this.state.rentAcuAnualDefault
              ),
              rentAnualUsofruto: this.state.rentUsfAnualDefault,
              rentMensalUsofruto: this.desinflarTaxa(
                this.state.rentUsfAnualDefault
              ),
              taxaInflacao: this.state.InflacaoAnualDefault,
              taxaInflacaoMensal: this.desinflarTaxa(
                this.state.InflacaoAnualDefault
              ),
              atualizarRentabInflacao: true,
              IsProcessed: false,
              idAposDadoExtra: null,
            });
          }
        } else {
          console.log(res.data);
          this.setState({
            txtIdadeInicial: "",
            txtIdadeFutura: "",
            txtIdadeUsofruto: "",
            txtGastosAtuais:
              res.data.gastosAtuais != null ? res.data.gastosAtuais : null,
            txtGastosCorrigidos: null,
            txtParcelaAposent: null,
            txtPatrimonioDestinado: res.data.destinacaoInicial,
            rentAnualAcumulacao: this.state.rentAcuAnualDefault,
            rentMensalAcumulacao: this.desinflarTaxa(
              this.state.rentAcuAnualDefault
            ),
            rentAnualUsofruto: this.state.rentUsfAnualDefault,
            rentMensalUsofruto: this.desinflarTaxa(
              this.state.rentUsfAnualDefault
            ),
            taxaInflacao: this.state.InflacaoAnualDefault,
            taxaInflacaoMensal: this.desinflarTaxa(
              this.state.InflacaoAnualDefault
            ),
            atualizarRentabInflacao: true,
            IsProcessed: false,
            idAposDadoExtra: null,
          });
        }
      });
    }
  };

  recalculo = () => {
    if (
      !(
        this.state.txtIdadeInicial === undefined ||
        this.state.txtIdadeInicial === "" ||
        this.state.txtIdadeFutura === undefined ||
        this.state.txtIdadeFutura === "" ||
        this.state.txtIdadeUsofruto === undefined ||
        this.state.txtIdadeUsofruto === "" ||
        this.state.txtGastosCorrigidos === undefined ||
        this.state.txtGastosCorrigidos === "" ||
        this.state.txtParcelaAposent === undefined ||
        this.state.txtParcelaAposent === ""
      )
    ) {
      //console.log(this.state.txtGastosAtuais, this.state.txtIdadeInicial, this.state.txtIdadeFutura, this.state.taxaInflacao);
      this.setState({
        txtGastosCorrigidos: this.calcularGastoCorrigido(
          this.state.gastosAtuais,
          this.state.txtIdadeInicial,
          this.state.txtIdadeFutura,
          this.state.taxaInflacao
        ),
      });
    } else {
      const options = {
        title: "Informação - Dados inconsistentes",
        message: "Erro ao calcular gastos corrigidos. Tente novamente.",
        buttons: [
          {
            label: "Ok",
          },
        ],
      };
      confirmAlert(options);
    }
  };

  calcularGastoCorrigido = (valor, inicio, limite, taxa) => {
    var valorInt = 0;
    console.log("E", valor);
    if (Number.isNaN(valor)) {
      valorInt = this.tratarMascara(this.state.txtGastosAtuais);
    } else if (typeof valor == "string") {
      valorInt = this.tratarMascara(valor);
    } else {
      valorInt = valor;
    }
    //console.log("valor A: ", valorInt);
    //console.log('tx0',valorInt, inicio, limite, taxa, Number.isNaN(valorInt));
    valorInt = parseFloat(valorInt);
    //console.log('tx1',valorInt, inicio, limite, taxa, Number.isNaN(valorInt));
    //console.log(typeof(inicio), inicio, typeof(limite),limite, limite == null || limite == undefined, limite == '' );

    for (let index = inicio; index <= limite + 1; index++) {
      valorInt = valorInt * (1 + taxa / 100);
    }
    console.log("valor Saida: ", valorInt);
    return valorInt.toFixed(0);
  };

  calcularParcelaIdeal = () => {
    if (
      this.state.txtIdadeInicial === undefined ||
      Number.isNaN(this.state.txtIdadeInicial) ||
      this.state.txtIdadeInicial === null ||
      this.state.txtIdadeInicial === "" ||
      this.state.txtIdadeFutura === undefined ||
      this.state.txtIdadeFutura === null ||
      this.state.txtIdadeFutura === "" ||
      Number.isNaN(this.state.txtIdadeFutura) ||
      this.state.txtIdadeUsofruto === undefined ||
      this.state.txtIdadeUsofruto === null ||
      this.state.txtIdadeUsofruto === "" ||
      Number.isNaN(this.state.txtIdadeUsofruto) ||
      this.state.txtGastosCorrigidos === undefined ||
      this.state.txtGastosCorrigidos === null ||
      this.state.txtGastosCorrigidos === "" ||
      Number.isNaN(this.state.txtGastosCorrigidos)
    ) {
      const options = {
        title: "Informação - Dados incompletos",
        message: "Por favor, preencha os dados ou carregue um plano.",
        buttons: [
          {
            label: "Ok",
          },
        ],
      };
      confirmAlert(options);
    }    
    else if (this.state.txtIdadeInicial > this.state.txtIdadeFutura 
      // || this.state.txtIdadeInicial == this.state.txtIdadeFutura
      ) {
      const options = {
        title: "Informação - Dados inconsistentes",
        message:
          "A idade de aposentadoria não pode ser inferior ou igual à idade inicial. Revise.",
        buttons: [
          {
            label: "Ok",
          },
        ],
      };
      confirmAlert(options);
    } else if (
      this.state.txtIdadeFutura > this.state.txtIdadeUsofruto ||
      this.state.txtIdadeFutura == this.state.txtIdadeUsofruto
    ) {
      const options = {
        title: "Informação - Dados inconsistentes",
        message:
          "A idade de aposentadoria não pode ser maior ou igual que a idade de usofruto. Revise.",
        buttons: [
          {
            label: "Ok",
          },
        ],
      };
      confirmAlert(options);
    } else {
      var url =
        global.server_api_new +
        global.apiToken +
        "/aposentadoria/calcularparcela";

      console.log(this.state.txtIdadeUsofruto);

      var dados = {
        GastosFutAtual: this.state.txtGastosCorrigidos,
        AnosUsof:
          parseInt(this.state.txtIdadeUsofruto) -
          parseInt(this.state.txtIdadeFutura) +
          1,
        AnosInvestimento:
          parseInt(this.state.txtIdadeFutura) -
          parseInt(this.state.txtIdadeInicial),
        DestinacaoInicial:
          this.state.txtPatrimonioDestinado === undefined ||
          this.state.txtPatrimonioDestinado == null ||
          this.state.txtPatrimonioDestinado == ""
            ? 0
            : this.tratarMascara(this.state.txtPatrimonioDestinado),
        TxRentabMes: parseFloat(this.state.rentMensalAcumulacao / 100),
        TxRentUsuf: parseFloat(this.state.rentMensalUsofruto / 100),
        TxInflacao: parseFloat(this.state.taxaInflacao / 100),
        AtualizAporte: this.state.atualizarRentabInflacao,
        IdAposentadoria: this.state.idAposDadoExtra,
        IdadeInicio: parseInt(this.state.txtIdadeInicial),
        IdadeAposentadoria: parseInt(this.state.txtIdadeFutura),
        IdadeUsofruto: parseInt(this.state.txtIdadeUsofruto),
      };
      console.log("dados calcularParcelaIdeal", dados);

      var config = {
        headers: {
          Authorization: "bearer " + this.state.accessToken,
          "Access-Control-Allow-Origin": "*",
          "Access-Control-Allow-Headers": "Authorization",
          "Access-Control-Allow-Methods":
            "GET, POST, OPTIONS, PUT, PATCH, DELETE",
        },
      };
      axios.post(url, dados, config).then((res) => {
        console.log(res.data);
        if (res.data.success === true) {
          this.setState({ txtParcelaAposent: res.data.parcelaIdeal });
          this.ObterDadosAPIDetalhado();
        }
      });
    }
  };

  validaDados = () => {
    if (
      this.state.txtIdadeInicial === undefined ||
      this.state.txtIdadeFutura === undefined ||
      this.state.txtIdadeUsofruto === undefined ||
      this.state.txtGastosCorrigidos === undefined ||
      this.state.txtParcelaAposent === undefined
    ) {
      return false;
    } else {
      return true;
    }
  };

  ObterDadosAPIDetalhado = (scroll = true) => {
    //console.log(this.state.txtIdadeInicial);
    if (
      this.state.txtIdadeInicial === undefined ||
      this.state.txtIdadeInicial === "" ||
      this.state.txtIdadeFutura === undefined ||
      this.state.txtIdadeFutura === "" ||
      this.state.txtIdadeUsofruto === undefined ||
      this.state.txtIdadeUsofruto === "" ||
      this.state.txtGastosAtuais === undefined ||
      this.state.txtGastosAtuais === "" ||
      this.state.txtParcelaAposent === undefined ||
      this.state.txtParcelaAposent === "" ||
      this.state.txtParcelaAposent === undefined ||
      this.state.txtParcelaAposent === ""
    ) {
      const options = {
        title: "Informação - Dados incompletos",
        message: "Por favor, preencha os dados ou carregue um plano.",
        buttons: [
          {
            label: "Ok",
          },
        ],
      };
      confirmAlert(options);
    } else if (this.state.txtIdadeInicial > this.state.txtIdadeFutura 
      // || this.state.txtIdadeInicial == this.state.txtIdadeFutura
      ) {
      const options = {
        title: "Informação - Dados inconsistentes",
        message:
          "A idade de aposentadoria não pode ser inferior ou igual à idade inicial. Revise.",
        buttons: [
          {
            label: "Ok",
          },
        ],
      };
      confirmAlert(options);
    } else if (
      this.state.txtIdadeFutura > this.state.txtIdadeUsofruto ||
      this.state.txtIdadeFutura == this.state.txtIdadeUsofruto
    ) {
      const options = {
        title: "Informação - Dados inconsistentes",
        message:
          "A idade de aposentadoria não pode ser maior ou igual que a idade de usofruto. Revise.",
        buttons: [
          {
            label: "Ok",
          },
        ],
      };
      confirmAlert(options);
    } else {
      this.setState({
        txtGastosCorrigidos: this.calcularGastoCorrigido(
          this.state.txtGastosAtuais,
          this.state.txtIdadeInicial,
          this.state.txtIdadeFutura,
          this.state.taxaInflacao
        ),
      });

      var url =
        global.server_api_new +
        global.apiToken +
        "/aposentadoria/calculardetalhado";

      var dados = {
        AnoInicio: 2021,
        IdadeInicial: parseInt(this.state.txtIdadeInicial),
        IdadeAposentadoria: parseInt(this.state.txtIdadeFutura),
        IdadeMaxUsofruto: parseInt(this.state.txtIdadeUsofruto),
        GastosAtuais: this.tratarMascara(this.state.txtGastosAtuais),
        GastosCorrigidos: this.tratarMascara(this.state.txtGastosCorrigidos),
        InvestimentoAposentadoria: this.tratarMascara(
          this.state.txtParcelaAposent
        ),
        DestinacaoInicial:
          this.state.txtPatrimonioDestinado === undefined ||
          this.state.txtPatrimonioDestinado == null ||
          this.state.txtPatrimonioDestinado == ""
            ? 0
            : this.tratarMascara(this.state.txtPatrimonioDestinado),
        RentabilidadeAnualAcum: parseFloat(
          this.desinflarTaxaFull(this.state.rentAnualAcumulacao) / 100
        ),
        RentabilidadeAnualUsufruto: parseFloat(
          this.desinflarTaxaFull(this.state.rentAnualUsofruto) / 100
        ),
        InflacaoAnual: parseFloat(this.state.taxaInflacao / 100),
        Id: this.state.idAposDadoExtra,
        AtualizarAporteAnualmenteInflacao: this.state.atualizarRentabInflacao,
      };
      console.log("dados calc", dados);

      var config = {
        headers: {
          Authorization: "bearer " + this.state.accessToken,
          "Access-Control-Allow-Origin": "*",
          "Access-Control-Allow-Headers": "Authorization",
          "Access-Control-Allow-Methods":
            "GET, POST, OPTIONS, PUT, PATCH, DELETE",
        },
      };
      axios.post(url, dados, config).then((res) => {
        if (res.data.success === true) {
          console.debug(res.data, 'DEBUG - Simulação aposentadoria, dados');
          const currentYear = getYear(new Date())
          const anosEditado = res.data.anos.filter((item) => item.ano >= currentYear)
          console.debug('ANOS EDITADO')
          this.setState({
            IsProcessed: true,
            grCurva_series: [
              {
                name: "Curva Ideal",
                color: "#9A3DFF",
                data: res.data.grafico,
              },
              {
                name: "Curva Atual",
                color: "#03989e",
                //data: [],
                data: res.data.graficoRealizado,
              },
              {
                name: "Curva Atual Estimada",
                color: "#03989e",
                data: res.data.graficoRealizadoEstimado,
                //data: [],
              },
            ],
            grCurva_options: {
              chart: {
                type: "area",
                height: 350,
              },
              colors: ["#9A3DFF", "#03989e", "#03989e"],
              dataLabels: {
                enabled: false,
              },
              stroke: {
                curve: "straight",
                dashArray: [0, 0, 8],
              },
              title: {
                text: "Detalhamento",
                align: "left",
                style: {
                  fontSize: "14px",
                },
              },
              xaxis: {
                type: "number",
                axisBorder: {
                  show: false,
                },
                axisTicks: {
                  show: false,
                },
                title: {
                  text: "Idade",
                },
              },
              yaxis: {
                title: {
                  text: "Acumulado",
                },
                tickAmount: 3,
                floating: false,
                labels: {
                  offsetY: -7,
                  offsetX: 0,
                },
                axisBorder: {
                  show: false,
                },
                axisTicks: {
                  show: false,
                },
                labels: {
                  style: {
                    colors: "#8e8da4",
                  },
                  offsetX: 0,
                  formatter: function (val) {
                    try {
                      var numero = val.toFixed(2).split(".");
                      numero[0] =
                        "R$ " + numero[0].split(/(?=(?:...)*$)/).join(".");
                      return numero.join(",");
                    } catch {
                      try {
                        return val.toFixed(2);
                      } catch {
                        return val;
                      }
                    }
                  },
                },
              },
              fill: {
                opacity: 0.5,
              },
              tooltip: {
                enabled: true,
                shared: true,
                followCursor: false,
                fixed: {
                  enabled: false,
                  position: "topLeft", // topRight, topLeft, bottomRight, bottomLeft
                  offsetY: 30,
                  offsetX: 60,
                },
              },
              grid: {
                yaxis: {
                  lines: {
                    offsetX: -30,
                  },
                },
                padding: {
                  left: 20,
                },
              },
              legend: {
                position: "top",
                horizontalAlign: "center",
                floating: true,
                offsetY: -25,
                offsetX: -5,
              },
              //labels: [30, 40, 50, 60, 75],
            },
            grCurvaMenor_series: [
              {
                name: "Aporte",
                color: "#9A3DFF",
                data: res.data.graficoMenorAporte,
              },
            ],
            rowsTable: anosEditado,
            grPie_series: [res.data.percAporte, res.data.percJuros],
            valorFuturoState: res.data.totAcumulado,
            valorMetaPlano: res.data.valorMeta,
          });

          sessionStorage.removeItem("idAposentadoria");
          if (scroll) {
            window.scrollTo(0, 600);
          }
        }
      });
    }
  };

  carregaDadosSelect = async () => {
    global.spinnerShow($);

    var url =
      global.server_api_new +
      global.apiToken +
      "/sonho/familia/aposentadoria/" +
      this.state.familiaId;

    var config = {
      headers: {
        Authorization: "bearer " + this.state.accessToken,
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Headers": "Authorization",
        "Access-Control-Allow-Methods":
          "GET, POST, OPTIONS, PUT, PATCH, DELETE",
      },
    };

    var selecao = [{ value: -99, label: "Cadastrar Plano de Aposentadoria" }];
    //alert('executou');
    await axios.get(url, config).then((res) => {
      //console.log("res:", res.data);
      if (res.data.results != null) {
        //console.log(res.data.results);
        res.data.results.forEach((item) => {
          selecao.push({ value: item.id, label: item.descricao });
        });
      }
      this.setState({ selectOp: selecao });

      let item = selecao.findIndex(
        (x) => x.value === parseInt(sessionStorage.getItem("idAposentadoria"))
      );
      //sessionStorage.removeItem("idAposentadoria");
      //console.log(item, sessionStorage.getItem("idAposentadoria"));

      if (item > 0 && item != undefined) {
        this.handleChange(selecao[item]);
      }
    });

    global.spinnerHide($, [
      document.documentElement.scrollLeft || document.body.scrollLeft,
      document.documentElement.scrollTop || document.body.scrollTop,
    ]);

    console.log(selecao);
  };

  calcularValorPresente = () => {
    var limite = this.state.txtIdadeFutura - this.state.txtIdadeInicial;
    var parcela = this.tratarMascara(this.state.txtParcelaAposent);
    var tot = 0;

    for (let index = 1; index <= limite + 1; index++) {
      if (this.state.atualizarRentabInflacao == true) {
        parcela = parcela * (1 + this.state.taxaInflacao / 100);
        tot = tot + parcela * 12;
      } else {
        tot = tot + parcela * 12;
      }
    }
    console.log(tot);
    return parseFloat(tot);
  };

  SalvarPlano = async () => {
    global.spinnerShow($);
    //console.log(this.state.txtIdadeInicial);
    if (
      this.state.txtIdadeInicial === undefined ||
      this.state.txtIdadeInicial === "" ||
      this.state.txtIdadeFutura === undefined ||
      this.state.txtIdadeFutura === "" ||
      this.state.txtIdadeUsofruto === undefined ||
      this.state.txtIdadeUsofruto === "" ||
      this.state.txtGastosCorrigidos === undefined ||
      this.state.txtGastosCorrigidos === "" ||
      this.state.txtParcelaAposent === undefined ||
      this.state.txtParcelaAposent === ""
    ) {
      const options = {
        title: "Informação - Dados incompletos",
        message: "Por favor, preencha os dados ou carregue um plano salvo.",
        buttons: [
          {
            label: "Ok",
          },
        ],
      };
      confirmAlert(options);
    } else if (this.state.txtIdadeInicial > this.state.txtIdadeFutura) {
      const options = {
        title: "Informação - Dados inconsistentes",
        message:
          "A idade de aposentadoria não pode ser inferior à idade inicial. Revise.",
        buttons: [
          {
            label: "Ok",
          },
        ],
      };
      confirmAlert(options);
    } else if (this.state.txtIdadeFutura > this.state.txtIdadeUsofruto) {
      const options = {
        title: "Informação - Dados inconsistentes",
        message:
          "A idade de aposentadoria não pode ser maior que a idade de usofruto. Revise.",
        buttons: [
          {
            label: "Ok",
          },
        ],
      };
      confirmAlert(options);
    } else if (this.state.aposentadoriaSelecionada === -1) {
      const options = {
        title: "Informação",
        message: "Selecione um plano ou realize o cadastro antes de salvar.",
        buttons: [
          {
            label: "Ok",
          },
        ],
      };
      confirmAlert(options);
    } else if (this.state.IsProcessed === true) {
      //Salva o Plano!

      var anos = this.state.txtIdadeFutura - this.state.txtIdadeInicial;
      var prazo = 12 * anos;
      var sonhoAposentadoria =
        this.state.aposentadoriaSelecionada === -1
          ? null
          : this.state.aposentadoriaSelecionada;

      var item = {
        familiaId:
          this.state.familiaId != null ? parseInt(this.state.familiaId) : null,
        empresaId:
          this.state.empresaId != null ? parseInt(this.state.empresaId) : null,
        descricao: this.state.sonhoSelec_descricao,
        imagem: null,
        sonhoId: sonhoAposentadoria,
        statusSonho: this.state.status != null ? this.state.status : 1,
        tipo: "plano",
        //Dados do Plano
        dataInicial: this.state.dataInicial, //Data que criou o plano (nao editavel)
        prazo: prazo, //Meses
        dataFinal: this.state.dataFinal, //Mesmo dia (x meses para frente - calculado automicamente)
        valorEstimadoPresente: this.calcularValorPresente(), //Valor estimado hoje
        inflacao: this.state.taxaInflacao, //Inflacao
        //valorEstimadoFuturo: this.state.valorFuturo != null && this.state.valorFuturo !== "" ? parseFloat(this.state.valorFuturo).toFixed(2) : null,      //VEF = VEP * (1 + inflação/100) ^ ( prazo / 12 )
        valorEstimadoFuturo: this.state.valorMetaPlano,
        taxa: this.desinflarTaxaFull(this.state.rentAnualAcumulacao), //Taxa do Rendimento
        parcela:
          this.state.txtParcelaAposent != null &&
          this.state.txtParcelaAposent !== ""
            ? this.tratarMascara(this.state.txtParcelaAposent).toFixed(2)
            : null, //Valor da Parcela (PMT [TAXA] do excel)
        categoriaSonho: global.idSonhoAposentadoria,
      };

      //var url = global.server_api + 'api/sonho' + (this.state.id != null ? "/" + this.state.id: "");
      var url =
        global.server_api_new +
        global.apiToken +
        "/sonho" +
        (this.state.id != null ? "/" + this.state.id : "");

      var config = {
        headers: {
          Authorization: "bearer " + this.state.accessToken,
          "Access-Control-Allow-Origin": "*",
          "Access-Control-Allow-Headers": "Authorization",
          "Access-Control-Allow-Methods":
            "GET, POST, OPTIONS, PUT, PATCH, DELETE",
        },
      };
      console.log(url, item);
      await axios.post(url, item, config).then((res) => {
        if (res.data.success === true) {
          //Salva os dados Extras
          var dadoExtra = {
            AnoInicio: 2021,
            SonhoId: parseInt(this.state.aposentadoriaSelecionada),
            IdadeInicial: parseInt(this.state.txtIdadeInicial),
            IdadeAposentadoria: parseInt(this.state.txtIdadeFutura),
            IdadeMaxUsofruto: parseInt(this.state.txtIdadeUsofruto),
            GastosAtuais: this.tratarMascara(
              this.state.txtGastosAtuais
            ).toFixed(2),
            GastosCorrigidos: this.tratarMascara(
              this.state.txtGastosCorrigidos
            ).toFixed(2),
            InvestimentoAposentadoria: this.tratarMascara(
              this.state.txtParcelaAposent
            ).toFixed(2),
            DestinacaoInicial:
              this.state.txtPatrimonioDestinado === undefined ||
              this.state.txtPatrimonioDestinado == null ||
              this.state.txtPatrimonioDestinado == ""
                ? 0
                : this.tratarMascara(this.state.txtPatrimonioDestinado),
            RentabilidadeAnualAcum: parseFloat(this.state.rentAnualAcumulacao),
            RentabilidadeAnualUsufruto: parseFloat(
              this.state.rentAnualUsofruto
            ),
            InflacaoAnual: parseFloat(this.state.taxaInflacao),
            AtualizarAporteAnualmenteInflacao: this.state
              .atualizarRentabInflacao,
          };

          console.log(dadoExtra);
          var url2 =
            global.server_api_new +
            global.apiToken +
            "/aposentadoria/cadastrar";
          var config2 = {
            headers: {
              Authorization: "bearer " + this.state.accessToken,
              "Access-Control-Allow-Origin": "*",
              "Access-Control-Allow-Headers": "Authorization",
              "Access-Control-Allow-Methods":
                "GET, POST, OPTIONS, PUT, PATCH, DELETE",
            },
          };
          axios.post(url2, dadoExtra, config2).then((res) => {
            if (res.data.success) {
              this.setState({ idAposDadoExtra: res.data.dadosCarga.id });
              const options = {
                title: "Informação",
                message: "Plano Salvo com sucesso!",
                buttons: [
                  {
                    label: "Ok",
                  },
                ],
              };
              confirmAlert(options);
              this.ObterDadosAPIDetalhado();
            } else {
              console.log(res.data);
              const options = {
                title: "Erro ao salvar o plano",
                message: res.data.responseMessage,
                buttons: [
                  {
                    label: "Ok",
                  },
                ],
              };
              confirmAlert(options);
            }
          });
        }
      });
    } else {
      const options = {
        title: "Informação - Dados incompletos",
        message:
          "Por favor, preencha os dados e, em seguida, calcule o Plano antes de salvar.",
        buttons: [
          {
            label: "Ok",
          },
        ],
      };
      confirmAlert(options);
    }

    global.spinnerHide($, [
      document.documentElement.scrollLeft || document.body.scrollLeft,
      document.documentElement.scrollTop || document.body.scrollTop,
    ]);
  };

  analiseIdade = (idade, inicio) => {
    if (inicio === true) {
      if (
        this.state.txtIdadeFutura == undefined ||
        this.state.txtIdadeFutura == null ||
        this.state.txtIdadeFutura == ""
      ) {
        return;
      }
      if (parseInt(idade) > this.state.txtIdadeFutura) {
        //var temp = parseInt(this.state.txtIdadeFutura - 1);

        this.setState({ IsProcessed: false });
        const options = {
          title: "Informação - Dados inconsistentes",
          message:
            "A idade atual não pode ser superior à idade de aposentadoria. Revise.",
          buttons: [
            {
              label: "Ok",
            },
          ],
        };
        confirmAlert(options);
      } else {
        this.setState({
          txtGastosCorrigidos: this.calcularGastoCorrigido(
            this.state.txtGastosAtuais,
            parseInt(idade),
            this.state.txtIdadeFutura,
            this.state.taxaInflacao
          ),
        });
      }
    } else {
      if (
        this.state.txtIdadeInicial == undefined ||
        this.state.txtIdadeInicial == null
      ) {
        return;
      }
      if (parseInt(idade) < this.state.txtIdadeInicial) {
        this.setState({ IsProcessed: false });
        const options = {
          title: "Informação - Dados inconsistentes",
          message:
            "A idade de aposentadoria não pode ser inferior à idade inicial. Revise.",
          buttons: [
            {
              label: "Ok",
            },
          ],
        };
        confirmAlert(options);
      } else {
        this.setState({
          txtGastosCorrigidos: this.calcularGastoCorrigido(
            this.state.txtGastosAtuais,
            this.state.txtIdadeInicial,
            parseInt(idade),
            this.state.taxaInflacao
          ),
        });
      }
    }
  };

  desinflarTaxa = (taxaAnual) => {
    var novaTaxa = (1 + taxaAnual / 100) ** (1 / 12) - 1;
    return (novaTaxa * 100).toFixed(2);
  };

  desinflarTaxaFull = (taxaAnual) => {
    var novaTaxa = (1 + taxaAnual / 100) ** (1 / 12) - 1;
    return novaTaxa * 100;
  };

  handleUsoFruto = (event) => {
    this.setState({
      txtIdadeUsofruto: parseInt(event.target.value),
    });
  };

  handlePatrimDestinado = (event) => {
    this.setState({
      txtPatrimonioDestinado: this.tratarMascara(event.target.value),
    });
  };

  handleGastosAtuais = (event) => {
    this.setState({
      txtGastosAtuais: event.target.value,
      txtGastosCorrigidos: this.calcularGastoCorrigido(
        this.tratarMascara(event.target.value),
        this.state.txtIdadeInicial,
        this.state.txtIdadeFutura,
        this.state.taxaInflacao
      ),
    });
    //setTimeout(this.recalculo(), 1000);
    console.log("olaaaaaaaaaaaaaaaaaaaaaaaa");
  };

  handleGastosCorrigidos = (event) => {
    this.setState({
      txtGastosCorrigidos: event.target.value,
    });
  };

  handleChangeRadio = (event) => {
    var aporte = "true";
    if (event.target.value === "false") {
      aporte = "false";
    }
    this.setState({
      aporteResgate_EhAporte: aporte,
    });
  };

  tratarMascara = (dado) => {
    var temp = dado + "";
    console.log(temp);
    if (temp.includes(",")) {
      return parseFloat(
        temp
          .trim()
          .replaceAll("R$", "")
          .replaceAll(".", "")
          .replaceAll(",", ".")
      );
    } else {
      return parseFloat(temp.trim().replaceAll("R$", ""));
    }
  };

  SalvarAporteExtra = async () => {
    if (this.state.aposentadoriaSelecionada == -1) {
      const options = {
        title: "Informação",
        message:
          "É necessário selecionar ou cadastrar um plano antes de salvar aportes/resgates.",
        buttons: [
          {
            label: "Ok",
          },
        ],
      };
      confirmAlert(options);
    } else if (
      this.state.aporteResgate_Ano === null ||
      this.state.aporteResgate_Ano === undefined ||
      this.state.aporteResgate_Ano === "" ||
      this.state.aporteResgate_Mes === undefined ||
      this.state.aporteResgate_Mes === null ||
      this.state.aporteResgate_Mes === "" ||
      this.state.aporteResgate_Valor === null ||
      this.state.aporteResgate_Valor === undefined
    ) {
      const options = {
        title: "Informação - Dados incompletos",
        message: "Por favor, preencha a data e o valor do aporte",
        buttons: [
          {
            label: "Ok",
          },
        ],
      };
      confirmAlert(options);
    } else {
      var url =
        global.server_api_new +
        global.apiToken +
        "/aposentadoria/incluir/aporteresgate";

      var config = {
        headers: {
          Authorization: "bearer " + this.state.accessToken,
          "Access-Control-Allow-Origin": "*",
          "Access-Control-Allow-Headers": "Authorization",
          "Access-Control-Allow-Methods":
            "GET, POST, OPTIONS, PUT, PATCH, DELETE",
        },
      };
      var data =
        this.state.aporteResgate_Ano +
        "/" +
        this.state.aporteResgate_Mes +
        "/01 00:00:00";
      var EAporte = this.state.aporteResgate_EhAporte === "true" ? true : false;
      //console.log(this.state.aporteResgate_Valor);
      var valor = this.tratarMascara(this.state.aporteResgate_Valor);
      var item = {
        AposentadoriaDadosExtraId: this.state.idAposDadoExtra,
        Data: data,
        Valor: valor,
        EhAporte: EAporte,
        qtdeRepetido:
          this.state.aporteResgate_QtdeRepet == null ||
          this.state.aporteResgate_QtdeRepet == undefined ||
          this.state.aporteResgate_QtdeRepet == ""
            ? 0
            : this.state.aporteResgate_QtdeRepet,
        repetido:
          this.state.aporteResgate_QtdeRepet == null ||
          this.state.aporteResgate_QtdeRepet == "" ||
          this.state.aporteResgate_QtdeRepet == undefined
            ? false
            : true,
      };
      console.log(item);
      await axios.post(url, item, config).then((res) => {
        console.log(res.data);
        if (res.data.success === true) {
          this.setState({
            aporteResgate_Ano: "",
            aporteResgate_Valor: null,
            aporteResgate_QtdeRepet: "",
          });
          const options = {
            title: "Informação",
            message: "Aporte/Resgate salvo com sucesso!",
            buttons: [
              {
                label: "Ok",
              },
            ],
          };
          confirmAlert(options);
          this.modalAporteExtra.hide();
          this.ObterDadosAPIDetalhado();
        }
      });
    }
  };

  SalvarConfigExtra = async () => {
    if (this.state.idAposDadoExtra == null) {
      const options = {
        title: "Informação",
        message:
          "É necessário salvar o plano antes de gravar as configurações avançadas. Porém fique tranquilo, os dados informados estarão sendo utilizados nos calculos realizados.",
        buttons: [
          {
            label: "Ok",
          },
        ],
      };
      confirmAlert(options);
    } else if (
      this.state.rentAnualAcumulacao === null ||
      this.state.rentAnualAcumulacao === undefined ||
      this.state.rentAnualAcumulacao === "" ||
      this.state.rentAnualUsofruto === undefined ||
      this.state.rentAnualUsofruto === null ||
      this.state.rentAnualUsofruto === "" ||
      this.state.taxaInflacao === null ||
      this.state.taxaInflacao === undefined ||
      this.state.taxaInflacao === ""
    ) {
      console.log(
        this.state.rentAnualAcumulacao,
        this.state.rentAnualUsofruto,
        this.state.taxaInflacao
      );
      const options = {
        title: "Informação",
        message: "Dados incompletos, revise.",
        buttons: [
          {
            label: "Ok",
          },
        ],
      };
      confirmAlert(options);
    } else {
      var url =
        global.server_api_new +
        global.apiToken +
        "/aposentadoria/update/config-avancada";

      var config = {
        headers: {
          Authorization: "bearer " + this.state.accessToken,
          "Access-Control-Allow-Origin": "*",
          "Access-Control-Allow-Headers": "Authorization",
          "Access-Control-Allow-Methods":
            "GET, POST, OPTIONS, PUT, PATCH, DELETE",
        },
      };
      console.log(
        this.state.rentAnualAcumulacao,
        this.state.rentAnualUsofruto,
        this.state.taxaInflacao
      );
      var item = {
        Id: this.state.idAposDadoExtra,
        RentabilidadeAnualAcum: this.state.rentAnualAcumulacao,
        RentabilidadeAnualUsufruto: this.state.rentAnualUsofruto,
        InflacaoAnual: this.state.taxaInflacao,
        AtualizarAporteAnualmenteInflacao: this.state.atualizarRentabInflacao,
      };
      console.log("SalvarConfigExtra Item", item);
      await axios.post(url, item, config).then((res) => {
        console.log("result", res.data);
        if (res.data.success === true) {
          const options = {
            title: "Informação",
            message: "Configuração salva com sucesso!",
            buttons: [
              {
                label: "Ok",
              },
            ],
          };
          confirmAlert(options);
          this.ObterDadosAPIDetalhado();
          this.modalConfigAvancadas.hide();
        } else {
          const options = {
            title: "Erro",
            message: res.data.error,
            buttons: [
              {
                label: "Ok",
              },
            ],
          };
          confirmAlert(options);
        }
      });
    }
  };

  callModalConfig = () => {
    //this.handleChangeTable();
    global.showModal(this.modalConfigAvancadas);
  };

  callModalAporte = () => {
    if (this.state.idAposDadoExtra == null) {
      const options = {
        title: "Informação",
        message:
          "É necessário salvar pelo menos uma vez o plano antes de incluir aportes ou resgates.",
        buttons: [
          {
            label: "Ok",
          },
        ],
      };
      confirmAlert(options);
    } else {
      global.showModal(this.modalAporteExtra);
    }
  };

  renderModalAporteExtra = () => {
    return (
      <SkyLight
        ref={(ref) => (this.modalAporteExtra = ref)}
        transitionDuration={500}
        title={
          <>
            <div>
              <h2 className="modal_title text-center">
                Aporte Extra: Informe os dados do aporte
              </h2>
            </div>
          </>
        }
      >
        <div className="py-5">
          <div className="form-inline" style={{ justifyContent: "center" }}>
            <Input
              className="input-component"
              style={{ marginLeft: 15, marginRight: 15, width: 110 }}
              id="aps_aporteExtraAno"
              placeholder="Ano"
              value={this.state.aporteResgate_Ano}
              onChange={(digitado) =>
                this.setState({
                  aporteResgate_Ano: parseInt(digitado.target.value),
                })
              }
              type="number"
              inputProps={{
                "aria-label": "description",
                min: "1",
                max: "150",
              }}
            />
            {this.renderSelectAporteExtra()}
            <Input
              className="input-component"
              style={{ marginLeft: 15, marginRight: 15, width: 150 }}
              id="aps_aporteExtra"
              placeholder="Qtde de Repetições"
              value={this.state.aporteResgate_QtdeRepet}
              onChange={(digitado) =>
                this.setState({
                  aporteResgate_QtdeRepet: parseInt(digitado.target.value),
                })
              }
              type="number"
              inputProps={{
                "aria-label": "description",
                min: "1",
                max: "150",
              }}
            />
          </div>
          <hr />
          <div className="form-inline" style={{ justifyContent: "center" }}>
            <div
              className="MuiInputBase-root MuiInput-root MuiInput-underline"
              style={{
                marginLeft: 15,
                marginRight: 15,
                maxWidth: 500,
                textAlign: "center",
              }}
            >
              <CurrencyInput
                id="aps_aporteExtraValor"
                className="MuiInputBase-input MuiInput-input"
                placeholder="R$ Valor"
                value={this.state.aporteResgate_Valor}
                decimalSeparator={","}
                thousandSeparator={"."}
                precision="2"
                prefix="R$ "
                onChangeEvent={(digitado) =>
                  this.setState({
                    aporteResgate_Valor: digitado.target.value,
                  })
                }
              />
            </div>

            <div className="pl-3">
              <FormControl component="fieldset">
                <FormLabel component="legend">Aporte/Resgate</FormLabel>
                <RadioGroup
                  aria-label="gender"
                  name="gender1"
                  value={this.state.aporteResgate_EhAporte}
                  onChange={this.handleChangeRadio}
                >
                  <div className="form-inline">
                    <FormControlLabel
                      value="true"
                      control={<Radio />}
                      label="Aporte"
                    />
                    <FormControlLabel
                      value="false"
                      control={<Radio />}
                      label="Resgate"
                    />
                  </div>
                </RadioGroup>
              </FormControl>
            </div>
          </div>
          <Button
            className="featured-button my-4 my-3"
            color="primary"
            type="button"
            style={{
              backgroundImage:
                "url(" +
                require("../../../assets/img/theme/botao-destaque.png") +
                ")",
              minWidth: 150,
            }}
            onClick={() => this.SalvarAporteExtra()}
          >
            Salvar
          </Button>
        </div>
        <br />
      </SkyLight>
    );
  };

  renderSelectAporteExtra = () => {
    return (
      <Select
        id="aporteExtraMes"
        onChange={(selectedOption) =>
          this.setState({ aporteResgate_Mes: selectedOption.value })
        }
        className="select-component col-xl-4 ml-3 py-3"
        options={this.selectAporteExtra}
        placeholder="Selecione o mês.."
      />
    );
  };

  renderModalConfigAvancado = () => {
    return (
      <div>
        <SkyLight
          ref={(ref) => (this.modalConfigAvancadas = ref)}
          transitionDuration={500}
          dialogStyles={{ borderRadius: "1rem", padding: "2rem" }}
          title={
            <>
              <div className="pop-up-title">
                <i className="ni ni-check-bold"></i>
                <h2>Configurações Avançadas</h2>
              </div>
            </>
          }
        >
          <Row>
            <Col lg="6" xl="6">
              <div>
                <label>Rentab. Anual Acumulação</label>
                <input
                  type="number"
                  id="nome"
                  value={this.state.rentAnualAcumulacao}
                  style={{ paddingLeft: 24, color: "#8898aa" }}
                  onChange={(e) =>
                    this.setState({
                      rentAnualAcumulacao: e.target.value,
                      rentMensalAcumulacao: this.desinflarTaxa(e.target.value),
                    })
                  }
                ></input>
              </div>
            </Col>
            <Col lg="6" xl="6">
              <div>
                <label>Rentabilidade Mensal</label>
                <input
                  type="number"
                  id="nome"
                  disabled
                  value={this.state.rentMensalAcumulacao}
                  style={{ paddingLeft: 24, color: "#8898aa" }}
                ></input>
              </div>
            </Col>

            <Col lg="6" xl="6">
              <div>
                <label>Rentabilidade Anual no Usufruto</label>
                <input
                  type="number"
                  id="nome"
                  value={this.state.rentAnualUsofruto}
                  style={{ paddingLeft: 24, color: "#8898aa" }}
                  onChange={(e) =>
                    this.setState({
                      rentAnualUsofruto: e.target.value,
                      rentMensalUsofruto: this.desinflarTaxa(e.target.value),
                    })
                  }
                ></input>
              </div>
            </Col>
            <Col lg="6" xl="6">
              <div>
                <label>Rentabilidade Mensal</label>
                <input
                  type="number"
                  id="nome"
                  disabled
                  value={this.state.rentMensalUsofruto}
                  style={{ paddingLeft: 24, color: "#8898aa" }}
                ></input>
              </div>
            </Col>

            <Col lg="6" xl="6">
              <div>
                <label>Inflação Anual</label>
                <input
                  type="number"
                  id="nome"
                  value={this.state.taxaInflacao}
                  style={{ paddingLeft: 24, color: "#8898aa" }}
                  onChange={(e) =>
                    this.setState({
                      taxaInflacao: e.target.value,
                      taxaInflacaoMensal: this.desinflarTaxa(e.target.value),
                      txtGastosCorrigidos: this.calcularGastoCorrigido(
                        this.state.txtGastosAtuais,
                        this.state.txtIdadeInicial,
                        this.state.txtIdadeFutura,
                        e.target.value
                      ),
                    })
                  }
                ></input>
              </div>
            </Col>
            <Col lg="6" xl="6">
              <div>
                <label>Inflação Mensal</label>
                <input
                  type="number"
                  id="nome"
                  disabled
                  value={this.state.taxaInflacaoMensal}
                  style={{ paddingLeft: 24, color: "#8898aa" }}
                ></input>
              </div>
            </Col>
            <Col xs="12">
              <FormGroup style={{ padding: 0, textAlign: "center" }}>
                <FormControlLabel
                  control={
                    <Checkbox
                      checked={this.state.atualizarRentabInflacao}
                      onChange={() =>
                        this.setState({
                          atualizarRentabInflacao: !this.state
                            .atualizarRentabInflacao,
                        })
                      }
                    />
                  }
                  label="Atualizar aporte conforme inflação"
                />
              </FormGroup>
            </Col>
          </Row>
          <Row>
            <Col
              lg="12"
              xl="12"
              style={{ display: "flex", justifyContent: "center" }}
            >
              <button
                className="general-button aux-button dark with-left-margin nav-item"
                onClick={() => this.SalvarConfigExtra()}
              >
                Salvar
              </button>
            </Col>
          </Row>
        </SkyLight>
      </div>
    );
  };

  renderDadosDetalhado = () => {
    if (this.state.IsProcessed) {
      return (
        <>
          <h3
            style={{
              paddingTop: 60,
              paddingBottom: 25,
              textAlign: "center",
              fontSize: "2.0rem",
              color: "#27adc0",
            }}
          >
            Gráfico: Detalhamento
          </h3>
          <br />
          <div id="chart" className="form-inline">
            <ApexChart
              options={this.state.grCurva_options}
              series={this.state.grCurva_series}
              type="area"
              className="col-xl-12"
            />
            <ApexChart
              options={this.state.grPie_options}
              series={this.state.grPie_series}
              type="pie"
              className="col-xl-4 py-3"
            />

            <ApexChart
              options={this.state.grCurvaMenor_options}
              series={this.state.grCurvaMenor_series}
              type="area"
              className="col-xl-8 py-3"
            />
          </div>
          <br />
          <div style={{ textAlign: "end" }}>
            <Button
              className="my-4"
              color="primary"
              type="button"
              onClick={() => this.callModalAporte()}
            >
              Aporte/Resgate
              <br />
              Extra
            </Button>
          </div>
          <div style={{ height: 600, width: "100%" }}>
            <Tabela
              aposentadoriaId={this.state.idAposDadoExtra}
              data={this.state.rowsTable}
              reload={() => this.ObterDadosAPIDetalhado(false)}
            />
          </div>
        </>
      );
    }
  };

  renderSonhosCadastrados = () => {
    let item = this.state.selectOp.findIndex(
      (x) => x.value === parseInt(sessionStorage.getItem("idAposentadoria"))
    );
    //sessionStorage.removeItem("idAposentadoria");

    return (
      <Select
        id="campoCalculado"
        onChange={this.handleChange}
        className="select-component col-xl-8 ml-2 py-3"
        options={this.state.selectOp}
        value={this.state.selectOp[item]}
        //defaultValue={this.gestaoPlanoCampoCalculado[0]}
        placeholder="Selecione o seu Plano de Aposentadoria.."
      />
    );
  };

  renderPaginaSimulacao = () => {
    const TooltipRestore = withStyles((theme) => ({
      tooltip: {
        backgroundColor: "#888888",
        color: "white",
        fontSize: 12,
        fontWeight: "bold",
        fontFamily: "Open Sans",
      },
    }))(Tooltip);

    console.log(this.state.aposentadoriaSelecionada);
    if (
      this.state.aposentadoriaSelecionada != -1 &&
      this.state.aposentadoriaSelecionada != -99 &&
      this.state.aposentadoriaSelecionada != null
    ) {
      return (
        <>
          <div className="" style={{ padding: 0 }}>
            <Col
              lg="12"
              xl="12"
              md="12"
              className="ml-3"
              style={{ marginTop: 30 }}
            >
              <h2 style={{ paddingBottom: 25 }}>Um pouco sobre você...</h2>

              <p className="aposentadoria text-justify">
                Eu tenho
                <Input
                  style={{ marginLeft: 15, width: 130, textAlign: "center" }}
                  id="aps_idade"
                  placeholder="Sua Idade Atual"
                  type="number"
                  inputProps={{
                    "aria-label": "description",
                    min: "1",
                    max: "100",
                    fullWidth: true,
                  }}
                  //onChange={this.handleIdadeInicial}
                  onChange={(digitado) =>
                    this.setState({
                      txtIdadeInicial: parseInt(digitado.target.value),
                    })
                  }
                  defaultValue={null}
                  value={this.state.txtIdadeInicial}
                  onBlur={() =>
                    this.analiseIdade(this.state.txtIdadeInicial, true)
                  }
                />
                <TooltipRestore
                  disableFocusListener
                  disableTouchListener
                  title="Sua Idade atual"
                >
                  <Link>
                    <i
                      class="fas fa-info-circle"
                      style={{
                        textAlign: "start",
                        background: "white",
                        color: "#03989e",
                        fontSize: "20px",
                        paddingTop: "0px",
                        paddingLeft: "5px",
                        cursor: "info",
                        marginRight: 15,
                      }}
                    ></i>
                  </Link>
                </TooltipRestore>
                anos e gostaria de alcançar minha independência financeira aos
                <Input
                  style={{ marginLeft: 15, width: 195 }}
                  id="aps_tempoInv"
                  className="pr-3"
                  placeholder="Idade para se Aposentar"
                  type="number"
                  inputProps={{
                    "aria-label": "description",
                    min: "1",
                    max: "150",
                  }}
                  allowEmpty
                  value={this.state.txtIdadeFutura}
                  //onChange={this.handleTxtIdadeFut}
                  onChange={(digitado) =>
                    this.setState({
                      txtIdadeFutura: parseInt(digitado.target.value),
                    })
                  }
                  onBlur={() =>
                    this.analiseIdade(this.state.txtIdadeFutura, false)
                  }
                />
                <TooltipRestore
                  disableFocusListener
                  disableTouchListener
                  title="Idade em que irei me aposentar"
                >
                  <Link>
                    <i
                      class="fas fa-info-circle"
                      style={{
                        textAlign: "start",
                        background: "white",
                        color: "#03989e",
                        fontSize: "20px",
                        paddingTop: "0px",
                        paddingLeft: "5px",
                        cursor: "info",
                        marginRight: 15,
                      }}
                    ></i>
                  </Link>
                </TooltipRestore>
                anos.
              </p>

              <p className="text-justify">
                Tenho um valor acumulado de
                <div
                  className="MuiInputBase-root MuiInput-root MuiInput-underline"
                  style={{ marginLeft: 15, width: 200 }}
                >
                  <CurrencyInput
                    className="MuiInputBase-input MuiInput-input"
                    id="aps_valoracm"
                    placeholder="R$ Patrimônio Destinado"
                    type="text"
                    inputProps={{
                      "aria-label": "description",
                    }}
                    inputType="text"
                    prefix="R$ "
                    precision="2"
                    thousandSeparator={"."}
                    decimalSeparator={","}
                    value={this.state.txtPatrimonioDestinado}
                    onChangeEvent={(event) =>
                      this.setState({
                        txtPatrimonioDestinado: event.target.value,
                      })
                    }
                  />
                </div>
                <TooltipRestore
                  disableFocusListener
                  disableTouchListener
                  title="Retornar ao valor de Patrimônio destinado"
                >
                  <Link onClick={() => this.obterPatrimonioNuvem()}>
                    <i
                      class="fas fa-fast-backward"
                      style={{
                        textAlign: "start",
                        background: "white",
                        color: "#03989e",
                        fontSize: "20px",
                        paddingTop: "0px",
                        paddingLeft: "5px",
                        cursor: "info",
                        marginRight: 15,
                      }}
                    ></i>
                  </Link>
                </TooltipRestore>
                <TooltipRestore
                  disableFocusListener
                  disableTouchListener
                  title="Valor do Patrimônio destinado ao Plano. Altere esse valor no menu Financeiro -> Destinação de Patrimônio"
                >
                  <Link>
                    <i
                      class="fas fa-info-circle"
                      style={{
                        textAlign: "start",
                        background: "white",
                        color: "#03989e",
                        fontSize: "20px",
                        paddingTop: "0px",
                        paddingLeft: "5px",
                        cursor: "info",
                        marginRight: 15,
                      }}
                    ></i>
                  </Link>
                </TooltipRestore>
                para minha aposentadoria.
              </p>

              <p>
                No momento, meus gastos mensais atuais são
                <div
                  className="MuiInputBase-root MuiInput-root MuiInput-underline"
                  style={{ marginLeft: 15, maxWidth: 300 }}
                >
                  <CurrencyInput
                    className="MuiInputBase-input MuiInput-input"
                    id="aps_gastosAtuais"
                    placeholder="R$ Gastos Atuais"
                    inputProps={{ "aria-label": "description" }}
                    value={this.state.txtGastosAtuais}
                    onChangeEvent={this.handleGastosAtuais}
                    inputType="text"
                    prefix="R$ "
                    precision="2"
                    thousandSeparator={"."}
                    decimalSeparator={","}
                  />
                  <TooltipRestore
                    disableFocusListener
                    disableTouchListener
                    title="Retornar ao valor de Gastos Mensais"
                  >
                    <Link onClick={() => this.obterGastoNuvem()}>
                      <i
                        class="fas fa-fast-backward"
                        style={{
                          textAlign: "start",
                          background: "white",
                          color: "#03989e",
                          fontSize: "20px",
                          paddingTop: "0px",
                          paddingLeft: "5px",
                          cursor: "info",
                          marginRight: 15,
                        }}
                      ></i>
                    </Link>
                  </TooltipRestore>

                  <TooltipRestore
                    disableFocusListener
                    disableTouchListener
                    title="Valor da sua Despesa mensal estimada"
                  >
                    <Link>
                      <i
                        class="fas fa-info-circle"
                        style={{
                          textAlign: "start",
                          background: "white",
                          color: "#03989e",
                          fontSize: "20px",
                          paddingTop: "0px",
                          paddingLeft: "5px",
                          cursor: "info",
                          marginRight: 15,
                        }}
                      ></i>
                    </Link>
                  </TooltipRestore>
                </div>
                e na aposentadoria, corrigidos pela inflação, serão{" "}
                <span style={{ marginRight: 15 }}></span>
                <div
                  className="MuiInputBase-root MuiInput-root MuiInput-underline"
                  style={{ marginRight: 15, maxWidth: 220 }}
                >
                  <CurrencyInput
                    className="MuiInputBase-input MuiInput-input"
                    id="aps_custoFut"
                    placeholder="R$ Gastos Corrigidos"
                    inputProps={{ "aria-label": "description" }}
                    value={this.state.txtGastosCorrigidos}
                    onChangeEvent={this.handleGastosCorrigidos}
                    prefix="R$ "
                    thousandSeparator={"."}
                    decimalSeparator={","}
                    precision="2"
                  />
                  <TooltipRestore
                    disableFocusListener
                    disableTouchListener
                    title="Valor dos seus gastos atuais quando se aposentar, atualizados com a inflação"
                  >
                    <Link>
                      <i
                        class="fas fa-info-circle"
                        style={{
                          textAlign: "start",
                          background: "white",
                          color: "#03989e",
                          fontSize: "20px",
                          paddingTop: "0px",
                          paddingLeft: "5px",
                          cursor: "info",
                          marginRight: 15,
                        }}
                      ></i>
                    </Link>
                  </TooltipRestore>
                </div>
                .
              </p>

              <p className="text-justify">
                Gostaria de permanecer com minha independência financeira até
                <Input
                  style={{ marginLeft: 15, width: 200 }}
                  id="aps_usofruto"
                  placeholder="Idade estimada Usufruto"
                  type="number"
                  inputProps={{
                    "aria-label": "description",
                    min: "1",
                    max: "150",
                  }}
                  value={this.state.txtIdadeUsofruto}
                  onChange={this.handleUsoFruto}
                />
                <TooltipRestore
                  disableFocusListener
                  disableTouchListener
                  title="Idade até onde pretendo usufruir da minha aposentadoria"
                >
                  <Link>
                    <i
                      class="fas fa-info-circle"
                      style={{
                        textAlign: "start",
                        background: "white",
                        color: "#03989e",
                        fontSize: "20px",
                        paddingTop: "0px",
                        paddingLeft: "5px",
                        cursor: "info",
                        marginRight: 15,
                      }}
                    ></i>
                  </Link>
                </TooltipRestore>
                anos de idade.
              </p>

              <div className="form-inline" style={{ padding: 0 }}>
                <h3 style={{ paddingTop: 30, paddingRight: 30 }}>
                  A parcela mensal ideal é de
                  <div
                    className="MuiInputBase-root MuiInput-root MuiInput-underline"
                    style={{ marginLeft: 15, marginRight: 15, maxWidth: 250 }}
                  >
                    <CurrencyInput
                      className="MuiInputBase-input MuiInput-input"
                      id="aps_calculo"
                      placeholder="R$ "
                      value={this.state.txtParcelaAposent}
                      onChangeEvent={(digitado) =>
                        this.setState({
                          txtParcelaAposent:
                            digitado.target
                              .value /*parseFloat(digitado.target.value.replace('R$', ''))*/,
                        })
                      }
                      prefix="R$ "
                      thousandSeparator={"."}
                      decimalSeparator={","}
                      precision="2"
                    />

                    {/*Parcela Ideal*/}
                    <TooltipRestore
                      disableFocusListener
                      disableTouchListener
                      title="O Vista calcula a parcela ideal para você"
                    >
                      <Link onClick={() => this.calcularParcelaIdeal()}>
                        <i
                          class="fas fa-piggy-bank"
                          style={{
                            textAlign: "start",
                            background: "white",
                            color: "#03989e",
                            fontSize: "20px",
                            paddingTop: "0px",
                            paddingLeft: "5px",
                            cursor: "pointer",
                          }}
                        ></i>
                      </Link>
                    </TooltipRestore>

                    <TooltipRestore
                      disableFocusListener
                      disableTouchListener
                      title="Essa é a parcela mensal. Você pode testar valores ou deixar que nós façamos o cálculo para você no botão Parcela Ideal"
                    >
                      <Link>
                        <i
                          class="fas fa-info-circle"
                          style={{
                            textAlign: "start",
                            background: "white",
                            color: "#03989e",
                            fontSize: "20px",
                            paddingTop: "0px",
                            paddingLeft: "10px",
                            cursor: "info",
                            marginRight: 0,
                          }}
                        ></i>
                      </Link>
                    </TooltipRestore>
                  </div>
                </h3>

                {/*Calcular Dados*/}
                <TooltipRestore
                  disableFocusListener
                  disableTouchListener
                  title="Calcula o seu Plano de acordo com os dados informados"
                >
                  <Link onClick={() => this.ObterDadosAPIDetalhado()}>
                    <i
                      class="fas fa-sync-alt"
                      style={{
                        textAlign: "start",
                        background: "white",
                        color: "#3cb371",
                        fontSize: "40px",
                        paddingTop: "0px",
                        paddingLeft: "15px",
                        cursor: "pointer",
                      }}
                    ></i>
                  </Link>
                </TooltipRestore>

                {/*Salva Parcela*/}
                <TooltipRestore
                  disableFocusListener
                  disableTouchListener
                  title="Salvar o seu Plano"
                >
                  <Link onClick={() => this.SalvarPlano()}>
                    <i
                      class="fas fa-save"
                      style={{
                        textAlign: "start",
                        background: "white",
                        color: "#9A3DFF",
                        fontSize: "40px",
                        paddingTop: "0px",
                        cursor: "pointer",
                        paddingLeft: "15px",
                      }}
                    ></i>
                  </Link>
                </TooltipRestore>
              </div>
              <a
                style={{ color: "#0f9890" }}
                className="my-4 text-center"
                onClick={() => this.callModalConfig()}
              >
                Configurações Avançadas
              </a>
            </Col>
          </div>
        </>
      );
    }
  };

  render() {
    const item = {
      lineHeight: "2rem",
      paddingLeft: 0,
      cursor: "pointer",
    };
    return (
      <div style={{ padding: "0 5%", marginTop: 20 }}>
        <div className="home-summary-top">
          <Row style={{ paddingLeft: 0 }}>
            <Col lg="12" xl="12">
              <Card>
                <CardHeader
                  className="border-0 form-inline"
                  style={{ padding: "30px 15px 30px 0" }}
                >
                  <Row
                    className="align-items-center"
                    style={{ paddingLeft: 0 }}
                  >
                    <img
                      src={require("../../../assets/img/theme/icone-calendario.png")}
                      alt="Gestão de Planos"
                      className="before-title-img"
                    />
                    <h3 className="mb-0 chart-title">
                      Plano de Independência Financeira
                    </h3>
                  </Row>
                  {this.renderSonhosCadastrados()}
                </CardHeader>
              </Card>
            </Col>
          </Row>
        </div>
        {this.renderPaginaSimulacao()}
        {this.renderRedirect()}
        {this.renderModalConfigAvancado()}
        {this.renderModalAporteExtra()}
        {this.renderDadosDetalhado()}
        {this.state.showModalSonho && <ModalSonho Show={this.ShowModalSonho} />}
      </div>
    );
  }
}

export default Aposentadoria;
