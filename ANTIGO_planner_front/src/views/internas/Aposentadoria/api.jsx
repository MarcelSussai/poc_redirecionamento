import $ from "jquery";

const BASE_API = `${global.server_api_new}${localStorage.getItem(
  "tokenAcesso"
)}`;

const HEADERS = {
  Authorization: `bearer ${localStorage.getItem("access-token")}`,
  "Access-Control-Allow-Origin": "*",
  "Access-Control-Allow-Headers": "Authorization",
  "Access-Control-Allow-Methods": "GET, POST, OPTIONS, PUT, PATCH, DELETE",
  Accept: "application/json",
  "Content-Type": "application/json",
};

export default {
  ExcluirAporteResgate: async (Id, Data) => {
    try {
      global.spinnerShow($);

      const request = await fetch(
        `${BASE_API}/aposentadoria/excluir/aporteresgate`,
        {
          method: "Post",
          headers: HEADERS,
          body: JSON.stringify({ AposentadoriaDadosExtraId: Id, Data: Data }),
        }
      );

      return await request.json();
    } catch (err) {
      // console.log("API::ExcluirAporteResgate: err " + err);
    } finally {
      global.spinnerHide($, [
        document.documentElement.scrollLeft || document.body.scrollLeft,
        document.documentElement.scrollTop || document.body.scrollTop,
      ]);
    }
  },

  SalvarSonho: async (item) => {
    try {
      global.spinnerShow($);

      const request = await fetch(`${BASE_API}/sonho/`, {
        method: "Post",
        headers: HEADERS,
        body: JSON.stringify(item),
      });

      return await request.json();
    } catch (err) {
      // console.log("API::SalvarSonho: err " + err);
    } finally {
      global.spinnerHide($, [
        document.documentElement.scrollLeft || document.body.scrollLeft,
        document.documentElement.scrollTop || document.body.scrollTop,
      ]);
    }
  },
};
