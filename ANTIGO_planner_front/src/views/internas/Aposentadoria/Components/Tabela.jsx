import React from "react";
import { MuiThemeProvider, createMuiTheme } from "@material-ui/core/styles";
import MUIDataTable from "mui-datatables";
import TableRow from "@material-ui/core/TableRow";
import TableCell from "@material-ui/core/TableCell";
import { confirmAlert } from "react-confirm-alert";
import CustomFooter from "./CustomFooter";
import api from "../api";

export default ({ aposentadoriaId, data, reload }) => {
  const theme = createMuiTheme({
    overrides: {
      MuiTableRow: {
        root: {
          "&:nth-of-type(odd)": {
            backgroundColor: "#f8f9fe",
          },
          "& input": {
            background: "transparent",
          },
        },
      },
      MUIDataTableSelectCell: {
        fixedHeaderCommon: {
          backgroundColor: "unset",
        },
      },
      MuiTableCell: {
        root: {
          fontFamily: "unset",
          borderTop: "none",
          padding: "16px 10px",
          textAlign: "center",
        },
        footer: {
          color: "#FFF",
          fontWeight: 700,
        },
      },
      MUIDataTableHeadCell: {
        fixedHeaderYAxis: {
          fontWeight: 700,
        },
        fixedHeaderCommon: {
          zIndex: 0,
        },
      },
      MuiPaper: {
        root: {
          overflow: "hidden",
          margin: 0,
        },
      },
      MuiToolbar: {
        regular: {
          backgroundColor: "#e4eaf4",
          minHeight: "0 !important",
        },
      },
    },
  });

  const customRender = {
    customHeadRender: ({ index, ...column }) => (
      <TableCell
        key={index}
        style={{
          textAlign: "center",
          fontWeight: "bold",
          backgroundColor: "#ffffff",
        }}
      >
        {column.label}
      </TableCell>
    ),
    customBodyRenderFormatCurrency: (value) => (
      <span style={{ color: value < 0 ? "#c21515" : "#000000de" }}>
        {value.toLocaleString("pt-BR", {
          style: "currency",
          currency: "BRL",
        })}
      </span>
    ),
    customBodyRenderFormatPercent: (value) => (
      <span>
        {value.toLocaleString("pt-BR", {
          style: "percent",
          minimumSignificantDigits: 2,
        })}
      </span>
    ),
    customBodyRenderExcluirAporteResgate: (value, { rowData }) => {
      if (value === undefined || value === null || value === 0) {
        return;
      }
      return (
        <>
        <i
          style={{ cursor: "pointer" }}
          className="far fa-trash-alt"
          onClick={() => {
            const options = {
              title: "Confirmação",
              message:
                "Você tem certeza que deseja excluir este aporte/resgate definitivamente?",
              buttons: [
                {
                  label: "Sim",
                  onClick: async () => {
                    let mes = global.retornoMes(rowData[1]);
                    let res = await api.ExcluirAporteResgate(
                      aposentadoriaId,
                      `${rowData[0]}/${mes}/01 00:00:00`
                    );

                    if (res.success) {
                      reload();
                      confirmAlert({
                        title: "Informação",
                        message:
                          "Excluído com sucesso!",
                        buttons: [{ label: "Ok" }],
                      });
                    } else {
                      confirmAlert({
                        title: "Erro ao remover",
                        message:
                          "Não foi possível excluir esse aporte/resgate no momento. Por favor, tente novamente mais tarde.",
                        buttons: [{ label: "Ok" }],
                      });
                    }
                  },
                },
                {
                  label: "Não",
                },
              ],
            };

            confirmAlert(options);
          }}
        ></i>
          &nbsp;&nbsp;
          {value.toLocaleString("pt-BR", {
            style: "currency",
            currency: "BRL",
          })}
        </>
      );
    },
  };

  const columns = {
    root: [
      {
        name: "id",
        label: "Id",
        options: {
          display: false,
          filter: false,
          customHeadRender: (props) => customRender.customHeadRender(props),
        },
      },
      {
        name: "ano",
        label: "Ano",
        options: {
          filter: true,
          customHeadRender: (props) => customRender.customHeadRender(props),
        },
      },
      {
        name: "idade",
        label: "Idade",
        options: {
          filter: true,
          customHeadRender: (props) => customRender.customHeadRender(props),
        },
      },
      {
        name: "patrimonioInicial",
        label: "Patrimônio Inicial",
        options: {
          display: false,
          filter: true,
          customHeadRender: (props) => customRender.customHeadRender(props),
          customBodyRender: (props) =>
            customRender.customBodyRenderFormatCurrency(props),
        },
      },
      {
        name: "patrimonioFinal",
        label: "Patrimônio Fim Período",
        options: {          
          filter: true,
          customHeadRender: (props) => customRender.customHeadRender(props),
          customBodyRender: (props) =>
            customRender.customBodyRenderFormatCurrency(props),
        },
      },
      {
        name: "investimentoRealizado",
        label: "Investimento Realizado",
        options: {
          filter: true,
          customHeadRender: (props) => customRender.customHeadRender(props),
          customBodyRender: (props) =>
            customRender.customBodyRenderFormatCurrency(props),
        },
      },
      {
        name: "aporteMensalCorrigido",
        label: "Investimento(+) | Resgate(-)\nEstimado",
        options: {
          filter: true,
          customHeadRender: (props) => customRender.customHeadRender(props),
          customBodyRender: (props) =>
            customRender.customBodyRenderFormatCurrency(props),
        },
      },      
      {
        name: "aporteExtra",
        label: "Aporte/Resgate Extra",
        options: {
          filter: true,
          customHeadRender: (props) => customRender.customHeadRender(props),
          customBodyRender: (props) =>
            customRender.customBodyRenderFormatCurrency(props),
        },
      },
    ],
    expandable: [
      {
        name: "ano",
        label: "Ano",
        options: {
          display: false,
          filter: false,
          customHeadRender: (props) => customRender.customHeadRender(props),
        },
      },
      {
        name: "mesExtenso",
        label: "Mês",
        options: {
          filter: true,
          customHeadRender: (props) => customRender.customHeadRender(props),
        },
      },
      {
        name: "patrimonioInicioMes",
        label: "Patrimônio Início Mês",
        options: {
          filter: true,
          customHeadRender: (props) => customRender.customHeadRender(props),
          customBodyRender: (props) =>
            customRender.customBodyRenderFormatCurrency(props),
        },
      },
      {
        name: "rentabilidadeNomAc",
        label: "Rentabilidade Nom. Acumulação",
        options: {
          filter: true,
          customHeadRender: (props) => customRender.customHeadRender(props),
          customBodyRender: (props) =>
            customRender.customBodyRenderFormatPercent(props),
        },
      },
      {
        name: "aporteMes",
        label: "Aporte Mês",
        options: {
          filter: true,
          customHeadRender: (props) => customRender.customHeadRender(props),
          customBodyRender: (props) =>
            customRender.customBodyRenderFormatCurrency(props),
        },
      },
      {
        name: "aporteRealizado",
        label: "Aporte Realizado",
        options: {
          filter: true,
          customHeadRender: (props) => customRender.customHeadRender(props),
          customBodyRender: (props) =>
            customRender.customBodyRenderFormatCurrency(props),
        },
      },
      {
        name: "aporteResgExtra",
        label: "Aporte/Resgate Extra",
        options: {
          filter: true,
          customHeadRender: (props) => customRender.customHeadRender(props),
          customBodyRender: (value, tableMeta) =>
            customRender.customBodyRenderExcluirAporteResgate(value, tableMeta),
        },
      },
      {
        name: "resultadoMes",
        label: "Resultado Mês",
        options: {
          filter: true,
          customHeadRender: (props) => customRender.customHeadRender(props),
          customBodyRender: (props) =>
            customRender.customBodyRenderFormatCurrency(props),
        },
      },
    ],
  };

  const options = {
    root: {
      rowsPerPage: 100,
      textLabels: global.textLabels,
      filter: true,
      print: true,
      download: true,
      filterType: "dropdown",
      responsive: "standard",
      selectableRows: false,
      expandableRows: true,
      expandableRowsHeader: false,
      expandableRowsOnClick: true,
      customFooter: (
        count,
        page,
        rowsPerPage,
        changeRowsPerPage,
        changePage,
        textLabels
      ) => {
        return (
          <CustomFooter
            count={count}
            page={page}
            rowsPerPage={rowsPerPage}
            changeRowsPerPage={changeRowsPerPage}
            changePage={changePage}
            textLabels={textLabels}
          />
        );
      },
      renderExpandableRow: (rowData) => {
        const colSpan = rowData.length + 1;
        return (
          <TableRow>
            <TableCell colSpan={colSpan}>
              <MuiThemeProvider theme={theme}>
                <MUIDataTable
                  data={data[rowData[0] - 1].meses}
                  columns={columns.expandable}
                  options={options.expandable}
                />
              </MuiThemeProvider>
            </TableCell>
          </TableRow>
        );
      },
    },
    expandable: {
      search: false,
      filter: false,
      print: true,
      download: true,
      viewColumns: false,
      responsive: "standard",
      selectableRows: false,
      rowsPerPage: 12,
      customFooter: () => {},
    },
  }; 
  
  return (
    <MuiThemeProvider theme={theme}>
      <MUIDataTable data={data} columns={columns.root} options={options.root} />
    </MuiThemeProvider>
  );
};
