import React from "react";
import SkyLight from "react-skylight";
import Select from "react-select";
import { confirmAlert } from "react-confirm-alert";
import { Row, Col } from "reactstrap";
import $ from "jquery";
import api from "../api";
import "react-datepicker/dist/react-datepicker.css";

class ModalSonho extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      accessToken: localStorage.getItem("access-token"),
      empresaId: localStorage.getItem("empresa-id"),
      familiaId: localStorage.getItem("familia-id"),
      statuses: [],
      imagem: require("../../../../assets/img/theme/aposentados.jpg"),
      descricao: null,
      status: 1,
    };
  }

  componentDidMount() {
    this.modal.show();

    var statuses = [];
    global.statusSonho.forEach(function (value, index) {
      statuses.push({
        value: index,
        label: value,
      });
    });
    this.setState({ statuses: statuses });

    var $this = this;
    setTimeout(function () {
      $(document).on("change", "#imagem", function (event) {
        var file = event.target.files[0];
        var reader = new FileReader();

        reader.onload = function (readerEvt) {
          var binaryString = readerEvt.target.result;
          var b64 = btoa(binaryString);
          $this.state.imagem = "data:image/png;base64," + b64;
          $this.mostrarThumb();
        };

        reader.readAsBinaryString(file);
      });
    }, 1000);
  }

  mostrarThumb = () => {
    var novaLinha =
      '<img src="' +
      this.state.imagem +
      '" onclick="document.getElementById(\'imagem\').click()" style="max-width: 100%; max-height: 280px; margin-left: 50%; border-radius: 10px; transform:translateX(-50%);">';
    $("#linha").html(novaLinha);
  };

  salvar = async () => {
    if (this.state.descricao == "" || this.state.descricao == null) {
      confirmAlert({
        title: "Informação",
        message: "Dados em branco. Favor verifique.",
        buttons: [
          {
            label: "Ok",
          },
        ],
      });
    } else {
      var item = {
        familiaId:
          this.state.familiaId !== null ? parseInt(this.state.familiaId) : null,
        empresaId:
          this.state.empresaId !== null ? parseInt(this.state.empresaId) : null,
        descricao: this.state.descricao,
        imagem: this.state.imagem,
        tipo: "sonho",
        categoriasonho: global.idSonhoAposentadoria,
        statusSonho: this.state.status !== null ? this.state.status : 0,
      };
      console.log(item);
      let res = await api.SalvarSonho(item);
      console.log(res);
      if (res.success) {
        sessionStorage.setItem("idAposentadoria", res.singleResult.id);
        window.location.reload();
      } else {
        confirmAlert({
          title: "Erro ao Salvar Sonho",
          message: "erro",
          buttons: [
            {
              label: "Ok",
            },
          ],
        });
      }
    }
  };

  render() {
    return (
      <div>
        <SkyLight
          ref={(ref) => (this.modal = ref)}
          afterClose={() => this.props.Show(false)}
          dialogStyles={{ borderRadius: "1rem", padding: "2rem" }}
          title={
            <>
              <div className="pop-up-title">
                <i className="ni ni-check-bold"></i>
                <h2>Cadastro de Planos & Sonhos</h2>
              </div>
            </>
          }
        >
          <Row>
            <Col lg="6" xl="6">
              <div>
                <label>Descrição *</label>
                <input
                  type="text"
                  id="nome"
                  value={this.state.descricao}
                  style={{ paddingLeft: 24, color: "#8898aa" }}
                  onChange={(e) => this.setState({ descricao: e.target.value })}
                ></input>
              </div>
            </Col>
            <Col lg="6" xl="6">
              <div>
                <label>Status *</label>
                <Select
                  id={"select-status"}
                  onChange={(selectedOption) =>
                    this.setState({ status: selectedOption.value })
                  }
                  className="select-component"
                  options={this.state.statuses}
                  defaultValue={
                    this.state.statuses != null ? this.state.statuses[1] : ""
                  }
                  placeholder="Selecione..."
                  value={
                    this.state.statuses != null && this.state.status != null
                      ? this.state.statuses.find((op) => {
                          if ($.isNumeric(this.state.status)) {
                            //ID
                            return (
                              (op.value != null
                                ? op.value.toString()
                                : null) ===
                              (this.state.status != null
                                ? this.state.status.toString()
                                : null)
                            );
                          } else if (
                            op.value != null &&
                            this.state.status != null
                          ) {
                            return op.value.id === this.state.status.id;
                          } else {
                            //TEXTO
                            return op.label === this.state.status;
                          }
                        })
                      : this.state.statuses[1]
                  }
                />
              </div>
            </Col>
            <Col lg="12" xl="12">
              <div>
                <label
                  style={{ textAlign: "center", fontSize: 14, marginBottom: 0 }}
                >
                  <b>Imagem:</b> (clique sobre a imagem para trocar a foto)
                </label>
                <input
                  type="file"
                  id="imagem"
                  onChange={(e) => {
                    this.setState({ imagem: e.target.value });
                  }}
                  style={{ position: "fixed", left: "-4000px" }}
                ></input>
              </div>
              <div>
                <div id="linha">
                  {this.state.imagem != null && this.mostrarThumb()}
                </div>
              </div>
            </Col>
          </Row>
          <Row>
            <Col
              lg="12"
              xl="12"
              style={{ display: "flex", justifyContent: "center" }}
            >
              <button
                className="general-button aux-button dark with-left-margin nav-item"
                onClick={() => this.salvar()}
              >
                Salvar
              </button>
            </Col>
          </Row>
        </SkyLight>
      </div>
    );
  }
}

export default ModalSonho;
