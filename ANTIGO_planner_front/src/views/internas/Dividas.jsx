import React from "react";
import Select from "react-select";
import { Redirect } from "react-router-dom";
import { CircularProgress, Typography } from "@material-ui/core";
import MUIDataTable from "mui-datatables";
import axios from "axios";
import $ from "jquery";
import { confirmAlert } from "react-confirm-alert";
import SkyLight from "react-skylight";
import Datepicker, { registerLocale } from "react-datepicker";
import MaskedInput from "react-maskedinput";
import br from "date-fns/locale/pt-BR";
import "react-datepicker/dist/react-datepicker.css";
import CurrencyInput from "react-currency-input";
import { createMuiTheme, MuiThemeProvider } from "@material-ui/core/styles";

import CustomFooter from "./CustomFooter.jsx";

import { Col, Row, Card, CardHeader } from "reactstrap";

registerLocale("br", br);

class Dividas extends React.Component {
  moment = require("moment");

  state = {
    originalData: [],
    data: [],
    item: {},
    passoAtual: 1,
    statusDivida: [],
    amortizacao: [],
    indexador: [],
    patrimonios: [],
    sumario: [],
  };

  getMuiTheme = () =>
    createMuiTheme({
      overrides: {
        MuiTableRow: {
          root: {
            "&:nth-of-type(odd)": {
              backgroundColor: "#f8f9fe",
            },
          },
        },
        MUIDataTableSelectCell: {
          fixedHeaderCommon: {
            backgroundColor: "unset",
          },
        },
        MuiTableCell: {
          root: {
            fontFamily: "unset",
            borderTop: "none",
            padding: "16px 10px",
          },
          footer: {
            color: "#FFF",
            fontWeight: 700,
          },
        },
        MUIDataTableHeadCell: {
          fixedHeaderYAxis: {
            fontWeight: 700,
          },
        },
        MuiPaper: {
          root: {
            overflow: "hidden",
            margin: 0,
          },
        },
        MuiToolbar: {
          regular: {
            backgroundColor: "#e4eaf4",
          },
        },
      },
    });

  setRedirect = () => {
    this.setState({
      redirect: true,
    });
  };

  renderRedirect = () => {
    if (this.state.redirect) {
      return <Redirect to="/auth/login" />;
    }
  };

  _next = () => {
    let passoAtual = this.state.passoAtual;

    passoAtual += 1;

    this.setState({
      passoAtual: passoAtual,
    });
  };

  _prev = () => {
    let passoAtual = this.state.passoAtual;

    passoAtual -= 1;

    this.setState({
      passoAtual: passoAtual,
    });
  };

  componentDidMount() {
    this.setState(
      {
        accessToken: localStorage.getItem("access-token"),
        empresaId: localStorage.getItem("empresa-id"),
        familiaId: localStorage.getItem("familia-id"),
        planejadorId: localStorage.getItem("planejador-id"),
      },
      function () {
        if (this.state.accessToken == null) {
          this.setRedirect();
          return;
        }

        this.getData();
        this.getPatrimonios();

        var indexador = this.state.indexador;
        var amortizacao = this.state.amortizacao;
        var statusDivida = this.state.statusDivida;

        global.indexador.forEach(function (value, index) {
          indexador.push({
            value: index,
            label: value,
          });
        });

        global.amortizacao.forEach(function (value, index) {
          amortizacao.push({
            value: index,
            label: value,
          });
        });

        global.statusDivida.forEach(function (value, index) {
          statusDivida.push({
            value: index,
            label: value,
          });
        });

        this.setState({
          indexador: indexador,
          amortizacao: amortizacao,
          statusDivida: statusDivida,
        });

        global.mostrarAjudaDaPaginaAutomaticamente(this);
      }
    );
  }

  setRedirect = () => {
    this.setState({
      redirect: true,
    });
  };

  renderRedirect = () => {
    if (this.state.redirect) {
      return <Redirect to="/auth/login" />;
    }
  };

  getData = () => {
    if (this.state.interval != null) {
      clearInterval(this.state.interval);
    }

    //var url = global.server_api + 'api/divida/familia/' + this.state.familiaId + '/filtro';
    var url =
      global.server_api_new +
      global.apiToken +
      "/divida/familia/" +
      this.state.familiaId +
      "/filtro";

    var filtro = {
      Filtro: this.state.textoDigitado,
      Ordenacao:
        this.state.sortField != null ? this.state.sortField : "Data Desc",
    };

    var config = {
      headers: {
        Authorization: "bearer " + this.state.accessToken,
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Headers": "Authorization",
        "Access-Control-Allow-Methods":
          "GET, POST, OPTIONS, PUT, PATCH, DELETE",
      },
    };

    var currentScroll = [
      document.documentElement.scrollLeft || document.body.scrollLeft,
      document.documentElement.scrollTop || document.body.scrollTop,
    ];

    global.spinnerShow($);

    axios.post(url, filtro, config).then((res) => {
      var data = [];

      var totalParcelas = 0;
      var totalSaldoDevedor = 0;

      if (res.data.results != null) {
        data = res.data.results;

        data.forEach((item) => {
          totalParcelas += item.valorAtualParcela;
          totalSaldoDevedor += item.saldoDevedor;
        });
      }

      totalParcelas =
        "Valor Total de Parcelas: " +
        totalParcelas
          .toLocaleString("pt-BR", { style: "currency", currency: "BRL" })
          .replace("R$", "");
      totalSaldoDevedor =
        "Saldo Devedor Total: " +
        totalSaldoDevedor
          .toLocaleString("pt-BR", { style: "currency", currency: "BRL" })
          .replace("R$", "");

      this.setState(
        {
          data: data,
          originalData: data,
          sumario: [totalParcelas, totalSaldoDevedor],
        },
        function () {
          global.spinnerHide($, currentScroll);
        }
      );
    });
  };

  getPatrimonios = () => {
    //var url = global.server_api + 'api/patrimonio/familia/' + this.state.familiaId + '/filtro';
    var url =
      global.server_api_new +
      global.apiToken +
      "/patrimonio/familia/" +
      this.state.familiaId +
      "/filtro";

    var config = {
      headers: {
        Authorization: "bearer " + this.state.accessToken,
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Headers": "Authorization",
        "Access-Control-Allow-Methods":
          "GET, POST, OPTIONS, PUT, PATCH, DELETE",
      },
    };

    axios.post(url, config).then((res) => {
      var data = [
        {
          label: "Selecione...",
          value: null,
        },
      ];

      if (res.data.results != null) {
        res.data.results.forEach((mp) => {
          if (mp.tipoPatrimonio !== 3) {
            data.push({
              value: mp.id,
              label: mp.descricao,
            });
          }
        });
      }

      this.setState({ patrimonios: data });
    });
  };

  editarRegistro = (rowData) => {
    console.log("rowData", rowData);

    var id = rowData[0];

    if (id == null) return;

    var items = this.state.data.filter(function (temp) {
      return temp.id === id;
    });

    if (items != null && items.length > 0) {
      var item = items[0];

      for (var prop in item) {
        if (item[prop] == null) {
          delete item[prop];
          continue;
        }
      }

      delete item.familiaId;
      delete item.empresaId;

      if (item.patrimonio != null) {
        item.patrimonioId = item.patrimonio.id;
      }

      console.log("item para edicao", item);

      this.setState({ item: item }, function () {
        global.showModal(this.modal);
      });
    }
  };

  salvar = () => {
    //var url = global.server_api + 'api/divida' + (this.state.item.id != null ? "/" + this.state.item.id: "");
    var url =
      global.server_api_new +
      global.apiToken +
      "/divida" +
      (this.state.item.id != null ? "/" + this.state.item.id : "");

    var config = {
      headers: {
        Authorization: "bearer " + this.state.accessToken,
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Headers": "Authorization",
        "Access-Control-Allow-Methods":
          "GET, POST, OPTIONS, PUT, PATCH, DELETE",
      },
    };

    var currentScroll = [
      document.documentElement.scrollLeft || document.body.scrollLeft,
      document.documentElement.scrollTop || document.body.scrollTop,
    ];

    global.spinnerShow($);

    var item = this.state.item;

    item.empresaId = this.state.empresaId;
    item.familiaId = this.state.familiaId;

    if (this.state.patrimonioId != null) {
      item.patrimonioId = this.state.patrimonioId;
    }

    axios.post(url, item, config).then((res) => {
      global.spinnerHide($, currentScroll);

      if (res.data.success === true) {
        window.location.reload();
      } else {
        console.log(res.data.exception);
        const options = {
          title: "Erro ao salvar dívida",
          message: res.data.exception.Message,
          buttons: [
            {
              label: "Ok",
            },
          ],
        };

        confirmAlert(options);
      }
    });
  };

  setItemOnState = (key, value, callback) => {
    var item = this.state.item;

    item[key] = value;

    if (callback) this.setState({ item: item }, callback);
    else this.setState({ item: item });
  };

  gerarDatePicker = (id, fieldKey, defaultDate) => {
    var currentDate = null;

    var item = this.state.item;

    var value = item[fieldKey];

    //date type
    if (value != null) {
      //string vinda da edicao
      if (typeof value === "string") {
        var temp = this.moment(value, "YYYY-MM-DDThh:mm:ss");
        currentDate = temp.toDate();
        // this.setItemOnState(fieldKey, temp);
      }
      //data vinda da selecao atraves do calendario
      else {
        currentDate = value;
      }
    }
    //momentjs date
    else if (defaultDate != null) {
      currentDate = defaultDate.toDate();
      // this.setItemOnState(fieldKey, currentDate);
    }
    //now (date)
    else {
      currentDate = new Date();
      // this.setItemOnState(fieldKey, currentDate);
    }

    return (
      <Datepicker
        locale="br"
        id={id}
        dateFormat="dd/MM/yyyy"
        selected={currentDate}
        showYearDropdown
        dateFormatCalendar="MMMM"
        scrollableYearDropdown
        yearDropdownItemNumber={5}
        onChange={(date) => this.setItemOnState(fieldKey, date)}
        customInput={<MaskedInput mask="11/11/1111" />}
      />
    );
  };

  preparaDataParaGrid = (dataString) => {
    var data = this.moment(dataString, "YYYY-MM-DDThh:mm:ss");
    return data.format("DD/MM/YYYY");
  };

  limparSelecao = () => {
    this.setState({
      item: {},
      passoAtual: 1,
    });
  };

  removerRegistros = (rowsDeleted) => {
    const options = {
      title: "Exclusão",
      message:
        "Tem certeza que deseja excluir? Este procedimento é irreversível!",
      buttons: [
        {
          label: "Sim",
          onClick: () => {
            //console.log(rowsDeleted.data);
            var ids = [];

            for (var key in rowsDeleted.data) {
              ids.push(this.state.data[rowsDeleted.data[key].dataIndex].id);
            }

            if (ids.length > 0) {
              //var url = global.server_api + 'api/divida/bulk/' + ids.join(",");
              var url =
                global.server_api_new +
                global.apiToken +
                "/divida/bulk/" +
                ids.join(",");

              var config = {
                headers: {
                  Authorization: "bearer " + this.state.accessToken,
                  "Access-Control-Allow-Origin": "*",
                  "Access-Control-Allow-Headers": "Authorization",
                  "Access-Control-Allow-Methods":
                    "GET, POST, OPTIONS, PUT, PATCH, DELETE",
                },
              };

              axios.delete(url, {}, config).then((res) => {
                const result = res.data;

                if (result.success && result.affectedResults > 0) {
                  this.getData();
                } else {
                  alert("Erro ao remover itens. " + res.data.exception.Message);
                  console.log(res.data.exception);
                }
              });
            }
          },
        },
        {
          label: "Não",
        },
      ],
    };

    confirmAlert(options);

    return false;
  };

  filtroPorTexto = (valorDigitado) => {
    const textoDigitado = valorDigitado;

    this.setState({ textoDigitado }, function () {
      if (this.state.interval != null) {
        clearInterval(this.state.interval);
      }

      const $this = this;

      const interval = setInterval(function () {
        console.log($this.state.textoDigitado);
        $this.getData();
      }, 1000);

      this.setState({ interval });
    });
  };

  mostrarPopup = () => (
    <div className="planosSonhos">
      <SkyLight
        ref={(ref) => (this.modal = ref)}
        transitionDuration={0}
        beforeOpen={this.preCarregarParaEdicao}
        afterClose={this.limparSelecao}
        title={
          <>
            <div className="pop-up-title">
              <i className="ni ni-check-bold"></i>
              <h2>Cadastro de Dívidas</h2>
            </div>
          </>
        }
      >
        <Row
          style={
            this.state.passoAtual === 1
              ? { display: "flex" }
              : { display: "none" }
          }
        >
          <Col lg="6" xl="6">
            <div>
              <label>Identificação *</label>
              <input
                type="text"
                value={this.state.item.identificacao}
                onChange={(e) =>
                  this.setItemOnState("identificacao", e.target.value)
                }
              ></input>
            </div>
          </Col>
          <Col lg="6" xl="6">
            <div>
              <label>Credor *</label>
              <input
                type="text"
                value={this.state.item.favorecido}
                onChange={(e) =>
                  this.setItemOnState("favorecido", e.target.value)
                }
              ></input>
            </div>
          </Col>
          <Col lg="6" xl="6">
            <div>
              <label>Data da Dívida *</label>
              {this.gerarDatePicker("data-divida", "dataDivida", this.moment())}
            </div>
          </Col>
          <Col lg="6" xl="6">
            <div>
              <label>Valor *</label>
              <CurrencyInput
                id="valor"
                value={
                  this.state.item.valorFormatado != null
                    ? this.state.item.valorFormatado
                    : this.state.item.valor
                }
                thousandSeparator={"."}
                decimalSeparator={","}
                precision="2" //prefix={'R$ '}
                onChangeEvent={(event, formattedValue, value) => {
                  this.setItemOnState("valor", value);
                  this.setItemOnState("valorFormatado", formattedValue);
                }}
              />
            </div>
          </Col>
          {/*
          <Col lg="6" xl="6">
            <div>
              <label>Motivo *</label>
              <input type="text" value={this.state.item.motivo} 
                onChange={e => this.setItemOnState('motivo', e.target.value)}></input>
            </div>
          </Col>
          */}
          <Col lg="6" xl="6">
            <div>
              <label>Status da Dívida *</label>
              <Select
                onChange={(selectedOption) =>
                  this.setItemOnState("statusDivida", selectedOption.value)
                }
                className="select-component"
                options={this.state.statusDivida}
                defaultValue={this.state.statusDivida[0]}
                placeholder="Selecione..."
                value={
                  this.state.item.statusDivida != null
                    ? this.state.statusDivida.find((op) => {
                        return op.value === this.state.item.statusDivida;
                      })
                    : null
                }
              />
            </div>
          </Col>
          <Col lg="12" xl="12">
            <label>
              Os valores informados anteriormente{" "}
              <b>não são calculados automaticamente</b>, porque dependem de
              outros fatores que dependem de cada instituição financeira.
            </label>
          </Col>
        </Row>
        <Row
          style={
            this.state.passoAtual === 2
              ? { display: "flex" }
              : { display: "none" }
          }
        >
          <Col lg="6" xl="6">
            <div>
              <label>Percentual (CET a.m.)</label>
              <CurrencyInput
                value={
                  this.state.item.percentualFormatado != null
                    ? this.state.item.percentualFormatado
                    : this.state.item.percentual
                }
                thousandSeparator={"."}
                decimalSeparator={","}
                suffix={"%"}
                onChangeEvent={(event, formattedValue, value) => {
                  this.setItemOnState("percentual", value);
                  this.setItemOnState("percentualFormatado", formattedValue);
                }}
              />
            </div>
          </Col>
          <Col lg="6" xl="6">
            <div>
              <label>Indexador</label>
              <Select
                onChange={(selectedOption) =>
                  this.setItemOnState("indexador", selectedOption.value)
                }
                className="select-component"
                options={this.state.indexador}
                defaultValue={this.state.indexador[0]}
                placeholder="Selecione..."
                value={
                  this.state.item.indexador != null
                    ? this.state.indexador.find((op) => {
                        return op.value === this.state.item.indexador;
                      })
                    : null
                }
              />
            </div>
          </Col>
          {/* <Col lg="4" xl="4">
            <div>
              <label>Percentual Pré</label>
              <CurrencyInput value={this.state.item.percentualPreFormatado != null ? this.state.item.percentualPreFormatado : this.state.item.percentualPre} 
                thousandSeparator={'.'}
                decimalSeparator={','} 
                suffix={'%'} 
                onChangeEvent={(event, formattedValue, value) => {
                  this.setItemOnState('percentualPre', value);
                  this.setItemOnState('percentualPreFormatado', formattedValue);
                }}  
              />
            </div>
          </Col> */}
          <Col lg="6" xl="6">
            <div>
              <label>Total de Parcelas</label>
              <input
                type="number"
                value={this.state.item.parcelas}
                onChange={(e) =>
                  this.setItemOnState("parcelas", e.target.value)
                }
              ></input>
            </div>
          </Col>
          <Col lg="6" xl="6">
            <div>
              <label>Parcelas Restantes</label>
              <input
                type="number"
                value={this.state.item.parcelasRestantes}
                onChange={(e) =>
                  this.setItemOnState("parcelasRestantes", e.target.value)
                }
              ></input>
            </div>
          </Col>
          <Col lg="6" xl="6">
            <div>
              <label>Valor Atual da Parcela</label>
              <CurrencyInput
                value={
                  this.state.item.valorAtualParcelaFormatado != null
                    ? this.state.item.valorAtualParcelaFormatado
                    : this.state.item.valorAtualParcela
                }
                thousandSeparator={"."}
                decimalSeparator={","}
                precision="2" //prefix={'R$ '}
                onChangeEvent={(event, formattedValue, value) => {
                  this.setItemOnState("valorAtualParcela", value);
                  this.setItemOnState(
                    "valorAtualParcelaFormatado",
                    formattedValue
                  );
                }}
              />
            </div>
          </Col>
          <Col lg="6" xl="6">
            <div>
              <label>Amortização</label>
              <Select
                onChange={(selectedOption) =>
                  this.setItemOnState("amortizacao", selectedOption.value)
                }
                className="select-component"
                options={this.state.amortizacao}
                defaultValue={this.state.amortizacao[0]}
                placeholder="Selecione..."
                value={
                  this.state.item.amortizacao != null
                    ? this.state.amortizacao.find((op) => {
                        return op.value === this.state.item.amortizacao;
                      })
                    : null
                }
              />
            </div>
          </Col>
          <Col lg="12" xl="12">
            <label>
              Os valores informados anteriormente{" "}
              <b>não são calculados automaticamente</b>, porque dependem de
              outros fatores que dependem de cada instituição financeira.
            </label>
          </Col>
        </Row>
        <Row
          style={
            this.state.passoAtual === 3
              ? { display: "flex" }
              : { display: "none" }
          }
        >
          <Col lg="6" xl="6">
            <div>
              <label>Saldo Devedor</label>
              <CurrencyInput
                value={
                  this.state.item.saldoDevedorFormatado != null
                    ? this.state.item.saldoDevedorFormatado
                    : this.state.item.saldoDevedor
                }
                thousandSeparator={"."}
                decimalSeparator={","}
                precision="2" //prefix={'R$ '}
                onChangeEvent={(event, formattedValue, value) => {
                  this.setItemOnState("saldoDevedor", value);
                  this.setItemOnState("saldoDevedorFormatado", formattedValue);
                }}
              />
            </div>
          </Col>
          <Col lg="6" xl="6">
            <div>
              <label>Saldo Atualizado Em</label>
              {this.gerarDatePicker(
                "data-saldo-devedor",
                "dataDoSaldoDevedor",
                this.moment()
              )}
            </div>
          </Col>
          <Col lg="6" xl="6">
            <div>
              <label>Patrimônio</label>
              <Select
                onChange={(selectedOption) =>
                  this.setItemOnState("patrimonioId", selectedOption.value)
                }
                className="select-component"
                options={this.state.patrimonios}
                defaultValue={this.state.patrimonios[0]}
                placeholder="Selecione..."
                value={
                  this.state.item.patrimonioId != null
                    ? this.state.patrimonios.find((op) => {
                        return op.value === this.state.item.patrimonioId;
                      })
                    : null
                }
              />
            </div>
          </Col>
          <Col lg="6" xl="6">
            <div>
              <label>Observações</label>
              <textarea
                onChange={(e) => {
                  this.setItemOnState("observacoes", e.target.value);
                }}
                value={this.state.item.observacoes}
              ></textarea>
            </div>
          </Col>
          <Col lg="12" xl="12">
            <label>
              Os valores informados anteriormente{" "}
              <b>não são calculados automaticamente</b> porque dependem de
              outros fatores que dependem de cada instituição financeira.
            </label>
          </Col>
        </Row>
        <Row>
          <Col lg="12" xl="12">
            {this.state.passoAtual === 3 ? (
              <button
                onClick={() => this.salvar()}
                className="featured-button"
                style={{
                  float: "right",
                  backgroundImage:
                    "url(" +
                    require("../../assets/img/theme/botao-destaque.png") +
                    ")",
                }}
              >
                Salvar
              </button>
            ) : (
              <></>
            )}

            {this.state.passoAtual < 3 ? (
              <button
                onClick={() => this._next()}
                className="aux-button"
                style={{ float: "right", marginRight: "10px" }}
              >
                Próximo
              </button>
            ) : (
              <></>
            )}

            {this.state.passoAtual > 1 ? (
              <button
                onClick={() => this._prev()}
                className="aux-button"
                style={{ float: "right", marginRight: "10px" }}
              >
                Anterior
              </button>
            ) : (
              <></>
            )}
          </Col>
        </Row>
      </SkyLight>
    </div>
  );

  render() {
    const columns = [
      {
        name: "id",
        options: {
          display: false,
          viewColumns: false,
          filter: false,
        },
      },
      {
        name: "patrimonio.Id",
        options: {
          display: false,
          viewColumns: false,
          filter: false,
        },
      },
      {
        name: "dataDivida",
        label: "Data",
        options: {
          display: true,
          viewColumns: true,
          filter: false,
          customBodyRender: (value) => {
            return <>{value != null ? this.preparaDataParaGrid(value) : ""}</>;
          },
        },
      },
      {
        name: "identificacao",
        label: "Identificação",
        options: {
          display: true,
          viewColumns: true,
          filter: false,
        },
      },
      {
        name: "favorecido",
        label: "Credor",
        options: {
          display: true,
          viewColumns: true,
          filter: false,
        },
      },
      {
        name: "valor",
        label: "Valor",
        options: {
          display: true,
          viewColumns: true,
          filter: false,
          customBodyRender: (value) => {
            return (
              <>
                {value != null
                  ? value
                      .toLocaleString("pt-BR", {
                        style: "currency",
                        currency: "BRL",
                      })
                      .replace("R$", "")
                  : ""}
              </>
            );
          },
        },
      },
      /*{
        name: "motivo", 
        label: "Motivo",
        options: {
          display: true, viewColumns: true, filter: false,
        }
      },*/
      {
        name: "percentual",
        label: "Percentual",
        options: {
          display: false,
          viewColumns: true,
          filter: false,
          customBodyRender: (value) => {
            return <>{value != null ? value + " %" : ""}</>;
          },
        },
      },
      {
        name: "indexador",
        label: "Indexador",
        options: {
          display: false,
          viewColumns: true,
          filter: false,
          customBodyRender: (value) => {
            return <>{value != null ? global.indexador[value] : ""}</>;
          },
        },
      },
      // {
      //   name: "percentualPre",
      //   label: "Percentual Pré",
      //   options: {
      //     display: false, viewColumns: true, filter: false,
      //     customBodyRender: (value) => {
      //       return (
      //         <>{value != null ? value + " %" : ""}</>
      //       );
      //     }
      //   }
      // },
      {
        name: "parcelas",
        label: "Total de Parcelas",
        options: {
          display: false,
          viewColumns: true,
          filter: false,
        },
      },
      {
        name: "valorAtualParcela",
        label: "Valor da Parcela",
        options: {
          display: true,
          viewColumns: true,
          filter: false,
          customBodyRender: (value) => {
            return (
              <>
                {value != null
                  ? value
                      .toLocaleString("pt-BR", {
                        style: "currency",
                        currency: "BRL",
                      })
                      .replace("R$", "")
                  : ""}
              </>
            );
          },
        },
      },
      {
        name: "parcelasRestantes",
        label: "Qtd. de Parcelas Restantes",
        options: {
          display: false,
          viewColumns: true,
          filter: false,
        },
      },
      {
        name: "amortizacao",
        label: "Amortizacao",
        options: {
          display: false,
          viewColumns: true,
          filter: false,
          customBodyRender: (value) => {
            return <>{value != null ? global.amortizacao[value] : ""}</>;
          },
        },
      },
      {
        name: "saldoDevedor",
        label: "Saldo Devedor",
        options: {
          display: true,
          viewColumns: true,
          filter: false,
          customBodyRender: (value) => {
            return (
              <>
                {value != null
                  ? value
                      .toLocaleString("pt-BR", {
                        style: "currency",
                        currency: "BRL",
                      })
                      .replace("R$", "")
                  : ""}
              </>
            );
          },
        },
      },
      {
        name: "dataDoSaldoDevedor",
        label: "Saldo Atualizado Em",
        options: {
          display: false,
          viewColumns: true,
          filter: false,
          customBodyRender: (value) => {
            return <>{value != null ? this.preparaDataParaGrid(value) : ""}</>;
          },
        },
      },
      {
        name: "patrimonio.descricao",
        label: "Patrimônio Associado",
        options: {
          display: false,
          viewColumns: true,
          filter: false,
        },
      },
      {
        name: "observacoes",
        label: "Observações",
        options: {
          display: false,
          viewColumns: true,
          filter: false,
        },
      },
      {
        name: "statusDivida",
        label: "Status",
        options: {
          display: true,
          viewColumns: true,
          filter: false,
          customBodyRender: (value) => {
            return <>{value != null ? global.statusDivida[value] : ""}</>;
          },
        },
      },
    ];
    columns.forEach((item) => {
      if (item.name === this.state.colunaOrdenacao) {
        item.options.sortDirection = this.state.direcaoOrdenacao;
      }
    });

    const { data, isLoading } = this.state;

    const options = {
      textLabels: global.textLabels,
      filter: true,
      filterType: "dropdown",
      responsive: "stacked",
      print: false,
      download: false,
      serverSide: true,
      searchText: this.state.textoDigitado,
      sort: true,
      selectableRows: "multiple",
      // rowsPerPage: 20,
      pagination: false,
      onRowClick: this.editarRegistro,
      onRowsDelete: (rowsDeleted) => {
        this.removerRegistros(rowsDeleted);
        return false;
      },
      onFilterChange: (changedColumn, filterList) => {
        console.log("onFilterChange");
      },
      onColumnSortChange: (changedColumn) => {
        //ASC
        let order = "";
        let direcaoOrdenacao = "asc";

        if (this.state.colunaOrdenacao === changedColumn) {
          if (
            this.state.direcaoOrdenacao != null &&
            this.state.direcaoOrdenacao === "asc"
          ) {
            order = " Desc";
            direcaoOrdenacao = "desc";
          }
        }

        if (changedColumn === "MeioPagamento") {
          changedColumn = "Meio Pagamento";
        }

        this.setState(
          {
            sortField: changedColumn + order,
            colunaOrdenacao: changedColumn,
            direcaoOrdenacao: direcaoOrdenacao,
          },
          () => {
            this.getData();
          }
        );
      },
      onTableChange: (action, tableState) => {
        switch (action) {
          case "changePage":
            this.setState({ numeroPagina: tableState.page + 1 }, () => {
              this.getData();
            });
            break;
          case "search":
            this.filtroPorTexto(tableState.searchText);
            break;
          default:
            break;
        }
      },
      customFooter: (
        count,
        page,
        rowsPerPage,
        changeRowsPerPage,
        changePage,
        textLabels
      ) => {
        return (
          <CustomFooter
            count={count}
            page={page}
            rowsPerPage={rowsPerPage}
            changeRowsPerPage={changeRowsPerPage}
            changePage={changePage}
            textLabels={textLabels}
            data2={this.state.sumario}
            hasPagination={false}
          />
        );
      },
    };

    return (
      <div>
        {this.renderRedirect()}

        <div
          className="home-summary-top"
          style={{ padding: "0 5%", marginTop: 20 }}
        >
          <Row>
            <Col lg="12" xl="12" style={{ marginBottom: "15px" }}>
              <div>
                <Card>
                  <CardHeader
                    className="border-0"
                    style={{ padding: "30px 15px 30px 0" }}
                  >
                    <Row className="align-items-center">
                      <div className="col">
                        <img
                          src={require("../../assets/img/theme/icone-calendario.png")}
                          alt="Dívidas"
                          className="before-title-img"
                        />
                        <h3 className="mb-0 chart-title">Dívidas</h3>
                      </div>
                      <div className="new-entry" style={{ float: "right" }}>
                        <div
                          className="nav-link-icon featured-button"
                          style={{
                            backgroundImage:
                              "url(" +
                              require("../../assets/img/theme/botao-destaque.png") +
                              ")",
                          }}
                          onClick={() => global.showModal(this.modal)}
                        >
                          <span className="nav-link-inner--text">
                            Nova Dívida
                          </span>
                        </div>
                      </div>
                    </Row>
                  </CardHeader>
                </Card>
              </div>
            </Col>
          </Row>
          <MuiThemeProvider theme={this.getMuiTheme()}>
            <MUIDataTable
              id="orcamento-table"
              title={
                <Typography>
                  {isLoading && (
                    <CircularProgress
                      size={24}
                      style={{ marginLeft: 15, position: "relative", top: 4 }}
                    />
                  )}
                </Typography>
              }
              data={data}
              columns={columns}
              options={options}
            />
          </MuiThemeProvider>
          {this.mostrarPopup()}
        </div>
      </div>
    );
  }
}

export default Dividas;
