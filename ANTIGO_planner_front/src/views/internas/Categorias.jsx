import React from "react";
import Select from "react-select";
import axios from "axios";
import { confirmAlert } from "react-confirm-alert";
import "react-confirm-alert/src/react-confirm-alert.css";
import SkyLight from "react-skylight";
import CurrencyInput from "react-currency-input";
import MUIDataTable from "mui-datatables";

// reactstrap components
import { Card, CardHeader, Container, Row, Col, Table } from "reactstrap";

class Categorias extends React.Component {
  moment = require("moment");

  state = {
    activePage: 1,
    itensPerPage: 12,
    pageRangeDisplayed: 5,
    totalItemsCount: 450,
    sortField: null,
    registros: [],
    carregamentoInicial: false,
    interval: null,
    textoDigitado: "",
    orcamentos: [],
    orcamentosCombo: [],
    orcamentosComboFiltrado: [],
    frequencias: [],
    grupos: [],
  };

  camposParaOrdenacao = [
    { value: "Nome", label: "Nome" },
    { value: "Nome Desc", label: "Nome Decrescente" },
    { value: "Orcamento", label: "Orçamento" },
    { value: "Grupo", label: "Grupo de Categorias" },
    { value: "Frequencia", label: "Frequência" },
    { value: "Valor Estimado", label: "Valor Estimado" },
    { value: "Valor Estimado Desc", label: "Valor Estimado Decrescente" },
  ];

  atualizarOrcamentos = () => {
    //var url = global.server_api + 'api/orcamento/filtro';
    var url = global.server_api_new + global.apiToken + "/orcamento/filtro";

    if (this.state.familiaId != null) {
      //url = global.server_api + 'api/orcamento/familia/'+this.state.familiaId+'/filtro';
      url =
        global.server_api_new +
        global.apiToken +
        "/orcamento/familia/" +
        this.state.familiaId +
        "/filtro";
    }

    // if(this.state.planejadorId != null){
    //   url = global.server_api + 'api/orcamento/planejador/'+this.state.planejadorId+'/filtro';
    // }

    var config = {
      headers: {
        Authorization: "bearer " + this.state.accessToken,
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Headers": "Authorization",
        "Access-Control-Allow-Methods":
          "GET, POST, OPTIONS, PUT, PATCH, DELETE",
      },
    };

    axios.post(url, {}, config).then((res) => {
      const orcamentos = res.data.results;
      var orcamentosCombo = [
        {
          label: "Selecione...",
          value: null,
        },
      ];

      if (orcamentos != null) {
        orcamentos.forEach((mp, key) => {
          orcamentosCombo.push({
            value: mp,
            label: mp.nome,
          });
        });
      }

      this.setState({ orcamentos });
      this.setState({ orcamentosCombo });
    });
  };

  mapearFrequenciaPeloId = (id) => {
    var frequencia = this.state.frequencias.filter(function (item) {
      return parseInt(item.value) === parseInt(id);
    });
    return frequencia != null && frequencia.length > 0
      ? frequencia[0].label
      : "";
  };

  mapearGrupoPeloId = (id) => {
    var grupo = this.state.grupos.filter(function (item) {
      return parseInt(item.value) === parseInt(id);
    });
    return grupo != null && grupo.length > 0 ? grupo[0].label : "";
  };

  montarDropDown = (itens, id, changeMethod, selectedValue, type) => (
    <Select
      id={id}
      onChange={changeMethod}
      className="select-component"
      options={itens}
      defaultValue={itens != null ? itens[0] : ""}
      placeholder="Selecione..."
      value={
        itens != null && selectedValue != null
          ? itens.find((op) => {
              if (type === "enum") {
                return (
                  (op.value != null ? op.value.toString() : null) ===
                  (selectedValue != null ? selectedValue.toString() : null)
                );
              } else if (op.value != null && selectedValue != null) {
                return op.value.id === selectedValue.id;
              }
              return false;
            })
          : null
      }
    />
  );

  mudarOrdenacao = (selectedOption) => {
    const sortField = selectedOption.value;

    this.setState({ sortField }, function () {
      this.atualizarRegistros();
    });
  };

  mostrarLista = () => {
    return (
      <>
        <Card className="shadow list-entries">
          <Table>
            <th width="15"></th>
            <th width="15"></th>
            <th>Nome</th>
            <th>Grupo</th>
            <th>Orçamento</th>
            <th>Frequência</th>
            {/*Campos exclusivos para familia*/}
            {this.state.familiaId != null &&
              this.camposDoTituloExclusivosParaFamilia()}
            {/*Dados*/}
            {this.state.registros != null &&
              this.mostrarDados(this.state.registros)}
          </Table>
        </Card>
      </>
    );
  };

  mostrarListMUI = () => {
    const options = {
      textLabels: global.textLabels,
      filter: true,
      filterType: "dropdown",
      responsive: "stacked",
      print: false,
      download: false,
      rowsPerPage: 15,
      onRowsDelete: (rowsDeleted) => {
        this.removerRegistros(rowsDeleted);
        return false;
      },
      onRowClick: (rowData) => {
        this.editar(rowData[0]);
      },
    };
    const columns = [
      {
        name: "id",
        options: {
          display: false,
          viewColumns: false,
          filter: false,
        },
      },
      {
        name: "orcamentoId",
        options: {
          display: false,
          viewColumns: false,
          filter: false,
        },
      },
      {
        name: "nome",
        label: "Nome",
      },
      {
        name: "grupo",
        label: "Grupo",
        options: {
          display: true,
          viewColumns: true,
          filter: true,
          customBodyRender: (value) => {
            if (value != null) {
              return <>{this.mapearGrupoPeloId(value)}</>;
            }
            return <></>;
          },
        },
      },
      {
        name: "orcamento",
        label: "Orçamento",
      },
      {
        name: "frequencia",
        label: "Frequência",
        options: {
          display: true,
          viewColumns: true,
          filter: true,
          customBodyRender: (value) => {
            if (value != null) {
              return <>{this.mapearFrequenciaPeloId(value)}</>;
            }
            return <></>;
          },
        },
      },
    ];
    var data = [];
    if (this.state.registros != null) {
      data = this.state.registros.map((item, index) => {
        return {
          id: item.id,
          orcamentoId: item.orcamento.id,
          nome: item.nome,
          grupo: item.grupo,
          frequencia: item.frequencia,
          orcamento: item.orcamento.nome,
        };
      });
    }
    return (
      <div style={{ width: "100%" }}>
        <MUIDataTable data={data} columns={columns} options={options} />
      </div>
    );
  };

  camposDoTituloExclusivosParaFamilia = () => {
    return (
      <>
        <th>Valor Estimado</th>
      </>
    );
  };

  mostrarDados = (registros) => {
    if (registros == null) {
      return <></>;
    }

    return registros.map((item, key) => {
      return (
        <>
          <tr>
            <td>
              <div onClick={() => this.confirmaExcluir(item.id)}>
                <i className="far fa-trash-alt"></i>
              </div>
            </td>
            <td>
              <div onClick={() => this.editar(item.id)}>
                <i className="fas fa-info"></i>
              </div>
            </td>
            <td onClick={() => this.editar(item.id)}>{item.nome}</td>
            <td onClick={() => this.editar(item.id)}>
              {this.mapearGrupoPeloId(item.grupo)}
            </td>
            <td onClick={() => this.editar(item.id)}>{item.orcamento.nome}</td>
            <td onClick={() => this.editar(item.id)}>
              {this.mapearFrequenciaPeloId(item.frequencia)}
            </td>
            {/*Campos exclusivos para familia*/}
            {this.state.familiaId != null &&
              this.camposDosDadosExclusivosParaFamilia(item)}
          </tr>
        </>
      );
    });
  };

  camposDosDadosExclusivosParaFamilia = (item) => {
    return (
      <>
        <td onClick={() => this.editar(item.id)}>
          {item.valorEstimado
            .toLocaleString("pt-BR", { style: "currency", currency: "BRL" })
            .replace("R$", "")}
        </td>
      </>
    );
  };

  atualizarRegistros = () => {
    if (this.state.interval != null) {
      clearInterval(this.state.interval);
    }

    //var url = global.server_api + 'api/categoria/filtro';
    var url = global.server_api_new + global.apiToken + "/categoria/filtro";

    if (this.state.familiaId != null) {
      //url = global.server_api + 'api/categoria/familia/'+this.state.familiaId+'/filtro';
      url =
        global.server_api_new +
        global.apiToken +
        "/categoria/familia/" +
        this.state.familiaId +
        "/filtro";
    }

    // if(this.state.planejadorId != null ){
    //   url = global.server_api + 'api/categoria/planejador/'+this.state.planejadorId+'/filtro';
    // }

    var filtro = {
      Filtro: this.state.textoDigitado,
      Ordenacao: this.state.sortField,
      Frequencia:
        this.state.frequenciaFiltro != null &&
        this.state.frequenciaFiltro !== ""
          ? parseInt(this.state.frequenciaFiltro)
          : null,
      Grupo:
        this.state.grupoFiltro != null && this.state.grupoFiltro !== ""
          ? parseInt(this.state.grupoFiltro)
          : null,
      OrcamentoId:
        this.state.orcamentoFiltro != null
          ? this.state.orcamentoFiltro.id
          : null,
      IncluirOrcamentos: true,
    };

    var config = {
      headers: {
        Authorization: "bearer " + this.state.accessToken,
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Headers": "Authorization",
        "Access-Control-Allow-Methods":
          "GET, POST, OPTIONS, PUT, PATCH, DELETE",
      },
    };

    axios.post(url, filtro, config).then((res) => {
      var registros = [];

      if (res.data.results != null) {
        registros = res.data.results.filter(function (item) {
          //Apenas receitas e despesas
          return (
            item.orcamento.tipoOrcamento === 0 ||
            item.orcamento.tipoOrcamento === 1
          );
        });
      }

      this.setState({ registros });
    });
  };

  confirmaExcluir = (id) => {
    const options = {
      title: "Exclusão de Registro",
      message:
        "Tem certeza que deseja excluir este Registro? Este procedimento é irreversível!",
      buttons: [
        {
          label: "Sim",
          onClick: () => this.excluir(id),
        },
        {
          label: "Não",
        },
      ],
    };

    confirmAlert(options);
  };

  excluir = (id) => {
    //var url = global.server_api + 'api/categoria/' + id;
    var url = global.server_api_new + global.apiToken + "/categoria/" + id;

    if (this.state.familiaId == null) {
      //url = global.server_api + 'api/categoria/padrao/' + id;
      url = global.server_api_new + global.apiToken + "/categoria/padrao/" + id;
    }

    var config = {
      headers: {
        Authorization: "bearer " + this.state.accessToken,
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Headers": "Authorization",
        "Access-Control-Allow-Methods":
          "GET, POST, OPTIONS, PUT, PATCH, DELETE",
      },
    };

    axios.delete(url, {}, config).then((res) => {
      const result = res.data;

      if (result.success && result.affectedResults > 0) {
        this.atualizarRegistros();
      } else {
        alert("Erro ao remover registro. " + res.data.exception.Message);
      }
    });
  };

  editar = (id) => {
    var registroEdicao = this.state.registros.filter(function (item) {
      return item.id === id;
    });

    if (registroEdicao != null && registroEdicao.length > 0) {
      registroEdicao = registroEdicao[0];

      var orcamentoTemp = this.state.orcamentosCombo.filter(function (item) {
        if (item.value != null) {
          return item.value.id === registroEdicao.orcamento.id;
        }
        return false;
      });

      //Seta item edicao
      this.setState(
        {
          nome: registroEdicao.nome,
          grupo: registroEdicao.grupo,
          frequencia: registroEdicao.frequencia,
          valorEstimado: registroEdicao.valorEstimado,
          orcamentoId:
            orcamentoTemp != null && orcamentoTemp.length > 0
              ? orcamentoTemp[0].value
              : null,
          id: registroEdicao.id,
        },
        function () {
          //Abre modal
          global.showModal(this.modal);
        }
      );
    }
  };

  filtroPorTexto = (valorDigitado) => {
    const textoDigitado = valorDigitado.target.value;

    this.setState({ textoDigitado }, function () {
      if (this.state.interval != null) {
        clearInterval(this.state.interval);
      }

      const $this = this;

      const interval = setInterval(function () {
        $this.atualizarRegistros();
      }, 1000);

      this.setState({ interval });
    });
  };

  limparSelecao = () => {
    this.setState({
      nome: "",
      grupo: null,
      frequencia: null,
      valorEstimado: "",
      orcamentoId: null,
      id: null,
    });
  };

  salvar = () => {
    var item = {
      Nome: this.state.nome,
      Grupo: this.state.grupo,
      Frequencia: this.state.frequencia,
      ValorEstimado: this.state.valorEstimado,
      OrcamentoId:
        this.state.orcamentoId != null ? this.state.orcamentoId.id : null,
      FamiliaId:
        this.state.familiaId != null
          ? parseInt(this.state.familiaId)
          : this.state.planejadorId != null
          ? this.state.familiaId2
          : null,
      EmpresaId:
        this.state.empresaId != null ? parseInt(this.state.empresaId) : null,
    };

    //var url = global.server_api + 'api/categoria' + (this.state.id != null ? "/" + this.state.id: "");
    var url =
      global.server_api_new +
      global.apiToken +
      "/categoria" +
      (this.state.id != null ? "/" + this.state.id : "");

    var config = {
      headers: {
        Authorization: "bearer " + this.state.accessToken,
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Headers": "Authorization",
        "Access-Control-Allow-Methods":
          "GET, POST, OPTIONS, PUT, PATCH, DELETE",
      },
    };

    axios.post(url, item, config).then((res) => {
      if (res.data.success === true) {
        this.limparSelecao();
        window.location.reload();
      } else {
        const options = {
          title: "Erro ao Salvar Registro",
          message: res.data.exception.Message,
          buttons: [
            {
              label: "Ok",
            },
          ],
        };

        confirmAlert(options);
      }
    });
  };

  componentDidMount() {
    var grupos = [{ value: null, label: "Selecione..." }];

    global.grupoDeCategoria.forEach(function (value, index) {
      grupos.push({
        value: index,
        label: value,
      });
    });

    var frequencias = [{ value: null, label: "Selecione..." }];

    global.frequenciaCategoria.forEach(function (value, index) {
      frequencias.push({
        value: index,
        label: value,
      });
    });

    this.setState(
      {
        accessToken: localStorage.getItem("access-token"),
        empresaId: localStorage.getItem("empresa-id"),
        familiaId: localStorage.getItem("familia-id"),
        planejadorId: localStorage.getItem("planejador-id"),
        grupos: grupos,
        frequencias: frequencias,
      },
      function () {
        this.atualizarRegistros();
        this.atualizarOrcamentos();
        global.mostrarAjudaDaPaginaAutomaticamente(this);
      }
    );
  }

  removerRegistros = (rowsDeleted) => {
    const options = {
      title: "Exclusão de Categorias",
      message:
        "Confirma procedimento de exclusão? Este procedimento é irreversível!",
      buttons: [
        {
          label: "Sim",
          onClick: () => {
            var ids = [];

            for (var key in rowsDeleted.data) {
              ids.push(
                this.state.registros[rowsDeleted.data[key].dataIndex].id
              );
            }

            if (ids.length > 0) {
              //var url = global.server_api + 'api/categoria/bulk/' + ids.join(",");
              var url =
                global.server_api_new +
                global.apiToken +
                "/categoria/bulk/" +
                ids.join(",");

              if (this.state.familiaId == null) {
                //url = global.server_api + 'api/categoria/bulk/padrao/' + ids.join(",");
                url =
                  global.server_api_new +
                  global.apiToken +
                  "/categoria/bulk/padrao/" +
                  ids.join(",");
              }

              var config = {
                headers: {
                  Authorization: "bearer " + this.state.accessToken,
                  "Access-Control-Allow-Origin": "*",
                  "Access-Control-Allow-Headers": "Authorization",
                  "Access-Control-Allow-Methods":
                    "GET, POST, OPTIONS, PUT, PATCH, DELETE",
                },
              };

              axios.delete(url, {}, config).then((res) => {
                const result = res.data;

                if (result.success && result.affectedResults > 0) {
                  this.atualizarRegistros();
                } else {
                  alert("Erro ao remover. " + res.data.exception.Message);
                  console.log(res.data.exception);
                }
              });
            }
          },
        },
        {
          label: "Não",
        },
      ],
    };

    confirmAlert(options);

    return false;
  };

  mostrarPopup = () => (
    <div>
      <SkyLight
        ref={(ref) => (this.modal = ref)}
        transitionDuration={0}
        beforeOpen={this.preCarregarParaEdicao}
        afterClose={this.limparSelecao}
        title={
          <>
            <div className="pop-up-title">
              <i className="ni ni-check-bold"></i>{" "}
              <h2>Cadastro de Categoria</h2>
            </div>
          </>
        }
      >
        <Row>
          <Col lg="6" xl="6">
            <div>
              <label>Nome</label>
              <input
                type="text"
                id="nome"
                value={this.state.nome}
                onChange={(e) => this.setState({ nome: e.target.value })}
              ></input>
            </div>
          </Col>
          <Col lg="6" xl="6">
            <div>
              <label>Orçamento</label>
              {this.montarDropDown(
                this.state.orcamentosCombo,
                "select-orcamento",
                this.mudarOrcamento,
                this.state.orcamentoId,
                "entidade"
              )}
            </div>
          </Col>
          <Col lg="6" xl="6">
            <div>
              <label>Grupo</label>
              {this.montarDropDown(
                this.state.grupos,
                "select-grupo",
                this.mudarGrupo,
                this.state.grupo,
                "enum"
              )}
            </div>
          </Col>
          <Col lg="6" xl="6">
            <div>
              <label>Frequência</label>
              {this.montarDropDown(
                this.state.frequencias,
                "select-frequencia",
                this.mudarFrequencia,
                this.state.frequencia,
                "enum"
              )}
            </div>
          </Col>
          {this.state.empresaId != null ? (
            <Col lg="12" xl="12">
              <div>
                <label>Valor Estimado</label>
                <CurrencyInput
                  id="valor-estimado"
                  value={
                    this.state.valorEstimadoFormatado != null
                      ? this.state.valorEstimadoFormatado
                      : this.state.valorEstimado
                  }
                  thousandSeparator={"."}
                  decimalSeparator={","}
                  precision="2" //prefix={'R$ '}
                  onChangeEvent={(event, formattedValue, value) => {
                    this.setState({ valorEstimado: value });
                    this.setState({ valorEstimadoFormatado: formattedValue });
                  }}
                />
              </div>
            </Col>
          ) : (
            <></>
          )}
          <Col lg="12" xl="12">
            <button
              id="salvar-lancamento"
              className="featured-button"
              onClick={() => this.salvar()}
              style={{
                backgroundImage:
                  "url(" +
                  require("../../assets/img/theme/botao-destaque.png") +
                  ")",
              }}
            >
              Salvar
            </button>
          </Col>
        </Row>
      </SkyLight>
    </div>
  );

  mudarOrcamento = (selectedOption) => {
    const orcamentoId = selectedOption.value;
    this.setState({ orcamentoId });
  };

  mudarOrcamentoFiltro = (selectedOption) => {
    const orcamentoFiltro = selectedOption.value;

    this.setState({ orcamentoFiltro }, function () {
      this.atualizarRegistros();
    });
  };

  mudarFrequencia = (selectedOption) => {
    const frequencia = selectedOption.value;
    this.setState({ frequencia });
  };

  mudarFrequenciaFiltro = (selectedOption) => {
    const frequenciaFiltro = selectedOption.value;

    this.setState({ frequenciaFiltro }, function () {
      this.atualizarRegistros();
    });
  };

  mudarGrupo = (selectedOption) => {
    const grupo = selectedOption.value;
    this.setState({ grupo });
  };

  mudarGrupoFiltro = (selectedOption) => {
    const grupoFiltro = selectedOption.value;

    this.setState({ grupoFiltro }, function () {
      this.atualizarRegistros();
    });
  };

  alterarFiltro = (stateToStore) => {
    this.setState(stateToStore, function () {
      this.atualizarRegistros();
    });
  };

  render() {
    return (
      <>
        <Container fluid>
          <div className="home-summary-top">
            <Row>
              <Col lg="12" xl="12" style={{ marginBottom: "15px" }}>
                <div>
                  <Card>
                    <CardHeader className="border-0">
                      <Row className="align-items-center">
                        <div className="col">
                          <img
                            src={require("../../assets/img/theme/icone-calendario.png")}
                            alt="Categorias"
                            className="before-title-img"
                          />
                          <h3 className="mb-0 chart-title">Categorias</h3>
                        </div>
                        <div className="new-entry" style={{ float: "right" }}>
                          <div
                            className="nav-link-icon featured-button"
                            style={{
                              backgroundImage:
                                "url(" +
                                require("../../assets/img/theme/botao-destaque.png") +
                                ")",
                            }}
                            onClick={() => global.showModal(this.modal)}
                          >
                            <span className="nav-link-inner--text">
                              Nova Categoria
                            </span>
                          </div>
                        </div>
                      </Row>
                    </CardHeader>
                  </Card>
                </div>
              </Col>
            </Row>
            {/* <Row>
              <Col lg="6" md="6" className="filterAndOnder">
                <label>Orçamento </label>
                {this.montarDropDown(this.state.orcamentosCombo, "select-orcamento-filtro", this.mudarOrcamentoFiltro , this.state.orcamentoFiltro, "entidade")}
              </Col>
              <Col lg="6" md="6" className="filterAndOnder">
                <label>Grupo </label>
                {this.montarDropDown(this.state.grupos, "select-grupo-filtro", this.mudarGrupoFiltro , this.state.grupoFiltro, "enum")}
              </Col>
            </Row>
            <Row style={{marginTop:"5px"}}>
              <Col lg="6" md="6" className="filterAndOnder">
                <label>Frequencia </label>
                {this.montarDropDown(this.state.frequencias, "select-frequencia", this.mudarFrequenciaFiltro , this.state.frequenciaFiltro, "enum")}
              </Col>
              <Col lg="6" md="6" className="filterAndOnder">
                <label>Ordenar </label>{this.montarDropDown(this.camposParaOrdenacao, "selecao-ordenacao", this.mudarOrdenacao, null, null)}
              </Col>
            </Row> */}
            {/* <Row>
              <Col lg="6" md="6" className="filterAndOnder text-field">
                <label>Filtrar </label><input type="text" onKeyUp={this.filtroPorTexto}/>
              </Col>
            </Row> */}
            <Row className="entry-list">{this.mostrarListMUI()}</Row>
          </div>
        </Container>
        {this.mostrarPopup()}
      </>
    );
  }
}

export default Categorias;
