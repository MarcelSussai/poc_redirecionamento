import React, { useEffect } from "react";

// http://localhost:3001/admin/planos-sonhos

const PlanosSonhos = () => {
  useEffect(() => {
    window.location.href = 'http://localhost:3001/admin/planos-sonhos'
  })
  return ( <> </> )
}

export default PlanosSonhos;

// import React from "react";
// import { Redirect, Link } from "react-router-dom";
// import { MDBProgress } from "mdbreact";
// import MUIDataTable from "mui-datatables";
// import axios from "axios";
// import SkyLight from "react-skylight";
// import $ from "jquery";
// import Select from "react-select";
// import TableRow from "@material-ui/core/TableRow";
// import TableCell from "@material-ui/core/TableCell";
// import { confirmAlert } from "react-confirm-alert";
// import Datepicker, { registerLocale } from "react-datepicker";
// import MaskedInput from "react-maskedinput";
// import br from "date-fns/locale/pt-BR";
// import "react-datepicker/dist/react-datepicker.css";
// import ExcelFormulas from "../../assets/js/ExcelFormulas.js";
// import CurrencyInput from "react-currency-input";
// import { createMuiTheme, MuiThemeProvider } from "@material-ui/core/styles";
// import IconeQuero from "@material-ui/icons/AddCircleOutlineOutlined";
// import Tooltip from "@material-ui/core/Tooltip";

// import {
//   Card,
//   CardHeader,
//   Container,
//   Row,
//   Col,
//   NavItem,
//   Navbar,
//   NavLink,
// } from "reactstrap";

// registerLocale("br", br);

// class PlanosSonhos extends React.Component {
//   // moment não é uma boa prática, inclusive no site da lib diz que não é mais atualizado, está deprecado!
//   moment = require("moment");
//   imagem = require("../../assets/img/theme/sonhos-padrao.jpg");

//   imagemSonhos = {};
//   changeThis = null;
//   forThis = null;

//   state = {
//     statuses: [],
//     tipo: "sonho",
//     parcela: "",
//     valorFuturo: "",
//     valorPresente: "",
//     taxa: global.taxa,
//     id: null,
//     inflacao: global.inflacao,
//     prazo: "",
//     valorAcumulado: 0,
//     novoValorAcumulado: "",
//     dataInicial: null,
//     dataFinal: null,
//     ordemPrioridade: "",
//     statusSonhoRealizado: 3,
//     statusSonhoParalisado: 2,
//     editando: false,
//     dictBucketList: {},
//     idSonhoPadrao: 0,
//   };

//   getMuiTheme = () =>
//     createMuiTheme({
//       overrides: {
//         MuiTableRow: {
//           root: {
//             "&:nth-of-type(even)": {
//               backgroundColor: "#f8f9fe",
//             },
//           },
//           head: {
//             display: "none",
//           },
//         },
//         MuiTableCell: {
//           root: {
//             fontFamily: "unset",
//           },
//         },
//         MUIDataTableSelectCell: {
//           fixedHeaderCommon: {
//             backgroundColor: "unset",
//             position: "relative",
//           },
//         },
//         MuiPaper: {
//           root: {
//             overflow: "hidden",
//             margin: "0 10px",
//           },
//           elevation4: {
//             boxShadow: "none",
//             border: "0.5px solid rgb(228, 234, 244)",
//           },
//         },
//         MUIDataTableToolbar: {
//           actions: {
//             display: "none",
//           },
//         },
//         MuiToolbar: {
//           regular: {
//             backgroundColor: "#e4eaf4",
//           },
//         },
//         MUIDataTable: {
//           responsiveScroll: {
//             "&::-webkit-scrollbar": {
//               width: 10,
//             },
//             "&::-webkit-scrollbar-thumb": {
//               backgroundColor: "#98aecb",
//               borderRadius: 8,
//             },
//             "&::-webkit-scrollbar-track": {
//               background: "#f7faff",
//               boxShadow: "inset 0px 0px 1px #606c7a",
//             },
//             "&::-webkit-scrollbar-thumb:hover": {
//               background: "#606c7a",
//             },
//           },
//         },
//       },
//     });

//   setRedirect = () => {
//     this.setState({
//       redirect: true,
//     });
//   };

//   renderRedirect = () => {
//     if (this.state.redirect) {
//       return <Redirect to="/auth/login" />;
//     }
//   };

//   componentDidMount() {
//     var statuses = [];

//     global.statusSonho.forEach(function (value, index) {
//       statuses.push({
//         value: index,
//         label: value,
//       });
//     });

//     this.setState(
//       {
//         accessToken: localStorage.getItem("access-token"),
//         empresaId: localStorage.getItem("empresa-id"),
//         familiaId: localStorage.getItem("familia-id"),
//         statuses: statuses,
//       },
      
//       function () {
//         if (this.state.accessToken == null) {
//           this.setRedirect();
//           return;
//         }

//         this.atualizarDados();
//         global.mostrarAjudaDaPaginaAutomaticamente(this);
//       }
//     );

//     var $this = this;

//     setTimeout(function () {
//       $(document).on("change", "#imagem", function (event) {
//         var file = event.target.files[0];
//         var reader = new FileReader();

//         reader.onload = function (readerEvt) {
//           var binaryString = readerEvt.target.result;
//           var b64 = btoa(binaryString);
//           $this.imagem = "data:image/png;base64," + b64;
//           $this.mostrarThumb();
//         };

//         reader.readAsBinaryString(file);
//       });
//     }, 1000);
//   }

//   atualizarDados = () => {
//     this.imagemSonhos = {};
//     this.atualizarBucketList();
//     this.atualizarPlanosSonhos();
//     this.sonhosAgrupados((res) => {
//       if (res.data && res.data.singleResult) {
//         this.setState({ dictBucketList: res.data.singleResult });
//       }
//     });
//   };

//   buscarImagemSonho = (id) => {
//     if (this.imagemSonhos != null && this.imagemSonhos[id] != null) {
//       $("#bucket-list-" + id).css(
//         "backgroundImage",
//         "url(" + this.imagemSonhos[id] + ")"
//       );
//       return;
//     }

//     //var url = global.server_api + 'api/sonho/imagem/' + id;
//     var url = global.server_api_new + global.apiToken + "/sonho/imagem/" + id;

//     var config = {
//       headers: {
//         Authorization: "bearer " + this.state.accessToken,
//         "Access-Control-Allow-Origin": "*",
//         "Access-Control-Allow-Headers": "Authorization",
//         "Access-Control-Allow-Methods":
//           "GET, POST, OPTIONS, PUT, PATCH, DELETE",
//       },
//     };

//     axios.get(url, config).then((res) => {
//       if (res.data.singleResult != null && res.data.singleResult !== "") {
//         $("#dream-card-data-" + id).css(
//           "backgroundImage",
//           "url(" + res.data.singleResult + ")"
//         );
//         this.imagemSonhos[id] = res.data.singleResult;
//       }
//     });
//   };

//   atualizarBucketList = () => {
//     //var url = global.server_api + 'api/sonho/bucketlist/familia/' + this.state.familiaId;
//     var url =
//       global.server_api_new +
//       global.apiToken +
//       "/sonho/bucketlist/familia/" +
//       this.state.familiaId;

//     var config = {
//       headers: {
//         Authorization: "bearer " + this.state.accessToken,
//         "Access-Control-Allow-Origin": "*",
//         "Access-Control-Allow-Headers": "Authorization",
//         "Access-Control-Allow-Methods":
//           "GET, POST, OPTIONS, PUT, PATCH, DELETE",
//       },
//     };

//     axios.get(url, config).then((res) => {
//       var bucketList = [];

//       if (res.data.results != null) {
//         bucketList = res.data.results;
//       }

//       this.setState({ bucketList });
//     });
//   };

//   sonhosAgrupados = (callback) => {
//     //var url = `${global.server_api}api/bucketlist/agrupado`;
//     var url = `${global.server_api_new}${global.apiToken}/bucketlist/agrupado`;

//     var config = {
//       headers: {
//         Authorization: "bearer " + this.state.accessToken,
//         "Access-Control-Allow-Origin": "*",
//         "Access-Control-Allow-Headers": "Authorization",
//         "Access-Control-Allow-Methods":
//           "GET, POST, OPTIONS, PUT, PATCH, DELETE",
//       },
//     };

//     axios.get(url, config).then(callback);
//   };

//   mostrarCategorias = (categorias) => {
//     if (categorias === null || categorias === undefined) return [];
//     var keys = Object.keys(categorias);

//     var result = keys.map((value) => {
//       return {
//         id: value,
//         nome: global.grupoCategoriaSonho[value],
//         sonhos: categorias[value],
//       };
//     });
//     return result;
//   };

//   atualizarPlanosSonhos = (hideSpinner) => {
//     var currentScroll = [
//       document.documentElement.scrollLeft || document.body.scrollLeft,
//       document.documentElement.scrollTop || document.body.scrollTop,
//     ];

//     if (hideSpinner == null || hideSpinner === false) global.spinnerShow($);

//     //var url = global.server_api + 'api/sonho/planos/familia/' + this.state.familiaId;
//     var url =
//       global.server_api_new +
//       global.apiToken +
//       "/sonho/planos/familia/" +
//       this.state.familiaId;

//     var config = {
//       headers: {
//         Authorization: "bearer " + this.state.accessToken,
//         "Access-Control-Allow-Origin": "*",
//         "Access-Control-Allow-Headers": "Authorization",
//         "Access-Control-Allow-Methods":
//           "GET, POST, OPTIONS, PUT, PATCH, DELETE",
//       },
//     };

//     axios.get(url, config).then((res) => {
//       if (hideSpinner == null || hideSpinner === false)
//         global.spinnerHide($, currentScroll);

//       var dreamList = [];

//       if (res.data.results != null) {
//         dreamList = res.data.results;
        
//       console.debug(res.data.results, 'DEBUG - PlanosSonhos')
//     }

//       this.setState({ dreamList }, function () {
//         if (this.state.dreamList != null) {
//           this.state.dreamList.forEach((item, key) => {
//             if (item.statusSonho === 0 || item.statusSonho === 1) {
//               this.buscarPercentualRealizado(item.sonhoId, key);
//             }
//             this.buscarImagemSonho(item.sonhoId);
//           });
//         }
//       });
//     });
//   };

//   mudarStatus = (selectedOption) => {
//     const status = selectedOption.value;
//     this.setState({ status });
//   };

//   limparSelecao = () => {
//     this.imagem = require("../../assets/img/theme/sonhos-padrao.jpg");

//     $("#linha").html("");

//     this.setState({
//       id: null,
//       status: null,
//       descricao: "",
//       editando: false,
//       planoId: null,
//       tipo: "sonho",
//       //Dados do Plano
//       dataInicial: null,
//       prazo: "",
//       dataFinal: null,
//       valorPresente: "",
//       valorPresenteFormatado: "",
//       inflacao: global.inflacao,
//       inflacaoFormatado: global.inflacao,
//       valorFuturo: "",
//       taxa: global.taxa,
//       taxaFormatado: global.taxa,
//       parcela: "",
//       ordemPrioridade: "",
//       novoValorAcumulado: "",
//       novoValorAcumuladoFormatado: "",
//       idSonhoAposentadoria: 0,
//     });
//   };

//   atualizarPrioridades = () => {
//     var novasPrioridades = [];
//     var index = 1;

//     this.state.dreamList.forEach(function (item) {
//       if (item.tipo === "Plano") {
//         novasPrioridades.push({
//           sonhoId: item.sonhoId,
//           ordemPrioridade: index++,
//         });
//       }
//     });

//     //var url = global.server_api + 'api/sonho/novasPrioridades/' + this.state.familiaId;
//     var url =
//       global.server_api_new +
//       global.apiToken +
//       "/sonho/novasPrioridades/" +
//       this.state.familiaId;

//     var config = {
//       headers: {
//         Authorization: "bearer " + this.state.accessToken,
//         "Access-Control-Allow-Origin": "*",
//         "Access-Control-Allow-Headers": "Authorization",
//         "Access-Control-Allow-Methods":
//           "GET, POST, OPTIONS, PUT, PATCH, DELETE",
//       },
//     };

//     axios.post(url, novasPrioridades, config).then((res) => {
//       this.atualizarPlanosSonhos(true);
//     });
//   };

//   renderDreamCards = () => {
//     if (this.state.dreamList == null || this.state.dreamList.length === 0) {
//       return <div>Nenhum Sonho Encontrado.</div>;
//     }
//     return this.state.dreamList.map((item, key) => {
//       let imageData = item.imagem;
//       if (imageData === null || imageData === "") {
//         imageData = require("../../assets/img/theme/sonhos-padrao.jpg");
//       }

//       let finishedClass =
//         item.statusSonho === this.state.statusSonhoRealizado
//           ? "finished"
//           : "hidden";

//       let paralisadoClass =
//         item.statusSonho === this.state.statusSonhoParalisado
//           ? "finished paralisado"
//           : "hidden";

//       let progressClass =
//         item.tipo !== "Plano" ||
//         item.statusSonho === this.state.statusSonhoRealizado
//           ? "hidden"
//           : "progress-bar-right-sidebar";

//       let percentualRealizadoFuturo =
//         item.percentualRealizadoFuturo == null
//           ? 0
//           : item.percentualRealizadoFuturo;

//       let isDraggable =
//         item.tipo === "Plano" &&
//         item.statusSonho !== this.state.statusSonhoParalisado &&
//         item.statusSonho !== this.state.statusSonhoRealizado;

//       //console.log(item.sonhoId);
//       return (
//         <Col
//           id={"dream-card-holder-" + item.sonhoId}
//           draggable
//           onDragOver={(event) => {
//             if (isDraggable && this.changeForThis !== event.target.id)
//               this.changeForThis = event.target.id;
//           }}
//           onDragStart={(event) => {
//             if (isDraggable && this.changeThis == null) {
//               this.changeThis = event.target.id;
//             }
//           }}
//           onDragEnd={(event) => {
//             if (isDraggable && this.changeThis !== this.changeForThis) {
//               this.imagemSonhos = {};
//               var changeThisId = parseInt(this.changeThis.split("-")[3]);
//               var changeForThisId = parseInt(this.changeForThis.split("-")[3]);
//               var changeThisIndex = null;
//               var changeForThisIndex = null;
//               for (var i = 0; i < this.state.dreamList.length; i++) {
//                 var item = this.state.dreamList[i];
//                 if (item.sonhoId === changeThisId) {
//                   changeThisIndex = i;
//                 }
//                 if (item.sonhoId === changeForThisId) {
//                   changeForThisIndex = i;
//                 }
//                 this.buscarImagemSonho(item.sonhoId);
//               }
//               var dreamList = this.arrayMove(
//                 this.state.dreamList,
//                 changeThisIndex,
//                 changeForThisIndex
//               );
//               this.setState({ dreamList: dreamList }, function () {
//                 this.atualizarPrioridades();
//               });
//               this.changeThis = null;
//               this.changeForThis = null;
//             }
//           }}
//           key={key}
//           lg="4"
//           md="6"
//           className="dream-card"
//         >
//           <div
//             className="excluir-sonho-plano"
//             alt="Excluir plano ou sonho"
//             onClick={() => this.removerRegistro(item.sonhoId)}
//           >
//             <span>x</span>
//           </div>
//           <div
//             id={"dream-card-data-" + item.sonhoId}
//             className="dream-card-data"
//             style={{ height: 130 }}
//             // style={{backgroundImage: "url(" + imageData + ")"}}
//             onClick={() => this.editarPS(item)}
//           >
//             <div
//               className={finishedClass}
//               style={{
//                 backgroundImage:
//                   "url(" +
//                   require("../../assets/img/theme/realizado.png") +
//                   ")",
//               }}
//             ></div>
//             <div
//               className={paralisadoClass}
//               style={{
//                 backgroundImage:
//                   "url(" +
//                   require("../../assets/img/theme/paralisado.png") +
//                   ")",
//               }}
//             ></div>
//             <MDBProgress
//               material
//               value={percentualRealizadoFuturo}
//               height="21px"
//               style={{ fontSize: 20 }}
//               color={
//                 percentualRealizadoFuturo != null &&
//                 percentualRealizadoFuturo >= 100
//                   ? "success"
//                   : "info"
//               }
//               className={progressClass}
//             >
//               {" "}
//               &nbsp;
//               {`${percentualRealizadoFuturo.toFixed(2)}%`}
//             </MDBProgress>
//           </div>
//           <div
//             className="dream-card-title"
//             style={{ padding: 9, fontSize: "0.7rem" }}
//             onClick={() => this.editarPS(item)}
//           >
//             <span
//               className="fa-stack"
//               style={
//                 item.ordemPrioridade != null && item.ordemPrioridade !== ""
//                   ? { width: "1.6em", height: "1.6em", marginRight: 5 }
//                   : { display: "none" }
//               }
//             >
//               <span
//                 className="fas fa-calendar fa fa-stack-2x"
//                 style={{ fontSize: "1.6em" }}
//               ></span>
//               <strong
//                 className="fa-stack-1x"
//                 style={{
//                   fontSize: "90%",
//                   color: "white",
//                   marginTop: "0px",
//                   marginLeft: "-0.5px",
//                 }}
//               >
//                 {item.ordemPrioridade}
//               </strong>
//             </span>
//             {item.descricao}
//           </div>
//         </Col>
//       );
//     });
//   };

//   removerRegistro = (id) => {
//     if (id == null) {
//       console.log("Id errado");
//     }

//     const options = {
//       title: "Exclusão de Planos & Sonhos",
//       message:
//         "Tem certeza que deseja excluir? Este procedimento é irreversível!",
//       buttons: [
//         {
//           label: "Sim",
//           onClick: () => {
//             //var url = global.server_api + 'api/sonho/' + id;
//             var url = global.server_api_new + global.apiToken + "/sonho/" + id;

//             var config = {
//               headers: {
//                 Authorization: "bearer " + this.state.accessToken,
//                 "Access-Control-Allow-Origin": "*",
//                 "Access-Control-Allow-Headers": "Authorization",
//                 "Access-Control-Allow-Methods":
//                   "GET, POST, OPTIONS, PUT, PATCH, DELETE",
//               },
//             };

//             axios.delete(url, {}, config).then((res) => {
//               const result = res.data;

//               if (result.success && result.affectedResults > 0) {
//                 this.atualizarDados();
//               } else {
//                 console.log(res.data.exception);
//                 alert(
//                   "Erro ao remover plano ou sonho. " +
//                     res.data.exception.Message
//                 );
//               }
//             });
//           },
//         },
//         {
//           label: "Não",
//         },
//       ],
//     };

//     confirmAlert(options);

//     return false;
//   };

//   arrayMove = (arr, old_index, new_index) => {
//     if (new_index >= arr.length) {
//       var k = new_index - arr.length + 1;
//       while (k--) {
//         arr.push(undefined);
//       }
//     }
//     arr.splice(new_index, 0, arr.splice(old_index, 1)[0]);
//     return arr; // for testing
//   };

//   ordenarCategorias = (item1, item2) => {
//     const name1 = item1.nome.toUpperCase();
//     const name2 = item2.nome.toUpperCase();

//     let comparison = 0;
//     if (name1 > name2) {
//       comparison = 1;
//     } else if (name1 < name2) {
//       comparison = -1;
//     }

//     //console.log(name1, name2, comparison);

//     return comparison;
//   };

//   mostrarBucketList = () => {
//     if (this.state.bucketList == null || this.state.bucketList.length === 0) {
//       return <div>Nenhum sonho encontrado.</div>;
//     }

//     const options = {
//       textLabels: global.textLabels,
//       selectableRows: false,
//       expandableRows: true,
//       filter: false,
//       filterType: "dropdown",
//       print: false,
//       download: false,
//       pagination: false,
//       searchable: false,
//       viewColumns: true,
//       responsive: "scroll",
//       fixedHeader: true,
//       onRowsDelete: (rowsDeleted) => {
//         this.removerRegistro(rowsDeleted);
//         return false;
//       },
//       onRowClick: (rowData) => {
//         this.editar(rowData[0]);
//         this.setarIdCategoria(rowData[0]);
//       },
//       renderExpandableRow: (rowData, rowMeta) => {
//         return this.subnivel(rowData[2]);
//       },
//     };

//     const columns = [
//       {
//         name: "id",
//         options: {
//           display: false,
//           viewColumns: true,
//           filter: false,
//         },
//       },
//       {
//         name: "nome",
//         label: "Categorias de Sonhos",
//         options: {
//           display: true,
//         },
//       },
//       {
//         name: "sonhos",
//         options: {
//           display: false,
//           viewColumns: false,
//           filter: false,
//         },
//       },
//     ];

//     var keys = Object.keys(this.state.dictBucketList);
//     var list = keys.map((value) => {
//       return {
//         id: value,
//         nome: global.grupoCategoriaSonho[value],
//         sonhos: this.state.dictBucketList[value],
//       };
//     });

//     list.sort(this.ordenarCategorias);

//     return (
//       <>
//         <div style={{ width: "100%", marginBottom: 30 }}>
//           <MuiThemeProvider theme={this.getMuiTheme()}>
//             <MUIDataTable
//               title={"Lista de sonhos"}
//               id={"mui-data-table"}
//               columns={columns}
//               data={list}
//               options={options}
//             />
//           </MuiThemeProvider>
//         </div>
//         {/* {this.removeMUIDecorator()} */}
//       </>
//     );
//   };

//   gerarDados = (bucketList) => {
//     if (bucketList == null) {
//       return [];
//     }
//     var imageData = require("../../assets/img/theme/sonhos-padrao.jpg");
//     return bucketList.map((item, key) => {
//       return {
//         id: item.id,
//         img: (
//           <img
//             id={"bucket-list-" + item.id}
//             src={item.imagem != null ? item.imagem : imageData}
//             style={{ maxWidth: "200px", maxHeight: "200px" }}
//             alt={item.descricao}
//           />
//         ),
//         descricao: item.descricao,
//       };
//     });
//   };

//   // removeMUIDecorator = () => {
//   //   setTimeout(() => {
//   //     // $("div.MuiToolbar-root.MuiToolbar-regular.MUIDataTableToolbar-root-12.MuiToolbar-gutters").remove();
//   //     $(".MuiTableHead-root.MUIDataTableHead-responsiveStacked-29.MUIDataTableHead-main-28").remove();
//   //   }, 10);
//   // }

//   subnivel = (bucketListFake) => {
//     var resultList = [];
//     this.state.bucketList.forEach((first) => {
//       bucketListFake.forEach((second) => {
//         if (first.bucketListId === second.id) {
//           resultList.push(first);
//         }
//       });
//     });
//     const colSpan = 20;
//     return resultList.map((item, key) => {
//       return (
//         <TableRow>
//           <TableCell colSpan={colSpan}>
//             <Row key={key}>
//               <Col
//                 lg="9"
//                 xl="9"
//                 md="9"
//                 sm="9"
//                 xs="9"
//                 style={{
//                   fontSize: "13px",
//                   lineHeight: "1rem",
//                   alignSelf: "center",
//                 }}
//               >
//                 {item.descricao}
//               </Col>

//               <Col
//                 lg="3"
//                 xl="3"
//                 md="3"
//                 sm="3"
//                 xs="3"
//                 style={{ fontSize: "13px", textAlign: "center" }}
//               >
//                 {item.tipo === "Plano" ? (
//                   <Tooltip
//                     disableFocusListener
//                     disableTouchListener
//                     title="Plano"
//                     placement="left"
//                   >
//                     <i
//                       className="fas fa-piggy-bank"
//                       style={{ color: "#0fa4c39e", fontSize: "24px" }}
//                     />
//                   </Tooltip>
//                 ) : item.tipo === "Sonho" ? (
//                   <Tooltip
//                     disableFocusListener
//                     disableTouchListener
//                     title="Sonho"
//                     placement="left"
//                   >
//                     <i
//                       className="far fa-lightbulb"
//                       style={{
//                         cursor: "pointer",
//                         color: "#9a3dff99",
//                         fontSize: "24px",
//                       }}
//                       onClick={() => this.removerRegistro(item.sonhoId)}
//                     />
//                   </Tooltip>
//                 ) : (
//                   <Tooltip
//                     disableFocusListener
//                     disableTouchListener
//                     title="Eu quero"
//                     placement="left"
//                   >
//                     <IconeQuero
//                       style={{ cursor: "pointer", color: "#8898aa" }}
//                       onClick={() => this.editar(item.bucketListId)}
//                     />
//                   </Tooltip>
//                 )}
//               </Col>
//             </Row>
//           </TableCell>
//         </TableRow>
//       );
//     });
//   };

//   editarPS = (edicao) => {
//     if (edicao == null) {
//       return;
//     }

//     if (edicao.imagem == null) {
//       edicao.imagem = this.imagemSonhos[edicao.sonhoId];
//     }

//     this.imagem = edicao.imagem;

//     if (this.imagem == null || this.imagem === "") {
//       this.imagem = require("../../assets/img/theme/sonhos-padrao.jpg");
//     }

//     try {
//       //Seta item edicao
//       this.setState(
//         {
//           id: edicao.sonhoId,
//           planoId: edicao.planoId,
//           descricao: edicao.descricao,
//           status: edicao.statusSonho,
//           editando: true,
//           tipo: edicao.planoId == null ? "sonho" : "plano",
//           //Dados do Plano
//           dataInicial:
//             edicao.dataInicial != null
//               ? this.moment(edicao.dataInicial, "YYYY-MM-DDTHH:mm:ss").toDate()
//               : null, //2019-12-02T07:01:20,
//           prazo: edicao.prazo,
//           dataFinal:
//             edicao.dataFinal != null
//               ? this.moment(edicao.dataFinal, "YYYY-MM-DDTHH:mm:ss").toDate()
//               : null,
//           valorPresente: edicao.valorEstimadoPresente,
//           inflacao: edicao.inflacao,
//           valorFuturo: edicao.valorEstimadoFuturo,
//           taxa: edicao.taxa,
//           parcela: edicao.parcela,
//           ordemPrioridade: edicao.ordemPrioridade,
//           valorAcumulado: edicao.valorAcumulado,
//         },
//         function () {
//           global.showModal(this.modal);
//         }
//       );
//     } catch (ex) {
//       console.log(ex);
//     }
//   };

//   setarIdCategoria = (id) => {
//     //this.setState({idSonhoPadrao: id});
//     console.log(id);
//   };

//   editar = (id) => {
//     if (id == null) {
//       return;
//     }
//     //this.setarIdCategoria(id);
//     var edicao = this.state.bucketList.filter(function (item) {
//       return item.bucketListId === id;
//     });

//     if (edicao != null && edicao.length > 0) {
//       edicao = edicao[0];

//       var $this = this;

//       //var url = global.server_api + 'api/sonho/bucketList/imagem/' + edicao.bucketListId;
//       var url =
//         global.server_api_new +
//         global.apiToken +
//         "/sonho/bucketList/imagem/" +
//         edicao.bucketListId;

//       var config = {
//         headers: {
//           Authorization: "bearer " + this.state.accessToken,
//           "Access-Control-Allow-Origin": "*",
//           "Access-Control-Allow-Headers": "Authorization",
//           "Access-Control-Allow-Methods":
//             "GET, POST, OPTIONS, PUT, PATCH, DELETE",
//         },
//       };

//       var currentScroll = [
//         document.documentElement.scrollLeft || document.body.scrollLeft,
//         document.documentElement.scrollTop || document.body.scrollTop,
//       ];

//       global.spinnerShow($);

//       axios.get(url, config).then((res) => {
//         $this.imagem = res.data.singleResult;

//         if (this.imagem == null || this.imagem === "") {
//           this.imagem = require("../../assets/img/theme/sonhos-padrao.jpg");
//         }

//         //Seta item edicao
//         this.setState(
//           {
//             id: edicao.sonhoId,
//             planoId: edicao.planoId,
//             descricao: edicao.descricao,
//             status: edicao.statusSonho,
//             bucketListId: edicao.bucketListId,
//             editando: true,
//             tipo: "sonho",
//             idSonhoPadrao: id,
//           },
//           function () {
//             global.showModal(this.modal);
//             global.spinnerHide($, currentScroll);
//           }
//         );
//       });
//     }
//   };

//   montarDropDown = (itens, id, changeMethod, selectedValue, type) => (
//     <Select
//       id={id}
//       onChange={changeMethod}
//       className="select-component"
//       options={itens}
//       defaultValue={itens != null ? itens[0] : ""}
//       placeholder="Selecione..."
//       value={
//         itens != null && selectedValue != null
//           ? itens.find((op) => {
//               if (type === "enum" && $.isNumeric(selectedValue)) {
//                 //ID
//                 return (
//                   (op.value != null ? op.value.toString() : null) ===
//                   (selectedValue != null ? selectedValue.toString() : null)
//                 );
//               } else if (type === "enum") {
//                 //TEXTO
//                 return op.label === selectedValue;
//               } else if (op.value != null && selectedValue != null) {
//                 return op.value.id === selectedValue.id;
//               }
//               return false;
//             })
//           : itens[0]
//       }
//     />
//   );

//   mostrarThumb = () => {
//     var novaLinha =
//       '<img src="' +
//       this.imagem +
//       '" onclick="document.getElementById(\'imagem\').click()" style="max-width: 100%; max-height: 280px; margin-left: 50%; border-radius: 10px; transform:translateX(-50%);">';
//     $("#linha").html(novaLinha);
//   };

//   setDataInicial = (date) => {
//     this.alterarPrazoECalcularDataFinal("dataInicial", date, null);
//   };

//   gerarDatePicker = (id, callback, defaultDate) => {
//     var currentDate = null;

//     //date type
//     if (this.state.dataInicial != null) {
//       //string vinda da edicao
//       if (typeof this.state.dataInicial === "string") {
//         var temp = this.moment(this.state.dataInicial, "YYYY-MM-DDThh:mm:ss");
//         this.setState({ dataInicial: temp });
//         currentDate = temp.toDate();
//       }
//       //data vinda da selecao atraves do calendario
//       else {
//         currentDate = this.state.dataInicial;
//       }
//     }
//     //momentjs date
//     else if (defaultDate != null) {
//       currentDate = defaultDate.toDate();
//       this.setState({ dataInicial: currentDate });
//     }
//     //now (date)
//     else {
//       currentDate = new Date();
//       this.setState({ dataInicial: currentDate });
//     }

//     return (
//       <Datepicker
//         locale="br"
//         id={id}
//         dateFormat="dd/MM/yyyy"
//         showYearDropdown
//         dateFormatCalendar="MMMM"
//         scrollableYearDropdown
//         yearDropdownItemNumber={5}
//         selected={currentDate}
//         onChange={(date) => callback(date)}
//         style={{ color: "#8898aa" }}
//         customInput={<MaskedInput mask="11/11/1111" />}
//       />
//     );
//   };

//   alterarPrazoECalcularDataFinal = (campo, valor, callback) => {
//     var dataFinal = null;

//     if (campo === "dataInicial") {
//       dataFinal = this.moment(valor).add(this.state.prazo, "M");

//       this.setState(
//         {
//           dataInicial: valor,
//           dataFinal: dataFinal,
//         },
//         function () {
//           if (callback) {
//             callback();
//           }
//         }
//       );
//     } else if (campo === "prazo") {
//       dataFinal = this.moment(this.state.dataInicial).add(valor, "M");

//       this.setState(
//         {
//           prazo: valor,
//           dataFinal: dataFinal,
//         },
//         function () {
//           if (callback) {
//             callback();
//           }
//         }
//       );
//     }
//   };

//   calcularEAtualizarParcelaEValorFuturo = () => {
//     if (
//       this.state.valorPresente != null &&
//       this.state.valorPresente !== "" &&
//       this.state.prazo != null &&
//       this.state.prazo !== ""
//     ) {
//       var taxa =
//         this.state.taxa != null && this.state.taxa !== "" ? this.state.taxa : 0;
//       var inflacao =
//         this.state.inflacao != null && this.state.inflacao !== ""
//           ? this.state.inflacao
//           : 0;

//       var novoValorAcumulado =
//         this.state.novoValorAcumulado != null &&
//         this.state.novoValorAcumulado !== ""
//           ? this.state.novoValorAcumulado
//           : 0;
//       var acumulado =
//         this.state.valorAcumulado != null && this.state.valorAcumulado !== ""
//           ? this.state.valorAcumulado
//           : 0;

//       if (novoValorAcumulado > 0) {
//         acumulado = novoValorAcumulado;
//       }

//       // console.log(acumulado);

//       var prazoEmAnos = this.state.prazo / 12;

//       var valorFuturo = ExcelFormulas.FV(
//         inflacao,
//         prazoEmAnos,
//         this.state.valorPresente
//       );
//       var parcela = -ExcelFormulas.PMT(
//         taxa,
//         this.state.prazo,
//         -acumulado,
//         valorFuturo,
//         0
//       );

//       this.setState({
//         valorFuturo: valorFuturo,
//         parcela: parcela,
//       });
//     } else {
//       this.setState({
//         valorFuturo: null,
//         parcela: null,
//       });
//     }
//   };

//   mostrarPopup = () => (
//     <div className="planosSonhos">
//       <SkyLight
//         ref={(ref) => (this.modal = ref)}
//         transitionDuration={0}
//         beforeOpen={this.preCarregarParaEdicao}
//         afterClose={this.limparSelecao}
//         dialogStyles={{ borderRadius: "1rem", padding: "2rem" }}
//         title={
//           <>
//             <div className="pop-up-title">
//               <i className="ni ni-check-bold"></i>
//               <h2>Cadastro de Planos & Sonhos</h2>
//               <Navbar style={{ visibility: "hidden" }}>
//                 <Container>
//                   <NavItem
//                     id="sonho"
//                     className={
//                       this.state.tipo === "sonho"
//                         ? "lancamento-opcoes active"
//                         : "lancamento-opcoes"
//                     }
//                     onClick={() => this.setState({ tipo: "sonho" })}
//                   >
//                     <NavLink className="nav-link-icon">
//                       <span className="nav-link-inner--text">Sonho</span>
//                     </NavLink>
//                   </NavItem>
//                   <NavItem
//                     id="plano"
//                     className={
//                       this.state.tipo === "plano"
//                         ? "lancamento-opcoes active"
//                         : "lancamento-opcoes"
//                     }
//                     onClick={() => this.setState({ tipo: "plano" })}
//                   >
//                     <NavLink className="nav-link-icon">
//                       <span className="nav-link-inner--text">Plano</span>
//                     </NavLink>
//                   </NavItem>
//                 </Container>
//               </Navbar>
//             </div>
//           </>
//         }
//       >
//         <Row>
//           <Col lg="6" xl="6">
//             <div>
//               <label>Descrição *</label>
//               <input
//                 type="text"
//                 id="nome"
//                 value={this.state.descricao}
//                 style={{ paddingLeft: 24, color: "#8898aa" }}
//                 onChange={(e) => this.setState({ descricao: e.target.value })}
//               ></input>
//             </div>
//           </Col>
//           <Col lg="6" xl="6">
//             <div>
//               <label>Status *</label>
//               {this.montarDropDown(
//                 this.state.statuses,
//                 "select-status",
//                 this.mudarStatus,
//                 this.state.status,
//                 "enum"
//               )}
//             </div>
//           </Col>
//           <Col
//             lg="3"
//             xl="3"
//             style={
//               this.state.tipo === "sonho"
//                 ? { display: "none" }
//                 : { display: "block" }
//             }
//           >
//             <div>
//               <label>Data inicial *</label>
//               {this.gerarDatePicker(
//                 "dataInicial",
//                 this.setDataInicial,
//                 this.moment()
//               )}
//             </div>
//           </Col>
//           <Col
//             lg="3"
//             xl="3"
//             style={
//               this.state.tipo === "sonho"
//                 ? { display: "none" }
//                 : { display: "block" }
//             }
//           >
//             <div>
//               <label>Prazo (meses) *</label>
//               <input
//                 type="number"
//                 value={this.state.prazo}
//                 style={{ paddingLeft: 24, color: "#8898aa" }}
//                 onChange={(e) =>
//                   this.alterarPrazoECalcularDataFinal(
//                     "prazo",
//                     e.target.value,
//                     this.calcularEAtualizarParcelaEValorFuturo
//                   )
//                 }
//               ></input>
//             </div>
//           </Col>
//           <Col
//             lg="6"
//             xl="6"
//             style={
//               this.state.tipo === "sonho"
//                 ? { display: "none" }
//                 : { display: "block" }
//             }
//           >
//             <div>
//               <label>Data final</label>
//               <input
//                 style={{
//                   borderRadius: 15,
//                   background: "#F7F9FB",
//                   padding: "5px 24px",
//                   color: "#8898aa",
//                   minHeight: 34,
//                 }}
//                 value={
//                   this.state.dataFinal != null
//                     ? this.moment(this.state.dataFinal).format("DD/MM/YYYY")
//                     : ""
//                 }
//                 placeholder="Preencha o prazo para calcularmos a data final"
//                 readOnly
//               />
//             </div>
//           </Col>

//           <Col
//             lg="3"
//             xl="3"
//             style={
//               this.state.tipo === "sonho"
//                 ? { display: "none" }
//                 : { display: "block" }
//             }
//           >
//             <div>
//               <label>Valor do Plano hoje *</label>
//               <CurrencyInput
//                 id="valor-estimado"
//                 style={{ paddingLeft: 24, color: "#8898aa" }}
//                 value={
//                   this.state.valorPresenteFormatado
//                     ? this.state.valorPresenteFormatado
//                     : this.state.valorPresente
//                 }
//                 thousandSeparator={"."}
//                 decimalSeparator={","}
//                 precision="2" //prefix={'R$ '}
//                 onChangeEvent={(event, formattedValue, value) => {
//                   this.setState(
//                     {
//                       valorPresente: value,
//                       valorPresenteFormatado: formattedValue,
//                     },
//                     this.calcularEAtualizarParcelaEValorFuturo
//                   );
//                 }}
//               />
//             </div>
//           </Col>
//           <Col
//             lg="3"
//             xl="3"
//             style={
//               this.state.tipo === "sonho"
//                 ? { display: "none" }
//                 : { display: "block" }
//             }
//           >
//             <div>
//               <label>Taxa (a.m.)*</label>
//               <CurrencyInput
//                 style={{ paddingLeft: 24, color: "#8898aa" }}
//                 value={this.state.taxa}
//                 thousandSeparator={"."}
//                 decimalSeparator={","}
//                 suffix={"%"}
//                 onChangeEvent={(event, formattedValue, value) => {
//                   this.setState(
//                     {
//                       taxa: value,
//                       taxaFormatado: formattedValue,
//                     },
//                     this.calcularEAtualizarParcelaEValorFuturo
//                   );
//                 }}
//               />
//             </div>
//           </Col>
//           <Col
//             lg="3"
//             xl="3"
//             style={
//               this.state.tipo === "sonho"
//                 ? { display: "none" }
//                 : { display: "block" }
//             }
//           >
//             <div>
//               <label>Inflação (a.a.)*</label>
//               <CurrencyInput
//                 style={{ paddingLeft: 24, color: "#8898aa" }}
//                 value={this.state.inflacao}
//                 thousandSeparator={"."}
//                 decimalSeparator={","}
//                 suffix={"%"}
//                 onChangeEvent={(event, formattedValue, value) => {
//                   this.setState(
//                     {
//                       inflacao: value,
//                       inflacaoFormatado: formattedValue,
//                     },
//                     this.calcularEAtualizarParcelaEValorFuturo
//                   );
//                 }}
//               />
//             </div>
//           </Col>
//           <Col
//             lg="3"
//             xl="3"
//             style={
//               this.state.tipo === "sonho"
//                 ? { display: "none" }
//                 : { display: "block" }
//             }
//           >
//             <div>
//               <label>Ordem de prioridade</label>
//               <input
//                 type="number"
//                 value={this.state.ordemPrioridade}
//                 style={{ paddingLeft: 24, color: "#8898aa" }}
//                 onChange={(e) =>
//                   this.setState({ ordemPrioridade: e.target.value })
//                 }
//                 disabled
//               ></input>
//             </div>
//           </Col>
//           <Col
//             lg="3"
//             xl="3"
//             style={
//               this.state.tipo === "sonho"
//                 ? { display: "none" }
//                 : { display: "block" }
//             }
//           >
//             <div>
//               <label>Simulação do valor acumulado</label>
//               <CurrencyInput
//                 id="simuladoVA"
//                 style={{ paddingLeft: 24, color: "#8898aa" }}
//                 value={
//                   this.state.novoValorAcumuladoFormatado != null
//                     ? this.state.novoValorAcumuladoFormatado
//                     : this.state.novoValorAcumulado
//                 }
//                 thousandSeparator={"."}
//                 decimalSeparator={","}
//                 precision="2" //prefix={'R$ '}
//                 onChangeEvent={(event, formattedValue, value) => {
//                   this.setState(
//                     {
//                       novoValorAcumulado: value,
//                       novoValorAcumuladoFormatado: formattedValue,
//                     },
//                     this.calcularEAtualizarParcelaEValorFuturo
//                   );
//                 }}
//               />
//             </div>
//           </Col>
//           <Col
//             lg="3"
//             xl="3"
//             style={
//               this.state.tipo === "sonho"
//                 ? { display: "none" }
//                 : { display: "block" }
//             }
//           >
//             <div>
//               <label>Valor acumulado</label>
//               <label>
//                 {this.state.valorAcumulado != null &&
//                 this.state.valorAcumulado !== ""
//                   ? parseFloat(this.state.valorAcumulado)
//                       .toLocaleString("pt-BR", {
//                         style: "currency",
//                         currency: "BRL",
//                       })
//                       .replace("R$", "")
//                   : "0,00"}
//               </label>
//             </div>
//           </Col>
//           <Col
//             lg="3"
//             xl="3"
//             style={
//               this.state.tipo === "sonho"
//                 ? { display: "none" }
//                 : { display: "block" }
//             }
//           >
//             <div>
//               <label>
//                 <b>Valor do Plano futuro</b>
//               </label>
//               <label>
//                 {this.state.valorFuturo != null
//                   ? this.state.valorFuturo
//                       .toLocaleString("pt-BR", {
//                         style: "currency",
//                         currency: "BRL",
//                       })
//                       .replace("R$", "")
//                   : "0,00"}
//               </label>
//             </div>
//           </Col>
//           <Col
//             lg="3"
//             xl="3"
//             style={
//               this.state.tipo === "sonho"
//                 ? { display: "none" }
//                 : { display: "block" }
//             }
//           >
//             <div>
//               <label>
//                 <b>Valor da parcela</b>
//               </label>
//               <label>
//                 {this.state.parcela != null
//                   ? this.state.parcela
//                       .toLocaleString("pt-BR", {
//                         style: "currency",
//                         currency: "BRL",
//                       })
//                       .replace("R$", "")
//                   : "0,00"}
//               </label>
//             </div>
//           </Col>
//           <Col lg="12" xl="12">
//             <div>
//               <label
//                 style={{ textAlign: "center", fontSize: 14, marginBottom: 0 }}
//               >
//                 <b>Imagem:</b> (clique sobre a imagem para trocar a foto)
//               </label>
//               <input
//                 type="file"
//                 id="imagem"
//                 onChange={(e) => {
//                   this.imagem = e.target.value;
//                 }}
//                 style={{ position: "fixed", left: "-4000px" }}
//               ></input>
//             </div>
//             <div>
//               <div id="linha">
//                 {this.state.editando != null &&
//                   this.imagem != null &&
//                   this.mostrarThumb()}
//               </div>
//             </div>
//           </Col>
//         </Row>
//         <Row>
//           <Col
//             lg="12"
//             xl="12"
//             style={{ display: "flex", justifyContent: "center" }}
//           >
//             <button
//               className="featured-button"
//               onClick={() => this.salvar(true)}
//               style={
//                 (console.log(this.state.idSonhoPadrao),
//                 (this.state.novoValorAcumulado == null ||
//                   this.state.novoValorAcumulado === "") &&
//                 this.state.tipo !== "plano" &&
//                 this.state.idSonhoPadrao != global.idSonhoAposentadoria
//                   ? {
//                       marginLeft: "10px",
//                       backgroundImage:
//                         "url(" +
//                         require("../../assets/img/theme/botao-destaque.png") +
//                         ")",
//                       backgroundSize: "cover",
//                       textTransform: "uppercase",
//                     }
//                   : { display: "none" })
//               }
//             >
//               Transformar em Plano
//             </button>
//             <button
//               className="general-button aux-button dark with-left-margin nav-item"
//               onClick={() => this.salvar(false)}
//               style={
//                 this.state.novoValorAcumulado == null ||
//                 this.state.novoValorAcumulado === ""
//                   ? {
//                       marginLeft: "10px",
//                       minWidth: 200,
//                       textTransform: "uppercase",
//                     }
//                   : { display: "none" }
//               }
//             >
//               Salvar
//             </button>
//             <button
//               className="featured-button"
//               onClick={() => {
//                 this.setState(
//                   { novoValorAcumulado: "" },
//                   this.calcularEAtualizarParcelaEValorFuturo
//                 );
//                 $("#simuladoVA").val("");
//               }}
//               style={
//                 this.state.novoValorAcumulado != null &&
//                 this.state.novoValorAcumulado !== ""
//                   ? {
//                       backgroundImage:
//                         "url(" +
//                         require("../../assets/img/theme/botao-destaque.png") +
//                         ")",
//                     }
//                   : { display: "none" }
//               }
//             >
//               Limpar
//             </button>
//           </Col>
//         </Row>
//       </SkyLight>
//     </div>
//   );

//   buscarPercentualRealizado = (sonhoId, rowIndex) => {
//     //var url = global.server_api + 'api/sonho/planos/familia/' + this.state.familiaId + '/realizado/' + sonhoId;
//     var url =
//       global.server_api_new +
//       global.apiToken +
//       "/sonho/planos/familia/" +
//       this.state.familiaId +
//       "/realizado/" +
//       sonhoId;

//     var config = {
//       headers: {
//         Authorization: "bearer " + this.state.accessToken,
//         "Access-Control-Allow-Origin": "*",
//         "Access-Control-Allow-Headers": "Authorization",
//         "Access-Control-Allow-Methods":
//           "GET, POST, OPTIONS, PUT, PATCH, DELETE",
//       },
//     };

//     axios.get(url, config).then((res) => {
//       if (res.data.success === true) {
//         if (res.data.singleResult == null) {
//           console.log("Não veio nenhum resultado");
//           return;
//         }

//         var dreamList = this.state.dreamList;
//         //console.log(dreamList);
//         if (dreamList != null && dreamList[rowIndex] != null) {
//           if (dreamList[rowIndex].sonhoId !== sonhoId) {
//             console.log(
//               "Pegamos o rowIndex errado para o sonhoId",
//               sonhoId,
//               rowIndex
//             );
//           } else {
//             dreamList[rowIndex].percentualRealizadoFuturo =
//               res.data.singleResult.percentualRealizadoFuturo != null
//                 ? res.data.singleResult.percentualRealizadoFuturo
//                 : 0;

//             dreamList[rowIndex].valorAcumulado =
//               res.data.singleResult.realizado != null
//                 ? res.data.singleResult.realizado
//                 : 0;

//             this.setState({ dreamList });
//           }
//         }
//       } else {
//         console.log(res.data.exception);
//       }
//     });
//   };

//   salvar = async (transformarEmPlano) => {
//     var currentScroll = [
//       document.documentElement.scrollLeft || document.body.scrollLeft,
//       document.documentElement.scrollTop || document.body.scrollTop,
//     ];

//     global.spinnerShow($);

//     var item = {
//       familiaId:
//         this.state.familiaId != null ? parseInt(this.state.familiaId) : null,
//       empresaId:
//         this.state.empresaId != null ? parseInt(this.state.empresaId) : null,
//       descricao: this.state.descricao,
//       imagem: this.imagem,
//       statusSonho: this.state.status != null ? this.state.status : 0,
//       sonhoId: this.state.id,
//       planoId: this.state.planoId,
//       tipo: this.state.tipo,
//       categoriasonho: this.state.idSonhoPadrao,
//       //Dados do Plano
//       dataInicial: this.state.dataInicial, //Data que criou o plano (nao editavel)
//       prazo: this.state.prazo, //Meses
//       dataFinal: this.state.dataFinal, //Mesmo dia (x meses para frente - calculado automicamente)
//       valorEstimadoPresente: this.state.valorPresente, //Valor estimado hoje
//       inflacao: this.state.inflacao, //Inflacao
//       valorEstimadoFuturo:
//         this.state.valorFuturo != null && this.state.valorFuturo !== ""
//           ? parseFloat(this.state.valorFuturo).toFixed(2)
//           : null, //VEF = VEP * (1 + inflação/100) ^ ( prazo / 12 )
//       taxa: this.state.taxa, //Taxa do Rendimento
//       parcela:
//         this.state.parcela != null && this.state.parcela !== ""
//           ? parseFloat(this.state.parcela).toFixed(2)
//           : null, //Valor da Parcela (PMT [TAXA] do excel)
//       ordemPrioridade: this.state.ordemPrioridade, //Sempre entra no final - Drag Drop
//     };

//     //var url = global.server_api + 'api/sonho' + (this.state.id != null ? "/" + this.state.id: "");
//     var url =
//       global.server_api_new +
//       global.apiToken +
//       "/sonho" +
//       (this.state.id != null ? "/" + this.state.id : "");

//     var config = {
//       headers: {
//         Authorization: "bearer " + this.state.accessToken,
//         "Access-Control-Allow-Origin": "*",
//         "Access-Control-Allow-Headers": "Authorization",
//         "Access-Control-Allow-Methods":
//           "GET, POST, OPTIONS, PUT, PATCH, DELETE",
//       },
//     };

//     //console.log(item, url);
//     var idRetorno = 0;
//     var idredirectAposent = this.state.idSonhoPadrao;
//     await axios.post(url, item, config).then((res) => {
//       global.spinnerHide($, currentScroll);

//       if (res.data.success === true) {
//         if (transformarEmPlano) {
//           this.atualizarDados();

//           if (res.data.singleResult && res.data.singleResult.id) {
//             this.setState({ id: res.data.singleResult.id });
//           }

//           $("#plano").click();
//           this.setState({
//             taxa: global.taxa,
//             inflacao: global.inflacao,
//           });
//         } else {
//           idRetorno = res.data.singleResult.id;
//           this.limparSelecao();
//           this.modal.hide();
//           this.atualizarDados();

//           if (idredirectAposent == global.idSonhoAposentadoria) {
//             sessionStorage.setItem("idAposentadoria", idRetorno);
//             this.props.history.push("/admin/aposentadoria");
//           }
//         }
//       } else {
//         console.log(res.data.exception);
//         const options = {
//           title: "Erro ao Salvar Banco",
//           message: res.data.exception.Message,
//           buttons: [
//             {
//               label: "Ok",
//             },
//           ],
//         };

//         confirmAlert(options);
//       }
//     });
//   };

//   render() {
//     return (
//       <>
//         {this.renderRedirect()}
//         {this.mostrarPopup()}
//         <div style={{ padding: "0 5%", marginTop: 20 }}>
//           <div className="home-summary-top" style={{ height: "100vh" }}>
//             <Row>
//               <Col lg="12" xl="12">
//                 <div>
//                   <Card>
//                     <CardHeader
//                       className="border-0"
//                       style={{ padding: "30px 15px 30px 0" }}
//                     >
//                       <Row className="align-items-center">
//                         <Col xs="12" lg="4" md="12">
//                           <img
//                             src={require("../../assets/img/theme/icone-balao.png")}
//                             className="before-title-img"
//                             alt=""
//                           />
//                           <h3 className="mb-0 chart-title">Planos & Sonhos</h3>
//                         </Col>
//                         <Col xs="12" lg="8" md="12">
//                           <Row className="d-flex justify-content-end">
//                             <NavItem
//                               style={{ listStyleType: "none", marginTop: 10 }}
//                             >
//                               <NavLink
//                                 className="nav-link-icon aux-button mr-3"
//                                 to="/admin/gestao-planos-sonhos"
//                                 tag={Link}
//                               >
//                                 <span className="nav-link-inner--text inner">
//                                   Gestão de Planos
//                                 </span>
//                               </NavLink>
//                             </NavItem>
//                             <NavItem
//                               style={{ listStyleType: "none", marginTop: 10 }}
//                             >
//                               <NavLink
//                                 className="nav-link-icon aux-button mr-3"
//                                 to="/admin/destinacao-patrimonio"
//                                 tag={Link}
//                               >
//                                 <span className="nav-link-inner--text">
//                                   Destinação de Patrimônio
//                                 </span>
//                               </NavLink>
//                             </NavItem>
//                             <div style={{ marginTop: 10 }}>
//                               <div
//                                 className="nav-link-icon featured-button  mr-3"
//                                 style={{
//                                   backgroundImage:
//                                     "url(" +
//                                     require("../../assets/img/theme/botao-destaque.png") +
//                                     ")",
//                                   backgroundSize: "cover",
//                                 }}
//                                 onClick={() => global.showModal(this.modal)}
//                               >
//                                 <span className="nav-link-inner--text">
//                                   Novo Plano ou Sonho
//                                 </span>
//                               </div>
//                             </div>
//                           </Row>
//                         </Col>
//                       </Row>
//                     </CardHeader>
//                   </Card>
//                 </div>
//               </Col>
//             </Row>
//             <Row className="dream-list">
//               <Col lg="12" xl="5" md="12" style={{ padding: 0 }}>
//                 <Card>
//                   <div>{this.mostrarBucketList()}</div>
//                 </Card>
//               </Col>
//               <Col lg="12" xl="7" md="12" style={{ zIndex: 1 }}>
//                 <div className="shadow" style={{ borderRadius: 15 }}>
//                   <div>
//                     <Row
//                       onDrop={(event) => {
//                         console.log("Droped!!!");
//                       }}
//                       className="dream-general mb-5"
//                     >
//                       {this.renderDreamCards()}
//                     </Row>
//                   </div>
//                 </div>
//               </Col>
//             </Row>
//           </div>
//         </div>
//       </>
//     );
//   }
// }

// export default PlanosSonhos;
