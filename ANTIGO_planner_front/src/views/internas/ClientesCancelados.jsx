import React from "react";
import { Redirect } from "react-router-dom";
import MUIDataTable from "mui-datatables";
import axios from "axios";
import $ from "jquery";
import { createMuiTheme, MuiThemeProvider } from "@material-ui/core/styles";
import { Row, Card, CardHeader } from "reactstrap";

class ClientesCancelados extends React.Component {
  moment = require("moment");

  state = {
    page: 0,
    count: 1,
    numeroItensPorPagina: 15,
  };

  getMuiTheme = () =>
    createMuiTheme({
      overrides: {
        MuiTableRow: {
          root: {
            "&:nth-of-type(odd)": {
              backgroundColor: "#f8f9fe",
            },
          },
        },
        MUIDataTableSelectCell: {
          fixedHeaderCommon: {
            backgroundColor: "unset",
          },
        },
        MuiTableCell: {
          root: {
            fontFamily: "unset",
            fontSize: "unset",
          },
          footer: {
            fontWeight: 700,
            borderRadius: "0 0 15px 15px",
          },
        },
        MUIDataTableHeadCell: {
          fixedHeaderYAxis: {
            fontWeight: 700,
          },
        },
        MuiPaper: {
          root: {
            margin: "10px 40px",
          },
          elevation4: {
            borderRadius: 15,
          },
        },
        MUIDataTableToolbar: {
          left: {
            margin: 10,
          },
        },
        MuiToolbar: {
          regular: {
            backgroundColor: "#e4eaf4",
            borderRadius: "15px 15px 0 0",
            "&:last-child": {
              borderRadius: 0,
            },
          },
        },
        MUIDataTablePagination: {
          root: {
            "&:last-child": {
              margin: "0px 24px 0px 24px",
              padding: 0,
            },
          },
        },
      },
    });

  componentDidMount() {
    this.setState(
      {
        accessToken: localStorage.getItem("access-token"),
        empresaId: localStorage.getItem("empresa-id"),
        planejadorId: localStorage.getItem("planejador-id"),
        planejadorPrincipal: localStorage.getItem("planejador-principal"),
        tipoUsuario: localStorage.getItem("tipo-usuario"),
      },
      function () {
        //Se é o planejador senior ou o admin
        if (
          parseInt(this.state.tipoUsuario) === 0 ||
          (parseInt(this.state.planejadorPrincipal) === 1 &&
            parseInt(this.state.tipoUsuario) === 1)
        ) {
          this.getData();
        }
        global.mostrarAjudaDaPaginaAutomaticamente(this);
      }
    );
  }

  setRedirect = () => {
    this.setState({
      redirect: true,
    });
  };

  renderRedirect = () => {
    if (this.state.redirect) {
      return <Redirect to="/auth/login" />;
    }
  };

  getData = () => {
    if (this.state.interval != null) {
      clearInterval(this.state.interval);
    }

    if (this.state.accessToken == null) {
      this.setRedirect();
      return;
    }

    //var url = global.server_api + 'api/familia/filtroCancelado/a0285a3d-21a0-4efd-8f3f-8808c6a25c68'; // TROCAR PARA API COM LISTA DE INATIVOS
    var url =
      global.server_api_new + global.apiToken + "/familia/filtroCancelado";

    /*var filtro = {
            Filtro: this.state.textoDigitado,
            Ordenacao: this.state.sortField != null ? this.state.sortField : "dataUltimaAtualizacao", // FILTRO PARA TRAZER NA ORDEM DA INATIVAÇÃO MAIS RECENTE
            NumeroPagina: this.state.numeroPagina != null ? this.state.numeroPagina : 1,
        }*/

    var config = {
      headers: {
        Authorization: "bearer " + this.state.accessToken,
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Headers": "Authorization",
        "Access-Control-Allow-Methods":
          "GET, POST, OPTIONS, PUT, PATCH, DELETE",
      },
    };

    var currentScroll = [
      document.documentElement.scrollLeft || document.body.scrollLeft,
      document.documentElement.scrollTop || document.body.scrollTop,
    ];

    global.spinnerShow($);

    axios.get(url, config).then((res) => {
      var data = [];

      if (res.data.results != null) {
        res.data.results.forEach((item) => {
          data.push(item);
        });
      }

      global.spinnerHide($, currentScroll);

      this.setState({ clientesCancelados: data });
    });
  };

  render() {
    const { clientesCancelados, page, numeroItensPorPagina } = this.state;

    const options = {
      print: false,
      download: false,
      filter: false,
      pagination: true,
      page: page,
      selectableRows: "none",
      textLabels: global.textLabels,
      rowsPerPage: numeroItensPorPagina,
      onTableChange: (action, tableState) => {
        switch (action) {
          case "changePage":
            this.setState({ page: tableState.page + 1 });
            break;
          default:
            return;
        }
      },
    };

    const columns = [
      {
        name: "id",
        label: "id",
        options: {
          display: false,
          viewColumns: false,
          filter: false,
        },
      },
      {
        name: "nome",
        label: "Nome",
        options: {
          display: true,
          viewColumns: true,
          filter: false,
        },
      },
      {
        name: "planejador.nome",
        label: "Planejador",
        options: {
          display: true,
          viewColumns: true,
          filter: false,
        },
      },
      {
        name: "planejador.sobrenome",
        label: "Sobrenome",
        options: {
          display: true,
          viewColumns: true,
          filter: false,
        },
      },
      {
        name: "dataCriacao",
        label: "Data Criação",
        options: {
          display: true,
          viewColumns: true,
          filter: true,
          customBodyRender: (value) => {
            if (value !== null) {
              return <>{this.moment(value).format("DD/MM/YYYY")}</>;
            } else {
              return <></>;
            }
          },
        },
      },
      {
        name: "fimContrato",
        label: "Data Inativação",
        options: {
          display: true,
          viewColumns: true,
          filter: true,
          customBodyRender: (value, tableMeta) => {
            if (value !== null) {
              return <>{this.moment(value).format("DD/MM/YYYY")}</>;
            } else {
              return <>---------------</>;
            }
          },
        },
      },
    ];

    return (
      <div>
        <Card>
          <CardHeader className="border-0">
            <Row className="align-items-center">
              <div className="col">
                <img
                  src={require("../../assets/img/theme/icone-calendario.png")}
                  alt="ícone calendário"
                  className="before-title-img"
                />
                <h3 className="mb-0 chart-title">Clientes cancelados</h3>
              </div>
            </Row>
          </CardHeader>
        </Card>
        <MuiThemeProvider theme={this.getMuiTheme()}>
          <MUIDataTable
            data={clientesCancelados}
            columns={columns}
            options={options}
          />
        </MuiThemeProvider>
      </div>
    );
  }
}

export default ClientesCancelados;
