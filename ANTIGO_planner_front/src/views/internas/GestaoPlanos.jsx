import React from "react";
import Select from "react-select";
import { Redirect, Link } from "react-router-dom";
import { CircularProgress } from "@material-ui/core";
import MUIDataTable from "mui-datatables";
import axios from "axios";
import $ from "jquery";
import { confirmAlert } from "react-confirm-alert";
import ExcelFormulas from "../../assets/js/ExcelFormulas.js";
import CurrencyInput from "react-currency-input";
import { createMuiTheme, MuiThemeProvider } from "@material-ui/core/styles";

import CustomFooter from "../../views/internas/CustomFooter.jsx";

import { Col, Row, Card, CardHeader, NavItem, NavLink } from "reactstrap";

class GestaoPlanos extends React.Component {
  moment = require("moment");

  gestaoPlanoCampoCalculado = [
    { value: 0, label: "Parcela" },
    { value: 1, label: "Taxa" },
    { value: 2, label: "Prazo" },
    { value: 3, label: "Valor do Plano no Presente" },
  ];

  originalData = [];

  state = {
    data: [],
    campoCalculado: 0,
    sumario: [],
    sumarioAlterado: [],
    diferencaSumario: [],
    footerColumns: [
      "",
      "Valor Presente",
      "Valor Futuro",
      "Valor Acumulado",
      "Parcela",
    ],
  };

  getMuiTheme = () =>
    createMuiTheme({
      overrides: {
        MuiTableRow: {
          root: {
            "&:nth-of-type(odd)": {
              backgroundColor: "#f8f9fe",
            },
            "& input": {
              background: "transparent",
            },
          },
        },
        MUIDataTableSelectCell: {
          fixedHeaderCommon: {
            backgroundColor: "unset",
          },
        },
        MuiTableCell: {
          root: {
            fontFamily: "unset",
            borderTop: "none",
            padding: "16px 10px",
          },
          footer: {
            color: "#FFF",
            fontWeight: 700,
          },
        },
        MUIDataTableHeadCell: {
          fixedHeaderYAxis: {
            fontWeight: 700,
            zIndex: 8,
          },
        },
        MuiPaper: {
          root: {
            overflow: "hidden",
            margin: "0 0 30px 0",
          },
        },
        MuiToolbar: {
          regular: {
            backgroundColor: "#e4eaf4",
            height: 70,
          },
        },
        MUIDataTableBodyCell: {
          stackedCommon: {
            height: "fit-content!important",
          },
        },
      },
    });

  buscarPercentualRealizado = (sonhoId, rowIndex) => {
    //var url = global.server_api + 'api/sonho/planos/familia/' + this.state.familiaId + '/realizado/' + sonhoId;
    var url =
      global.server_api_new +
      global.apiToken +
      "/sonho/planos/familia/" +
      this.state.familiaId +
      "/realizado/" +
      sonhoId;

    //console.log(url);
    var config = {
      headers: {
        Authorization: "bearer " + this.state.accessToken,
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Headers": "Authorization",
        "Access-Control-Allow-Methods":
          "GET, POST, OPTIONS, PUT, PATCH, DELETE",
      },
    };

    axios.get(url, config).then((res) => {
      //console.log(res.data);
      if (res.data.success === true) {
        if (res.data.singleResult == null) {
          console.log("Não veio nenhum resultado");
          return;
        }
        var data = this.state.data;
        var originalData = this.originalData;
        //console.log(this.state.data);
        //console.log(originalData);
        if (data != null && data[rowIndex] != null) {
          if (data[rowIndex].sonhoId !== sonhoId) {
            console.log(
              "Pegamos o rowIndex errado para o sonhoId",
              sonhoId,
              rowIndex
            );
          } else {
            data[rowIndex].realizado =
              res.data.singleResult.realizado != null
                ? res.data.singleResult.realizado
                : 0;

            originalData[rowIndex].realizado =
              res.data.singleResult.realizado != null
                ? res.data.singleResult.realizado
                : 0;

            this.originalData = originalData;
            //console.log(this.originalData);
            this.setState({ data: data }, function () {
              if (this.state.campoCalculado === 0) {
                this.calculoParcela(rowIndex);
              } else if (this.state.campoCalculado === 1) {
                this.calculoTaxaTemp(rowIndex);
              } else if (this.state.campoCalculado === 2) {
                this.calculoPrazoTemp(rowIndex);
              } else if (this.state.campoCalculado === 3) {
                this.calculoValorPlanoPresente(rowIndex);
              }
            });
          }
        }
      } else {
        console.log(res.data.exception);
      }
    });
  };

  //OK
  calculoParcela = (rowIndex) => {
    var data = this.state.data;
    try {
      if (data[rowIndex].ehAposentadoria === true) {
        //console.log("linha", rowIndex, "Sai..");
        return;
      }
    } catch (e) {
      console.log(e);
    }

    var taxa = data[rowIndex].taxa;
    //console.log(taxa);
    //console.log(data[rowIndex]);
    var inflacao = data[rowIndex].inflacao;
    var prazo = data[rowIndex].prazoRestante;
    var prazoEmAnos = prazo / 12;

    var acumulado =
      data[rowIndex].realizado != null ? data[rowIndex].realizado : 0;
    var valorEstimadoPresente = data[rowIndex].valorEstimadoPresente;

    var valorEstimadoFuturo = parseFloat(
      parseFloat(
        ExcelFormulas.FV(inflacao, prazoEmAnos, valorEstimadoPresente)
      ).toFixed(2)
    );

    var parcela = parseFloat(
      (-ExcelFormulas.PMT(
        taxa,
        prazo,
        -acumulado,
        valorEstimadoFuturo,
        0
      )).toFixed(2)
    );

    data[rowIndex].parcela = parcela;
    data[rowIndex].valorEstimadoFuturo = valorEstimadoFuturo;

    this.setState({ data }, function () {
      this.preencherSumario();
    });
  };

  calculoPrazoTemp = (rowIndex) => {
    if (this.state.interval != null) {
      clearInterval(this.state.interval);
    }

    const $this = this;

    const interval = setTimeout(function () {
      $this.calculoPrazo(rowIndex);
    }, 1000);

    this.setState({ interval });
  };

  //Ok
  calculoPrazo = (rowIndex) => {
    var data = this.state.data;

    var taxa = data[rowIndex].taxa;
    var inflacao = data[rowIndex].inflacao;
    var parcela = data[rowIndex].parcela;

    var acumulado =
      data[rowIndex].realizado != null ? data[rowIndex].realizado : 0;
    var valorEstimadoPresente = data[rowIndex].valorEstimadoPresente;

    if (
      parcela == null ||
      parcela === "" ||
      inflacao == null ||
      inflacao === "" ||
      taxa == null ||
      taxa === "" ||
      valorEstimadoPresente == null ||
      valorEstimadoPresente === ""
    ) {
      return;
    }

    var valorEstimadoFuturo = valorEstimadoPresente;

    var prazoEstimado = null;
    var iteracoes = 0;

    do {
      iteracoes++;

      console.log("valorEstimadoFuturo", valorEstimadoFuturo);

      var novoPrazo = Math.trunc(
        ExcelFormulas.NPER(taxa, -parcela, acumulado, valorEstimadoFuturo, 1)
      );

      console.log("novoPrazo", novoPrazo);

      //Se parou de incrementar o prazo, é porque sao iguais
      if (prazoEstimado != null && novoPrazo === prazoEstimado) {
        break;
      }
      //Se nao, atualiza o prazo estimado
      else {
        prazoEstimado = novoPrazo;
      }

      valorEstimadoFuturo = ExcelFormulas.FV(
        inflacao,
        prazoEstimado / 12,
        valorEstimadoPresente
      );

      console.log("novo valorEstimadoFuturo", valorEstimadoFuturo);
    } while (iteracoes < 100);

    var prazo = prazoEstimado;
    valorEstimadoFuturo = parseFloat(
      ExcelFormulas.FV(inflacao, prazo / 12, valorEstimadoPresente).toFixed(2)
    );
    //parcela = - (ExcelFormulas.PMT(taxa, prazo, -acumulado, valorEstimadoFuturo, 0));

    data[rowIndex].valorEstimadoFuturo = valorEstimadoFuturo;
    data[rowIndex].prazo = prazo;
    // data[rowIndex].parcela = parcela;

    this.setState({ data }, function () {
      this.preencherSumario();
    });
  };

  calculoTaxaTemp = (rowIndex) => {
    if (this.state.interval != null) {
      clearInterval(this.state.interval);
    }

    const $this = this;

    const interval = setTimeout(function () {
      $this.calculoTaxa(rowIndex);
    }, 1000);

    this.setState({ interval });
  };

  //Ok
  calculoTaxa = (rowIndex) => {
    var data = this.state.data;

    var inflacao = data[rowIndex].inflacao;
    var prazo = data[rowIndex].prazoRestante;
    var prazoEmAnos = prazo / 12;
    var parcela = data[rowIndex].parcela;

    var acumulado =
      data[rowIndex].realizado != null ? data[rowIndex].realizado : 0;
    var valorEstimadoPresente = data[rowIndex].valorEstimadoPresente;

    var valorEstimadoFuturo = parseFloat(
      ExcelFormulas.FV(inflacao, prazoEmAnos, valorEstimadoPresente).toFixed(2)
    );

    console.log(prazo, parcela, valorEstimadoFuturo);

    var temp =
      ExcelFormulas.RATE(prazo, -parcela, -acumulado, valorEstimadoFuturo, 1) *
      100;

    console.log(temp);

    var taxa = parseFloat(temp.toFixed(4));

    data[rowIndex].valorEstimadoFuturo = valorEstimadoFuturo;
    data[rowIndex].taxa = taxa;

    this.setState({ data }, function () {
      this.preencherSumario();
    });
  };

  // calculoTaxa2 = (rowIndex) => {
  //   var data = this.state.data;

  //   var inflacao = data[rowIndex].inflacao;
  //   var prazo = data[rowIndex].prazo;
  //   var prazoEmAnos = (prazo / 12);
  //   var parcela = data[rowIndex].parcela;

  //   var acumulado = data[rowIndex].realizado != null ? data[rowIndex].realizado : 0;
  //   var valorEstimadoPresente = data[rowIndex].valorEstimadoPresente;

  //   var valorEstimadoFuturo = ExcelFormulas.FV(inflacao, prazoEmAnos, valorEstimadoPresente);

  //   var taxaTemp = 0;
  //   var parcelaTemp = - (ExcelFormulas.PMT(taxaTemp, prazo, -acumulado, valorEstimadoFuturo, 0));

  //   console.log('Passo', 0, taxaTemp, parcelaTemp, 'valor futuro', valorEstimadoFuturo, 'prazo', prazo);

  //   var i = 1;

  //   while(parcelaTemp > parcela && parcelaTemp > 0 && taxaTemp < 100) {
  //     taxaTemp += 0.1;
  //     parcelaTemp = - (ExcelFormulas.PMT(taxaTemp, prazo, -acumulado, valorEstimadoFuturo, 0));
  //   }

  //   i = 1;
  //   var decresceuTaxa = false;

  //   while (parcelaTemp < parcela && taxaTemp >= 0){
  //     taxaTemp -= 0.01;
  //     parcelaTemp = - (ExcelFormulas.PMT(taxaTemp, prazo, -acumulado, valorEstimadoFuturo, 0));
  //     decresceuTaxa = true;
  //   }

  //   //Ajuste por causa da ultima interacao do loop anterior
  //   if(decresceuTaxa){
  //     taxaTemp += 0.01;
  //     parcelaTemp = - (ExcelFormulas.PMT(taxaTemp, prazo, -acumulado, valorEstimadoFuturo, 0));
  //   }

  //   var taxa = taxaTemp;
  //   parcela = parseFloat(parcelaTemp.toFixed(2));

  //   data[rowIndex].valorEstimadoFuturo = parseFloat(valorEstimadoFuturo.toFixed(2));
  //   data[rowIndex].taxa = taxa;
  //   data[rowIndex].parcela = parcela.toFixed(2);

  //   this.setState({data}, function(){
  //   this.preencherSumario();
  // });
  // }

  //OK
  calculoValorPlanoPresente = (rowIndex) => {
    var data = this.state.data;

    var taxa = data[rowIndex].taxa;
    var inflacao = data[rowIndex].inflacao;
    var prazo = data[rowIndex].prazoRestante;
    var prazoEmAnos = prazo / 12;

    var acumulado =
      data[rowIndex].realizado != null ? data[rowIndex].realizado : 0;
    var valorEstimadoFuturo = data[rowIndex].valorEstimadoFuturo;
    var valorEstimadoPresente = parseFloat(
      ExcelFormulas.PV(inflacao, prazoEmAnos, valorEstimadoFuturo).toFixed(2)
    );

    var parcela = parseFloat(
      (-ExcelFormulas.PMT(
        taxa,
        prazo,
        -acumulado,
        valorEstimadoFuturo,
        0
      )).toFixed(2)
    );

    data[rowIndex].valorEstimadoPresente = valorEstimadoPresente;
    data[rowIndex].parcela = parcela;

    this.setState({ data }, function () {
      this.preencherSumario();
    });
  };

  setRedirect = () => {
    this.setState({
      redirect: true,
    });
  };

  renderRedirect = () => {
    if (this.state.redirect) {
      return <Redirect to="/auth/login" />;
    }
  };

  componentDidMount() {
    this.setState(
      {
        accessToken: localStorage.getItem("access-token"),
        empresaId: localStorage.getItem("empresa-id"),
        familiaId: localStorage.getItem("familia-id"),
        planejadorId: localStorage.getItem("planejador-id"),
      },
      function () {
        if (this.state.accessToken == null) {
          this.setRedirect();
          return;
        }

        this.getData();
        global.mostrarAjudaDaPaginaAutomaticamente(this);
      }
    );
  }

  setRedirect = () => {
    this.setState({
      redirect: true,
    });
  };

  renderRedirect = () => {
    if (this.state.redirect) {
      return <Redirect to="/auth/login" />;
    }
  };

  getData = () => {
    if (this.state.interval != null) {
      clearInterval(this.state.interval);
    }

    //var url = global.server_api + 'api/sonho/planos/familia/' + this.state.familiaId + "/emAndamento";
    var url =
      global.server_api_new +
      global.apiToken +
      "/sonho/planos/familia/" +
      this.state.familiaId +
      "/emAndamento";

    // var filtro = {
    //   Filtro: this.state.textoDigitado,
    //   Ordenacao: this.state.sortField != null ? this.state.sortField : "Data Desc"
    // }

    var config = {
      headers: {
        Authorization: "bearer " + this.state.accessToken,
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Headers": "Authorization",
        "Access-Control-Allow-Methods":
          "GET, POST, OPTIONS, PUT, PATCH, DELETE",
      },
    };

    var currentScroll = [
      document.documentElement.scrollLeft || document.body.scrollLeft,
      document.documentElement.scrollTop || document.body.scrollTop,
    ];

    global.spinnerShow($);

    axios.get(url, config).then((res) => {
      //console.log(res.data.results);
      var data = [];
      var originalData = [];

      if (res.data.results != null) {
        res.data.results.forEach((item) => {
          data.push(item);
          originalData.push(JSON.parse(JSON.stringify(item)));
        });
      }
      console.log(data);

      this.originalData = originalData;

      this.setState({ data: data }, function () {
        global.spinnerHide($, currentScroll);
        if (this.state.data != null) {
          this.state.data.forEach((item, key) => {
            this.buscarPercentualRealizado(item.sonhoId, key);
          });
        }
        this.preencherSumario();
      });
    });
  };

  preencherSumario = () => {
    var totalValorPresente = 0;
    var totalValorFuturo = 0;
    var totalAcumulado = 0;
    var totalParcela = 0;

    if (this.originalData != null) {
      this.originalData.forEach((item) => {
        totalValorPresente += item.valorEstimadoPresente;
        totalValorFuturo += item.valorEstimadoFuturo;
        totalAcumulado += item.realizado;
        item.parcela > 0 ? (totalParcela += item.parcela) : (totalParcela += 0);
      });
    }

    this.preencherSumarioAlterado(
      totalValorPresente,
      totalValorFuturo,
      totalAcumulado,
      totalParcela
    );

    totalValorPresente = totalValorPresente
      .toLocaleString("pt-BR", { style: "currency", currency: "BRL" })
      .replace("R$", "");
    totalValorFuturo = totalValorFuturo
      .toLocaleString("pt-BR", { style: "currency", currency: "BRL" })
      .replace("R$", "");
    totalAcumulado = totalAcumulado
      .toLocaleString("pt-BR", { style: "currency", currency: "BRL" })
      .replace("R$", "");
    totalParcela = totalParcela
      .toLocaleString("pt-BR", { style: "currency", currency: "BRL" })
      .replace("R$", "");

    var sumario = [
      "Plano Atual",
      totalValorPresente,
      totalValorFuturo,
      totalAcumulado,
      totalParcela,
    ];

    this.setState({ sumario: sumario });
  };

  preencherSumarioAlterado = (
    totalValorPresenteOriginal,
    totalValorFuturoOriginal,
    totalAcumuladoOriginal,
    totalParcelaOriginal
  ) => {
    var totalValorPresente = 0;
    var totalValorFuturo = 0;
    var totalAcumulado = 0;
    var totalParcela = 0;

    if (this.state.data != null) {
      this.state.data.forEach((item) => {
        totalValorPresente += item.valorEstimadoPresente;
        totalValorFuturo += item.valorEstimadoFuturo;
        totalAcumulado += item.realizado;
        totalParcela += item.parcela;
      });
    }

    this.preencherDiferencaSumario(
      totalValorPresente - totalValorPresenteOriginal,
      totalValorFuturo - totalValorFuturoOriginal,
      totalParcela - totalParcelaOriginal
    );

    var sumarioAlterado = [];

    if (
      totalValorPresenteOriginal - totalValorPresente !== 0 ||
      totalValorFuturoOriginal - totalValorFuturo !== 0 ||
      totalParcelaOriginal - totalParcela !== 0
    ) {
      totalValorPresente = totalValorPresente
        .toLocaleString("pt-BR", { style: "currency", currency: "BRL" })
        .replace("R$", "");
      totalValorFuturo = totalValorFuturo
        .toLocaleString("pt-BR", { style: "currency", currency: "BRL" })
        .replace("R$", "");
      totalAcumulado = totalAcumulado
        .toLocaleString("pt-BR", { style: "currency", currency: "BRL" })
        .replace("R$", "");
      totalParcela = totalParcela
        .toLocaleString("pt-BR", { style: "currency", currency: "BRL" })
        .replace("R$", "");

      sumarioAlterado = [
        "Novo Plano",
        totalValorPresente,
        totalValorFuturo,
        totalAcumulado,
        totalParcela,
      ];
    }

    this.setState({ sumarioAlterado: sumarioAlterado });
  };

  preencherDiferencaSumario = (
    diffValorPresente,
    diffValorFuturo,
    diffParcela
  ) => {
    var diferencaSumario = [];

    if (diffValorPresente !== 0 || diffValorFuturo !== 0 || diffParcela !== 0) {
      diffValorPresente = diffValorPresente
        .toLocaleString("pt-BR", { style: "currency", currency: "BRL" })
        .replace("R$", "");
      diffValorFuturo = diffValorFuturo
        .toLocaleString("pt-BR", { style: "currency", currency: "BRL" })
        .replace("R$", "");
      diffParcela = diffParcela
        .toLocaleString("pt-BR", { style: "currency", currency: "BRL" })
        .replace("R$", "");

      diferencaSumario = [
        "Diferença",
        diffValorPresente,
        diffValorFuturo,
        "-",
        diffParcela,
      ];
    }

    this.setState({ diferencaSumario: diferencaSumario });
  };

  cancelar = () => {
    this.setState({ data: this.originalData }, function () {
      this.preencherSumario();
    });
  };

  salvar = () => {
    //var url = global.server_api + 'api/sonho/planos/atualizarEmLote';
    var url =
      global.server_api_new + global.apiToken + "/sonho/planos/atualizarEmLote";

    var config = {
      headers: {
        Authorization: "bearer " + this.state.accessToken,
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Headers": "Authorization",
        "Access-Control-Allow-Methods":
          "GET, POST, OPTIONS, PUT, PATCH, DELETE",
      },
    };

    var currentScroll = [
      document.documentElement.scrollLeft || document.body.scrollLeft,
      document.documentElement.scrollTop || document.body.scrollTop,
    ];

    global.spinnerShow($);

    this.state.data.forEach((item) => {
      //console.log(item);
      item.prazo = item.prazoRestante;
    });

    axios.post(url, this.state.data, config).then((res) => {
      global.spinnerHide($, currentScroll);

      if (res.data.success === true) {
        window.location.reload();
      } else {
        console.log(res.data.exception);
        const options = {
          title: "Erro ao aplicar mudanças nos planos",
          message: res.data.exception.Message,
          buttons: [
            {
              label: "Ok",
            },
          ],
        };

        confirmAlert(options);
      }
    });
  };

  mudarCampoCalculado = (selectedOption) => {
    const campoCalculado = selectedOption.value;
    this.setState({ campoCalculado });
  };

  getOriginalData = (planoId) => {
    if (this.originalData != null && this.originalData.length > 0) {
      return this.originalData.find(function (item) {
        return item.planoId === planoId;
      });
    }

    return null;
  };

  render() {
    const columns = [
      {
        name: "planoId",
        options: {
          display: false,
          viewColumns: false,
          filter: false,
        },
      },
      {
        name: "descricao",
        label: "Plano",
        options: {
          display: true,
          viewColumns: true,
          filter: false,
        },
      },
      // {
      //   name: "prazo",
      //   label: "Prazo Inicial",
      //   options: {
      //     display: true,
      //     viewColumns: true,
      //     filter: false,
      //     customBodyRender: (value, tableMeta, updateValue) => {

      //       if(value == null) value = 0;

      //       return(
      //           <>
      //             {parseFloat(value).toFixed(0) +  ' meses'}
      //           </>
      //         );
      //     }
      //   }
      // },
      {
        name: "prazoRestante",
        label: "Prazo",
        options: {
          display: true,
          viewColumns: true,
          filter: false,
          customBodyRender: (value, tableMeta, updateValue) => {
            if (value == null) value = 0;

            var planoOriginal = this.getOriginalData(tableMeta.rowData[0]);
            var valorOriginal =
              planoOriginal != null && planoOriginal.prazoRestante != null
                ? planoOriginal.prazoRestante
                : 0;
            var icon = "";

            if (value < valorOriginal) {
              icon = "-";
            } else if (value > valorOriginal) {
              icon = "+";
            }

            if (this.state.campoCalculado === 2) {
              return (
                <>
                  {parseFloat(value).toFixed(0) + " meses"}{" "}
                  {icon === "-" ? (
                    <i
                      style={{
                        position: "absolute",
                        color: "#9633ff",
                        width: 130,
                      }}
                      className="fas fa-long-arrow-alt-down ml-1"
                    ></i>
                  ) : icon === "+" ? (
                    <i
                      style={{
                        position: "absolute",
                        color: "#9633ff",
                        width: 130,
                      }}
                      className="fas fa-long-arrow-alt-up ml-1"
                    ></i>
                  ) : (
                    <></>
                  )}
                </>
              );
            } else if (tableMeta.rowData[9] === true) {
              return (
                <>
                  {parseFloat(value)
                    .toLocaleString("pt-BR", {
                      style: "currency",
                      currency: "BRL",
                    })
                    .replace("R$", "")}{" "}
                  {icon === "-" ? (
                    <i
                      style={{ position: "absolute", color: "#9633ff" }}
                      className="fas fa-long-arrow-alt-down ml-1"
                    ></i>
                  ) : icon === "+" ? (
                    <i
                      style={{ position: "absolute", color: "#9633ff" }}
                      className="fas fa-long-arrow-alt-up ml-1"
                    ></i>
                  ) : (
                    <></>
                  )}
                </>
              );
            } else {
              return (
                <>
                  <CurrencyInput
                    id={
                      "prazoRestante_" +
                      tableMeta.rowData[0] +
                      "_" +
                      tableMeta.rowData[1]
                    }
                    style={{ width: "90%" }}
                    value={value}
                    thousandSeparator={"."}
                    precision="0"
                    suffix={" meses"}
                    onChangeEvent={(event, formattedValue, value) => {
                      var data = this.state.data;
                      data[tableMeta.rowIndex].prazoRestante = value;

                      this.setState({ data }, function () {
                        if (this.state.campoCalculado === 0) {
                          this.calculoParcela(tableMeta.rowIndex);
                        } else if (this.state.campoCalculado === 1) {
                          this.calculoTaxaTemp(tableMeta.rowIndex);
                        } else if (this.state.campoCalculado === 2) {
                          this.calculoPrazoTemp(tableMeta.rowIndex);
                        } else if (this.state.campoCalculado === 3) {
                          this.calculoValorPlanoPresente(tableMeta.rowIndex);
                        }
                      });
                    }}
                  />{" "}
                  {icon === "-" ? (
                    <i
                      style={{ position: "absolute", color: "#9633ff" }}
                      className="fas fa-long-arrow-alt-down ml-1"
                    ></i>
                  ) : icon === "+" ? (
                    <i
                      style={{ position: "absolute", color: "#9633ff" }}
                      className="fas fa-long-arrow-alt-up ml-1"
                    ></i>
                  ) : (
                    <></>
                  )}
                </>
              );
            }
          },
        },
      },
      {
        name: "valorEstimadoPresente",
        label: "Valor Estimado Presente",
        options: {
          display: true,
          viewColumns: true,
          filter: false,
          customBodyRender: (value, tableMeta, updateValue) => {
            if (value == null) value = 0;
            //console.log('table', tableMeta);
            var planoOriginal = this.getOriginalData(tableMeta.rowData[0]);
            var valorOriginal =
              planoOriginal != null &&
              planoOriginal.valorEstimadoPresente != null
                ? planoOriginal.valorEstimadoPresente
                : 0;
            var icon = "";

            if (value < valorOriginal) {
              icon = "-";
            } else if (value > valorOriginal) {
              icon = "+";
            }

            if (this.state.campoCalculado === 3) {
              return (
                <>
                  {parseFloat(value)
                    .toLocaleString("pt-BR", {
                      style: "currency",
                      currency: "BRL",
                    })
                    .replace("R$", "")}{" "}
                  {icon === "-" ? (
                    <i
                      style={{ position: "absolute", color: "#9633ff" }}
                      className="fas fa-long-arrow-alt-down ml-1"
                    ></i>
                  ) : icon === "+" ? (
                    <i
                      style={{ position: "absolute", color: "#9633ff" }}
                      className="fas fa-long-arrow-alt-up ml-1"
                    ></i>
                  ) : (
                    <></>
                  )}
                </>
              );
            } else if (tableMeta.rowData[9] === true) {
              return (
                <>
                  {parseFloat(value)
                    .toLocaleString("pt-BR", {
                      style: "currency",
                      currency: "BRL",
                    })
                    .replace("R$", "")}{" "}
                  {icon === "-" ? (
                    <i
                      style={{ position: "absolute", color: "#9633ff" }}
                      className="fas fa-long-arrow-alt-down ml-1"
                    ></i>
                  ) : icon === "+" ? (
                    <i
                      style={{ position: "absolute", color: "#9633ff" }}
                      className="fas fa-long-arrow-alt-up ml-1"
                    ></i>
                  ) : (
                    <></>
                  )}
                </>
              );
            } else {
              return (
                <>
                  <CurrencyInput
                    id={"valorpresente_" + tableMeta.rowIndex}
                    style={{ width: "90%" }}
                    value={value}
                    thousandSeparator={"."}
                    decimalSeparator={","}
                    precision="2" //prefix={'R$ '}
                    onChangeEvent={(event, formattedValue, value) => {
                      var data = this.state.data;
                      data[tableMeta.rowIndex].valorEstimadoPresente = value;

                      this.setState({ data }, function () {
                        if (this.state.campoCalculado === 0) {
                          this.calculoParcela(tableMeta.rowIndex);
                        } else if (this.state.campoCalculado === 1) {
                          this.calculoTaxaTemp(tableMeta.rowIndex);
                        } else if (this.state.campoCalculado === 2) {
                          this.calculoPrazoTemp(tableMeta.rowIndex);
                        } else if (this.state.campoCalculado === 3) {
                          this.calculoValorPlanoPresente(tableMeta.rowIndex);
                        }
                      });
                    }}
                  />{" "}
                  {icon === "-" ? (
                    <i
                      style={{ position: "absolute", color: "#9633ff" }}
                      className="fas fa-long-arrow-alt-down ml-1"
                    ></i>
                  ) : icon === "+" ? (
                    <i
                      style={{ position: "absolute", color: "#9633ff" }}
                      className="fas fa-long-arrow-alt-up ml-1"
                    ></i>
                  ) : (
                    <></>
                  )}
                </>
              );
            }
          },
        },
      },
      {
        name: "valorEstimadoFuturo",
        label: "Valor Estimado Futuro",
        options: {
          display: true,
          viewColumns: true,
          filter: false,
          customBodyRender: (value, tableMeta, updateValue) => {
            if (value == null) value = 0;

            var planoOriginal = this.getOriginalData(tableMeta.rowData[0]);
            var valorOriginal =
              planoOriginal != null && planoOriginal.valorEstimadoFuturo != null
                ? planoOriginal.valorEstimadoFuturo
                : 0;
            var icon = "";

            if (value < valorOriginal) {
              icon = "-";
            } else if (value > valorOriginal) {
              icon = "+";
            }

            if (this.state.campoCalculado !== 3) {
              return (
                <>
                  {parseFloat(value)
                    .toLocaleString("pt-BR", {
                      style: "currency",
                      currency: "BRL",
                    })
                    .replace("R$", "")}{" "}
                  {icon === "-" ? (
                    <i
                      style={{ position: "absolute", color: "#9633ff" }}
                      className="fas fa-long-arrow-alt-down ml-1"
                    ></i>
                  ) : icon === "+" ? (
                    <i
                      style={{ position: "absolute", color: "#9633ff" }}
                      className="fas fa-long-arrow-alt-up ml-1"
                    ></i>
                  ) : (
                    <></>
                  )}
                </>
              );
            } else {
              return (
                <>
                  <CurrencyInput
                    id={"valorfuturo_" + tableMeta.rowIndex}
                    style={{ width: "90%" }}
                    value={value}
                    thousandSeparator={"."}
                    decimalSeparator={","}
                    precision="2" //prefix={'R$ '}
                    onChangeEvent={(event, formattedValue, value) => {
                      var data = this.state.data;
                      data[tableMeta.rowIndex].valorEstimadoFuturo = value;

                      this.setState({ data }, function () {
                        if (this.state.campoCalculado === 0) {
                          this.calculoParcela(tableMeta.rowIndex);
                        } else if (this.state.campoCalculado === 1) {
                          this.calculoTaxaTemp(tableMeta.rowIndex);
                        } else if (this.state.campoCalculado === 2) {
                          this.calculoPrazoTemp(tableMeta.rowIndex);
                        } else if (this.state.campoCalculado === 3) {
                          this.calculoValorPlanoPresente(tableMeta.rowIndex);
                        }
                      });
                    }}
                  />{" "}
                  {icon === "-" ? (
                    <i
                      style={{ position: "absolute", color: "#9633ff" }}
                      className="fas fa-long-arrow-alt-down ml-1"
                    ></i>
                  ) : icon === "+" ? (
                    <i
                      style={{ position: "absolute", color: "#9633ff" }}
                      className="fas fa-long-arrow-alt-up ml-1"
                    ></i>
                  ) : (
                    <></>
                  )}
                </>
              );
            }
          },
        },
      },
      {
        name: "realizado",
        label: "Acumulado",
        options: {
          display: true,
          viewColumns: true,
          filter: false,
          customBodyRender: (value, tableMeta, updateValue) => {
            if (value == null) value = 0;

            return (
              <>
                {parseFloat(value)
                  .toLocaleString("pt-BR", {
                    style: "currency",
                    currency: "BRL",
                  })
                  .replace("R$", "")}
              </>
            );
          },
        },
      },
      {
        name: "inflacao",
        label: "Inflação",
        options: {
          display: true,
          viewColumns: true,
          filter: false,
          customBodyRender: (value, tableMeta, updateValue) => {
            if (value == null) value = 0;

            var planoOriginal = this.getOriginalData(tableMeta.rowData[0]);
            var valorOriginal =
              planoOriginal != null && planoOriginal.inflacao != null
                ? planoOriginal.inflacao
                : 0;
            var icon = "";

            if (value < valorOriginal) {
              icon = "-";
            } else if (value > valorOriginal) {
              icon = "+";
            }
            if (tableMeta.rowData[9] === true) {
              return (
                <>
                  {parseFloat(value).toFixed(2) + "%"}{" "}
                  {icon === "-" ? (
                    <i
                      style={{ position: "absolute", color: "#9633ff" }}
                      className="fas fa-long-arrow-alt-down ml-1"
                    ></i>
                  ) : icon === "+" ? (
                    <i
                      style={{ position: "absolute", color: "#9633ff" }}
                      className="fas fa-long-arrow-alt-up ml-1"
                    ></i>
                  ) : (
                    <></>
                  )}
                </>
              );
            } else {
              return (
                <>
                  <CurrencyInput
                    id={"inflacao_" + tableMeta.rowIndex}
                    style={{ width: "50px" }}
                    value={value}
                    thousandSeparator={"."}
                    decimalSeparator={","}
                    suffix={" %"}
                    onChangeEvent={(event, formattedValue, value) => {
                      var data = this.state.data;
                      data[tableMeta.rowIndex].inflacao = value;

                      this.setState({ data }, function () {
                        if (this.state.campoCalculado === 0) {
                          this.calculoParcela(tableMeta.rowIndex);
                        } else if (this.state.campoCalculado === 1) {
                          this.calculoTaxaTemp(tableMeta.rowIndex);
                        } else if (this.state.campoCalculado === 2) {
                          this.calculoPrazoTemp(tableMeta.rowIndex);
                        } else if (this.state.campoCalculado === 3) {
                          this.calculoValorPlanoPresente(tableMeta.rowIndex);
                        }
                      });
                    }}
                  />{" "}
                  {icon === "-" ? (
                    <i
                      style={{ position: "absolute", color: "#9633ff" }}
                      className="fas fa-long-arrow-alt-down ml-1"
                    ></i>
                  ) : icon === "+" ? (
                    <i
                      style={{ position: "absolute", color: "#9633ff" }}
                      className="fas fa-long-arrow-alt-up ml-1"
                    ></i>
                  ) : (
                    <></>
                  )}
                </>
              );
            }
          },
        },
      },
      {
        name: "taxa",
        label: "Taxa",
        options: {
          display: true,
          viewColumns: true,
          filter: false,
          customBodyRender: (value, tableMeta, updateValue) => {
            if (value == null) value = 0;

            var planoOriginal = this.getOriginalData(tableMeta.rowData[0]);
            var valorOriginal =
              planoOriginal != null && planoOriginal.taxa != null
                ? planoOriginal.taxa
                : 0;
            var icon = "";

            if (value < valorOriginal) {
              icon = "-";
            } else if (value > valorOriginal) {
              icon = "+";
            }

            if (this.state.campoCalculado === 1) {
              return (
                <>
                  {parseFloat(value).toFixed(4)}{" "}
                  {icon === "-" ? (
                    <i
                      style={{ position: "absolute", color: "#9633ff" }}
                      className="fas fa-long-arrow-alt-down ml-1"
                    ></i>
                  ) : icon === "+" ? (
                    <i
                      style={{ position: "absolute", color: "#9633ff" }}
                      className="fas fa-long-arrow-alt-up ml-1"
                    ></i>
                  ) : (
                    <></>
                  )}
                </>
              );
            } else if (tableMeta.rowData[9] === true) {
              return (
                <>
                  {parseFloat(value).toFixed(2) + "%"}{" "}
                  {icon === "-" ? (
                    <i
                      style={{ position: "absolute", color: "#9633ff" }}
                      className="fas fa-long-arrow-alt-down ml-1"
                    ></i>
                  ) : icon === "+" ? (
                    <i
                      style={{ position: "absolute", color: "#9633ff" }}
                      className="fas fa-long-arrow-alt-up ml-1"
                    ></i>
                  ) : (
                    <></>
                  )}
                </>
              );
            } else {
              return (
                <>
                  <CurrencyInput
                    id={"taxa_" + tableMeta.rowIndex}
                    style={{ width: "50px" }}
                    value={value}
                    thousandSeparator={"."}
                    decimalSeparator={","}
                    suffix={" %"}
                    onChangeEvent={(event, formattedValue, value) => {
                      var data = this.state.data;
                      data[tableMeta.rowIndex].taxa = value;

                      this.setState({ data }, function () {
                        if (this.state.campoCalculado === 0) {
                          this.calculoParcela(tableMeta.rowIndex);
                        } else if (this.state.campoCalculado === 1) {
                          this.calculoTaxaTemp(tableMeta.rowIndex);
                        } else if (this.state.campoCalculado === 2) {
                          this.calculoPrazoTemp(tableMeta.rowIndex);
                        } else if (this.state.campoCalculado === 3) {
                          this.calculoValorPlanoPresente(tableMeta.rowIndex);
                        }
                      });
                    }}
                  />{" "}
                  {icon === "-" ? (
                    <i
                      style={{ position: "absolute", color: "#9633ff" }}
                      className="fas fa-long-arrow-alt-down ml-1"
                    ></i>
                  ) : icon === "+" ? (
                    <i
                      style={{ position: "absolute", color: "#9633ff" }}
                      className="fas fa-long-arrow-alt-up ml-1"
                    ></i>
                  ) : (
                    <></>
                  )}
                </>
              );
            }
          },
        },
      },
      {
        name: "parcela",
        label: "Parcela",
        options: {
          display: true,
          viewColumns: true,
          filter: false,
          customBodyRender: (value, tableMeta, updateValue) => {
            if (value == null || value < 0) value = 0;

            var planoOriginal = this.getOriginalData(tableMeta.rowData[0]);
            var valorOriginal =
              planoOriginal != null && planoOriginal.parcela != null
                ? planoOriginal.parcela
                : 0;
            var icon = "";

            if (value === 0) {
              icon = " ";
            } else if (value < valorOriginal) {
              icon = "-";
            } else if (value > valorOriginal) {
              icon = "+";
            }

            if (
              this.state.campoCalculado === 0 ||
              this.state.campoCalculado === 3
            ) {
              return (
                <>
                  {parseFloat(value)
                    .toLocaleString("pt-BR", {
                      style: "currency",
                      currency: "BRL",
                    })
                    .replace("R$", "")}
                  {icon === "-" ? (
                    <i
                      style={{ position: "absolute", color: "#9633ff" }}
                      className="fas fa-long-arrow-alt-down ml-1"
                    ></i>
                  ) : icon === "+" ? (
                    <i
                      style={{ position: "absolute", color: "#9633ff" }}
                      className="fas fa-long-arrow-alt-up ml-1"
                    ></i>
                  ) : icon === " " ? (
                    <i
                      style={{ position: "absolute", color: "#2bacc2" }}
                      className="fas fa-check ml-1"
                    ></i>
                  ) : (
                    <></>
                  )}
                </>
              );
            } else {
              return (
                <>
                  <CurrencyInput
                    id={"parcela_" + tableMeta.rowIndex}
                    style={{ width: "90%" }}
                    value={value}
                    thousandSeparator={"."}
                    decimalSeparator={","}
                    precision="2" //prefix={'R$ '}
                    onChangeEvent={(event, formattedValue, value) => {
                      var data = this.state.data;
                      data[tableMeta.rowIndex].parcela = value;

                      this.setState({ data }, function () {
                        if (this.state.campoCalculado === 0) {
                          this.calculoParcela(tableMeta.rowIndex);
                        } else if (this.state.campoCalculado === 1) {
                          this.calculoTaxaTemp(tableMeta.rowIndex);
                        } else if (this.state.campoCalculado === 2) {
                          this.calculoPrazoTemp(tableMeta.rowIndex);
                        } else if (this.state.campoCalculado === 3) {
                          this.calculoValorPlanoPresente(tableMeta.rowIndex);
                        }
                      });
                    }}
                  />{" "}
                  {icon === "-" ? (
                    <i
                      style={{ position: "absolute", color: "#9633ff" }}
                      className="fas fa-long-arrow-alt-down ml-1"
                    ></i>
                  ) : icon === "+" ? (
                    <i
                      style={{ position: "absolute", color: "#9633ff" }}
                      className="fas fa-long-arrow-alt-up ml-1"
                    ></i>
                  ) : (
                    <></>
                  )}
                </>
              );
            }
          },
        },
      },
      {
        name: "ehAposentadoria",
        options: {
          display: false,
          viewColumns: false,
          filter: false,
        },
      },
    ];

    columns.forEach((item) => {
      if (item.name === this.state.colunaOrdenacao) {
        item.options.sortDirection = this.state.colunaOrdenacao;
      }
    });
    const { data, isLoading } = this.state;

    const options = {
      textLabels: global.textLabels,
      filter: true,
      filterType: "dropdown",
      responsive: "stacked",
      print: false,
      download: false,
      searchText: this.state.textoDigitado,
      sort: true,
      selectableRows: "none",
      pagination: false,
      //rowsPerPage: 1000000,
      onRowClick: this.editarRegistro,
      onFilterChange: (changedColumn, filterList) => {
        console.log("onFilterChange");
      },
      onColumnSortChange: (changedColumn) => {
        //ASC
        let order = "";
        let direcaoOrdenacao = "asc";

        if (this.state.colunaOrdenacao === changedColumn) {
          if (
            this.state.direcaoOrdenacao != null &&
            this.state.direcaoOrdenacao === "asc"
          ) {
            order = " Desc";
            direcaoOrdenacao = "desc";
          }
        }

        if (changedColumn === "MeioPagamento") {
          changedColumn = "Meio Pagamento";
        }

        console.log(changedColumn + order);

        this.setState(
          {
            sortField: changedColumn + order,
            colunaOrdenacao: changedColumn,
            direcaoOrdenacao: direcaoOrdenacao,
          },
          () => {
            this.getData();
          }
        );
      },
      onTableChange: (action, tableState) => {
        // var totalEstimado = 0;
        // var totalRealizado = 0;
        // $('table.MUIDataTable-tableRoot-3 > tbody > tr').each(function( index ) {
        //     var trId = $( this ).attr('id');
        //     if(trId != null){
        //         var trIdArray = trId.split('-');
        //         var idLinha = trIdArray[trIdArray.length - 1];
        //         var linha = $this.state.data[idLinha];
        //         totalEstimado += linha.Estimado;
        //         totalRealizado += linha.TotalMedio;
        //     }
        // });
        // totalEstimado = "Total Estimado: " + totalEstimado.toLocaleString('pt-BR', { style: 'currency', currency: 'BRL' }).replace('R$', '');
        // totalRealizado = "Total Realizado: " + totalRealizado.toLocaleString('pt-BR', { style: 'currency', currency: 'BRL' }).replace('R$', '');
        // $('tfoot.MuiTableFooter-root > tr > td:nth-child(2)').html(totalEstimado);
        // $('tfoot.MuiTableFooter-root > tr > td:nth-child(3)').html(totalRealizado);
      },
      customFooter: (
        count,
        page,
        rowsPerPage,
        changeRowsPerPage,
        changePage,
        textLabels
      ) => {
        return (
          <CustomFooter
            count={count}
            page={page}
            rowsPerPage={rowsPerPage}
            changeRowsPerPage={changeRowsPerPage}
            changePage={changePage}
            textLabels={textLabels}
            header={this.state.footerColumns}
            data={this.state.sumario}
            data2={this.state.sumarioAlterado}
            data3={this.state.diferencaSumario}
            hasPagination={false}
          />
        );
      },
    };

    return (
      <div style={{ padding: "0 5%", marginTop: 20 }}>
        <div className="home-summary-top">
          <Row style={{ paddingLeft: 0 }}>
            <Col lg="12" xl="12">
              <Card>
                <CardHeader
                  className="border-0"
                  style={{ padding: "30px 15px 30px 0" }}
                >
                  <Row
                    className="align-items-center"
                    style={{ paddingLeft: 0 }}
                  >
                    <div className="col-4 col-xs-12">
                      <img
                        src={require("../../assets/img/theme/icone-calendario.png")}
                        alt="Gestão de Planos"
                        className="before-title-img"
                      />
                      <h3 className="mb-0 chart-title">Gestão de Planos</h3>
                    </div>
                    <div className="col-8 col-xs-12">
                      <Row style={{ justifyContent: "flex-end" }}>
                        <NavItem className="general-button dark with-left-margin">
                          <NavLink
                            className="nav-link-icon aux-button"
                            to="/admin/planos-sonhos"
                            tag={Link}
                          >
                            <span className="nav-link-inner--text">
                              Planos & Sonhos
                            </span>
                          </NavLink>
                        </NavItem>
                        <NavItem className="general-button dark with-left-margin">
                          <NavLink
                            className="nav-link-icon aux-button"
                            to="/admin/destinacao-patrimonio"
                            tag={Link}
                          >
                            <span className="nav-link-inner--text inner">
                              Destinação de Patrimônio
                            </span>
                          </NavLink>
                        </NavItem>
                      </Row>
                    </div>
                  </Row>
                </CardHeader>
              </Card>
            </Col>
          </Row>
        </div>

        <MuiThemeProvider theme={this.getMuiTheme()}>
          <MUIDataTable
            id="orcamento-table"
            title={
              <div
                style={{ float: "left" }}
                className="gestao-planos-grid-title"
              >
                {isLoading && (
                  <CircularProgress
                    size={24}
                    style={{ marginLeft: 15, position: "relative", top: 4 }}
                  />
                )}
                <span
                  style={{ float: "left", fontWeight: "600", marginTop: "7px" }}
                >
                  Campo Calculado:
                </span>
                <Select
                  id="campoCalculado"
                  onChange={this.mudarCampoCalculado}
                  className="select-component"
                  options={this.gestaoPlanoCampoCalculado}
                  defaultValue={this.gestaoPlanoCampoCalculado[0]}
                  placeholder="Selecione..."
                  value={
                    this.state.campoCalculado != null
                      ? this.gestaoPlanoCampoCalculado.find((op) => {
                          return op.value === this.state.campoCalculado;
                        })
                      : null
                  }
                />
              </div>
            }
            data={data}
            columns={columns}
            options={options}
          />
        </MuiThemeProvider>
        <div style={{ display: "flex", marginBottom: "20px" }}>
          <Col lg="12" xl="12" style={{ paddingRight: 0 }}>
            <button
              onClick={() => this.salvar()}
              className="featured-button"
              style={{
                float: "right",
                backgroundImage:
                  "url(" +
                  require("../../assets/img/theme/botao-destaque.png") +
                  ")",
              }}
            >
              Salvar
            </button>
            <button
              onClick={() => this.cancelar()}
              className="aux-button"
              style={{ float: "right", marginRight: "10px" }}
            >
              Cancelar
            </button>
          </Col>
        </div>
        {this.renderRedirect()}
      </div>
    );
  }
}

export default GestaoPlanos;
