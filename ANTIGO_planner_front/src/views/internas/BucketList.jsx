import React from "react";
import Select from "react-select";
import axios from "axios";
import { confirmAlert } from "react-confirm-alert";
import TableRow from "@material-ui/core/TableRow";
import TableCell from "@material-ui/core/TableCell";
import "react-confirm-alert/src/react-confirm-alert.css";
import SkyLight from "react-skylight";
import MUIDataTable from "mui-datatables";
import $ from "jquery";
import { Redirect } from "react-router-dom";

// reactstrap components
import { Card, CardHeader, Container, Row, Col } from "reactstrap";

class BucketList extends React.Component {
  moment = require("moment");

  state = {
    activePage: 1,
    itensPerPage: 12,
    pageRangeDisplayed: 5,
    totalItemsCount: 450,
    sortField: null,
    carregamentoInicial: false,
    interval: null,
    textoDigitado: "",
    editando: false,
  };

  imagens = {};

  bucketList = [];

  imagem = null;

  camposParaOrdenacao = [
    { value: "Descricao", label: "Descrição" },
    { value: "Descricao Desc", label: "Descrição Decrescente" },
  ];

  montarDropDown = (itens, id, changeMethod, component) => (
    <Select
      id={id}
      onChange={() => changeMethod}
      className="select-component"
      options={itens}
      defaultValue={itens != null ? itens[0] : ""}
      component={component != null ? component : null}
    />
  );

  mostrarCamposParaOrdenacao = (camposParaOrdenacao) =>
    this.montarDropDown(
      camposParaOrdenacao,
      "selecao-ordenacao",
      this.mudarOrdenacao,
      null
    );

  mudarOrdenacao = (selectedOption) => {
    const sortField = selectedOption.value;

    this.setState({ sortField }, function () {
      this.atualizarBucketList();
    });
  };

  removerRegistros = (rowsDeleted) => {
    const options = {
      title: "Exclusão de Sonhos",
      message:
        "Confirma procedimento de exclusão? Este procedimento é irreversível!",
      buttons: [
        {
          label: "Sim",
          onClick: () => {
            var ids = [];

            for (var key in rowsDeleted.data) {
              ids.push(
                this.state.bucketListFake[rowsDeleted.data[key].dataIndex]
              );
            }

            if (ids.length > 0) {
              //var url = global.server_api + 'api/bucketlist/bulk/' + ids.join(",");
              var url =
                global.server_api_new +
                global.apiToken +
                "/bucketlist/bulk/" +
                ids.join(",");

              var config = {
                headers: {
                  Authorization: "bearer " + this.state.accessToken,
                  "Access-Control-Allow-Origin": "*",
                  "Access-Control-Allow-Headers": "Authorization",
                  "Access-Control-Allow-Methods":
                    "GET, POST, OPTIONS, PUT, PATCH, DELETE",
                },
              };

              axios.delete(url, {}, config).then((res) => {
                const result = res.data;

                if (result.success && result.affectedResults > 0) {
                  this.atualizarBucketList();
                } else {
                  console.log(res.data.exception);
                  alert("Erro ao remover. " + res.data.exception.Message);
                }
              });
            }
          },
        },
        {
          label: "Não",
        },
      ],
    };

    confirmAlert(options);

    return false;
  };

  mostrarLista = () => {
    const options = {
      textLabels: global.textLabels,
      selectableRows: false,
      expandableRows: true,
      filter: false,
      searchable: false,
      filterType: "dropdown",
      responsive: "stacked",
      print: false,
      download: false,
      pagination: false,
      viewColumns: false,
      onRowsDelete: (rowsDeleted) => {
        this.removerRegistros(rowsDeleted);
        return false;
      },
      onRowClick: (rowData) => {
        this.editar(rowData[0]);
      },
      renderExpandableRow: (rowData, rowMeta) => {
        return this.subnivel(rowData[2]);
      },
    };
    const columns = [
      {
        name: "id",
        options: {
          display: false,
          viewColumns: false,
          filter: false,
        },
      },
      {
        name: "nome",
        label: "Categorias de Sonhos",
      },
      {
        name: "sonhos",
        options: {
          display: false,
          viewColumns: false,
          filter: false,
        },
      },
    ];

    var list = this.mostrarCategorias(this.state.dictBucketList);

    list.sort(this.ordenarCategorias2);

    return (
      <div style={{ width: "100%" }}>
        <MUIDataTable columns={columns} data={list} options={options} />
      </div>
    );
  };

  gerarDados = (bucketList) => {
    if (bucketList == null) {
      return [];
    }
    var imageData = require("../../assets/img/theme/sonhos-padrao.jpg");
    return bucketList.map((item, key) => {
      return {
        id: item.id,
        img: (
          <img
            id={"bucket-list-" + item.id}
            src={item.imagem != null ? item.imagem : imageData}
            style={{ maxWidth: "200px", maxHeight: "200px" }}
            alt={item.descricao}
          />
        ),
        descricao: item.descricao,
      };
    });
  };

  subnivel = (bucketListFake) => {
    bucketListFake.forEach((value) => {
      this.buscarImagemSonho(value.id);
    });
    const options = {
      textLabels: global.textLabels,
      selectableRows: true,
      expandableRows: false,
      filter: false,
      filterType: "dropdown",
      responsive: "stacked",
      print: false,
      download: false,
      pagination: false,
      viewColumns: false,
      onRowsDelete: (rowsDeleted) => {
        this.removerRegistros(rowsDeleted);
        return false;
      },
      onRowClick: (rowData) => {
        this.editar(rowData[0]);
      },
    };
    const columns = [
      {
        name: "id",
        options: {
          display: false,
          viewColumns: false,
          filter: false,
        },
      },
      {
        name: "imagem",
        label: "Imagem",
        options: {
          display: true,
          viewColumns: false,
          filter: false,
          customBodyRender: (value, tableMeta) => {
            if (value == null) {
              this.buscarImagemSonho(tableMeta.rowData[0]);
              value = require("../../assets/img/theme/sonhos-padrao.jpg");
            }

            return (
              <img
                id={"bucket-list-" + tableMeta.rowData[0]}
                className="navbar-brand-img"
                src={value}
                style={{
                  marginTop: "8px",
                  maxHeight: "100px",
                  maxWidth: "100px",
                  cursor: "pointer",
                }}
                alt="Imagem"
                onClick={() => {
                  this.editar(tableMeta.rowData[0]);
                }}
              />
            );
          },
        },
      },
      {
        name: "descricao",
        label: "Descrição",
      },
    ];
    const colSpan = 20;
    return (
      <TableRow>
        <TableCell colSpan={colSpan}>
          <div id="orcamento-table">
            <div style={{ width: "100%" }}>
              <MUIDataTable
                columns={columns}
                data={this.gerarDados(bucketListFake)}
                options={options}
              />
            </div>
          </div>
        </TableCell>
      </TableRow>
    );
  };

  mostrarCategorias = (categorias) => {
    if (categorias === null || categorias === undefined) return [];
    var keys = Object.keys(categorias);

    var result = keys.map((value) => {
      return {
        id: value,
        nome: global.grupoCategoriaSonho[value],
        sonhos: categorias[value],
      };
    });
    return result;
  };

  mostrarDados = (bucketList) => {
    if (bucketList == null) {
      return <></>;
    }

    var imageData = require("../../assets/img/theme/sonhos-padrao.jpg");

    return bucketList.map((item, key) => {
      return (
        <>
          <tr key={key}>
            <td>
              <div onClick={() => this.confirmaExcluir(item)}>
                <i className="far fa-trash-alt"></i>
              </div>
            </td>
            <td>
              <div onClick={() => this.editar(item)}>
                <i className="fas fa-info"></i>
              </div>
            </td>
            <td onClick={() => this.editar(item)} style={{ width: "200px" }}>
              <img
                src={
                  this.bucketList[item].imagem != null
                    ? this.bucketList[item].imagem
                    : imageData
                }
                style={{ maxWidth: "200px", maxHeight: "200px" }}
                alt={this.bucketList[item].descricao}
              ></img>
            </td>
            <td onClick={() => this.editar(item)}>
              {this.bucketList[item].descricao}
            </td>
          </tr>
        </>
      );
    });
  };

  buscarImagemSonho = (id) => {
    if (this.imagens != null && this.imagens[id] != null) {
      $("#bucket-list-" + id).attr("src", this.imagens[id]);
      return;
    }

    //var url = global.server_api + 'api/sonho/bucketList/imagem/' + id;
    var url =
      global.server_api_new +
      global.apiToken +
      "/sonho/bucketList/imagem/" +
      id;

    var config = {
      headers: {
        Authorization: "bearer " + this.state.accessToken,
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Headers": "Authorization",
        "Access-Control-Allow-Methods":
          "GET, POST, OPTIONS, PUT, PATCH, DELETE",
      },
    };

    axios.get(url, config).then((res) => {
      if (res.data.singleResult != null && res.data.singleResult !== "") {
        $("#bucket-list-" + id).attr("src", res.data.singleResult);
        this.imagens[id] = res.data.singleResult;
      }
    });
  };

  atualizarBucketList = () => {
    if (this.state.interval != null) {
      clearInterval(this.state.interval);
    }

    //var url = global.server_api + 'api/bucketlist/filtro';
    var url = global.server_api_new + global.apiToken + "/bucketlist/filtro";

    var filtro = {
      Filtro: this.state.textoDigitado,
      Ordenacao: this.state.sortField,
    };

    var config = {
      headers: {
        Authorization: "bearer " + this.state.accessToken,
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Headers": "Authorization",
        "Access-Control-Allow-Methods":
          "GET, POST, OPTIONS, PUT, PATCH, DELETE",
      },
    };

    var currentScroll = [
      document.documentElement.scrollLeft || document.body.scrollLeft,
      document.documentElement.scrollTop || document.body.scrollTop,
    ];

    global.spinnerShow($);

    axios.post(url, filtro, config).then((res) => {
      this.bucketList = {};
      var bucketListFake = [];

      if (res.data.results != null) {
        res.data.results.forEach((item) => {
          bucketListFake.push(item.id);
          this.bucketList[item.id] = item;
        });
      }

      global.spinnerHide($, currentScroll);

      this.setState({ bucketListFake });
    });
  };

  confirmaExcluir = (id) => {
    const options = {
      title: "Exclusão de Sonho",
      message:
        "Tem certeza que deseja excluir este Sonho? Este procedimento é irreversível!",
      buttons: [
        {
          label: "Sim",
          onClick: () => this.excluir(id),
        },
        {
          label: "Não",
        },
      ],
    };

    confirmAlert(options);
  };

  excluir = (id) => {
    //var url = global.server_api + 'api/bucketlist/' + id;
    var url = global.server_api_new + global.apiToken + "/bucketlist/" + id;

    var config = {
      headers: {
        Authorization: "bearer " + this.state.accessToken,
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Headers": "Authorization",
        "Access-Control-Allow-Methods":
          "GET, POST, OPTIONS, PUT, PATCH, DELETE",
      },
    };

    axios.delete(url, {}, config).then((res) => {
      const result = res.data;

      if (result.success && result.affectedResults > 0) {
        this.atualizarBucketList();
      } else {
        alert("Erro ao remover sonho. " + res.data.exception.Message);
        console.log(res.data.exception);
      }
    });
  };

  editar = (id) => {
    var sonhoEdicao = this.bucketList[id];

    if (sonhoEdicao != null) {
      this.imagem = this.imagens[id];

      //Seta item edicao
      this.setState(
        {
          id: id,
          descricao: sonhoEdicao.descricao,
          editando: true,
        },
        function () {
          //Abre modal
          global.showModal(this.modal);
        }
      );
    }
  };

  filtroPorTexto = (valorDigitado) => {
    const textoDigitado = valorDigitado.target.value;

    this.setState({ textoDigitado }, function () {
      if (this.state.interval != null) {
        clearInterval(this.state.interval);
      }

      const $this = this;

      const interval = setInterval(function () {
        $this.atualizarBucketList();
      }, 1000);

      this.setState({ interval });
    });
  };

  limparSelecao = () => {
    this.imagem = null;
    $("#linha").html("");
    this.setState({
      id: null,
      descricao: "",
      editando: false,
    });
  };

  salvar = () => {
    var item = {
      id: this.state.id,
      descricao: this.state.descricao,
      categoria: this.state.idCategoria,
      imagem: this.imagem,
    };

    //var url = global.server_api + 'api/bucketlist' + (this.state.id != null ? "/" + this.state.id: "");
    var url =
      global.server_api_new +
      global.apiToken +
      "/bucketlist" +
      (this.state.id != null ? "/" + this.state.id : "");

    var config = {
      headers: {
        Authorization: "bearer " + this.state.accessToken,
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Headers": "Authorization",
        "Access-Control-Allow-Methods":
          "GET, POST, OPTIONS, PUT, PATCH, DELETE",
      },
    };

    console.log(item);

    axios.post(url, item, config).then((res) => {
      console.log(res);
      if (res.data.success === true) {
        this.limparSelecao();
        window.location.reload();
      } else {
        console.log(res.data.exception);
        const options = {
          title: "Erro ao Salvar Sonho",
          message: res.data.exception.Message,
          buttons: [
            {
              label: "Ok",
            },
          ],
        };

        confirmAlert(options);
      }
    });
  };

  componentDidMount() {
    this.setState(
      {
        accessToken: localStorage.getItem("access-token"),
        empresaId: localStorage.getItem("empresa-id"),
        familiaId: localStorage.getItem("familia-id"),
      },
      function () {
        if (this.state.accessToken == null) {
          this.setRedirect();
          return;
        }

        this.atualizarBucketList();
        global.mostrarAjudaDaPaginaAutomaticamente(this);
        //var url = `${global.server_api}api/bucketlist/agrupado`;
        var url = `${global.server_api_new}${global.apiToken}/bucketlist/agrupado`;

        var config = {
          headers: {
            Authorization: "bearer " + this.state.accessToken,
            "Access-Control-Allow-Origin": "*",
            "Access-Control-Allow-Headers": "Authorization",
            "Access-Control-Allow-Methods":
              "GET, POST, OPTIONS, PUT, PATCH, DELETE",
          },
        };

        var keys = Object.keys(global.grupoCategoriaSonho);
        var selectCategoriaSonhos = keys.map((value) => {
          return {
            value: value,
            label: global.grupoCategoriaSonho[value],
          };
        });

        selectCategoriaSonhos.sort(this.ordenarCategorias);

        axios.get(url, config).then((res) => {
          this.setState({
            dictBucketList: res.data.singleResult,
            selectCategoriaSonhos: selectCategoriaSonhos,
          });
        });
      }
    );

    var $this = this;

    setTimeout(function () {
      $(document).on("change", "#imagem", function (event) {
        var file = event.target.files[0];
        var reader = new FileReader();

        reader.onload = function (readerEvt) {
          var binaryString = readerEvt.target.result;
          var b64 = btoa(binaryString);
          $this.imagem = "data:image/png;base64," + b64;
          $this.mostrarThumb();
        };

        reader.readAsBinaryString(file);
      });
    }, 1000);
  }

  ordenarCategorias2 = (item1, item2) => {
    const name1 = item1.nome.toUpperCase();
    const name2 = item2.nome.toUpperCase();

    let comparison = 0;
    if (name1 > name2) {
      comparison = 1;
    } else if (name1 < name2) {
      comparison = -1;
    }

    return comparison;
  };

  ordenarCategorias = (item1, item2) => {
    const name1 = item1.label.toUpperCase();
    const name2 = item2.label.toUpperCase();

    let comparison = 0;
    if (name1 > name2) {
      comparison = 1;
    } else if (name1 < name2) {
      comparison = -1;
    }

    return comparison;
  };

  setRedirect = () => {
    this.setState({
      redirect: true,
    });
  };

  renderRedirect = () => {
    if (this.state.redirect) {
      return <Redirect to="/auth/login" />;
    }
  };

  mostrarThumb = () => {
    var novaLinha =
      '<img src="' +
      this.imagem +
      '" style="margin-top: 8px; max-width: 200px; max-height: 200px">';
    $("#linha").html(novaLinha);
  };

  mostrarPopup = () => (
    <div>
      <SkyLight
        ref={(ref) => (this.modal = ref)}
        transitionDuration={0}
        beforeOpen={this.preCarregarParaEdicao}
        afterClose={this.limparSelecao}
        title={
          <>
            <div className="pop-up-title">
              <i className="ni ni-check-bold"></i> <h2>Cadastro de Sonho</h2>
            </div>
          </>
        }
      >
        <Row>
          <Col lg="12" xl="12">
            <div>
              <label>Descrição</label>
              <input
                type="text"
                id="descricao"
                value={this.state.descricao}
                onChange={(e) => this.setState({ descricao: e.target.value })}
              ></input>
            </div>
          </Col>
          <Col lg="12" xl="12">
            <div>
              <label>Categoria</label>
              <Select
                className="select-component"
                onChange={(e) => {
                  this.setState({ idCategoria: e.value });
                }}
                options={this.state.selectCategoriaSonhos}
              />
            </div>
          </Col>
          <Col lg="12" xl="12">
            <div>
              <label>Imagem</label>
              <input
                type="file"
                id="imagem"
                onChange={(e) => (this.imagem = e.target.value)}
              ></input>
              <div id="linha">
                {this.state.editando != null &&
                  this.imagem != null &&
                  this.mostrarThumb()}
              </div>
            </div>
          </Col>
          <Col lg="12" xl="12">
            <button
              id="salvar-lancamento"
              className="featured-button"
              onClick={() => this.salvar()}
              style={{
                backgroundImage:
                  "url(" +
                  require("../../assets/img/theme/botao-destaque.png") +
                  ")",
              }}
            >
              Salvar
            </button>
          </Col>
        </Row>
      </SkyLight>
    </div>
  );

  render() {
    return (
      <>
        {this.renderRedirect()}
        <Container fluid>
          <div className="home-summary-top">
            <Row>
              <Col lg="12" xl="12" style={{ marginBottom: "15px" }}>
                <div>
                  <Card>
                    <CardHeader className="border-0">
                      <Row className="align-items-center">
                        <div className="col">
                          <img
                            src={require("../../assets/img/theme/icone-calendario.png")}
                            alt="BucketList"
                            className="before-title-img"
                          />
                          <h3 className="mb-0 chart-title">Lista de Sonhos</h3>
                        </div>
                        <div className="new-entry" style={{ float: "right" }}>
                          <div
                            className="nav-link-icon featured-button"
                            style={{
                              backgroundImage:
                                "url(" +
                                require("../../assets/img/theme/botao-destaque.png") +
                                ")",
                            }}
                            onClick={() => global.showModal(this.modal)}
                          >
                            <span className="nav-link-inner--text">
                              Novo Sonho
                            </span>
                          </div>
                        </div>
                      </Row>
                    </CardHeader>
                  </Card>
                </div>
              </Col>
            </Row>
            <Row className="entry-list">{this.mostrarLista()}</Row>
          </div>
        </Container>
        {this.mostrarPopup()}
      </>
    );
  }
}

export default BucketList;
