import React from "react";
import Select from "react-select";
import { Redirect, Link } from "react-router-dom";
import MUIDataTable from "mui-datatables";
import TableRow from "@material-ui/core/TableRow";
import TableCell from "@material-ui/core/TableCell";
import axios from "axios";
import $ from "jquery";
import { confirmAlert } from "react-confirm-alert";
import SkyLight from "react-skylight";
import Datepicker, { registerLocale } from "react-datepicker";
import br from "date-fns/locale/pt-BR";
import "react-datepicker/dist/react-datepicker.css";
import CurrencyInput from "react-currency-input";
import ApexChart from "react-apexcharts";
import MaskedInput from "react-maskedinput";
import { createMuiTheme, MuiThemeProvider } from "@material-ui/core/styles";
import CustomFooter from "./CustomFooter.jsx";
import Fab from "@material-ui/core/Fab";
import UpIcon from "@material-ui/icons/KeyboardArrowUp";
import RestoreDate from "@material-ui/icons/Restore";
import Tooltip from "@material-ui/core/Tooltip";
import { withStyles } from "@material-ui/core/styles";
import Restore3Months from "@material-ui/icons/Looks3Outlined";

import {
  Col,
  Row,
  Card,
  CardHeader,
  CardBody,
  NavItem,
  NavLink,
} from "reactstrap";

registerLocale("br", br);

class Patrimonios extends React.Component {
  moment = require("moment");

  state = {
    tiposInvestimento1: [],
    tiposInvestimento2: [],
    dataInvestimento1: [],
    dataInvestimento2: [],
    totalAplicado1: 0,
    totalAplicado2: 0,
    totalAtual1: 0,
    totalAtual2: 0,
    dataImobilizado: [],
    totalImobilizado: [],
    totalImobilizadoValor: 0,
    lancamentosInvestimentos: [],
    item: {
      descricao: "",
      tipoPatrimonio: null,
      dataPrimeiraCompra: null,
      tipoInvestimento: null,
      responsavel: null,
      valorAplicado: "",
      valorAtual: "",
      produtoCustomizadoNome: "",
      produtoCustomizadoTipoInvestimento: null,
      produtoCustomizadoMetodologiaDePreco: null,

      idCarteira: null,
      idProdutoNoPortfolio: null,
      cnpj: "",
      idAcaoBovespa: null,
      idTesouroDireto: null,
      idDebenture: null,
      dataVencimento: null,
      idEmissor: null,
      idProduto: null,
      idIndexador: null,
      serieDaMoeda: null,
    },
    passoAtual: 1,
    tiposPatrimonio: [],
    tiposInvestimento: [],
    tiposInvestimentoSemCustomizado: [],
    metodologiasDePreco: [],
    responsaveisCombo: [{ label: "Buscando...", value: null }],
    responsaveis: [],
    graficoConsolidacaoCarteiras: {},
    graficoRentabilidadeCarteiras: {},
    consolidacaoCarteiras: {},
    produtosNasCarteiras: {},
    consolidacaoProdutosNasCarteiras: {},
    graficoConsolidacaoProdutosNasCarteiras: {},
    startDate: localStorage.getItem("dataInicioPatrimonio")
      ? new Date(JSON.parse(localStorage.getItem("dataInicioPatrimonio")))
      : this.moment().startOf("month").toDate(),
    endDate: localStorage.getItem("dataFimPatrimonio")
      ? new Date(JSON.parse(localStorage.getItem("dataFimPatrimonio")))
      : this.moment().endOf("month").toDate(),
  };

  getMuiTheme = () =>
    createMuiTheme({
      overrides: {
        MuiTableRow: {
          root: {
            "&:nth-of-type(odd)": {
              backgroundColor: "#f8f9fe",
            },
          },
        },
        MUIDataTableSelectCell: {
          fixedHeaderCommon: {
            backgroundColor: "unset",
          },
        },
        MuiTableCell: {
          root: {
            fontFamily: "unset",
            borderTop: "none",
            padding: "16px 10px",
          },
          footer: {
            color: "#FFF",
            fontWeight: 700,
          },
        },
        MUIDataTableHeadCell: {
          fixedHeaderYAxis: {
            fontWeight: 700,
          },
        },
        MuiPaper: {
          root: {
            marginBottom: 30,
            overflow: "hidden",
          },
        },
        MuiToolbar: {
          regular: {
            backgroundColor: "#e4eaf4",
            paddingBottom: "1rem",
          },
        },
      },
    });

  setRedirect = () => {
    this.setState({
      redirect: true,
    });
  };

  renderRedirect = () => {
    if (this.state.redirect) {
      return <Redirect to="/auth/login" />;
    }
  };

  _next = () => {
    let passoAtual = this.state.passoAtual;

    passoAtual += 1;

    this.setState({
      passoAtual: passoAtual,
    });
  };

  _prev = () => {
    let passoAtual = this.state.passoAtual;

    passoAtual -= 1;

    this.setState({
      passoAtual: passoAtual,
    });
  };

  componentDidMount() {
    // const { open } = this.props.match.params

    if (sessionStorage.getItem("refresh") === "true") {
      sessionStorage.setItem("refresh", "false");

      document.getElementById("novo-lancamento").click();
      setTimeout(function () {
        var lancInvestButton = document.getElementById(
          "lancamento-investimento"
        );

        if (lancInvestButton) {
          lancInvestButton.click();
        }
      }, 50);
    }

    this.setState(
      {
        accessToken: localStorage.getItem("access-token"),
        empresaId: localStorage.getItem("empresa-id"),
        familiaId: localStorage.getItem("familia-id"),
        planejadorId: localStorage.getItem("planejador-id"),
        tipoUsuario: localStorage.getItem("tipo-usuario"),
      },
      function () {
        if (this.state.accessToken == null) {
          this.setRedirect();
          return;
        }

        this.getData();

        this.atualizarEntidadesDeDominio(
          "produtosRendaFixaPre",
          "papeisRendaFixaPre"
        );
        this.atualizarEntidadesDeDominio(
          "produtosRendaFixa",
          "papeisRendaFixa"
        );
        this.atualizarEntidadesDeDominio(
          "instituicoesFinanceiras",
          "instituicoesFinanceiras"
        );
        this.atualizarEntidadesDeDominio("indexadores", "indicesRendaFixa");
        this.atualizarEntidadesDeDominio("tesouros", "titulosTesouroDireto");
        this.atualizarEntidadesDeDominio("debentures", "debentures");
        this.atualizarEntidadesDeDominio("seriesMoedas", "moedas");
        this.atualizarEntidadesDeDominio("acoesBovespa", "acoes");

        this.getDataLancamentos();
        this.atualizarCategorias();
        this.atualizarMeiosPagamento();

        var tiposPatrimonio = this.state.tiposPatrimonio;

        global.tipoPatrimonio.forEach(function (value, index) {
          tiposPatrimonio.push({
            value: index,
            label: value,
          });
        });

        var tiposInvestimento = [],
          tiposInvestimentoSemCustomizado = [],
          metodologiasDePreco = [];

        Object.keys(global.tipoInvestimento).forEach(function (key) {
          key = parseInt(key);

          tiposInvestimento.push({
            value: key,
            label: global.tipoInvestimento[key],
          });

          if (key !== 99) {
            tiposInvestimentoSemCustomizado.push({
              value: key,
              label: global.tipoInvestimento[key],
            });
          }
        });

        tiposInvestimento = tiposInvestimento.sort(function compare(a, b) {
          if (a.label < b.label) {
            return -1;
          }
          if (a.label > b.label) {
            return 1;
          }
          return 0;
        });

        tiposInvestimentoSemCustomizado = tiposInvestimentoSemCustomizado.sort(
          function compare(a, b) {
            if (a.label < b.label) {
              return -1;
            }
            if (a.label > b.label) {
              return 1;
            }
            return 0;
          }
        );

        Object.keys(global.metodologiaDePreco).forEach(function (key) {
          key = parseInt(key);

          metodologiasDePreco.push({
            value: key,
            label: global.metodologiaDePreco[key],
          });
        });

        this.setState({
          tiposPatrimonio: tiposPatrimonio,
          tiposInvestimento: tiposInvestimento,
          tiposInvestimentoSemCustomizado: tiposInvestimentoSemCustomizado,
          metodologiasDePreco: metodologiasDePreco,
        });

        global.mostrarAjudaDaPaginaAutomaticamente(this);
      }
    );
  }

  atualizarEntidadesDeDominio = (fieldKey, identificador) => {
    //var url = global.server_api + 'api/patrimonio/metodosAuxiliares/porIdentificador';
    var url =
      global.server_api_new +
      global.apiToken +
      "/patrimonio/metodosAuxiliares/porIdentificador";

    var config = {
      headers: {
        Authorization: "bearer " + this.state.accessToken,
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Headers": "Authorization",
        "Access-Control-Allow-Methods":
          "GET, POST, OPTIONS, PUT, PATCH, DELETE",
      },
    };

    var data = {
      filtro: identificador,
    };

    axios.post(url, data, config).then((res) => {
      const itens = res.data.data;

      var combo = [];

      if (res.data.success === true && itens != null) {
        itens.forEach((item) => {
          combo.push({
            value: item.key,
            label: item.value,
          });
        });
      }

      this.setState({ [fieldKey]: combo });
    });
  };

  getPatrimonios = (currentScroll) => {
    //var url = global.server_api + 'api/patrimonio/familia/' + this.state.familiaId + '/filtro';
    var url =
      global.server_api_new +
      global.apiToken +
      "/patrimonio/familia/" +
      this.state.familiaId +
      "/filtro";

    var filtro = {
      Filtro: this.state.textoDigitado,
      Ordenacao:
        this.state.sortField != null ? this.state.sortField : "Data Desc",
      NumeroPagina:
        this.state.numeroPagina != null ? this.state.numeroPagina : null,
      NumeroItensPorPagina:
        this.state.numeroItensPorPagina != null
          ? this.state.numeroItensPorPagina
          : null,
      ApenasDadosNecessarios: true,
    };

    var config = {
      headers: {
        Authorization: "bearer " + this.state.accessToken,
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Headers": "Authorization",
        "Access-Control-Allow-Methods":
          "GET, POST, OPTIONS, PUT, PATCH, DELETE",
      },
    };

    axios.post(url, filtro, config).then((res) => {
      var dataInvestimento1 = [];
      var dataInvestimento2 = [];

      var dataImobilizado = [];
      var totalImobilizado = 0;
      var totalImobilizadoValor = 0;

      if (res.data.results != null) {
        var investimentos = res.data.results.filter(function (item) {
          return item.tipoPatrimonio === 3;
        });

        var imobilizado = res.data.results.filter(function (item) {
          return item.tipoPatrimonio !== 3;
        });

        var $this = this;

        if (imobilizado != null && imobilizado.length > 0) {
          imobilizado.forEach((item) => {
            if (item.responsavel) {
              item.responsavelId = item.responsavel.id;
              item.nomeResponsavel =
                item.responsavel.nome + " " + item.responsavel.sobrenome;
            }
            if (item.tipoInvestimento) {
              item.nomeTipoInvestimento =
                global.tipoInvestimento[item.tipoInvestimento];
            }

            if (item.tipoPatrimonio != null) {
              item.nomeTipoPatrimonio =
                global.tipoPatrimonio[item.tipoPatrimonio];
            }

            totalImobilizado += item.valorAtual;
            totalImobilizadoValor += item.valorAtual;
            dataImobilizado.push(item);
          });
        }

        if (investimentos == null || investimentos.length === 0) {
          //global.spinnerHide($, currentScroll);
        } else {
          investimentos.forEach((item) => {
            //console.log(item)
            if (item.responsavel) {
              item.responsavelId = item.responsavel.id;
              item.nomeResponsavel =
                item.responsavel.nome + " " + item.responsavel.sobrenome;
            }
            if (item.tipoInvestimento) {
              item.nomeTipoInvestimento =
                global.tipoInvestimento[item.tipoInvestimento];
            }

            if (item.tipoPatrimonio != null) {
              item.nomeTipoPatrimonio =
                global.tipoPatrimonio[item.tipoPatrimonio];
            }

            if (this.state.responsaveis.length > 1) {
              if (this.state.responsaveis[0].id === item.responsavelId) {
                dataInvestimento1.push(item);
              } else {
                dataInvestimento2.push(item);
              }
            } else {
              dataInvestimento1.push(item);
            }

            $this.buscarValorTotalEDestinadoDoPatrimonioFinanceiro(
              item.id,
              item.responsavelId
            );
          });
        }

        if (this.state.responsaveis.length > 0) {
          this.state.responsaveis.forEach((item) => {
            this.atualizarDadosCarteira(item.idContaFinanceira);
          });
        }

        global.spinnerHide($, currentScroll);
      }

      this.setState({ dataInvestimento1: dataInvestimento1 });

      this.setState({ dataInvestimento2: dataInvestimento2 });

      this.setState({ dataImobilizado: dataImobilizado });

      this.setState({
        totalImobilizado: [
          `Valor total: ${totalImobilizado
            .toLocaleString("pt-BR", { style: "currency", currency: "BRL" })
            .replace("R$", "")}`,
        ],
        totalImobilizadoValor: totalImobilizado,
      });
    });
  };

  getData = () => {
    if (this.state.interval != null) {
      clearInterval(this.state.interval);
    }

    var currentScroll = [
      document.documentElement.scrollLeft || document.body.scrollLeft,
      document.documentElement.scrollTop || document.body.scrollTop,
    ];

    global.spinnerShow($);

    //var url = global.server_api + 'api/pessoa/familia/'+this.state.familiaId+'/filtro';
    var url =
      global.server_api_new +
      global.apiToken +
      "/pessoa/familia/" +
      this.state.familiaId +
      "/filtro";

    var config = {
      headers: {
        Authorization: "bearer " + this.state.accessToken,
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Headers": "Authorization",
        "Access-Control-Allow-Methods":
          "GET, POST, OPTIONS, PUT, PATCH, DELETE",
      },
    };

    var $this = this;

    axios.post(url, { ApenasNome: true }, config).then((res) => {
      const responsaveis = res.data.results;
      var responsaveisCombo = [];
      var responsaveisComCarteira = [];

      if (responsaveis != null) {
        responsaveis.forEach((item) => {
          if (item.temCarteira === 1) {
            responsaveisCombo.push({
              value: item,
              label: item.nome + " " + item.sobrenome,
            });

            if (
              item.idContaFinanceira != null &&
              item.idContaFinanceira !== ""
            ) {
              responsaveisComCarteira.push(item);
              //this.atualizarDadosCarteira(item.idContaFinanceira);
            }
          }
        });

        $this.getPatrimonios(currentScroll);
      }
      //Nao tem responsaveis
      else {
        global.spinnerHide($, currentScroll);
      }

      this.setState({
        responsaveis: responsaveisComCarteira,
        responsaveisCombo: responsaveisCombo,
      });
    });
  };

  atualizarDadosCarteira = (idCarteira) => {
    var $this = this;
    setTimeout(function () {
      $this.atualizarConsolidacaoCarteira(idCarteira);
    }, 50);
    setTimeout(function () {
      $this.atualizarGraficoCarteira(idCarteira, 1);
    }, 50);
    setTimeout(function () {
      $this.atualizarGraficoCarteira(idCarteira, 2);
    }, 50);
    setTimeout(function () {
      $this.atualizarProdutosNasCarteiras(idCarteira);
    }, 50);
  };

  atualizarProdutosNasCarteiras = (idCarteira) => {
    //var url = global.server_api + 'api/patrimonio/carteira/buscaProdutos/' + idCarteira;
    var url =
      global.server_api_new +
      global.apiToken +
      "/patrimonio/carteira/buscaProdutos/" +
      idCarteira;

    var config = {
      headers: {
        Authorization: "bearer " + this.state.accessToken,
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Headers": "Authorization",
        "Access-Control-Allow-Methods":
          "GET, POST, OPTIONS, PUT, PATCH, DELETE",
      },
    };

    var $this = this;

    axios.get(url, config).then((res) => {
      if (res.data.success != null && res.data.success === true) {
        var produtosNasCarteiras = this.state.produtosNasCarteiras;
        produtosNasCarteiras[idCarteira] = res.data.results;
        this.setState({ produtosNasCarteiras: produtosNasCarteiras });

        if (res.data.results != null) {
          res.data.results.forEach(function (value) {
            var portfolioProdutoId = parseInt(value);
            $this.atualizarConsolidacaoProduto(idCarteira, portfolioProdutoId);
            $this.atualizarGraficoProduto(idCarteira, portfolioProdutoId, 1);
          });
        }
      }
    });
  };

  atualizarConsolidacaoProduto = (idCarteira, idPortfolioProduto) => {
    //var url = global.server_api + 'api/patrimonio/carteira/consolidacao/produto/' + idPortfolioProduto;
    var url =
      global.server_api_new +
      global.apiToken +
      "/patrimonio/carteira/consolidacao/produto/" +
      idPortfolioProduto;

    var config = {
      headers: {
        Authorization: "bearer " + this.state.accessToken,
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Headers": "Authorization",
        "Access-Control-Allow-Methods":
          "GET, POST, OPTIONS, PUT, PATCH, DELETE",
      },
    };

    var $this = this;

    axios.get(url, config).then((res) => {
      if (res.data.success != null && res.data.success === true) {
        var consolidacaoProdutosNasCarteiras = this.state
          .consolidacaoProdutosNasCarteiras;

        if (consolidacaoProdutosNasCarteiras[idCarteira] == null) {
          consolidacaoProdutosNasCarteiras[idCarteira] = {};
        }

        consolidacaoProdutosNasCarteiras[idCarteira][idPortfolioProduto] =
          res.data.data;

        $this.setState({
          consolidacaoProdutosNasCarteiras: consolidacaoProdutosNasCarteiras,
        });
      }
    });
  };

  atualizarGraficoProduto = (idCarteira, idPortfolioProduto) => {
    //var url = global.server_api + 'api/patrimonio/carteira/grafico/produto/' + idPortfolioProduto;
    var url =
      global.server_api_new +
      global.apiToken +
      "/patrimonio/carteira/grafico/produto/" +
      idPortfolioProduto;

    var config = {
      headers: {
        Authorization: "bearer " + this.state.accessToken,
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Headers": "Authorization",
        "Access-Control-Allow-Methods":
          "GET, POST, OPTIONS, PUT, PATCH, DELETE",
      },
    };

    var $this = this;

    axios.get(url, config).then((res) => {
      if (res.data.success != null && res.data.success === true) {
        var series = res.data.data.series;
        var options = {
          chart: {
            height: 280,
            type: "area",
          },
          dataLabels: {
            enabled: false,
          },
          stroke: {
            curve: "smooth",
          },
          yaxis: {
            labels: {
              formatter: function (value) {
                return value != null
                  ? parseFloat(value).toLocaleString("pt-BR", {
                      minimumFractionDigits: 2,
                      maximumFractionDigits: 2,
                    }) + "%"
                  : "";
              },
            },
          },
          xaxis: {
            categories: res.data.data.categories,
            axisTicks: {
              show: false,
            },
          },
        };

        var graficoConsolidacaoProdutosNasCarteiras = this.state
          .graficoConsolidacaoProdutosNasCarteiras;

        if (graficoConsolidacaoProdutosNasCarteiras[idCarteira] == null) {
          graficoConsolidacaoProdutosNasCarteiras[idCarteira] = {};
        }

        graficoConsolidacaoProdutosNasCarteiras[idCarteira][
          idPortfolioProduto
        ] = {
          series: series,
          options: options,
        };

        $this.setState({
          graficoConsolidacaoProdutosNasCarteiras: graficoConsolidacaoProdutosNasCarteiras,
        });
      }
    });
  };

  atualizarConsolidacaoCarteira = (idCarteira) => {
    //var url = global.server_api + 'api/patrimonio/carteira/consolidacao/' + idCarteira;
    var url =
      global.server_api_new +
      global.apiToken +
      "/patrimonio/carteira/consolidacao/" +
      idCarteira;

    var config = {
      headers: {
        Authorization: "bearer " + this.state.accessToken,
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Headers": "Authorization",
        "Access-Control-Allow-Methods":
          "GET, POST, OPTIONS, PUT, PATCH, DELETE",
      },
    };

    var $this = this;

    axios.get(url, config).then((res) => {
      if (res.data.success != null && res.data.success === true) {
        var consolidacaoCarteiras = this.state.consolidacaoCarteiras;
        consolidacaoCarteiras[idCarteira] = res.data.data;
        $this.setState({ consolidacaoCarteiras: consolidacaoCarteiras });
      }
    });
  };

  atualizarGraficoCarteira = (idCarteira, tipoCarteira) => {
    //var url = global.server_api + 'api/patrimonio/carteira/grafico/' + idCarteira + "/" + tipoCarteira;
    var url =
      global.server_api_new +
      global.apiToken +
      "/patrimonio/carteira/grafico/" +
      idCarteira +
      "/" +
      tipoCarteira;

    var config = {
      headers: {
        Authorization: "bearer " + this.state.accessToken,
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Headers": "Authorization",
        "Access-Control-Allow-Methods":
          "GET, POST, OPTIONS, PUT, PATCH, DELETE",
      },
    };

    var $this = this;

    axios.get(url, config).then((res) => {
      if (res.data.success != null && res.data.success === true) {
        var series = res.data.data.series;
        var options = {
          chart: {
            height: 280,
            type: "area",
          },
          dataLabels: {
            enabled: false,
          },
          stroke: {
            curve: "smooth",
          },
          yaxis: {
            labels: {
              formatter: function (value) {
                return value != null
                  ? parseFloat(value).toLocaleString("pt-BR", {
                      minimumFractionDigits: 2,
                      maximumFractionDigits: 2,
                    })
                  : "";
              },
            },
          },
          xaxis: {
            categories: res.data.data.categories,
            axisTicks: {
              show: false,
            },
          },
        };

        if (tipoCarteira === 1) {
          var graficoConsolidacaoCarteiras = this.state
            .graficoConsolidacaoCarteiras;

          graficoConsolidacaoCarteiras[idCarteira] = {
            series: series,
            options: options,
          };

          $this.setState({
            graficoConsolidacaoCarteiras: graficoConsolidacaoCarteiras,
          });
        } else if (tipoCarteira === 2) {
          var graficoRentabilidadeCarteiras = this.state
            .graficoRentabilidadeCarteiras;

          graficoRentabilidadeCarteiras[idCarteira] = {
            series: series,
            options: options,
          };

          $this.setState({
            graficoRentabilidadeCarteiras: graficoRentabilidadeCarteiras,
          });
        }
      }
    });
  };

  editarRegistro = (idPatrimonio, usuario) => {
    var id = idPatrimonio;
    var data = [];

    if (usuario === "usuario1") {
      data = this.state.dataInvestimento1;
    }

    if (usuario === "usuario2") {
      data = this.state.dataInvestimento2;
    }

    if (usuario === "imobilizado") {
      data = this.state.dataImobilizado;
    }

    if (id == null) return;

    var items = data.filter(function (temp) {
      return parseInt(temp.id) === parseInt(id);
    });

    if (items != null && items.length > 0) {
      var item = items[0];

      for (var prop in item) {
        if (item[prop] == null) {
          delete item[prop];
          continue;
        }
      }

      delete item.familiaId;
      delete item.empresaId;

      if (item.patrimonio != null) {
        item.patrimonioId = item.patrimonio.id;
      }

      if (item.responsavel != null) {
        item.responsavelId = item.responsavel.id;
      }

      if (item.produtoCustomizado != null) {
        item.produtoCustomizadoNome = item.produtoCustomizado.nome;
        item.produtoCustomizadoTipoInvestimento =
          item.produtoCustomizado.tipoInvestimento;
        item.produtoCustomizadoMetodologiaDePreco =
          item.produtoCustomizado.metodologiaDePreco;
        item.produtoCustomizadoIdProdutoCustomizado =
          item.produtoCustomizado.idProdutoCustomizado;
        item.produtoCustomizadoIdInstituicaoFinanceira =
          item.produtoCustomizado.idInstituicaoFinanceira;
        item.produtoCustomizadoIdCarteira = item.produtoCustomizado.idCarteira;
      }

      this.setState({ item: item }, function () {
        global.showModal(this.modal);
      });
    }
  };

  salvar = () => {
    //var url = global.server_api + 'api/patrimonio' + (this.state.item.id != null ? "/" + this.state.item.id: "");
    var url =
      global.server_api_new +
      global.apiToken +
      "/patrimonio" +
      (this.state.item.id != null ? "/" + this.state.item.id : "");

    var config = {
      headers: {
        Authorization: "bearer " + this.state.accessToken,
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Headers": "Authorization",
        "Access-Control-Allow-Methods":
          "GET, POST, OPTIONS, PUT, PATCH, DELETE",
      },
    };

    var currentScroll = [
      document.documentElement.scrollLeft || document.body.scrollLeft,
      document.documentElement.scrollTop || document.body.scrollTop,
    ];

    global.spinnerShow($);

    var item = this.state.item;

    item.empresaId = this.state.empresaId;
    item.familiaId = this.state.familiaId;

    if (item.tipoPatrimonio === 3 && item.tipoInvestimento === 99) {
      item.produtoCustomizado = {
        nome: item.produtoCustomizadoNome,
        metodologiaDePreco: item.produtoCustomizadoMetodologiaDePreco,
        tipoInvestimento: item.produtoCustomizadoTipoInvestimento,
        idProdutoCustomizado: item.produtoCustomizadoIdProdutoCustomizado,
        idInstituicaoFinanceira: item.produtoCustomizadoIdInstituicaoFinanceira,
        idCarteira: item.produtoCustomizadoIdCarteira,
      };
    } else {
      item.produtoCustomizado = null;
    }

    if (item.tipoPatrimonio != null && item.tipoPatrimonio !== 3) {
      item.qtdAtual = 1;
    }

    if (item.responsavel != null) {
      item.responsavelId = item.responsavel.id;
    }

    axios.post(url, item, config).then((res) => {
      global.spinnerHide($, currentScroll);

      if (res.data.success === true) {
        this.modal.hide();
        this.limparSelecao();
        // this.getData();
        if (
          item.tipoPatrimonio != null &&
          item.tipoPatrimonio === 3 &&
          item.id == null
        ) {
          sessionStorage.setItem("refresh", "true");
          window.location.reload();
        }
      } else {
        console.log(res.data.exception);
        const options = {
          title: "Erro ao salvar patrimônio",
          message: res.data.exception.Message,
          buttons: [
            {
              label: "Ok",
            },
          ],
        };

        confirmAlert(options);
      }
    });
  };

  buscarValorTotalEDestinadoDoPatrimonioFinanceiro = (
    patrimonioId,
    responsavelId
  ) => {
    //var url = global.server_api + 'api/patrimonio/destinacao/familia/' + this.state.familiaId + '/compras/' + patrimonioId;
    var url =
      global.server_api_new +
      global.apiToken +
      "/patrimonio/destinacao/familia/" +
      this.state.familiaId +
      "/compras/" +
      patrimonioId;

    var config = {
      headers: {
        Authorization: "bearer " + this.state.accessToken,
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Headers": "Authorization",
        "Access-Control-Allow-Methods":
          "GET, POST, OPTIONS, PUT, PATCH, DELETE",
      },
    };

    axios.get(url, config).then((res) => {
      if (res.data.success === true) {
        if (res.data.singleResult == null) {
          console.log("Não veio nenhum resultado");
          return;
        }

        var data =
          this.state.responsaveis != null &&
          this.state.responsaveis.length > 0 &&
          this.state.responsaveis[0].id === responsavelId
            ? this.state.dataInvestimento1
            : this.state.dataInvestimento2;

        var rowIndex = -1;

        var patrimonioFiltrado = data.filter(function (it, index) {
          var result = it.id === patrimonioId;

          if (result) {
            rowIndex = index;
          }

          return result;
        });

        if (
          data != null &&
          data[rowIndex] != null &&
          patrimonioFiltrado != null &&
          patrimonioFiltrado.length === 1
        ) {
          if (data[rowIndex].id !== patrimonioId) {
            console.log(
              "Pegamos o rowIndex errado para o patrimonioId",
              patrimonioId,
              data[rowIndex].id,
              rowIndex
            );
          } else {
            data[rowIndex].valorAplicado =
              res.data.singleResult.totalPatrimonio != null
                ? res.data.singleResult.totalPatrimonio
                : 0;

            data[rowIndex].valorAtual =
              res.data.singleResult.totalPatrimonioCotizado != null
                ? res.data.singleResult.totalPatrimonioCotizado
                : 0;

            if (this.state.responsaveis[0].id === responsavelId) {
              this.setState({ dataInvestimento1: data });
              this.getTiposInvestimentoeTotal("usuario1");
            }

            if (
              this.state.responsaveis[1] &&
              this.state.responsaveis[1].id === responsavelId
            ) {
              this.setState({ dataInvestimento2: data });
              this.getTiposInvestimentoeTotal("usuario2");
            }
          }
        }
      } else {
        console.log(res.data.exception);
      }
    });
  };

  buscaInvestimentoNaLista = (array, investimentoId) => {
    //checa se aquele tipo de investimento já existe no array, se existir retorna o índice
    var indexFiltrado = null;

    array.filter(function (arrItem, index) {
      if (arrItem.tipoInvestimento === investimentoId) {
        indexFiltrado = index;
        return indexFiltrado;
      }
      return false;
    });

    return indexFiltrado;
  };

  getTiposInvestimentoeTotal = (usuario) => {
    // soma o valor aplicado e atual de cada tipo de investimento

    var currentScroll = [
      document.documentElement.scrollLeft || document.body.scrollLeft,
      document.documentElement.scrollTop || document.body.scrollTop,
    ];

    var totalAplicado = 0;
    var totalAtual = 0;
    var tiposInvestimentoUsuario = [];
    var dataInvestimento = [];

    if (usuario === "usuario1") {
      dataInvestimento = this.state.dataInvestimento1;
    }

    if (usuario === "usuario2") {
      dataInvestimento = this.state.dataInvestimento2;
    }

    dataInvestimento.forEach((item) => {
      if (item.valorAplicado) {
        totalAplicado += item.valorAplicado;
      }
      if (item.valorAtual) {
        totalAtual += item.valorAtual;
      }

      var indexInvestimento = this.buscaInvestimentoNaLista(
        tiposInvestimentoUsuario,
        item.tipoInvestimento
      );

      if (indexInvestimento === null) {
        tiposInvestimentoUsuario.push({
          tipoInvestimento: item.tipoInvestimento,
          nomeTipoInvestimento: item.nomeTipoInvestimento,
          valorAplicado: item.valorAplicado,
          valorAtual: item.valorAtual,
          distribuição: 0,
        });
      } else {
        if (item.valorAplicado) {
          tiposInvestimentoUsuario[indexInvestimento].valorAplicado +=
            item.valorAplicado;
        }
        if (item.valorAtual) {
          tiposInvestimentoUsuario[indexInvestimento].valorAtual +=
            item.valorAtual;
        }
      }
    });

    tiposInvestimentoUsuario.forEach((item) => {
      item.distribuição = item.valorAtual / totalAtual;
    });

    if (usuario === "usuario1") {
      this.setState({
        totalAplicado1: `Total aplicado: ${totalAplicado
          .toLocaleString("pt-BR", { style: "currency", currency: "BRL" })
          .replace("R$", "")}`,
        totalAtual1: `Total atual: ${totalAtual
          .toLocaleString("pt-BR", { style: "currency", currency: "BRL" })
          .replace("R$", "")}`,
        tiposInvestimento1: tiposInvestimentoUsuario,
      });
      global.spinnerHide($, currentScroll);
    }

    if (usuario === "usuario2") {
      this.setState({
        totalAplicado2: `Total aplicado: ${totalAplicado
          .toLocaleString("pt-BR", { style: "currency", currency: "BRL" })
          .replace("R$", "")}`,
        totalAtual2: `Total atual: ${totalAtual
          .toLocaleString("pt-BR", { style: "currency", currency: "BRL" })
          .replace("R$", "")}`,
        tiposInvestimento2: tiposInvestimentoUsuario,
      });
      global.spinnerHide($, currentScroll);
    }
  };

  setItemOnState = (key, value, callback) => {
    var item = this.state.item;

    item[key] = value;

    if (callback) this.setState({ item: item }, callback);
    else this.setState({ item: item });
  };

  gerarDatePicker = (id, fieldKey, defaultDate) => {
    var currentDate = null;

    var item = this.state.item;

    var value = item[fieldKey];

    //date type
    if (value != null) {
      //string vinda da edicao
      if (typeof value === "string") {
        var temp = this.moment(value, "YYYY-MM-DDThh:mm:ss");
        currentDate = temp.toDate();
        // this.setItemOnState(fieldKey, temp);
      }
      //data vinda da selecao atraves do calendario
      else {
        currentDate = value;
      }
    }
    //momentjs date
    else if (defaultDate != null) {
      currentDate = defaultDate.toDate();
      // this.setItemOnState(fieldKey, currentDate);
    }
    //now (date)
    else {
      currentDate = new Date();
      // this.setItemOnState(fieldKey, currentDate);
    }

    return (
      <Datepicker
        locale="br"
        id={id}
        dateFormat="dd/MM/yyyy"
        selected={currentDate}
        showYearDropdown
        dateFormatCalendar="MMMM"
        scrollableYearDropdown
        yearDropdownItemNumber={5}
        onChange={(date) => this.setItemOnState(fieldKey, date)}
        customInput={<MaskedInput mask="11/11/1111" />}
      />
    );
  };

  preparaDataParaGrid = (dataString) => {
    var data = this.moment(dataString, "YYYY-MM-DDThh:mm:ss");
    return data.format("DD/MM/YYYY");
  };

  limparSelecao = () => {
    this.setState({
      item: {
        descricao: "",
        tipoPatrimonio: null,
        dataPrimeiraCompra: null,
        tipoInvestimento: null,
        responsavel: null,
        valorAtual: "",
        valorAplicado: "",
        produtoCustomizadoNome: "",
        produtoCustomizadoTipoInvestimento: null,
        produtoCustomizadoMetodologiaDePreco: null,

        idCarteira: null,
        idProdutoNoPortfolio: null,
        cnpj: "",
        idAcaoBovespa: null,
        idTesouroDireto: null,
        idDebenture: null,
        dataVencimento: null,
        idEmissor: null,
        idProduto: null,
        idIndexador: null,
        serieDaMoeda: null,
      },
    });
    //console.log('limpou');
  };

  removerRegistros = (patrimonioId) => {
    var id = patrimonioId;

    const options = {
      title: "Exclusão",
      message:
        "Tem certeza que deseja excluir? Este procedimento é irreversível e APAGARÁ também os lançamentos associados a este patrimônio!",
      buttons: [
        {
          label: "Sim",
          onClick: () => {
            //var url = global.server_api + 'api/patrimonio/bulk/' + id;
            var url =
              global.server_api_new +
              global.apiToken +
              "/patrimonio/bulk/" +
              id;

            var config = {
              headers: {
                Authorization: "bearer " + this.state.accessToken,
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Headers": "Authorization",
                "Access-Control-Allow-Methods":
                  "GET, POST, OPTIONS, PUT, PATCH, DELETE",
              },
            };

            axios.delete(url, {}, config).then((res) => {
              const result = res.data;

              if (result.success && result.affectedResults > 0) {
                //this.getData();
                window.location.reload();
              } else {
                console.log(res.data.exception);
                alert("Erro ao remover itens. " + res.data.exception.Message);
              }
            });
          },
        },
        {
          label: "Não",
        },
      ],
    };

    confirmAlert(options);

    return false;
  };

  filtroPorTexto = (valorDigitado) => {
    const textoDigitado = valorDigitado;

    this.setState({ textoDigitado }, function () {
      if (this.state.interval != null) {
        clearInterval(this.state.interval);
      }

      const $this = this;

      const interval = setInterval(function () {
        $this.getData();
      }, 1000);

      this.setState({ interval });
    });
  };

  mudarResponsavel = (selectedOption) => {
    this.setItemOnState("responsavel", selectedOption.value);
  };

  montarDropDown = (itens, id, changeMethod, selectedValue, type) => (
    <Select
      id={id}
      onChange={changeMethod}
      className="select-component"
      options={itens}
      defaultValue={itens != null ? itens[0] : ""}
      placeholder="Selecione..."
      value={
        itens != null && selectedValue != null
          ? itens.find((op) => {
              if (type === "enum") {
                return (
                  (op.value != null ? op.value.toString() : null) ===
                  (selectedValue != null ? selectedValue.toString() : null)
                );
              } else if (op.value != null && selectedValue != null) {
                return op.value.id === selectedValue.id;
              } else {
                return (
                  (selectedValue != null ? parseInt(selectedValue) : null) ===
                  (op.value != null && op.value.id
                    ? parseInt(op.value.id)
                    : null)
                );
              }
            })
          : null
      }
    />
  );

  mostrarPopup = () => (
    <div className="planosSonhos patrimonio-popup">
      <SkyLight
        ref={(ref) => (this.modal = ref)}
        transitionDuration={0}
        beforeOpen={this.preCarregarParaEdicao}
        afterClose={this.limparSelecao}
        dialogStyles={{ borderRadius: "1rem", padding: "2rem" }}
        title={
          <div className="pop-up-title">
            <i className="ni ni-check-bold"></i>
            <h2>Cadastro de Patrimônios</h2>
          </div>
        }
      >
        <Row>
          <Col lg="6" xl="6">
            <div>
              <label>Tipo de Patrimônio *</label>
              <Select
                onChange={(selectedOption) => {
                  var msg =
                    this.state.tipoUsuario > 1 ? (
                      <div style={{ padding: "5px 0px" }}>
                        <p style={{ padding: "0px", margin: "0px" }}>
                          Nenhum usuário foi marcado como responsável por
                          investimentos, "ative" esta opção na edição de
                          usuários.
                        </p>
                        <p style={{ padding: "0px", margin: "0px" }}>
                          Configurações <i class="fas fa-caret-right"></i>{" "}
                          Usuários <i class="fas fa-caret-right"></i> Clique em
                          um usuário e marque a opção "Possui Investimentos"
                        </p>
                      </div>
                    ) : (
                      <div style={{ padding: "5px 0px" }}>
                        <p style={{ padding: "0px", margin: "0px" }}>
                          Nenhum usuário foi marcado como responsável por
                          investimentos. Essa opção pode ser ativada pelo seu
                          Planejador Financeiro.
                        </p>
                      </div>
                    );

                  var botoes =
                    this.state.tipoUsuario > 1
                      ? [
                          {
                            label: "Alterar Agora",
                            onClick: () => {
                              window.location.href = "/admin/usuarios";
                            },
                          },
                          {
                            label: "Ok",
                          },
                        ]
                      : [
                          {
                            label: "Ok",
                          },
                        ];

                  if (
                    selectedOption.value === 3 &&
                    this.state.responsaveisCombo.length === 0
                  ) {
                    //this.modal.hide();
                    const options = {
                      title: "Responsável pelos Investimentos",
                      message: msg,
                      buttons: botoes,
                    };

                    confirmAlert(options);
                  } else {
                    this.setItemOnState("tipoPatrimonio", selectedOption.value);
                  }
                }}
                className="select-component"
                options={this.state.tiposPatrimonio}
                defaultValue={this.state.tiposPatrimonio[0]}
                placeholder="Selecione..."
                value={
                  this.state.item.tipoPatrimonio != null
                    ? this.state.tiposPatrimonio.find((op) => {
                        return op.value === this.state.item.tipoPatrimonio;
                      })
                    : null
                }
              />
            </div>
          </Col>
          <Col
            lg="6"
            xl="6"
            style={
              this.state.item.tipoPatrimonio === 3 ? {} : { display: "none" }
            }
          >
            <div>
              <label>Tipo de Investimento *</label>
              <Select
                onChange={(selectedOption) =>
                  this.setItemOnState("tipoInvestimento", selectedOption.value)
                }
                className="select-component"
                options={this.state.tiposInvestimento}
                defaultValue={this.state.tiposInvestimento[0]}
                placeholder="Selecione..."
                value={
                  this.state.item.tipoInvestimento != null
                    ? this.state.tiposInvestimento.find((item) => {
                        return (
                          parseInt(item.value) ===
                          parseInt(this.state.item.tipoInvestimento)
                        );
                      })
                    : null
                }
              />
            </div>
          </Col>
          <Col
            lg="6"
            xl="6"
            style={
              this.state.item.tipoPatrimonio === 3 ? {} : { display: "none" }
            }
          >
            <div>
              <label>Responsável pela Carteira *</label>
              {this.montarDropDown(
                this.state.responsaveisCombo,
                "select-responsavel",
                this.mudarResponsavel,
                this.state.item.responsavel,
                "model"
              )}
            </div>
          </Col>
          <Col lg="6" xl="6">
            <div>
              <label>Descrição do Patrimônio *</label>
              <input
                style={{ paddingLeft: 25 }}
                type="text"
                value={this.state.item.descricao}
                onChange={(e) =>
                  this.setItemOnState("descricao", e.target.value)
                }
              ></input>
            </div>
          </Col>
          <Col
            lg="6"
            xl="6"
            style={
              this.state.item.tipoPatrimonio != null &&
              this.state.item.tipoPatrimonio !== 3
                ? {}
                : { display: "none" }
            }
          >
            <div>
              <label>Data da Compra *</label>
              {this.gerarDatePicker(
                "data-compra",
                "dataPrimeiraCompra",
                this.moment()
              )}
            </div>
          </Col>
          <Col
            lg="6"
            xl="6"
            style={
              this.state.item.tipoPatrimonio != null &&
              this.state.item.tipoPatrimonio !== 3
                ? {}
                : { display: "none" }
            }
          >
            <div>
              <label>
                Valor{" "}
                {this.state.item.tipoPatrimonio != null &&
                this.state.item.tipoPatrimonio === 1
                  ? "de mercado"
                  : ""}
                *
              </label>
              <CurrencyInput
                id="valor"
                style={{ paddingLeft: 25 }}
                value={
                  this.state.item.valorAtualFormatado != null
                    ? this.state.item.valorAtualFormatado
                    : this.state.item.valorAtual
                }
                thousandSeparator={"."}
                decimalSeparator={","}
                precision="2" //prefix={'R$ '}
                onChangeEvent={(event, formattedValue, value) => {
                  this.setItemOnState("valorAtual", value);
                  this.setItemOnState("valorAtualFormatado", formattedValue);
                }}
              />
            </div>
          </Col>
          <Col
            lg="6"
            xl="6"
            style={
              parseInt(this.state.item.tipoPatrimonio) === 3 &&
              parseInt(this.state.item.tipoInvestimento) === 99
                ? {}
                : { display: "none" }
            }
          >
            <div>
              <label>Nome do Produto *</label>
              <input
                type="text"
                value={this.state.item.produtoCustomizadoNome}
                onChange={(e) =>
                  this.setItemOnState("produtoCustomizadoNome", e.target.value)
                }
              ></input>
            </div>
          </Col>
          <Col
            lg="6"
            xl="6"
            style={
              parseInt(this.state.item.tipoPatrimonio) === 3 &&
              parseInt(this.state.item.tipoInvestimento) === 99
                ? {}
                : { display: "none" }
            }
          >
            <div>
              <label>Tipo do Produto *</label>
              <Select
                onChange={(selectedOption) =>
                  this.setItemOnState(
                    "produtoCustomizadoTipoInvestimento",
                    selectedOption.value
                  )
                }
                className="select-component"
                options={this.state.tiposInvestimentoSemCustomizado}
                defaultValue={this.state.tiposInvestimentoSemCustomizado[0]}
                placeholder="Selecione..."
                value={
                  this.state.item.produtoCustomizadoTipoInvestimento != null
                    ? this.state.tiposInvestimentoSemCustomizado.find(
                        (item) => {
                          return (
                            parseInt(item.value) ===
                            parseInt(
                              this.state.item.produtoCustomizadoTipoInvestimento
                            )
                          );
                        }
                      )
                    : null
                }
              />
            </div>
          </Col>
          <Col
            lg="6"
            xl="6"
            style={
              parseInt(this.state.item.tipoPatrimonio) === 3 &&
              parseInt(this.state.item.tipoInvestimento) === 99
                ? {}
                : { display: "none" }
            }
          >
            <label>Metodologia de Preço *</label>
            <Select
              onChange={(selectedOption) =>
                this.setItemOnState(
                  "produtoCustomizadoMetodologiaDePreco",
                  selectedOption.value
                )
              }
              className="select-component"
              options={this.state.metodologiasDePreco}
              defaultValue={this.state.metodologiasDePreco[0]}
              placeholder="Selecione..."
              value={
                this.state.item.produtoCustomizadoMetodologiaDePreco != null
                  ? this.state.metodologiasDePreco.find((item) => {
                      return (
                        parseInt(item.value) ===
                        parseInt(
                          this.state.item.produtoCustomizadoMetodologiaDePreco
                        )
                      );
                    })
                  : null
              }
            />
          </Col>
          <Col
            lg="6"
            xl="6"
            style={
              parseInt(this.state.item.tipoPatrimonio) === 3 &&
              (parseInt(this.state.item.tipoInvestimento) === 1 ||
                parseInt(this.state.item.tipoInvestimento) === 2)
                ? {}
                : { display: "none" }
            }
          >
            <div>
              <label>CNPJ *</label>
              <MaskedInput
                name="CNPJ"
                className="form-control"
                mask={"11.111.111/1111-11"}
                value={this.state.item.cnpj}
                onChange={(e) => {
                  this.setItemOnState("cnpj", e.target.value);
                }}
              />
            </div>
          </Col>
          <Col
            lg="6"
            xl="6"
            style={
              parseInt(this.state.item.tipoPatrimonio) === 3 &&
              parseInt(this.state.item.tipoInvestimento) === 8
                ? {}
                : { display: "none" }
            }
          >
            <div>
              <label>Ações, FII, ETFs *</label>
              {this.montarDropDown(
                this.state.acoesBovespa,
                "lancamentoIdAcaoBovespa",
                (selectedOption) => {
                  this.setItemOnState("idAcaoBovespa", selectedOption.value);
                },
                this.state.item.idAcaoBovespa,
                "enum"
              )}
            </div>
          </Col>
          <Col
            lg="6"
            xl="6"
            style={
              parseInt(this.state.item.tipoPatrimonio) === 3 &&
              parseInt(this.state.item.tipoInvestimento) === 10
                ? {}
                : { display: "none" }
            }
          >
            <div>
              <label>Moeda *</label>
              {this.montarDropDown(
                this.state.seriesMoedas,
                "lancamentoSerieDaMoeda",
                (selectedOption) => {
                  this.setItemOnState("serieDaMoeda", selectedOption.value);
                },
                this.state.item.serieDaMoeda,
                "enum"
              )}
            </div>
          </Col>
          <Col
            lg="6"
            xl="6"
            style={
              parseInt(this.state.item.tipoPatrimonio) === 3 &&
              parseInt(this.state.item.tipoInvestimento) === 9
                ? {}
                : { display: "none" }
            }
          >
            <div>
              <label>Debênture *</label>
              {this.montarDropDown(
                this.state.debentures,
                "lancamentoIdDebenture",
                (selectedOption) => {
                  this.setItemOnState("idDebenture", selectedOption.value);
                },
                this.state.item.idDebenture,
                "enum"
              )}
            </div>
          </Col>
          <Col
            lg="6"
            xl="6"
            style={
              parseInt(this.state.item.tipoPatrimonio) === 3 &&
              parseInt(this.state.item.tipoInvestimento) === 4
                ? {}
                : { display: "none" }
            }
          >
            <div>
              <label>Título do Tesouro *</label>
              {this.montarDropDown(
                this.state.tesouros,
                "lancamentoIdTesouroDireto",
                (selectedOption) => {
                  this.setItemOnState("idTesouroDireto", selectedOption.value);
                },
                this.state.item.idTesouroDireto,
                "enum"
              )}
            </div>
          </Col>
          <Col
            lg="6"
            xl="6"
            style={
              parseInt(this.state.item.tipoPatrimonio) === 3 &&
              parseInt(this.state.item.tipoInvestimento) === 3
                ? {}
                : { display: "none" }
            }
          >
            <div>
              <label>Produto *</label>
              {this.montarDropDown(
                this.state.produtosRendaFixa,
                "lancamentoIdProduto",
                (selectedOption) => {
                  this.setItemOnState("idProduto", selectedOption.value);
                },
                this.state.item.idProduto,
                "enum"
              )}
            </div>
          </Col>
          <Col
            lg="6"
            xl="6"
            style={
              parseInt(this.state.item.tipoPatrimonio) === 3 &&
              parseInt(this.state.item.tipoInvestimento) === 6
                ? {}
                : { display: "none" }
            }
          >
            <div>
              <label>Produto *</label>
              {this.montarDropDown(
                this.state.produtosRendaFixaPre,
                "lancamentoIdProduto",
                (selectedOption) => {
                  this.setItemOnState("idProduto", selectedOption.value);
                },
                this.state.item.idProduto,
                "enum"
              )}
            </div>
          </Col>
          <Col
            lg="6"
            xl="6"
            style={
              parseInt(this.state.item.tipoPatrimonio) === 3 &&
              (parseInt(this.state.item.tipoInvestimento) === 3 ||
                parseInt(this.state.item.tipoInvestimento) === 6)
                ? {}
                : { display: "none" }
            }
          >
            <div>
              <label>Emissor *</label>
              {this.montarDropDown(
                this.state.instituicoesFinanceiras,
                "lancamentoIdEmissor",
                (selectedOption) => {
                  this.setItemOnState("idEmissor", selectedOption.value);
                },
                this.state.item.idEmissor,
                "enum"
              )}
            </div>
          </Col>
          <Col
            lg="6"
            xl="6"
            style={
              parseInt(this.state.item.tipoPatrimonio) === 3 &&
              parseInt(this.state.item.tipoInvestimento) === 3
                ? {}
                : { display: "none" }
            }
          >
            <div>
              <label>Indexador *</label>
              {this.montarDropDown(
                this.state.indexadores,
                "lancamentoIdIndexador",
                (selectedOption) => {
                  this.setItemOnState("idIndexador", selectedOption.value);
                },
                this.state.item.idIndexador,
                "enum"
              )}
            </div>
          </Col>
          {/* <Col lg="6" xl="6"
            style={parseInt(this.state.item.tipoPatrimonio) === 3 && (parseInt(this.state.item.tipoInvestimento) === 3 || 
                  parseInt(this.state.item.tipoInvestimento) === 6) 
                  ? {} : {display:"none"}}>
            <div>
              <label>Data Vencimento *</label>
              {this.gerarDatePicker('data-vencimento', 'dataVencimento', this.moment())}
            </div>
          </Col> */}
        </Row>
        <Row
          style={
            /*(this.state.item.tipoPatrimonio === 3) ? {} : */ {
              display: "none",
            }
          }
        >
          <Col lg="12" xl="12">
            <div>
              Para cadastrar um patrimônio do tipo "Investimento", o qual
              denominamos <b>Patrimônio Financeiro</b>, o valor e a quantidade
              atuais não são informados, do mesmo modo que nos demais tipos de
              patrimônio, Neste caso, estes campos são calculados baseando-se
              nas compras e vendas de ativos realizadas para este patrimônio,
              que podem ser cadastradas através do{" "}
              <b>lançamento do tipo investimento</b> (através do botão "Novo
              Lançamento" no menu superior)
            </div>
          </Col>
        </Row>
        <Row>
          <Col lg="12" xl="12">
            <button
              onClick={() => this.salvar()}
              className="featured-button"
              style={{
                float: "right",
                backgroundImage:
                  "url(" +
                  require("../../assets/img/theme/botao-destaque.png") +
                  ")",
              }}
            >
              Salvar
            </button>
          </Col>
        </Row>
      </SkyLight>
    </div>
  );

  mostrarGrafico = (dados, mostrarLabelEixoX) => {
    if (dados == null) {
      return <></>;
    }

    if (!mostrarLabelEixoX) {
      if (dados.options.xaxis == null) {
        dados.options.xaxis = {};
      }
      if (dados.options.xaxis.labels == null) {
        dados.options.xaxis.labels = {};
      }

      dados.options.xaxis.labels.show = false;
      dados.options.xaxis.axisTicks.show = false;
    }

    return (
      <ApexChart
        options={dados.options}
        series={dados.series}
        type="area"
        height="280px"
        width="100%"
      />
    );
  };

  mostrarResultadosDasCarterias = () => {
    if (
      this.state.responsaveis == null ||
      this.state.responsaveis.length === 0
    ) {
      return <></>;
    }

    return this.state.responsaveis.map((responsavel, key) => {
      var consolidacaoCarteira = this.state.consolidacaoCarteiras[
        responsavel.idContaFinanceira
      ];
      var graficoConsolidacaoCarteiras = this.state
        .graficoConsolidacaoCarteiras[responsavel.idContaFinanceira];
      var graficoRentabilidadeCarteiras = this.state
        .graficoRentabilidadeCarteiras[responsavel.idContaFinanceira];
      var produtosNaCarteira = this.state.produtosNasCarteiras[
        responsavel.idContaFinanceira
      ];

      if (consolidacaoCarteira == null) {
        return false;
      }

      return (
        <div key={key}>
          <div id={responsavel.id} style={{ height: 50 }}></div>
          <Card style={{ marginTop: 20 }}>
            <CardHeader>
              <h3>
                <b>Carteira: </b>
                {responsavel.nome} {responsavel.sobrenome}
              </h3>
            </CardHeader>
            <CardBody>
              <Row>
                <Col className="campo-consolidado" style={{ width: "auto" }}>
                  Valor Aplicado:{" "}
                  {consolidacaoCarteira.valueApplied != null
                    ? parseFloat(
                        consolidacaoCarteira.valueApplied
                      ).toLocaleString("pt-BR", {
                        minimumFractionDigits: 2,
                        maximumFractionDigits: 2,
                      })
                    : ""}
                </Col>
                <Col className="campo-consolidado" style={{ width: "auto" }}>
                  Saldo:{" "}
                  {consolidacaoCarteira.equity != null
                    ? parseFloat(consolidacaoCarteira.equity).toLocaleString(
                        "pt-BR",
                        {
                          minimumFractionDigits: 2,
                          maximumFractionDigits: 2,
                        }
                      )
                    : ""}
                </Col>
                <Col className="campo-consolidado" style={{ width: "auto" }}>
                  Rentabilidade:{" "}
                  {consolidacaoCarteira.profitability != null
                    ? parseFloat(
                        consolidacaoCarteira.profitability
                      ).toLocaleString("pt-BR", {
                        minimumFractionDigits: 2,
                        maximumFractionDigits: 2,
                      }) + " %"
                    : ""}
                </Col>
                <Col className="campo-consolidado" style={{ width: "auto" }}>
                  Primeira Aplicação:{" "}
                  {consolidacaoCarteira.firstApplicationDate}
                </Col>
                <Col className="campo-consolidado" style={{ width: "auto" }}>
                  Última Atualização: {consolidacaoCarteira.lastUpdateDate}
                </Col>
              </Row>
              <Row>
                <Col
                  lg="6"
                  xl="6"
                  style={{ marginTop: "20px" }}
                  className="consolidacao"
                >
                  <Card>
                    <CardHeader>
                      <h3>
                        <b>Total da Carteira X Rentabilidade</b>
                      </h3>
                    </CardHeader>
                    <CardBody>
                      {this.mostrarGrafico(graficoConsolidacaoCarteiras, false)}
                    </CardBody>
                  </Card>
                </Col>
                <Col
                  lg="6"
                  xl="6"
                  style={{ marginTop: "20px" }}
                  className="rentabilidade"
                >
                  <Card>
                    <CardHeader>
                      <h3>
                        <b>Rentabilidade X CDI</b>
                      </h3>
                    </CardHeader>
                    <CardBody>
                      {this.mostrarGrafico(
                        graficoRentabilidadeCarteiras,
                        false
                      )}
                    </CardBody>
                  </Card>
                </Col>
                {produtosNaCarteira != null &&
                  this.mostrarResultadosDosProdutos(
                    responsavel.idContaFinanceira,
                    produtosNaCarteira
                  )}
              </Row>
            </CardBody>
          </Card>
        </div>
      );
    });
  };

  mostrarResultadosDosProdutos = (idContaFinanceira, produtosNaCarteira) => {
    if (produtosNaCarteira == null) {
      return <></>;
    }

    return produtosNaCarteira.map((produto, key) => {
      var consolidacaoProduto =
        this.state.consolidacaoProdutosNasCarteiras[idContaFinanceira] != null
          ? this.state.consolidacaoProdutosNasCarteiras[idContaFinanceira][
              parseInt(produto)
            ]
          : null;
      var graficoConsolidacaoProduto =
        this.state.graficoConsolidacaoProdutosNasCarteiras[idContaFinanceira] !=
        null
          ? this.state.graficoConsolidacaoProdutosNasCarteiras[
              idContaFinanceira
            ][parseInt(produto)]
          : null;

      if (consolidacaoProduto == null) {
        return false;
      }

      return (
        <Col lg="6" xl="6" key={key} style={{ marginTop: "15px" }}>
          <Card>
            <CardHeader>
              <h3>
                <b>Produto: </b>
                {consolidacaoProduto.productName != null
                  ? consolidacaoProduto.productName
                  : consolidacaoProduto.name}
              </h3>
            </CardHeader>
            <CardBody>
              <Row>
                <Col lg="6" xl="6" className="campo-consolidado">
                  Valor Aplicado:{" "}
                  {consolidacaoProduto.valueApplied != null
                    ? parseFloat(
                        consolidacaoProduto.valueApplied
                      ).toLocaleString("pt-BR", {
                        minimumFractionDigits: 2,
                        maximumFractionDigits: 2,
                      })
                    : ""}
                </Col>
                <Col lg="6" xl="6" className="campo-consolidado">
                  Saldo:{" "}
                  {consolidacaoProduto.equity != null
                    ? parseFloat(consolidacaoProduto.equity).toLocaleString(
                        "pt-BR",
                        {
                          minimumFractionDigits: 2,
                          maximumFractionDigits: 2,
                        }
                      )
                    : ""}
                </Col>
                <Col lg="6" xl="6" className="campo-consolidado">
                  Rentabilidade:{" "}
                  {consolidacaoProduto.profitability != null
                    ? parseFloat(
                        consolidacaoProduto.profitability
                      ).toLocaleString("pt-BR", {
                        minimumFractionDigits: 2,
                        maximumFractionDigits: 2,
                      }) + " %"
                    : ""}
                </Col>
                <Col lg="6" xl="6" className="campo-consolidado">
                  Primeira Aplicação: {consolidacaoProduto.firstApplicationDate}
                </Col>
                <Col lg="6" xl="6" className="campo-consolidado">
                  Última Atualização: {consolidacaoProduto.lastUpdateDate}
                </Col>
                <Col
                  lg="12"
                  xl="12"
                  style={{ marginTop: "20px" }}
                  className="rentabilidade"
                >
                  {this.mostrarGrafico(graficoConsolidacaoProduto, false)}
                </Col>
              </Row>
            </CardBody>
          </Card>
        </Col>
      );
    });
  };

  getDataLancamentos = () => {
    if (this.state.accessToken === null) {
      this.setRedirect();
      return;
    }

    //var url = global.server_api + 'api/lancamento/familia/' + this.state.familiaId + '/filtro';
    var url =
      global.server_api_new +
      global.apiToken +
      "/lancamento/familia/" +
      this.state.familiaId +
      "/filtro";

    var filtro = {
      CategoriaId:
        this.state.categoryId != null ? this.state.categoryId.id : null,
      MeioPagamentoId:
        this.state.meioPagamentoId != null
          ? this.state.meioPagamentoId.id
          : null,
      OrcamentoId:
        this.state.orcamentoId != null ? this.state.orcamentoId.id : null,
      DataInicial: this.state.startDate,
      DataFinal: this.state.endDate,
      Filtro: this.state.textoDigitado,
      Ordenacao:
        this.state.sortField != null ? this.state.sortField : "Data Desc",
      NumeroPagina:
        this.state.numeroPagina != null ? this.state.numeroPagina : null,
      NumeroItensPorPagina:
        this.state.numeroItensPorPagina != null
          ? this.state.numeroItensPorPagina
          : null,
    };

    var config = {
      headers: {
        Authorization: "bearer " + this.state.accessToken,
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Headers": "Authorization",
        "Access-Control-Allow-Methods":
          "GET, POST, OPTIONS, PUT, PATCH, DELETE",
      },
    };

    axios.post(url, filtro, config).then((res) => {
      var listaDeLancamentos = [];

      if (res.data.results != null) {
        listaDeLancamentos = res.data.results.filter(
          (item) => item.tipoOrcamento === 2
        );
        listaDeLancamentos.filter((lancamento) => {
          let dataLancamento = new Date(lancamento.data);
          let dataFormatada = dataLancamento.toLocaleDateString();
          lancamento.data = dataFormatada;
          return listaDeLancamentos;
        });
      }

      this.setState({ lancamentosInvestimentos: listaDeLancamentos });
    });
  };

  excluirLancamentos = (rowsDeleted, lancamentos) => {
    var ids = [];

    var currentScroll = [
      document.documentElement.scrollLeft || document.body.scrollLeft,
      document.documentElement.scrollTop || document.body.scrollTop,
    ];

    for (var key in rowsDeleted.data) {
      ids.push(lancamentos[rowsDeleted.data[key].dataIndex].id);
    }

    var mensagem =
      "A exclusão é um procedimento irreversível! Tem certeza que deseja excluir estes lançamentos?";

    var buttons = [];

    buttons.push({
      label: "Sim",
      onClick: () => {
        if (ids.length > 0) {
          //var url = global.server_api + 'api/lancamento/bulk/' + ids.join(",");
          var url =
            global.server_api_new +
            global.apiToken +
            "/lancamento/bulk/" +
            ids.join(",");

          var config = {
            headers: {
              Authorization: "bearer " + this.state.accessToken,
              "Access-Control-Allow-Origin": "*",
              "Access-Control-Allow-Headers": "Authorization",
              "Access-Control-Allow-Methods":
                "GET, POST, OPTIONS, PUT, PATCH, DELETE",
            },
          };

          axios.delete(url, {}, config).then((res) => {
            const result = res.data;

            if (result.success) {
              this.getPatrimonios(currentScroll);
              this.getDataLancamentos();
            } else {
              console.log(res.data.exception);
              alert(
                "Erro ao remover lançamentos. " + res.data.exception.Message
              );
            }
          });
        }
      },
    });

    buttons.push({
      label: "Não",
    });

    const options = {
      title: "Exclusão de lançamentos",
      message: mensagem,
      buttons: buttons,
    };

    confirmAlert(options);

    return false;
  };

  atualizarMeiosPagamento = () => {
    var familiaId = this.state.familiaId;

    if (familiaId == null) {
      familiaId = localStorage.getItem("familia-id");
    }

    //var urlMeioPagamento = global.server_api + 'api/meioPagamento/familia/' + familiaId + "/filtro";
    var urlMeioPagamento =
      global.server_api_new +
      global.apiToken +
      "/meioPagamento/familia/" +
      familiaId +
      "/filtro";

    var config = {
      headers: {
        Authorization: "bearer " + this.state.accessToken,
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Headers": "Authorization",
        "Access-Control-Allow-Methods":
          "GET, POST, OPTIONS, PUT, PATCH, DELETE",
      },
    };

    axios.post(urlMeioPagamento, {}, config).then((res) => {
      const meiosPagamento = res.data.results;
      var meiosPagamentoCombo = [];

      if (meiosPagamento != null) {
        meiosPagamento.forEach((mp) => {
          meiosPagamentoCombo.push({
            value: mp,
            label: mp.nome,
          });
        });
      }
      this.setState({ meiosPagamento: meiosPagamentoCombo });
    });
  };

  atualizarCategorias = () => {
    var familiaId = this.state.familiaId;

    if (familiaId == null) {
      familiaId = localStorage.getItem("familia-id");
    }

    //var urlCategoria = global.server_api + 'api/categoria/familia/' + familiaId + "/filtro";
    var urlCategoria =
      global.server_api_new +
      global.apiToken +
      "/categoria/familia/" +
      familiaId +
      "/filtro";

    var config = {
      headers: {
        Authorization: "bearer " + this.state.accessToken,
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Headers": "Authorization",
        "Access-Control-Allow-Methods":
          "GET, POST, OPTIONS, PUT, PATCH, DELETE",
      },
    };

    axios
      .post(urlCategoria, { IncluirOrcamentos: true }, config)
      .then((res) => {
        const categorias = res.data.results;
        this.setState({ categorias });
      });
  };

  editarLancamento = (rowData) => {
    var id = rowData[0];

    var array = this.state.lancamentosInvestimentos.filter(function (item) {
      return item.Id === id || item.id === id;
    });

    if (array !== null && array.length === 1) {
      var itemSelecionado = array[0];

      //Especifico para lancamento (usar o storage local)
      localStorage.setItem("lancamentoEdicao", JSON.stringify(itemSelecionado));

      document.getElementById("novo-lancamento").click();
    } else {
      console.log(
        "Ou o id está no index errado do grid ou não existe o id selecionado no this.state.data"
      );
    }
  };

  gerarCalendario = (id, callback, defaultDate, type) => {
    var currentDate = null;

    if (!defaultDate) {
      currentDate = new Date();
    } else {
      if (type === "filtro-inicio" && this.state.startDate != null) {
        currentDate = this.state.startDate;
      } else if (type === "filtro-fim" && this.state.endDate != null) {
        currentDate = this.state.endDate;
      } else {
        currentDate = defaultDate.toDate();
      }
    }

    return (
      <Datepicker
        locale="br"
        id={id}
        dateFormat="dd/MM/yyyy"
        selected={currentDate}
        showYearDropdown
        dateFormatCalendar="MMMM"
        scrollableYearDropdown
        yearDropdownItemNumber={5}
        onChange={(date) => callback(date)}
        customInput={<MaskedInput mask="11/11/1111" />}
      />
    );
  };

  setStartDate = (date, callback) => {
    if (date != null) {
      date.setHours(0);
      date.setMinutes(0, 0, 0);
      localStorage.setItem("dataInicioPatrimonio", JSON.stringify(date));
    }

    const startDate = date;

    this.setState({ startDate: startDate }, function () {
      if (callback != null) {
        callback();
      } else {
        this.getDataLancamentos();
      }
    });
  };

  setEndDate = (date, callback) => {
    if (date != null) {
      date.setHours(20);
      date.setMinutes(59, 59, 0);
      localStorage.setItem("dataFimPatrimonio", JSON.stringify(date));
    }

    const endDate = date;

    this.setState({ endDate }, function () {
      if (callback != null) {
        callback();
      } else {
        this.getDataLancamentos();
      }
    });
  };

  obterTotal(usuario, tableState) {
    let aplicado = 0.0;
    let atual = 0.0;

    tableState.displayData.forEach((x) => {
      aplicado += global.convertToFloat(x.data[2].props.children);
      atual += global.convertToFloat(x.data[3].props.children);
    });

    if (usuario === "optionsUsuario1") {
      if (this.state.totalAplicado1 !== aplicado) {
        this.setState({ totalAplicado1: aplicado, totalAtual1: atual });
      }
    } else if (usuario === "optionsUsuario2") {
      if (this.state.totalAplicado2 !== aplicado) {
        this.setState({ totalAplicado2: aplicado, totalAtual2: atual });
      }
    }
  }

  render() {
    var dataUsuario1 = this.state.dataInvestimento1;
    var dataUsuario2 = this.state.dataInvestimento2;

    var agrupados1 = [];
    var agrupados2 = [];

    if (this.state.expandedIndexes1 != null) {
      agrupados1 = this.state.expandedIndexes1;
    }

    if (this.state.expandedIndexes2 != null) {
      agrupados2 = this.state.expandedIndexes2;
    }

    const optionsUsuario1 = {
      onTableChange: (action, tableState) => {
        switch (action) {
          case "expandRow":
            var expandedIndexes1 = tableState.expandedRows.data.map(function (
              value
            ) {
              return value.dataIndex;
            });
            this.setState({ expandedIndexes1: expandedIndexes1 });
            break;
          default:
            break;
        }

        this.obterTotal("optionsUsuario1", tableState);
      },
      textLabels: global.textLabels,
      expandableRows: true,
      expandableRowsOnClick: false,
      renderExpandableRow: (rowData, rowMeta) => {
        var tipoInvestimento = rowData[0];

        //Busca patrimônios filhos daquele investimento
        var patrimoniosDoInvestimento = dataUsuario1.filter(function (arrItem) {
          return arrItem.tipoInvestimento === tipoInvestimento;
        });

        if (patrimoniosDoInvestimento == null) {
          patrimoniosDoInvestimento = [];
        }

        var agrupados1Lancamentos = [];

        if (this.state.expandedIndexes1Lancamentos != null) {
          agrupados1Lancamentos = this.state.expandedIndexes1Lancamentos;
        }

        const colSpan = 20;

        const columnsPatrimonio = [
          {
            name: "Controle",
            label: "Controle",
            options: {
              display: true,
              viewColumns: false,
              filter: false,
              sort: false,
              customBodyRender: (value, tableMeta, updateValue) => {
                return (
                  <div>
                    <i
                      style={{ cursor: "pointer", marginLeft: "1.2rem" }}
                      onClick={(e) => {
                        e.stopPropagation();
                        var patrimonioId = tableMeta.rowData[2];
                        this.removerRegistros(patrimonioId);
                      }}
                      className="far fa-trash-alt"
                    ></i>
                  </div>
                );
              },
            },
          },
          {
            name: "tipoInvestimento",
            options: {
              display: false,
              viewColumns: false,
              filter: false,
            },
          },
          {
            name: "id",
            options: {
              display: false,
              viewColumns: false,
              filter: false,
            },
          },
          {
            name: "descricao",
            label: "Descrição",
            options: {
              display: true,
              viewColumns: true,
              filter: false,
              customBodyRender: (value, tableMeta, updateValue) => {
                return (
                  <div
                    style={{ cursor: "pointer" }}
                    onClick={() => {
                      var patrimonioId = tableMeta.rowData[2];
                      this.editarRegistro(patrimonioId, "usuario1");
                    }}
                  >
                    {value}
                  </div>
                );
              },
            },
          },
          {
            name: "valorAplicado",
            label: "Valor Aplicado",
            options: {
              display: true,
              viewColumns: true,
              filter: false,
              customBodyRender: (value, tableMeta, updateValue) => {
                return (
                  <div
                    style={{ cursor: "pointer" }}
                    onClick={() => {
                      var patrimonioId = tableMeta.rowData[2];
                      this.editarRegistro(patrimonioId, "usuario1");
                    }}
                  >
                    {value != null &&
                      !isNaN(value) &&
                      value
                        .toLocaleString("pt-BR", {
                          style: "currency",
                          currency: "BRL",
                        })
                        .replace("R$", "")}
                  </div>
                );
              },
            },
          },
          {
            name: "valorAtual",
            label: "Valor Atual",
            options: {
              display: true,
              viewColumns: true,
              filter: false,
              customBodyRender: (value, tableMeta, updateValue) => {
                return (
                  <div
                    style={{ cursor: "pointer" }}
                    onClick={() => {
                      var patrimonioId = tableMeta.rowData[2];
                      this.editarRegistro(patrimonioId, "usuario1");
                    }}
                  >
                    {value != null &&
                      !isNaN(value) &&
                      value
                        .toLocaleString("pt-BR", {
                          style: "currency",
                          currency: "BRL",
                        })
                        .replace("R$", "")}
                  </div>
                );
              },
            },
          },
        ];

        const optionsPatrimonio = {
          onTableChange: (action, tableState) => {
            switch (action) {
              case "expandRow":
                var expandedIndexes1Lancamentos = tableState.expandedRows.data.map(
                  function (value) {
                    return value.dataIndex;
                  }
                );
                this.setState({
                  expandedIndexes1Lancamentos: expandedIndexes1Lancamentos,
                });
                break;
              default:
                break;
            }
          },
          textLabels: global.textLabels,
          filter: false,
          download: false,
          print: false,
          selectableRows: false,
          filterType: "dropdown",
          responsive: "stacked",
          pagination: false,
          expandableRows: true,
          expandableRowsOnClick: false,
          renderExpandableRow: (rowData, rowMeta) => {
            var patrimonioId = rowData[2];

            var lancamentosDoPatrimonio =
              this.state.lancamentosInvestimentos !== null
                ? this.state.lancamentosInvestimentos.filter(
                    (item) => item.PatrimonioId === patrimonioId
                  )
                : "";

            const columnsLancamentos = [
              {
                name: "id",
                options: {
                  display: false,
                  viewColumns: false,
                  filter: false,
                },
              },
              {
                name: "data",
                label: "Data do lançamento",
                options: {
                  display: true,
                  viewColumns: true,
                  filter: false,
                },
              },
              {
                name: "valor",
                label: "Valor",
                options: {
                  display: true,
                  viewColumns: true,
                  filter: false,
                  customBodyRender: (value, tableMeta) => {
                    var valorReal = value;
                    if (
                      tableMeta.rowData[5] != null &&
                      tableMeta.rowData[5] !== ""
                    ) {
                      valorReal = tableMeta.rowData[5];
                    }

                    return (
                      <span>
                        {valorReal != null
                          ? valorReal
                              .toLocaleString("pt-BR", {
                                style: "currency",
                                currency: "BRL",
                              })
                              .replace("R$", "")
                          : "0,00"}
                      </span>
                    );
                  },
                },
              },
              {
                name: "idInstituicaoFinanceira",
                label: "Instituição financeira",
                options: {
                  display: true,
                  viewColumns: true,
                  filter: true,
                  customBodyRender: (value, tableMeta) => {
                    var instituicoes = this.state.instituicoesFinanceiras
                      ? this.state.instituicoesFinanceiras
                      : null;
                    var instituicao = instituicoes.filter(
                      (item) => item.value === value
                    );

                    if (instituicao && instituicao.length > 0) {
                      return <span>{instituicao[0].label}</span>;
                    } else {
                      return (
                        <span>Instituição financeira não identificada</span>
                      );
                    }
                  },
                },
              },
              {
                name: "operacao",
                label: "Tipo de movimentação",
                options: {
                  display: true,
                  viewColumns: true,
                  filter: true,
                  customBodyRender: (value, tableMeta) => {
                    let label;
                    switch (value) {
                      case 1:
                        label = "Venda";
                        break;
                      case 2:
                        label = "Saldo inicial";
                        break;
                      default:
                        label = "Compra";
                        break;
                    }

                    return <span>{label}</span>;
                  },
                },
              },
            ];

            const optionsLancamentos = {
              textLabels: global.textLabels,
              filter: false,
              download: false,
              print: false,
              selectableRows: true,
              filterType: "dropdown",
              responsive: "stacked",
              pagination: false,
              expandableRows: false,
              expandableRowsOnClick: false,
              onRowClick: (rowData, rowMeta) => {
                this.editarLancamento(rowData);
              },
              onRowsDelete: (rowsDeleted) => {
                this.excluirLancamentos(rowsDeleted, lancamentosDoPatrimonio);
                return false;
              },
            };

            return (
              <TableRow>
                <TableCell colSpan={colSpan}>
                  <div
                    className="table-lancamentos-categoria"
                    style={{ padding: "0 40px" }}
                  >
                    <MUIDataTable
                      data={lancamentosDoPatrimonio}
                      columns={columnsLancamentos}
                      options={optionsLancamentos}
                    />
                  </div>
                </TableCell>
              </TableRow>
            );
          },
          customFooter: (
            count,
            page,
            rowsPerPage,
            changeRowsPerPage,
            changePage,
            textLabels
          ) => {
            return (
              <CustomFooter
                count={count}
                page={page}
                rowsPerPage={rowsPerPage}
                changeRowsPerPage={changeRowsPerPage}
                changePage={changePage}
                textLabels={textLabels}
                hasPagination={false}
              />
            );
          },
        };

        optionsPatrimonio.rowsExpanded = agrupados1Lancamentos;

        return (
          <TableRow>
            <TableCell colSpan={colSpan}>
              <div>
                <MUIDataTable
                  data={patrimoniosDoInvestimento}
                  columns={columnsPatrimonio}
                  options={optionsPatrimonio}
                />
              </div>
            </TableCell>
          </TableRow>
        );
      },
      filter: true,
      filterType: "checkbox",
      responsive: "stacked",
      print: false,
      download: false,
      sort: true,
      selectableRows: "none",
      pagination: false,
      customFooter: (
        count,
        page,
        rowsPerPage,
        changeRowsPerPage,
        changePage,
        textLabels
      ) => {
        var total = [
          `Total aplicado: ${this.state.totalAplicado1.toLocaleString("pt-BR", {
            style: "currency",
            currency: "BRL",
          })}`,
          `Total atual: ${this.state.totalAtual1.toLocaleString("pt-BR", {
            style: "currency",
            currency: "BRL",
          })}`,
        ];

        return (
          <CustomFooter
            count={count}
            page={page}
            rowsPerPage={rowsPerPage}
            changeRowsPerPage={changeRowsPerPage}
            changePage={changePage}
            textLabels={textLabels}
            data2={total}
            hasPagination={false}
          />
        );
      },
    };

    optionsUsuario1.rowsExpanded = agrupados1;

    const optionsUsuario2 = {
      onTableChange: (action, tableState) => {
        switch (action) {
          case "expandRow":
            var expandedIndexes2 = tableState.expandedRows.data.map(function (
              value
            ) {
              return value.dataIndex;
            });
            this.setState({ expandedIndexes2: expandedIndexes2 });
            break;
          default:
            break;
        }

        this.obterTotal("optionsUsuario2", tableState);
      },
      textLabels: global.textLabels,
      expandableRows: true,
      expandableRowsOnClick: false,
      renderExpandableRow: (rowData, rowMeta) => {
        var tipoInvestimento = rowData[0];

        //Busca patrimônios filhos daquele investimento
        var patrimoniosDoInvestimento = dataUsuario2.filter(function (arrItem) {
          return arrItem.tipoInvestimento === tipoInvestimento;
        });

        if (patrimoniosDoInvestimento == null) {
          patrimoniosDoInvestimento = [];
        }

        var agrupados2Lancamentos = [];

        if (this.state.expandedIndexes2Lancamentos != null) {
          agrupados2Lancamentos = this.state.expandedIndexes2Lancamentos;
        }

        const colSpan = 20;

        const columnsPatrimonio = [
          {
            name: "Controle",
            label: "Controle",
            options: {
              display: true,
              viewColumns: false,
              filter: false,
              sort: false,
              customBodyRender: (value, tableMeta, updateValue) => {
                return (
                  <div>
                    <i
                      style={{ cursor: "pointer", marginLeft: "1.2rem" }}
                      onClick={(e) => {
                        e.stopPropagation();
                        var patrimonioId = tableMeta.rowData[2];
                        this.removerRegistros(patrimonioId);
                      }}
                      className="far fa-trash-alt"
                    ></i>
                  </div>
                );
              },
            },
          },
          {
            name: "tipoInvestimento",
            options: {
              display: false,
              viewColumns: false,
              filter: false,
            },
          },
          {
            name: "id",
            options: {
              display: false,
              viewColumns: false,
              filter: false,
            },
          },
          {
            name: "descricao",
            label: "Descrição",
            options: {
              display: true,
              viewColumns: true,
              filter: false,
              customBodyRender: (value, tableMeta, updateValue) => {
                return (
                  <div
                    style={{ cursor: "pointer" }}
                    onClick={() => {
                      var patrimonioId = tableMeta.rowData[2];
                      this.editarRegistro(patrimonioId, "usuario2");
                    }}
                  >
                    {value}
                  </div>
                );
              },
            },
          },
          {
            name: "valorAplicado",
            label: "Valor Aplicado",
            options: {
              display: true,
              viewColumns: true,
              filter: false,
              customBodyRender: (value, tableMeta, updateValue) => {
                return (
                  <div
                    style={{ cursor: "pointer" }}
                    onClick={() => {
                      var patrimonioId = tableMeta.rowData[2];
                      this.editarRegistro(patrimonioId, "usuario2");
                    }}
                  >
                    {value &&
                      value
                        .toLocaleString("pt-BR", {
                          style: "currency",
                          currency: "BRL",
                        })
                        .replace("R$", "")}
                  </div>
                );
              },
            },
          },
          {
            name: "valorAtual",
            label: "Valor Atual",
            options: {
              display: true,
              viewColumns: true,
              filter: false,
              customBodyRender: (value, tableMeta, updateValue) => {
                return (
                  <div
                    style={{ cursor: "pointer" }}
                    onClick={() => {
                      var patrimonioId = tableMeta.rowData[2];
                      this.editarRegistro(patrimonioId, "usuario2");
                    }}
                  >
                    {value &&
                      value
                        .toLocaleString("pt-BR", {
                          style: "currency",
                          currency: "BRL",
                        })
                        .replace("R$", "")}
                  </div>
                );
              },
            },
          },
        ];

        const optionsPatrimonio = {
          onTableChange: (action, tableState) => {
            switch (action) {
              case "expandRow":
                var expandedIndexes2Lancamentos = tableState.expandedRows.data.map(
                  function (value) {
                    return value.dataIndex;
                  }
                );
                this.setState({
                  expandedIndexes2Lancamentos: expandedIndexes2Lancamentos,
                });
                break;
              default:
                break;
            }
          },
          textLabels: global.textLabels,
          filter: false,
          download: false,
          print: false,
          selectableRows: false,
          filterType: "dropdown",
          responsive: "stacked",
          pagination: false,
          expandableRows: true,
          expandableRowsOnClick: false,
          renderExpandableRow: (rowData, rowMeta) => {
            var patrimonioId = rowData[2];

            var lancamentosDoPatrimonio =
              this.state.lancamentosInvestimentos !== null
                ? this.state.lancamentosInvestimentos.filter(
                    (item) => item.PatrimonioId === patrimonioId
                  )
                : "";

            const columnsLancamentos = [
              {
                name: "id",
                options: {
                  display: false,
                  viewColumns: false,
                  filter: false,
                },
              },
              {
                name: "data",
                label: "Data do lançamento",
                options: {
                  display: true,
                  viewColumns: true,
                  filter: false,
                },
              },
              {
                name: "valor",
                label: "Valor",
                options: {
                  display: true,
                  viewColumns: true,
                  filter: false,
                  customBodyRender: (value, tableMeta) => {
                    var valorReal = value;
                    if (
                      tableMeta.rowData[5] != null &&
                      tableMeta.rowData[5] !== ""
                    ) {
                      valorReal = tableMeta.rowData[5];
                    }

                    return (
                      <span>
                        {valorReal != null
                          ? valorReal
                              .toLocaleString("pt-BR", {
                                style: "currency",
                                currency: "BRL",
                              })
                              .replace("R$", "")
                          : "0,00"}
                      </span>
                    );
                  },
                },
              },
              {
                name: "idInstituicaoFinanceira",
                label: "Instituição financeira",
                options: {
                  display: true,
                  viewColumns: true,
                  filter: true,
                  customBodyRender: (value, tableMeta) => {
                    var instituicoes = this.state.instituicoesFinanceiras
                      ? this.state.instituicoesFinanceiras
                      : null;
                    var instituicao = instituicoes.filter(
                      (item) => item.value === value
                    );

                    if (instituicao && instituicao.length > 0) {
                      return <span>{instituicao[0].label}</span>;
                    } else {
                      return (
                        <span>Instituição financeira não identificada</span>
                      );
                    }
                  },
                },
              },
            ];

            const optionsLancamentos = {
              textLabels: global.textLabels,
              filter: false,
              download: false,
              print: false,
              selectableRows: true,
              filterType: "dropdown",
              responsive: "stacked",
              pagination: false,
              expandableRows: false,
              expandableRowsOnClick: false,
              onRowClick: (rowData, rowMeta) => {
                this.editarLancamento(rowData);
              },
              onRowsDelete: (rowsDeleted) => {
                this.excluirLancamentos(rowsDeleted, lancamentosDoPatrimonio);
                return false;
              },
            };

            return (
              <TableRow>
                <TableCell colSpan={colSpan}>
                  <div
                    className="table-lancamentos-categoria"
                    style={{ padding: "0 40px" }}
                  >
                    <MUIDataTable
                      data={lancamentosDoPatrimonio}
                      columns={columnsLancamentos}
                      options={optionsLancamentos}
                    />
                  </div>
                </TableCell>
              </TableRow>
            );
          },
          customFooter: (
            count,
            page,
            rowsPerPage,
            changeRowsPerPage,
            changePage,
            textLabels
          ) => {
            return (
              <CustomFooter
                count={count}
                page={page}
                rowsPerPage={rowsPerPage}
                changeRowsPerPage={changeRowsPerPage}
                changePage={changePage}
                textLabels={textLabels}
                hasPagination={false}
              />
            );
          },
        };

        optionsPatrimonio.rowsExpanded = agrupados2Lancamentos;

        return (
          <TableRow>
            <TableCell colSpan={colSpan}>
              <div>
                <MUIDataTable
                  data={patrimoniosDoInvestimento}
                  columns={columnsPatrimonio}
                  options={optionsPatrimonio}
                />
              </div>
            </TableCell>
          </TableRow>
        );
      },
      filter: true,
      filterType: "dropdown",
      responsive: "stacked",
      print: false,
      download: false,
      sort: true,
      selectableRows: "none",
      pagination: false,
      customFooter: (
        count,
        page,
        rowsPerPage,
        changeRowsPerPage,
        changePage,
        textLabels
      ) => {
        var total = [
          `Total aplicado: ${this.state.totalAplicado2.toLocaleString("pt-BR", {
            style: "currency",
            currency: "BRL",
          })}`,
          `Total atual: ${this.state.totalAtual2.toLocaleString("pt-BR", {
            style: "currency",
            currency: "BRL",
          })}`,
        ];

        return (
          <CustomFooter
            count={count}
            page={page}
            rowsPerPage={rowsPerPage}
            changeRowsPerPage={changeRowsPerPage}
            changePage={changePage}
            textLabels={textLabels}
            data2={total}
            hasPagination={false}
          />
        );
      },
    };

    optionsUsuario2.rowsExpanded = agrupados2;

    const columnsTiposInvestimento = [
      {
        name: "tipoInvestimento",
        options: {
          display: false,
          viewColumns: false,
          filter: false,
        },
      },
      {
        name: "nomeTipoInvestimento",
        label: "Tipo de Investimento",
        options: {
          display: true,
          viewColumns: true,
          filter: true,
          customBodyRender: (value) => {
            return <>{value != null ? value : "-"}</>;
          },
        },
      },
      {
        name: "valorAplicado",
        label: "Valor Aplicado",
        options: {
          display: true,
          viewColumns: true,
          filter: false,
          customBodyRender: (value) => {
            return (
              <>
                {value != null && !isNaN(value)
                  ? value
                      .toLocaleString("pt-BR", {
                        style: "currency",
                        currency: "BRL",
                      })
                      .replace("R$", "")
                  : "-"}
              </>
            );
          },
        },
      },
      {
        name: "valorAtual",
        label: "Valor Atual",
        options: {
          display: true,
          viewColumns: true,
          filter: false,
          customBodyRender: (value) => {
            return (
              <>
                {value != null && !isNaN(value)
                  ? value
                      .toLocaleString("pt-BR", {
                        style: "currency",
                        currency: "BRL",
                      })
                      .replace("R$", "")
                  : "-"}
              </>
            );
          },
        },
      },
      {
        name: "distribuição",
        label: "Distribuição Atual",
        options: {
          display: true,
          viewColumns: true,
          filter: false,
          customBodyRender: (value) => {
            if (Number.isNaN(value)) {
              return <span>0,00 %</span>;
            } else {
              return (
                <span>
                  {value.toLocaleString("pt-BR", {
                    style: "percent",
                    minimumFractionDigits: 2,
                    maximumFractionDigits: 2,
                  })}
                </span>
              );
            }
          },
        },
      },
    ];

    const columnsImobilizado = [
      {
        name: "Controle",
        label: "Controle",
        options: {
          display: true,
          viewColumns: false,
          filter: false,
          sort: false,
          customBodyRender: (value, tableMeta, updateValue) => {
            return (
              <div>
                <i
                  style={{ cursor: "pointer", marginLeft: "1.2rem" }}
                  onClick={(e) => {
                    e.stopPropagation();
                    var patrimonioId = tableMeta.rowData[1];
                    this.removerRegistros(patrimonioId);
                  }}
                  className="far fa-trash-alt"
                ></i>
              </div>
            );
          },
        },
      },
      {
        name: "id",
        options: {
          display: false,
          viewColumns: false,
          filter: false,
        },
      },
      {
        name: "descricao",
        label: "Descrição",
        options: {
          display: true,
          viewColumns: true,
          filter: false,
          customBodyRender: (value, tableMeta, updateValue) => {
            return (
              <div
                style={{ cursor: "pointer" }}
                onClick={() => {
                  var patrimonioId = tableMeta.rowData[1];
                  this.editarRegistro(patrimonioId, "imobilizado");
                }}
              >
                {value}
              </div>
            );
          },
        },
      },
      {
        name: "nomeTipoPatrimonio",
        label: "Tipo de Patrimônio",
        options: {
          display: true,
          viewColumns: true,
          filter: true,
          customBodyRender: (value, tableMeta, updateValue) => {
            return (
              <div
                style={{ cursor: "pointer" }}
                onClick={() => {
                  var patrimonioId = tableMeta.rowData[1];
                  this.editarRegistro(patrimonioId, "imobilizado");
                }}
              >
                {value}
              </div>
            );
          },
        },
      },
      {
        name: "dataPrimeiraCompra",
        label: "Data da Compra",
        options: {
          display: false,
          viewColumns: true,
          filter: false,
          customBodyRender: (value, tableMeta, updateValue) => {
            return (
              <div
                style={{ cursor: "pointer" }}
                onClick={() => {
                  var patrimonioId = tableMeta.rowData[2];
                  this.editarRegistro(patrimonioId, "imobilizado");
                }}
              >
                {this.preparaDataParaGrid(value)}
              </div>
            );
          },
        },
      },
      {
        name: "qtdAtual",
        label: "Quantidade",
        options: {
          display: false,
          viewColumns: true,
          filter: false,
          customBodyRender: (value, tableMeta, updateValue) => {
            return (
              <div
                style={{ cursor: "pointer" }}
                onClick={() => {
                  var patrimonioId = tableMeta.rowData[1];
                  this.editarRegistro(patrimonioId, "imobilizado");
                }}
              >
                {value}
              </div>
            );
          },
        },
      },
      {
        name: "valorAtual",
        label: "Valor Atual",
        options: {
          display: true,
          viewColumns: true,
          filter: false,
          customBodyRender: (value, tableMeta, updateValue) => {
            return (
              <div
                style={{ cursor: "pointer" }}
                onClick={() => {
                  var patrimonioId = tableMeta.rowData[1];
                  this.editarRegistro(patrimonioId, "imobilizado");
                }}
              >
                {value != null && !isNaN(value)
                  ? value
                      .toLocaleString("pt-BR", {
                        style: "currency",
                        currency: "BRL",
                      })
                      .replace("R$", "")
                  : "-"}
              </div>
            );
          },
        },
      },
    ];

    const optionsImobilizado = {
      textLabels: global.textLabels,
      filter: true,
      filterType: "dropdown",
      responsive: "stacked",
      print: false,
      download: false,
      // serverSide: true,
      searchText: this.state.textoDigitado,
      // count: count,
      // page: page,
      sort: true,
      selectableRows: false,
      //rowsPerPage: 20,
      pagination: false,
      onFilterChange: (changedColumn, filterList) => {
        //console.log('onFilterChange');
      },
      onColumnSortChange: (changedColumn) => {
        //ASC
        let order = "";
        let direcaoOrdenacao = "asc";

        if (this.state.colunaOrdenacao === changedColumn) {
          if (
            this.state.direcaoOrdenacao != null &&
            this.state.direcaoOrdenacao === "asc"
          ) {
            order = " Desc";
            direcaoOrdenacao = "desc";
          }
        }

        if (changedColumn === "MeioPagamento") {
          changedColumn = "Meio Pagamento";
        }

        this.setState(
          {
            sortField: changedColumn + order,
            colunaOrdenacao: changedColumn,
            direcaoOrdenacao: direcaoOrdenacao,
            clickedColumn: changedColumn,
          },
          () => {
            //this.getData();
          }
        );
      },
      onTableChange: (action, tableState) => {
        switch (action) {
          case "changePage":
            this.setState({ numeroPagina: tableState.page + 1 }, () => {
              this.getData();
            });
            break;
          case "search":
            this.filtroPorTexto(tableState.searchText);
            break;
          default:
            break;
        }
      },
      customFooter: (
        count,
        page,
        rowsPerPage,
        changeRowsPerPage,
        changePage,
        textLabels
      ) => {
        var total = this.state.totalImobilizado;

        return (
          <CustomFooter
            count={count}
            page={page}
            rowsPerPage={rowsPerPage}
            changeRowsPerPage={changeRowsPerPage}
            changePage={changePage}
            textLabels={textLabels}
            data2={total}
            hasPagination={false}
          />
        );
      },
    };

    columnsImobilizado.forEach((item) => {
      if (item.name === this.state.clickedColumn) {
        item.options.sortDirection = this.state.direcaoOrdenacao;
      }
    });

    const {
      dataImobilizado,
      dataInvestimento1,
      tiposInvestimento1,
      tiposInvestimento2,
      dataInvestimento2,
    } = this.state;

    var nomeUsuario1 =
      this.state.responsaveis != null && this.state.responsaveis.length > 0
        ? "carregando..."
        : "";
    var nomeUsuario2 =
      this.state.responsaveis != null && this.state.responsaveis.length > 0
        ? "carregando..."
        : "";

    if (dataInvestimento1[0]) {
      nomeUsuario1 = dataInvestimento1[0].nomeResponsavel;
    }

    if (dataInvestimento2[0]) {
      nomeUsuario2 = dataInvestimento2[0].nomeResponsavel;
    }

    const TooltipRestore = withStyles((theme) => ({
      tooltip: {
        backgroundColor: "#888888",
        color: "white",
        fontSize: 12,
        fontWeight: "bold",
        fontFamily: "Open Sans",
      },
    }))(Tooltip);

    return (
      <div id="tela-patrimonios" style={{ padding: "0 5%", marginTop: 20 }}>
        {this.renderRedirect()}
        <div id="tela-cadastro-lancamento"></div>

        <div className="home-summary-top">
          <Row>
            <Col lg="12" xl="12" style={{ marginBottom: "15px" }}>
              <div>
                <Card>
                  <CardHeader
                    className="border-0"
                    style={{ padding: "30px 15px 30px 0" }}
                  >
                    <Row className="align-items-center">
                      <Col lg="4" xl="5" className="pt-2">
                        <div className="col pt-2" style={{ paddingLeft: 0 }}>
                          <img
                            src={require("../../assets/img/theme/icone-calendario.png")}
                            alt="Patrimônios"
                            className="before-title-img"
                          />
                          <h2 className="mb-0 chart-title">Patrimônios</h2>
                        </div>
                      </Col>

                      <Col lg="8" xl="7">
                        <Row>
                          <NavItem
                            style={{ float: "right", listStyleType: "none" }}
                            className="pt-2 pl-2"
                          >
                            <NavLink
                              className="nav-link-icon aux-button"
                              to="/admin/destinacao-patrimonio"
                              tag={Link}
                            >
                              <span className="nav-link-inner--text">
                                Destinação de Patrimônio
                              </span>
                            </NavLink>
                          </NavItem>
                          <div className="pt-2 pl-2">
                            <div
                              className="nav-link-icon featured-button"
                              style={{
                                backgroundImage:
                                  "url(" +
                                  require("../../assets/img/theme/botao-destaque.png") +
                                  ")",
                              }}
                              onClick={() => global.showModal(this.modal)}
                            >
                              <span className="nav-link-inner--text">
                                Novo Patrimônio
                              </span>
                            </div>
                          </div>
                        </Row>
                      </Col>
                    </Row>
                  </CardHeader>
                </Card>
              </div>
            </Col>
          </Row>
        </div>
        <Row className="mb-2 pl-2">
          <h3
            className="mb-0 chart-title"
            style={{ marginRight: "10px", minWidth: 220 }}
          >
            Período dos lançamentos
          </h3>
          {this.gerarCalendario(
            "data-inicial",
            this.setStartDate,
            this.moment().startOf("month"),
            "filtro-inicio"
          )}
          {this.gerarCalendario(
            "data-final",
            this.setEndDate,
            this.moment().endOf("month"),
            "filtro-fim"
          )}
          <TooltipRestore
            disableFocusListener
            disableTouchListener
            title="Clique para ver apenas os lançamentos do mês atual"
          >
            <RestoreDate
              style={{ cursor: "pointer" }}
              onClick={() => {
                this.setStartDate(this.moment().startOf("month").toDate());
                this.setEndDate(this.moment().endOf("month").toDate());
              }}
            />
          </TooltipRestore>
          <TooltipRestore
            disableFocusListener
            disableTouchListener
            title="Clique para ver os lançamentos dos últimos três meses"
          >
            <Restore3Months
              style={{ cursor: "pointer" }}
              onClick={() => {
                this.setStartDate(
                  this.moment().subtract(3, "months").startOf("month").toDate()
                );
                this.setEndDate(
                  this.moment().subtract(1, "months").endOf("month").toDate()
                );
              }}
            />
          </TooltipRestore>
        </Row>
        <Row>
          <Col style={{ padding: 0 }}>
            <MuiThemeProvider theme={this.getMuiTheme()}>
              <MUIDataTable
                title={
                  <>
                    <h2 style={{ marginTop: "1rem" }}>
                      Investimentos: {nomeUsuario1}
                    </h2>
                    <a
                      href={
                        this.state.responsaveis[0]
                          ? `#${this.state.responsaveis[0].id}`
                          : ""
                      }
                      style={{ marginBottom: ".8rem" }}
                    >
                      Ver detalhes da carteira
                    </a>
                  </>
                }
                data={tiposInvestimento1}
                columns={columnsTiposInvestimento}
                options={optionsUsuario1}
              />
            </MuiThemeProvider>
          </Col>
        </Row>
        {this.state.responsaveis[1] && (
          <Row>
            <Col style={{ padding: 0 }}>
              <MuiThemeProvider theme={this.getMuiTheme()}>
                <MUIDataTable
                  title={
                    <>
                      <h2 style={{ marginTop: "1rem" }}>
                        Investimentos: {nomeUsuario2}
                      </h2>
                      <a
                        href={
                          this.state.responsaveis[1]
                            ? `#${this.state.responsaveis[1].id}`
                            : ""
                        }
                        style={{ marginBottom: ".8rem" }}
                      >
                        Ver detalhes da carteira
                      </a>
                    </>
                  }
                  data={tiposInvestimento2}
                  columns={columnsTiposInvestimento}
                  options={optionsUsuario2}
                />
              </MuiThemeProvider>
            </Col>
          </Row>
        )}
        {this.state.dataImobilizado.length > 0 && (
          <Row>
            <Col style={{ padding: 0 }}>
              <MuiThemeProvider theme={this.getMuiTheme()}>
                <MUIDataTable
                  title={<h2 style={{ marginTop: "1rem" }}>Imobilizado</h2>}
                  data={dataImobilizado}
                  columns={columnsImobilizado}
                  options={optionsImobilizado}
                />
              </MuiThemeProvider>
            </Col>
          </Row>
        )}

        <div
          class="row mx--2 mt-4"
          style={{
            background: "#2BACC2",
            color: "#FFFFFF",
            borderRadius: "1rem",
          }}
        >
          <div class="col-6 d-flex justify-content-center">
            <TableCell
              className="MuiTableCell-root"
              style={{
                color: "white",
                textAlign: "center",
                fontWeight: "bolder",
              }}
            >
              <font id={"footer-1"}>
                Patrimônio Financeiro Total:{" "}
                {(
                  this.state.totalAtual1 + this.state.totalAtual2
                ).toLocaleString("pt-BR", {
                  style: "currency",
                  currency: "BRL",
                })}
              </font>
            </TableCell>
          </div>
          <div class="col-6 d-flex justify-content-center">
            <TableCell
              className="MuiTableCell-root"
              style={{
                color: "white",
                textAlign: "center",
                fontWeight: "bolder",
              }}
            >
              <font id={"footer-2"}>
                Patrimônio Total:{" "}
                {(
                  this.state.totalAtual1 +
                  this.state.totalAtual2 +
                  this.state.totalImobilizadoValor
                ).toLocaleString("pt-BR", {
                  style: "currency",
                  currency: "BRL",
                })}
              </font>
            </TableCell>
          </div>
        </div>

        {this.mostrarResultadosDasCarterias()}
        {this.mostrarPopup()}

        <Link to="#">
          <Fab
            style={{
              position: "fixed",
              bottom: "1rem",
              right: "1rem",
              background: "#9A3DFF",
              zIndex: 110,
            }}
            aria-label="voltar ao topo"
          >
            <UpIcon style={{ color: "#FFF" }} />
          </Fab>
        </Link>
      </div>
    );
  }
}

export default Patrimonios;
