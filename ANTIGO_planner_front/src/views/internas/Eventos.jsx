import React from "react";
import { Redirect } from "react-router-dom";
import { Row } from "reactstrap";
import ButtonBackPage from "../../components/Utils/ButtonBackPage.jsx";

class Eventos extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      accessToken: localStorage.getItem("access-token"),
      empresaId: localStorage.getItem("empresa-id"),
      planejadorId: localStorage.getItem("planejador-id"),
      familiaId: localStorage.getItem("familia-id"),
      tipoUsuario: parseInt(localStorage.getItem("tipo-usuario")),

      selectedVideo: "cadastro",
    };
  }

  componentDidMount() {
    if (this.state.accessToken == null) {
      this.setRedirect();
      return;
    }
  }

  setRedirect = () => {
    this.setState({
      redirect: true,
    });
  };

  renderRedirect = () => {
    if (this.state.redirect) {
      return <Redirect to="/auth/login" />;
    }
  };

  render() {
    return (
      <div id="eventos" style={{ padding: "0 5%", marginTop: 20 }}>
        <Row>
          <div className="modal_pesquisa">
            <p className=" ">Caro Planejador Vista,</p>
            <p className=" pt-3 text-justify">
              Nosso próximo Encontro está se aproximando! Será no dia 30/11,
              segunda, das 19:00 às 20:30!
            </p>
            <br />
            <p className="  text-justify">
              <strong>Objetivo:</strong> trocar experiências sobre a carreira do
              planejador financeiro pessoal.
            </p>
            <p className="  text-justify">
              <strong>Público:</strong> planejadores independentes cadastrados
              na plataforma Vista.
            </p>
            <p className="  text-justify">
              <strong>Recorrência:</strong> toda última segunda feira de todo
              mês (próxima dia 30/11, coloque na agenda).
            </p>
            <p className="  text-justify">
              <strong>Horário:</strong> 19:00 as 20:30.
            </p>
            <br />
            <br />
            <p>
              <strong>Pauta:</strong>
            </p>
            <p className="text-justify">
              - Discussão entre o grupo sobre algum tema relacionado a carreira
              do planejador financeiro pessoal.
            </p>
            <p className="text-justify">
              - Aplicabilidade dentro da plataforma Vista
            </p>
            <p className="text-justify">
              - Apresentação de novidades e parcerias do Vista
            </p>

            <p className="pt-3 pb-4 text-justify">
              Para o próximo Encontro, discutiremos a importância em pedir
              indicação para os seus clientes. Sabemos que a melhor forma de
              crescer a carteira de forma sustentável é através de pedido de
              indicação dos próprios clientes. Mas, você sabia que para isso
              existem processos? Existem formas de “ensinar” o seu cliente pra
              te vender? Você tem algum passo a passo para pedir indicações?
              Venha compartilhar com a gente a sua experiência.
            </p>
            <div className="form-inline">
              <p className="modal_text pt-3 text-justify">
                Para participar, se inscreva aqui
              </p>

              <div
                className="place-items"
                style={{ marginLeft: "25px", fontSize: "20px" }}
              >
                <a
                  className="featured-button"
                  href="https://www.sympla.com.br/encontro-de-planejadores-vista-30112020__1060136"
                  style={{ backgroundColor: "#feae00", color: "#000" }}
                  target="_blank"
                >
                  QUERO PARTICIPAR
                </a>
              </div>
            </div>

            <p className="modal_text_footer">Aguardamos a sua participação!</p>
            <p className="modal_text_footer">
              Um forte abraço,
              <br />
              Luiz Fernando Schvatzman <br />
              CEO - VISTA
            </p>
            <ButtonBackPage />
          </div>
        </Row>
        {this.renderRedirect()}
      </div>
    );
  }
}

export default Eventos;
