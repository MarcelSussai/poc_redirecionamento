import React from "react";
import { Redirect } from "react-router-dom";
import { CircularProgress, Typography } from "@material-ui/core";
import MUIDataTable from "mui-datatables";
import axios from "axios";
import $ from "jquery";
import SkyLight from "react-skylight";
import { confirmAlert } from "react-confirm-alert";
import MaskedInput from "react-maskedinput";
import { createMuiTheme, MuiThemeProvider } from "@material-ui/core/styles";

import { Col, Row, Input, Card, CardHeader } from "reactstrap";

class Empresas extends React.Component {
  state = {
    page: 0,
    count: 1,
    data: [["Carregando Dados..."]],
    isLoading: false,
    pessoaFisicaAtivada: false,
  };

  mascaras = {
    telefone: "(11) 1111-1111",
    celular: "(11) 1 1111-1111",
    cpf: "111.111.111-11",
    cnpj: "11.111.111/1111-11",
  };

  getMuiTheme = () =>
    createMuiTheme({
      overrides: {
        MuiTableRow: {
          root: {
            "&:nth-of-type(odd)": {
              backgroundColor: "#f8f9fe",
            },
          },
        },
        MUIDataTableSelectCell: {
          fixedHeaderCommon: {
            backgroundColor: "unset",
          },
        },
        MuiTableCell: {
          root: {
            fontFamily: "unset",
            borderTop: "none",
            padding: "16px 10px",
          },
          footer: {
            color: "#FFF",
            fontWeight: 700,
          },
        },
        MUIDataTableHeadCell: {
          fixedHeaderYAxis: {
            fontWeight: 700,
          },
        },
        MuiPaper: {
          root: {
            overflow: "hidden",
            margin: "10px 40px",
          },
        },
        MuiToolbar: {
          regular: {
            backgroundColor: "#e4eaf4",
          },
        },
      },
    });

  componentDidMount() {
    this.setState(
      {
        accessToken: localStorage.getItem("access-token"),
        empresaId: localStorage.getItem("empresa-id"),
        planejadorId: localStorage.getItem("planejador-id"),
        planejadorPrincipal: localStorage.getItem("planejador-principal"),
        tipoUsuario: localStorage.getItem("tipo-usuario"),
      },
      function () {
        //Se é o planejador senior ou o admin
        if (
          parseInt(this.state.tipoUsuario) === 0 ||
          (parseInt(this.state.planejadorPrincipal) === 1 &&
            parseInt(this.state.tipoUsuario) === 1)
        ) {
          this.getData();
        }
        global.mostrarAjudaDaPaginaAutomaticamente(this);
      }
    );
  }

  setRedirect = () => {
    this.setState({
      redirect: true,
    });
  };

  renderRedirect = () => {
    if (this.state.redirect) {
      return <Redirect to="/auth/login" />;
    }
  };

  getData = () => {
    if (this.state.interval != null) {
      clearInterval(this.state.interval);
    }

    if (this.state.accessToken == null) {
      this.setRedirect();
      return;
    }

    this.setState({ isLoading: true });

    //Admins
    //var url = global.server_api + 'api/empresa/filtro/';
    var url = global.server_api_new + global.apiToken + "/empresa/filtro";

    //Planners
    if (this.state.planejadorId != null)
      //url = global.server_api + 'api/empresa/planejador/' + this.state.planejadorId + '/filtro/';
      url =
        global.server_api_new +
        global.apiToken +
        "/empresa/planejador/" +
        this.state.planejadorId +
        "/filtro/";

    var filtro = {
      Filtro: this.state.textoDigitado,
      Ordenacao: this.state.sortField != null ? this.state.sortField : "Nome",
    };

    var config = {
      headers: {
        Authorization: "bearer " + this.state.accessToken,
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Headers": "Authorization",
        "Access-Control-Allow-Methods":
          "GET, POST, OPTIONS, PUT, PATCH, DELETE",
      },
    };

    axios.post(url, filtro, config).then((res) => {
      console.log(res.data.results);
      const total = res.data.totalResults;
      this.setState({ data: res.data.results, isLoading: false, count: total });
    });
  };

  filtroPorTexto = (valorDigitado) => {
    const textoDigitado = valorDigitado;

    this.setState({ textoDigitado }, function () {
      if (this.state.interval != null) {
        clearInterval(this.state.interval);
      }

      const $this = this;

      const interval = setInterval(function () {
        console.log($this.state.textoDigitado);
        $this.getData();
      }, 1000);

      this.setState({ interval });
    });
  };

  editarRegistro = (rowData) => {
    var id = rowData[0];

    if (id == null) return;

    var items = this.state.data.filter(function (temp) {
      return temp.id === id;
    });

    if (items != null && items.length > 0) {
      var item = items[0];

      if (item.tipoEmpresa === 1) {
        this.setState({ pessoaFisicaAtivada: false });
      } else {
        this.setState({ pessoaFisicaAtivada: true });
      }

      //Seta item edicao
      this.setState(
        {
          id: item.id,
          nome: item.nome,
          documento: item.documento,
          telefone: item.telefone,
          cep: item.cep,
          tipoEmpresa: item.tipoEmpresa,
          editando: true,
        },
        function () {
          this.setState(
            {
              endereco: item.endereço,
              complemento: item.complemento,
              pais: item.pais != null ? item.pais : "Brasil",
              cidade: item.cidade,
              estado: global.estados[item.estado],
            },
            function () {
              //Abre modal
              global.showModal(this.modal);
            }
          );
        }
      );
    }
  };

  salvar = () => {
    var currentScroll = [
      document.documentElement.scrollLeft || document.body.scrollLeft,
      document.documentElement.scrollTop || document.body.scrollTop,
    ];

    global.spinnerShow($);

    var item = {
      Id: this.state.id,
      Nome: this.state.nome,
      Documento: this.state.documento,
      Telefone: this.state.telefone,
      Endereco: this.state.endereco,
      Complemento: this.state.complemento,
      Pais: this.state.pais,
      Cidade: this.state.cidade,
      Estado: this.state.estado,
      CEP: this.state.cep,
      tipoEmpresa: this.state.tipoEmpresa,
    };

    //var url = global.server_api + 'api/Empresa/' + this.state.id;
    var url =
      global.server_api_new + global.apiToken + "/Empresa/" + this.state.id;

    var config = {
      headers: {
        Authorization: "bearer " + this.state.accessToken,
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Headers": "Authorization",
        "Access-Control-Allow-Methods":
          "GET, POST, OPTIONS, PUT, PATCH, DELETE",
      },
    };

    axios
      .post(url, item, config)
      .then((res) => {
        global.spinnerHide($, currentScroll);

        if (res.data.success === true) {
          this.limparSelecao();
          window.location.reload();
        } else {
          console.log(res.data.exception);
          const options = {
            title: "Erro ao salvar empresa",
            message: res.data.exception.Message,
            buttons: [
              {
                label: "Ok",
              },
            ],
          };

          confirmAlert(options);
        }
      })
      .catch((error) => {
        global.spinnerHide($, currentScroll);
        global.logarErroDeRequisicao(error);
      });
  };

  limparSelecao = () => {
    this.setState({
      id: null,
      nome: "",
      documento: "",
      telefone: "",
      endereco: "",
      complemento: "",
      cidade: "",
      estado: "",
      cep: "",
      editando: false,
      tipoEmpresa: null,
      pessoaFisicaAtivada: false,
      usuario: null,
    });
  };

  removerRegistros = (rowsDeleted) => {
    const options = {
      title: "Exclusão de Empresa",
      message:
        "Confirma procedimento de exclusão? Este procedimento é irreversível!",
      buttons: [
        {
          label: "Sim",
          onClick: () => {
            //console.log(rowsDeleted.data);
            var ids = [];

            for (var key in rowsDeleted.data) {
              ids.push(this.state.data[rowsDeleted.data[key].dataIndex].id);
            }

            if (ids.length > 0) {
              //var url = global.server_api + 'api/Empresa/bulk/' + ids.join(",");
              var url =
                global.server_api_new +
                global.apiToken +
                "/Empresa/bulk/" +
                ids.join(",");

              var config = {
                headers: {
                  Authorization: "bearer " + this.state.accessToken,
                  "Access-Control-Allow-Origin": "*",
                  "Access-Control-Allow-Headers": "Authorization",
                  "Access-Control-Allow-Methods":
                    "GET, POST, OPTIONS, PUT, PATCH, DELETE",
                },
              };

              axios.delete(url, {}, config).then((res) => {
                const result = res.data;

                if (result.success && result.affectedResults > 0) {
                  this.getData();
                } else {
                  alert("Erro ao remover. " + res.data.exception.Message);
                  console.log(res.data.exception);
                }
              });
            }
          },
        },
        {
          label: "Não",
        },
      ],
    };

    confirmAlert(options);

    return false;
  };

  _onChange = (e) => {
    var nomeCampo = e.target.name;

    if (e.target.value.indexOf("_") > -1) {
      return;
    }

    this.setState({ [nomeCampo]: e.target.value });

    if (nomeCampo === "cep") {
      var cep = e.target.value;
      cep = cep.replace(".", "").replace("-", "");

      var cepUrl = "https://viacep.com.br/ws/" + cep + "/json";

      axios.get(cepUrl).then((res) => {
        if (nomeCampo === "cep") {
          this.atualizarDadosEndereco(res.data);
        }
      });
    }
  };

  atualizarDadosEndereco = (json) => {
    if (json != null) {
      this.setState({
        endereco: json.logradouro,
        cidade: json.localidade,
        estado: json.uf,
      });
    }
  };

  trocarTipoPessoa = () => {
    var pessoaFisicaAtivada = !this.state.pessoaFisicaAtivada;
    var tipoEmpresa = !pessoaFisicaAtivada ? 1 : 0;
    this.setState({
      tipoEmpresa: tipoEmpresa,
      pessoaFisicaAtivada: pessoaFisicaAtivada,
    });
  };

  mostrarPopup = () => (
    <div>
      <SkyLight
        ref={(ref) => (this.modal = ref)}
        transitionDuration={0}
        beforeOpen={this.preCarregarParaEdicao}
        afterClose={this.limparSelecao}
        title={
          <>
            <div className="pop-up-title">
              <i className="ni ni-check-bold"></i> <h2>Edição da Empresa</h2>
            </div>
          </>
        }
      >
        <Row>
          <Col lg="4" xl="4">
            <div>
              <label>Nome *</label>
              <Input
                type="text"
                value={this.state.nome}
                onChange={(e) => this.setState({ nome: e.target.value })}
              />
            </div>
          </Col>
          <Col>
            <div>
              <label for="pessoaFisicaAtivada">Pessoa Física</label>
              <input
                type="checkbox"
                id="pessoaFisicaAtivada"
                checked={this.state.pessoaFisicaAtivada}
                onChange={this.trocarTipoPessoa}
                style={{
                  float: "left",
                  width: "auto",
                  margin: "5px 5px 5px 0px",
                }}
              />{" "}
              Sim
            </div>
          </Col>
          <Col lg="4" xl="4">
            <div>
              {this.state.tipoEmpresa === 0 ? (
                <label>CPF *</label>
              ) : (
                <label>CNPJ *</label>
              )}
              <MaskedInput
                name="documento"
                className="form-control"
                mask={
                  this.state.tipoEmpresa === 0
                    ? this.mascaras.cpf
                    : this.mascaras.cnpj
                }
                value={this.state.documento}
                onChange={this._onChange}
              />
            </div>
          </Col>
          <Col lg="4" xl="4">
            <div>
              <label>Telefone *</label>
              <MaskedInput
                name="telefone"
                className="form-control"
                mask={
                  this.state.tipoEmpresa === 0
                    ? this.mascaras.celular
                    : this.mascaras.telefone
                }
                value={this.state.telefone}
                onChange={this._onChange}
              />
            </div>
          </Col>
          <Col lg="4" xl="4">
            <div>
              <label>CEP *</label>
              <MaskedInput
                name="cep"
                className="form-control"
                value={this.state.cep}
                mask="11.111-111"
                onChange={this._onChange}
              />
            </div>
          </Col>
          <Col lg="4" xl="4">
            <div>
              <label>Estado *</label>
              <Input
                type="text"
                value={this.state.estado}
                onChange={(e) => this.setState({ estado: e.target.value })}
              />
            </div>
          </Col>
          <Col lg="4" xl="4">
            <div>
              <label>Cidade *</label>
              <Input
                type="text"
                value={this.state.cidade}
                onChange={(e) => this.setState({ cidade: e.target.value })}
              />
            </div>
          </Col>
          <Col lg="4" xl="4">
            <div>
              <label>Endereço *</label>
              <Input
                type="text"
                value={this.state.endereco}
                onChange={(e) => this.setState({ endereco: e.target.value })}
              />
            </div>
          </Col>
          <Col lg="4" xl="4">
            <div>
              <label>Complemento</label>
              <Input
                type="text"
                value={this.state.complemento}
                onChange={(e) => this.setState({ complemento: e.target.value })}
              />
            </div>
          </Col>
          <Col lg="12" xl="12">
            <button
              id="salvar-lancamento"
              className="featured-button"
              onClick={() => this.salvar()}
              style={{
                backgroundImage:
                  "url(" +
                  require("../../assets/img/theme/botao-destaque.png") +
                  ")",
              }}
            >
              Salvar
            </button>
          </Col>
        </Row>
      </SkyLight>
    </div>
  );

  render() {
    const columns = [
      {
        name: "id",
        options: {
          display: false,
          viewColumns: false,
          filter: false,
        },
      },
      {
        name: "nome",
        label: "Nome",
        options: {
          display: true,
          viewColumns: true,
          filter: false,
        },
      },
      {
        name: "documento",
        label: "CPF/CNPJ",
        options: {
          display: true,
          viewColumns: true,
          filter: false,
        },
      },
      {
        name: "telefone",
        label: "Telefone",
        options: {
          display: true,
          viewColumns: true,
          filter: false,
        },
      },
      {
        name: "pais",
        label: "Pais",
        options: {
          display: false,
          viewColumns: true,
          filter: false,
        },
      },
      {
        name: "estado",
        label: "Estado",
        options: {
          display: true,
          viewColumns: true,
          filter: false,
          customBodyRender: (value) => {
            if (value != null) {
              return <>{global.estados[value]}</>;
            }
            return <></>;
          },
        },
      },
      {
        name: "cidade",
        label: "Cidade",
        options: {
          display: true,
          viewColumns: true,
          filter: false,
        },
      },
      {
        name: "endereço",
        label: "Endereço",
        options: {
          display: true,
          viewColumns: true,
          filter: false,
        },
      },
      {
        name: "complemento",
        label: "Complemento",
        options: {
          display: false,
          viewColumns: true,
          filter: false,
        },
      },
      {
        name: "cep",
        label: "CEP",
        options: {
          display: false,
          viewColumns: true,
          filter: false,
        },
      },
    ];

    const { data, isLoading } = this.state;

    const options = {
      textLabels: global.textLabels,
      filter: true,
      filterType: "dropdown",
      responsive: "stacked",
      print: false,
      download: false,
      serverSide: true,
      searchText: this.state.textoDigitado,
      // count: count,
      // page: page,
      sort: true,
      selectableRows: "multiple",
      //selectableRowsOnClick: true,
      // rowsPerPage: 15,
      pagination: false,
      onRowsDelete: (rowsDeleted) => {
        this.removerRegistros(rowsDeleted);
        return false;
      },
      onRowClick: (rowData, rowMeta) => {
        this.editarRegistro(rowData);
      },
      onFilterChange: (changedColumn, filterList) => {
        console.log("onFilterChange");
      },
      onColumnSortChange: (changedColumn) => {
        //ASC
        let order = "";
        let direcaoOrdenacao = "asc";

        if (this.state.colunaOrdenacao === changedColumn) {
          if (
            this.state.direcaoOrdenacao != null &&
            this.state.direcaoOrdenacao === "asc"
          ) {
            order = " Desc";
            direcaoOrdenacao = "desc";
          }
        }

        console.log(changedColumn + order);

        this.setState(
          {
            sortField: changedColumn + order,
            colunaOrdenacao: changedColumn,
            direcaoOrdenacao: direcaoOrdenacao,
          },
          () => {
            this.getData();
          }
        );
      },
      onTableChange: (action, tableState) => {
        switch (action) {
          case "changePage":
            this.setState({ numeroPagina: tableState.page + 1 }, () => {
              this.getData();
            });
            break;
          case "search":
            this.filtroPorTexto(tableState.searchText);
            break;
          default:
            break;
        }
      },
    };

    return (
      <div>
        <Card>
          <CardHeader className="border-0">
            <Row className="align-items-center">
              <div className="col">
                <img
                  src={require("../../assets/img/theme/icone-calendario.png")}
                  alt="Meios de Pagamento"
                  className="before-title-img"
                />
                <h3 className="mb-0 chart-title">Empresas</h3>
              </div>
            </Row>
          </CardHeader>
        </Card>
        <MuiThemeProvider theme={this.getMuiTheme()}>
          <MUIDataTable
            id="orcamento-table"
            title={
              <Typography>
                {isLoading && (
                  <CircularProgress
                    size={24}
                    style={{ marginLeft: 15, position: "relative", top: 4 }}
                  />
                )}
              </Typography>
            }
            data={data}
            columns={columns}
            options={options}
          />
        </MuiThemeProvider>
        {this.mostrarPopup()}
        {this.renderRedirect()}
      </div>
    );
  }
}

export default Empresas;
