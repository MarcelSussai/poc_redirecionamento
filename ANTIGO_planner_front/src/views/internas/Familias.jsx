import React from "react";
import { Redirect } from "react-router-dom";
import { CircularProgress, Typography } from "@material-ui/core";
import MUIDataTable from "mui-datatables";
import TableRow from "@material-ui/core/TableRow";
import TableCell from "@material-ui/core/TableCell";
import axios from "axios";
import $ from "jquery";
import SkyLight from "react-skylight";
import { confirmAlert } from "react-confirm-alert";
import MaskedInput from "react-maskedinput";
import imageCompression from "browser-image-compression";
import CurrencyInput from "react-currency-input";
import Datepicker, { registerLocale } from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import Select from "react-select";
import { createMuiTheme, MuiThemeProvider } from "@material-ui/core/styles";
import Button from '../../components/Buttons'
import GoBackButton from '../../components/Buttons/GoBackButton'

import { Col, Row, Input, Card, CardHeader, NavbarBrand } from "reactstrap";

import br from "date-fns/locale/pt-BR";

registerLocale("br", br);

class Familias extends React.Component {
  moment = require("moment");

  state = {
    page: 0,
    count: 1,
    numeroItensPorPagina: 15,
    data: [["Carregando Dados..."]],
    isLoading: false,
    pais: "Brasil",
    recorrencias: [],
    editando: false,
  };

  mascaras = {
    telefone: "(11) 1111-1111",
    celular: "(11) 1 1111-1111",
    cpf: "111.111.111-11",
    cnpj: "11.111.111/1111-11",
    data: "11/11/1111",
  };

  imagem = require("../../assets/img/theme/familia.png");

  imagens = {};
  buscandoImagens = {};

  membersPerId = {};

  componentDidMount() {
    const { open } = this.props.match.params;

    var recorrencias = [{ value: null, label: "Selecione..." }];

    global.tipoRecorrencia.forEach(function (value, index) {
      recorrencias.push({
        value: index,
        label: value,
      });
    });

    var statuses = [{ value: null, label: "Selecione..." }];

    global.statusFamilia.forEach(function (value, index) {
      statuses.push({
        value: index,
        label: value,
      });
    });

    this.setState(
      {
        accessToken: localStorage.getItem("access-token"),
        empresaId: localStorage.getItem("empresa-id"),
        planejadorId: localStorage.getItem("planejador-id"),
        tipoUsuario: localStorage.getItem("tipo-usuario"),
        recorrencias: recorrencias,
        statuses: statuses,
      },
      function () {
        this.setState({
          labelFamilia:
            parseInt(this.state.tipoUsuario) === 2 ? "família" : "cliente",
          labelFamiliaMaiusculo:
            parseInt(this.state.tipoUsuario) === 2 ? "Família" : "Cliente",
        });

        this.getData(open);

        global.mostrarAjudaDaPaginaAutomaticamente(this);
      }
    );

    var $this = this;

    setTimeout(function () {
      $(document).on("change", "#imagem", function (event) {
        const imageFile = event.target.files[0];

        var options = {
          maxSizeMB: 0.6,
          maxWidthOrHeight: 1024,
          useWebWorker: true,
        };
        try {
          imageCompression(imageFile, options)
            .then(function (compressedFile) {
              imageCompression
                .getDataUrlFromFile(compressedFile)
                .then(function (base64) {
                  $this.imagem = base64;
                  $this.mostrarThumb();
                });
            })
            .catch(function (error) {
              console.log(error.message);
            });
        } catch (error) {
          console.log(error);
        }
      });
    }, 1000);
  }

  getMuiTheme = () =>
    createMuiTheme({
      overrides: {
        MuiTableRow: {
          root: {
            "&:nth-of-type(odd)": {
              backgroundColor: "#f8f9fe",
            },
          },
        },
        MUIDataTableSelectCell: {
          fixedHeaderCommon: {
            backgroundColor: "unset",
          },
        },
        MuiTableCell: {
          root: {
            fontFamily: "unset",
            borderTop: "none",
            padding: "16px 10px",
          },
          footer: {
            color: "#FFF",
            fontWeight: 700,
          },
        },
        MUIDataTableHeadCell: {
          fixedHeaderYAxis: {
            fontWeight: 700,
          },
        },
        MuiPaper: {
          root: {
            overflow: "hidden",
          },
        },
        MuiToolbar: {
          regular: {
            backgroundColor: "#e4eaf4",
          },
        },
        MUIDataTablePagination: {
          root: {
            "&:last-child": {
              margin: "0px 24px 0px 24px",
              padding: 0,
            },
          },
        },
      },
    });

  mostrarThumb = () => {
    var novaLinha =
      '<img src="' +
      this.imagem +
      '" onclick="document.getElementById(\'imagem\').click()" style="margin-top: 8px; width: 100%; height: auto">';
    $("#linha").html(novaLinha);
  };

  setRedirect = () => {
    this.setState({
      redirect: true,
    });
  };

  renderRedirect = () => {
    if (this.state.redirect) {
      return <Redirect to="/auth/login" />;
    }
  };

  getData = (open) => {
    if (this.state.interval != null) {
      clearInterval(this.state.interval);
    }

    if (this.state.accessToken == null) {
      this.setRedirect();
      return;
    }

    this.setState({ isLoading: true });

    //Admins
    //var url = global.server_api + 'api/familia/filtro/';
    var url = global.server_api_new + global.apiToken + "/familia/filtro/";

    //Planners
    if (this.state.planejadorId != null)
      //url = global.server_api + 'api/familia/planejador/filtro/' + this.state.planejadorId;
      url =
        global.server_api_new +
        global.apiToken +
        "/familia/planejador/filtro/" +
        this.state.planejadorId;

    var filtro = {
      Filtro: this.state.textoDigitado,
      Ordenacao: this.state.sortField != null ? this.state.sortField : "Nome",
      NumeroPagina:
        this.state.numeroPagina != null ? this.state.numeroPagina : 1,
      NumeroItensPorPagina:
        this.state.numeroItensPorPagina != null
          ? this.state.numeroItensPorPagina
          : 10,
    };

    var config = {
      headers: {
        Authorization: "bearer " + this.state.accessToken,
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Headers": "Authorization",
        "Access-Control-Allow-Methods":
          "GET, POST, OPTIONS, PUT, PATCH, DELETE",
      },
    };

    var currentScroll = [
      document.documentElement.scrollLeft || document.body.scrollLeft,
      document.documentElement.scrollTop || document.body.scrollTop,
    ];

    global.spinnerShow($);

    axios.post(url, filtro, config).then((res) => {
      var data = [];

      if (res.data.results != null) {
        res.data.results.forEach(function (item) {
          var temp = item;
          temp.familiaId = item.id;
          temp.estado = global.estados[item.estado];

          data.push(temp);
        });
      }

      const total = res.data.totalResults;
      this.setState(
        { data: data, isLoading: false, count: total },
        function () {
          global.spinnerHide($, currentScroll);
          if (open != null) {
            global.showModal(this.modal);
          }
        }
      );
    });
  };

  buscaImagemFamilia = (familiaId, htmlSelector) => {
    if (this.imagens != null && this.imagens[familiaId] != null) {
      $(htmlSelector).attr("src", this.imagens[familiaId]);
      return;
    }

    if (this.buscandoImagens != null && this.buscandoImagens[familiaId]) {
      return;
    }

    this.buscandoImagens[familiaId] = true;

    //var url = global.server_api + 'api/familia/imagem/' + familiaId;
    var url =
      global.server_api_new + global.apiToken + "/familia/imagem/" + familiaId;

    var config = {
      headers: {
        Authorization: "bearer " + this.state.accessToken,
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Headers": "Authorization",
        "Access-Control-Allow-Methods":
          "GET, POST, OPTIONS, PUT, PATCH, DELETE",
      },
    };

    axios.get(url, config).then((res) => {
      var imagem = res.data.singleResult;

      if (imagem == null || imagem === "") {
        imagem = require("../../assets/img/theme/familia.png");
      }

      this.imagens[familiaId] = imagem;

      this.buscandoImagens[familiaId] = false;

      try {
        $(htmlSelector).attr("src", imagem);
      } catch (ex) {
        //console.log(ex);
      }
    });
  };

  filtroPorTexto = (valorDigitado) => {
    const textoDigitado = valorDigitado;

    this.setState({ textoDigitado }, function () {
      if (this.state.interval != null) {
        clearInterval(this.state.interval);
      }

      const $this = this;

      const interval = setInterval(function () {
        $this.getData();
      }, 1000);

      this.setState({ interval });
    });
  };

  getQuantidadeCarteirasDropDown = () => {
    var quantidadesDeCarteiras = [];

    Object.keys(global.planosCarteiras2).forEach(function (key) {
      key = parseInt(key);

      quantidadesDeCarteiras.push({
        value: key,
        label: global.planosCarteiras2[key],
      });
    });

    return quantidadesDeCarteiras;
  };

  montarDropDown = (itens, id, changeMethod, selectedValue) => (
    <Select
      id={id}
      onChange={changeMethod}
      className="select-component"
      options={itens}
      defaultValue={itens != null ? itens[0] : ""}
      placeholder="Selecione..."
      value={
        itens != null && selectedValue != null
          ? itens.find((op) => {
              return op.value === selectedValue;
            })
          : null
      }
    />
  );

  editarRegistro = (rowData) => {
    var id = rowData[0];

    console.log("editarRegistro", id);

    if (id == null) return;

    var items = this.state.data.filter(function (temp) {
      return temp.familiaId === id;
    });

    if (items != null && items.length > 0) {
      var item = items[0];

      this.imagem = this.imagens[id];

      console.log("editarRegistro", item);

      //Seta item edicao
      this.setState(
        {
          familiaId: item.familiaId,
          nome: item.nome,
          cep: item.cep,
          valorPago: item.valorPago,
          inicioContrato: item.inicioContrato,
          fimContrato: item.fimContrato,
          recorrencia: item.recorrencia,
          statusFamilia: item.statusFamilia,
          moeda: item.moeda,
          //fotoBase64: item.fotoBase64, //Nao editamos fotos
          quantidadeCarteiras: item.quantidadeCarteiras,
          editando: true,
        },
        function () {
          this.setState(
            {
              endereco: item.endereco,
              complemento: item.complemento,
              pais: item.pais != null ? item.pais : "Brasil",
              cidade: item.cidade,
              estado: item.estado,
            },
            function () {
              //Abre modal
              global.showModal(this.modal);
            }
          );
        }
      );
    }
  };

  salvar = () => {
    var currentScroll = [
      document.documentElement.scrollLeft || document.body.scrollLeft,
      document.documentElement.scrollTop || document.body.scrollTop,
    ];

    global.spinnerShow($);

    var item = {
      id: this.state.familiaId,
      nomeUsuario: this.state.nomeUsuario,
      sobrenome: this.state.sobrenome,
      cpf: this.state.cpf,
      celular: this.state.celular,
      email: this.state.email,
      enviarLogin: this.state.enviarLogin,
      nome: this.state.nome,
      endereco: this.state.endereco,
      complemento: this.state.complemento,
      pais: this.state.pais,
      cidade: this.state.cidade,
      estado: this.state.estado,
      cep: this.state.cep,
      fotoBase64: this.imagem,
      empresaId: this.state.empresaId,
      planejadorId: this.state.planejadorId,
      valorPago: this.state.valorPago,
      inicioContrato: this.state.inicioContrato,
      fimContrato: this.state.fimContrato,
      recorrencia: this.state.recorrencia,
      statusFamilia: this.state.statusFamilia,
      moeda: this.state.moeda,
      quantidadeCarteiras: this.state.quantidadeCarteiras,
    };

    //var url = global.server_api + 'api/familia/' + ((this.state.familiaId != null) ? this.state.familiaId : "");
    var url =
      global.server_api_new +
      global.apiToken +
      "/familia/" +
      (this.state.familiaId != null ? this.state.familiaId : "");

    var config = {
      headers: {
        Authorization: "bearer " + this.state.accessToken,
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Headers": "Authorization",
        "Access-Control-Allow-Methods":
          "GET, POST, OPTIONS, PUT, PATCH, DELETE",
      },
    };

    axios
      .post(url, item, config)
      .then((res) => {
        global.spinnerHide($, currentScroll);

        if (res.data.success === true) {
          var editando = this.state.editando;
          this.limparSelecao();
          this.modal.hide();
          this.getData();

          if (!editando) {
            $("#atualizar-familias-sidebar").click();
            global.showModal(this.modalCadastrarPessoas);
          }
        } else {
          const options = {
            title: "Erro ao salvar " + this.state.labelFamilia,
            message: res.data.exception.Message,
            buttons: [
              {
                label: "Ok",
              },
            ],
          };

          confirmAlert(options);
        }
      })
      .catch((error) => {
        global.spinnerHide($, currentScroll);
        global.logarErroDeRequisicao(error);
      });
  };

  limparSelecao = () => {
    this.imagens[this.state.familiaId] = null;

    this.setState({
      editando: false,
      familiaId: null,
      nomeUsuario: "",
      sobrenome: "",
      cpf: "",
      celular: "",
      email: "",
      enviarLogin: false,
      nome: "",
      endereco: "",
      complemento: "",
      cidade: "",
      estado: "",
      cep: "",
      fotoBase64: null,
      valorPago: "",
      valorPagoFormatado: "",
      inicioContrato: null,
      fimContrato: null,
      recorrencia: null,
      statusFamilia: null,
      moeda: "",
      quantidadeCarteiras: null,
    });

    this.imagem = require("../../assets/img/theme/familia.png");
  };

  removerRegistros = (rowsDeleted) => {
    const options = {
      title: "Exclusão de " + this.state.labelFamiliaMaiusculo,
      message:
        "Tem certeza que deseja excluir? Este procedimento é irreversível!",
      buttons: [
        {
          label: "Sim",
          onClick: () => {
            var ids = [];

            for (var key in rowsDeleted.data) {
              ids.push(
                this.state.data[rowsDeleted.data[key].dataIndex].familiaId
              );
            }

            if (ids.length > 0) {
              //var url = global.server_api + 'api/familia/bulk/' + ids.join(",");
              var url =
                global.server_api_new +
                global.apiToken +
                "/familia/bulk/" +
                ids.join(",");

              var config = {
                headers: {
                  Authorization: "bearer " + this.state.accessToken,
                  "Access-Control-Allow-Origin": "*",
                  "Access-Control-Allow-Headers": "Authorization",
                  "Access-Control-Allow-Methods":
                    "GET, POST, OPTIONS, PUT, PATCH, DELETE",
                },
              };

              axios.delete(url, {}, config).then((res) => {
                const result = res.data;

                if (result.success && result.affectedResults > 0) {
                  $("#atualizar-familias-sidebar").click();
                  //this.getData();
                  window.location.reload();
                } else {
                  alert(
                    "Erro ao executar remoção. " + res.data.exception.Message
                  );
                }
              });
            }
          },
        },
        {
          label: "Não",
        },
      ],
    };

    confirmAlert(options);

    return false;
  };

  _onChange = (e) => {
    var nomeCampo = e.target.name;

    if (e.target.value.indexOf("_") > -1) {
      return;
    }

    this.setState({ [nomeCampo]: e.target.value });

    if (nomeCampo === "cep") {
      var cep = e.target.value;
      cep = cep.replace(".", "").replace("-", "");

      var cepUrl = "https://viacep.com.br/ws/" + cep + "/json";

      axios.get(cepUrl).then((res) => {
        if (nomeCampo === "cep") {
          this.atualizarDadosEndereco(res.data);
        }
      });
    }
  };

  atualizarDadosEndereco = (json) => {
    if (json != null) {
      this.setState({
        endereco: json.logradouro,
        cidade: json.localidade,
        estado: json.uf,
      });
    }
  };

  mudarRecorrencia = (selectedOption) => {
    const recorrencia = selectedOption.value;
    this.setState({ recorrencia });
  };

  mudarStatus = (selectedOption) => {
    const statusFamilia = selectedOption.value;
    this.setState({ statusFamilia });
  };

  mudarQuantidadeCarteiras = (selectedOption) => {
    const quantidadeCarteiras = selectedOption.value;
    this.setState({ quantidadeCarteiras });
  };

  gerarDatePicker = (id, fieldKey, defaultDate) => {
    var currentDate = null;

    var item = this.state;

    var value = item[fieldKey];

    //date type
    if (value != null) {
      //string vinda da edicao
      if (typeof value === "string") {
        var temp = this.moment(value, "YYYY-MM-DDThh:mm:ss");
        currentDate = temp.toDate();
        // this.setItemOnState(fieldKey, temp);
      }
      //data vinda da selecao atraves do calendario
      else {
        currentDate = value;
      }
    }
    //momentjs date
    else if (defaultDate != null) {
      currentDate = defaultDate.toDate();
      this.setState({ [fieldKey]: currentDate });
    }
    //now (date)
    else {
      currentDate = new Date();
      this.setState({ [fieldKey]: currentDate });
    }

    return (
      <Datepicker
        locale="br"
        id={id}
        dateFormat="dd/MM/yyyy"
        selected={currentDate}
        showYearDropdown
        dateFormatCalendar="MMMM"
        scrollableYearDropdown
        yearDropdownItemNumber={5}
        onChange={(date) => this.setState({ [fieldKey]: date })}
      />
    );
  };

  asyncCall(index, expandedRows) {
    var id = this.state.data[index].familiaId;
    if (this.membersPerId[id] == null) {
      //var url = global.server_api + 'api/Pessoa/familia/' + id + "/filtro/";
      var url =
        global.server_api_new +
        global.apiToken +
        "/Pessoa/familia/" +
        id +
        "/filtro/";
      var config = {
        headers: {
          Authorization: "bearer " + this.state.accessToken,
          "Access-Control-Allow-Origin": "*",
          "Access-Control-Allow-Headers": "Authorization",
          "Access-Control-Allow-Methods":
            "GET, POST, OPTIONS, PUT, PATCH, DELETE",
        },
      };
      axios.post(url, {}, config).then((res) => {
        this.membersPerId[id] = res;
        this.setState({
          expandable: [index],
        });
      });
    }
  }

  mudarFamilia = (familiaId) => {
    if (familiaId != null) localStorage.setItem("familia-id", familiaId);

    setTimeout(function () {
      window.location.href = "/admin/index";
    }, 250);
  };

  preparaDataParaGrid = (dataString) => {
    var data = this.moment(dataString, "YYYY-MM-DDThh:mm:ss");
    return data.format("DD/MM/YYYY");
  };

  redirecionarUsuarios = () => {
    if (this.state.redirectUsers) {
      return <Redirect to="/admin/usuarios/1" />;
    }
  };

  mostrarPopupCadastrarPessoas = () => (
    <div>
      <SkyLight
        ref={(ref) => (this.modalCadastrarPessoas = ref)}
        transitionDuration={0}
        title={
          <>
            <div className="pop-up-title">
              <i className="ni ni-check-bold"></i>
              <h2>Cliente Cadastrado com Sucesso!</h2>
            </div>
          </>
        }
      >
        <Row>
          <Col lg="12" xl="12">
            Agora você precisa adicionar os usuários deste cliente e configurar
            o orçamento inicial.
            <br />
            <br />
            Você pode fazer isso selecionando o cliente no menu lateral esquerdo
            e realizando a Configuração Inicial.
          </Col>
          <Col lg="12" xl="12">
            <button
              onClick={() => this.modalCadastrarPessoas.hide()}
              className="aux-button"
              style={{ float: "right", marginRight: "10px" }}
            >
              Ok
            </button>
          </Col>
        </Row>
      </SkyLight>
    </div>
  );

  mostrarPopup = () => (
    <div className="nova-familia-popup">
      <SkyLight
        ref={(ref) => (this.modal = ref)}
        transitionDuration={0}
        beforeOpen={this.preCarregarParaEdicao}
        afterClose={this.limparSelecao}
        title={
          <>
            <div className="pop-up-title">
              <i className="ni ni-check-bold"></i>{" "}
              <h2>Edição de {this.state.labelFamiliaMaiusculo}</h2>
            </div>
          </>
        }
      >
        <Row>
          <Col lg="4" xl="4">
            <div>
              <label>Nome da família *</label>
              <Input
                type="text"
                value={this.state.nome}
                onChange={(e) => this.setState({ nome: e.target.value })}
              />
            </div>
          </Col>
          <Col lg="4" xl="4">
            <div>
              <label>CEP *</label>
              <MaskedInput
                name="cep"
                className="form-control"
                value={this.state.cep}
                mask="11.111-111"
                onChange={this._onChange}
              />
            </div>
          </Col>
          <Col lg="4" xl="4">
            <div>
              <label>Estado *</label>
              <Input
                type="text"
                value={this.state.estado}
                onChange={(e) => this.setState({ estado: e.target.value })}
              />
            </div>
          </Col>
          <Col lg="4" xl="4">
            <div>
              <label>Cidade *</label>
              <Input
                type="text"
                value={this.state.cidade}
                onChange={(e) => this.setState({ cidade: e.target.value })}
              />
            </div>
          </Col>
          <Col lg="4" xl="4">
            <div>
              <label>Endereço *</label>
              <Input
                type="text"
                value={this.state.endereco}
                onChange={(e) => this.setState({ endereco: e.target.value })}
              />
            </div>
          </Col>
          <Col lg="4" xl="4">
            <div>
              <label>Complemento</label>
              <Input
                type="text"
                value={this.state.complemento}
                onChange={(e) => this.setState({ complemento: e.target.value })}
              />
            </div>
          </Col>
          <Col lg="4" xl="4">
            <div>
              <label>Início do Contrato</label>
              {this.gerarDatePicker(
                "inicio-contrato",
                "inicioContrato",
                this.moment()
              )}
            </div>
          </Col>
          <Col lg="4" xl="4">
            <div>
              <label>Fim do Contrato</label>
              {this.gerarDatePicker(
                "fim-contrato",
                "fimContrato",
                this.moment().add(1, "years")
              )}
            </div>
          </Col>
          <Col lg="4" xl="4">
            <div>
              <label>Status do Contrato</label>
              {this.montarDropDown(
                this.state.statuses,
                "status-familia",
                this.mudarStatus,
                this.state.statusFamilia
              )}
            </div>
          </Col>
          <Col lg="4" xl="4">
            <div>
              <label>Recorrência</label>
              {this.montarDropDown(
                this.state.recorrencias,
                "recorrencia",
                this.mudarRecorrencia,
                this.state.recorrencia
              )}
            </div>
          </Col>
          <Col lg="4" xl="4">
            <div>
              <label>Valor</label>
              <CurrencyInput
                value={
                  this.state.valorPagoFormatado != null
                    ? this.state.valorPagoFormatado
                    : this.state.valorPago
                }
                thousandSeparator={"."}
                decimalSeparator={","}
                precision="2" //prefix={'R$ '}
                onChangeEvent={(event, formattedValue, value) => {
                  this.setState({ valorPago: value });
                  this.setState({ valorPagoFormatado: formattedValue });
                }}
              />
            </div>
          </Col>
          {/* <Col lg="4" xl="4">
                    <div>
                        <label>Moeda Principal (Símbolo)</label>
                        <Input type="text" value={this.state.moeda} onChange={e => this.setState({ moeda: e.target.value })}/>
                    </div>
                </Col> */}
          <Col lg="4" xl="4">
            <div>
              <label>Quantidade de Carteiras</label>
              {this.montarDropDown(
                this.getQuantidadeCarteirasDropDown(),
                "quantidade-carteiras",
                this.mudarQuantidadeCarteiras,
                this.state.quantidadeCarteiras
              )}
            </div>
          </Col>

          {/* <hr className="w-100 mx-3" />
          <Row style={{ padding: "15px" }}>
            <Col lg="4" xl="4">
              <div>
                <label>Nome *</label>
                <Input
                  type="text"
                  value={this.state.nomeUsuario}
                  onChange={(e) =>
                    this.setState({ nomeUsuario: e.target.value })
                  }
                  style={{ paddingLeft: 24 }}
                />
              </div>
            </Col>
            <Col lg="4" xl="4">
              <div>
                <label>Sobrenome *</label>
                <Input
                  type="text"
                  value={this.state.sobrenome}
                  onChange={(e) => this.setState({ sobrenome: e.target.value })}
                  style={{ paddingLeft: 24 }}
                />
              </div>
            </Col>
            <Col lg="4" xl="4">
              <div>
                <label>CPF *</label>
                <MaskedInput
                  name="documento"
                  className="form-control"
                  mask={this.mascaras.cpf}
                  value={this.state.cpf}
                  onChange={(e) => this.setState({ cpf: e.target.value })}
                  style={{ paddingLeft: 24 }}
                />
              </div>
            </Col>
            <Col lg="4" xl="4">
              <div>
                <label>Email *</label>
                <Input
                  type="text"
                  value={this.state.email}
                  onChange={(e) => this.setState({ email: e.target.value })}
                  style={{ paddingLeft: 24 }}
                />
              </div>
            </Col>
            <Col lg="4" xl="4">
              <div>
                <label>Celular *</label>
                <MaskedInput
                  name="celular"
                  className="form-control"
                  mask={this.mascaras.celular}
                  value={this.state.celular}
                  onChange={(e) => this.setState({ celular: e.target.value })}
                  style={{ paddingLeft: 24 }}
                />
              </div>
            </Col>
            <Col
              lg="4"
              xl="4"
              style={{ display: "flex", alignItems: "center" }}
            >
              <div>
                <label></label>
                <div
                  style={{
                    display: "flex",
                    justifyContent: "center",
                    alignContent: "center",
                  }}
                >
                  <span>
                    Deseja enviar o login e senha para o seu cliente agora?
                  </span>
                  &nbsp;
                  <Switch
                    onChange={(value) => this.setState({ enviarLogin: value })}
                    checked={this.state.enviarLogin}
                  />
                </div>
              </div>
            </Col>
          </Row> */}

          <Row style={{ padding: "15px" }}>
            {/* <Col lg="8" xl="8" style={this.state.editando ? {display:"none"} : {}}> */}
            <Col lg="8" xl="8" style={{ position: "fixed", left: "-4000px" }}>
              <div>
                <label>Foto</label>
                <input
                  type="file"
                  id="imagem"
                  onChange={(e) => (this.imagem = e.target.value)}
                ></input>
              </div>
            </Col>
            {/* <Col lg="4" xl="4" style={this.state.editando ? {display:"none"} : {}}> */}
            <Col lg="4" xl="4">
              <div id="linha">
                {this.state.editando != null &&
                  this.imagem != null &&
                  this.mostrarThumb()}
              </div>
            </Col>
            <Col lg="12" xl="12">
              <i style={{ fontSize: ".9rem" }}>
                Obs.: As informações referentes à valor, recorrência, prazo e
                contrato são apenas informativas (para facilitar a sua gestão) e
                não geram nenhuma cobrança automática.
              </i>
            </Col>
            <Col lg="12" xl="12">
              <button
                id="salvar-lancamento"
                className="featured-button"
                onClick={() => this.salvar()}
                style={{
                  backgroundImage:
                    "url(" +
                    require("../../assets/img/theme/botao-destaque.png") +
                    ")",
                }}
              >
                Salvar
              </button>
            </Col>
          </Row>
        </Row>
      </SkyLight>
    </div>
  );

  render() {
    if (localStorage.getItem("tipo-usuario") === '2') {
      this.props.history.push('/admin/index');
    }
    var columns = [
      {
        name: "familiaId",
        options: {
          display: false,
          viewColumns: false,
          filter: false,
        },
      },
      {
        name: "fotoBase64",
        label: "Foto",
        options: {
          display: true,
          viewColumns: false,
          filter: false,
          customBodyRender: (value, tableMeta) => {
            if (value == null) {
              this.buscaImagemFamilia(
                tableMeta.rowData[0],
                "#foto_familia_" + tableMeta.rowData[0]
              );
              value = require("../../assets/img/theme/familia.png");
            }

            return (
              <NavbarBrand className="pt-0">
                <img
                  id={"foto_familia_" + tableMeta.rowData[0]}
                  className="navbar-brand-img"
                  src={value}
                  style={{
                    marginTop: "8px",
                    maxHeight: "100px",
                    maxWidth: "100px",
                    cursor: "pointer",
                  }}
                  alt="Foto"
                  onClick={() => {
                    this.editarRegistro(tableMeta.rowData);
                  }}
                />
              </NavbarBrand>
            );
          },
        },
      },
      {
        name: "nome",
        label: "Nome",
        options: {
          display: true,
          viewColumns: true,
          filter: true,
          customBodyRender: (value, tableMeta) => {
            return (
              <div
                style={{ cursor: "pointer" }}
                onClick={() => {
                  this.editarRegistro(tableMeta.rowData);
                }}
              >
                {value}
              </div>
            );
          },
        },
      },
      {
        name: "recorrencia",
        label: "Recorrência",
        options: {
          display: true,
          viewColumns: true,
          filter: true,
          customBodyRender: (value, tableMeta) => {
            return (
              <div
                style={{ cursor: "pointer" }}
                onClick={() => {
                  this.editarRegistro(tableMeta.rowData);
                }}
              >
                {value != null ? global.tipoRecorrencia[value] : ""}
              </div>
            );
          },
        },
      },
      {
        name: "moeda",
        label: "Moeda",
        options: {
          display: false,
          viewColumns: false,
          filter: false,
          customBodyRender: (value, tableMeta) => {
            return (
              <div
                style={{ cursor: "pointer" }}
                onClick={() => {
                  this.editarRegistro(tableMeta.rowData);
                }}
              >
                {value}
              </div>
            );
          },
        },
      },
      {
        name: "valorPago",
        label: "Valor",
        options: {
          display: true,
          viewColumns: true,
          filter: true,
          customBodyRender: (value, tableMeta) => {
            return (
              <div
                style={{ cursor: "pointer" }}
                onClick={() => {
                  this.editarRegistro(tableMeta.rowData);
                }}
              >
                {value != null
                  ? value
                      .toLocaleString("pt-BR", {
                        style: "currency",
                        currency: "BRL",
                      })
                      .replace("R$", "")
                  : ""}
              </div>
            );
          },
        },
      },
      {
        name: "inicioContrato",
        label: "Início Contrato",
        options: {
          display: true,
          viewColumns: true,
          filter: true,
          customBodyRender: (value, tableMeta) => {
            return (
              <div
                style={{ cursor: "pointer" }}
                onClick={() => {
                  this.editarRegistro(tableMeta.rowData);
                }}
              >
                {value != null ? this.preparaDataParaGrid(value) : ""}
              </div>
            );
          },
        },
      },
      {
        name: "fimContrato",
        label: "Fim Contrato",
        options: {
          display: true,
          viewColumns: true,
          filter: true,
          customBodyRender: (value, tableMeta) => {
            return (
              <div
                style={{ cursor: "pointer" }}
                onClick={() => {
                  this.editarRegistro(tableMeta.rowData);
                }}
              >
                {value != null ? this.preparaDataParaGrid(value) : ""}
              </div>
            );
          },
        },
      },
      {
        name: "pais",
        label: "Pais",
        options: {
          display: false,
          viewColumns: true,
          filter: false,
        },
      },
      {
        name: "estado",
        label: "Estado",
        options: {
          display: false,
          viewColumns: true,
          filter: true,
          customBodyRender: (value, tableMeta) => {
            return (
              <div
                style={{ cursor: "pointer" }}
                onClick={() => {
                  this.editarRegistro(tableMeta.rowData);
                }}
              >
                {value}
              </div>
            );
          },
        },
      },
      {
        name: "cidade",
        label: "Cidade",
        options: {
          display: false,
          viewColumns: true,
          filter: true,
          customBodyRender: (value, tableMeta) => {
            return (
              <div
                style={{ cursor: "pointer" }}
                onClick={() => {
                  this.editarRegistro(tableMeta.rowData);
                }}
              >
                {value}
              </div>
            );
          },
        },
      },
      {
        name: "endereco",
        label: "Endereço",
        options: {
          display: false,
          viewColumns: true,
          filter: true,
          customBodyRender: (value, tableMeta) => {
            return (
              <div
                style={{ cursor: "pointer" }}
                onClick={() => {
                  this.editarRegistro(tableMeta.rowData);
                }}
              >
                {value}
              </div>
            );
          },
        },
      },
      {
        name: "complemento",
        label: "Complemento",
        options: {
          display: false,
          viewColumns: true,
          filter: true,
          customBodyRender: (value, tableMeta) => {
            return (
              <div
                style={{ cursor: "pointer" }}
                onClick={() => {
                  this.editarRegistro(tableMeta.rowData);
                }}
              >
                {value}
              </div>
            );
          },
        },
      },
      {
        name: "cep",
        label: "CEP",
        options: {
          display: false,
          viewColumns: true,
          filter: true,
          customBodyRender: (value, tableMeta) => {
            return (
              <div
                style={{ cursor: "pointer" }}
                onClick={() => {
                  this.editarRegistro(tableMeta.rowData);
                }}
              >
                {value}
              </div>
            );
          },
        },
      },
      {
        name: "statusFamilia",
        label: "Status",
        options: {
          display: true,
          viewColumns: true,
          filter: false,
          customBodyRender: (value, tableMeta) => {
            return (
              <div
                style={{ cursor: "pointer" }}
                onClick={() => {
                  this.editarRegistro(tableMeta.rowData);
                }}
              >
                {value != null ? global.statusFamilia[value] : ""}
              </div>
            );
          },
        },
      },
    ];

    if (this.state.tipoUsuario < 2) {
      columns.push({
        name: "statusFamilia",
        label: "Acessar Família",
        options: {
          display: true,
          viewColumns: true,
          filter: false,
          customBodyRender: (value, tableMeta) => {
            return (
              <div style={{ cursor: "pointer", textAlign: "left" }}>
                <div
                  onClick={() => {
                    this.mudarFamilia(tableMeta.rowData[0]);
                  }}
                  style={{
                    padding: "0.5rem 0.5rem",
                    float: "left",
                    color: "white",
                    borderRadius: "30px",
                    background: "#9947F4",
                    fontWeight: "100",
                    width: "147px",
                  }}
                >
                  <i
                    style={{ marginRight: ".5rem" }}
                    class="fas fa-sign-in-alt"
                  ></i>
                  Acessar Família
                </div>
              </div>
            );
          },
        },
      });
    }

    columns.forEach((item) => {
      if (item.name === this.state.colunaOrdenacao) {
        item.options.sortDirection = this.state.direcaoOrdenacao;
      }
    });
    var localExpandable = [];
    if (this.state.expandable != null) {
      localExpandable = this.state.expandable;
    }
    const { data, page, count, isLoading, numeroItensPorPagina } = this.state;
    const options = {
      expandableRows: true,
      expandableRowsOnClick: false,
      rowsExpanded: localExpandable,
      isRowExpandable: (dataIndex, expandedRows) => {
        return true;
      },
      onRowsExpand: (a, b) => {
        this.asyncCall(a[0].index);
      },
      renderExpandableRow: (rowData, rowMeta) => {
        const colSpan = rowData.length + 1;
        if (this.membersPerId[rowData[0]] != null) {
          var items = this.membersPerId[rowData[0]].data.results;
          if (items == null || items.length === 0) {
            return (
              <TableRow style={{ background: "#F9F9F9" }}>
                <TableCell colSpan={colSpan}>
                  Nenhum usuário encontrado
                </TableCell>
              </TableRow>
            );
          }
          return (
            <TableRow style={{ background: "#F9F9F9" }}>
              <TableCell colSpan={colSpan}>
                {items.map((value, index) => {
                  var isFinal =
                    index + 1 !== items.length ? (
                      <div
                        style={{
                          width: "100%",
                          height: "1px",
                          background: "#c1c1c1",
                        }}
                      ></div>
                    ) : (
                      <></>
                    );
                  return (
                    <div style={{ width: "100%" }}>
                      <div
                        style={{
                          padding: "1rem 2rem",
                          "text-align": "center",
                          width: "25%",
                          display: "inline-block",
                        }}
                      >
                        {value.nome} {value.sobrenome}
                      </div>
                      <div
                        style={{
                          padding: "1rem 2rem",
                          "text-align": "center",
                          width: "25%",
                          display: "inline-block",
                        }}
                      >
                        {value.celular}
                      </div>
                      <div
                        style={{
                          padding: "1rem 2rem",
                          "text-align": "center",
                          width: "25%",
                          display: "inline-block",
                        }}
                      >
                        {value.documento}
                      </div>
                      <div
                        style={{
                          padding: "1rem 2rem",
                          "text-align": "center",
                          width: "25%",
                          display: "inline-block",
                        }}
                      >
                        {global.estadoCivil[value.estadoCivil]}
                      </div>
                      {isFinal}
                    </div>
                  );
                })}
              </TableCell>
            </TableRow>
          );
        } else {
          return <></>;
        }
      },
      textLabels: global.textLabels,
      filter: true,
      filterType: "dropdown",
      responsive: "stacked",
      print: false,
      download: false,
      searchText: this.state.textoDigitado,
      count: count,
      page: page,
      sort: true,
      selectableRows: "multiple",
      //selectableRowsOnClick: true,
      serverSide: true,
      rowsPerPage: numeroItensPorPagina,
      onRowsDelete: (rowsDeleted) => {
        this.removerRegistros(rowsDeleted);
        return false;
      },
      onRowClick: (rowData, rowMeta) => {
        //this.editarRegistro(rowData);
      },
      onFilterChange: (changedColumn, filterList) => {},
      onColumnSortChange: (changedColumn) => {
        //ASC
        let order = "";
        let direcaoOrdenacao = "asc";

        if (this.state.colunaOrdenacao === changedColumn) {
          if (
            this.state.direcaoOrdenacao != null &&
            this.state.direcaoOrdenacao === "asc"
          ) {
            order = " Desc";
            direcaoOrdenacao = "desc";
          }
        }
        this.setState(
          {
            sortField: changedColumn + order,
            colunaOrdenacao: changedColumn,
            direcaoOrdenacao: direcaoOrdenacao,
          },
          () => {
            this.getData();
          }
        );
      },
      onTableChange: (action, tableState) => {
        switch (action) {
          case "changePage":
            this.setState({ numeroPagina: tableState.page + 1 }, () => {
              this.getData();
            });
            break;
          case "search":
            this.filtroPorTexto(tableState.searchText);
            break;
          default:
            break;
        }
      },
    };

    return (
      <div style={{ padding: "0 5%", marginTop: 20 }}>
        <Card style={{ border: "none" }}>
          <CardHeader className="border-0">
            <Row className="align-items-center" style={{ paddingLeft: 0 }}>
              <div className="col">
                <img
                  src={require("../../assets/img/theme/icone-calendario.png")}
                  alt="Meios de Pagamento"
                  className="before-title-img"
                />
                <h3 className="mb-0 chart-title">
                  {this.state.labelFamiliaMaiusculo}s
                </h3>
              </div>
              <GoBackButton />
              <Button text="Novo Cliente" onClick={() => global.showModal(this.modal)} />
            </Row>
          </CardHeader>
        </Card>
        <MuiThemeProvider theme={this.getMuiTheme()}>
          <MUIDataTable
            id="orcamento-table"
            title={
              <Typography>
                {isLoading && (
                  <CircularProgress
                    size={24}
                    style={{ marginLeft: 15, position: "relative", top: 4 }}
                  />
                )}
              </Typography>
            }
            data={data}
            columns={columns}
            options={options}
          />
        </MuiThemeProvider>
        {this.mostrarPopup()}
        {this.mostrarPopupCadastrarPessoas()}
        {this.renderRedirect()}
        {this.redirecionarUsuarios()}
      </div>
    );
  }
}

export default Familias;
