import React, { useEffect, useCallback } from "react";
import Tabela from "./Components/Tabela";
import { Card, CardHeader, Container, Row, Col } from "reactstrap";
import { confirmAlert } from "react-confirm-alert";
import { AssociateParams } from "../../../domain/meioPagamento/api/bankIntegrationAccountAssociation";
import { Params as DeleteConnectionParams } from "../../../domain/bankingIntegration/api/deleteConnection"
import { useRequiredAuth } from "../../../hooks/useAuth";
import { useCurrentPessoa } from "../../../hooks/useCurrentPessoa";
import { useCurrentFamily } from "../../../hooks/useCurrentFamily";
import { useFamilyMeiosPagamentos } from '../../../queries/meioPagamento/queries'
import { useBankIntegrationConnections } from "../../../queries/bankingIntegration/queries";
import { useAssociateToBankIntegrationAccount } from "../../../queries/meioPagamento/mutations";
import { useDeleteConnectionMutation } from "../../../queries/bankingIntegration/mutations";
import { getNewConnectionUrl } from "../../../domain/bankingIntegration/api/getNewConnectionUrl";
import Loading from '../../../components/Loading'
import ErrorMessageModal, { MessageComponentProps } from "../../../components/ErrorHandling/ErrorMessageModal";

export default () => {
  useRequiredAuth();

  const { personId, isLoadingPessoa, isErrorPessoa, pessoaError } = useCurrentPessoa()
  const { connections, isLoadingConnections, isErrorConnections, connectionsError } = useBankIntegrationConnections(personId)
  const { familyId, isLoadingFamily, isErrorFamily, familyError } = useCurrentFamily()
  const { meiosPagamento, isLoadingMeios, isErrorMeios, meiosPagamentoError } = useFamilyMeiosPagamentos(familyId)
  const isError = isErrorPessoa || isErrorFamily || isErrorMeios || isErrorConnections
  const isLoading = isLoadingPessoa || isLoadingFamily || isLoadingMeios || isLoadingConnections
  const labels = meiosPagamento?.map(m => ({ value: m.id, label: m.nome })) || []

  const meioPagamentoAssociationMutation = useAssociateToBankIntegrationAccount()
  const deleteConnectionMutation = useDeleteConnectionMutation()

  useEffect(() => global.mostrarAjudaDaPaginaAutomaticamente, []);

  const criarNovaConexao = async () => {
    const { url } = await getNewConnectionUrl(personId)
    if (url) {
      window.open(url, "_blank");
    } else {
      // TODO: Mudar para modal de erros
      confirmAlert({
        title: "Informação",
        message: "Erro",
        buttons: [
          {
            label: "Ok",
            onClick: () => { },
          },
        ],
      });
    }
  };

  const atualizarMeioPagamento = async ({ connectionId, accountId, meioPagamentoId }: AssociateParams) => {
    await meioPagamentoAssociationMutation.mutateAsync({ connectionId, accountId, meioPagamentoId })
    confirmAlert({
      title: "Informação",
      message: "Alteração realizada com sucesso. A partir de agora, os novos lançamentos estarão associados a esse meio de pagamento.",
      buttons: [
        {
          label: "Ok",
          onClick: () => { },
        },
      ],
    });
  };

  const deleteConnection = async (params: DeleteConnectionParams) => {
    await deleteConnectionMutation.mutateAsync(params)
  }

  const getErrorMessages = () => {
    const tags = { 
      personId: personId?.toString(), 
      connections: JSON.stringify(connections), 
      familyId: familyId?.toString(),
      meiosPagamento: JSON.stringify(meiosPagamento)
    }
    const errorMessages: MessageComponentProps[] = []
    const title = 'Erro de integração'
    if (isErrorPessoa)
      errorMessages.push({ title, text: `Erro ao recuperar dados da pessoa ${personId}`, error: pessoaError, tags})
    if (isErrorFamily)
      errorMessages.push({ title, text: `Erro ao recuperar dados da família ${familyId}`, error: familyError, tags})
    if (isErrorConnections)
      errorMessages.push({ title, text: `Erro ao recuperar conexões da família ${familyId}`, error: connectionsError, tags})
    if (isErrorMeios)
      errorMessages.push({ title, text: `Erro ao recuperar meios de pagamento da família ${familyId}`, error: meiosPagamentoError, tags})
    return errorMessages
  }

  return (
    <Container fluid>
      <ErrorMessageModal messages={getErrorMessages()} on={isError} />
      <Row className="home-summary-top">
        <Col lg="12" xl="12" style={{ marginBottom: "15px" }}>
          <Card>
            <CardHeader className="border-0">
              <Row className="align-items-center">
                <div className="col ml--3">
                  <img
                    src={require("../../../assets/img/theme/icone-calendario.png")}
                    alt="Meios de Pagamento"
                    className="before-title-img"
                  />
                  <h3 className="mb-0 chart-title">Integração bancária</h3>
                </div>
                <div style={{ float: "right" }}>
                  <button
                    id="salvar-lancamento"
                    className="featured-button float-right"
                    onClick={() => criarNovaConexao()}
                    style={{
                      backgroundImage: `url("${require("../../../assets/img/theme/botao-destaque.png")}")`,
                      backgroundSize: "cover",
                      marginLeft: 4
                    }}
                  >
                    Conectar Contas
                  </button>
                </div>
              </Row>
            </CardHeader>
          </Card>
        </Col>
      </Row>
      <Row className="entry-list">
        <Col lg="12" xl="12" style={{ marginBottom: "15px" }}>
          {
            isLoading
              ? <Loading />
              : <Tabela
                data={connections}
                meiosPagamento={labels}
                removerConexao={deleteConnection}
                atualizarMeioPagamento={atualizarMeioPagamento}
              />
          }
        </Col>
      </Row>
    </Container>
  );
};