import React from 'react'
import IntegracaoBancaria from "./index";
import { UseFamilyMeiosPagamentosResponse } from '../../../queries/meioPagamento/queries'
import { UseBankIntegrationConnectionsResponse } from "../../../queries/bankingIntegration/queries";
import { CurrentPersonContextValue } from "../../../hooks/contexts/currentPersonContext";
import { CurrentFamilyContextValue } from "../../../hooks/contexts/currentFamilyContext";
import { render } from '@testing-library/react';
import { useCurrentPessoa } from "../../../hooks/useCurrentPessoa";
import { useCurrentFamily } from "../../../hooks/useCurrentFamily";
import { useFamilyMeiosPagamentos } from '../../../queries/meioPagamento/queries'
import { useBankIntegrationConnections } from "../../../queries/bankingIntegration/queries";

window.newrelic = {}
window.newrelic.noticeError = jest.fn()

jest.mock("../../../hooks/useAuth")
jest.mock("../../../hooks/useCurrentPessoa")
jest.mock("../../../hooks/useCurrentFamily")
jest.mock('../../../queries/meioPagamento/queries')
jest.mock("../../../queries/bankingIntegration/queries")
jest.mock("../../../queries/meioPagamento/mutations")
jest.mock("../../../queries/bankingIntegration/mutations")

describe('IntegracaoBancaria', () => {
    beforeEach(() => {
        jest.resetModules()
    });

    describe('Error handling', () => {
        const setupMocks = ({ pessoa, family, meiosPagamemtos, connections }: Mocks = {}) => {
            (useCurrentPessoa as jest.MockedFunction<typeof useCurrentPessoa>)
                .mockImplementation(() => pessoa ?? defaultPessoaResponse);
            (useCurrentFamily as jest.MockedFunction<typeof useCurrentFamily>)
                .mockImplementation(() => family ?? defaultFamilyResponse);
            (useFamilyMeiosPagamentos as jest.MockedFunction<typeof useFamilyMeiosPagamentos>)
                .mockImplementation(() => meiosPagamemtos ?? defaultMeiosPagamentosResponse);
            (useBankIntegrationConnections as jest.MockedFunction<typeof useBankIntegrationConnections>)
                .mockImplementation(() => connections ?? defaultConnectionsResponse);
        }

        it('should call noticeError when error getting person data', () => {
            setupMocks({
                pessoa: {
                    isErrorPessoa: true,
                    isLoadingPessoa: false,
                    personId: 1,
                    pessoaError: new Error('my sample test'),
                    pessoa: undefined
                }
            })
            render(<IntegracaoBancaria />)
            expect(window.newrelic.noticeError).toHaveBeenCalledTimes(1)
        })
    })
})

interface Mocks {
    pessoa?: CurrentPersonContextValue,
    meiosPagamemtos?: UseFamilyMeiosPagamentosResponse,
    family?: CurrentFamilyContextValue,
    connections?: UseBankIntegrationConnectionsResponse
}

const defaultPessoaResponse: CurrentPersonContextValue = {
    isLoadingPessoa: false,
    isErrorPessoa: false,
    pessoaError: null,
    personId: 1,
    pessoa: {
        id: 1,
        nome: 'test pessoa',
        sobrenome: 'tester',
        documento: '030.348.262-14',
        celular: '11 987654321',
        nomeUsuario: 'Test',
        dataNascimento: '12/01/2022',
        estadoCivil: 0,
        temCarteira: 0,
        chaveBob: '1234567890',
        estaHabilitadoBob: true,
        dataCriacao: '12/01/2022',
        dataUltimaAtualizacao: '12/01/2022',
        ativo: 1,
    }
}

const defaultMeiosPagamentosResponse: UseFamilyMeiosPagamentosResponse = {
    isLoadingMeios: false,
    isErrorMeios: false,
    meiosPagamento: [],
    meiosPagamentoError: null
}

const defaultFamilyResponse: CurrentFamilyContextValue = {
    isLoadingFamily: false,
    isErrorFamily: false,
    family: {
        nome: 'Família teste',
        endereco: 'r. teste',
        complemento: 'c',
        pais: 'Brasil',
        cidade: 'Teste',
        estado: 2,
        cep: '01527020',
        valorPago: 0,
        inicioContrato: '12/01/2022',
        fimContrato: '12/01/2032',
        recorrencia: 1,
        statusFamilia: 1,
        quantidadeCarteiras: 1,
        id: 1,
        dataCriacao: '12/01/2022',
        dataUltimaAtualizacao: '12/01/2022',
        ativo: 1
    },
    familyId: 1,
    familyError: null
}

const defaultConnectionsResponse: UseBankIntegrationConnectionsResponse = {
    connections: [],
    connectionsError: null,
    isErrorConnections: false,
    isLoadingConnections: false
}