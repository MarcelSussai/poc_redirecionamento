import $ from "jquery";

const HEADERS = {
  Authorization: `bearer ${localStorage.getItem("access-token")}`,
  "Access-Control-Allow-Origin": "*",
  "Access-Control-Allow-Headers": "Authorization",
  "Access-Control-Allow-Methods": "GET, POST, OPTIONS, PUT, PATCH, DELETE",
  Accept: "application/json",
  "Content-Type": "application/json",
};

export default {
  obterConexoes: async (pessoaId) => {
    try {
      global.spinnerShow($);

      let request = await fetch(
        `${global.server_api_new}${localStorage.getItem(
          "tokenAcesso"
        )}/bankintegration/obterconexoesecontas/${pessoaId}`,
        {
          method: "Get",
          headers: HEADERS,
        }
      );

      let json = await request.json();

      return json;
    } catch (error) {
      console.log("API::obterConexoes: Erro: ", error);
    } finally {
      global.spinnerHide($, [
        document.documentElement.scrollLeft || document.body.scrollLeft,
        document.documentElement.scrollTop || document.body.scrollTop,
      ]);
    }
  },

  obterMeiosPagamento: async (familiaId) => {
    try {
      global.spinnerShow($);

      let request = await fetch(
        `${global.server_api_new}${localStorage.getItem(
          "tokenAcesso"
        )}/bob/mpgtoctbob/obtermeios/${familiaId}`,
        {
          method: "Get",
          headers: HEADERS,
        }
      );

      let json = await request.json();

      return json;
    } catch (error) {
      console.log("API::obterMeiosPagamento: Erro: ", error);
    } finally {
      global.spinnerHide($, [
        document.documentElement.scrollLeft || document.body.scrollLeft,
        document.documentElement.scrollTop || document.body.scrollTop,
      ]);
    }
  },

  removerConexao: async (conexaoId, pessoaId) => {
    try {
      global.spinnerShow($);

      let request = await fetch(
        `${global.server_api_new}${localStorage.getItem(
          "tokenAcesso"
        )}/bob/excluirconexao/${pessoaId}/conexao/${conexaoId}`,
        {
          method: "Post",
          headers: HEADERS,
          body: {},
        }
      );

      let json = await request.json();

      return json;
    } catch (error) {
      console.log("API::removerConexao: Erro: ", error);
    } finally {
      global.spinnerHide($, [
        document.documentElement.scrollLeft || document.body.scrollLeft,
        document.documentElement.scrollTop || document.body.scrollTop,
      ]);
    }
  },

  atualizarMeioPagamento: async (contaId, meioPagamentoId) => {
    try {
      global.spinnerShow($);

      let request = await fetch(
        `${global.server_api_new}${localStorage.getItem(
          "tokenAcesso"
        )}/bob/associacontaMeio/${meioPagamentoId}/conta/${contaId}`,
        {
          method: "Get",
          headers: HEADERS,
        }
      );

      let json = await request.json();

      return json;
    } catch (error) {
      console.log("API::atualizarMeioPagamento: Erro: ", error);
    } finally {
      global.spinnerHide($, [
        document.documentElement.scrollLeft || document.body.scrollLeft,
        document.documentElement.scrollTop || document.body.scrollTop,
      ]);
    }
  },
};
