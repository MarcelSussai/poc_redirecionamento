import React from "react";
import { MuiThemeProvider, createMuiTheme } from "@material-ui/core/styles";
import MUIDataTable from "mui-datatables";
import BotaoNovoMeioPagamento from "./BotaoNovoMeioPagamento";
import TableRow from "@material-ui/core/TableRow";
import TableCell from "@material-ui/core/TableCell";
import TableSortLabel from "@material-ui/core/TableSortLabel";
import { registerLocale } from "react-datepicker";
import br from "date-fns/locale/pt-BR";
import Select from "react-select";
import { AssociateParams } from  '../../../../domain/meioPagamento/api/bankIntegrationAccountAssociation'
import { Params as DeleteConnectionParams } from  '../../../../domain/bankingIntegration/api/deleteConnection'

registerLocale("br", br);
const moment = require("moment");

interface Params {
  data: any[],
  meiosPagamento: { value: number, label: string }[],
  removerConexao: (params: DeleteConnectionParams) => void,
  atualizarMeioPagamento: (params: AssociateParams) => Promise<void>,
}

export default ({
  data,
  meiosPagamento,
  removerConexao,
  atualizarMeioPagamento
}: Params) => {
  const theme = createMuiTheme({
    overrides: {
      MuiTableRow: {
        root: {
          "&:nth-of-type(odd)": {
            backgroundColor: "#f8f9fe",
          },
          "& input": {
            background: "transparent",
          },
        },
      },
      MuiTableCell: {
        root: {
          fontFamily: "unset",
          borderTop: "none",
          padding: "16px 10px",
          textAlign: "center",
          lineHeight: "1! important",
          fontSize: ".8rem !important",
        },
        footer: {
          color: "#FFF",
          fontWeight: 700,
        },
      },
      MuiPaper: {
        root: {
          overflow: "hidden",
          margin: 0,
        },
      },
      MuiToolbar: {
        regular: {
          backgroundColor: "#e4eaf4",
          minHeight: "0 !important",
        },
      },
    },
  });
  // Adding connection id to each account
  const connections = data.map(connection => 
    ({ 
      ...connection, 
      contas: connection.contas.map(conta => ({
        ...conta,
        connectionId: connection.id
      }))
    })
  )

  const customRender = {
    customHeadRender: (index, column, sortColumn) => (
      <TableCell
        onClick={() => sortColumn(index)}
        key={index}
        style={{
          textAlign: "center",
          fontWeight: "bold",
          backgroundColor: "#ffffff",
          cursor: "pointer",
        }}
      >
        {column.label}
        <TableSortLabel
          active={column.sortDirection !== "none"}
          direction={column.sortDirection || "asc"}
        />
      </TableCell>
    ),
    customBodyRenderFormatCurrency: (value) => (
      <span style={{ color: value < 0 ? "#c21515" : "#000000de" }}>
        {value.toLocaleString("pt-BR", {
          style: "currency",
          currency: "BRL",
        })}
      </span>
    ),
    customBodyRenderFormatPercent: (value) => (
      <span>
        {value.toLocaleString("pt-BR", {
          style: "percent",
          minimumFractionDigits: 2,
          maximumFractionDigits: 2,
        })}
      </span>
    ),
    customBodyRenderFormatDate: (value) => {
      return <span>{moment(value).format("DD/MM/yyyy HH:mm")}</span>;
    },
    customBodyRenderOptions: (value) => {
      return (
        <span>
          <i
            onClick={() => removerConexao({connectionId: value})}
            className="fas fa-trash-alt"
            style={{ cursor: "pointer", marginLeft: "1rem" }}
          ></i>
        </span>
      );
    },
    customBodyRenderStatus: (value, tableMeta) => {
      if (tableMeta.rowData[1] === true) {
        return (
          <span>
            <u>
              <a
                style={{ color: "red" }}
                href={tableMeta.rowData[2]}
                target="_blank"
                rel="noopener noreferrer"
              >
                {value}
              </a>
            </u>
          </span>
        );
      } else {
        return <span style={{ color: "green" }}>{value}</span>;
      }
    },
    customBodySelectMeioPagamento: (value, tableMeta) => {
      return (
        <Select
          placeholder="Selecione..."
          options={meiosPagamento}
          defaultValue={{ label: meiosPagamento.find(m => m.value === tableMeta.rowData[4])?.label || '' }}
          onChange={(item: { value: number, label: string }) => {
              console.log(tableMeta.rowData)
              const connectionId = tableMeta.rowData[3]
              const accountId = tableMeta.rowData[0]
              const meioPagamentoId = item.value
              atualizarMeioPagamento({connectionId, accountId, meioPagamentoId})
            }
          }
          menuPortalTarget={document.body}
          menuPosition={"fixed"}
        />
      );
    },
    renderNewMeioPagamentoButton: (value, tableMeta) => <BotaoNovoMeioPagamento />
  };

  const columns = {
    conexoes: [
      {
        name: "contas",
        options: {
          display: false,
          filter: false,
          viewColumns: false,
        },
      },
      {
        name: "acaoRequerida",
        options: {
          display: false,
          filter: false,
          viewColumns: false,
        },
      },
      {
        name: "urlAcao",
        options: {
          display: false,
          filter: false,
          viewColumns: false,
        },
      },
      {
        name: "id",
        options: {
          display: false,
          filter: false,
          viewColumns: false,
        },
      },
      {
        name: "conexaoNome",
        label: "Instituição",
        options: {
          customHeadRender: ({ index, ...column }, sortColumn) =>
            customRender.customHeadRender(index, column, sortColumn),
          filter: true,
        },
      },
      {
        name: "qtdContasCards",
        label: "Contas Conectadas",
        options: {
          filter: true,
          customHeadRender: ({ index, ...column }, sortColumn) =>
            customRender.customHeadRender(index, column, sortColumn),
        },
      },
      {
        name: "status",
        label: "Status",
        options: {
          filter: true,
          customHeadRender: ({ index, ...column }, sortColumn) =>
            customRender.customHeadRender(index, column, sortColumn),
          customBodyRender: (value, tableMeta) =>
            customRender.customBodyRenderStatus(value, tableMeta),
        },
      },
      {
        name: "ultimoSucesso",
        label: "Última Atualizacão",
        options: {
          filter: false,
          customHeadRender: ({ index, ...column }, sortColumn) =>
            customRender.customHeadRender(index, column, sortColumn),
          customBodyRender: (props) =>
            customRender.customBodyRenderFormatDate(props),
        },
      },
      {
        name: "id",
        label: "Opções",
        options: {
          filter: false,
          customHeadRender: ({ index, ...column }, sortColumn) =>
            customRender.customHeadRender(index, column, sortColumn),
          customBodyRender: (props) =>
            customRender.customBodyRenderOptions(props),
        },
      },
    ],
    contas: [
      {
        name: "id",
        options: {
          display: false,
          filter: false,
          viewColumns: false,
        },
      },
      {
        name: "descricao",
        label: "Descricao",
        options: {
          customHeadRender: ({ index, ...column }, sortColumn) =>
            customRender.customHeadRender(index, column, sortColumn),
        },
      },
      {
        name: "meioPagamento.label",
        label: "Meio de Pagamento",
        options: {
          customHeadRender: ({ index, ...column }, sortColumn) =>
            customRender.customHeadRender(index, column, sortColumn),
          customBodyRender: (value, tableMeta) =>
            customRender.customBodySelectMeioPagamento(value, tableMeta),
        },
      },
      {
        name: "connectionId",
        options: {
          display: false,
          filter: false,
          viewColumns: false,
        },
      },
      {
        name: "meioPagamentoId",
        options: {
          display: false,
          filter: false,
          viewColumns: false,
        },
      },
      {
        name: "novoMeioPagamentoBotao",
        label: " ",
        options: {
          customBodyRender: (value, tableMeta) =>
            customRender.renderNewMeioPagamentoButton(value, tableMeta)
        }
      }
    ],
  };

  const options = {
    conexoes: {
      rowsPerPage: 100,
      textLabels: (global as any).textLabels,
      filter: true,
      print: true,
      download: true,
      filterType: "dropdown",
      responsive: "standard",
      sort: true,
      pagination: false,
      selectableRows: false,
      expandableRows: true,
      expandableRowsHeader: false,
      expandableRowsOnClick: false,
      customFooter: () => {},
      renderExpandableRow: (rowData) => {
        if (rowData[0] == null) {
          return false;
        }

        const colSpan = rowData.length + 1;
        return (
          <TableRow>
            <TableCell colSpan={colSpan}>
              <MuiThemeProvider theme={theme}>
                <MUIDataTable
                  data={rowData[0]}
                  columns={columns.contas}
                  options={options.contas}
                />
              </MuiThemeProvider>
            </TableCell>
          </TableRow>
        );
      },
    },
    contas: {
      search: false,
      filter: false,
      print: false,
      download: false,
      viewColumns: false,
      responsive: "standard",
      selectableRows: false,
      rowsPerPage: 12,
      customFooter: () => {},
    },
  };

  return (
    <MuiThemeProvider theme={theme}>
      <MUIDataTable
        title={<span className="font-weight-600">Conexões</span>}
        data={connections}
        columns={columns.conexoes}
        options={options.conexoes}
      />
    </MuiThemeProvider>
  );
};
