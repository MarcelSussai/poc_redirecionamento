import React from "react";
import IconButton from "@material-ui/core/IconButton";
import ControlPointIcon from '@material-ui/icons/ControlPoint';
import routes from "../../../../routes";
import './BotaoNovoMeiopagamento.css'

const BotaoNovoMeioPagamento = () => {
  
  const redirectToMeioPagametosPage = () => {
    const meioPagamentoRoute = routes.find(route => route.name === "Meios de Pagamento")
    if (!meioPagamentoRoute) {
    // TODO: Adicionar error handling para quando não encontramos rota
      const mensagemErr = `Meio pagamento route not found`
      console.error(mensagemErr)
      throw new Error(mensagemErr)
    }
    window.location.replace(`${meioPagamentoRoute.layout}${meioPagamentoRoute.path}`);
  }

  return (
    <IconButton className="hoverable" aria-label="Criar meio de pagamento" onClick={redirectToMeioPagametosPage}>
      <span className="hoverable-text" >Criar meio de pagamento</span>
      <ControlPointIcon />
    </IconButton>
  )
}

export default BotaoNovoMeioPagamento;