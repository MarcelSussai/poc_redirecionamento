import React from "react";
import { Redirect } from "react-router-dom";
import { Row } from "reactstrap";
import ButtonBackPage from "../../components/Utils/ButtonBackPage.jsx";

class Leads extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      accessToken: localStorage.getItem("access-token"),
      empresaId: localStorage.getItem("empresa-id"),
      planejadorId: localStorage.getItem("planejador-id"),
      familiaId: localStorage.getItem("familia-id"),
      tipoUsuario: parseInt(localStorage.getItem("tipo-usuario")),

      selectedVideo: "cadastro",
    };
  }

  componentDidMount() {
    if (this.state.accessToken == null) {
      this.setRedirect();
      return;
    }
  }

  setRedirect = () => {
    this.setState({
      redirect: true,
    });
  };

  renderRedirect = () => {
    if (this.state.redirect) {
      return <Redirect to="/auth/login" />;
    }
  };

  render() {
    return (
      <div id="leads" style={{ padding: "0 5%", marginTop: 20 }}>
        <Row>
          <div className="modal_pesquisa">
            <p className=" ">
              Criaremos uma página para atrair clientes para os planejadores.
            </p>
            <p className=" pt-3 text-justify">
              Os clientes preencherão um formulário com sua história e
              responderão algumas perguntas. A partir daí, direcionaremos esses
              leads para a nossa base de planejadores.
            </p>
            <br />

            <p className="  text-justify">
              Para isso, algumas regras deverão ser cumpridas:
            </p>
            <p className="  text-justify">
              - É obrigatório a utilização do Vista durante todo o trabalho;
              <br />
              - A metodologia, preço e forma de trabalho é de livre escolha; -
              Não temos responsabilidade na execução e nos pagamentos;
              <br />
              - Os critérios para indicação dos planejadores serão:
              <br />
              Planejadores que mais utilizam o Vista;
              <br />
              Perfil do planejador e lead;
              <br />
              Certificações.
            </p>
            <br />
            <br />
            <p className="pb-4">
              Vale lembrar que estamos fazendo um "piloto" que pode, ainda,
              sofrer alterações. Não garantimos que todos receberão leads.
            </p>

            <div
              className="place-items pb-3"
              style={{ marginLeft: "0px", fontSize: "20px" }}
            >
              <a
                className="featured-button"
                href="https://docs.google.com/forms/d/e/1FAIpQLSf1JvvEX4t-MgEgpu9sVSQBu5nf6Scxs1ukPo9tnJmqXNDv8A/viewform"
                style={{ backgroundColor: "#172b4d", color: "#000" }}
                target="_blank"
              >
                RESPONDER FORMULÁRIOS
              </a>
            </div>

            <p className="pt-5 modal_text_footer">
              Um forte abraço,
              <br />
              Luiz Fernando Schvatzman <br />
              CEO - VISTA
            </p>

            <ButtonBackPage />
          </div>
        </Row>
        {this.renderRedirect()}
      </div>
    );
  }
}

export default Leads;
