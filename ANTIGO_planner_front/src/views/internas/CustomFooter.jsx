import React from "react";
import TableFooter from "@material-ui/core/TableFooter";
import TableRow from "@material-ui/core/TableRow";
import TableCell from "@material-ui/core/TableCell";
// import MuiTablePagination from "@material-ui/core/TablePagination";
import { withStyles } from "@material-ui/core/styles";

const defaultFooterStyles = {
};

class CustomFooter extends React.Component {

  handleRowChange = event => {
    this.props.changeRowsPerPage(event.target.value);
  };

  handlePageChange = (_, page) => {
    this.props.changePage(page);
  };

  dataPagination = (dados, style) => {

    if(dados == null || dados.length === 0){
        return (<></>);
    }

    return dados.map((item, key) => {
        return (
            <TableCell key={key} className="MuiTableCell-root" style={style}>
                <font id={'footer-' + key}>{item}</font>
            </TableCell>
        );
    });
  }

  render() {
    const { header, data, data2, data3, data4, data5, data6, data7, data8} = this.props;

    // var footerStyle = {};

    // if(hasPagination){
    //     footerStyle = {
    //         display:'flex', 
    //         justifyContent: 'flex-end',
    //         padding: '0px 24px 0px 24px'
    //     };
    // }
    // else {
    //     footerStyle = {
    //         display:'none',
    //     };
    // }

    return (
        <TableFooter>
            <TableRow style={{background: "#E4EAF5"}}>
                {this.dataPagination(header, {fontWeight:"600", color:"black", textAlign:"center"})}
            </TableRow>
            <TableRow style={{background: "#E4EAF5"}}>
                {this.dataPagination(data, {color:"black", textAlign:"center"})}
            </TableRow>
            <TableRow style={{background: "#2BACC2"}}>
                {this.dataPagination(data2, {color:"white", textAlign:"center"})}
            </TableRow>
            <TableRow style={{background: "#9633FF"}}>
                {this.dataPagination(data3, {color:"white", textAlign:"center"})}
            </TableRow>
            <TableRow style={{background: "#2BACC2"}}>
                {this.dataPagination(data4, {color:"white", textAlign:"center"})}
            </TableRow>
            <TableRow style={{background: "#2BACC2"}}>
                {this.dataPagination(data5, {color:"white", textAlign:"center"})}
            </TableRow>
            <TableRow style={{background: "#2BACC2"}}>
                {this.dataPagination(data6, {color:"white", textAlign:"center"})}
            </TableRow>
            <TableRow style={{background: "#2BACC2"}}>
                {this.dataPagination(data7, {color:"white", textAlign:"center"})}
            </TableRow>
            <TableRow style={{background: "#2BACC2"}}>
                {this.dataPagination(data8, {color:"white", textAlign:"center"})}
            </TableRow>
            {/* <TableRow>
                <TableCell style={footerStyle} colSpan={1000}>
                    <MuiTablePagination
                        component="div"
                        count={count}
                        rowsPerPage={rowsPerPage}
                        page={page}
                        labelRowsPerPage={textLabels.rowsPerPage}
                        labelDisplayedRows={({ from, to, count }) => `${from}-${to} ${textLabels.displayRows} ${count}`}
                        backIconButtonProps={{
                            'aria-label': textLabels.previous,
                        }}
                        nextIconButtonProps={{
                            'aria-label': textLabels.next,
                        }}
                        rowsPerPageOptions={[0,1,2,3,4,5,10,25,50,100]}
                        onChangePage={this.handlePageChange}
                        onChangeRowsPerPage={this.handleRowChange}
                    />
                </TableCell>
            </TableRow> */}
        </TableFooter>
    );
  }

}

export default withStyles(defaultFooterStyles, { name: "CustomFooter" })(CustomFooter);