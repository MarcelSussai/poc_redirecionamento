import React from "react";
import { Redirect } from "react-router-dom";
import { Col, Row, Card, CardHeader } from "reactstrap";
import ButtonBackPage from "../../components/Utils/ButtonBackPage.jsx";

class Tutoriais extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      accessToken: localStorage.getItem("access-token"),
      empresaId: localStorage.getItem("empresa-id"),
      planejadorId: localStorage.getItem("planejador-id"),
      familiaId: localStorage.getItem("familia-id"),
      tipoUsuario: parseInt(localStorage.getItem("tipo-usuario")),

      selectedVideo: "cadastro",
    };
  }

  componentDidMount() {
    if (this.state.accessToken == null) {
      this.setRedirect();
      return;
    }
  }

  setRedirect = () => {
    this.setState({
      redirect: true,
    });
  };

  renderRedirect = () => {
    if (this.state.redirect) {
      return <Redirect to="/auth/login" />;
    }
  };

  render() {
    const item = {
      lineHeight: "2rem",
      paddingLeft: 0,
      cursor: "pointer",
    };

    var videoURL;

    switch (this.state.selectedVideo) {
      case "cadastro":
        videoURL = "https://player.vimeo.com/video/435336780";
        break;

      case "configuracao":
        videoURL = "https://player.vimeo.com/video/436438112";
        break;

      case "home":
        videoURL = "https://player.vimeo.com/video/442365125";
        break;

      case "planos":
        videoURL = "https://player.vimeo.com/video/436459118";
        break;

      case "orcamento":
        videoURL = "https://player.vimeo.com/video/442736513";
        break;

      case "patrimonio":
        videoURL = "https://player.vimeo.com/video/444637440";
        break;

      case "destinacao":
        videoURL = "https://player.vimeo.com/video/444640836";
        break;

      case "nfe":
        videoURL = "https://player.vimeo.com/video/436539456";
        break;

      case "clicksign":
        videoURL = "https://player.vimeo.com/video/436533781";
        break;

      case "vindi":
        videoURL = "https://player.vimeo.com/video/436533456";
        break;

      default:
        videoURL = "https://player.vimeo.com/video/435336780";
    }

    return (
      <div id="tutoriais" style={{ padding: "0 5%", marginTop: 20 }}>
        <Row>
          <Col>
            <Card style={{ border: "none" }}>
              <CardHeader
                className="border-0"
                style={{ padding: "30px 15px 30px 0" }}
              >
                <Row className="align-items-center" style={{ paddingLeft: 0 }}>
                  <div className="col">
                    <img
                      src={require("../../assets/img/theme/icone-calendario.png")}
                      alt="Meios de Pagamento"
                      className="before-title-img"
                    />
                    <h3 className="mb-0 chart-title">Tutoriais</h3>
                  </div>
                </Row>
              </CardHeader>
            </Card>
          </Col>
        </Row>
        <Row>
          <Col lg="5">
            <h3>Vista</h3>
            <ul style={{ paddingLeft: 0 }}>
              <li
                className="nav-link"
                onClick={() => this.setState({ selectedVideo: "cadastro" })}
                style={item}
              >
                <i class="far fa-play-circle pr-2"></i>Cadastro Vista Planejador
                e Novo Cliente
              </li>

              <li
                className="nav-link"
                onClick={() => this.setState({ selectedVideo: "configuracao" })}
                style={item}
              >
                <i class="far fa-play-circle pr-2"></i>Configuração Inicial e do
                Orçamento
              </li>

              <li
                className="nav-link"
                onClick={() => this.setState({ selectedVideo: "home" })}
                style={item}
              >
                <i class="far fa-play-circle pr-2"></i>Home e Equilíbiro
                Financeiro
              </li>

              <li
                className="nav-link"
                onClick={() => this.setState({ selectedVideo: "planos" })}
                style={item}
              >
                <i class="far fa-play-circle pr-2"></i>Planos e Sonhos
              </li>

              <li
                className="nav-link"
                onClick={() => this.setState({ selectedVideo: "orcamento" })}
                style={item}
              >
                <i class="far fa-play-circle pr-2"></i>Gestão de Orçamento,
                Dívidas e Extrato
              </li>

              <li
                className="nav-link"
                onClick={() => this.setState({ selectedVideo: "patrimonio" })}
                style={item}
              >
                <i class="far fa-play-circle pr-2"></i>Patrimônio
              </li>

              <li
                className="nav-link"
                onClick={() => this.setState({ selectedVideo: "destinacao" })}
                style={item}
              >
                <i class="far fa-play-circle pr-2"></i>Destinação de Patrimônio
                e de Destinação de Investimentos
              </li>
            </ul>

            <h3>Parceiros</h3>
            <ul style={{ paddingLeft: 0 }}>
              <li
                className="nav-link"
                onClick={() => this.setState({ selectedVideo: "nfe" })}
                style={item}
              >
                <i class="far fa-play-circle pr-2"></i>NFE.io e integração com o
                Vindi
              </li>

              <li
                className="nav-link"
                onClick={() => this.setState({ selectedVideo: "clicksign" })}
                style={item}
              >
                <i class="far fa-play-circle pr-2"></i>ClickSign
              </li>

              <li
                className="nav-link"
                onClick={() => this.setState({ selectedVideo: "vindi" })}
                style={item}
              >
                <i class="far fa-play-circle pr-2"></i>Vindi
              </li>
            </ul>
          </Col>
          <Col lg="7" className="text-center mt-3">
            <div style={{ padding: "56.25% 0 0 0", position: "relative" }}>
              <iframe
                src={videoURL}
                style={{
                  position: "absolute",
                  top: 0,
                  left: 0,
                  width: "100%",
                  height: "100%",
                }}
                frameborder="0"
                allow="autoplay; fullscreen"
                allowFullScreen
                title="vídeo selecionado"
              ></iframe>
            </div>
            <script src="https://player.vimeo.com/api/player.js"></script>
          </Col>
        </Row>
        <ButtonBackPage />

        {this.renderRedirect()}
      </div>
    );
  }
}

export default Tutoriais;
