import React from "react";
import { Redirect } from "react-router-dom";
import { CircularProgress, Typography } from "@material-ui/core";
import MUIDataTable from "mui-datatables";
import axios from "axios";
import $ from "jquery";
import SkyLight from "react-skylight";
import { confirmAlert } from "react-confirm-alert";
import MaskedInput from "react-maskedinput";
import Select from "react-select";

import { Col, Row, Input, Card, CardHeader } from "reactstrap";
import { createMuiTheme, MuiThemeProvider } from "@material-ui/core/styles";

class Planejadores extends React.Component {
  state = {
    page: 0,
    count: 1,
    data: [["Carregando Dados..."]],
    isLoading: false,
    pais: "Brasil",
  };

  mascaras = {
    telefone: "(11) 1111-1111",
    celular: "(11) 1 1111-1111",
    cpf: "111.111.111-11",
    cnpj: "11.111.111/1111-11",
  };

  estadoCivil = [
    { value: 0, label: "Solteiro" },
    { value: 1, label: "Casado" },
    { value: 2, label: "Divorciado" },
    { value: 3, label: "Viúvo" },
  ];

  getMuiTheme = () =>
    createMuiTheme({
      overrides: {
        MuiTableRow: {
          root: {
            "&:nth-of-type(odd)": {
              backgroundColor: "#f8f9fe",
            },
          },
        },
        MUIDataTableSelectCell: {
          fixedHeaderCommon: {
            backgroundColor: "unset",
          },
        },
        MuiTableCell: {
          root: {
            fontFamily: "unset",
          },
          footer: {
            color: "#FFF",
            fontWeight: 700,
          },
        },
        MUIDataTableHeadCell: {
          fixedHeaderYAxis: {
            fontWeight: 700,
          },
        },
        MuiPaper: {
          root: {
            overflow: "hidden",
            margin: "10px 40px",
          },
        },
        MuiToolbar: {
          regular: {
            backgroundColor: "#e4eaf4",
          },
        },
        MUIDataTablePagination: {
          root: {
            "&:last-child": {
              margin: "0px 24px 0px 24px",
              padding: 0,
            },
          },
        },
      },
    });

  componentDidMount() {
    this.setState(
      {
        accessToken: localStorage.getItem("access-token"),
        empresaId: localStorage.getItem("empresa-id"),
        planejadorId: localStorage.getItem("planejador-id"),
        familiaId: localStorage.getItem("familia-id"),
        pessoaId: localStorage.getItem("pessoa-id"),
      },
      function () {
        this.getData();
        global.mostrarAjudaDaPaginaAutomaticamente(this);
      }
    );
  }

  setRedirect = () => {
    this.setState({
      redirect: true,
    });
  };

  renderRedirect = () => {
    if (this.state.redirect) {
      return <Redirect to="/auth/login" />;
    }
  };

  getData = () => {
    if (this.state.interval != null) {
      clearInterval(this.state.interval);
    }

    if (this.state.accessToken == null) {
      this.setRedirect();
      return;
    }

    this.setState({ isLoading: true });

    //Admins
    //var url = global.server_api + 'api/Planejador/filtro/';
    var url = global.server_api_new + global.apiToken + "/Planejador/filtro/";

    //Planners
    if (this.state.planejadorId != null && this.state.empresaId != null)
      //url = global.server_api + 'api/Planejador/empresa/' + this.state.empresaId + "/filtro/";
      url =
        global.server_api_new +
        global.apiToken +
        "/Planejador/empresa/" +
        this.state.empresaId +
        "/filtro/";

    var filtro = {
      Filtro: this.state.textoDigitado,
      Ordenacao: this.state.sortField != null ? this.state.sortField : "Nome",
      NumeroPagina:
        this.state.numeroPagina != null ? this.state.numeroPagina : 1,
      NumeroItensPorPagina:
        this.state.numeroItensPorPagina != null
          ? this.state.numeroItensPorPagina
          : 10,
    };

    var config = {
      headers: {
        Authorization: "bearer " + this.state.accessToken,
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Headers": "Authorization",
        "Access-Control-Allow-Methods":
          "GET, POST, OPTIONS, PUT, PATCH, DELETE",
      },
    };

    axios.post(url, filtro, config).then((res) => {
      var data = [];

      if (res.data.results != null) {
        res.data.results.forEach((item) => {
          var temp = {
            id: item.id,
            nome: item.nome,
            sobrenome: item.sobrenome,
            documento: item.documento,
            estadoCivil: item.estadoCivil,
            celular: item.celular,
            telefone: item.telefone,
            endereco: item.endereco,
            complemento: item.complemento,
            pais: item.pais,
            cidade: item.cidade,
            estado: global.estados[item.estado],
            cep: item.cep,
            tipo: 1, //0 = Admin , 1 = Planejador, 2 = Pessoa
            usuario: item.usuario,
          };

          data.push(temp);
        });
      }

      const total = res.data.totalResults;
      this.setState({ data: data, isLoading: false, count: total });
    });
  };

  filtroPorTexto = (valorDigitado) => {
    const textoDigitado = valorDigitado;

    this.setState({ textoDigitado }, function () {
      if (this.state.interval != null) {
        clearInterval(this.state.interval);
      }

      const $this = this;

      const interval = setInterval(function () {
        console.log($this.state.textoDigitado);
        $this.getData();
      }, 1000);

      this.setState({ interval });
    });
  };

  editarRegistro = (rowData) => {
    var id = rowData[0];

    if (id == null) return;

    var items = this.state.data.filter(function (temp) {
      return temp.id === id;
    });

    if (items != null && items.length > 0) {
      var item = items[0];

      console.log(
        "email",
        item.usuario != null ? item.usuario.email : "",
        item
      );

      //Seta item edicao
      this.setState(
        {
          id: item.id,
          nome: item.nome,
          sobrenome: item.sobrenome,
          documento: item.documento,
          estadoCivil: item.estadoCivil,
          celular: item.celular,
          telefone: item.telefone,
          cep: item.cep,
          email: item.usuario != null ? item.usuario.email : "",
          tipo: 1,
          editando: true,
        },
        function () {
          this.setState(
            {
              endereco: item.endereco,
              complemento: item.complemento,
              pais: item.pais != null ? item.pais : "Brasil",
              cidade: item.cidade,
              estado: item.estado,
            },
            function () {
              //Abre modal
              global.showModal(this.modal);
            }
          );
        }
      );
    }
  };

  salvar = () => {
    var currentScroll = [
      document.documentElement.scrollLeft || document.body.scrollLeft,
      document.documentElement.scrollTop || document.body.scrollTop,
    ];

    global.spinnerShow($);

    var usuarioDto = {
      Senha: this.state.senha != null ? this.state.senha : "",
      NovaSenha: this.state.senha != null ? this.state.senha : "",
      Email: this.state.email,
      Tipo: 1, //0 = Admin , 1 = Planejador, 2 = Pessoa
    };

    var item = {
      Id: this.state.id,
      Nome: this.state.nome,
      Sobrenome: this.state.sobrenome,
      Documento: this.state.documento,
      EstadoCivil: this.state.estadoCivil,
      Endereco: this.state.endereco,
      Complemento: this.state.complemento,
      Pais: this.state.pais,
      Cidade: this.state.cidade,
      Estado: this.state.estado,
      CEP: this.state.cep,
      Celular: this.state.celular,
      Telefone: this.state.telefone,
      UsuarioDTO: usuarioDto,
      FamiliaId: this.state.familiaId,
      EmpresaId: this.state.empresaId,
      PlanejadorId: this.state.planejadorId,
    };

    //var url = global.server_api + 'api/Planejador/' + ((this.state.id != null) ? this.state.id : "");
    var url =
      global.server_api_new +
      global.apiToken +
      "/Planejador/" +
      (this.state.id != null ? this.state.id : "");

    var config = {
      headers: {
        Authorization: "bearer " + this.state.accessToken,
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Headers": "Authorization",
        "Access-Control-Allow-Methods":
          "GET, POST, OPTIONS, PUT, PATCH, DELETE",
      },
    };

    axios
      .post(url, item, config)
      .then((res) => {
        global.spinnerHide($, currentScroll);

        if (res.data.success === true) {
          this.limparSelecao();
          window.location.reload();
        } else {
          console.log(res.data.exception);
          const options = {
            title: "Erro ao salvar planejador",
            message: res.data.exception.Message,
            buttons: [
              {
                label: "Ok",
              },
            ],
          };

          confirmAlert(options);
        }
      })
      .catch((error) => {
        global.spinnerHide($, currentScroll);
        global.logarErroDeRequisicao(error);
      });
  };

  limparSelecao = () => {
    this.setState({
      id: null,
      nome: null,
      sobrenome: null,
      documento: null,
      estadoCivil: null,
      endereco: null,
      complemento: null,
      cidade: null,
      estado: null,
      cep: null,
      editando: null,
      celular: null,
      telefone: null,
      nomeUsuario: null,
      senha: null,
      email: null,
      tipo: null,
    });
  };

  removerRegistros = (rowsDeleted) => {
    const options = {
      title: "Exclusão de Planejador",
      message:
        "Tem certeza que deseja excluir? Este procedimento é irreversível!",
      buttons: [
        {
          label: "Sim",
          onClick: () => {
            //console.log(rowsDeleted.data);
            var ids = [];

            for (var key in rowsDeleted.data) {
              ids.push(this.state.data[rowsDeleted.data[key].dataIndex].id);
            }

            if (ids.length > 0) {
              //var url = global.server_api + 'api/Planejador/bulk/' + ids.join(",");
              var url =
                global.server_api_new +
                global.apiToken +
                "/Planejador/bulk/" +
                ids.join(",");

              var config = {
                headers: {
                  Authorization: "bearer " + this.state.accessToken,
                  "Access-Control-Allow-Origin": "*",
                  "Access-Control-Allow-Headers": "Authorization",
                  "Access-Control-Allow-Methods":
                    "GET, POST, OPTIONS, PUT, PATCH, DELETE",
                },
              };

              axios.delete(url, {}, config).then((res) => {
                const result = res.data;

                if (result.success && result.affectedResults > 0) {
                  this.getData();
                } else {
                  alert("Erro ao remover. " + res.data.exception.Message);
                  console.log(res.data.exception);
                }
              });
            }
          },
        },
        {
          label: "Não",
        },
      ],
    };

    confirmAlert(options);

    return false;
  };

  _onChangeSelection = (selectedOption) => {
    var valor = selectedOption.value;
    this.setState({ estadoCivil: valor });
  };

  _onChange = (e) => {
    var nomeCampo = e.target.name;

    console.log(nomeCampo, e.target.value);

    if (e.target.value.indexOf("_") > -1) {
      return;
    }

    this.setState({ [nomeCampo]: e.target.value });

    if (nomeCampo === "cep") {
      var cep = e.target.value;
      cep = cep.replace(".", "").replace("-", "");

      var cepUrl = "https://viacep.com.br/ws/" + cep + "/json";

      axios.get(cepUrl).then((res) => {
        if (nomeCampo === "cep") {
          this.atualizarDadosEndereco(res.data);
        }
      });
    }
  };

  atualizarDadosEndereco = (json) => {
    if (json != null) {
      this.setState({
        endereco: json.logradouro,
        cidade: json.localidade,
        estado: json.uf,
      });
    }
  };

  montarDropDown = (itens, id, changeMethod, selectedValue, type) => (
    <Select
      id={id}
      onChange={changeMethod}
      className="select-component"
      options={itens}
      defaultValue={itens != null ? itens[0] : ""}
      placeholder="Selecione..."
      value={
        itens != null && selectedValue != null
          ? itens.find((op) => {
              if (type === "enum") {
                return (
                  (op.value != null ? op.value.toString() : null) ===
                  (selectedValue != null ? selectedValue.toString() : null)
                );
              } else if (op.value != null && selectedValue != null) {
                return op.value.id === selectedValue.id;
              }
              return false;
            })
          : null
      }
    />
  );

  mostrarPopup = () => (
    <div>
      <SkyLight
        ref={(ref) => (this.modal = ref)}
        transitionDuration={0}
        beforeOpen={this.preCarregarParaEdicao}
        afterClose={this.limparSelecao}
        title={
          <>
            <div className="pop-up-title">
              <i className="ni ni-check-bold"></i> <h2>Edição do Planejador</h2>
            </div>
          </>
        }
      >
        <Row>
          <Col lg="4" xl="4">
            <div>
              <label>Nome *</label>
              <Input
                type="text"
                value={this.state.nome}
                onChange={(e) => this.setState({ nome: e.target.value })}
              />
            </div>
          </Col>
          <Col lg="4" xl="4">
            <div>
              <label>Sobrenome *</label>
              <Input
                type="text"
                value={this.state.sobrenome}
                onChange={(e) => this.setState({ sobrenome: e.target.value })}
              />
            </div>
          </Col>
          <Col lg="4" xl="4">
            <div>
              <label>CPF *</label>
              <MaskedInput
                name="documento"
                className="form-control"
                mask={this.mascaras.cpf}
                value={this.state.documento}
                onChange={this._onChange}
              />
            </div>
          </Col>
          <Col lg="4" xl="4">
            <div>
              <label>Estado Civil *</label>
              <Select
                className="select-component"
                value={this.estadoCivil.find((item) => {
                  return item.value === this.state.estadoCivil;
                })}
                onChange={this._onChangeSelection}
                options={this.estadoCivil}
              />
            </div>
          </Col>
          <Col lg="4" xl="4">
            <div>
              <label>Celular *</label>
              <MaskedInput
                name="celular"
                className="form-control"
                mask={this.mascaras.celular}
                value={this.state.celular}
                onChange={this._onChange}
              />
            </div>
          </Col>
          <Col lg="4" xl="4">
            <div>
              <label>Telefone</label>
              <MaskedInput
                name="telefone"
                className="form-control"
                mask={this.mascaras.telefone}
                value={this.state.telefone}
                onChange={this._onChange}
              />
            </div>
          </Col>
          <Col lg="4" xl="4">
            <div>
              <label>CEP *</label>
              <MaskedInput
                name="cep"
                className="form-control"
                value={this.state.cep}
                mask="11.111-111"
                onChange={this._onChange}
              />
            </div>
          </Col>
          <Col lg="4" xl="4">
            <div>
              <label>Estado *</label>
              <Input
                type="text"
                value={this.state.estado}
                onChange={(e) => this.setState({ estado: e.target.value })}
              />
            </div>
          </Col>
          <Col lg="4" xl="4">
            <div>
              <label>Cidade *</label>
              <Input
                type="text"
                value={this.state.cidade}
                onChange={(e) => this.setState({ cidade: e.target.value })}
              />
            </div>
          </Col>
          <Col lg="4" xl="4">
            <div>
              <label>Endereço *</label>
              <Input
                type="text"
                value={this.state.endereco}
                onChange={(e) => this.setState({ endereco: e.target.value })}
              />
            </div>
          </Col>
          <Col lg="4" xl="4">
            <div>
              <label>Complemento</label>
              <Input
                type="text"
                value={this.state.complemento}
                onChange={(e) => this.setState({ complemento: e.target.value })}
              />
            </div>
          </Col>
          <Col lg="4" xl="4">
            <div>
              <label>E-mail *</label>
              <Input
                type="email"
                value={this.state.email}
                onChange={(e) => this.setState({ email: e.target.value })}
                autocomplete="new-password"
              />
            </div>
          </Col>
          <Col lg="4" xl="4">
            <div>
              <label>Senha *</label>
              <Input
                type="password"
                value={this.state.senha}
                onChange={(e) => this.setState({ senha: e.target.value })}
                autocomplete="new-password"
              />
            </div>
          </Col>
          <Col lg="12" xl="12">
            <button
              id="salvar-lancamento"
              className="featured-button"
              onClick={() => this.salvar()}
              style={{
                backgroundImage:
                  "url(" +
                  require("../../assets/img/theme/botao-destaque.png") +
                  ")",
              }}
            >
              Salvar
            </button>
          </Col>
        </Row>
      </SkyLight>
    </div>
  );

  render() {
    const columns = [
      {
        name: "id",
        options: {
          display: false,
          viewColumns: false,
          filter: false,
        },
      },
      {
        name: "nome",
        label: "Nome",
        options: {
          display: true,
          viewColumns: true,
          filter: false,
        },
      },
      {
        name: "sobrenome",
        label: "Sobrenome",
        options: {
          display: true,
          viewColumns: true,
          filter: false,
        },
      },
      {
        name: "documento",
        label: "CPF",
        options: {
          display: true,
          viewColumns: true,
          filter: false,
        },
      },
      {
        name: "estadoCivil",
        label: "Estado Civil",
        options: {
          display: true,
          viewColumns: true,
          filter: false,
          customBodyRender: (value) => {
            if (value != null) {
              return <>{global.estadoCivil[value]}</>;
            }
            return <></>;
          },
        },
      },
      {
        name: "celular",
        label: "Celular",
        options: {
          display: true,
          viewColumns: true,
          filter: false,
        },
      },
      {
        name: "telefone",
        label: "Telefone",
        options: {
          display: false,
          viewColumns: true,
          filter: false,
        },
      },
      {
        name: "pais",
        label: "Pais",
        options: {
          display: false,
          viewColumns: true,
          filter: false,
        },
      },
      {
        name: "estado",
        label: "Estado",
        options: {
          display: false,
          viewColumns: true,
          filter: false,
        },
      },
      {
        name: "cidade",
        label: "Cidade",
        options: {
          display: false,
          viewColumns: true,
          filter: false,
        },
      },
      {
        name: "endereco",
        label: "Endereço",
        options: {
          display: false,
          viewColumns: true,
          filter: false,
        },
      },
      {
        name: "complemento",
        label: "Complemento",
        options: {
          display: false,
          viewColumns: true,
          filter: false,
        },
      },
      {
        name: "cep",
        label: "CEP",
        options: {
          display: false,
          viewColumns: true,
          filter: false,
        },
      },
    ];

    columns.forEach((item) => {
      if (item.name === this.state.colunaOrdenacao) {
        item.options.sortDirection = this.state.colunaOrdenacao;
      }
    });
    const { data, page, count, isLoading } = this.state;

    const options = {
      textLabels: global.textLabels,
      filter: true,
      filterType: "dropdown",
      responsive: "stacked",
      print: false,
      download: false,
      serverSide: true,
      searchText: this.state.textoDigitado,
      count: count,
      page: page,
      sort: true,
      selectableRows: "multiple",
      //selectableRowsOnClick: true,
      rowsPerPage: 15,
      onRowsDelete: (rowsDeleted) => {
        this.removerRegistros(rowsDeleted);
        return false;
      },
      onRowClick: (rowData, rowMeta) => {
        this.editarRegistro(rowData);
      },
      onFilterChange: (changedColumn, filterList) => {
        console.log("onFilterChange");
      },
      onColumnSortChange: (changedColumn) => {
        //ASC
        let order = "";
        let direcaoOrdenacao = "asc";

        if (this.state.colunaOrdenacao === changedColumn) {
          if (
            this.state.direcaoOrdenacao != null &&
            this.state.direcaoOrdenacao === "asc"
          ) {
            order = " Desc";
            direcaoOrdenacao = "desc";
          }
        }

        console.log(changedColumn + order);

        this.setState(
          {
            sortField: changedColumn + order,
            colunaOrdenacao: changedColumn,
            direcaoOrdenacao: direcaoOrdenacao,
          },
          () => {
            this.getData();
          }
        );
      },
      onTableChange: (action, tableState) => {
        switch (action) {
          case "changePage":
            this.setState({ numeroPagina: tableState.page + 1 }, () => {
              this.getData();
            });
            break;
          case "search":
            this.filtroPorTexto(tableState.searchText);
            break;
          default:
            break;
        }
      },
    };

    return (
      <div>
        <Card>
          <CardHeader className="border-0">
            <Row className="align-items-center">
              <div className="col">
                <img
                  src={require("../../assets/img/theme/icone-calendario.png")}
                  alt="Meios de Pagamento"
                  className="before-title-img"
                />
                <h3 className="mb-0 chart-title">
                  {/*Planejadores*/}
                  Perfil do Planejador
                </h3>
              </div>
              {/*
                        <div className="new-entry" style={{float: "right"}}>
                            <div className="nav-link-icon featured-button" style={{backgroundImage: "url(" + require("../../assets/img/theme/botao-destaque.png") + ")"}} onClick={() => global.showModal(this.modal)}>
                                <span className="nav-link-inner--text">Novo Planejador</span>                
                            </div>
                        </div>
                        */}
            </Row>
          </CardHeader>
        </Card>
        <MuiThemeProvider theme={this.getMuiTheme()}>
          <MUIDataTable
            id="orcamento-table"
            title={
              <Typography>
                {isLoading && (
                  <CircularProgress
                    size={24}
                    style={{ marginLeft: 15, position: "relative", top: 4 }}
                  />
                )}
              </Typography>
            }
            data={data}
            columns={columns}
            options={options}
          />
        </MuiThemeProvider>
        {this.mostrarPopup()}
        {this.renderRedirect()}
      </div>
    );
  }
}

export default Planejadores;
