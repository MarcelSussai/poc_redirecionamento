import React from "react";
import Datepicker, { registerLocale } from "react-datepicker";
import br from "date-fns/locale/pt-BR";
import "react-datepicker/dist/react-datepicker.css";
import MaskedInput from "react-maskedinput";
import { Link, Redirect } from "react-router-dom";
import ApexChart from "react-apexcharts";
import { CircularProgress } from "@material-ui/core";
import MUIDataTable from "mui-datatables";
import TableRow from "@material-ui/core/TableRow";
import TableCell from "@material-ui/core/TableCell";
import axios from "axios";
import Rightbar from "../../components/Sidebar/RightBar.jsx";
import $ from "jquery";
import SkyLight from "react-skylight";
import { confirmAlert } from "react-confirm-alert";
import Select from "react-select";
import CurrencyInput from "react-currency-input";
import Switch from "react-switch";
import CustomFooter from "./CustomFooter.jsx";
import { createMuiTheme, MuiThemeProvider } from "@material-ui/core/styles";
import { Col, Row, Card, CardHeader, NavLink, NavItem } from "reactstrap";
import "../../assets/css/tabela-terceiro-nivel.css";
import RestoreDate from "@material-ui/icons/Restore";
import Tooltip from "@material-ui/core/Tooltip";
import { withStyles } from "@material-ui/core/styles";
import Restore3Months from "@material-ui/icons/Looks3Outlined";

registerLocale("br", br);

class OrcamentoGeral extends React.Component {
  constructor(props) {
    super(props);
    this.valorEstimado = React.createRef();
  }

  moment = require("moment");

  filtroPorTipo = {};

  state = {
    accessToken: localStorage.getItem("access-token"),
    empresaId: localStorage.getItem("empresa-id"),
    familiaId: localStorage.getItem("familia-id"),
    planejadorId: localStorage.getItem("planejador-id"),
    tipoUsuario: localStorage.getItem("tipo-usuario"),
    labelFamilia:
      parseInt(localStorage.getItem("tipo-usuario")) === 2
        ? "família"
        : "cliente",
    labelFamiliaMaiusculo:
      parseInt(localStorage.getItem("tipo-usuario")) === 2
        ? "Família"
        : "Cliente",
    editarOrcamento: false,
    cadastrarOrcamento: false,
    cadastrarCategoria: false,
    page: 0,
    count: 1,
    data: [["Carregando Dados..."]],
    isLoading: false,
    renderExpadable: false,
    tiposOrcamento: [],
    frequencias: [],
    grupos: [],
    orcamentos: [],
    categorias: [],
    listaDeOrcamentos: [],
    listaDeCategorias: {},
    listaDeLancamentos: [],
    listaDeGrupos: [],
    listaDeCategoriasNoGrupo: {},
    listaDeOrcamentosComSuspensos: [],
    listaDeCategoriasComSuspensos: {},
    listaDeGruposComSuspensos: [],
    listaDeCategoriasNoGrupoComSuspensos: {},
    listaDeCategoriasSemSuspensos: [],
    suspenso: false,
    mostrarSuspensos: false,
    mostrarAgrupado: false,
    Sumario: {
      Orcamento: {
        ReceitasEstimadas: 0,
        ReceitasRealizadas: 0,
        DespesasEstimadas: 0,
        DespesasRealizadas: 0,
        InvestimentosEstimados: 0,
        InvestimentosRealizados: 0,
        DividasEstimadas: 0,
        DividasRealizadas: 0,
      },
    },
    persistenciaColunas:
      sessionStorage.getItem(btoa("persistenciaColunas")) !== null
        ? JSON.parse(atob(sessionStorage.getItem(btoa("persistenciaColunas"))))
        : {
            OrcamentoCategoria: {
              NomeMeioPagamento: false,
              Frequencia: true,
              Grupo: true,
              Estimado: true,
              TotalMedio: true,
              Quantidade: false,
              Total: true,
              Media: false,
              Minimo: false,
              Maximo: false,
              Suspenso: false,
              PercentualDespesas: false,
            },
            Lancamentos: {
              data: true,
              descricao: true,
              valor: true,
              nomeMeioPagamento: false,
              valorParcela: false,
              agrupamento: false,
              operacao: false,
              tipoOperacao: false,
            },
            Orcamento: {
              Estimado: true,
              TotalMedio: true,
              Quantidade: false,
              Total: true,
              Media: false,
              Minimo: false,
              Maximo: false,
              Suspenso: false,
              PercentualDespesas: false,
            },
            GrupoCategoria: {
              NomeMeioPagamento: false,
              Frequencia: true,
              Estimado: true,
              TotalMedio: true,
              OrcamentoSuspenso: false,
              Quantidade: false,
              Total: true,
              Media: false,
              Minimo: false,
              Maximo: false,
              PercentualDespesas: false,
            },
            Grupo: {
              Estimado: true,
              TotalMedio: true,
              Quantidade: false,
              Total: true,
              Media: false,
              Minimo: false,
              Maximo: false,
              Suspenso: false,
              PercentualDespesas: false,
            },
            Categoria: {
              NomeMeioPagamento: false,
              Frequencia: true,
              Grupo: true,
              Estimado: true,
              TotalMedio: true,
              CategoriaSuspenso: false,
              Quantidade: false,
              Total: true,
              Media: false,
              Minimo: false,
              Maximo: false,
              PercentualDespesas: false,
            },
          },
    persistenciaLinhas:
      sessionStorage.getItem(btoa("persistenciaLinhas")) !== null
        ? JSON.parse(atob(sessionStorage.getItem(btoa("persistenciaLinhas"))))
        : {
            OrcamentoCategoria: [],
            Lancamentos: [],
            Orcamento: [],
            GrupoCategoria: [],
            Grupo: [],
            Categoria: [],
          },
    startDate: localStorage.getItem("dataInicio")
      ? new Date(JSON.parse(localStorage.getItem("dataInicio")))
      : this.moment().startOf("month").toDate(),
    endDate: localStorage.getItem("dataFim")
      ? new Date(JSON.parse(localStorage.getItem("dataFim")))
      : this.moment().endOf("month").toDate(),
  };

  selecaoDeAgrupamento = [
    { value: 0, label: "Visão dos Orçamentos" },
    { value: 1, label: "Visão dos Grupos" },
    { value: 2, label: "Visão das Categorias" },
  ];

  timeouts = {};
  timeoutUpdateData = null;

  getMuiTheme = () =>
    createMuiTheme({
      overrides: {
        MUIDataTableBodyCell: {
          root: { padding: 0 },
        },
        MuiTableRow: {
          root: {
            "&:nth-of-type(odd)": {
              backgroundColor: "#f8f9fe",
            },
            "& input": {
              background: "transparent",
            },
          },
        },
        MUIDataTableSelectCell: {
          fixedHeaderCommon: {
            backgroundColor: "unset",
          },
        },
        MuiTableCell: {
          root: {
            fontFamily: "unset",
            borderTop: "none",
            padding: "16px 10px",
          },
          footer: {
            color: "#FFF",
            fontWeight: 700,
          },
        },
        MUIDataTableHeadCell: {
          fixedHeaderYAxis: {
            fontWeight: 700,
          },
          fixedHeaderCommon: {
            zIndex: 0,
          },
        },
        MuiPaper: {
          root: {
            overflow: "hidden",
            margin: 0,
          },
        },
        MuiToolbar: {
          regular: {
            backgroundColor: "#e4eaf4",
          },
        },
      },
    });

  componentDidMount() {
    var grupos = [{ value: null, label: "Selecione..." }];

    global.grupoDeCategoria.forEach(function (value, index) {
      grupos.push({
        value: index,
        label: value,
      });
    });

    var frequencias = [{ value: null, label: "Selecione..." }];

    global.frequenciaCategoria.forEach(function (value, index) {
      frequencias.push({
        value: index,
        label: value,
      });
    });

    var tiposOrcamento = [{ value: null, label: "Selecione..." }];

    global.tipoOrcamento.forEach(function (value, index) {
      tiposOrcamento.push({
        value: index,
        label: value,
      });
    });

    var tiposOrcamentoMenorQueDois = tiposOrcamento.filter(function (item) {
      return item.value < 2;
    });

    const input = document.querySelector(".moeda");

    if (input) {
      input.addEventListener("keyup", function (e) {
        global.maskCurrency(e, this);
      });
    }

    global.mostrarAjudaDaPaginaAutomaticamente(this);

    this.setState(
      {
        tipoOrcamento: 1,
        agrupamentoSelecionado: 0,
        grupos: grupos,
        frequencias: frequencias,
        tiposOrcamento: tiposOrcamento,
        tiposOrcamentoMenorQueDois: tiposOrcamentoMenorQueDois,
      },
      function () {
        var $this = this;

        this.setStartDate(this.state.startDate, function () {
          $this.setEndDate($this.state.endDate, function () {
            $this.getData(false);
            $this.atualizarMeiosPagamento();
            $this.atualizarCategorias();
            $this.atualizarOrcamentos();
            $this.atualizarOrcamentosPadrao();
          });
        });
      }
    );
  }

  buscaOrcamentoNaLista = (array, orcamentoId) => {
    var indexFiltrado = null;

    array.filter(function (arrItem, index) {
      if (arrItem.OrcamentoId === orcamentoId) {
        indexFiltrado = index;
        return true;
      }
      return false;
    });

    return indexFiltrado;
  };

  buscaGrupoNaLista = (array, grupo) => {
    var indexFiltrado = null;

    array.filter(function (arrItem, index) {
      if (arrItem.GrupoId === grupo) {
        indexFiltrado = index;
        return true;
      }
      return false;
    });

    return indexFiltrado;
  };

  getData = (notLoading) => {
    //console.log("Go go go!!");

    var familiaId = this.state.familiaId;

    if (familiaId == null) {
      familiaId = localStorage.getItem("familia-id");
    }

    if (this.state.accessToken == null) {
      this.setRedirect();
      return;
    }

    if (this.state.interval != null) {
      clearInterval(this.state.interval);
    }

    if (notLoading !== true) {
      this.setState({ isLoading: true });
    }

    //var url = global.server_api + 'api/dashboard/orcamento-no-periodo/' + familiaId;
    var url =
      global.server_api_new +
      global.apiToken +
      "/dashboard/orcamento-no-periodo/" +
      familiaId;

    var filtro = {
      Filtro: this.state.textoDigitado,
      Ordenacao:
        this.state.sortField != null ? this.state.sortField : "Data Desc",
      NumeroPagina:
        this.state.numeroPagina != null ? this.state.numeroPagina : null,
      NumeroItensPorPagina:
        this.state.numeroItensPorPagina != null
          ? this.state.numeroItensPorPagina
          : null,
      DataInicial: this.state.startDate,
      DataFinal: this.state.endDate,
      CategoriaId:
        this.state.categoriaFiltro != null
          ? this.state.categoriaFiltro.id
          : null,
      OrcamentoId:
        this.state.orcamentoFiltro != null
          ? this.state.orcamentoFiltro.id
          : null,
      Grupo:
        this.state.grupoFiltro != null ? this.state.grupoFiltro.value : null,
      Frequencia:
        this.state.frequenciaFiltro != null
          ? this.state.frequenciaFiltro.value
          : null,
      Suspenso: true /*SEMPRE trazer os suspensos e tratar internamente no JSX*/,
    };

    var config = {
      headers: {
        Authorization: "bearer " + this.state.accessToken,
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Headers": "Authorization",
        "Access-Control-Allow-Methods":
          "GET, POST, OPTIONS, PUT, PATCH, DELETE",
      },
    };

    var currentScroll = [
      document.documentElement.scrollLeft || document.body.scrollLeft,
      document.documentElement.scrollTop || document.body.scrollTop,
    ];

    if (notLoading !== true) {
      global.spinnerShow($);
    }

    var $this = this;

    axios.post(url, filtro, config).then((res) => {
      var listaDeOrcamentos = [];
      var listaDeCategorias = {};
      var listaDeGrupos = [];
      var listaDeCategoriasNoGrupo = {};

      var listaDeOrcamentosComSuspensos = [];
      var listaDeCategoriasComSuspensos = {};
      var listaDeGruposComSuspensos = [];
      var listaDeCategoriasNoGrupoComSuspensos = {};

      var listaDeCategoriasSemSuspensos = [];
      var listaDeCategoriasOriginal = [];

      var totalDespesaEstimado = 0;
      var totalDespesaRealizado = 0;
      var totalDespesaEstimadoComSuspensos = 0;
      var totalDespesaRealizadoComSuspensos = 0;

      var totalReceitaEstimado = 0;
      var totalReceitaRealizado = 0;
      var totalReceitaEstimadoComSuspensos = 0;
      var totalReceitaRealizadoComSuspensos = 0;

      var totalInvestimentosEstimado = 0;
      var totalInvestimentosRealizado = 0;
      var totalInvestimentosEstimadoComSuspensos = 0;
      var totalInvestimentosRealizadoComSuspensos = 0;

      var totalDividasEstimado = 0;
      var totalDividasRealizado = 0;
      var totalDividasEstimadoComSuspensos = 0;
      var totalDividasRealizadoComSuspensos = 0;

      if (res.data.results != null) {
        res.data.results.forEach(function (item) {
          var orcamentoId = item.orcamentoId;
          var categoriaId = item.categoriaId;

          var orcamentoSuspenso = item.orcamentoSuspenso === 1;
          var categoriaSuspenso = item.categoriaSuspenso === 1;

          item.estimadoAnual =
            item.frequencia === 1 ? item.estimado * 12 : item.estimado;

          // if(item.frequencia === 1){
          //     console.log(item);
          // }

          if (item.tipoOrcamento === 0) {
            totalReceitaEstimadoComSuspensos += item.estimado;
            totalReceitaRealizadoComSuspensos += item.totalMedio;
          }

          if (item.tipoOrcamento === 1) {
            totalDespesaEstimadoComSuspensos += item.estimado;
            totalDespesaRealizadoComSuspensos += item.totalMedio;
          }

          if (item.tipoOrcamento === 2) {
            totalInvestimentosEstimadoComSuspensos += item.estimado;
            totalInvestimentosRealizadoComSuspensos += item.totalMedio;
          }

          if (item.tipoOrcamento === 3) {
            totalDividasEstimadoComSuspensos += item.estimado;
            totalDividasRealizadoComSuspensos += item.totalMedio;
          }

          if (orcamentoSuspenso === false) {
            if (!categoriaSuspenso) {
              if (item.tipoOrcamento === 0) {
                totalReceitaEstimado += item.estimado;
                totalReceitaRealizado += item.totalMedio;
              }

              if (item.tipoOrcamento === 1) {
                totalDespesaEstimado += item.estimado;
                totalDespesaRealizado += item.totalMedio;
              }

              if (item.tipoOrcamento === 2) {
                totalInvestimentosEstimado += item.estimado;
                totalInvestimentosRealizado += item.totalMedio;
              }

              if (item.tipoOrcamento === 3) {
                totalDividasEstimado += item.estimado;
                totalDividasRealizado += item.totalMedio;
              }
            }
          }

          var indexOrcamento = null;

          //INICIO - Orçamento sem suspenso
          if (!orcamentoSuspenso) {
            indexOrcamento = $this.buscaOrcamentoNaLista(
              listaDeOrcamentos,
              orcamentoId
            );

            //Se orçamento nao suspenso
            if (indexOrcamento == null) {
              listaDeOrcamentos.push({
                OrcamentoId: item.orcamentoId,
                CategoriaId: item.categoriaId,
                NomeOrcamento: item.nomeOrcamento,
                Estimado:
                  !categoriaSuspenso &&
                  item.estimado != null &&
                  item.estimado !== ""
                    ? item.estimado
                    : 0,
                TotalMedio:
                  !categoriaSuspenso &&
                  item.totalMedio != null &&
                  item.totalMedio !== ""
                    ? item.totalMedio
                    : 0,
                Suspenso:
                  item.orcamentoSuspenso != null ? item.orcamentoSuspenso : 0,
                TipoOrcamento: item.tipoOrcamento,
                Total:
                  !categoriaSuspenso && item.total != null ? item.total : 0,
                Media:
                  !categoriaSuspenso && item.media != null ? item.media : 0,
                Minimo:
                  !categoriaSuspenso && item.minimo != null ? item.minimo : 0,
                Maximo:
                  !categoriaSuspenso && item.maximo != null ? item.maximo : 0,
                Quantidade:
                  !categoriaSuspenso && item.quantidade != null
                    ? item.quantidade
                    : 0,
                PercentualDespesas: 0,
              });

              listaDeCategorias[orcamentoId] = [];
            }
            //Se não, se categoria nao suspenso, acrescental no estimado e realizado
            else if (!categoriaSuspenso) {
              listaDeOrcamentos[indexOrcamento].Estimado +=
                item.estimado != null &&
                item.estimado !== "" &&
                item.estimado > 0
                  ? item.estimado
                  : 0;
              listaDeOrcamentos[indexOrcamento].TotalMedio +=
                item.totalMedio != null && item.totalMedio !== ""
                  ? item.totalMedio
                  : 0;

              listaDeOrcamentos[indexOrcamento].Total +=
                item.total != null ? item.total : 0;
              listaDeOrcamentos[indexOrcamento].Quantidade +=
                item.quantidade != null ? item.quantidade : 0;

              if (
                item.minimo != null &&
                item.minimo < listaDeOrcamentos[indexOrcamento].Minimo
              ) {
                listaDeOrcamentos[indexOrcamento].Minimo = item.minimo;
              }

              if (
                item.maximo != null &&
                item.maximo > listaDeOrcamentos[indexOrcamento].Maximo
              ) {
                listaDeOrcamentos[indexOrcamento].Maximo = item.maximo;
              }

              if (item.media != null) {
                var mediaAnterior = listaDeOrcamentos[indexOrcamento].Media;
                listaDeOrcamentos[indexOrcamento].Media =
                  (mediaAnterior + item.media) / 2;
              }
            }
          }
          //FIM - Orçamento sem suspenso

          //INICIO - Orçamento com suspenso
          indexOrcamento = $this.buscaOrcamentoNaLista(
            listaDeOrcamentosComSuspensos,
            orcamentoId
          );

          if (indexOrcamento == null) {
            listaDeOrcamentosComSuspensos.push({
              OrcamentoId: item.orcamentoId,
              CategoriaId: item.categoriaId,
              NomeOrcamento: item.nomeOrcamento,
              Estimado:
                item.estimado != null && item.estimado !== ""
                  ? item.estimado
                  : 0,
              TotalMedio:
                item.totalMedio != null && item.totalMedio !== ""
                  ? item.totalMedio
                  : 0,
              Suspenso:
                item.orcamentoSuspenso != null ? item.orcamentoSuspenso : 0,
              TipoOrcamento: item.tipoOrcamento,
              Total: item.total != null ? item.total : 0,
              Media: item.media != null ? item.media : 0,
              Minimo: item.minimo != null ? item.minimo : 0,
              Maximo: item.maximo != null ? item.maximo : 0,
              Quantidade: item.quantidade != null ? item.quantidade : 0,
              PercentualDespesas: 0,
            });

            listaDeCategoriasComSuspensos[orcamentoId] = [];
          } else {
            listaDeOrcamentosComSuspensos[indexOrcamento].Estimado +=
              item.estimado != null && item.estimado !== "" ? item.estimado : 0;
            listaDeOrcamentosComSuspensos[indexOrcamento].TotalMedio +=
              item.totalMedio != null && item.totalMedio !== ""
                ? item.totalMedio
                : 0;

            listaDeOrcamentosComSuspensos[indexOrcamento].Total +=
              item.total != null ? item.total : 0;
            listaDeOrcamentosComSuspensos[indexOrcamento].Quantidade +=
              item.quantidade != null ? item.quantidade : 0;

            if (
              item.minimo != null &&
              item.minimo < listaDeOrcamentosComSuspensos[indexOrcamento].Minimo
            ) {
              listaDeOrcamentosComSuspensos[indexOrcamento].Minimo =
                item.minimo;
            }

            if (
              item.maximo != null &&
              item.maximo > listaDeOrcamentosComSuspensos[indexOrcamento].Maximo
            ) {
              listaDeOrcamentosComSuspensos[indexOrcamento].Maximo =
                item.maximo;
            }

            if (item.media != null) {
              mediaAnterior =
                listaDeOrcamentosComSuspensos[indexOrcamento].Media;
              listaDeOrcamentosComSuspensos[indexOrcamento].Media =
                (mediaAnterior + item.media) / 2;
            }
          }
          //FIM - Orçamento com suspenso

          //INICIO - Grupo sem suspenso
          if (!orcamentoSuspenso) {
            indexOrcamento = $this.buscaGrupoNaLista(listaDeGrupos, item.grupo);

            //Se orçamento nao suspenso
            if (indexOrcamento == null) {
              listaDeGrupos.push({
                OrcamentoId: item.orcamentoId,
                CategoriaId: item.categoriaId,
                GrupoId: item.grupo,
                Grupo: $this.mapearGrupoPeloId(item.grupo),
                Estimado:
                  item.estimado != null && item.estimado !== ""
                    ? item.estimado
                    : 0,
                TotalMedio:
                  item.totalMedio != null && item.totalMedio !== ""
                    ? item.totalMedio
                    : 0,
                Suspenso: 0,
                TipoOrcamento: item.tipoOrcamento,
                Total: item.total != null ? item.total : 0,
                Media: item.media != null ? item.media : 0,
                Minimo: item.minimo != null ? item.minimo : 0,
                Maximo: item.maximo != null ? item.maximo : 0,
                Quantidade: item.quantidade != null ? item.quantidade : 0,
                PercentualDespesas: 0,
              });

              listaDeCategoriasNoGrupo[item.grupo] = [];
            }
            //Se não, se categoria nao suspenso, acrescental no estimado e realizado
            else if (!categoriaSuspenso) {
              listaDeGrupos[indexOrcamento].Estimado +=
                item.estimado != null && item.estimado !== ""
                  ? item.estimado
                  : 0;
              listaDeGrupos[indexOrcamento].TotalMedio +=
                item.totalMedio != null && item.totalMedio !== ""
                  ? item.totalMedio
                  : 0;

              listaDeGrupos[indexOrcamento].Total +=
                item.total != null ? item.total : 0;
              listaDeGrupos[indexOrcamento].Quantidade +=
                item.quantidade != null ? item.quantidade : 0;

              if (
                item.minimo != null &&
                item.minimo < listaDeGrupos[indexOrcamento].Minimo
              ) {
                listaDeGrupos[indexOrcamento].Minimo = item.minimo;
              }

              if (
                item.maximo != null &&
                item.maximo > listaDeGrupos[indexOrcamento].Maximo
              ) {
                listaDeGrupos[indexOrcamento].Maximo = item.maximo;
              }

              if (item.media != null) {
                mediaAnterior = listaDeGrupos[indexOrcamento].Media;
                listaDeGrupos[indexOrcamento].Media =
                  (mediaAnterior + item.media) / 2;
              }
            }
          }
          //FIM - Grupo sem suspenso

          //INICIO - Grupo com suspenso
          indexOrcamento = $this.buscaGrupoNaLista(
            listaDeGruposComSuspensos,
            item.grupo
          );

          if (indexOrcamento == null) {
            listaDeGruposComSuspensos.push({
              OrcamentoId: item.orcamentoId,
              CategoriaId: item.categoriaId,
              GrupoId: item.grupo,
              Grupo: $this.mapearGrupoPeloId(item.grupo),
              Estimado:
                item.estimado != null && item.estimado !== ""
                  ? item.estimado
                  : 0,
              TotalMedio:
                item.totalMedio != null && item.totalMedio !== ""
                  ? item.totalMedio
                  : 0,
              Suspenso: 0,
              TipoOrcamento: item.tipoOrcamento,
              Total: item.total != null ? item.total : 0,
              Media: item.media != null ? item.media : 0,
              Minimo: item.minimo != null ? item.minimo : 0,
              Maximo: item.maximo != null ? item.maximo : 0,
              Quantidade: item.quantidade != null ? item.quantidade : 0,
              PercentualDespesas: 0,
            });

            listaDeCategoriasNoGrupoComSuspensos[item.grupo] = [];
          } else {
            listaDeGruposComSuspensos[indexOrcamento].Estimado +=
              item.estimado != null && item.estimado !== "" ? item.estimado : 0;
            listaDeGruposComSuspensos[indexOrcamento].TotalMedio +=
              item.totalMedio != null && item.totalMedio !== ""
                ? item.totalMedio
                : 0;

            listaDeGruposComSuspensos[indexOrcamento].Total +=
              item.total != null ? item.total : 0;
            listaDeGruposComSuspensos[indexOrcamento].Quantidade +=
              item.quantidade != null ? item.quantidade : 0;

            if (
              item.minimo != null &&
              item.minimo < listaDeGruposComSuspensos[indexOrcamento].Minimo
            ) {
              listaDeGruposComSuspensos[indexOrcamento].Minimo = item.minimo;
            }

            if (
              item.maximo != null &&
              item.maximo > listaDeGruposComSuspensos[indexOrcamento].Maximo
            ) {
              listaDeGruposComSuspensos[indexOrcamento].Maximo = item.maximo;
            }

            if (item.media != null) {
              mediaAnterior = listaDeGruposComSuspensos[indexOrcamento].Media;
              listaDeGruposComSuspensos[indexOrcamento].Media =
                (mediaAnterior + item.media) / 2;
            }
          }
          //FIM - Grupo com suspenso

          if (categoriaId != null) {
            var categoria = {
              OrcamentoId: item.orcamentoId,
              CategoriaId: item.categoriaId,
              MeioPagamentoPadraoId: item.meioPagamentoPadraoId,
              GrupoId: item.grupo,
              NomeOrcamento: item.nomeOrcamento,
              NomeCategoria: item.nomeCategoria,
              NomeMeioPagamento: item.nomeMeioPagamento,
              Grupo: $this.mapearGrupoPeloId(item.grupo),
              Frequencia: $this.mapearFrequenciaPeloId(item.frequencia),
              Estimado:
                item.estimadoAnual != null && item.estimadoAnual > 0
                  ? item.estimadoAnual
                  : 0,
              Total: item.total != null ? item.total : 0,
              TotalMedio: item.totalMedio != null ? item.totalMedio : 0,
              Media: item.media != null ? item.media : 0,
              Minimo: item.minimo != null ? item.minimo : 0,
              Maximo: item.maximo != null ? item.maximo : 0,
              Quantidade: item.quantidade != null ? item.quantidade : 0,
              Suspenso:
                item.categoriaSuspenso != null ? item.categoriaSuspenso : 0,
              TipoOrcamento: item.tipoOrcamento,
              OrcamentoSuspenso: item.orcamentoSuspenso,
              PercentualDespesas: 0,
            };

            if (orcamentoSuspenso === false && categoriaSuspenso === false) {
              //Adiciona categoria na lista de orçamentos
              listaDeCategorias[item.orcamentoId].push(categoria);
              listaDeCategoriasNoGrupo[item.grupo].push(categoria);
              listaDeCategoriasSemSuspensos.push(categoria);
            }

            //Adiciona categoria na lista de orçamentos (incluindo suspensos)
            listaDeCategoriasComSuspensos[item.orcamentoId].push(categoria);
            listaDeCategoriasNoGrupoComSuspensos[item.grupo].push(categoria);
            listaDeCategoriasOriginal.push(categoria);
          }
        });
      }

      totalDespesaEstimado =
        "Despesas estimadas: " +
        totalDespesaEstimado
          .toLocaleString("pt-BR", { style: "currency", currency: "BRL" })
          .replace("R$", "");
      totalDespesaEstimadoComSuspensos =
        "Despesas estimadas: " +
        totalDespesaEstimadoComSuspensos
          .toLocaleString("pt-BR", { style: "currency", currency: "BRL" })
          .replace("R$", "");
      totalDespesaRealizado =
        "Despesas realizadas: " +
        totalDespesaRealizado
          .toLocaleString("pt-BR", { style: "currency", currency: "BRL" })
          .replace("R$", "");
      totalDespesaRealizadoComSuspensos =
        "Despesas realizadas: " +
        totalDespesaRealizadoComSuspensos
          .toLocaleString("pt-BR", { style: "currency", currency: "BRL" })
          .replace("R$", "");

      totalReceitaEstimado =
        "Receitas estimadas: " +
        totalReceitaEstimado
          .toLocaleString("pt-BR", { style: "currency", currency: "BRL" })
          .replace("R$", "");
      totalReceitaEstimadoComSuspensos =
        "Receitas estimadas: " +
        totalReceitaEstimadoComSuspensos
          .toLocaleString("pt-BR", { style: "currency", currency: "BRL" })
          .replace("R$", "");
      totalReceitaRealizado =
        "Receitas realizadas: " +
        totalReceitaRealizado
          .toLocaleString("pt-BR", { style: "currency", currency: "BRL" })
          .replace("R$", "");
      totalReceitaRealizadoComSuspensos =
        "Receitas realizadas: " +
        totalReceitaRealizadoComSuspensos
          .toLocaleString("pt-BR", { style: "currency", currency: "BRL" })
          .replace("R$", "");

      totalInvestimentosEstimado =
        "Investimentos estimados: " +
        totalInvestimentosEstimado
          .toLocaleString("pt-BR", { style: "currency", currency: "BRL" })
          .replace("R$", "");
      totalInvestimentosEstimadoComSuspensos =
        "Investimentos estimados: " +
        totalInvestimentosEstimadoComSuspensos
          .toLocaleString("pt-BR", { style: "currency", currency: "BRL" })
          .replace("R$", "");
      totalInvestimentosRealizado =
        "Investimentos realizados: " +
        totalInvestimentosRealizado
          .toLocaleString("pt-BR", { style: "currency", currency: "BRL" })
          .replace("R$", "");
      totalInvestimentosRealizadoComSuspensos =
        "Investimentos realizados: " +
        totalInvestimentosRealizadoComSuspensos
          .toLocaleString("pt-BR", { style: "currency", currency: "BRL" })
          .replace("R$", "");

      totalDividasEstimado =
        "Dividas estimadas: " +
        totalDividasEstimado
          .toLocaleString("pt-BR", { style: "currency", currency: "BRL" })
          .replace("R$", "");
      totalDividasEstimadoComSuspensos =
        "Dividas estimadas: " +
        totalDividasEstimadoComSuspensos
          .toLocaleString("pt-BR", { style: "currency", currency: "BRL" })
          .replace("R$", "");
      totalDividasRealizado =
        "Dividas realizadas: " +
        totalDividasRealizado
          .toLocaleString("pt-BR", { style: "currency", currency: "BRL" })
          .replace("R$", "");
      totalDividasRealizadoComSuspensos =
        "Dividas realizadas: " +
        totalDividasRealizadoComSuspensos
          .toLocaleString("pt-BR", { style: "currency", currency: "BRL" })
          .replace("R$", "");

      var sumario = [
        totalDespesaEstimado,
        totalDespesaRealizado,
        totalDespesaEstimadoComSuspensos,
        totalDespesaRealizadoComSuspensos,
        totalReceitaEstimado,
        totalReceitaRealizado,
        totalReceitaEstimadoComSuspensos,
        totalReceitaRealizadoComSuspensos,
        totalInvestimentosEstimado,
        totalInvestimentosRealizado,
        totalInvestimentosEstimadoComSuspensos,
        totalInvestimentosRealizadoComSuspensos,
        totalDividasEstimado,
        totalDividasRealizado,
        totalDividasEstimadoComSuspensos,
        totalDividasRealizadoComSuspensos,
      ];

      const total = res.data.totalResults;

      $this.setState(
        {
          originalData: listaDeCategoriasOriginal,
          listaDeOrcamentos: listaDeOrcamentos,
          listaDeOrcamentosComSuspensos: listaDeOrcamentosComSuspensos,
          listaDeCategorias: listaDeCategorias,
          listaDeCategoriasComSuspensos: listaDeCategoriasComSuspensos,
          listaDeGrupos: listaDeGrupos,
          listaDeGruposComSuspensos: listaDeGruposComSuspensos,
          listaDeCategoriasNoGrupo: listaDeCategoriasNoGrupo,
          listaDeCategoriasNoGrupoComSuspensos:
            listaDeCategoriasNoGrupoComSuspensos,
          listaDeCategoriasSemSuspensos: listaDeCategoriasSemSuspensos,
          isLoading: false,
          count: total,
          sumario: sumario,
        },
        function () {
          global.spinnerHide($, currentScroll);
          this.getDataLancamentos();
        }
      );

      this.obterPorcentagemDespesaOrcamentos();
      this.obterPorcentagemDespesaGrupos();
      this.obterPorcentagemDespesaCategorias();
    });
  };

  getDataLancamentos = () => {
    if (this.state.interval != null) {
      clearInterval(this.state.interval);
    }

    if (this.state.accessToken === null) {
      this.setRedirect();
      return;
    }

    this.setState({ isLoading: true });

    //var url = global.server_api + 'api/lancamento/familia/' + this.state.familiaId + '/filtro';
    var url =
      global.server_api_new +
      global.apiToken +
      "/lancamento/familia/" +
      this.state.familiaId +
      "/filtro";

    var filtro = {
      CategoriaId:
        this.state.categoryId != null ? this.state.categoryId.id : null,
      MeioPagamentoId:
        this.state.meioPagamentoId != null
          ? this.state.meioPagamentoId.id
          : null,
      OrcamentoId:
        this.state.orcamentoId != null ? this.state.orcamentoId.id : null,
      DataInicial: this.state.startDate,
      DataFinal: this.state.endDate,
      Filtro: this.state.textoDigitado,
      Ordenacao:
        this.state.sortField != null ? this.state.sortField : "Data Desc",
      NumeroPagina:
        this.state.numeroPagina != null ? this.state.numeroPagina : null,
      NumeroItensPorPagina:
        this.state.numeroItensPorPagina != null
          ? this.state.numeroItensPorPagina
          : null,
    };

    var config = {
      headers: {
        Authorization: "bearer " + this.state.accessToken,
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Headers": "Authorization",
        "Access-Control-Allow-Methods":
          "GET, POST, OPTIONS, PUT, PATCH, DELETE",
      },
    };

    axios.post(url, filtro, config).then((res) => {
      var listaDeLancamentos = [];
      var $this = this;

      if (res.data.results != null) {
        //console.log(res.data.results)
        res.data.results.forEach(function (item) {
          var temp = item;

          temp.data = $this.preparaDataParaGrid(item.data);
          temp.tipoOperacao = global.operacao2[item.operacao];

          listaDeLancamentos.push(temp);
        });
      }

      //const total = res.data.totalResults;
      this.setState({
        listaDeLancamentos: listaDeLancamentos,
        isLoading: false,
      });
    });
  };

  atualizarOrcamentos = () => {
    var familiaId = this.state.familiaId;

    if (familiaId == null) {
      familiaId = localStorage.getItem("familia-id");
    }

    //var url = global.server_api + 'api/orcamento/familia/'+familiaId+'/filtro';
    var url =
      global.server_api_new +
      global.apiToken +
      "/orcamento/familia/" +
      familiaId +
      "/filtro";

    var config = {
      headers: {
        Authorization: "bearer " + this.state.accessToken,
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Headers": "Authorization",
        "Access-Control-Allow-Methods":
          "GET, POST, OPTIONS, PUT, PATCH, DELETE",
      },
    };

    axios.post(url, {}, config).then((res) => {
      const orcamentos = res.data.results;

      var orcamentosCombo = [
        {
          label: "Selecione...",
          value: null,
        },
      ];

      if (orcamentos != null) {
        orcamentos.forEach((mp) => {
          orcamentosCombo.push({
            value: mp,
            label: mp.nome,
          });
        });
      }

      this.setState({
        orcamentos: orcamentos,
        orcamentosCombo: orcamentosCombo,
      });
    });
  };

  atualizarOrcamentosPadrao = () => {
    //var url = global.server_api + 'api/orcamento/filtro';
    var url = global.server_api_new + global.apiToken + "/orcamento/filtro";

    var config = {
      headers: {
        Authorization: "bearer " + this.state.accessToken,
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Headers": "Authorization",
        "Access-Control-Allow-Methods":
          "GET, POST, OPTIONS, PUT, PATCH, DELETE",
      },
    };

    let $this = this;

    axios.post(url, {}, config).then((res) => {
      $this.filtroPorTipo = {};

      const orcamentosPadrao = res.data.results;

      var orcamentosPadraoCombo = [
        {
          label: "Selecione...",
          value: null,
        },
      ];

      if (orcamentosPadrao != null) {
        orcamentosPadrao.forEach((mp) => {
          orcamentosPadraoCombo.push({
            value: mp,
            label: mp.nome,
          });

          if ($this.filtroPorTipo[mp.tipoOrcamento] == null) {
            $this.filtroPorTipo[mp.tipoOrcamento] = [];
          }

          $this.filtroPorTipo[mp.tipoOrcamento].push({
            value: mp,
            label: mp.nome,
          });
        });
      }

      this.setState({ orcamentosPadraoCombo: orcamentosPadraoCombo });
    });
  };

  atualizarMeiosPagamento = () => {
    var familiaId = this.state.familiaId;

    if (familiaId == null) {
      familiaId = localStorage.getItem("familia-id");
    }

    //var urlMeioPagamento = global.server_api + 'api/meioPagamento/familia/' + familiaId + "/filtro";
    var urlMeioPagamento =
      global.server_api_new +
      global.apiToken +
      "/meioPagamento/familia/" +
      familiaId +
      "/filtro";

    var config = {
      headers: {
        Authorization: "bearer " + this.state.accessToken,
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Headers": "Authorization",
        "Access-Control-Allow-Methods":
          "GET, POST, OPTIONS, PUT, PATCH, DELETE",
      },
    };

    axios.post(urlMeioPagamento, {}, config).then((res) => {
      const meiosPagamento = res.data.results;
      var meiosPagamentoCombo = [];

      if (meiosPagamento != null) {
        meiosPagamento.forEach((mp) => {
          meiosPagamentoCombo.push({
            value: mp,
            label: mp.nome,
          });
        });
      }
      this.setState({ meiosPagamento: meiosPagamentoCombo });
    });
  };

  atualizarCategorias = () => {
    var familiaId = this.state.familiaId;

    if (familiaId == null) {
      familiaId = localStorage.getItem("familia-id");
    }

    //var urlCategoria = global.server_api + 'api/categoria/familia/' + familiaId + "/filtro";
    var urlCategoria =
      global.server_api_new +
      global.apiToken +
      "/categoria/familia/" +
      familiaId +
      "/filtro";

    var config = {
      headers: {
        Authorization: "bearer " + this.state.accessToken,
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Headers": "Authorization",
        "Access-Control-Allow-Methods":
          "GET, POST, OPTIONS, PUT, PATCH, DELETE",
      },
    };

    axios
      .post(urlCategoria, { IncluirOrcamentos: true }, config)
      .then((res) => {
        // console.log(res.data.results)
        const categorias = res.data.results;
        this.setState({ categorias });
      });
  };

  mapearFrequenciaPeloId = (id) => {
    if (id == null) return "";

    var frequencia = this.state.frequencias.filter(function (item) {
      return (
        (item.value != null ? item.value.toString() : null) ===
        (id != null ? id.toString() : null)
      );
    });

    return frequencia != null && frequencia.length > 0
      ? frequencia[0].label
      : "";
  };

  mapearGrupoPeloId = (id) => {
    if (id == null) return "";

    var grupo = this.state.grupos.filter(function (item) {
      return (
        (item.value != null ? item.value.toString() : null) ===
        (id != null ? id.toString() : null)
      );
    });

    return grupo != null && grupo.length > 0 ? grupo[0].label : "";
  };

  mapearTipoOrcamentoPeloId = (id) => {
    if (id == null) return "";

    var tipoOrcamento = this.state.tiposOrcamento.filter(function (item) {
      return item.value === id;
    });

    return tipoOrcamento[0].label;
  };

  mudarTipoOrcamentoFiltro = (selectedOption) => {
    const tipoOrcamentoFiltro = selectedOption.value;

    this.setState({ tipoOrcamentoFiltro }, function () {
      this.atualizarRegistros();
    });
  };

  setRedirect = () => {
    this.setState({
      redirect: true,
    });
  };

  renderRedirect = () => {
    if (this.state.redirect) {
      return <Redirect to="/auth/login" />;
    }
  };

  preparaDataParaGrid = (dataString) => {
    if (dataString == null) return "";

    var data = this.moment(dataString, "YYYY-MM-DDThh:mm:ss");
    return data.format("DD/MM/YYYY");
  };

  editarRegistro = (idOrcamento) => {
    //ultimo campo do table é o id neste caso
    var id = idOrcamento;

    var registroEdicao = this.state.orcamentos.find(function (item) {
      return parseInt(item.id) === parseInt(id);
    });

    if (registroEdicao != null && registroEdicao.tipoOrcamento < 2) {
      this.setState({
        orcamentoId: id,
        nomeOrcamento: registroEdicao.nome,
        tipoOrcamento: registroEdicao.tipoOrcamento,
        suspenso: registroEdicao.suspenso === 1 ? true : false,
      });

      this.setState({ editarOrcamento: true }, function () {
        global.showModal(this.modal2);
        $("#nome-value").val(registroEdicao.nome);
      });

      if (this.props.putNavBack) {
        this.props.putNavBack();
      }
    } else {
      //console.log('Orçamento não encontrado', id);
    }
  };

  editarLancamento = (rowData) => {
    var id = rowData[0];

    var array = this.state.listaDeLancamentos.filter(function (item) {
      return item.Id === id || item.id === id;
    });

    if (array != null && array.length === 1) {
      var itemSelecionado = array[0];

      //Especifico para lancamento (usar o storage local)
      localStorage.setItem("lancamentoEdicao", JSON.stringify(itemSelecionado));

      document.getElementById("novo-lancamento").click();
    } else {
      console.log(
        "Ou o id está no index errado do grid ou não existe o id selecionado no this.state.data"
      );
    }
  };

  setStartDate = (date, callback) => {
    if (date != null) {
      date.setHours(0);
      date.setMinutes(0, 0, 0);
      localStorage.setItem("dataInicio", JSON.stringify(date));
    }

    const startDate = date;

    this.setState({ startDate: startDate }, function () {
      document.getElementById("rightbar-startdate-refresh").click();

      if (callback != null) {
        callback();
      } else {
        this.getData();
      }
    });
  };

  setEndDate = (date, callback) => {
    if (date != null) {
      date.setHours(20);
      date.setMinutes(59, 59, 0);
      localStorage.setItem("dataFim", JSON.stringify(date));
    }

    const endDate = date;

    this.setState({ endDate: endDate }, function () {
      document.getElementById("rightbar-enddate-refresh").click();

      if (callback != null) {
        callback();
      } else {
        this.getData();
      }
    });
  };

  atualizarValorEstimado = (idCategoria, novoValor) => {
    //var url = global.server_api + 'api/categoria/' + idCategoria;
    var url =
      global.server_api_new + global.apiToken + "/categoria/" + idCategoria;

    var config = {
      headers: {
        Authorization: "bearer " + this.state.accessToken,
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Headers": "Authorization",
        "Access-Control-Allow-Methods":
          "GET, POST, OPTIONS, PUT, PATCH, DELETE",
      },
    };

    var dados = {
      ValorEstimado: novoValor,
    };

    var $this = this;

    axios.put(url, dados, config).then((res) => {
      const result = res.data;

      if (result.success === true) {
        $this.timeoutUpdateData = setTimeout(function () {
          $this.getData(true);
        }, 3500);
      } else {
        alert(
          "Erro ao atualizar estimativa da categoria. " +
            res.data.exception.Message
        );
        window.location.reload();
      }
    });
  };

  editarCategoria = (id) => {
    var registroEdicao = this.state.categorias.filter(function (item) {
      return item.id === id;
    });

    if (
      registroEdicao != null &&
      registroEdicao.length > 0 &&
      registroEdicao[0].orcamento.tipoOrcamento < 2
    ) {
      registroEdicao = registroEdicao[0];

      var orcamentoTemp = this.state.orcamentosCombo.filter(function (item) {
        if (item.value != null && registroEdicao.orcamento != null) {
          return item.value.id === registroEdicao.orcamento.id;
        }

        return false;
      });
      //console.log(registroEdicao.valorEstimado.toLocaleString('pt-BR', { style: 'currency', currency: 'BRL' }).replace('R$', ''));
      //Seta item edicao
      this.setState(
        {
          nome: registroEdicao.nome,
          grupo: registroEdicao.grupo,
          frequencia: registroEdicao.frequencia,
          valorEstimado: registroEdicao.valorEstimado,
          orcamentoId:
            orcamentoTemp != null && orcamentoTemp.length > 0
              ? orcamentoTemp[0].value
              : null,
          id: registroEdicao.id,
          suspenso: registroEdicao.suspenso === 1 ? true : false,
          nomeMeioPagamento:
            registroEdicao.meioPagamentoPadrao != null
              ? registroEdicao.meioPagamentoPadrao.nome
              : null,
          meioPagamentoPadraoId:
            registroEdicao.meioPagamentoPadrao != null
              ? registroEdicao.meioPagamentoPadrao.id
              : null,
        },
        function () {
          //Abre modal
          this.setState({ cadastrarCategoria: true }, function () {
            global.showModal(this.modal);
            $("#nome-value-categoria").val(registroEdicao.nome);
            $("#lancamento-valor").val(
              registroEdicao.valorEstimado
                .toLocaleString("pt-BR", { style: "currency", currency: "BRL" })
                .replace("R$", "")
            );
          });

          if (this.props.putNavBack) {
            this.props.putNavBack();
          }
        }
      );
    }
  };

  salvar = () => {
    var str = $("#lancamento-valor").val().replace(/\./g, "");
    str = str.replace(",", ".");
    var result = parseFloat(str);
    var item = {
      Nome: $("#nome-value-categoria").val(),
      Grupo: this.state.grupo,
      Frequencia: this.state.frequencia,
      ValorEstimado: isNaN(result) ? null : result,
      OrcamentoId:
        this.state.orcamentoId != null ? this.state.orcamentoId.id : null,
      FamiliaId:
        this.state.familiaId != null ? parseInt(this.state.familiaId) : null,
      EmpresaId:
        this.state.empresaId != null ? parseInt(this.state.empresaId) : null,
      Suspenso: this.state.suspenso === true ? 1 : 0,
      MeioPagamentoPadraoId:
        this.state.meioPagamentoPadraoId != null
          ? this.state.meioPagamentoPadraoId
          : null,
    };

    //var url = global.server_api + 'api/categoria/' + (this.state.id != null ? this.state.id : "");
    var url =
      global.server_api_new +
      global.apiToken +
      "/categoria/" +
      (this.state.id != null ? this.state.id : "");

    var config = {
      headers: {
        Authorization: "bearer " + this.state.accessToken,
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Headers": "Authorization",
        "Access-Control-Allow-Methods":
          "GET, POST, OPTIONS, PUT, PATCH, DELETE",
      },
    };

    var currentScroll = [
      document.documentElement.scrollLeft || document.body.scrollLeft,
      document.documentElement.scrollTop || document.body.scrollTop,
    ];

    global.spinnerShow($);
    //console.log(item);
    axios.post(url, item, config).then((res) => {
      //console.log(res);
      global.spinnerHide($, currentScroll);

      if (res.data.success === true) {
        this.modal.hide();
        this.limparSelecao();
        this.getData();
        this.atualizarCategorias();
        $("#atualizar-dados").click();
      } else {
        const options = {
          title: "Erro ao salvar categoria",
          message: res.data.exception.Message,
          buttons: [
            {
              label: "Ok",
            },
          ],
        };
        confirmAlert(options);
      }
    });
  };

  salvarOrcamento = () => {
    var item = {
      Nome: $("#nome-value").val(),
      TipoOrcamento: parseInt(this.state.tipoOrcamento),
      FamiliaId:
        this.state.familiaId != null
          ? parseInt(this.state.familiaId)
          : this.state.planejadorId != null
          ? this.state.familiaId2
          : null,
      EmpresaId:
        this.state.empresaId != null ? parseInt(this.state.empresaId) : null,
      PlanejadorId: this.state.planejadorId,
      Suspenso: this.state.suspenso === true ? 1 : 0,
      OrcamentoModelo: this.state.orcamentoModelo,
    };

    //var url = global.server_api + 'api/orcamento' + (this.state.orcamentoId != null ? "/" + this.state.orcamentoId : "");
    var url =
      global.server_api_new +
      global.apiToken +
      "/orcamento" +
      (this.state.orcamentoId != null ? "/" + this.state.orcamentoId : "");

    var config = {
      headers: {
        Authorization: "bearer " + this.state.accessToken,
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Headers": "Authorization",
        "Access-Control-Allow-Methods":
          "GET, POST, OPTIONS, PUT, PATCH, DELETE",
      },
    };

    var currentScroll = [
      document.documentElement.scrollLeft || document.body.scrollLeft,
      document.documentElement.scrollTop || document.body.scrollTop,
    ];

    global.spinnerShow($);
    axios.post(url, item, config).then((res) => {
      global.spinnerHide($, currentScroll);

      if (res.data.success === true) {
        this.modal2.hide();
        this.limparSelecao2();
        this.getData();
        this.atualizarOrcamentos();
        this.atualizarOrcamentosPadrao();
        $("#atualizar-dados").click();
      } else {
        const options = {
          title: "Erro ao Salvar Registro",
          message: res.data.exception.Message,
          buttons: [
            {
              label: "Ok",
            },
          ],
        };

        confirmAlert(options);
      }
    });
  };

  confirmaExcluirCategoria = (id) => {
    const options = {
      title: "Exclusão de Categoria",
      message:
        "Tem certeza que deseja excluir? Este procedimento é irreversível!",
      buttons: [
        {
          label: "Sim",
          onClick: () => this.excluirCategoria(id),
        },
        {
          label: "Não",
        },
      ],
    };

    confirmAlert(options);
  };

  confirmaExcluirOrcamento = (id) => {
    const options = {
      title: "Exclusão de Orçamento",
      message:
        "Tem certeza que deseja excluir? Este procedimento é irreversível!",
      buttons: [
        {
          label: "Sim",
          onClick: () => this.excluirOrcamento(id),
        },
        {
          label: "Não",
        },
      ],
    };

    confirmAlert(options);
  };

  excluirLancamentos = (rowsDeleted, lancamentosPorCategoria) => {
    var ids = [];
    var temParcelado = false;
    var apenasParcelado = true;

    for (var key in rowsDeleted.data) {
      ids.push(lancamentosPorCategoria[rowsDeleted.data[key].dataIndex].id);
      var parcelado =
        lancamentosPorCategoria[rowsDeleted.data[key].dataIndex].agrupamento;

      // console.log(lancamentosPorCategoria[rowsDeleted.data[key].dataIndex], parcelado);

      if (!temParcelado && parcelado != null && parcelado !== "") {
        temParcelado = true;
      } else if (apenasParcelado && (parcelado == null || parcelado === "")) {
        apenasParcelado = false;
      }
    }

    // console.log(temParcelado, apenasParcelado);

    var mensagem =
      "A exclusão é um procedimento irreversível! Tem certeza que deseja excluir estes lançamentos?";

    if (apenasParcelado) {
      mensagem +=
        " A exclusão total apaga todos os lançamentos envolvidos no parcelamento/repetição. A exclusão parcial remove o lançamento marcado para exclusão e os lançamentos futuros do mesmo parcelamento/repetição";
    } else if (temParcelado) {
      mensagem +=
        " Os lançamentos parcelados/repetidos serão TOTALMENTE excluídos, incluindo os meses passados ou futuros";
    }

    var buttons = [];

    buttons.push({
      label: apenasParcelado ? "Exclusão total" : "Sim",
      onClick: () => {
        if (ids.length > 0) {
          //var url = global.server_api + 'api/lancamento/bulk/' + ids.join(",");
          var url =
            global.server_api_new +
            global.apiToken +
            "/lancamento/bulk/" +
            ids.join(",");

          var config = {
            headers: {
              Authorization: "bearer " + this.state.accessToken,
              "Access-Control-Allow-Origin": "*",
              "Access-Control-Allow-Headers": "Authorization",
              "Access-Control-Allow-Methods":
                "GET, POST, OPTIONS, PUT, PATCH, DELETE",
            },
          };

          axios.delete(url, {}, config).then((res) => {
            const result = res.data;

            if (result.success) {
              this.getData();
            } else {
              console.log(res.data.exception);
              alert(
                "Erro ao remover lançamentos. " + res.data.exception.Message
              );
            }
          });
        }
      },
    });

    //Primeiro Botao
    if (apenasParcelado) {
      buttons.push({
        label: "Exclusão parcial",
        onClick: () => {
          if (ids.length > 0) {
            //var url = global.server_api + 'api/lancamento/bulk/currentAndFutureOnly/' + ids.join(",");
            var url =
              global.server_api_new +
              global.apiToken +
              "/lancamento/bulk/currentAndFutureOnly/" +
              ids.join(",");

            var config = {
              headers: {
                Authorization: "bearer " + this.state.accessToken,
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Headers": "Authorization",
                "Access-Control-Allow-Methods":
                  "GET, POST, OPTIONS, PUT, PATCH, DELETE",
              },
            };

            axios.delete(url, {}, config).then((res) => {
              const result = res.data;

              if (result.success) {
                this.getData();
              } else {
                console.log(res.data.exception);
                alert(
                  "Erro ao remover lançamentos. " + res.data.exception.Message
                );
              }
            });
          }
        },
      });
    }

    buttons.push({
      label: "Não",
    });

    const options = {
      title: "Exclusão de lançamentos",
      message: mensagem,
      buttons: buttons,
    };

    confirmAlert(options);

    return false;
  };

  excluirCategoria = (id) => {
    //var url = global.server_api + 'api/categoria/' + id;
    var url = global.server_api_new + global.apiToken + "/categoria/" + id;

    var config = {
      headers: {
        Authorization: "bearer " + this.state.accessToken,
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Headers": "Authorization",
        "Access-Control-Allow-Methods":
          "GET, POST, OPTIONS, PUT, PATCH, DELETE",
      },
    };
    axios.delete(url, {}, config).then((res) => {
      if (res.data.success === true) {
        this.limparSelecao();
        this.limparSelecao2();
        // window.location.reload();
        this.getData(false);
      } else {
        const options = {
          title: "Erro ao salvar categoria",
          message: res.data.exception.Message,
          buttons: [
            {
              label: "Ok",
            },
          ],
        };

        confirmAlert(options);
      }
    });
  };

  excluirOrcamento = (id) => {
    //var url = global.server_api + 'api/orcamento/' + id;
    var url = global.server_api_new + global.apiToken + "/orcamento/" + id;

    var config = {
      headers: {
        Authorization: "bearer " + this.state.accessToken,
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Headers": "Authorization",
        "Access-Control-Allow-Methods":
          "GET, POST, OPTIONS, PUT, PATCH, DELETE",
      },
    };
    axios.delete(url, {}, config).then((res) => {
      if (res.data.success === true) {
        this.limparSelecao();
        this.limparSelecao2();
        // window.location.reload();
        this.getData(false);
      } else {
        const options = {
          title: "Erro ao salvar categoria",
          message: res.data.exception.Message,
          buttons: [
            {
              label: "Ok",
            },
          ],
        };

        confirmAlert(options);
      }
    });
  };

  limparSelecao = () => {
    this.setState({
      id: null,
      nome: "",
      grupo: null,
      frequencia: null,
      valorEstimado: "",
      valorEstimadoFormatado: "",
      orcamentoId: null,
      suspenso: false,
      editarOrcamento: false,
      cadastrarOrcamento: false,
      cadastrarCategoria: false,
      meioPagamentoPadraoId: "",
      nomeMeioPagamento: "",
    });

    if (this.props.putNavFront) {
      this.props.putNavFront();
    }
  };

  limparSelecao2 = () => {
    this.setState({
      tipoOrcamento: 1,
      orcamentoId: null,
      nomeOrcamento: "",
      suspenso: false,
      orcamentoModelo: null,
      editarOrcamento: false,
      cadastrarOrcamento: false,
      cadastrarCategoria: false,
      RealizadoOrcamentoGrafico: [],
      EstimadoOrcamentoGrafico: [],
    });

    if (this.props.putNavFront) {
      this.props.putNavFront();
    }
  };

  mudarOrcamento = (selectedOption) => {
    const orcamentoId = selectedOption.value;
    this.setState({ orcamentoId });
  };

  mudarOrcamentoModelo = (selectedOption) => {
    var orcamentoModelo = null;

    if (selectedOption.value != null) {
      orcamentoModelo = selectedOption.value.id;
    }

    this.setState({ orcamentoModelo });
  };

  mudarFrequencia = (selectedOption) => {
    const frequencia = selectedOption.value;
    this.setState({ frequencia });
  };

  mudarGrupo = (selectedOption) => {
    const grupo = selectedOption.value;
    this.setState({ grupo });
  };

  handleOnClick = (event, gridUpdateValue) => {
    event.stopPropagation();
    if (event.type === "keyup") {
      var categoriaId = parseInt(event.target.id);

      if (this.timeouts[categoriaId] != null) {
        clearTimeout(this.timeouts[categoriaId]);
      }

      if (this.timeoutUpdateData != null) {
        clearTimeout(this.timeoutUpdateData);
      }

      var value = $(event.target).val();
      value = value.replace(/[^0-9]+/g, "");

      if (isNaN(value) || value === "") {
        value = 0;
      }

      value = parseInt(value) / 100;
      var formatado = value
        .toLocaleString("pt-BR", { style: "currency", currency: "BRL" })
        .replace("R$", "");

      $(event.target).val(formatado);

      var $this = this;

      const timeout = setTimeout(function () {
        $this.atualizarValorEstimado(categoriaId, value);
      }, 1000);

      this.timeouts[categoriaId] = timeout;
    }
  };

  agrupamento = (selectedOption) => {
    this.setState({
      agrupamentoSelecionado: selectedOption.value,
      expandedIndexes: [],
    });
    this.getData(true);
  };

  esconderPeriodo() {
    const TooltipRestore = withStyles((theme) => ({
      tooltip: {
        backgroundColor: "#888888",
        color: "white",
        fontSize: 12,
        fontWeight: "bold",
        fontFamily: "Open Sans",
      },
    }))(Tooltip);

    if (!this.props.onSteps)
      return (
        <CardHeader
          className="border-0"
          style={{ padding: "30px 15px 30px 0" }}
        >
          <Row className="align-items-center">
            <Col lg="2" xs="5" style={{ paddingTop: 20, paddingRight: 0 }}>
              <img
                src={require("../../assets/img/theme/icone-calendario.png")}
                alt="Gestão de Orçamento"
                className="before-title-img"
              />
              <h3 className="mb-0 chart-title">Orçamento</h3>
            </Col>
            <Col lg="4" xs="7" style={{ paddingTop: 20 }}>
              <div
                className="nav-link-icon"
                style={{ borderRadius: "15px", display: "flex" }}
              >
                <h3
                  className="mb-0 chart-title"
                  style={{ textAlign: "center" }}
                >
                  Período
                </h3>
                <Datepicker
                  locale="br"
                  id={"data-inicial"}
                  dateFormat="dd/MM/yyyy"
                  selected={this.state.startDate}
                  onChange={(date) => this.setStartDate(date)}
                  showYearDropdown
                  dateFormatCalendar="MMMM"
                  scrollableYearDropdown
                  yearDropdownItemNumber={5}
                  style={{ width: "50%" }}
                  customInput={<MaskedInput mask="11/11/1111" />}
                />
                <Datepicker
                  locale="br"
                  id={"data-final"}
                  dateFormat="dd/MM/yyyy"
                  selected={this.state.endDate}
                  onChange={(date) => this.setEndDate(date)}
                  showYearDropdown
                  dateFormatCalendar="MMMM"
                  scrollableYearDropdown
                  yearDropdownItemNumber={5}
                  style={{ width: "50%" }}
                  popperPlacement="bottom-end"
                  customInput={<MaskedInput mask="11/11/1111" />}
                />
                <TooltipRestore
                  disableFocusListener
                  disableTouchListener
                  title="Clique para ver apenas o orçamento do mês atual"
                >
                  <RestoreDate
                    style={{ cursor: "pointer" }}
                    onClick={() => {
                      this.setStartDate(
                        this.moment().startOf("month").toDate()
                      );
                      this.setEndDate(this.moment().endOf("month").toDate());
                    }}
                  />
                </TooltipRestore>
                <TooltipRestore
                  disableFocusListener
                  disableTouchListener
                  title="Clique para ver o orçamento dos últimos três meses"
                >
                  <Restore3Months
                    style={{ cursor: "pointer" }}
                    onClick={() => {
                      this.setStartDate(
                        this.moment()
                          .subtract(3, "months")
                          .startOf("month")
                          .toDate()
                      );
                      this.setEndDate(
                        this.moment()
                          .subtract(1, "months")
                          .endOf("month")
                          .toDate()
                      );
                    }}
                  />
                </TooltipRestore>
              </div>
            </Col>
            <Col lg="auto" xs="12">
              <Row style={{ justifyContent: "flex-start", paddingTop: 20 }}>
                <NavItem
                  style={{
                    marginLeft: 8,
                    listStyleType: "none",
                    marginBottom: 10,
                  }}
                >
                  {/* <NavLink
                    className="nav-link-icon featured-button"
                    to="/admin/extrato"
                    onClick={() => {
                      sessionStorage.setItem("checkedNotCategorized", true);
                    }}
                    tag={Link}
                    style={{
                      backgroundImage:
                        "url(" +
                        require("../../assets/img/theme/botao-destaque.png") +
                        ")",
                    }}
                  >
                    <span className="nav-link-inner--text inner">
                      Não categorizados
                    </span>
                  </NavLink> */}
                </NavItem>
                <NavItem
                  style={{
                    marginLeft: 8,
                    listStyleType: "none",
                    marginBottom: 10,
                  }}
                >
                  <NavLink
                    className="nav-link-icon featured-button"
                    to="/admin/destinacao-patrimonio"
                    onClick={() => {
                      this.setState({ cadastrarOrcamento: true }, function () {
                        global.showModal(this.modal2);
                      });
                      if (this.props.putNavBack) {
                        this.props.putNavBack();
                      }
                    }}
                    style={{
                      backgroundImage:
                        "url(" +
                        require("../../assets/img/theme/botao-destaque.png") +
                        ")",
                    }}
                  >
                    <span className="nav-link-inner--text inner">
                      Novo orçamento
                    </span>
                  </NavLink>
                </NavItem>
                <NavItem style={{ marginLeft: 8, listStyleType: "none" }}>
                  <NavLink
                    className="nav-link-icon aux-button"
                    to="/admin/gestao-planos-sonhos"
                    onClick={() => {
                      this.setState({ cadastrarCategoria: true }, function () {
                        global.showModal(this.modal);
                      });
                      if (this.props.putNavBack) {
                        this.props.putNavBack();
                      }
                    }}
                  >
                    <span className="nav-link-inner--text inner">
                      Nova categoria
                    </span>
                  </NavLink>
                </NavItem>
                <NavItem
                  style={{
                    width: "250px",
                    marginLeft: 8,
                    listStyleType: "none",
                  }}
                >
                  <Select
                    id="campoCalculado"
                    onChange={this.agrupamento}
                    className="select-component"
                    options={this.selecaoDeAgrupamento}
                    defaultValue={this.selecaoDeAgrupamento[0]}
                    placeholder="Selecione..."
                  />
                </NavItem>
              </Row>
            </Col>
          </Row>
        </CardHeader>
      );
    else
      return (
        <CardHeader className="border-0">
          <Row className="align-items-center">
            <div style={{ float: "left", width: "160px" }}>
              <img
                src={require("../../assets/img/theme/icone-calendario.png")}
                alt="Gestão de Orçamento"
                className="before-title-img"
              />
              <h3 className="mb-0 chart-title">Orçamento</h3>
            </div>
            <Col lg="9" xl="9" style={{ float: "right !important" }}>
              <NavItem
                className="general-button dark"
                style={{ float: "right", marginLeft: "5px" }}
              >
                <NavLink
                  className="nav-link-icon featured-button"
                  to="/admin/destinacao-patrimonio"
                  onClick={() => {
                    this.setState({ cadastrarOrcamento: true }, function () {
                      global.showModal(this.modal2);
                    });
                    if (this.props.putNavBack) {
                      this.props.putNavBack();
                    }
                  }}
                  style={{
                    backgroundImage:
                      "url(" +
                      require("../../assets/img/theme/botao-destaque.png") +
                      ")",
                  }}
                >
                  <span className="nav-link-inner--text inner">
                    Novo orçamento
                  </span>
                </NavLink>
              </NavItem>
              <NavItem
                className="general-button dark"
                style={{ float: "right", marginLeft: "5px" }}
              >
                <NavLink
                  className="nav-link-icon aux-button"
                  to="/admin/gestao-planos-sonhos"
                  onClick={() => {
                    this.setState({ cadastrarCategoria: true }, function () {
                      global.showModal(this.modal);
                    });
                    if (this.props.putNavBack) {
                      this.props.putNavBack();
                    }
                  }}
                >
                  <span className="nav-link-inner--text inner">
                    Nova categoria
                  </span>
                </NavLink>
              </NavItem>
              <Col lg="10" xl="10" style={{ float: "right !important" }}>
                <NavItem
                  className="general-button dark"
                  style={{ float: "right", marginLeft: "5px" }}
                >
                  <NavLink
                    className="nav-link-icon aux-button"
                    to="/admin/gestao-planos-sonhos"
                    onClick={this.props.onContinue}
                  >
                    <span className="nav-link-inner--text inner">Terminar</span>
                  </NavLink>
                </NavItem>
              </Col>
            </Col>
          </Row>
        </CardHeader>
      );
  }

  mostrarPopupCadastroOrcamentos = () => (
    <div>
      <SkyLight
        ref={(ref) => (this.modal2 = ref)}
        transitionDuration={0}
        beforeOpen={this.preCarregarParaEdicao}
        afterClose={this.limparSelecao2}
        title={
          <>
            <div className="pop-up-title">
              <i className="ni ni-check-bold"></i>{" "}
              <h2>
                {this.state.cadastrarOrcamento === true
                  ? "Cadastro de Orçamento"
                  : "Edição de Orçamento"}
              </h2>
            </div>
          </>
        }
        dialogStyles={{ borderRadius: "1rem", padding: "2rem" }}
      >
        <Row>
          <Col lg="6" xl="6">
            <div>
              <label>Tipo de Orçamento *</label>
              {this.state.orcamentoId == null ||
              this.state.tipoOrcamento < 2 ? (
                <Select
                  id={"select-tipo-orcamento"}
                  onChange={(e) =>
                    this.setState({
                      tipoOrcamento: e.value,
                      orcamentoModelo: null,
                    })
                  }
                  className="select-component"
                  options={this.state.tiposOrcamentoMenorQueDois}
                  defaultValue={
                    this.state.tiposOrcamentoMenorQueDois != null
                      ? this.state.tiposOrcamentoMenorQueDois[0]
                      : ""
                  }
                  placeholder="Selecione..."
                  value={
                    this.state.tiposOrcamentoMenorQueDois != null &&
                    this.state.tipoOrcamento != null
                      ? this.state.tiposOrcamentoMenorQueDois.find((op) => {
                          return op.value === this.state.tipoOrcamento;
                        })
                      : null
                  }
                />
              ) : (
                <b>{global.tipoOrcamento[this.state.tipoOrcamento]}</b>
              )}
            </div>
          </Col>
          <Col
            lg="6"
            xl="6"
            style={this.state.orcamentoId != null ? { display: "none" } : {}}
          >
            <div>
              <label>
                Orçamento Modelo{" "}
                <span style={{ fontSize: "10px" }}>
                  (utilize para clonar categorias associadas)
                </span>
              </label>
              <Select
                id={"select-frequencia"}
                onChange={this.mudarOrcamentoModelo}
                className="select-component"
                options={this.filtrarPadraoPeloTipo()}
                defaultValue={
                  this.filtrarPadraoPeloTipo() != null
                    ? this.filtrarPadraoPeloTipo()[0]
                    : ""
                }
                placeholder="Selecione..."
                value={
                  this.state.orcamentoModelo != null
                    ? this.filtrarPadraoPeloTipo().find((op) => {
                        if (
                          op.value != null &&
                          this.state.orcamentoModelo != null
                        ) {
                          return op.value.id === this.state.orcamentoModelo.id;
                        }
                        return null;
                      })
                    : null
                }
              />
            </div>
          </Col>
          <Col lg="6" xl="6">
            <div>
              <label>Nome *</label>
              <input
                type="text"
                id="nome-value"
                style={{ paddingLeft: 24, color: "#808080", height: 38 }}
              ></input>
            </div>
          </Col>
          <Col lg="6" xl="6">
            <div>
              <label>Suspenso</label>
              <Switch
                onChange={this.handleSuspensoChange}
                checked={this.state.suspenso}
              />
            </div>
          </Col>
          <Col
            lg="12"
            xl="12"
            style={{
              display: "flex",
              justifyContent: "center",
              marginTop: "2rem",
            }}
          >
            <button
              id="salvar-lancamento"
              className="featured-button"
              onClick={() => this.salvarOrcamento()}
              style={{
                backgroundImage:
                  "url(" +
                  require("../../assets/img/theme/botao-destaque.png") +
                  ")",
              }}
            >
              SALVAR
            </button>
            {this.state.cadastrarOrcamento === true ? (
              " "
            ) : (
              <button
                id="apagar-orcamento"
                className="nav-link-icon aux-button nav-link ml-3"
                onClick={() =>
                  this.confirmaExcluirOrcamento(this.state.orcamentoId)
                }
              >
                APAGAR ORÇAMENTO
              </button>
            )}
          </Col>
        </Row>
      </SkyLight>
    </div>
  );

  handleChangeMeioPagamento = (selectedOption) => {
    this.setState({
      meioPagamentoPadraoId: selectedOption.value.id,
      nomeMeioPagamento: selectedOption.value.nome,
    });
  };

  mostrarPopupCadastroCategorias = () => (
    <div>
      <SkyLight
        ref={(ref) => (this.modal = ref)}
        transitionDuration={0}
        beforeOpen={this.preCarregarParaEdicao}
        afterClose={this.limparSelecao}
        title={
          <>
            <div className="pop-up-title">
              <i className="ni ni-check-bold"></i> <h2>Edição de Categoria</h2>
            </div>
          </>
        }
        dialogStyles={{ borderRadius: "1rem", padding: "2rem" }}
      >
        <Row>
          <Col lg="6" xl="6">
            <div>
              <label>Nome</label>
              <input
                type="text"
                id="nome-value-categoria"
                style={{ paddingLeft: 24, color: "#808080" }}
              ></input>
            </div>
          </Col>
          <Col lg="6" xl="6">
            <div>
              <label>Orçamento</label>
              <Select
                id={"select-orcamento"}
                onChange={this.mudarOrcamento}
                className="select-component"
                options={this.state.orcamentosCombo}
                defaultValue={
                  this.state.orcamentosCombo != null
                    ? this.state.orcamentosCombo[0]
                    : ""
                }
                placeholder="Selecione..."
                value={
                  this.state.orcamentosCombo != null
                    ? this.state.orcamentosCombo.find((op) => {
                        if (
                          op.value != null &&
                          this.state.orcamentoId != null
                        ) {
                          return op.value.id === this.state.orcamentoId.id;
                        }
                        return null;
                      })
                    : null
                }
              />
            </div>
          </Col>
          <Col lg="6" xl="6">
            <div>
              <label>Grupo</label>
              <Select
                id={"select-grupo"}
                onChange={this.mudarGrupo}
                className="select-component"
                options={this.state.grupos}
                defaultValue={
                  this.state.grupos != null ? this.state.grupos[0] : ""
                }
                placeholder="Selecione..."
                value={
                  this.state.grupos != null && this.state.grupo != null
                    ? this.state.grupos.find((op) => {
                        return op.value === this.state.grupo;
                      })
                    : null
                }
              />
            </div>
          </Col>
          <Col lg="6" xl="6">
            <div>
              <label>Frequência</label>
              <Select
                id={"select-frequencia"}
                onChange={this.mudarFrequencia}
                className="select-component"
                options={this.state.frequencias}
                defaultValue={
                  this.state.frequencias != null
                    ? this.state.frequencias[0]
                    : ""
                }
                placeholder="Selecione..."
                value={
                  this.state.frequencias != null &&
                  this.state.frequencia != null
                    ? this.state.frequencias.find((op) => {
                        return op.value === this.state.frequencia;
                      })
                    : null
                }
              />
            </div>
          </Col>
          <Col lg="6" xl="6">
            <div>
              <label>Valor Estimado</label>
              <CurrencyInput
                id="lancamento-valor"
                value={$("#lancamento-valor").val()}
                thousandSeparator={"."}
                decimalSeparator={","}
                precision="2" //prefix={'R$ '}
                style={{ paddingLeft: 24, color: "#808080" }}
              />
            </div>
          </Col>
          <Col lg="6" xl="6">
            <div>
              <label>Forma de pagamento padrão</label>
              <Select
                id="meioPagamentoPadrao"
                onChange={this.handleChangeMeioPagamento}
                className="select-component"
                options={this.state.meiosPagamento}
                defaultValue={
                  this.state.meiosPagamento != null
                    ? this.state.meiosPagamento[0]
                    : ""
                }
                placeholder="Selecione..."
                value={
                  this.state.meiosPagamento != null &&
                  this.state.nomeMeioPagamento != null
                    ? this.state.meiosPagamento.find((op) => {
                        return op.label === this.state.nomeMeioPagamento;
                      })
                    : null
                }
              />
            </div>
          </Col>
          <Col lg="6" xl="6">
            <div>
              <label>Suspenso</label>
              <Switch
                onChange={this.handleSuspensoChange}
                checked={this.state.suspenso}
              />
            </div>
          </Col>
          <Col
            lg="12"
            xl="12"
            style={{ display: "flex", justifyContent: "center" }}
          >
            <button
              id="salvar-lancamento"
              className="featured-button"
              onClick={() => this.salvar()}
              style={{
                backgroundImage:
                  "url(" +
                  require("../../assets/img/theme/botao-destaque.png") +
                  ")",
              }}
            >
              SALVAR
            </button>
          </Col>
        </Row>
      </SkyLight>
    </div>
  );

  render() {
    var { isLoading } = this.state;

    var data = [];
    var change = this.state.agrupamentoSelecionado;

    if (change === 0) {
      data = !this.state.mostrarSuspensos
        ? this.state.listaDeOrcamentos
        : this.state.listaDeOrcamentosComSuspensos;
    } else if (change === 1) {
      data = !this.state.mostrarSuspensos
        ? this.state.listaDeGrupos
        : this.state.listaDeGruposComSuspensos;
    } else {
      data = !this.state.mostrarSuspensos
        ? this.state.listaDeCategoriasSemSuspensos
        : this.state.originalData;
    }

    var labels = data.map((value) => {
      return change === 0
        ? value["NomeOrcamento"]
        : change === 1
        ? value["Grupo"]
        : value["NomeCategoria"];
    });

    var estimados = data.map((value) => {
      return value["Estimado"];
    });

    var realizados = data.map((value) => {
      return value["TotalMedio"];
    });

    var chartOptions = {
      options: {
        legend: {
          show: true,
          position: "top",
          horizontalAlign: "center",
          fontFamily: "Open Sans, sans-serif",
          fontSize: "14px",
          markers: {
            width: 20,
            height: 10,
            radius: 15,
          },
        },
        plotOptions: {
          bar: {
            horizontal: true,
            endingShape: "rounded",
            dataLabels: {
              position: "top",
            },
          },
        },
        dataLabels: {
          enabled: true,
          textAnchor: "middle",
          style: {
            fontSize: "11px",
            fontFamily: "Open Sans, sans-serif",
            colors: ["#57657D"],
            textAlign: "center",
          },
          offsetX: 50,
          formatter: function (value) {
            return value !== null && value !== 0
              ? value
                  .toLocaleString("pt-BR", {
                    style: "currency",
                    currency: "BRL",
                  })
                  .replace("R$", "")
              : "";
          },
        },
        stroke: {
          curve: "smooth",
        },
        chart: {
          id: "basic-bar",
        },
        yaxis: {
          labels: {
            formatter: function (value) {
              return value != null
                ? value
                    .toLocaleString("pt-BR", {
                      style: "currency",
                      currency: "BRL",
                    })
                    .replace("R$", "")
                : "";
            },
            style: {
              fontSize: "11px",
              fontFamily: "Open Sans, sans-serif",
              colors: ["#57657D"],
              textAlign: "center",
            },
          },
        },
        xaxis: {
          categories: labels,
          labels: {
            formatter: function (value) {
              return value != null
                ? value
                    .toLocaleString("pt-BR", {
                      style: "currency",
                      currency: "BRL",
                    })
                    .replace("R$", "")
                : "";
            },
            style: {
              fontSize: "11px",
              fontFamily: "Open Sans, sans-serif",
              colors: ["#57657D"],
              textAlign: "center",
            },
          },
        },
        colors: ["#9A3DFF", "#28ADC0"],
        responsive: [
          {
            breakpoint: 767,
            options: {
              dataLabels: {
                enabled: false,
              },
            },
          },
        ],
      },
      series: [
        {
          name: "Estimados",
          data: estimados,
        },
        {
          name: "Realizados",
          data: realizados,
        },
      ],
    };

    if (change === 0) {
      var selecionadaCategoria = data.find((categoria) => {
        return categoria.OrcamentoId === this.state.orcamentoSelecionado;
      });

      var labelsOrcamento = [];
      var estimadosOrcamento = [];
      var realizadoOrcamento = [];

      if (selecionadaCategoria != null && this.state.originalData != null) {
        selecionadaCategoria.categorias = this.state.originalData.filter(
          function (value) {
            return value.OrcamentoId === selecionadaCategoria.OrcamentoId;
          }
        );
      }
      if (
        selecionadaCategoria != null &&
        selecionadaCategoria.categorias != null
      ) {
        labelsOrcamento = selecionadaCategoria.categorias.map(function (value) {
          return value["NomeCategoria"];
        });
        estimadosOrcamento = selecionadaCategoria.categorias.map(function (
          value
        ) {
          return value["Estimado"];
        });
        realizadoOrcamento = selecionadaCategoria.categorias.map(function (
          value
        ) {
          return value["TotalMedio"];
        });
      }
    }

    var chartOptionsOrcamentos = {
      options: {
        legend: {
          show: true,
          position: "top",
          horizontalAlign: "center",
          fontFamily: "Open Sans, sans-serif",
          fontSize: "14px",
          offsetY: -15,
          itemMargin: {
            horizontal: 5,
            vertical: 10,
          },
          markers: {
            width: 20,
            height: 10,
            radius: 15,
          },
          onItemHover: {
            highlightDataSeries: false,
          },
        },
        plotOptions: {
          bar: {
            horizontal: false,
            endingShape: "flat",
            columnWidth: "10%",
          },
        },
        colors: ["#9A3DFF", "#28ADC0"],
        dataLabels: {
          enabled: true,
          enabledOnSeries: [0, 1],
          formatter: function (value) {
            return value !== null && value !== 0
              ? value
                  .toLocaleString("pt-BR", {
                    style: "currency",
                    currency: "BRL",
                  })
                  .replace("R$", "")
              : "";
          },
          offsetY: -10,
          style: {
            fontSize: "11px",
            fontFamily: "Open Sans, sans-serif",
            colors: ["#57657D"],
            textAlign: "center",
          },
          background: {
            enabled: false,
          },
        },
        stroke: {
          curve: "smooth",
          width: [0, 4],
        },
        chart: {
          id: "basic-bar",
          type: "line",
        },
        yaxis: {
          labels: {
            formatter: function (value) {
              return value != null
                ? value
                    .toLocaleString("pt-BR", {
                      style: "currency",
                      currency: "BRL",
                    })
                    .replace("R$", "")
                : "";
            },
          },
        },
      },
      series: [
        {
          name: "Realizado",
          type: "column",
          data: this.state.RealizadoOrcamentoGrafico,
        },
        {
          name: "Estimado",
          type: "line",
          data: this.state.EstimadoOrcamentoGrafico,
        },
      ],
    };
    return (
      <div id="tela-gestao-orcamento">
        <SkyLight
          ref={(ref) => (this.modal3 = ref)}
          transitionDuration={0}
          beforeOpen={this.preCarregarParaEdicao}
          afterClose={this.limparSelecao2}
          dialogStyles={{ borderRadius: "1rem", padding: "2rem" }}
          title={
            <>
              <div className="pop-up-title">
                <i className="ni ni-check-bold"></i>{" "}
                <h2>
                  Gráfico{" "}
                  {selecionadaCategoria != null
                    ? selecionadaCategoria.NomeOrcamento
                    : ""}
                </h2>
              </div>
            </>
          }
        >
          <div>
            <ApexChart
              options={chartOptionsOrcamentos.options}
              series={chartOptionsOrcamentos.series}
              type="line"
              height="600px"
              width="100%"
              className="home-estimate-realized"
            />
          </div>
        </SkyLight>
        <SkyLight
          ref={(ref) => (this.modalGraficoCategoria = ref)}
          transitionDuration={0}
          beforeOpen={this.preCarregarParaEdicao}
          afterClose={this.limparSelecao2}
          dialogStyles={{
            borderRadius: "1rem",
            padding: "2rem 2rem 4rem 2rem",
          }}
          title={
            <>
              <div className="pop-up-title">
                <i className="ni ni-check-bold"></i>{" "}
                <h2>
                  Gráfico{" "}
                  {this.state.modalNomeCategoria != null
                    ? this.state.modalNomeCategoria
                    : ""}
                </h2>
              </div>
            </>
          }
        >
          <div>{this.loadCategoriaGrafico()}</div>
        </SkyLight>
        <div
          id="tela-cadastro-lancamento"
          style={{ padding: "0 5%", marginTop: 20 }}
        >
          <div className="home-summary-top">
            <Row>
              <Col lg="12" xl="12" style={{ marginBottom: "15px" }}>
                <Card>{this.esconderPeriodo()}</Card>
              </Col>
            </Row>
          </div>

          <div id="orcamento-table">
            {this.dataTablePlot(this.state.agrupamentoSelecionado, isLoading)}
          </div>
          {(() => {
            if (this.props.onSteps == null) {
              return (
                <Col
                  lg="12"
                  xl="12"
                  className="filterAndOnder lancamentos period"
                  style={{ marginTop: "60px", padding: 0 }}
                >
                  <ApexChart
                    options={chartOptions.options}
                    series={chartOptions.series}
                    type="bar"
                    width="100%"
                    className="home-estimate-realized"
                  />
                </Col>
              );
            }
          })()}
        </div>
        <Rightbar
          {...this.props}
          logo={{
            innerLink: "/admin/index",
            imgSrc: require("../../assets/img/brand/logo-nova-branca.png"),
            imgAlt: "...",
          }}
          id="right-sidenav-main"
          position="fixed-right"
          menuItens={[
            "sumarioPrevistoRealizadoGeral",
            "gastosPorOrcamento",
            "gastosPorCategoria",
          ]}
        />
        {this.state.cadastrarOrcamento && this.mostrarPopupCadastroOrcamentos()}
        {this.state.editarOrcamento && this.mostrarPopupCadastroOrcamentos()}
        {this.state.cadastrarCategoria && this.mostrarPopupCadastroCategorias()}
        {this.renderRedirect()}
      </div>
    );
  }

  FixarDesfixarColuna = (tabela, coluna, valor) => {
    console.log("tabela:", tabela);

    var persistenciaColunas = this.state.persistenciaColunas;
    switch (tabela) {
      case "OrcamentoCategoria":
        switch (coluna) {
          case "NomeMeioPagamento":
            persistenciaColunas.OrcamentoCategoria.NomeMeioPagamento = valor;
            break;
          case "Frequencia":
            persistenciaColunas.OrcamentoCategoria.Frequencia = valor;
            break;
          case "Grupo":
            persistenciaColunas.OrcamentoCategoria.Grupo = valor;
            break;
          case "Estimado":
            persistenciaColunas.OrcamentoCategoria.Estimado = valor;
            break;
          case "TotalMedio":
            persistenciaColunas.OrcamentoCategoria.TotalMedio = valor;
            break;
          case "Quantidade":
            persistenciaColunas.OrcamentoCategoria.Quantidade = valor;
            break;
          case "Total":
            persistenciaColunas.OrcamentoCategoria.Total = valor;
            break;
          case "Media":
            persistenciaColunas.OrcamentoCategoria.Media = valor;
            break;
          case "Minimo":
            persistenciaColunas.OrcamentoCategoria.Minimo = valor;
            break;
          case "Maximo":
            persistenciaColunas.OrcamentoCategoria.Maximo = valor;
            break;
          case "Suspenso":
            persistenciaColunas.OrcamentoCategoria.Suspenso = valor;
            break;
          case "PercentualDespesas":
            persistenciaColunas.OrcamentoCategoria.PercentualDespesas = valor;
            break;
          default:
            break;
        }
        break;
      case "Lancamentos":
        switch (coluna) {
          case "id":
            persistenciaColunas.Lancamentos.id = valor;
            break;
          case "data":
            persistenciaColunas.Lancamentos.data = valor;
            break;
          case "descricao":
            persistenciaColunas.Lancamentos.descricao = valor;
            break;
          case "valor":
            persistenciaColunas.Lancamentos.valor = valor;
            break;
          case "nomeMeioPagamento":
            persistenciaColunas.Lancamentos.nomeMeioPagamento = valor;
            break;
          case "valorParcela":
            persistenciaColunas.Lancamentos.valorParcela = valor;
            break;
          case "agrupamento":
            persistenciaColunas.Lancamentos.agrupamento = valor;
            break;
          case "operacao":
            persistenciaColunas.Lancamentos.operacao = valor;
            break;
          case "tipoOperacao":
            persistenciaColunas.Lancamentos.tipoOperacao = valor;
            break;
          default:
            break;
        }
        break;
      case "Orcamento":
        switch (coluna) {
          case "Estimado":
            persistenciaColunas.Orcamento.Estimado = valor;
            break;
          case "TotalMedio":
            persistenciaColunas.Orcamento.TotalMedio = valor;
            break;
          case "Quantidade":
            persistenciaColunas.Orcamento.Quantidade = valor;
            break;
          case "Total":
            persistenciaColunas.Orcamento.Total = valor;
            break;
          case "Media":
            persistenciaColunas.Orcamento.Media = valor;
            break;
          case "Minimo":
            persistenciaColunas.Orcamento.Minimo = valor;
            break;
          case "Maximo":
            persistenciaColunas.Orcamento.Maximo = valor;
            break;
          case "Suspenso":
            persistenciaColunas.Orcamento.Suspenso = valor;
            break;
          case "PercentualDespesas":
            persistenciaColunas.Orcamento.PercentualDespesas = valor;
            break;
          default:
            break;
        }
        break;
      case "GrupoCategoria":
        switch (coluna) {
          case "NomeMeioPagamento":
            persistenciaColunas.GrupoCategoria.NomeMeioPagamento = valor;
            break;
          case "Frequencia":
            persistenciaColunas.GrupoCategoria.Frequencia = valor;
            break;
          case "Estimado":
            persistenciaColunas.GrupoCategoria.Estimado = valor;
            break;
          case "TotalMedio":
            persistenciaColunas.GrupoCategoria.TotalMedio = valor;
            break;
          case "OrcamentoSuspenso":
            persistenciaColunas.GrupoCategoria.OrcamentoSuspenso = valor;
            break;
          case "Quantidade":
            persistenciaColunas.GrupoCategoria.Quantidade = valor;
            break;
          case "Total":
            persistenciaColunas.GrupoCategoria.Total = valor;
            break;
          case "Media":
            persistenciaColunas.GrupoCategoria.Media = valor;
            break;
          case "Minimo":
            persistenciaColunas.GrupoCategoria.Minimo = valor;
            break;
          case "Maximo":
            persistenciaColunas.GrupoCategoria.Maximo = valor;
            break;
          case "PercentualDespesas":
            persistenciaColunas.GrupoCategoria.PercentualDespesas = valor;
            break;
          default:
            break;
        }
        break;
      case "Grupo":
        switch (coluna) {
          case "Estimado":
            persistenciaColunas.Grupo.Estimado = valor;
            break;
          case "TotalMedio":
            persistenciaColunas.Grupo.TotalMedio = valor;
            break;
          case "Quantidade":
            persistenciaColunas.Grupo.Quantidade = valor;
            break;
          case "Total":
            persistenciaColunas.Grupo.Total = valor;
            break;
          case "Media":
            persistenciaColunas.Grupo.Media = valor;
            break;
          case "Minimo":
            persistenciaColunas.Grupo.Minimo = valor;
            break;
          case "Maximo":
            persistenciaColunas.Grupo.Maximo = valor;
            break;
          case "Suspenso":
            persistenciaColunas.Grupo.Suspenso = valor;
            break;
          case "PercentualDespesas":
            persistenciaColunas.Grupo.PercentualDespesas = valor;
            break;
          default:
            break;
        }
        break;
      case "Categoria":
        switch (coluna) {
          case "NomeMeioPagamento":
            persistenciaColunas.Categoria.NomeMeioPagamento = valor;
            break;
          case "Frequencia":
            persistenciaColunas.Categoria.Frequencia = valor;
            break;
          case "Grupo":
            persistenciaColunas.Categoria.Grupo = valor;
            break;
          case "Estimado":
            persistenciaColunas.Categoria.Estimado = valor;
            break;
          case "TotalMedio":
            persistenciaColunas.Categoria.TotalMedio = valor;
            break;
          case "CategoriaSuspenso":
            persistenciaColunas.Categoria.CategoriaSuspenso = valor;
            break;
          case "Quantidade":
            persistenciaColunas.Categoria.Quantidade = valor;
            break;
          case "Total":
            persistenciaColunas.Categoria.Total = valor;
            break;
          case "Media":
            persistenciaColunas.Categoria.Media = valor;
            break;
          case "Minimo":
            persistenciaColunas.Categoria.Minimo = valor;
            break;
          case "Maximo":
            persistenciaColunas.Categoria.Maximo = valor;
            break;
          case "PercentualDespesas":
            persistenciaColunas.Categoria.PercentualDespesas = valor;
            break;
          default:
            break;
        }
        break;
      default:
        break;
    }

    sessionStorage.setItem(
      btoa("persistenciaColunas"),
      btoa(JSON.stringify(persistenciaColunas))
    );
    this.setState({ persistenciaColunas });
  };

  FixarDesfixarLinha = (tabela, valor) => {
    console.log("tabela:", tabela);
    console.log("valor:", valor);

    var persistenciaLinhas = this.state.persistenciaLinhas;
    switch (tabela) {
      case "OrcamentoCategoria":
        persistenciaLinhas.OrcamentoCategoria = valor;
        break;
      case "Lancamentos":
        persistenciaLinhas.Lancamentos = valor;
        break;
      case "Orcamento":
        persistenciaLinhas.Orcamento = valor;
        break;
      case "GrupoCategoria":
        persistenciaLinhas.GrupoCategoria = valor;
        break;
      case "Grupo":
        persistenciaLinhas.Grupo = valor;
        break;
      case "Categoria":
        persistenciaLinhas.Categoria = valor;
        break;
      default:
        break;
    }

    console.log("persistenciaLinhas:", persistenciaLinhas);
    sessionStorage.setItem(
      btoa("persistenciaLinhas"),
      btoa(JSON.stringify(persistenciaLinhas))
    );
    this.setState({ persistenciaLinhas });
  };

  Pesquisa_Filtros(tableState) {
    var Sumario = {
      Orcamento: {
        ReceitasEstimadas: 0,
        ReceitasRealizadas: 0,
        DespesasEstimadas: 0,
        DespesasRealizadas: 0,
        InvestimentosEstimados: 0,
        InvestimentosRealizados: 0,
        DividasEstimadas: 0,
        DividasRealizadas: 0,
      },
    };

    if (tableState.columns.length === 14) {
      tableState.displayData.forEach((x) => {
        switch (x.data[12]) {
          case 0:
            Sumario.Orcamento.ReceitasEstimadas += global.convertToFloat(
              x.data[4].props.children
            );
            Sumario.Orcamento.ReceitasRealizadas += global.convertToFloat(
              x.data[5].props.children
            );
            break;
          case 1:
            Sumario.Orcamento.DespesasEstimadas += global.convertToFloat(
              x.data[4].props.children
            );
            Sumario.Orcamento.DespesasRealizadas += global.convertToFloat(
              x.data[5].props.children
            );
            break;
          case 2:
            Sumario.Orcamento.InvestimentosEstimados += global.convertToFloat(
              x.data[4].props.children
            );
            Sumario.Orcamento.InvestimentosRealizados += global.convertToFloat(
              x.data[5].props.children
            );
            break;
          case 3:
            Sumario.Orcamento.DividasEstimadas += global.convertToFloat(
              x.data[4].props.children
            );
            Sumario.Orcamento.DividasRealizadas += global.convertToFloat(
              x.data[5].props.children
            );
            break;
          default:
            break;
        }
      });
    } else if (tableState.columns.length === 19) {
      tableState.displayData.forEach((x) => {
        switch (x.data[11]) {
          case 0:
            Sumario.Orcamento.ReceitasEstimadas += global.convertToFloat(
              x.data[17]
            );
            Sumario.Orcamento.ReceitasRealizadas += global.convertToFloat(
              x.data[9].props.children
            );
            break;
          case 1:
            Sumario.Orcamento.DespesasEstimadas += global.convertToFloat(
              x.data[17]
            );
            Sumario.Orcamento.DespesasRealizadas += global.convertToFloat(
              x.data[9].props.children
            );
            break;
          case 2:
            Sumario.Orcamento.InvestimentosEstimados += global.convertToFloat(
              x.data[17]
            );
            Sumario.Orcamento.InvestimentosRealizados += global.convertToFloat(
              x.data[9].props.children
            );
            break;
          case 3:
            Sumario.Orcamento.DividasEstimadas += global.convertToFloat(
              x.data[17]
            );
            Sumario.Orcamento.DividasRealizadas += global.convertToFloat(
              x.data[9].props.children
            );
            break;
          default:
            break;
        }
      });
    }

    if (
      Sumario.Orcamento.ReceitasEstimadas !==
        this.state.Sumario.Orcamento.ReceitasEstimadas ||
      Sumario.Orcamento.ReceitasRealizadas !==
        this.state.Sumario.Orcamento.ReceitasRealizadas ||
      Sumario.Orcamento.DespesasEstimadas !==
        this.state.Sumario.Orcamento.DespesasEstimadas ||
      Sumario.Orcamento.DespesasRealizadas !==
        this.state.Sumario.Orcamento.DespesasRealizadas ||
      Sumario.Orcamento.InvestimentosEstimados !==
        this.state.Sumario.Orcamento.InvestimentosEstimados ||
      Sumario.Orcamento.InvestimentosRealizados !==
        this.state.Sumario.Orcamento.InvestimentosRealizados ||
      Sumario.Orcamento.DividasEstimadas !==
        this.state.Sumario.Orcamento.DividasEstimadas ||
      Sumario.Orcamento.DividasRealizadas !==
        this.state.Sumario.Orcamento.DividasRealizadas
    ) {
      this.setState({ Sumario });
    }
  }

  obterCor = (tipo) => {
    if (tipo === 0) {
      return "#e7f4e3";
    }
    if (tipo === 1 || tipo === 3) {
      return "#f8dfe2";
    }
    if (tipo === 2) {
      return "#e3f3ff";
    }
  };

  dataTablePlot = (change, isLoading) => {
    var $this = this;
    var lancamentos = this.state.listaDeLancamentos
      ? this.state.listaDeLancamentos
      : [];

    if (change == null) {
      return <>Carregando Visão...</>;
    } else if (change === 0) {
      var data = !this.state.mostrarSuspensos
        ? this.state.listaDeOrcamentos
        : this.state.listaDeOrcamentosComSuspensos;
      var orcamentoChilds = !this.state.mostrarSuspensos
        ? this.state.listaDeCategorias
        : this.state.listaDeCategoriasComSuspensos;

      // var agrupados = [];

      // if (this.state.mostrarAgrupado === true) {
      //   agrupados = data.map(function (value, index) {
      //     return index;
      //   });
      // } else if (this.state.expandedIndexes != null) {
      //   agrupados = this.state.expandedIndexes;
      // }

      const optionsOrcamento = {
        onColumnViewChange: (changedColumn, action) => {
          switch (action) {
            case "add":
              this.FixarDesfixarColuna("Orcamento", changedColumn, true);
              break;
            case "remove":
              this.FixarDesfixarColuna("Orcamento", changedColumn, false);
              break;
            default:
              break;
          }
        },
        onTableChange: (action, tableState) => {
          tableState.data.filter((obj) => {
            if (obj.data[3] === "Despesa não Categorizada") {
              if (this.state.DespesaIndex !== obj.index) {
                this.setState({ DespesaIndex: obj.index });
              }
            } else if (obj.data[3] === "Receita não Categorizada") {
              if (this.state.ReceitaIndex !== obj.index) {
                this.setState({ ReceitaIndex: obj.index });
              }
            }
          });
          switch (action) {
            case "expandRow":
              var expandedIndexes = tableState.expandedRows.data.map(function (
                value
              ) {
                return value.dataIndex;
              });
              this.FixarDesfixarLinha("Orcamento", expandedIndexes);
              this.setState({ expandedIndexes: expandedIndexes });
              break;
            default:
              break;
          }

          this.Pesquisa_Filtros(tableState);
        },
        isRowExpandable: (dataIndex, expandedRows) => {
          if (
            dataIndex === this.state.DespesaIndex ||
            dataIndex === this.state.ReceitaIndex
          )
            return false;
          return true;
        },
        rowsExpanded: this.state.persistenciaLinhas.Orcamento,
        textLabels: global.textLabels,
        expandableRows: true,
        expandableRowsOnClick: false,
        renderExpandableRow: (rowData, rowMeta) => {
          //Busca categorias filhas do orçamento
          var childsOrcamentoCategoria = orcamentoChilds[rowData[0]];

          if (childsOrcamentoCategoria == null) {
            childsOrcamentoCategoria = [];
          }

          childsOrcamentoCategoria =
            this.obterPorcentagemDespesaOrcamentosCategorias(
              childsOrcamentoCategoria
            );

          const colSpan = 20;

          const columnsOrcamentoCategoria = [
            {
              name: "Controle",
              label: "Controle",
              options: {
                display: true,
                viewColumns: false,
                filter: false,
                sort: false,
                customBodyRender: (value, tableMeta, updateValue) => {
                  return (
                    <div
                      style={{
                        backgroundColor: this.obterCor(tableMeta.rowData[15]),
                        padding: "16px",
                      }}
                    >
                      {/* se é receita ou despesa aparece o ícone para deletar */}
                      {tableMeta.rowData[2] != null &&
                        tableMeta.rowData[1] !== "" &&
                        tableMeta.rowData[15] < 2 && (
                          <i
                            style={{ cursor: "pointer" }}
                            onClick={(e) => {
                              e.stopPropagation();
                              var categoriaId = tableMeta.rowData[2];
                              this.confirmaExcluirCategoria(categoriaId);
                            }}
                            className="far fa-trash-alt"
                          ></i>
                        )}

                      <i
                        style={{ cursor: "pointer", marginLeft: "15px" }}
                        onClick={(e) => {
                          e.stopPropagation();
                          var categoriaId = tableMeta.rowData[2];
                          //var service = `api/dashboard/categoria-previsto-realizado-mes-a-mes/${this.state.familiaId}/${categoriaId}`;
                          var service = `${global.apiToken}/dashboard/categoria-previsto-realizado-mes-a-mes/${this.state.familiaId}/${categoriaId}`;
                          var url = global.server_api_new + service;
                          var config = {
                            headers: {
                              Authorization: "bearer " + this.state.accessToken,
                              "Access-Control-Allow-Origin": "*",
                              "Access-Control-Allow-Headers": "Authorization",
                              "Access-Control-Allow-Methods":
                                "GET, POST, OPTIONS, PUT, PATCH, DELETE",
                            },
                          };

                          var filtro = {
                            DataInicial: this.state.startDate,
                            DataFinal: this.state.endDate,
                            Ordenacao: "Previsto Desc",
                          };
                          this.setState({ periodosCategoria: null });
                          axios.post(url, filtro, config).then((res) => {
                            this.setState({
                              periodosCategoria: res.data.results,
                              categoriaGraficoId: categoriaId,
                            });
                          });
                          global.showModal(this.modalGraficoCategoria);
                          if (this.props.putNavBack) {
                            this.props.putNavBack();
                          }
                        }}
                        className="fas fa-chart-bar"
                      ></i>
                    </div>
                  );
                },
              },
            },
            {
              name: "OrcamentoId",
              options: {
                display: false,
                viewColumns: false,
                filter: false,
              },
            },
            {
              name: "CategoriaId",
              options: {
                display: false,
                viewColumns: false,
                filter: false,
              },
            },
            {
              name: "NomeCategoria",
              label: "Categoria",
              options: {
                display: true,
                viewColumns: false,
                filter: true,
                customBodyRender: (value, tableMeta, updateValue) => {
                  return (
                    <div
                      style={{
                        cursor: "pointer",
                        backgroundColor: this.obterCor(tableMeta.rowData[15]),
                        padding: "16px",
                      }}
                      onClick={() => {
                        var categoriaId = tableMeta.rowData[2];
                        this.editarCategoria(categoriaId);
                      }}
                    >
                      {value}
                    </div>
                  );
                },
              },
            },
            {
              name: "NomeMeioPagamento",
              label: "Meio de Pagto. Padrão",
              options: {
                display:
                  $this.state.persistenciaColunas.OrcamentoCategoria
                    .NomeMeioPagamento,
                viewColumns: true,
                filter: true,
                customBodyRender: (value, tableMeta, updateValue) => {
                  return (
                    <div
                      style={{
                        cursor: "pointer",
                        backgroundColor: this.obterCor(tableMeta.rowData[15]),
                        padding: "16px",
                      }}
                      onClick={() => {
                        var categoriaId = tableMeta.rowData[2];
                        this.editarCategoria(categoriaId);
                      }}
                    >
                      {value}
                    </div>
                  );
                },
              },
            },
            {
              name: "Frequencia",
              label: "Frequência",
              options: {
                display:
                  $this.state.persistenciaColunas.OrcamentoCategoria.Frequencia,
                viewColumns: true,
                filter: true,
                customBodyRender: (value, tableMeta, updateValue) => {
                  return (
                    <div
                      style={{
                        cursor: "pointer",
                        backgroundColor: this.obterCor(tableMeta.rowData[15]),
                        padding: "16px",
                      }}
                      onClick={() => {
                        var categoriaId = tableMeta.rowData[2];
                        this.editarCategoria(categoriaId);
                      }}
                    >
                      {value}
                    </div>
                  );
                },
              },
            },
            {
              name: "Grupo",
              label: "Grupo",
              options: {
                display:
                  $this.state.persistenciaColunas.OrcamentoCategoria.Grupo,
                viewColumns: true,
                filter: true,
                customBodyRender: (value, tableMeta, updateValue) => {
                  return (
                    <div
                      style={{
                        cursor: "pointer",
                        backgroundColor: this.obterCor(tableMeta.rowData[15]),
                        padding: "16px",
                      }}
                      onClick={() => {
                        var categoriaId = tableMeta.rowData[2];
                        this.editarCategoria(categoriaId);
                      }}
                    >
                      {value}
                    </div>
                  );
                },
              },
            },
            {
              name: "Estimado",
              label: "Estimado",
              options: {
                display:
                  $this.state.persistenciaColunas.OrcamentoCategoria.Estimado,
                viewColumns: true,
                filter: true,
                customBodyRender: (value, tableMeta, updateValue) => {
                  //Se tem categoria e orçamento E tipo de orçamento é despesa / receita
                  if (
                    tableMeta.rowData[2] != null &&
                    tableMeta.rowData[1] !== "" &&
                    tableMeta.rowData[15] < 2
                  ) {
                    var formatado = value
                      .toLocaleString("pt-BR", {
                        style: "currency",
                        currency: "BRL",
                      })
                      .replace("R$", "");
                    setTimeout(function () {
                      $("#" + tableMeta.rowData[2]).val(formatado);
                    }, 50);
                    return (
                      <div
                        style={{
                          backgroundColor: this.obterCor(tableMeta.rowData[15]),
                          padding: "12px",
                        }}
                      >
                        <input
                          type="text"
                          id={tableMeta.rowData[2]}
                          ref={this.valorEstimado}
                          onClick={(e) => this.handleOnClick(e, updateValue)}
                          onKeyUp={(e) => this.handleOnClick(e, updateValue)}
                        />
                      </div>
                    );
                  } else {
                    return (
                      <div
                        style={{
                          cursor: "pointer",
                          backgroundColor: this.obterCor(tableMeta.rowData[15]),
                          padding: "16px",
                        }}
                        onClick={() => {
                          var categoriaId = tableMeta.rowData[2];
                          this.editarCategoria(categoriaId);
                        }}
                      >
                        {value != null &&
                          value
                            .toLocaleString("pt-BR", {
                              style: "currency",
                              currency: "BRL",
                            })
                            .replace("R$", "")}
                      </div>
                    );
                  }
                },
              },
            },
            {
              name: "TotalMedio",
              label: "Realizado mensal",
              options: {
                display:
                  $this.state.persistenciaColunas.OrcamentoCategoria.TotalMedio,
                viewColumns: true,
                filter: true,
                customBodyRender: (value, tableMeta, updateValue) => {
                  return (
                    <div
                      style={{
                        cursor: "pointer",
                        backgroundColor: this.obterCor(tableMeta.rowData[15]),
                        padding: "16px",
                      }}
                      onClick={() => {
                        var categoriaId = tableMeta.rowData[2];
                        this.editarCategoria(categoriaId);
                      }}
                    >
                      {value != null &&
                        value
                          .toLocaleString("pt-BR", {
                            style: "currency",
                            currency: "BRL",
                          })
                          .replace("R$", "")}
                    </div>
                  );
                },
              },
            },
            {
              name: "Quantidade",
              label: "Quantidade",
              options: {
                display:
                  $this.state.persistenciaColunas.OrcamentoCategoria.Quantidade,
                viewColumns: true,
                filter: false,
                customBodyRender: (value, tableMeta, updateValue) => {
                  return (
                    <div
                      style={{
                        cursor: "pointer",
                        backgroundColor: this.obterCor(tableMeta.rowData[15]),
                        padding: "16px",
                      }}
                      onClick={() => {
                        var categoriaId = tableMeta.rowData[2];
                        this.editarCategoria(categoriaId);
                      }}
                    >
                      {value}
                    </div>
                  );
                },
              },
            },
            {
              name: "Total",
              label: "Total",
              options: {
                display:
                  $this.state.persistenciaColunas.OrcamentoCategoria.Total,
                viewColumns: true,
                filter: false,
                customBodyRender: (value, tableMeta, updateValue) => {
                  return (
                    <div
                      style={{
                        cursor: "pointer",
                        backgroundColor: this.obterCor(tableMeta.rowData[15]),
                        padding: "16px",
                      }}
                      onClick={() => {
                        var categoriaId = tableMeta.rowData[2];
                        this.editarCategoria(categoriaId);
                      }}
                    >
                      {value != null &&
                        value
                          .toLocaleString("pt-BR", {
                            style: "currency",
                            currency: "BRL",
                          })
                          .replace("R$", "")}
                    </div>
                  );
                },
              },
            },
            {
              name: "Media",
              label: "Média",
              options: {
                display:
                  $this.state.persistenciaColunas.OrcamentoCategoria.Media,
                viewColumns: true,
                filter: false,
                customBodyRender: (value, tableMeta, updateValue) => {
                  return (
                    <div
                      style={{
                        cursor: "pointer",
                        backgroundColor: this.obterCor(tableMeta.rowData[15]),
                        padding: "16px",
                      }}
                      onClick={() => {
                        var categoriaId = tableMeta.rowData[2];
                        this.editarCategoria(categoriaId);
                      }}
                    >
                      {tableMeta.rowData[10] > 0 && tableMeta.rowData[9] > 0
                        ? (tableMeta.rowData[10] / tableMeta.rowData[9])
                            .toLocaleString("pt-BR", {
                              style: "currency",
                              currency: "BRL",
                            })
                            .replace("R$", "")
                        : "0,00"}
                    </div>
                  );
                },
              },
            },
            {
              name: "Minimo",
              label: "Mínimo",
              options: {
                display:
                  $this.state.persistenciaColunas.OrcamentoCategoria.Minimo,
                viewColumns: true,
                filter: false,
                customBodyRender: (value, tableMeta, updateValue) => {
                  return (
                    <div
                      style={{
                        cursor: "pointer",
                        backgroundColor: this.obterCor(tableMeta.rowData[15]),
                        padding: "16px",
                      }}
                      onClick={() => {
                        var categoriaId = tableMeta.rowData[2];
                        this.editarCategoria(categoriaId);
                      }}
                    >
                      {value != null &&
                        value
                          .toLocaleString("pt-BR", {
                            style: "currency",
                            currency: "BRL",
                          })
                          .replace("R$", "")}
                    </div>
                  );
                },
              },
            },
            {
              name: "Maximo",
              label: "Máximo",
              options: {
                display:
                  $this.state.persistenciaColunas.OrcamentoCategoria.Maximo,
                viewColumns: true,
                filter: false,
                customBodyRender: (value, tableMeta, updateValue) => {
                  return (
                    <div
                      style={{
                        cursor: "pointer",
                        backgroundColor: this.obterCor(tableMeta.rowData[15]),
                        padding: "16px",
                      }}
                      onClick={() => {
                        var categoriaId = tableMeta.rowData[2];
                        this.editarCategoria(categoriaId);
                      }}
                    >
                      {value != null &&
                        value
                          .toLocaleString("pt-BR", {
                            style: "currency",
                            currency: "BRL",
                          })
                          .replace("R$", "")}
                    </div>
                  );
                },
              },
            },
            {
              name: "Suspenso",
              label: "Suspenso",
              options: {
                display:
                  $this.state.persistenciaColunas.OrcamentoCategoria.Suspenso,
                viewColumns: true,
                filter: false,
                customBodyRender: (value, tableMeta, updateValue) => {
                  return (
                    <div
                      style={{
                        cursor: "pointer",
                        backgroundColor: this.obterCor(tableMeta.rowData[15]),
                        padding: "16px",
                      }}
                      onClick={() => {
                        var categoriaId = tableMeta.rowData[2];
                        this.editarCategoria(categoriaId);
                      }}
                    >
                      {value != null && value === 1 ? "Sim" : "Não"}
                    </div>
                  );
                },
              },
            },
            {
              name: "TipoOrcamento",
              options: {
                display: false,
                viewColumns: false,
                filter: false,
              },
            },
            {
              name: "PercentualDespesas",
              label: "Percentual de Despesas",
              options: {
                display:
                  $this.state.persistenciaColunas.OrcamentoCategoria
                    .PercentualDespesas,
                viewColumns: true,
                filter: false,
                customBodyRender: (value, tableMeta) => {
                  return (
                    <span
                      style={{
                        cursor: "pointer",
                        backgroundColor: this.obterCor(tableMeta.rowData[15]),
                        padding: "16px",
                      }}
                    >
                      {value.toLocaleString("pt-BR", {
                        style: "percent",
                        minimumFractionDigits: 2,
                        maximumFractionDigits: 2,
                      })}
                    </span>
                  );
                },
              },
            },
          ];

          const optionsOrcamentoCategoria = {
            onColumnViewChange: (changedColumn, action) => {
              switch (action) {
                case "add":
                  this.FixarDesfixarColuna(
                    "OrcamentoCategoria",
                    changedColumn,
                    true
                  );
                  break;
                case "remove":
                  this.FixarDesfixarColuna(
                    "OrcamentoCategoria",
                    changedColumn,
                    false
                  );
                  break;
                default:
                  break;
              }
            },
            onTableChange: (action, tableState) => {
              switch (action) {
                case "expandRow":
                  var expandedIndexes = tableState.expandedRows.data.map(
                    function (value) {
                      return value.dataIndex;
                    }
                  );
                  this.FixarDesfixarLinha(
                    "OrcamentoCategoria",
                    expandedIndexes
                  );
                  this.setState({ expandedIndexes: expandedIndexes });
                  break;
                default:
                  break;
              }
            },
            rowsExpanded: this.state.persistenciaLinhas.OrcamentoCategoria,
            textLabels: global.textLabels,
            filter: false,
            download: false,
            print: false,
            selectableRows: false,
            filterType: "dropdown",
            responsive: "stacked",
            pagination: false,
            sort: true,
            expandableRows: true,
            expandableRowsOnClick: false,
            renderExpandableRow: (rowData, rowMeta) => {
              if (rowData[1] === -1) {
                return;
              }

              var lancamentosPorCategoria = lancamentos.filter(function (
                arrItem
              ) {
                return arrItem.categoriaId === rowData[2];
              });

              if (!lancamentosPorCategoria) {
                lancamentosPorCategoria = [];
              }

              const columnsLancamentos = [
                {
                  name: "id",
                  options: {
                    display: false,
                    viewColumns: false,
                    filter: false,
                  },
                },
                {
                  name: "data",
                  label: "Data",
                  options: {
                    display: $this.state.persistenciaColunas.Lancamentos.data,
                    viewColumns: true,
                    filter: false,
                  },
                },
                {
                  name: "descricao",
                  label: "Descrição",
                  options: {
                    display:
                      $this.state.persistenciaColunas.Lancamentos.descricao,
                    viewColumns: true,
                    filter: false,
                  },
                },
                {
                  name: "valor",
                  label: "Valor",
                  options: {
                    display: $this.state.persistenciaColunas.Lancamentos.valor,
                    viewColumns: true,
                    filter: false,
                    customBodyRender: (value, tableMeta) => {
                      var valorReal = value;
                      if (
                        tableMeta.rowData[5] != null &&
                        tableMeta.rowData[5] !== ""
                      ) {
                        valorReal = tableMeta.rowData[5];
                      }

                      return (
                        <span>
                          {valorReal != null
                            ? valorReal
                                .toLocaleString("pt-BR", {
                                  style: "currency",
                                  currency: "BRL",
                                })
                                .replace("R$", "")
                            : "0,00"}
                        </span>
                      );
                    },
                  },
                },
                {
                  name: "nomeMeioPagamento",
                  label: "Meio de Pagamento",
                  options: {
                    display:
                      $this.state.persistenciaColunas.Lancamentos
                        .nomeMeioPagamento,
                    viewColumns: true,
                    filter: true,
                  },
                },
                {
                  name: "valorParcela",
                  options: {
                    display:
                      $this.state.persistenciaColunas.Lancamentos.valorParcela,
                    viewColumns: true,
                    filter: false,
                  },
                },
                {
                  name: "agrupamento",
                  label: "Parcelado?",
                  options: {
                    display:
                      $this.state.persistenciaColunas.Lancamentos.agrupamento,
                    viewColumns: true,
                    filter: false,
                    customBodyRender: (value, tableMeta) => {
                      if (value != null && value !== "") {
                        return (
                          <div>
                            Sim<span style={{ display: "none" }}>{value}</span>
                          </div>
                        );
                      } else {
                        return <span>Não</span>;
                      }
                    },
                  },
                },
                {
                  name: "operacao",
                  options: {
                    display:
                      $this.state.persistenciaColunas.Lancamentos.operacao,
                    viewColumns: true,
                    filter: false,
                  },
                },
                {
                  name: "tipoOperacao",
                  label: "Tipo de Operação",
                  options: {
                    display:
                      $this.state.persistenciaColunas.Lancamentos.tipoOperacao,
                    viewColumns: true,
                    filter: true,
                  },
                },
              ];
              const optionsLancamentos = {
                onColumnViewChange: (changedColumn, action) => {
                  switch (action) {
                    case "add":
                      this.FixarDesfixarColuna(
                        "Lancamentos",
                        changedColumn,
                        true
                      );
                      break;
                    case "remove":
                      this.FixarDesfixarColuna(
                        "Lancamentos",
                        changedColumn,
                        false
                      );
                      break;
                    default:
                      break;
                  }
                },
                onTableChange: (action, tableState) => {
                  switch (action) {
                    case "expandRow":
                      var expandedIndexes = tableState.expandedRows.data.map(
                        function (value) {
                          return value.dataIndex;
                        }
                      );
                      this.setState({ expandedIndexes: expandedIndexes });
                      break;
                    default:
                      break;
                  }
                },
                textLabels: global.textLabels,
                filter: false,
                download: false,
                print: false,
                selectableRows: true,
                filterType: "dropdown",
                responsive: "stacked",
                pagination: false,
                expandableRows: false,
                expandableRowsOnClick: false,
                onRowClick: (rowData, rowMeta) => {
                  this.editarLancamento(rowData);
                },
                onRowsDelete: (rowsDeleted) => {
                  this.excluirLancamentos(rowsDeleted, lancamentosPorCategoria);
                  return false;
                },
              };
              return (
                <TableRow>
                  <TableCell colSpan={colSpan}>
                    <div
                      className="table-lancamentos-categoria"
                      style={{ padding: "0 40px" }}
                    >
                      <MUIDataTable
                        data={lancamentosPorCategoria}
                        columns={columnsLancamentos}
                        options={optionsLancamentos}
                      />
                    </div>
                  </TableCell>
                </TableRow>
              );
            },
            customFooter: (
              count,
              page,
              rowsPerPage,
              changeRowsPerPage,
              changePage,
              textLabels
            ) => {
              return (
                <CustomFooter
                  count={count}
                  page={page}
                  rowsPerPage={rowsPerPage}
                  changeRowsPerPage={changeRowsPerPage}
                  changePage={changePage}
                  textLabels={textLabels}
                  hasPagination={false}
                />
              );
            },
          };
          return (
            <TableRow>
              <TableCell colSpan={colSpan}>
                <div id="orcamento-table">
                  <MUIDataTable
                    data={childsOrcamentoCategoria}
                    columns={columnsOrcamentoCategoria}
                    options={optionsOrcamentoCategoria}
                  />
                </div>
              </TableCell>
            </TableRow>
          );
        },
        filter: true,
        filterType: "dropdown",
        responsive: "stacked",
        print: false,
        download: false,
        sort: true,
        selectableRows: "none",
        pagination: false,
        customFooter: (
          count,
          page,
          rowsPerPage,
          changeRowsPerPage,
          changePage,
          textLabels
        ) => {
          var Sumario = this.state.Sumario;
          var sumarioLinha1 = [
            `Receitas estimadas: ${Sumario.Orcamento.ReceitasEstimadas.toLocaleString(
              "pt-BR",
              {
                style: "currency",
                currency: "BRL",
              }
            )}`,
            `Receitas realizadas: ${Sumario.Orcamento.ReceitasRealizadas.toLocaleString(
              "pt-BR",
              {
                style: "currency",
                currency: "BRL",
              }
            )}`,
          ];

          var sumarioLinha2 = [
            `Despesas estimadas: ${Sumario.Orcamento.DespesasEstimadas.toLocaleString(
              "pt-BR",
              {
                style: "currency",
                currency: "BRL",
              }
            )}`,
            `Despesas realizadas: ${Sumario.Orcamento.DespesasRealizadas.toLocaleString(
              "pt-BR",
              {
                style: "currency",
                currency: "BRL",
              }
            )}`,
          ];

          var sumarioLinha3 = [
            `Investimentos estimadas: ${Sumario.Orcamento.InvestimentosEstimados.toLocaleString(
              "pt-BR",
              {
                style: "currency",
                currency: "BRL",
              }
            )}`,
            `Investimentos realizadas: ${Sumario.Orcamento.InvestimentosRealizados.toLocaleString(
              "pt-BR",
              {
                style: "currency",
                currency: "BRL",
              }
            )}`,
          ];

          var sumarioLinha4 = [
            `Dívidas estimadas: ${Sumario.Orcamento.DividasEstimadas.toLocaleString(
              "pt-BR",
              {
                style: "currency",
                currency: "BRL",
              }
            )}`,
            `Dívidas realizadas: ${Sumario.Orcamento.DividasRealizadas.toLocaleString(
              "pt-BR",
              {
                style: "currency",
                currency: "BRL",
              }
            )}`,
          ];

          return (
            <CustomFooter
              count={count}
              page={page}
              rowsPerPage={rowsPerPage}
              changeRowsPerPage={changeRowsPerPage}
              changePage={changePage}
              textLabels={textLabels}
              data2={sumarioLinha1}
              data4={sumarioLinha2}
              data5={sumarioLinha3}
              data6={sumarioLinha4}
              hasPagination={false}
            />
          );
        },
      };

      // optionsOrcamento.rowsExpanded = agrupados;

      const columnsOrcamento = [
        {
          name: "OrcamentoId",
          options: {
            display: false,
            viewColumns: false,
            filter: false,
          },
        },
        {
          name: "CategoriaId",
          options: {
            display: false,
            viewColumns: false,
            filter: false,
          },
        },
        {
          name: "Controle",
          label: "Controle",
          options: {
            display: true,
            viewColumns: false,
            filter: false,
            sort: false,
            customBodyRender: (value, tableMeta, updateValue) => {
              return (
                <div
                  style={{
                    backgroundColor: this.obterCor(tableMeta.rowData[12]),
                    padding: "16px",
                  }}
                >
                  {tableMeta.rowData[3] !== "Investimentos" &&
                  tableMeta.rowData[3] !== "Dívidas" &&
                  tableMeta.rowData[3] !== "Despesa não Categorizada" &&
                  tableMeta.rowData[3] !== "Receita não Categorizada" ? (
                    <i
                      style={{ cursor: "pointer" }}
                      onClick={(e) => {
                        e.stopPropagation();
                        var orcamentoId = tableMeta.rowData[0];
                        this.confirmaExcluirOrcamento(orcamentoId);
                      }}
                      className="far fa-trash-alt"
                    ></i>
                  ) : (
                    ""
                  )}
                  <i
                    style={{ cursor: "pointer", marginLeft: "15px" }}
                    onClick={async (e) => {
                      var orcamentoId = tableMeta.rowData[0];
                      await this.setState({
                        orcamentoSelecionado: orcamentoId,
                      });
                      this.loadOrcamentoGrafico();
                      global.showModal(this.modal3);
                      if (this.props.putNavBack) {
                        this.props.putNavBack();
                      }
                    }}
                    className="fas fa-chart-bar"
                  ></i>
                </div>
              );
            },
          },
        },
        {
          name: "NomeOrcamento",
          label: "Orçamento",
          options: {
            display: true,
            viewColumns: false,
            filter: true,
            customBodyRender: (value, tableMeta) => {
              if (
                tableMeta.rowData[3] !== "Despesa não Categorizada" &&
                tableMeta.rowData[3] !== "Receita não Categorizada"
              ) {
                return (
                  <div
                    style={{
                      cursor: "pointer",
                      backgroundColor: this.obterCor(tableMeta.rowData[12]),
                      padding: "16px",
                    }}
                    onClick={() => {
                      var orcamentoId = tableMeta.rowData[0];
                      this.editarRegistro(orcamentoId);
                    }}
                  >
                    {value}
                  </div>
                );
              } else {
                return (
                  <NavLink
                    style={{
                      cursor: "pointer",
                      backgroundColor: this.obterCor(tableMeta.rowData[12]),
                      padding: "16px",
                      color: "black",
                    }}
                    to="/admin/extrato"
                    tag={Link}
                    onClick={() => {
                      sessionStorage.setItem("checkedNotCategorized", true);
                    }}
                  >
                    {value}
                  </NavLink>
                );
              }
            },
          },
        },
        {
          name: "Estimado",
          label: "Estimado",
          options: {
            display: $this.state.persistenciaColunas.Orcamento.Estimado,
            viewColumns: true,
            filter: false,
            customBodyRender: (value, tableMeta) => {
              if (
                tableMeta.rowData[3] !== "Despesa não Categorizada" &&
                tableMeta.rowData[3] !== "Receita não Categorizada"
              ) {
                return (
                  <div
                    id={"orcamento-total-" + tableMeta.rowData[0]}
                    style={{
                      cursor: "pointer",
                      backgroundColor: this.obterCor(tableMeta.rowData[12]),
                      padding: "16px",
                    }}
                    onClick={() => {
                      var orcamentoId = tableMeta.rowData[0];
                      this.editarRegistro(orcamentoId);
                    }}
                  >
                    {value != null &&
                      value
                        .toLocaleString("pt-BR", {
                          style: "currency",
                          currency: "BRL",
                        })
                        .replace("R$", "")}
                  </div>
                );
              } else {
                return (
                  <NavLink
                    style={{
                      cursor: "pointer",
                      backgroundColor: this.obterCor(tableMeta.rowData[12]),
                      padding: "16px",
                      color: "black",
                    }}
                    to="/admin/extrato"
                    tag={Link}
                    onClick={() => {
                      sessionStorage.setItem("checkedNotCategorized", true);
                    }}
                  >
                    {value != null &&
                      value
                        .toLocaleString("pt-BR", {
                          style: "currency",
                          currency: "BRL",
                        })
                        .replace("R$", "")}
                  </NavLink>
                );
              }
            },
          },
        },
        {
          name: "TotalMedio",
          label: "Realizado mensal",
          options: {
            display: $this.state.persistenciaColunas.Orcamento.TotalMedio,
            viewColumns: true,
            filter: false,
            customBodyRender: (value, tableMeta) => {
              if (
                tableMeta.rowData[3] !== "Despesa não Categorizada" &&
                tableMeta.rowData[3] !== "Receita não Categorizada"
              ) {
                return (
                  <div
                    id={"orcamento-total-" + tableMeta.rowData[0]}
                    style={{
                      cursor: "pointer",
                      backgroundColor: this.obterCor(tableMeta.rowData[12]),
                      padding: "16px",
                    }}
                    onClick={() => {
                      var orcamentoId = tableMeta.rowData[0];
                      this.editarRegistro(orcamentoId);
                    }}
                  >
                    {value != null &&
                      value
                        .toLocaleString("pt-BR", {
                          style: "currency",
                          currency: "BRL",
                        })
                        .replace("R$", "")}
                  </div>
                );
              } else {
                return (
                  <NavLink
                    style={{
                      cursor: "pointer",
                      backgroundColor: this.obterCor(tableMeta.rowData[12]),
                      padding: "16px",
                      color: "black",
                    }}
                    to="/admin/extrato"
                    tag={Link}
                    onClick={() => {
                      sessionStorage.setItem("checkedNotCategorized", true);
                    }}
                  >
                    {value != null &&
                      value
                        .toLocaleString("pt-BR", {
                          style: "currency",
                          currency: "BRL",
                        })
                        .replace("R$", "")}
                  </NavLink>
                );
              }
            },
          },
        },
        {
          name: "Quantidade",
          label: "Quantidade",
          options: {
            display: $this.state.persistenciaColunas.Orcamento.Quantidade,
            viewColumns: true,
            filter: false,
            customBodyRender: (value, tableMeta) => {
              if (
                tableMeta.rowData[3] !== "Despesa não Categorizada" &&
                tableMeta.rowData[3] !== "Receita não Categorizada"
              ) {
                return (
                  <div
                    id={"orcamento-total-" + tableMeta.rowData[0]}
                    style={{
                      cursor: "pointer",
                      backgroundColor: this.obterCor(tableMeta.rowData[12]),
                      padding: "16px",
                    }}
                    onClick={() => {
                      var orcamentoId = tableMeta.rowData[0];
                      this.editarRegistro(orcamentoId);
                    }}
                  >
                    {value}
                  </div>
                );
              } else {
                return (
                  <NavLink
                    style={{
                      cursor: "pointer",
                      backgroundColor: this.obterCor(tableMeta.rowData[12]),
                      padding: "16px",
                      color: "black",
                    }}
                    to="/admin/extrato"
                    tag={Link}
                    onClick={() => {
                      sessionStorage.setItem("checkedNotCategorized", true);
                    }}
                  >
                    {value}
                  </NavLink>
                );
              }
            },
          },
        },
        {
          name: "Total",
          label: "Total",
          options: {
            display: $this.state.persistenciaColunas.Orcamento.Total,
            viewColumns: true,
            filter: false,
            customBodyRender: (value, tableMeta) => {
              if (
                tableMeta.rowData[3] !== "Despesa não Categorizada" &&
                tableMeta.rowData[3] !== "Receita não Categorizada"
              ) {
                return (
                  <div
                    id={"orcamento-total-" + tableMeta.rowData[0]}
                    style={{
                      cursor: "pointer",
                      backgroundColor: this.obterCor(tableMeta.rowData[12]),
                      padding: "16px",
                    }}
                    onClick={() => {
                      var orcamentoId = tableMeta.rowData[0];
                      this.editarRegistro(orcamentoId);
                    }}
                  >
                    {value != null &&
                      value
                        .toLocaleString("pt-BR", {
                          style: "currency",
                          currency: "BRL",
                        })
                        .replace("R$", "")}
                  </div>
                );
              } else {
                return (
                  <NavLink
                    style={{
                      cursor: "pointer",
                      backgroundColor: this.obterCor(tableMeta.rowData[12]),
                      padding: "16px",
                      color: "black",
                    }}
                    to="/admin/extrato"
                    tag={Link}
                    onClick={() => {
                      sessionStorage.setItem("checkedNotCategorized", true);
                    }}
                  >
                    {value != null &&
                      value
                        .toLocaleString("pt-BR", {
                          style: "currency",
                          currency: "BRL",
                        })
                        .replace("R$", "")}
                  </NavLink>
                );
              }
            },
          },
        },
        {
          name: "Media",
          label: "Média",
          options: {
            display: $this.state.persistenciaColunas.Orcamento.Media,
            viewColumns: true,
            filter: false,
            customBodyRender: (value, tableMeta) => {
              if (
                tableMeta.rowData[3] !== "Despesa não Categorizada" &&
                tableMeta.rowData[3] !== "Receita não Categorizada"
              ) {
                return (
                  <div
                    id={"orcamento-total-" + tableMeta.rowData[0]}
                    style={{
                      cursor: "pointer",
                      backgroundColor: this.obterCor(tableMeta.rowData[12]),
                      padding: "16px",
                    }}
                    onClick={() => {
                      var orcamentoId = tableMeta.rowData[0];
                      this.editarRegistro(orcamentoId);
                    }}
                  >
                    {tableMeta.rowData[7] > 0 && tableMeta.rowData[6] > 0
                      ? (tableMeta.rowData[7] / tableMeta.rowData[6])
                          .toLocaleString("pt-BR", {
                            style: "currency",
                            currency: "BRL",
                          })
                          .replace("R$", "")
                      : "0,00"}
                  </div>
                );
              } else {
                return (
                  <NavLink
                    style={{
                      cursor: "pointer",
                      backgroundColor: this.obterCor(tableMeta.rowData[12]),
                      padding: "16px",
                      color: "black",
                    }}
                    to="/admin/extrato"
                    tag={Link}
                    onClick={() => {
                      sessionStorage.setItem("checkedNotCategorized", true);
                    }}
                  >
                    {tableMeta.rowData[7] > 0 && tableMeta.rowData[6] > 0
                      ? (tableMeta.rowData[7] / tableMeta.rowData[6])
                          .toLocaleString("pt-BR", {
                            style: "currency",
                            currency: "BRL",
                          })
                          .replace("R$", "")
                      : "0,00"}
                  </NavLink>
                );
              }
            },
          },
        },
        {
          name: "Minimo",
          label: "Mínimo",
          options: {
            display: $this.state.persistenciaColunas.Orcamento.Minimo,
            viewColumns: true,
            filter: false,
            customBodyRender: (value, tableMeta) => {
              if (
                tableMeta.rowData[3] !== "Despesa não Categorizada" &&
                tableMeta.rowData[3] !== "Receita não Categorizada"
              ) {
                return (
                  <div
                    id={"orcamento-total-" + tableMeta.rowData[0]}
                    style={{
                      cursor: "pointer",
                      backgroundColor: this.obterCor(tableMeta.rowData[12]),
                      padding: "16px",
                    }}
                    onClick={() => {
                      var orcamentoId = tableMeta.rowData[0];
                      this.editarRegistro(orcamentoId);
                    }}
                  >
                    {value != null &&
                      value
                        .toLocaleString("pt-BR", {
                          style: "currency",
                          currency: "BRL",
                        })
                        .replace("R$", "")}
                  </div>
                );
              } else {
                return (
                  <NavLink
                    style={{
                      cursor: "pointer",
                      backgroundColor: this.obterCor(tableMeta.rowData[12]),
                      padding: "16px",
                      color: "black",
                    }}
                    to="/admin/extrato"
                    tag={Link}
                    onClick={() => {
                      sessionStorage.setItem("checkedNotCategorized", true);
                    }}
                  >
                    {value != null &&
                      value
                        .toLocaleString("pt-BR", {
                          style: "currency",
                          currency: "BRL",
                        })
                        .replace("R$", "")}
                  </NavLink>
                );
              }
            },
          },
        },
        {
          name: "Maximo",
          label: "Máximo",
          options: {
            display: $this.state.persistenciaColunas.Orcamento.Maximo,
            viewColumns: true,
            filter: false,
            customBodyRender: (value, tableMeta) => {
              if (
                tableMeta.rowData[3] !== "Despesa não Categorizada" &&
                tableMeta.rowData[3] !== "Receita não Categorizada"
              ) {
                return (
                  <div
                    id={"orcamento-total-" + tableMeta.rowData[0]}
                    style={{
                      cursor: "pointer",
                      backgroundColor: this.obterCor(tableMeta.rowData[12]),
                      padding: "16px",
                    }}
                    onClick={() => {
                      var orcamentoId = tableMeta.rowData[0];
                      this.editarRegistro(orcamentoId);
                    }}
                  >
                    {value != null &&
                      value
                        .toLocaleString("pt-BR", {
                          style: "currency",
                          currency: "BRL",
                        })
                        .replace("R$", "")}
                  </div>
                );
              } else {
                return (
                  <NavLink
                    style={{
                      cursor: "pointer",
                      backgroundColor: this.obterCor(tableMeta.rowData[12]),
                      padding: "16px",
                      color: "black",
                    }}
                    to="/admin/extrato"
                    tag={Link}
                    onClick={() => {
                      sessionStorage.setItem("checkedNotCategorized", true);
                    }}
                  >
                    {value != null &&
                      value
                        .toLocaleString("pt-BR", {
                          style: "currency",
                          currency: "BRL",
                        })
                        .replace("R$", "")}
                  </NavLink>
                );
              }
            },
          },
        },
        {
          name: "Suspenso",
          label: "Suspenso",
          options: {
            display: $this.state.persistenciaColunas.Orcamento.Suspenso,
            viewColumns: true,
            filter: false,
            customBodyRender: (value, tableMeta) => {
              if (
                tableMeta.rowData[3] !== "Despesa não Categorizada" &&
                tableMeta.rowData[3] !== "Receita não Categorizada"
              ) {
                return (
                  <div
                    id={"orcamento-total-" + tableMeta.rowData[0]}
                    style={{
                      cursor: "pointer",
                      backgroundColor: this.obterCor(tableMeta.rowData[12]),
                      padding: "16px",
                    }}
                    onClick={() => {
                      var orcamentoId = tableMeta.rowData[0];
                      this.editarRegistro(orcamentoId);
                    }}
                  >
                    {value != null && value === 1 ? "Sim" : "Não"}
                  </div>
                );
              } else {
                return (
                  <NavLink
                    style={{
                      cursor: "pointer",
                      backgroundColor: this.obterCor(tableMeta.rowData[12]),
                      padding: "16px",
                      color: "black",
                    }}
                    to="/admin/extrato"
                    tag={Link}
                    onClick={() => {
                      sessionStorage.setItem("checkedNotCategorized", true);
                    }}
                  >
                    {value != null && value === 1 ? "Sim" : "Não"}
                  </NavLink>
                );
              }
            },
          },
        },
        {
          name: "TipoOrcamento",
          options: {
            display: false,
            viewColumns: false,
            filter: false,
          },
        },
        {
          name: "PercentualDespesas",
          label: "Percentual de Despesas",
          options: {
            display:
              $this.state.persistenciaColunas.Orcamento.PercentualDespesas,
            viewColumns: true,
            filter: false,
            customBodyRender: (value, tableMeta) => {
              if (
                tableMeta.rowData[3] !== "Despesa não Categorizada" &&
                tableMeta.rowData[3] !== "Receita não Categorizada"
              ) {
                return (
                  <div
                    id={"orcamento-total-" + tableMeta.rowData[0]}
                    style={{
                      cursor: "pointer",
                      backgroundColor: this.obterCor(tableMeta.rowData[12]),
                      padding: "16px",
                    }}
                    onClick={() => {
                      var orcamentoId = tableMeta.rowData[0];
                      this.editarRegistro(orcamentoId);
                    }}
                  >
                    {value.toLocaleString("pt-BR", {
                      style: "percent",
                      minimumFractionDigits: 2,
                      maximumFractionDigits: 2,
                    })}
                  </div>
                );
              } else {
                return (
                  <NavLink
                    style={{
                      cursor: "pointer",
                      backgroundColor: this.obterCor(tableMeta.rowData[12]),
                      padding: "16px",
                      color: "black",
                    }}
                    to="/admin/extrato"
                    tag={Link}
                    onClick={() => {
                      sessionStorage.setItem("checkedNotCategorized", true);
                    }}
                  >
                    {value.toLocaleString("pt-BR", {
                      style: "percent",
                      minimumFractionDigits: 2,
                      maximumFractionDigits: 2,
                    })}
                  </NavLink>
                );
              }
            },
          },
        },
      ];

      if (data == null || data.length === 0) {
        return <>Nenhum orçamento encontrado</>;
      }

      return (
        <MuiThemeProvider theme={this.getMuiTheme()}>
          <MUIDataTable
            title={
              <div
                style={{ float: "left" }}
                className="gestao-planos-grid-title"
              >
                {isLoading && (
                  <CircularProgress
                    size={24}
                    style={{ marginLeft: 15, position: "relative", top: 4 }}
                  />
                )}
                <div style={{ float: "left", margin: "10px" }}>
                  <span
                    style={{
                      float: "left",
                      fontWeight: "600",
                      marginTop: "7px",
                    }}
                  >
                    Expandir todas as categorias?
                  </span>{" "}
                  &nbsp;
                  <Switch
                    onChange={this.handleMostrarAgrupadosChange}
                    checked={this.state.mostrarAgrupado}
                  />
                </div>
                <div style={{ float: "left", margin: "10px" }}>
                  <span
                    style={{
                      float: "left",
                      fontWeight: "600",
                      marginTop: "7px",
                    }}
                  >
                    Mostrar suspensos?
                  </span>{" "}
                  &nbsp;
                  <Switch
                    onChange={this.handleMostrarSuspensoChange}
                    checked={this.state.mostrarSuspensos}
                  />
                </div>
              </div>
            }
            data={data}
            columns={columnsOrcamento}
            options={optionsOrcamento}
          />
        </MuiThemeProvider>
      );
    } else if (change === 1) {
      data = !this.state.mostrarSuspensos
        ? this.state.listaDeGrupos
        : this.state.listaDeGruposComSuspensos;
      var grupoChilds = !this.state.mostrarSuspensos
        ? this.state.listaDeCategoriasNoGrupo
        : this.state.listaDeCategoriasNoGrupoComSuspensos;

      // agrupados = [];
      // if (this.state.mostrarAgrupado === true) {
      //   agrupados = data.map(function (value, index) {
      //     return index;
      //   });
      // } else if (this.state.expandedIndexes != null) {
      //   agrupados = this.state.expandedIndexes;
      // }

      const optionsGrupo = {
        onColumnViewChange: (changedColumn, action) => {
          switch (action) {
            case "add":
              this.FixarDesfixarColuna("Grupo", changedColumn, true);
              break;
            case "remove":
              this.FixarDesfixarColuna("Grupo", changedColumn, false);
              break;
            default:
              break;
          }
        },
        onTableChange: (action, tableState) => {
          switch (action) {
            case "expandRow":
              var expandedIndexes = tableState.expandedRows.data.map(function (
                value
              ) {
                return value.dataIndex;
              });
              this.FixarDesfixarLinha("Grupo", expandedIndexes);
              this.setState({ expandedIndexes: expandedIndexes });
              break;
            default:
              break;
          }

          this.Pesquisa_Filtros(tableState);
        },
        rowsExpanded: this.state.persistenciaLinhas.Grupo,
        expandableRows: true,
        renderExpandableRow: (rowData, rowMeta) => {
          //Busca categorias filhas do grupo
          var childsGrupoCategoria = grupoChilds[rowData[2]];

          if (childsGrupoCategoria == null) {
            childsGrupoCategoria = [];
          }

          childsGrupoCategoria =
            this.obterPorcentagemDespesaGruposCategorias(childsGrupoCategoria);

          const colSpan = 20;

          const columnsGrupoCategoria = [
            {
              name: "Controle",
              label: "Controle",
              options: {
                display: true,
                viewColumns: false,
                filter: false,
                sort: false,
                customBodyRender: (value, tableMeta, updateValue) => {
                  return (
                    <div
                      style={{
                        backgroundColor: this.obterCor(tableMeta.rowData[15]),
                        padding: "16px",
                      }}
                    >
                      {tableMeta.rowData[3] !== "Investimentos" &&
                      tableMeta.rowData[3] !== "Dívidas" ? (
                        <i
                          style={{ cursor: "pointer" }}
                          onClick={(e) => {
                            e.stopPropagation();
                            var categoriaId = tableMeta.rowData[2];
                            this.confirmaExcluirCategoria(categoriaId);
                          }}
                          className="far fa-trash-alt"
                        ></i>
                      ) : (
                        ""
                      )}
                      <i
                        style={{ cursor: "pointer", marginLeft: "15px" }}
                        onClick={(e) => {
                          e.stopPropagation();
                          var categoriaId = tableMeta.rowData[2];
                          //var service = `api/dashboard/categoria-previsto-realizado-mes-a-mes/${this.state.familiaId}/${categoriaId}`;
                          var service = `${global.apiToken}/dashboard/categoria-previsto-realizado-mes-a-mes/${this.state.familiaId}/${categoriaId}`;
                          var url = global.server_api_new + service;
                          var config = {
                            headers: {
                              Authorization: "bearer " + this.state.accessToken,
                              "Access-Control-Allow-Origin": "*",
                              "Access-Control-Allow-Headers": "Authorization",
                              "Access-Control-Allow-Methods":
                                "GET, POST, OPTIONS, PUT, PATCH, DELETE",
                            },
                          };

                          var filtro = {
                            DataInicial: this.state.startDate,
                            DataFinal: this.state.endDate,
                            Ordenacao: "Previsto Desc",
                          };
                          this.setState({ periodosCategoria: null });
                          axios.post(url, filtro, config).then((res) => {
                            this.setState({
                              periodosCategoria: res.data.results,
                            });
                          });
                          global.showModal(this.modalGraficoCategoria);
                          if (this.props.putNavBack) {
                            this.props.putNavBack();
                          }
                        }}
                        className="fas fa-chart-bar"
                      ></i>
                    </div>
                  );
                },
              },
            },
            {
              name: "OrcamentoId",
              options: {
                display: false,
                viewColumns: false,
                filter: false,
              },
            },
            {
              name: "CategoriaId",
              options: {
                display: false,
                viewColumns: false,
                filter: false,
              },
            },
            {
              name: "NomeOrcamento",
              label: "Orçamento",
              options: {
                display: true,
                viewColumns: false,
                filter: true,
                customBodyRender: (value, tableMeta, updateValue) => {
                  return (
                    <div
                      style={{
                        cursor: "pointer",
                        backgroundColor: this.obterCor(tableMeta.rowData[15]),
                        padding: "16px",
                      }}
                      onClick={() => {
                        var orcamentoId = tableMeta.rowData[1];
                        this.editarRegistro(orcamentoId);
                      }}
                    >
                      {value}
                    </div>
                  );
                },
              },
            },
            {
              name: "NomeCategoria",
              label: "Categoria",
              options: {
                display: true,
                viewColumns: false,
                filter: true,
                customBodyRender: (value, tableMeta, updateValue) => {
                  return (
                    <div
                      style={{
                        cursor: "pointer",
                        backgroundColor: this.obterCor(tableMeta.rowData[15]),
                        padding: "16px",
                      }}
                      onClick={() => {
                        var categoriaId = tableMeta.rowData[2];
                        this.editarCategoria(categoriaId);
                      }}
                    >
                      {value}
                    </div>
                  );
                },
              },
            },
            {
              name: "NomeMeioPagamento",
              label: "Meio de Pagto. Padrão",
              options: {
                display:
                  $this.state.persistenciaColunas.GrupoCategoria
                    .NomeMeioPagamento,
                viewColumns: true,
                filter: true,
                customBodyRender: (value, tableMeta, updateValue) => {
                  return (
                    <div
                      style={{
                        backgroundColor: this.obterCor(tableMeta.rowData[15]),
                        padding: "16px",
                      }}
                    >
                      {value}
                    </div>
                  );
                },
              },
            },
            {
              name: "Frequencia",
              label: "Frequência",
              options: {
                display:
                  $this.state.persistenciaColunas.GrupoCategoria.Frequencia,
                viewColumns: true,
                filter: true,
                customBodyRender: (value, tableMeta, updateValue) => {
                  return (
                    <div
                      style={{
                        cursor: "pointer",
                        backgroundColor: this.obterCor(tableMeta.rowData[15]),
                        padding: "16px",
                      }}
                    >
                      {value}
                    </div>
                  );
                },
              },
            },
            {
              name: "Estimado",
              label: "Estimado",
              options: {
                display:
                  $this.state.persistenciaColunas.GrupoCategoria.Estimado,
                viewColumns: true,
                filter: true,
                customBodyRender: (value, tableMeta, updateValue) => {
                  //Se tem categoria e orçamento E tipo de orçamento é despesa / receita
                  if (
                    tableMeta.rowData[2] != null &&
                    tableMeta.rowData[1] !== "" &&
                    tableMeta.rowData[15] < 2 &&
                    tableMeta.rowData[9] < 1
                  ) {
                    var formatado = value
                      .toLocaleString("pt-BR", {
                        style: "currency",
                        currency: "BRL",
                      })
                      .replace("R$", "");
                    setTimeout(function () {
                      $("#" + tableMeta.rowData[2]).val(formatado);
                    }, 50);
                    return (
                      <div
                        style={{
                          cursor: "pointer",
                          backgroundColor: this.obterCor(tableMeta.rowData[15]),
                          padding: "12px",
                        }}
                      >
                        <input
                          type="text"
                          id={tableMeta.rowData[2]}
                          ref={this.valorEstimado}
                          onClick={(e) => this.handleOnClick(e, updateValue)}
                          onKeyUp={(e) => this.handleOnClick(e, updateValue)}
                        />
                      </div>
                    );
                  } else {
                    return (
                      <div
                        style={{
                          cursor: "pointer",
                          backgroundColor: this.obterCor(tableMeta.rowData[15]),
                          padding: "16px",
                        }}
                      >
                        {value != null &&
                          value
                            .toLocaleString("pt-BR", {
                              style: "currency",
                              currency: "BRL",
                            })
                            .replace("R$", "")}
                      </div>
                    );
                  }
                },
              },
            },
            {
              name: "TotalMedio",
              label: "Realizado mensal",
              options: {
                display:
                  $this.state.persistenciaColunas.GrupoCategoria.TotalMedio,
                viewColumns: true,
                filter: true,
                customBodyRender: (value, tableMeta, updateValue) => {
                  if (value == null) {
                    value = 0;
                  }
                  return (
                    <div
                      style={{
                        cursor: "pointer",
                        backgroundColor: this.obterCor(tableMeta.rowData[15]),
                        padding: "16px",
                      }}
                    >
                      {value != null &&
                        value
                          .toLocaleString("pt-BR", {
                            style: "currency",
                            currency: "BRL",
                          })
                          .replace("R$", "")}
                    </div>
                  );
                },
              },
            },
            {
              name: "OrcamentoSuspenso",
              label: "Suspenso",
              options: {
                display:
                  $this.state.persistenciaColunas.GrupoCategoria
                    .OrcamentoSuspenso,
                viewColumns: true,
                filter: false,
                customBodyRender: (value, tableMeta, updateValue) => {
                  return (
                    <div
                      style={{
                        cursor: "pointer",
                        backgroundColor: this.obterCor(tableMeta.rowData[15]),
                        padding: "16px",
                      }}
                    >
                      {value != null && value === 1 ? "Sim" : "Não"}
                    </div>
                  );
                },
              },
            },
            {
              name: "Quantidade",
              label: "Quantidade",
              options: {
                display:
                  $this.state.persistenciaColunas.GrupoCategoria.Quantidade,
                viewColumns: true,
                filter: false,
                customBodyRender: (value, tableMeta) => {
                  return (
                    <div
                      style={{
                        cursor: "pointer",
                        backgroundColor: this.obterCor(tableMeta.rowData[15]),
                        padding: "16px",
                      }}
                      onClick={() => {
                        var orcamentoId = tableMeta.rowData[0];
                        this.editarRegistro(orcamentoId);
                      }}
                    >
                      {value}
                    </div>
                  );
                },
              },
            },
            {
              name: "Total",
              label: "Total",
              options: {
                display: $this.state.persistenciaColunas.GrupoCategoria.Total,
                viewColumns: true,
                filter: false,
                customBodyRender: (value, tableMeta) => {
                  return (
                    <div
                      style={{
                        cursor: "pointer",
                        backgroundColor: this.obterCor(tableMeta.rowData[15]),
                        padding: "16px",
                      }}
                      onClick={() => {
                        var orcamentoId = tableMeta.rowData[0];
                        this.editarRegistro(orcamentoId);
                      }}
                    >
                      {value
                        .toLocaleString("pt-BR", {
                          style: "currency",
                          currency: "BRL",
                        })
                        .replace("R$", "")}
                    </div>
                  );
                },
              },
            },
            {
              name: "Media",
              label: "Média",
              options: {
                display: $this.state.persistenciaColunas.GrupoCategoria.Media,
                viewColumns: true,
                filter: false,
                customBodyRender: (value, tableMeta, updateValue) => {
                  return (
                    <div
                      style={{
                        cursor: "pointer",
                        backgroundColor: this.obterCor(tableMeta.rowData[15]),
                        padding: "16px",
                      }}
                      onClick={() => {
                        var orcamentoId = tableMeta.rowData[0];
                        this.editarRegistro(orcamentoId);
                      }}
                    >
                      {tableMeta.rowData[11] > 0 && tableMeta.rowData[10] > 0
                        ? (tableMeta.rowData[11] / tableMeta.rowData[10])
                            .toLocaleString("pt-BR", {
                              style: "currency",
                              currency: "BRL",
                            })
                            .replace("R$", "")
                        : "0,00"}
                    </div>
                  );
                },
              },
            },
            {
              name: "Minimo",
              label: "Mínimo",
              options: {
                display: $this.state.persistenciaColunas.GrupoCategoria.Minimo,
                viewColumns: true,
                filter: false,
                customBodyRender: (value, tableMeta) => {
                  return (
                    <div
                      style={{
                        cursor: "pointer",
                        backgroundColor: this.obterCor(tableMeta.rowData[15]),
                        padding: "16px",
                      }}
                      onClick={() => {
                        var orcamentoId = tableMeta.rowData[0];
                        this.editarRegistro(orcamentoId);
                      }}
                    >
                      {value
                        .toLocaleString("pt-BR", {
                          style: "currency",
                          currency: "BRL",
                        })
                        .replace("R$", "")}
                    </div>
                  );
                },
              },
            },
            {
              name: "Maximo",
              label: "Máximo",
              options: {
                display: $this.state.persistenciaColunas.GrupoCategoria.Maximo,
                viewColumns: true,
                filter: false,
                customBodyRender: (value, tableMeta) => {
                  return (
                    <div
                      style={{
                        cursor: "pointer",
                        backgroundColor: this.obterCor(tableMeta.rowData[15]),
                        padding: "16px",
                      }}
                      onClick={() => {
                        var orcamentoId = tableMeta.rowData[0];
                        this.editarRegistro(orcamentoId);
                      }}
                    >
                      {value
                        .toLocaleString("pt-BR", {
                          style: "currency",
                          currency: "BRL",
                        })
                        .replace("R$", "")}
                    </div>
                  );
                },
              },
            },
            {
              name: "TipoOrcamento",
              options: {
                display: false,
                viewColumns: false,
                filter: false,
              },
            },
            {
              name: "PercentualDespesas",
              label: "Percentual de Despesas",
              options: {
                display:
                  $this.state.persistenciaColunas.GrupoCategoria
                    .PercentualDespesas,
                viewColumns: true,
                filter: false,
                customBodyRender: (value, tableMeta) => {
                  return (
                    <span
                      style={{
                        cursor: "pointer",
                        backgroundColor: this.obterCor(tableMeta.rowData[15]),
                        padding: "16px",
                      }}
                    >
                      {value.toLocaleString("pt-BR", {
                        style: "percent",
                        minimumFractionDigits: 2,
                        maximumFractionDigits: 2,
                      })}
                    </span>
                  );
                },
              },
            },
          ];

          const optionsGrupoCategoria = {
            onColumnViewChange: (changedColumn, action) => {
              switch (action) {
                case "add":
                  this.FixarDesfixarColuna(
                    "GrupoCategoria",
                    changedColumn,
                    true
                  );
                  break;
                case "remove":
                  this.FixarDesfixarColuna(
                    "GrupoCategoria",
                    changedColumn,
                    false
                  );
                  break;
                default:
                  break;
              }
            },
            onTableChange: (action, tableState) => {
              switch (action) {
                case "expandRow":
                  var expandedIndexes = tableState.expandedRows.data.map(
                    function (value) {
                      return value.dataIndex;
                    }
                  );
                  this.FixarDesfixarLinha("GrupoCategoria", expandedIndexes);
                  this.setState({ expandedIndexes: expandedIndexes });
                  break;
                default:
                  break;
              }
            },
            rowsExpanded: this.state.persistenciaLinhas.GrupoCategoria,
            textLabels: global.textLabels,
            filter: false,
            download: false,
            print: false,
            selectableRows: false,
            pagination: false,
            filterType: "dropdown",
            responsive: "stacked",
            expandableRows: true,
            expandableRowsOnClick: false,
            renderExpandableRow: (rowData, rowMeta) => {
              //console.log(rowData[2])
              var lancamentosPorCategoria = lancamentos.filter(function (
                arrItem
              ) {
                return arrItem.categoriaId === rowData[2];
              });

              if (!lancamentosPorCategoria) {
                lancamentosPorCategoria = [];
              }

              const columnsLancamentos = [
                {
                  name: "id",
                  options: {
                    display: false,
                    viewColumns: false,
                    filter: false,
                  },
                },
                {
                  name: "data",
                  label: "Data",
                  options: {
                    display: $this.state.persistenciaColunas.Lancamentos.data,
                    viewColumns: true,
                    filter: false,
                  },
                },
                {
                  name: "descricao",
                  label: "Descrição",
                  options: {
                    display:
                      $this.state.persistenciaColunas.Lancamentos.descricao,
                    viewColumns: true,
                    filter: false,
                  },
                },
                {
                  name: "valor",
                  label: "Valor",
                  options: {
                    display: $this.state.persistenciaColunas.Lancamentos.valor,
                    viewColumns: true,
                    filter: false,
                    customBodyRender: (value, tableMeta) => {
                      var valorReal = value;
                      if (
                        tableMeta.rowData[5] != null &&
                        tableMeta.rowData[5] !== ""
                      ) {
                        valorReal = tableMeta.rowData[5];
                      }

                      return (
                        <span>
                          {valorReal != null
                            ? valorReal
                                .toLocaleString("pt-BR", {
                                  style: "currency",
                                  currency: "BRL",
                                })
                                .replace("R$", "")
                            : "0,00"}
                        </span>
                      );
                    },
                  },
                },
                {
                  name: "nomeMeioPagamento",
                  label: "Meio de Pagamento",
                  options: {
                    display:
                      $this.state.persistenciaColunas.Lancamentos
                        .nomeMeioPagamento,
                    viewColumns: true,
                    filter: true,
                  },
                },
                {
                  name: "valorParcela",
                  options: {
                    display:
                      $this.state.persistenciaColunas.Lancamentos.valorParcela,
                    viewColumns: true,
                    filter: false,
                  },
                },
                {
                  name: "agrupamento",
                  label: "Parcelado?",
                  options: {
                    display:
                      $this.state.persistenciaColunas.Lancamentos.agrupamento,
                    viewColumns: true,
                    filter: false,
                    customBodyRender: (value, tableMeta) => {
                      if (value != null && value !== "") {
                        return (
                          <div>
                            Sim<span style={{ display: "none" }}>{value}</span>
                          </div>
                        );
                      } else {
                        return <span>Não</span>;
                      }
                    },
                  },
                },
                {
                  name: "operacao",
                  options: {
                    display:
                      $this.state.persistenciaColunas.Lancamentos.operacao,
                    viewColumns: true,
                    filter: false,
                  },
                },
                {
                  name: "tipoOperacao",
                  label: "Tipo de Operação",
                  options: {
                    display:
                      $this.state.persistenciaColunas.Lancamentos.tipoOperacao,
                    viewColumns: true,
                    filter: true,
                  },
                },
              ];
              const optionsLancamentos = {
                onColumnViewChange: (changedColumn, action) => {
                  switch (action) {
                    case "add":
                      this.FixarDesfixarColuna(
                        "Lancamentos",
                        changedColumn,
                        true
                      );
                      break;
                    case "remove":
                      this.FixarDesfixarColuna(
                        "Lancamentos",
                        changedColumn,
                        false
                      );
                      break;
                    default:
                      break;
                  }
                },
                onTableChange: (action, tableState) => {
                  switch (action) {
                    case "expandRow":
                      var expandedIndexes = tableState.expandedRows.data.map(
                        function (value) {
                          return value.dataIndex;
                        }
                      );
                      this.setState({ expandedIndexes: expandedIndexes });
                      break;
                    default:
                      break;
                  }
                },
                textLabels: global.textLabels,
                filter: false,
                download: false,
                print: false,
                selectableRows: true,
                filterType: "dropdown",
                responsive: "stacked",
                pagination: false,
                expandableRows: false,
                expandableRowsOnClick: false,
                onRowClick: (rowData, rowMeta) => {
                  this.editarLancamento(rowData);
                },
                onRowsDelete: (rowsDeleted) => {
                  this.excluirLancamentos(rowsDeleted, lancamentosPorCategoria);
                  return false;
                },
              };
              return (
                <TableRow>
                  <TableCell colSpan={colSpan}>
                    <div
                      className="table-lancamentos-categoria"
                      style={{ padding: "0 40px" }}
                    >
                      <MUIDataTable
                        data={lancamentosPorCategoria}
                        columns={columnsLancamentos}
                        options={optionsLancamentos}
                      />
                    </div>
                  </TableCell>
                </TableRow>
              );
            },
            customFooter: (
              count,
              page,
              rowsPerPage,
              changeRowsPerPage,
              changePage,
              textLabels
            ) => {
              return (
                <CustomFooter
                  count={count}
                  page={page}
                  rowsPerPage={rowsPerPage}
                  changeRowsPerPage={changeRowsPerPage}
                  changePage={changePage}
                  textLabels={textLabels}
                  hasPagination={false}
                />
              );
            },
          };
          return (
            <TableRow>
              <TableCell colSpan={colSpan}>
                <div id="orcamento-table">
                  <MUIDataTable
                    data={childsGrupoCategoria}
                    columns={columnsGrupoCategoria}
                    options={optionsGrupoCategoria}
                  />
                </div>
              </TableCell>
            </TableRow>
          );
        },
        textLabels: global.textLabels,
        filter: true,
        filterType: "dropdown",
        responsive: "stacked",
        print: false,
        download: false,
        sort: true,
        selectableRows: "none",
        pagination: false,
        customFooter: (
          count,
          page,
          rowsPerPage,
          changeRowsPerPage,
          changePage,
          textLabels
        ) => {
          var Sumario = this.state.Sumario;
          var sumarioLinha1 = [
            `Receitas estimadas: ${Sumario.Orcamento.ReceitasEstimadas.toLocaleString(
              "pt-BR",
              {
                style: "currency",
                currency: "BRL",
              }
            )}`,
            `Receitas realizadas: ${Sumario.Orcamento.ReceitasRealizadas.toLocaleString(
              "pt-BR",
              {
                style: "currency",
                currency: "BRL",
              }
            )}`,
          ];

          var sumarioLinha2 = [
            `Despesas estimadas: ${Sumario.Orcamento.DespesasEstimadas.toLocaleString(
              "pt-BR",
              {
                style: "currency",
                currency: "BRL",
              }
            )}`,
            `Despesas realizadas: ${Sumario.Orcamento.DespesasRealizadas.toLocaleString(
              "pt-BR",
              {
                style: "currency",
                currency: "BRL",
              }
            )}`,
          ];

          var sumarioLinha3 = [
            `Investimentos estimadas: ${Sumario.Orcamento.InvestimentosEstimados.toLocaleString(
              "pt-BR",
              {
                style: "currency",
                currency: "BRL",
              }
            )}`,
            `Investimentos realizadas: ${Sumario.Orcamento.InvestimentosRealizados.toLocaleString(
              "pt-BR",
              {
                style: "currency",
                currency: "BRL",
              }
            )}`,
          ];

          var sumarioLinha4 = [
            `Dívidas estimadas: ${Sumario.Orcamento.DividasEstimadas.toLocaleString(
              "pt-BR",
              {
                style: "currency",
                currency: "BRL",
              }
            )}`,
            `Dívidas realizadas: ${Sumario.Orcamento.DividasRealizadas.toLocaleString(
              "pt-BR",
              {
                style: "currency",
                currency: "BRL",
              }
            )}`,
          ];

          return (
            <CustomFooter
              count={count}
              page={page}
              rowsPerPage={rowsPerPage}
              changeRowsPerPage={changeRowsPerPage}
              changePage={changePage}
              textLabels={textLabels}
              data2={sumarioLinha1}
              data4={sumarioLinha2}
              data5={sumarioLinha3}
              data6={sumarioLinha4}
              hasPagination={false}
            />
          );
        },
      };

      // optionsGrupo.rowsExpanded = agrupados;

      const columnsGrupo = [
        {
          name: "OrcamentoId",
          options: {
            display: false,
            viewColumns: false,
            filter: false,
          },
        },
        {
          name: "CategoriaId",
          options: {
            display: false,
            viewColumns: false,
            filter: false,
          },
        },
        {
          name: "GrupoId",
          options: {
            display: false,
            viewColumns: false,
            filter: false,
          },
        },
        {
          name: "Grupo",
          label: "Grupo",
          options: {
            display: true,
            viewColumns: false,
            filter: true,
            customBodyRender: (value, tableMeta) => {
              return (
                <div
                  style={{
                    cursor: "pointer",
                    backgroundColor: this.obterCor(tableMeta.rowData[12]),
                    padding: "16px",
                  }}
                >
                  {value}
                </div>
              );
            },
          },
        },
        {
          name: "Estimado",
          label: "Estimado",
          options: {
            display: $this.state.persistenciaColunas.Grupo.Estimado,
            viewColumns: true,
            filter: false,
            customBodyRender: (value, tableMeta, updateValue) => {
              return (
                <div
                  style={{
                    backgroundColor: this.obterCor(tableMeta.rowData[12]),
                    padding: "16px",
                  }}
                  id={"orcamento-total-grupo-" + tableMeta.rowData[3]}
                >
                  {value != null &&
                    value
                      .toLocaleString("pt-BR", {
                        style: "currency",
                        currency: "BRL",
                      })
                      .replace("R$", "")}
                </div>
              );
            },
          },
        },
        {
          name: "TotalMedio",
          label: "Realizado mensal",
          options: {
            display: $this.state.persistenciaColunas.Grupo.TotalMedio,
            viewColumns: true,
            filter: false,
            customBodyRender: (value, tableMeta, updateValue) => {
              if (value == null) {
                value = 0;
              }
              return (
                <div
                  style={{
                    backgroundColor: this.obterCor(tableMeta.rowData[12]),
                    padding: "16px",
                  }}
                >
                  {value != null &&
                    value
                      .toLocaleString("pt-BR", {
                        style: "currency",
                        currency: "BRL",
                      })
                      .replace("R$", "")}
                </div>
              );
            },
          },
        },
        {
          name: "Quantidade",
          label: "Quantidade",
          options: {
            display: $this.state.persistenciaColunas.Grupo.Quantidade,
            viewColumns: true,
            filter: false,
            customBodyRender: (value, tableMeta, updateValue) => {
              return (
                <div
                  style={{
                    backgroundColor: this.obterCor(tableMeta.rowData[12]),
                    padding: "16px",
                  }}
                >
                  {value}
                </div>
              );
            },
          },
        },
        {
          name: "Total",
          label: "Total",
          options: {
            display: $this.state.persistenciaColunas.Grupo.Total,
            viewColumns: true,
            filter: false,
            customBodyRender: (value, tableMeta, updateValue) => {
              return (
                <div
                  style={{
                    backgroundColor: this.obterCor(tableMeta.rowData[12]),
                    padding: "16px",
                  }}
                >
                  {value != null &&
                    value
                      .toLocaleString("pt-BR", {
                        style: "currency",
                        currency: "BRL",
                      })
                      .replace("R$", "")}
                </div>
              );
            },
          },
        },
        {
          name: "Media",
          label: "Média",
          options: {
            display: $this.state.persistenciaColunas.Grupo.Media,
            viewColumns: true,
            filter: false,
            customBodyRender: (value, tableMeta, updateValue) => {
              return (
                <div
                  style={{
                    backgroundColor: this.obterCor(tableMeta.rowData[12]),
                    padding: "16px",
                  }}
                >
                  {tableMeta.rowData[7] > 0 && tableMeta.rowData[6] > 0
                    ? (tableMeta.rowData[7] / tableMeta.rowData[6])
                        .toLocaleString("pt-BR", {
                          style: "currency",
                          currency: "BRL",
                        })
                        .replace("R$", "")
                    : "0,00"}
                </div>
              );
            },
          },
        },
        {
          name: "Minimo",
          label: "Mínimo",
          options: {
            display: $this.state.persistenciaColunas.Grupo.Minimo,
            viewColumns: true,
            filter: false,
            customBodyRender: (value, tableMeta, updateValue) => {
              return (
                <div
                  style={{
                    backgroundColor: this.obterCor(tableMeta.rowData[12]),
                    padding: "16px",
                  }}
                >
                  {value != null &&
                    value
                      .toLocaleString("pt-BR", {
                        style: "currency",
                        currency: "BRL",
                      })
                      .replace("R$", "")}
                </div>
              );
            },
          },
        },
        {
          name: "Maximo",
          label: "Máximo",
          options: {
            display: $this.state.persistenciaColunas.Grupo.Maximo,
            viewColumns: true,
            filter: false,
            customBodyRender: (value, tableMeta, updateValue) => {
              return (
                <div
                  style={{
                    backgroundColor: this.obterCor(tableMeta.rowData[12]),
                    padding: "16px",
                  }}
                >
                  {value != null &&
                    value
                      .toLocaleString("pt-BR", {
                        style: "currency",
                        currency: "BRL",
                      })
                      .replace("R$", "")}
                </div>
              );
            },
          },
        },
        {
          name: "Suspenso",
          label: "Suspenso",
          options: {
            display: $this.state.persistenciaColunas.Grupo.Suspenso,
            viewColumns: true,
            filter: false,
            customBodyRender: (value, tableMeta, updateValue) => {
              return (
                <div
                  style={{
                    backgroundColor: this.obterCor(tableMeta.rowData[12]),
                    padding: "16px",
                  }}
                >
                  {value != null && value === 1 ? "Sim" : "Não"}
                </div>
              );
            },
          },
        },
        {
          name: "TipoOrcamento",
          options: {
            display: false,
            viewColumns: false,
            filter: false,
          },
        },
        {
          name: "PercentualDespesas",
          label: "Percentual de Despesas",
          options: {
            display: $this.state.persistenciaColunas.Grupo.PercentualDespesas,
            viewColumns: true,
            filter: false,
            customBodyRender: (value, tableMeta) => {
              return (
                <span
                  style={{
                    backgroundColor: this.obterCor(tableMeta.rowData[12]),
                    padding: "16px",
                  }}
                >
                  {value.toLocaleString("pt-BR", {
                    style: "percent",
                    minimumFractionDigits: 2,
                    maximumFractionDigits: 2,
                  })}
                </span>
              );
            },
          },
        },
      ];

      if (data == null || data.length === 0) {
        return <>Nenhum grupo encontrado</>;
      }

      return (
        <MuiThemeProvider theme={this.getMuiTheme()}>
          <MUIDataTable
            title={
              <div
                style={{ float: "left" }}
                className="gestao-planos-grid-title"
              >
                {isLoading && (
                  <CircularProgress
                    size={24}
                    style={{ marginLeft: 15, position: "relative", top: 4 }}
                  />
                )}
                <div style={{ float: "left", margin: "10px" }}>
                  <span
                    style={{
                      float: "left",
                      fontWeight: "600",
                      marginTop: "7px",
                    }}
                  >
                    Expandir todas as categorias?
                  </span>{" "}
                  &nbsp;
                  <Switch
                    onChange={this.handleMostrarAgrupadosChange}
                    checked={this.state.mostrarAgrupado}
                  />
                </div>
                <div style={{ float: "left", margin: "10px" }}>
                  <span
                    style={{
                      float: "left",
                      fontWeight: "600",
                      marginTop: "7px",
                    }}
                  >
                    Mostrar suspensos?
                  </span>{" "}
                  &nbsp;
                  <Switch
                    onChange={this.handleMostrarSuspensoChange}
                    checked={this.state.mostrarSuspensos}
                  />
                </div>
              </div>
            }
            data={data}
            columns={columnsGrupo}
            options={optionsGrupo}
          />
        </MuiThemeProvider>
      );
    } else {
      data = !this.state.mostrarSuspensos
        ? this.state.listaDeCategoriasSemSuspensos
        : this.state.originalData;

      // agrupados = [];
      // if (this.state.mostrarAgrupado === true) {
      //   agrupados = data.map(function (value, index) {
      //     return index;
      //   });
      // } else if (this.state.expandedIndexes != null) {
      //   agrupados = this.state.expandedIndexes;
      // }

      const columnsCategoria = [
        {
          name: "Controle",
          label: "Controle",
          options: {
            display: true,
            viewColumns: false,
            filter: false,
            sort: false,
            customBodyRender: (value, tableMeta, updateValue) => {
              return (
                <div
                  style={{
                    backgroundColor: this.obterCor(tableMeta.rowData[11]),
                    padding: "16px",
                  }}
                >
                  {tableMeta.rowData[3] !== "Investimentos" &&
                  tableMeta.rowData[3] !== "Dívidas" ? (
                    <i
                      style={{ cursor: "pointer" }}
                      onClick={(e) => {
                        e.stopPropagation();
                        var categoriaId = tableMeta.rowData[2];
                        this.confirmaExcluirCategoria(categoriaId);
                      }}
                      className="far fa-trash-alt"
                    ></i>
                  ) : (
                    ""
                  )}
                  <i
                    style={{ cursor: "pointer", marginLeft: "15px" }}
                    onClick={(e) => {
                      e.stopPropagation();
                      var categoriaId = tableMeta.rowData[2];
                      //var service = `api/dashboard/categoria-previsto-realizado-mes-a-mes/${this.state.familiaId}/${categoriaId}`;
                      var service = `${global.apiToken}/dashboard/categoria-previsto-realizado-mes-a-mes/${this.state.familiaId}/${categoriaId}`;
                      var url = global.server_api_new + service;
                      var config = {
                        headers: {
                          Authorization: "bearer " + this.state.accessToken,
                          "Access-Control-Allow-Origin": "*",
                          "Access-Control-Allow-Headers": "Authorization",
                          "Access-Control-Allow-Methods":
                            "GET, POST, OPTIONS, PUT, PATCH, DELETE",
                        },
                      };

                      var filtro = {
                        DataInicial: this.state.startDate,
                        DataFinal: this.state.endDate,
                        Ordenacao: "Previsto Desc",
                      };
                      this.setState({ periodosCategoria: null });
                      axios.post(url, filtro, config).then((res) => {
                        this.setState({ periodosCategoria: res.data.results });
                      });
                      global.showModal(this.modalGraficoCategoria);
                      if (this.props.putNavBack) {
                        this.props.putNavBack();
                      }
                    }}
                    className="fas fa-chart-bar"
                  ></i>
                </div>
              );
            },
          },
        },
        {
          name: "OrcamentoId",
          options: {
            display: false,
            viewColumns: false,
            filter: false,
          },
        },
        {
          name: "CategoriaId",
          options: {
            display: false,
            viewColumns: false,
            filter: false,
          },
        },
        {
          name: "NomeOrcamento",
          label: "Orçamento",
          options: {
            display: true,
            viewColumns: false,
            filter: true,
            customBodyRender: (value, tableMeta, updateValue) => {
              return (
                <div
                  style={{
                    cursor: "pointer",
                    backgroundColor: this.obterCor(tableMeta.rowData[11]),
                    padding: "16px",
                  }}
                  onClick={() => {
                    var orcamentoId = tableMeta.rowData[1];
                    this.editarRegistro(orcamentoId);
                  }}
                >
                  {value}
                </div>
              );
            },
          },
        },
        {
          name: "NomeCategoria",
          label: "Categoria",
          options: {
            display: true,
            viewColumns: false,
            filter: true,
            customBodyRender: (value, tableMeta, updateValue) => {
              return (
                <div
                  style={{
                    cursor: "pointer",
                    backgroundColor: this.obterCor(tableMeta.rowData[11]),
                    padding: "16px",
                  }}
                  onClick={() => {
                    var categoriaId = tableMeta.rowData[2];
                    this.editarCategoria(categoriaId);
                  }}
                >
                  {value}
                </div>
              );
            },
          },
        },
        {
          name: "NomeMeioPagamento",
          label: "Meio de Pagto. Padrão",
          options: {
            display:
              $this.state.persistenciaColunas.Categoria.NomeMeioPagamento,
            viewColumns: true,
            filter: true,
            customBodyRender: (value, tableMeta, updateValue) => {
              return (
                <div
                  style={{
                    cursor: "pointer",
                    backgroundColor: this.obterCor(tableMeta.rowData[11]),
                    padding: "16px",
                  }}
                  onClick={() => {
                    var categoriaId = tableMeta.rowData[2];
                    this.editarCategoria(categoriaId);
                  }}
                >
                  {value}
                </div>
              );
            },
          },
        },
        {
          name: "Frequencia",
          label: "Frequência",
          options: {
            display: $this.state.persistenciaColunas.Categoria.Frequencia,
            viewColumns: true,
            filter: true,
            customBodyRender: (value, tableMeta, updateValue) => {
              return (
                <div
                  style={{
                    cursor: "pointer",
                    backgroundColor: this.obterCor(tableMeta.rowData[11]),
                    padding: "16px",
                  }}
                  onClick={() => {
                    var categoriaId = tableMeta.rowData[2];
                    this.editarCategoria(categoriaId);
                  }}
                >
                  {value}
                </div>
              );
            },
          },
        },
        {
          name: "Grupo",
          label: "Grupo",
          options: {
            display: $this.state.persistenciaColunas.Categoria.Grupo,
            viewColumns: true,
            filter: true,
            customBodyRender: (value, tableMeta, updateValue) => {
              return (
                <div
                  style={{
                    cursor: "pointer",
                    backgroundColor: this.obterCor(tableMeta.rowData[11]),
                    padding: "16px",
                  }}
                  onClick={() => {
                    var categoriaId = tableMeta.rowData[2];
                    this.editarCategoria(categoriaId);
                  }}
                >
                  {value}
                </div>
              );
            },
          },
        },
        {
          name: "Estimado",
          label: "Estimado",
          options: {
            display: $this.state.persistenciaColunas.Categoria.Estimado,
            viewColumns: true,
            filter: false,
            customBodyRender: (value, tableMeta, updateValue) => {
              //Se tem categoria e orçamento E tipo de orçamento é despesa / receita
              if (
                tableMeta.rowData[2] != null &&
                tableMeta.rowData[1] !== "" &&
                tableMeta.rowData[11] < 2
              ) {
                var formatado = value
                  .toLocaleString("pt-BR", {
                    style: "currency",
                    currency: "BRL",
                  })
                  .replace("R$", "");
                setTimeout(function () {
                  $("#" + tableMeta.rowData[2]).val(formatado);
                }, 50);
                return (
                  <div
                    style={{
                      backgroundColor: this.obterCor(tableMeta.rowData[11]),
                      padding: "12px",
                    }}
                  >
                    <input
                      type="text"
                      id={tableMeta.rowData[2]}
                      ref={this.valorEstimado}
                      onClick={(e) => this.handleOnClick(e, updateValue)}
                      onKeyUp={(e) => this.handleOnClick(e, updateValue)}
                    />
                  </div>
                );
              } else {
                return (
                  <div
                    style={{
                      cursor: "pointer",
                      backgroundColor: this.obterCor(tableMeta.rowData[11]),
                      padding: "16px",
                    }}
                    onClick={() => {
                      var categoriaId = tableMeta.rowData[2];
                      this.editarCategoria(categoriaId);
                    }}
                  >
                    {value != null &&
                      value
                        .toLocaleString("pt-BR", {
                          style: "currency",
                          currency: "BRL",
                        })
                        .replace("R$", "")}
                  </div>
                );
              }
            },
          },
        },
        {
          name: "TotalMedio",
          label: "Realizado mensal",
          options: {
            display: $this.state.persistenciaColunas.Categoria.TotalMedio,
            viewColumns: true,
            filter: false,
            customBodyRender: (value, tableMeta, updateValue) => {
              if (value == null || value === "") {
                value = 0;
              }
              return (
                <div
                  style={{
                    cursor: "pointer",
                    backgroundColor: this.obterCor(tableMeta.rowData[11]),
                    padding: "16px",
                  }}
                  onClick={() => {
                    var categoriaId = tableMeta.rowData[2];
                    this.editarCategoria(categoriaId);
                  }}
                >
                  {value != null &&
                    value
                      .toLocaleString("pt-BR", {
                        style: "currency",
                        currency: "BRL",
                      })
                      .replace("R$", "")}
                </div>
              );
            },
          },
        },
        {
          name: "CategoriaSuspenso",
          label: "Suspenso",
          options: {
            display:
              $this.state.persistenciaColunas.Categoria.CategoriaSuspenso,
            viewColumns: true,
            filter: false,
            customBodyRender: (value, tableMeta, updateValue) => {
              return (
                <div
                  style={{
                    cursor: "pointer",
                    backgroundColor: this.obterCor(tableMeta.rowData[11]),
                    padding: "16px",
                  }}
                  onClick={() => {
                    var categoriaId = tableMeta.rowData[2];
                    this.editarCategoria(categoriaId);
                  }}
                >
                  {value != null && value === 1 ? "Sim" : "Não"}
                </div>
              );
            },
          },
        },
        {
          name: "TipoOrcamento",
          options: {
            display: false,
            viewColumns: false,
            filter: false,
          },
        },
        {
          name: "Quantidade",
          label: "Quantidade",
          options: {
            display: $this.state.persistenciaColunas.Categoria.Quantidade,
            viewColumns: true,
            filter: false,
            customBodyRender: (value, tableMeta, updateValue) => {
              return (
                <div
                  style={{
                    cursor: "pointer",
                    backgroundColor: this.obterCor(tableMeta.rowData[11]),
                    padding: "16px",
                  }}
                  onClick={() => {
                    var categoriaId = tableMeta.rowData[2];
                    this.editarCategoria(categoriaId);
                  }}
                >
                  {value}
                </div>
              );
            },
          },
        },
        {
          name: "Total",
          label: "Total",
          options: {
            display: $this.state.persistenciaColunas.Categoria.Total,
            viewColumns: true,
            filter: false,
            customBodyRender: (value, tableMeta, updateValue) => {
              return (
                <div
                  style={{
                    cursor: "pointer",
                    backgroundColor: this.obterCor(tableMeta.rowData[11]),
                    padding: "16px",
                  }}
                  onClick={() => {
                    var categoriaId = tableMeta.rowData[2];
                    this.editarCategoria(categoriaId);
                  }}
                >
                  {value != null &&
                    value
                      .toLocaleString("pt-BR", {
                        style: "currency",
                        currency: "BRL",
                      })
                      .replace("R$", "")}
                </div>
              );
            },
          },
        },
        {
          name: "Media",
          label: "Média",
          options: {
            display: $this.state.persistenciaColunas.Categoria.Media,
            viewColumns: true,
            filter: false,
            customBodyRender: (value, tableMeta, updateValue) => {
              return (
                <div
                  style={{
                    cursor: "pointer",
                    backgroundColor: this.obterCor(tableMeta.rowData[11]),
                    padding: "16px",
                  }}
                  onClick={() => {
                    var categoriaId = tableMeta.rowData[2];
                    this.editarCategoria(categoriaId);
                  }}
                >
                  {tableMeta.rowData[13] > 0 && tableMeta.rowData[12] > 0
                    ? (tableMeta.rowData[13] / tableMeta.rowData[12])
                        .toLocaleString("pt-BR", {
                          style: "currency",
                          currency: "BRL",
                        })
                        .replace("R$", "")
                    : "0,00"}
                </div>
              );
            },
          },
        },
        {
          name: "Minimo",
          label: "Mínimo",
          options: {
            display: $this.state.persistenciaColunas.Categoria.Minimo,
            viewColumns: true,
            filter: false,
            customBodyRender: (value, tableMeta, updateValue) => {
              return (
                <div
                  style={{
                    cursor: "pointer",
                    backgroundColor: this.obterCor(tableMeta.rowData[11]),
                    padding: "16px",
                  }}
                  onClick={() => {
                    var categoriaId = tableMeta.rowData[2];
                    this.editarCategoria(categoriaId);
                  }}
                >
                  {value != null &&
                    value
                      .toLocaleString("pt-BR", {
                        style: "currency",
                        currency: "BRL",
                      })
                      .replace("R$", "")}
                </div>
              );
            },
          },
        },
        {
          name: "Maximo",
          label: "Máximo",
          options: {
            display: $this.state.persistenciaColunas.Categoria.Maximo,
            viewColumns: true,
            filter: false,
            customBodyRender: (value, tableMeta, updateValue) => {
              return (
                <div
                  style={{
                    cursor: "pointer",
                    backgroundColor: this.obterCor(tableMeta.rowData[11]),
                    padding: "16px",
                  }}
                  onClick={() => {
                    var categoriaId = tableMeta.rowData[2];
                    this.editarCategoria(categoriaId);
                  }}
                >
                  {value != null &&
                    value
                      .toLocaleString("pt-BR", {
                        style: "currency",
                        currency: "BRL",
                      })
                      .replace("R$", "")}
                </div>
              );
            },
          },
        },
        {
          name: "Estimado",
          options: {
            display: false,
            viewColumns: false,
            filter: false,
          },
        },
        {
          name: "PercentualDespesas",
          label: "Percentual de Despesas",
          options: {
            display:
              $this.state.persistenciaColunas.Categoria.PercentualDespesas,
            viewColumns: true,
            filter: false,
            customBodyRender: (value, tableMeta) => {
              return (
                <span
                  style={{
                    backgroundColor: this.obterCor(tableMeta.rowData[11]),
                    padding: "16px",
                  }}
                >
                  {value.toLocaleString("pt-BR", {
                    style: "percent",
                    minimumFractionDigits: 2,
                    maximumFractionDigits: 2,
                  })}
                </span>
              );
            },
          },
        },
      ];

      const optionsCategoria = {
        onColumnViewChange: (changedColumn, action) => {
          switch (action) {
            case "add":
              this.FixarDesfixarColuna("Categoria", changedColumn, true);
              break;
            case "remove":
              this.FixarDesfixarColuna("Categoria", changedColumn, false);
              break;
            default:
              break;
          }
        },
        onTableChange: (action, tableState) => {
          switch (action) {
            case "expandRow":
              var expandedIndexes = tableState.expandedRows.data.map(function (
                value
              ) {
                return value.dataIndex;
              });
              this.FixarDesfixarLinha("Categoria", expandedIndexes);
              this.setState({ expandedIndexes: expandedIndexes });
              break;
            default:
              break;
          }

          this.Pesquisa_Filtros(tableState);
        },
        rowsExpanded: this.state.persistenciaLinhas.Categoria,
        textLabels: global.textLabels,
        filter: true,
        download: false,
        print: false,
        selectableRows: false,
        filterType: "dropdown",
        responsive: "stacked",
        expandableRows: true,
        expandableRowsOnClick: false,
        renderExpandableRow: (rowData, rowMeta) => {
          //console.log(rowData[2])
          const colSpan = 20;

          var lancamentosPorCategoria = lancamentos.filter(function (arrItem) {
            return arrItem.categoriaId === rowData[2];
          });
          // console.log(lancamentosPorCategoria)
          if (!lancamentosPorCategoria) {
            lancamentosPorCategoria = [];
          }

          const columnsLancamentos = [
            {
              name: "id",
              options: {
                display: false,
                viewColumns: false,
                filter: false,
              },
            },
            {
              name: "data",
              label: "Data",
              options: {
                display: $this.state.persistenciaColunas.Lancamentos.data,
                viewColumns: true,
                filter: false,
              },
            },
            {
              name: "descricao",
              label: "Descrição",
              options: {
                display: $this.state.persistenciaColunas.Lancamentos.descricao,
                viewColumns: true,
                filter: false,
              },
            },
            {
              name: "valor",
              label: "Valor",
              options: {
                display: $this.state.persistenciaColunas.Lancamentos.valor,
                viewColumns: true,
                filter: false,
                customBodyRender: (value, tableMeta) => {
                  var valorReal = value;
                  if (
                    tableMeta.rowData[5] != null &&
                    tableMeta.rowData[5] !== ""
                  ) {
                    valorReal = tableMeta.rowData[5];
                  }

                  return (
                    <span>
                      {valorReal != null
                        ? valorReal
                            .toLocaleString("pt-BR", {
                              style: "currency",
                              currency: "BRL",
                            })
                            .replace("R$", "")
                        : "0,00"}
                    </span>
                  );
                },
              },
            },
            {
              name: "nomeMeioPagamento",
              label: "Meio de Pagamento",
              options: {
                display:
                  $this.state.persistenciaColunas.Lancamentos.nomeMeioPagamento,
                viewColumns: true,
                filter: true,
              },
            },
            {
              name: "valorParcela",
              options: {
                display:
                  $this.state.persistenciaColunas.Lancamentos.valorParcela,
                viewColumns: true,
                filter: false,
              },
            },
            {
              name: "agrupamento",
              label: "Parcelado?",
              options: {
                display:
                  $this.state.persistenciaColunas.Lancamentos.agrupamento,
                viewColumns: true,
                filter: false,
                customBodyRender: (value, tableMeta) => {
                  if (value != null && value !== "") {
                    return (
                      <div>
                        Sim<span style={{ display: "none" }}>{value}</span>
                      </div>
                    );
                  } else {
                    return <span>Não</span>;
                  }
                },
              },
            },
            {
              name: "operacao",
              options: {
                display: $this.state.persistenciaColunas.Lancamentos.operacao,
                viewColumns: true,
                filter: false,
              },
            },
            {
              name: "tipoOperacao",
              label: "Tipo de Operação",
              options: {
                display:
                  $this.state.persistenciaColunas.Lancamentos.tipoOperacao,
                viewColumns: true,
                filter: true,
              },
            },
          ];
          const optionsLancamentos = {
            onColumnViewChange: (changedColumn, action) => {
              switch (action) {
                case "add":
                  this.FixarDesfixarColuna("Lancamentos", changedColumn, true);
                  break;
                case "remove":
                  this.FixarDesfixarColuna("Lancamentos", changedColumn, false);
                  break;
                default:
                  break;
              }
            },
            onTableChange: (action, tableState) => {
              switch (action) {
                case "expandRow":
                  var expandedIndexes = tableState.expandedRows.data.map(
                    function (value) {
                      return value.dataIndex;
                    }
                  );
                  this.setState({ expandedIndexes: expandedIndexes });
                  break;
                default:
                  break;
              }
            },
            textLabels: global.textLabels,
            filter: false,
            download: false,
            print: false,
            selectableRows: true,
            filterType: "dropdown",
            responsive: "stacked",
            pagination: false,
            expandableRows: false,
            expandableRowsOnClick: false,
            onRowClick: (rowData, rowMeta) => {
              this.editarLancamento(rowData);
            },
            onRowsDelete: (rowsDeleted) => {
              this.excluirLancamentos(rowsDeleted, lancamentosPorCategoria);
              return false;
            },
          };
          return (
            <TableRow>
              <TableCell colSpan={colSpan}>
                <div
                  className="table-lancamentos-categoria"
                  style={{ padding: "0 40px" }}
                >
                  <MUIDataTable
                    data={lancamentosPorCategoria}
                    columns={columnsLancamentos}
                    options={optionsLancamentos}
                  />
                </div>
              </TableCell>
            </TableRow>
          );
        },
        customFooter: (
          count,
          page,
          rowsPerPage,
          changeRowsPerPage,
          changePage,
          textLabels
        ) => {
          var Sumario = this.state.Sumario;
          var sumarioLinha1 = [
            `Receitas estimadas: ${Sumario.Orcamento.ReceitasEstimadas.toLocaleString(
              "pt-BR",
              {
                style: "currency",
                currency: "BRL",
              }
            )}`,
            `Receitas realizadas: ${Sumario.Orcamento.ReceitasRealizadas.toLocaleString(
              "pt-BR",
              {
                style: "currency",
                currency: "BRL",
              }
            )}`,
          ];

          var sumarioLinha2 = [
            `Despesas estimadas: ${Sumario.Orcamento.DespesasEstimadas.toLocaleString(
              "pt-BR",
              {
                style: "currency",
                currency: "BRL",
              }
            )}`,
            `Despesas realizadas: ${Sumario.Orcamento.DespesasRealizadas.toLocaleString(
              "pt-BR",
              {
                style: "currency",
                currency: "BRL",
              }
            )}`,
          ];

          var sumarioLinha3 = [
            `Investimentos estimadas: ${Sumario.Orcamento.InvestimentosEstimados.toLocaleString(
              "pt-BR",
              {
                style: "currency",
                currency: "BRL",
              }
            )}`,
            `Investimentos realizadas: ${Sumario.Orcamento.InvestimentosRealizados.toLocaleString(
              "pt-BR",
              {
                style: "currency",
                currency: "BRL",
              }
            )}`,
          ];

          var sumarioLinha4 = [
            `Dívidas estimadas: ${Sumario.Orcamento.DividasEstimadas.toLocaleString(
              "pt-BR",
              {
                style: "currency",
                currency: "BRL",
              }
            )}`,
            `Dívidas realizadas: ${Sumario.Orcamento.DividasRealizadas.toLocaleString(
              "pt-BR",
              {
                style: "currency",
                currency: "BRL",
              }
            )}`,
          ];

          return (
            <CustomFooter
              count={count}
              page={page}
              rowsPerPage={rowsPerPage}
              changeRowsPerPage={changeRowsPerPage}
              changePage={changePage}
              textLabels={textLabels}
              data2={sumarioLinha1}
              data4={sumarioLinha2}
              data5={sumarioLinha3}
              data6={sumarioLinha4}
              hasPagination={false}
            />
          );
        },
      };

      // optionsCategoria.rowsExpanded = agrupados;

      return (
        <MuiThemeProvider theme={this.getMuiTheme()}>
          <MUIDataTable
            title={
              <div
                style={{ float: "left", margin: "10px" }}
                className="gestao-planos-grid-title"
              >
                {isLoading && (
                  <CircularProgress
                    size={24}
                    style={{ marginLeft: 15, position: "relative", top: 4 }}
                  />
                )}
                <div style={{ width: "100%" }}>
                  <span
                    style={{
                      float: "left",
                      fontWeight: "600",
                      marginTop: "7px",
                    }}
                  >
                    Mostrar suspensos?
                  </span>{" "}
                  &nbsp;
                  <Switch
                    onChange={this.handleMostrarSuspensoChange}
                    checked={this.state.mostrarSuspensos}
                  />
                </div>
              </div>
            }
            data={data}
            columns={columnsCategoria}
            options={optionsCategoria}
          />
        </MuiThemeProvider>
      );
    }
  };

  obterPorcentagemDespesaOrcamentos = () => {
    var listaDeOrcamentos = this.state.listaDeOrcamentos;
    var listaDeOrcamentosComSuspensos =
      this.state.listaDeOrcamentosComSuspensos;

    var totalDespesaDividas = 0;
    if (listaDeOrcamentos.length > 0) {
      listaDeOrcamentos.reduce((total, num) => {
        total =
          total.TipoOrcamento === 1 || total.TipoOrcamento === 3
            ? total.TotalMedio === undefined ||
              total.TotalMedio === null ||
              total.TotalMedio === ""
              ? 0
              : total.TotalMedio
            : 0;

        num =
          num.TipoOrcamento === 1 || num.TipoOrcamento === 3
            ? num.TotalMedio === undefined ||
              num.TotalMedio === null ||
              num.TotalMedio === ""
              ? 0
              : num.TotalMedio
            : 0;

        totalDespesaDividas += total + num;

        return total + num;
      });
    }

    var totalDespesaDividasComSuspensos = 0;
    if (listaDeOrcamentosComSuspensos.length > 0) {
      listaDeOrcamentosComSuspensos.reduce((total, num) => {
        total =
          total.TipoOrcamento === 1 || total.TipoOrcamento === 3
            ? total.TotalMedio === undefined ||
              total.TotalMedio === null ||
              total.TotalMedio === ""
              ? 0
              : total.TotalMedio
            : 0;

        num =
          num.TipoOrcamento === 1 || num.TipoOrcamento === 3
            ? num.TotalMedio === undefined ||
              num.TotalMedio === null ||
              num.TotalMedio === ""
              ? 0
              : num.TotalMedio
            : 0;

        totalDespesaDividasComSuspensos += total + num;

        return total + num;
      });
    }

    listaDeOrcamentos.forEach((item) => {
      item.PercentualDespesas =
        item.TipoOrcamento === 1 || item.TipoOrcamento === 3
          ? totalDespesaDividas !== 0
            ? item.TotalMedio / totalDespesaDividas
            : 0
          : "--";
    });

    listaDeOrcamentosComSuspensos.forEach((item) => {
      item.PercentualDespesas =
        item.TipoOrcamento === 1 || item.TipoOrcamento === 3
          ? totalDespesaDividasComSuspensos !== 0
            ? item.TotalMedio / totalDespesaDividasComSuspensos
            : 0
          : "--";
    });

    this.setState({ listaDeOrcamentos, listaDeOrcamentosComSuspensos });
  };

  obterPorcentagemDespesaOrcamentosCategorias = (childsOrcamentoCategoria) => {
    var totalDespesaDividas = 0;
    if (childsOrcamentoCategoria.length > 0) {
      childsOrcamentoCategoria.reduce((total, num) => {
        total =
          total.TipoOrcamento === 1 || total.TipoOrcamento === 3
            ? total.TotalMedio === undefined ||
              total.TotalMedio === null ||
              total.TotalMedio === ""
              ? 0
              : total.TotalMedio
            : 0;

        num =
          num.TipoOrcamento === 1 || num.TipoOrcamento === 3
            ? num.TotalMedio === undefined ||
              num.TotalMedio === null ||
              num.TotalMedio === ""
              ? 0
              : num.TotalMedio
            : 0;

        totalDespesaDividas += total + num;

        return total + num;
      });
    }

    childsOrcamentoCategoria.forEach((item) => {
      item.PercentualDespesas =
        item.TipoOrcamento === 1 || item.TipoOrcamento === 3
          ? totalDespesaDividas !== 0
            ? item.TotalMedio / totalDespesaDividas
            : 0
          : "--";
    });

    return childsOrcamentoCategoria;
  };

  obterPorcentagemDespesaGruposCategorias = (childsGrupoCategoria) => {
    var totalDespesaDividas = 0;
    if (childsGrupoCategoria.length > 0) {
      childsGrupoCategoria.reduce((total, num) => {
        total =
          total.TipoOrcamento === 1 || total.TipoOrcamento === 3
            ? total.TotalMedio === undefined ||
              total.TotalMedio === null ||
              total.TotalMedio === ""
              ? 0
              : total.TotalMedio
            : 0;

        num =
          num.TipoOrcamento === 1 || num.TipoOrcamento === 3
            ? num.TotalMedio === undefined ||
              num.TotalMedio === null ||
              num.TotalMedio === ""
              ? 0
              : num.TotalMedio
            : 0;

        totalDespesaDividas += total + num;

        return total + num;
      });
    }

    childsGrupoCategoria.forEach((item) => {
      item.PercentualDespesas =
        item.TipoOrcamento === 1 || item.TipoOrcamento === 3
          ? totalDespesaDividas !== 0
            ? item.TotalMedio / totalDespesaDividas
            : 0
          : "--";
    });

    return childsGrupoCategoria;
  };

  obterPorcentagemDespesaGrupos = () => {
    var listaDeGrupos = this.state.listaDeGrupos;
    var listaDeGruposComSuspensos = this.state.listaDeGruposComSuspensos;

    var totalDespesaDividas = 0;
    if (listaDeGrupos.length > 0) {
      listaDeGrupos.reduce((total, num) => {
        total =
          total.TipoOrcamento === 1 || total.TipoOrcamento === 3
            ? total.TotalMedio === undefined ||
              total.TotalMedio === null ||
              total.TotalMedio === ""
              ? 0
              : total.TotalMedio
            : 0;

        num =
          num.TipoOrcamento === 1 || num.TipoOrcamento === 3
            ? num.TotalMedio === undefined ||
              num.TotalMedio === null ||
              num.TotalMedio === ""
              ? 0
              : num.TotalMedio
            : 0;

        totalDespesaDividas += total + num;

        return total + num;
      });
    }

    var totalDespesaDividasComSuspensos = 0;
    if (listaDeGruposComSuspensos.length > 0) {
      listaDeGruposComSuspensos.reduce((total, num) => {
        total =
          total.TipoOrcamento === 1 || total.TipoOrcamento === 3
            ? total.TotalMedio === undefined ||
              total.TotalMedio === null ||
              total.TotalMedio === ""
              ? 0
              : total.TotalMedio
            : 0;

        num =
          num.TipoOrcamento === 1 || num.TipoOrcamento === 3
            ? num.TotalMedio === undefined ||
              num.TotalMedio === null ||
              num.TotalMedio === ""
              ? 0
              : num.TotalMedio
            : 0;

        totalDespesaDividasComSuspensos += total + num;

        return total + num;
      });
    }

    listaDeGrupos.forEach((item) => {
      item.PercentualDespesas =
        item.TipoOrcamento === 1 || item.TipoOrcamento === 3
          ? totalDespesaDividas !== 0
            ? item.TotalMedio / totalDespesaDividas
            : 0
          : "--";
    });

    listaDeGruposComSuspensos.forEach((item) => {
      item.PercentualDespesas =
        item.TipoOrcamento === 1 || item.TipoOrcamento === 3
          ? totalDespesaDividasComSuspensos !== 0
            ? item.TotalMedio / totalDespesaDividasComSuspensos
            : 0
          : "--";
    });

    this.setState({ listaDeGrupos, listaDeGruposComSuspensos });
  };

  obterPorcentagemDespesaCategorias = () => {
    var listaDeCategoriasSemSuspensos =
      this.state.listaDeCategoriasSemSuspensos;
    var originalData = this.state.originalData;

    var totalDespesaDividas = 0;
    if (listaDeCategoriasSemSuspensos.length > 0) {
      listaDeCategoriasSemSuspensos.reduce((total, num) => {
        total =
          total.TipoOrcamento === 1 || total.TipoOrcamento === 3
            ? total.TotalMedio === undefined ||
              total.TotalMedio === null ||
              total.TotalMedio === ""
              ? 0
              : total.TotalMedio
            : 0;

        num =
          num.TipoOrcamento === 1 || num.TipoOrcamento === 3
            ? num.TotalMedio === undefined ||
              num.TotalMedio === null ||
              num.TotalMedio === ""
              ? 0
              : num.TotalMedio
            : 0;

        totalDespesaDividas += total + num;

        return total + num;
      });
    }

    var totalDespesaDividasComSuspensos = 0;
    if (originalData.length > 0) {
      originalData.reduce((total, num) => {
        total =
          total.TipoOrcamento === 1 || total.TipoOrcamento === 3
            ? total.TotalMedio === undefined ||
              total.TotalMedio === null ||
              total.TotalMedio === ""
              ? 0
              : total.TotalMedio
            : 0;

        num =
          num.TipoOrcamento === 1 || num.TipoOrcamento === 3
            ? num.TotalMedio === undefined ||
              num.TotalMedio === null ||
              num.TotalMedio === ""
              ? 0
              : num.TotalMedio
            : 0;

        totalDespesaDividasComSuspensos += total + num;

        return total + num;
      });
    }

    listaDeCategoriasSemSuspensos.forEach((item) => {
      item.PercentualDespesas =
        item.TipoOrcamento === 1 || item.TipoOrcamento === 3
          ? totalDespesaDividas !== 0
            ? item.TotalMedio / totalDespesaDividas
            : 0
          : "--";
    });

    originalData.forEach((item) => {
      item.PercentualDespesas =
        item.TipoOrcamento === 1 || item.TipoOrcamento === 3
          ? totalDespesaDividasComSuspensos !== 0
            ? item.TotalMedio / totalDespesaDividasComSuspensos
            : 0
          : "--";
    });

    this.setState({
      listaDeCategoriasSemSuspensos,
      originalData,
    });
  };

  handleSuspensoChange = (checked) => {
    this.setState({ suspenso: checked });
  };

  handleMostrarSuspensoChange = (checked) => {
    this.setState(
      {
        mostrarSuspensos: checked,
        expandedIndexes: [],
      },
      function () {
        this.getData(true);
      }
    );
  };

  handleMostrarAgrupadosChange = (checked) => {
    try {
      var persistenciaLinhas = this.state.persistenciaLinhas;

      if (checked) {
        var linhasOrcamentos = [];
        for (
          let index = 0;
          index < this.state.listaDeOrcamentos.length;
          index++
        ) {
          linhasOrcamentos.push(index);
        }

        var linhasGrupos = [];
        for (let index = 0; index < this.state.listaDeGrupos.length; index++) {
          linhasGrupos.push(index);
        }

        persistenciaLinhas.Orcamento = linhasOrcamentos;
        persistenciaLinhas.Grupo = linhasGrupos;
      } else {
        persistenciaLinhas.Orcamento = [];
        persistenciaLinhas.Grupo = [];
      }

      this.setState({ persistenciaLinhas, mostrarAgrupado: checked });
    } catch (error) {
      console.log("handleMostrarAgrupadosChange error: ", error);
    }
  };

  filtrarPadraoPeloTipo = () => {
    if (this.state.orcamentosPadraoCombo == null) return [];

    if (this.state.tipoOrcamento == null) return [];

    // filtro executado na busca
    if (
      this.filtroPorTipo != null &&
      this.filtroPorTipo[this.state.tipoOrcamento] != null &&
      this.filtroPorTipo[this.state.tipoOrcamento].length > 0
    ) {
      return this.filtroPorTipo[this.state.tipoOrcamento];
    }

    let $this = this;

    return this.state.orcamentosPadraoCombo.filter(function (item) {
      return (
        item.value == null ||
        item.value.tipoOrcamento === $this.state.tipoOrcamento
      );
    });
  };

  loadCategoriaGrafico = () => {
    var $this = this;
    var labelsOrcamento = [];
    var realizadoCategoria = [];
    var estimadoCategoria = [];

    if (this.state.periodosCategoria != null) {
      var categoriaSelecionada = this.state.categorias.filter(function (item) {
        return item.id === $this.state.categoriaGraficoId;
      });

      labelsOrcamento = this.state.periodosCategoria.map(function (value) {
        return `${value["mes"]}/${value["ano"]}`;
      });
      realizadoCategoria = this.state.periodosCategoria.map(function (value) {
        return value["total"];
      });

      estimadoCategoria = this.state.periodosCategoria.map(function (value) {
        return categoriaSelecionada && categoriaSelecionada[0]
          ? categoriaSelecionada[0].valorEstimado
          : 0;
      });
    }

    var chartOptionsCategoria = {
      options: {
        legend: {
          show: true,
          position: "top",
          horizontalAlign: "center",
          fontFamily: "Open Sans, sans-serif",
          fontSize: "14px",
          offsetY: -15,
          itemMargin: {
            horizontal: 5,
            vertical: 10,
          },
          markers: {
            width: 20,
            height: 10,
            radius: 15,
          },
          onItemHover: {
            highlightDataSeries: false,
          },
        },
        plotOptions: {
          bar: {
            horizontal: false,
            endingShape: "flat",
            columnWidth: "10%",
          },
        },
        colors: ["#9A3DFF", "#28ADC0"],
        dataLabels: {
          enabled: true,
          enabledOnSeries: [0, 1],
          formatter: function (value) {
            return value !== null && value !== 0
              ? value
                  .toLocaleString("pt-BR", {
                    style: "currency",
                    currency: "BRL",
                  })
                  .replace("R$", "")
              : "";
          },
          offsetY: -10,
          style: {
            fontSize: "11px",
            fontFamily: "Open Sans, sans-serif",
            colors: ["#57657D"],
            textAlign: "center",
          },
          background: {
            enabled: false,
          },
        },
        stroke: {
          curve: "smooth",
          width: [0, 4],
        },
        chart: {
          id: "basic-bar",
          type: "line",
        },
        yaxis: {
          labels: {
            formatter: function (value) {
              return value != null
                ? value
                    .toLocaleString("pt-BR", {
                      style: "currency",
                      currency: "BRL",
                    })
                    .replace("R$", "")
                : "";
            },
          },
        },
        xaxis: {
          categories: labelsOrcamento,
        },
      },
      series: [
        {
          name: "Realizado",
          type: "column",
          data: realizadoCategoria,
        },
        {
          name: "Estimado",
          type: "line",
          data: estimadoCategoria,
        },
      ],
    };
    return (
      <ApexChart
        options={chartOptionsCategoria.options}
        series={chartOptionsCategoria.series}
        type="line"
        height="400px"
        width="100%"
        className="home-estimate-realized"
      />
    );
  };

  loadOrcamentoGrafico = () => {
    if (this.state.orcamentoSelecionado === undefined) {
      return;
    }

    var url =
      global.server_api_new +
      global.apiToken +
      "/dashboard/orcamento/grafico-orcamento-periodo/" +
      this.state.orcamentoSelecionado;

    var filtro = {
      Filtro: this.state.textoDigitado,
      Ordenacao:
        this.state.sortField != null ? this.state.sortField : "Data Desc",
      NumeroPagina:
        this.state.numeroPagina != null ? this.state.numeroPagina : null,
      NumeroItensPorPagina:
        this.state.numeroItensPorPagina != null
          ? this.state.numeroItensPorPagina
          : null,
      DataInicial: this.state.startDate,
      DataFinal: this.state.endDate,
      CategoriaId:
        this.state.categoriaFiltro != null
          ? this.state.categoriaFiltro.id
          : null,
      OrcamentoId:
        this.state.orcamentoFiltro != null
          ? this.state.orcamentoFiltro.id
          : null,
      Grupo:
        this.state.grupoFiltro != null ? this.state.grupoFiltro.value : null,
      Frequencia:
        this.state.frequenciaFiltro != null
          ? this.state.frequenciaFiltro.value
          : null,
    };

    var config = {
      headers: {
        Authorization: "bearer " + this.state.accessToken,
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Headers": "Authorization",
        "Access-Control-Allow-Methods":
          "GET, POST, OPTIONS, PUT, PATCH, DELETE",
      },
    };

    axios.post(url, filtro, config).then((res) => {
      if (res.data.results != null) {
        this.setState({
          RealizadoOrcamentoGrafico: res.data.results[0].realizado,
          EstimadoOrcamentoGrafico: res.data.results[0].estimado,
        });
      }
    });
  };
}

export default OrcamentoGeral;
