import React from "react";
import { Redirect } from "react-router-dom";
import { Row } from "reactstrap";
import axios from "axios";

class TermosPrivacidade extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      accessToken: localStorage.getItem("access-token"),
      empresaId: localStorage.getItem("empresa-id"),
      planejadorId: localStorage.getItem("planejador-id"),
      familiaId: localStorage.getItem("familia-id"),
      tipoUsuario: parseInt(localStorage.getItem("tipo-usuario")),
      usuarioEmail: localStorage.getItem("usuario-email"),

      selectedVideo: "cadastro",
    };
  }

  componentDidMount() {
    localStorage.setItem("naoLembrarAtzApp", false);
    if (this.state.accessToken === null) {
      this.setRedirect();
      return;
    }
  }

  setRedirect = () => {
    this.setState({
      redirect: true,
    });
  };

  renderRedirect = () => {
    if (this.state.redirect) {
      return <Redirect to="/admin/index" />;
    }
  };

  updateTermo = () => {
    //console.log(url);

    var url =
      global.server_api_new +
      global.apiToken +
      "/Termo/cadastroaceite/" +
      global.termoId +
      "/" +
      this.state.usuarioEmail;
    //https://localhost:5002/api/{apiKey}/Termo/cadastroaceite/{termo-Id}/{emailUser};

    var config = {
      headers: {
        Authorization: "bearer " + this.state.accessToken,
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Headers": "Authorization",
        "Access-Control-Allow-Methods":
          "GET, POST, OPTIONS, PUT, PATCH, DELETE",
      },
    };
    try {
      axios.get(url, config).then((res) => {
        //console.log(url, res);
        if (res.data.code === 200) {
          alert(res.data.message);
          //window.history.back(2);
          window.location = "/admin/index";
        } else {
          alert(res.data.message);
        }
      });
    } catch (error) {
      console.log(error);
    }
  };

  render() {
    return (
      <div id="leads" style={{ padding: "0 5%", marginTop: 20 }}>
        <Row>
          <div className="modal_pesquisa">
            <h2 className="modal_title text-center">Política de privacidade</h2>
            <p className=" pt-3 text-justify">
              Esta Política de Privacidade abrange o Aplicativo e o{" "}
              <em>Website</em> do Vista Sistemas de Soluções em Tecnologia
              Financeira LTDA., tendo como objetivo fornecer uma visão
              transparente das práticas relacionadas à coleta, ao uso, ao
              armazenamento e ao tratamento dos dados pelos referidos aplicativo
              e <em>website</em>.
            </p>

            <p className="text-justify">
              Para usar a Plataforma do Vista de maneira satisfatória, o Usuário
              e o Cliente terão que inserir seus Dados pessoais, orçamentos,
              receitas, gastos, dados financeiros pessoais, a fim de que
              interajam na Plataforma ou <em>Website</em>.<br />
              Nos importamos, e respeitamos a confiança depositada pelos
              Usuários de nosso <em>Website</em> ou Plataforma no Vista. Por
              essa razão, adotamos rigorosos padrões de proteção para os nossos
              sistemas e informamos que não utilizaremos seus Dados Pessoais de
              forma diversa ao que esteja expressamente prevista nesta Política
              de Privacidade.
              <br />
              Se depois de ler a Política você ainda tiver alguma dúvida, poderá
              entrar em contato conosco pelo e-mail{" "}
              <a href="mailto:suporte@meuvista.com">suporte@meuvista.com</a>
              <br />
            </p>

            <h2 className="privacy_title py-3">DAS DEFINIÇÕES</h2>

            <p className="text-justify">
              <strong>PLANEJADOR FINANCEIRO:</strong> pessoa física ou jurídica,
              com capacidade legal para oferecer e executar serviços de
              planejamento financeiro a terceiros, que não possui qualquer
              vínculo ou relação de subordinação com a plataforma VISTA®, e que
              aceita todos seus Termos e Condições de Uso, bem como a Política
              de Privacidade, a partir do momento em que iniciar sua utilização.
              <br />
            </p>

            <p className="text-justify">
              <strong>CLIENTE DO PLANEJADOR FINANCEIRO:</strong> pessoa física
              ou jurídica cadastrada pelo PLANEJADOR FINANCEIRO, a qual possui
              relação direta com o mesmo, não havendo qualquer tipo de relação
              com a plataforma VISTA®;
              <br />
            </p>

            <p className="text-justify">
              <strong>PLATAFORMA VISTA:</strong> Ferramenta a ser utilizada
              pelos USUÁRIOS, realizando os respectivos cadastros por meio das
              informações disponibilizadas, visando a utilização de suas
              funcionalidades.
              <br />
              USUÁRIOS: Quando se referir às informações tanto do PLANEJADOR
              FINANCEIRO, quanto do CLIENTE DO PLANEJADOR FINANCEIRO.
              <br />
            </p>

            <h2 className="privacy_title">1. DOS DADOS COLETADOS</h2>
            <br />
            <p className="text-justify">
              1.1 Os dados coletados pelo Vista têm 3 (três) fontes diferentes:
              (a) dados inseridos e disponibilizados pelo PLANEJADOR FINANCEIRO
              ao usar a PLATAFORMA VISTA; (b) dados inseridos e disponibilizados
              pelo CLIENTE DO PLANEJADOR FINANCEIRO ao usar a PLATAFORMA VISTA;
              (c) dados coletados pela PLATAFORMA VISTA por meio de seu{" "}
              <em>Website</em> ou Aplicativo próprio ou serviço terceirizado.
            </p>
            <br />

            <ul className="text-justify terms-list">
              <li>
                Os Dados disponibilizados pelo PLANEJADOR FINANCEIRO incluem
                todas as informações que o mesmo inseriu de forma voluntária, na
                PLATAFORMA VISTA, abrangendo: Dados Pessoais e Dados de Conta,
                patrimônios, receitas e despesas, planos e sonhos.
              </li>
              <li>
                Os dados disponibilizados pelo CLIENTE DO PLANEJADOR FINANCEIRO
                incluem todas as informações que o mesmo inseriu de forma
                voluntária, na PLATAFORMA VISTA.
              </li>
              <li>
                Os Dados coletados pela PLATAFORMA VISTA, incluem todas as
                informações inseridas e disponibilizadas pelos USUÁRIOS
                abrangendo: Dados Financeiros Pessoais, Dados de Acesso
                (incluindo Cookies, conforme disposto no Item 5 desta Política
                de Privacidade), e eventuais correspondências entre as partes.
              </li>
            </ul>
            <br />

            <p className="text-justify">
              <strong>1.2 Informações que recebemos de terceiros</strong>
            </p>

            <p className="text-justify">
              Para fornecer aos USUÁRIOS, as atualizações dos investimentos
              inseridos na PLATAFORMA VISTA pelos mesmos, utilizamos informações
              obtidas por API (<em>Application Programming Interface</em>), com
              a empresa Kinvo.
            </p>
            <br />

            <h2 className="privacy_title">
              2. DA UTILIZAÇÃO DOS DADOS COLETADOS
            </h2>
            <br />

            <p className="text-justify">
              Conforme estabelecem a Lei Geral de Proteção de Dados Pessoais
              (LGPD), nos casos de usuários baseados em território nacional e a{" "}
              <em>General Data Protection Regulation</em> (GDPR), nos casos de
              usuários baseados em território ligado à União Europeia, o
              Tratamento dos Dados Pessoais deve ser realizado mediante
              propósitos legítimos, específicos, explícitos sem possibilidade de
              Tratamento posterior de forma incompatível com as finalidades para
              as quais foram coletados e seguindo certos requisitos de
              tratamento.
            </p>

            <p className="text-justify">
              Os requisitos mediante os quais poderemos realizar o Tratamento
              dos Dados Pessoais disponibilizados são:
            </p>

            <p className="text-justify">
              <strong>– Execução de Contrato:</strong> Para cumprirmos nossas
              obrigações contratuais, precisamos coletar e processar suas
              informações pessoais, para que possamos realizar todas as
              funcionalidades de organização e gestão de finanças pessoais
              contratadas ao Aceitar nossos Termos de Uso e Política de
              Privacidade;
            </p>

            <p className="text-justify">
              <strong>– Legítimo Interesse:</strong> Tratamento de Dados
              Pessoais necessários para atender em determinadas circunstâncias,
              nós (ou parceiros em nosso nome) podemos usar suas informações
              pessoais para buscar interesses legítimos nossos ou de terceiros,
              mas isso é fornecido desde que seus interesses e direitos
              fundamentais não os substituam. Para fins dessa Política de
              Privacidade, entende-se por “interesses legítimos”, o intuito em
              fornecer serviços aos USUÁRIOS da PLATAFORMA VISTA, da melhor
              maneira possível, para tanto poderá se realizar pesquisas que
              possam aferir os anseios dos USUÁRIOS, propiciando assim uma forma
              de melhorar os serviços entregues, com o oferecimento de eventuais
              novas funcionalidades;
            </p>

            <p className="text-justify">
              <strong>– Consentimento:</strong> Trata-se de hipótese para
              Tratamento de Dados Pessoais com finalidade que não envolve
              atividade necessária para execução de nossos serviços e tampouco
              pode ser enquadrada em nosso Legítimo Interesse de operação.
              Assim, tais finalidades de Tratamento deverão ser consentidas de
              forma individual, destacada, informada de inequívoca pelos
              USUÁRIOS, podendo ser, mas não se limitando a, envio de e-mail e
              alertas e transmissão de Dados Pessoais para terceiros.
            </p>

            <p className="text-justify">
              O consentimento dos USUÁRIOS poderá ser revogado a qualquer
              momento, e nossa Plataforma disponibiliza o e-mail
              contato@meuvista.com para que o Usuário entre em contato conosco
              solicitando a revogação.
            </p>
            <br />

            <ul>
              <li>FINALIDADE DOS DADOS COLETADOS</li>
            </ul>
            <h2 className="privacy_title">Funcionamento da PLATAFORMA VISTA</h2>
            <br />

            <p className="text-justify">
              Todos os dados coletados são utilizados para manter e aprimorar o
              funcionamento da PLATAFORMA VISTA e oferecer a melhor experiência
              aos USUÁRIOS como, por exemplo, para (i) sincronizar
              automaticamente os investimentos a partir dos dados fornecidos;
              (ii) desenvolver novas funcionalidades; (iii) realizar
              estatísticas, gráficos, tabelas, informações para monitoramento de
              utilização do Vista da melhor maneira possível; (iv) verificar a
              proteção do Vista contra erros, fraudes ou qualquer outro crime
              eletrônico; v) oferecer produtos e serviços que se enquadrem no
              perfil do Usuário.
            </p>

            <p className="text-justify">
              A PLATAFORMA VISTA poderá acessar os seus Dados Financeiros para
              lhe prestar atendimento quando você precisar como, por exemplo,
              para entender o motivo de qualquer problema relacionado a
              eventuais bugs no software.
            </p>
            <br />

            <h2 className="privacy_title">
              Comunicação com os PLANEJADORES FINANCEIROS
            </h2>
            <br />

            <p className="text-justify">
              A PLATAFORMA VISTA poderá ligar para os PLANEJADORES FINANCEIROS
              e/ou enviar-lhes mensagens, e-mails ou notificações com alertas e
              comunicados a fim de auxiliá-los a explorar todas as suas
              funcionalidades, para melhores práticas de planejamento
              financeiro, inclusão de funcionalidades, lembretes de uso do
              Site/Aplicativo, recomendação de produtos e serviços de Parceiros,
              entre outros.
            </p>

            <h2 className="privacy_title">Pesquisa de produtos/serviços</h2>

            <p className="text-justify">
              A PLATAFORMA VISTA poderá processar os dados coletados para fins
              de pesquisa, bem como para identificar necessidades dos USUÁRIOS,
              e desenvolver funcionalidades/melhorias.
            </p>
            <br />

            <h2 className="privacy_title">
              3. DO COMPARTILHAMENTO DAS INFORMAÇÕES
            </h2>
            <br />

            <p className="text-justify">
              <strong>Potenciais Parceiros:</strong> Com o objetivo de
              estabelecer novas parcerias, a PLATAFORMA VISTA poderá
              compartilhar com potenciais Parceiros algumas informações que
              sejam necessárias à concretização de tal parceria. O potencial
              Parceiro apenas poderá receber as informações após a assinatura de
              Acordo de Confidencialidade prevendo que os dados compartilhados
              serão utilizados única e exclusivamente para os fins da
              concretização da parceria e serão destruídos caso essa não
              aconteça. Nenhum Dado Financeiro não anonimizado poderá ser
              compartilhado sem a autorização dos USUÁRIOS;
            </p>

            <p className="text-justify">
              <strong>Consultores profissionais:</strong> Consultores fiscais,
              jurídicos ou outros consultores corporativos que nos prestam
              serviços profissionais, visando efetivar algum eventual auxílio ou
              solução de problemas no decorrer da utilização da PLATAFORMA
              VISTA;
            </p>

            <p className="text-justify">
              <strong>Fornecedores:</strong> Que nos ajudam com atividades de
              marketing (por exemplo: envios de e-mails marketing), parceiros de
              negócio e subcontratados de terceiros para administração, suporte,
              processamento, serviços ou propósitos de TI;
            </p>

            <p className="text-justify">
              <strong>Terceiros:</strong> No caso de considerarmos vender ou
              comprar qualquer negócio ou ativo, poderemos divulgar suas
              informações pessoais a possíveis vendedores ou compradores de tais
              negócios ou ativos. Se a PLATAFORMA VISTA estiver envolvida em uma
              fusão, aquisição ou venda de ativos, suas informações pessoais
              poderão ser transferidas como um ativo comercial. Nesses casos,
              forneceremos um aviso prévio de pelo menos 30 dias antes de suas
              informações pessoais serem transferidas e/ou ficarem sujeitas a
              uma Política de Privacidade diferente;
            </p>

            <p className="text-justify">
              <strong>Empresas que prestam serviços para o Vista:</strong> A
              PLATAFORMA VISTA poderá compartilhar dados coletados com
              prestadores de serviços terceirizados na medida em que tais
              informações sejam necessárias à prestação do serviço, a qual é
              feita sob orientação e em nome do Vista. Esses prestadores de
              serviços terceirizados podem ter sido contratados para, por
              exemplo, prestar atendimento aos USUÁRIOS, auxiliar na resolução
              de problemas técnicos, realizar a checagem de informações e
              prevenção à fraude, avaliar e gerenciar riscos, entre outros.
            </p>
            <br />

            <h2 className="privacy_title">
              4. DO ARMAZENAMENTO DAS INFORMAÇÕES COLETADAS
            </h2>
            <br />

            <p className="text-justify">
              Consideramos nossa prioridade a segurança de todos os dados
              relacionados aos nossos Usuários. Apesar de qualquer sistema de
              proteção estar sujeito a defeitos e possíveis violações que podem
              levar ao vazamento de dados, buscamos sempre evitar que isso
              ocorra ao adotar um nível de proteção alto, certificado por
              empresas especializadas em segurança.
            </p>

            <p className="text-justify">
              Todos os dados identificáveis são armazenados de forma
              criptografada. As informações são protegidas com a tecnologia SSL
              (<em>Secure Socket Layer</em>) para que os dados dos USUÁRIOS
              permaneçam em sigilo. Além disso, essa tecnologia visa ao
              impedimento que as informações sejam transmitidas ou acessadas por
              terceiros.
            </p>

            <p className="text-justify">
              Por todo o tempo em que os USUÁRIOS mantiverem a sua Conta na
              PLATAFORMA VISTA, todas as informações coletadas serão
              sincronizadas, processadas e armazenadas em servidores com alto
              padrão de segurança, inclusive em servidores localizados fora do
              Brasil.
            </p>

            <p className="text-justify">
              A PLATAFORMA VISTA poderá armazenar certos Dados Pessoais seus
              durante o período exigido legislação específica, tal como (i)
              Armazenamento de Dados Pessoais cadastrais por 5 anos após o
              término da relação jurídica (Art. 12 e 34, do Código de Defesa do
              Consumidor); e (ii) Armazenamento de Dados de identificação
              digital por 6 meses (Art. 14, do Marco Civil da Internet). Além
              disso, em cumprimento de eventuais ordens de autoridade públicas,
              para viabilizar o exercício regular de nossos direitos em
              processos judiciais, administrativos ou arbitrais, demonstrações
              de auditoria, prevenção à fraude e para outros interesses
              legítimos nossos, sempre em conformidade com as legislações
              mencionadas em seu item 2, A PLATAFORMA VISTA também poderá
              guardar dados de certos USUÁRIOS por um período legal superior ao
              indicado acima e, mediante decisão judicial, poderá ter que
              disponibilizar dados coletados de USUÁRIOS a um requerente.
            </p>
            <br />

            <h2 className="privacy_title">5. DOS COOKIES</h2>
            <br />
            <p className="text-justify">
              A PLATAFORMA VISTA utiliza cookies e tecnologias similares, como
              pixels e tags, para certificar que os serviços prestados estão de
              acordo com o melhor padrão esperado pelo Usuário.
            </p>

            <p className="text-justify">
              Os <em>cookies</em> coletados fornecem somente estatísticas e não
              serão utilizados para propósitos diversos dos expressamente
              previstos nesta Política de Privacidade e nos Termos de Uso.
            </p>

            <ul>
              <li>
                O que é <em>cookie</em>?
              </li>
            </ul>

            <p className="text-justify">
              <em>Cookie</em> é um pequeno arquivo adicionado ao seu dispositivo
              ou computador para fornecer uma experiência personalizada de
              acesso ao Vista
            </p>

            <ul>
              <li>
                Como a PLATAFORMA VISTA faz a coleta de <em>cookies</em>?
              </li>
            </ul>

            <p className="text-justify">
              A PLATAFORMA VISTA utiliza empresas especializadas em veiculação
              de propagandas, por exemplo, o Google e Facebook.
              <br />
              Ao aceitar esta Política de Privacidade, você concorda com a
              coleta dos cookies por empresas contratadas pela PLATAFORMA VISTA
              para esse fim.
            </p>

            <ul>
              <li>
                Que tipos de cookies <em>cookies </em>A PLATAFORMA VISTA
                utiliza?
              </li>
            </ul>

            <p className="text-justify">
              A PLATAFORMA VISTA permite a coleta de dois tipos de cookies:
              salvo e temporário:
            </p>

            <p className="text-justify">
              (i) Um cookie salvo é aquele que é introduzido no seu terminal de
              acesso (ex.: computador, tablet, etc.) quando você entra na sua
              conta. Este cookie serve para armazenar informações, como nome e
              senha, de maneira que o Usuário não tenha que se conectar sempre
              que acessar à PLATAFORMA VISTA.
            </p>

            <p className="text-justify">
              (ii) Um cookie temporário é aquele que é usado para identificar
              uma visita específica à PLATAFORMA VISTA. Estes cookies são
              removidos do terminal de acesso (ex.: computador, tablet, etc.)
              dos Usuários assim que este finaliza a utilização do navegador e
              são utilizados para armazenar informações temporárias.
            </p>

            <ul>
              <li>
                Para que os <em>cookies </em>são utilizados?
              </li>
            </ul>

            <p className="text-justify">
              A PLATAFORMA VISTA utiliza cookies para vários fins, incluindo:
            </p>

            <p className="text-justify">
              <strong>(i) ações de marketing para remarketing. </strong>
              Este recurso nos permite atingir os Usuários ou visitantes do{" "}
              <em>Website</em> para lembrá-los de efetuarem o cadastro na
              PLATAFORMA VISTA ou para que voltem a acessar a plataforma com
              facilidade; e
            </p>

            <p className="text-justify">
              <strong>(ii) entender </strong> o comportamento de uso do{" "}
              <em>Website</em> e do Aplicativo para melhor desenvolvimento do
              produto.
            </p>
            <br />

            <ul>
              <li>
                É possível limitar a coleta de <em>cookies</em>?
              </li>
            </ul>

            <p className="text-justify">
              Os navegadores em geral permitem que seja desabilitada a coleta de
              cookies. Dessa forma, caso você não altere os padrões de coleta de
              cookie do seu navegador, nós iremos considerar que você concorda
              com a coleta de cookies. Infelizmente, a PLATAFORMA VISTA poderá
              não funcionar da forma desejada caso seja desabilitada a coleta de
              cookies.
            </p>
            <br />

            <h2 className="privacy_title">6. DAS CAMPANHAS PUBLICITÁRIAS</h2>
            <br />

            <p className="text-justify">
              A PLATAFORMA VISTA poderá se valer de campanhas publicitárias
              visando à divulgação dos seus serviços. Nesse contexto, poderão
              ser utilizadas tecnologias como os cookies e/ou web beacons quando
              publicitam nosso <em>website</em>, o que fará com que essas
              ferramentas publicitárias (como o Google através do Google
              AdSense), também recebam a sua informação pessoal, como o endereço
              IP, o seu ISP, o seu browser, etc. Esta função é geralmente
              utilizada para geotargeting (ex: mostrar publicidades de São
              Paulo, para pessoas residentes em São Paulo) ou apresentar
              publicidade direcionada a um tipo de utilizador (Ex: mostrar
              publicidade de uma loja de roupas, a um utilizador que visita{" "}
              <em>websites</em> de marcas de roupa regularmente).
            </p>

            <p className="text-justify">
              Tal como outros <em>websites</em>, nos anúncios veiculados na
              internet, coletamos e utilizamos informações contidas nos anúncios
              feitos pela PLATAFORMA VISTA. A informação contida nos anúncios
              inclui o seu endereço IP (Internet Protocol), o seu ISP (Internet
              Service Provider), o browser que utilizou ao visitar nosso{" "}
              <em>website</em>, o tempo de visita e que páginas visitou dentro
              nosso <em>website</em>.
            </p>
            <br />

            <h2 className="privacy_title">7. DOS WEBSITES DE TERCEIROS</h2>
            <br />

            <p className="text-justify">
              A PLATAFORMA VISTA poderá ter ligações com outros{" "}
              <em>websites</em>, os quais poderão conter informações úteis aos
              nossos USUÁRIOS. Nossa política de privacidade não é extensiva aos{" "}
              <em>websites</em>
              de terceiros visitados a partir do nosso website. Em caso de
              visita à websites de terceiros, a partir do nosso <em>website</em>
              , deverá o utilizador ler a política de privacidade do{" "}
              <em>website</em> terceiro, a qual não se vincula em hipótese
              alguma politica de privacidade da PLATAFORMA VISTA.
            </p>

            <p className="text-justify">
              Não nos responsabilizamos pela política de privacidade ou conteúdo
              presente nos <em>websites</em> de terceiros.
            </p>
            <br />

            <h2 className="privacy_title">
              8. DO GERENCIAMENTO DE VIOLAÇÃO DE DADOS
            </h2>
            <br />

            <p className="text-justify">
              Todos os incidentes e potenciais violações de dados serão
              reportadas à nossa equipe responsável pela Tecnologia da
              Informação da PLATAFORMA VISTA.
            </p>

            <p className="text-justify">
              Todos os integrantes da referida equipe, estão cientes de sua
              responsabilidade pessoal de encaminhar e escalonar possíveis
              problemas, bem como de denunciar violações ou suspeitas de
              violações de Dados Pessoais assim que as identificarem.
            </p>

            <p className="text-justify">
              No momento em que um incidente ou violação real for descoberto, é
              essencial que os incidentes sejam informados e formalizados de
              forma tempestiva.
            </p>

            <p className="text-justify">
              Violações de Dados incluem, mas não se limitam a, qualquer perda,
              exclusão, roubo ou acesso não autorizado de Dados Pessoais
              controlados ou tratados pelo Vista.
            </p>
            <br />

            <h2 className="privacy_title">
              9. DA REVISÃO PERIÓDICA DOS PROCEDIMENTOS INTERNOS DE PROTEÇÃO DE
              DADOS
            </h2>
            <br />

            <p className="text-justify">
              A PLATAFORMA VISTA garante a realização de revisões periódicas a
              fim de confirmar que as iniciativas de Privacidade, seu sistema,
              medidas, processos, precauções e outras atividades incluindo o
              gerenciamento de proteção de Dados Pessoais são efetivamente
              hígidos e estão em conformidade com a legislação e regulamentação
              aplicáveis, bem como se encontram efetivamente aptos à mitigação
              riscos de violação de dados.
            </p>
            <br />

            <h2 className="privacy_title">
              10. DA REVISÃO, CORREÇÃO E DESCARTE DOS DADOS
            </h2>
            <br />

            <p className="text-justify">
              Os USUÁRIOS poderão solicitar a revisão e correção de seus dados
              sem qualquer ônus e a qualquer tempo. Para isso, basta entrar em
              contato por meio de um dos canais de atendimento disponíveis. Ao
              terminar sua relação com o Vista, caso deseje excluir seus dados,
              lembre-se que a PLATAFORMA VISTA, com o fim de cumprir com
              obrigações legais, armazenará determinados dados pelo período e
              nos termos que a legislação vigente aplicável exigir, conforme
              descrito no item 4 dessa Política de Privacidade.
            </p>
            <br />

            <h2 className="privacy_title">
              11. DO ACEITE DA POLÍTICA DE PRIVACIDADE DO VISTA
            </h2>
            <br />

            <p className="text-justify">
              A utilização da PLATAFORMA VISTA pressupõe aceitação integral
              desta Política de Privacidade. A equipe da PLATAFORMA VISTA
              reserva-se o direito de alterá-la sem aviso prévio. Deste modo,
              recomendamos que consulte nossa política de privacidade com
              regularidade de forma a estar sempre atualizado e em caso de
              qualquer eventual discordância, deverá se manifestar formalmente,
              por meios dos canais de comunicação disponibilizados.
            </p>
            <br />

            <p className="text-justify">Última revisão: 14 de Agosto de 2020</p>
            <br />

            <div
              className="justify-content-center pb-3"
              style={{ fontSize: "20px" }}
            ></div>
          </div>
          {this.renderRedirect()}
        </Row>
      </div>
    );
  }
}

export default TermosPrivacidade;
