import React from "react";
import jsPDF from "jspdf";
import "jspdf-autotable";

class Pdf extends React.Component {
  moment = require("moment");
  salvar = () => {
    var data = this.props.data;
    var total = 0;
    data.forEach((item) => {
      if (!this.props.familia) {
        total += item.valor;
      } else {
        total += item.valorPlano;
      }
    });

    var doc = new jsPDF("p", "pt");
    var source = window.document.getElementById(this.props.id);
    if (source === null) return;
    var res = doc.autoTableHtmlToJson(source);

    doc.setFontSize(24);
    doc.setTextColor(80);
    doc.text("Fatura Vista", 40, 50);

    var options = {
      startY: 75,
      theme: "striped",
      margin: { top: 30 },
    };

    doc.autoTable(res.columns, res.data, options);

    let width = doc.internal.pageSize.getWidth();
    doc.setFontSize(11);
    doc.setTextColor(100);
    doc.text(
      "Total: " +
        total.toLocaleString("pt-BR", {
          style: "currency",
          currency: "BRL",
        }),
      width - 120,
      doc.lastAutoTable.finalY + 40
    );

    doc.save("fatura-vista.pdf");
  };

  tablesRows = () => {
    var data = this.props.data;
    if (!this.props.familia) {
      return data.map((value, i) => {
        return this.line(value, i);
      });
    } else {
      return data.map((value, i) => {
        return this.lineFamilias(value, i);
      });
    }
  };

  line = (empresa, i) => {
    return (
      <tr id={i} style={{ height: "13px" }}>
        <td style={{ height: "12px" }}>{empresa.id}</td>
        <td style={{ height: "12px" }}>{empresa.nome}</td>
        <td style={{ height: "12px" }}>{empresa.documento}</td>
        <td style={{ height: "12px" }}>{empresa.telefone}</td>
        <td style={{ height: "12px" }}>
          {empresa.valor
            .toLocaleString("pt-BR", { style: "currency", currency: "BRL" })
            .replace("R$", "")}
        </td>
      </tr>
    );
  };

  lineFamilias = (familia, i) => {
    return (
      <tr id={i} style={{ height: "13px" }}>
        <td style={{ height: "12px" }}>{familia.id}</td>
        <td style={{ height: "12px" }}>{familia.nome}</td>
        <td style={{ height: "12px" }}>
          {!familia.quantidadeCarteiras ? 0 : familia.quantidadeCarteiras}
        </td>
        <td style={{ height: "12px" }}>
          {familia.valorPlano
            .toLocaleString("pt-BR", { style: "currency", currency: "BRL" })
            .replace("R$", "")}
        </td>
        <td style={{ height: "12px" }}>
          {this.convertDate(familia.dataCriacao)}
        </td>
        <td style={{ height: "12px" }}>
          {familia.ativo === 1 ? "Ativo" : "Inativo"}
        </td>
        <td style={{ height: "12px" }}>
          {familia.ativo === 0
            ? this.convertDate(familia.dataUltimaAtualizacao)
            : ""}
        </td>
      </tr>
    );
  };

  convertDate = (value) => {
    if (value !== null) {
      return <>{this.moment(value).format("DD/MM/YYYY")}</>;
    } else {
      return <></>;
    }
  };

  head = () => {
    if (this.props.familia === false) {
      return (
        <tr style={{ height: "12px" }}>
          <th>ID</th>
          <th>Nome</th>
          <th>Documento</th>
          <th>Telefone</th>
          <th>Valor</th>
        </tr>
      );
    } else {
      return (
        <tr style={{ height: "12px" }}>
          <th>ID</th>
          <th>Nome</th>
          <th>Qtd. Carteiras</th>
          <th>Valor</th>
          <th>Data Criação</th>
          <th>Status</th>
          <th>Data Inativação</th>
        </tr>
      );
    }
  };

  render() {
    return (
      <div style={{ float: "left", marginLeft: "5px" }}>
        <button
          style={{
            background: "#9a3dff",
            border: "0px solid #9a3dff",
            padding: "3px 10px",
            fontSize: 15,
            color: "white",
            borderRadius: "30px",
            cursor: "pointer",
          }}
          onClick={() => {
            this.salvar();
          }}
        >
          Fatura PDF
        </button>
        <table
          id={this.props.id}
          style={{ fontSize: "9px", position: "absolute", zIndex: "-1" }}
        >
          <thead>{this.head()}</thead>
          <tbody>{this.tablesRows()}</tbody>
        </table>
      </div>
    );
  }
}

export default Pdf;
