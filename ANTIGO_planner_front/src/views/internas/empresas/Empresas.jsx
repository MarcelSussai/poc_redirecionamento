import React from "react";
import { Redirect } from "react-router-dom";
import { CircularProgress } from "@material-ui/core";
import MUIDataTable from "mui-datatables";
import Datepicker from "react-datepicker";
import TableRow from "@material-ui/core/TableRow";
import TableCell from "@material-ui/core/TableCell";
import axios from "axios";
import CustomFooter from "../CustomFooter.jsx";
import Pdf from "./Pdf";
import { createMuiTheme, MuiThemeProvider } from "@material-ui/core/styles";
import { Row, Card, CardHeader } from "reactstrap";
import GoBackButton from '../../../components/Buttons/GoBackButton'

class Empresas extends React.Component {
  moment = require("moment");

  state = {
    page: 0,
    count: 1,
    data: [["Carregando Dados..."]],
    isLoading: false,
    dataProcurada: this.moment(new Date())
      .add(1, "months")
      .startOf("month")
      .toDate(),
    sumario: [],
    familias: [],
    empresas: [],
  };

  mascaras = {
    telefone: "(11) 1111-1111",
    celular: "(11) 1 1111-1111",
    cpf: "111.111.111-11",
    cnpj: "11.111.111/1111-11",
    inputMes: "11/1111",
  };

  getMuiTheme = () =>
    createMuiTheme({
      overrides: {
        MuiTableRow: {
          root: {
            "&:nth-of-type(odd)": {
              backgroundColor: "#f8f9fe",
            },
          },
        },
        MUIDataTableSelectCell: {
          fixedHeaderCommon: {
            backgroundColor: "unset",
          },
        },
        MuiTableCell: {
          root: {
            fontFamily: "unset",
            fontSize: "unset",
          },
          footer: {
            fontWeight: 700,
            borderRadius: "0 0 15px 15px",
          },
        },
        MUIDataTableHeadCell: {
          fixedHeaderYAxis: {
            fontWeight: 700,
          },
        },
        MuiPaper: {
          root: {
            overflow: "hidden",
          },
          elevation4: {
            borderRadius: 15,
          },
        },
        MUIDataTableToolbar: {
          left: {
            margin: 10,
          },
        },
        MuiToolbar: {
          regular: {
            backgroundColor: "#e4eaf4",
            borderRadius: "15px 15px 0 0",
          },
        },
      },
    });

  componentDidMount() {
    this.setState(
      {
        accessToken: localStorage.getItem("access-token"),
        empresaId: localStorage.getItem("empresa-id"),
        planejadorId: localStorage.getItem("planejador-id"),
        planejadorPrincipal: localStorage.getItem("planejador-principal"),
        tipoUsuario: localStorage.getItem("tipo-usuario"),
      },
      function () {
        //Se é o planejador senior ou o admin
        if (
          parseInt(this.state.tipoUsuario) === 0 ||
          (parseInt(this.state.planejadorPrincipal) === 1 &&
            parseInt(this.state.tipoUsuario) === 1)
        ) {
          this.getData();
        }
        global.mostrarAjudaDaPaginaAutomaticamente(this);
      }
    );
  }

  setRedirect = () => {
    this.setState({
      redirect: true,
    });
  };

  renderRedirect = () => {
    if (this.state.redirect) {
      return <Redirect to="/auth/login" />;
    }
  };

  getData = () => {
    if (this.state.interval != null) {
      clearInterval(this.state.interval);
    }

    if (this.state.accessToken == null) {
      this.setRedirect();
      return;
    }

    this.setState({ isLoading: true });

    //Admins
    //var url = global.server_api + 'api/empresa/filtro/';
    var url = global.server_api_new + global.apiToken + "/empresa/filtro";

    //Planners
    if (this.state.planejadorId != null)
      //url = global.server_api + 'api/empresa/planejador/' + this.state.planejadorId + '/filtro/';
      url =
        global.server_api_new +
        global.apiToken +
        "/empresa/planejador/" +
        this.state.planejadorId +
        "/filtro/";

    var filtro = {
      Filtro: this.state.textoDigitado,
      Ordenacao: this.state.sortField != null ? this.state.sortField : "Nome",
    };

    var config = {
      headers: {
        Authorization: "bearer " + this.state.accessToken,
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Headers": "Authorization",
        "Access-Control-Allow-Methods":
          "GET, POST, OPTIONS, PUT, PATCH, DELETE",
      },
    };

    axios.post(url, filtro, config).then((res) => {
      var empresas = res.data.results;
      var empresasIds = this.allEmpresasIds(empresas);
      this.recursiveEmpresas(empresas, empresasIds, 0, () => {
        const total = res.data.totalResults;
        this.setState({
          empresas: empresas,
          empresasIds: empresasIds,
          isLoading: false,
          count: total,
        });
      });
    });
  };

  recursiveEmpresas(empresas, empresasIds, i, ready) {
    if (i === empresasIds.length) {
      ready();
      return;
    }
    var id = empresasIds[i];
    this.getFamiliaId(id, (res) => {
      if (res.data.success) {
        var toUpdate = empresas[i];
        var familias = res.data.results;
        toUpdate.familias = familias;
        this.recursiveEmpresas(empresas, empresasIds, ++i, ready);
      }
    });
  }

  currentData(array) {
    var empresas = {};
    array.forEach((value) => {
      if (
        value.empresa !== undefined &&
        empresas[value.empresa.id] === undefined
      ) {
        empresas[value.empresa.id] = value.empresa;
      }
    });

    return empresas;
  }

  allEmpresasIds(empresas) {
    return empresas.map((value) => value.id);
  }

  allFamilias = (empresas) => {
    var keys = Object.keys(empresas);
    var familias = [];
    for (var i = 0; i < keys.length; i++) {
      var empresa = empresas[keys[i]];
      empresa.familias.forEach((value) => {
        familias.push(value);
      });
    }
    return familias;
  };

  calcularTodos(array) {
    // calcularTodos(array, proximoMes) {
    var valor = 0;
    if (array == null) {
      return valor;
    }
    array.forEach((value) => {
      var dataAtual = new Date(this.state.dataProcurada);
      var dataCriacao = new Date(value.dataCriacao);
      var dataAtualizacao = new Date(value.dataUltimaAtualizacao);

      dataAtual.setHours(12, 0, 0);
      dataCriacao.setHours(12, 0, 0);
      dataAtualizacao.setHours(12, 0, 0);

      var carteiras = value.quantidadeCarteiras;

      var valorReferencia = 0;

      //Plano individual
      if (carteiras <= 1) {
        valorReferencia = 49.9;
      }
      //Plano familiar
      else {
        valorReferencia = 59.9;
      }

      var primeiroDia = this.moment(dataAtual)
        .add(-1, "months")
        .startOf("month")
        .toDate();
      var ultimoDia = this.moment(dataAtual)
        .add(-1, "months")
        .endOf("month")
        .toDate();

      primeiroDia.setHours(12, 0, 0);
      ultimoDia.setHours(12, 0, 0);

      var hoje = new Date();
      hoje.setHours(12, 0, 0);

      // var carencia = global.carenciaCobranca;
      var clienteAtivo = value.ativo === 1;

      // Mostra a diferença em dias
      const qtdDiasPeriodo = global.diferencaEntreDatasEmDias(
        primeiroDia,
        ultimoDia
      );

      const qtdDiasExistencia = !clienteAtivo
        ? global.diferencaEntreDatasEmDias(dataCriacao, dataAtualizacao)
        : global.diferencaEntreDatasEmDias(
            dataCriacao,
            hoje.getTime() > ultimoDia.getTime() ? hoje : ultimoDia
          );

      //g('hoje', hoje, 'primeiro Dia', primeiroDia, 'ultimo dia', ultimoDia);

      if (dataCriacao.getTime() > ultimoDia.getTime()) {
        valorReferencia = 0;
        //console.log('Criado após o periodo de referencia - ZERO', value.empresa.nome, value.nome);
      }
      //Inativado a mais de 1 periodo de referencia - ZERO
      else if (
        !clienteAtivo &&
        dataAtualizacao.getTime() < primeiroDia.getTime()
      ) {
        valorReferencia = 0;
        //console.log('Inativado a mais de 1 periodo de referencia - ZERO', value.empresa.nome, value.nome);
      }
      //Trial - cancelados (data ultima atualizacao - data criacao <= 7)
      else if (
        global.proRata7Dias === true &&
        !clienteAtivo &&
        qtdDiasExistencia >= 0 &&
        qtdDiasExistencia <= 7
      ) {
        valorReferencia = 0;
        //console.log('Dentro do periodo de TRIAL - ZERO', qtdDiasExistencia, value.empresa.nome, value.nome);
      }
      //Trial - ativos (data de hoje - criacao <= 7)
      else if (
        global.proRata7Dias === true &&
        clienteAtivo &&
        qtdDiasExistencia >= 0 &&
        qtdDiasExistencia <= 7
      ) {
        valorReferencia = 0;
        //console.log('Dentro do periodo de TRIAL - ZERO', qtdDiasExistencia, value.empresa.nome, value.nome);
      }
      //Pro-Rata - criacao (final do periodo - data criacao)
      else if (
        clienteAtivo &&
        dataCriacao.getTime() >= primeiroDia.getTime() &&
        dataCriacao.getTime() <= ultimoDia.getTime()
      ) {
        var utilizado1 = global.diferencaEntreDatasEmDias(
          dataCriacao,
          ultimoDia
        );
        valorReferencia = (utilizado1 / qtdDiasPeriodo) * valorReferencia;
        //console.log('ATIVO - PRO RATA', utilizado1, qtdDiasPeriodo, utilizado1 / qtdDiasPeriodo, valorReferencia, value.empresa.nome, value.nome);
      }
      //Pro-Rata - exclusao (data atualizacao - inicio do periodo)
      else if (
        !clienteAtivo &&
        dataAtualizacao.getTime() >= primeiroDia.getTime() &&
        dataAtualizacao.getTime() <= ultimoDia.getTime()
      ) {
        var utilizado2 = global.diferencaEntreDatasEmDias(
          primeiroDia,
          dataAtualizacao
        );
        valorReferencia = (utilizado2 / qtdDiasPeriodo) * valorReferencia;
        //console.log('INATIVO - PRO RATA', utilizado2, qtdDiasPeriodo, utilizado2 / qtdDiasPeriodo, valorReferencia, value.empresa.nome, value.nome);
      } else {
        //console.log('FULL 1', primeiroDia, ultimoDia, global.proRata7Dias);
        //console.log('FULL 2', clienteAtivo, qtdDiasExistencia, dataCriacao, dataAtualizacao, value.empresa.nome, value.nome);
      }

      valor += valorReferencia;
    });

    return valor;
  }

  getDateFromString(str) {
    return new Date(str);
  }

  monthCorrection = (mes) => {
    return mes > 9 ? mes.toString() : "0" + mes;
  };

  handleChange = (date) => {
    this.setState({
      dataProcurada: date,
    });
  };

  loadingRender = (empresas) => {
    if (this.state.isLoading) {
      return (
        <CircularProgress
          size={24}
          style={{ marginLeft: 15, position: "relative", top: 4 }}
        />
      );
    } else {
      return (
        <div>
          <div style={{ float: "left" }}>
            <font>Data de referência:</font>
            <Datepicker
              locale="br"
              id={0}
              dateFormat="MM/yyyy"
              onChange={this.handleChange}
              showYearDropdown
              dateFormatCalendar="MMMM"
              scrollableYearDropdown
              yearDropdownItemNumber={5}
              showMonthYearPicker
              showFullMonthYearPicker
              selected={this.state.dataProcurada}
            />
          </div>
          <Pdf id={"raiz"} familia={false} data={empresas} />
        </div>
      );
    }
  };

  getFamiliaId = (id, callback) => {
    //var url = `${global.server_api}api/familia/empresa/${id}/billing`;
    var url = `${global.server_api_new}${global.apiToken}/familia/empresa/${id}/billing`;

    var config = {
      headers: {
        Authorization: "bearer " + this.state.accessToken,
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Headers": "Authorization",
        "Access-Control-Allow-Methods":
          "GET, POST, OPTIONS, PUT, PATCH, DELETE",
      },
    };

    axios.get(url, {}, config).then((res) => {
      if (callback) {
        callback(res);
      }
    });
  };

  render() {
    var empresas = [];
    if (this.state.empresas) empresas = this.state.empresas;
    var valorTotal = 0;

    empresas.forEach((value) => {
      value.valor = this.calcularTodos(value.familias);
      //value.valorProxMes = this.calcularTodos(value.familias, true);
      valorTotal += value.valor;
    });
    var sumario = [
      `Total: ${valorTotal
        .toLocaleString("pt-BR", { style: "currency", currency: "BRL" })
        .replace("R$", "")}`,
    ];

    const columns = [
      {
        name: "id",
        options: {
          display: false,
          viewColumns: false,
          filter: false,
        },
      },
      {
        name: "nome",
        label: "Nome",
        options: {
          display: true,
          viewColumns: true,
          filter: false,
        },
      },
      {
        name: "documento",
        label: "CPF/CNPJ",
        options: {
          display: true,
          viewColumns: true,
          filter: false,
        },
      },
      {
        name: "telefone",
        label: "Telefone",
        options: {
          display: true,
          viewColumns: true,
          filter: false,
        },
      },
      {
        name: "valor",
        label: "Valor Total",
        options: {
          display: true,
          viewColumns: true,
          filter: false,
          customBodyRender: (value) => {
            if (value)
              return (
                <>
                  {value
                    .toLocaleString("pt-BR", {
                      style: "currency",
                      currency: "BRL",
                    })
                    .replace("R$", "")}
                </>
              );
            else return <></>;
          },
        },
      },
      // {
      //     name: "valorProxMes",
      //     label: "Valor próximo mês",
      //     options: {
      //         display: true,
      //         viewColumns: true,
      //         filter: false,
      //         customBodyRender: (value) => {
      //             if(value)
      //             return (
      //                 <>{value.toLocaleString('pt-BR', { style: 'currency', currency: 'BRL' }).replace('R$', '')}</>);
      //             else
      //                 return (<></>);
      //         }
      //     }
      // }
    ];

    //const { page, count } = this.state;

    const options = {
      textLabels: global.textLabels,
      filter: false,
      filterType: "dropdown",
      responsive: "stacked",
      print: false,
      download: true,
      //serverSide: true,
      searchText: this.state.textoDigitado,
      // count: count,
      // page: page,
      pagination: false,
      sort: true,
      expandableRows: true,
      selectableRows: "none",
      viewColumns: false,
      //selectableRowsOnClick: true,
      // rowsPerPage: 15,
      onColumnSortChange: (changedColumn) => {
        //ASC
        let order = "";
        let direcaoOrdenacao = "asc";

        if (this.state.colunaOrdenacao === changedColumn) {
          if (
            this.state.direcaoOrdenacao != null &&
            this.state.direcaoOrdenacao === "asc"
          ) {
            order = " Desc";
            direcaoOrdenacao = "desc";
          }
        }

        this.setState(
          {
            sortField: changedColumn + order,
            colunaOrdenacao: changedColumn,
            direcaoOrdenacao: direcaoOrdenacao,
          },
          () => {
            this.getData();
          }
        );
      },
      renderExpandableRow: (rowData, rowMeta) => {
        var empresaId = rowData[0];
        var empresa = this.state.empresas.find((value) => {
          return value.id === empresaId;
        });
        var familias = [];
        if (
          empresa !== undefined &&
          empresa.familias !== undefined &&
          empresa.familias !== null
        ) {
          familias = empresa.familias.map((value) => {
            value.valorPlano = this.calcularTodos([value]);
            return value;
          });
        }

        const options = {
          print: false,
          download: false,
          filter: false,
          pagination: false,
          selectableRows: "none",
          textLabels: global.textLabels,
        };

        const columns = [
          {
            name: "nome",
            label: "Nome",
            options: {
              display: true,
              viewColumns: true,
              filter: false,
            },
          },
          {
            name: "quantidadeCarteiras",
            label: "Qtd. Carteiras",
            options: {
              display: true,
              viewColumns: true,
              filter: false,
              customBodyRender: (value) => {
                return <>{value === null || value === undefined ? 0 : value}</>;
              },
            },
          },
          {
            name: "valorPlano",
            label: "Valor",
            options: {
              display: true,
              viewColumns: true,
              filter: false,
              customBodyRender: (value) => {
                if (value != null)
                  return (
                    <>
                      {value
                        .toLocaleString("pt-BR", {
                          style: "currency",
                          currency: "BRL",
                        })
                        .replace("R$", "")}
                    </>
                  );
                else return <></>;
              },
            },
          },
          {
            name: "dataCriacao",
            label: "Data Criação",
            options: {
              display: true,
              viewColumns: true,
              filter: false,
              customBodyRender: (value) => {
                if (value !== null) {
                  return <>{this.moment(value).format("DD/MM/YYYY")}</>;
                } else {
                  return <></>;
                }
              },
            },
          },
          {
            name: "ativo",
            label: "Status",
            options: {
              display: true,
              viewColumns: true,
              filter: false,
              customBodyRender: (value) => {
                if (value === 1) {
                  return <>Ativo</>;
                } else {
                  return <>Inativo</>;
                }
              },
            },
          },
          {
            name: "dataUltimaAtualizacao",
            label: "Data Inativação",
            options: {
              display: true,
              viewColumns: true,
              filter: false,
              customBodyRender: (value, tableMeta) => {
                var ativo = tableMeta.rowData[4];
                if (value !== null && ativo === 0) {
                  return <>{this.moment(value).format("DD/MM/YYYY")}</>;
                } else {
                  return <></>;
                }
              },
            },
          },
        ];
        const colSpan = 20;
        return (
          <TableRow>
            <TableCell colSpan={colSpan}>
              <div id="orcamento-table">
                <MuiThemeProvider theme={this.getMuiTheme()}>
                  <MUIDataTable
                    title={
                      <div>
                        <Pdf
                          id={"empresa-" + empresaId}
                          familia={true}
                          data={familias}
                        />
                      </div>
                    }
                    data={familias}
                    columns={columns}
                    options={options}
                  />
                </MuiThemeProvider>
              </div>
            </TableCell>
          </TableRow>
        );
      },
      customFooter: (
        count,
        page,
        rowsPerPage,
        changeRowsPerPage,
        changePage,
        textLabels
      ) => {
        return (
          <CustomFooter
            count={count}
            page={page}
            rowsPerPage={rowsPerPage}
            changeRowsPerPage={changeRowsPerPage}
            changePage={changePage}
            textLabels={textLabels}
            data2={sumario}
            hasPagination={false}
          />
        );
      },
    };

    return (
      <div style={{ padding: "0 5%", marginTop: 20 }}>
        <Card style={{ border: "none" }}>
          <CardHeader className="border-0">
            <Row className="align-items-center">
              <div className="col">
                <img
                  src={require("../../../assets/img/theme/icone-calendario.png")}
                  alt="Meios de Pagamento"
                  className="before-title-img"
                />
                <h3 className="mb-0 chart-title">Empresas</h3>
              </div>
            </Row>
            <GoBackButton />
          </CardHeader>
        </Card>
        <MuiThemeProvider theme={this.getMuiTheme()}>
          <MUIDataTable
            id="orcamento-table"
            title={<div>{this.loadingRender(empresas)}</div>}
            data={empresas}
            columns={columns}
            options={options}
          />
        </MuiThemeProvider>
      </div>
    );
  }
}

export default Empresas;
