import React from "react";
import Select from "react-select";
import axios from "axios";
import { confirmAlert } from "react-confirm-alert";
import "react-confirm-alert/src/react-confirm-alert.css";
import SkyLight from "react-skylight";
import MUIDataTable from "mui-datatables";

// reactstrap components
import { Card, CardHeader, Container, Row, Col, Table } from "reactstrap";

class Orcamentos extends React.Component {
  moment = require("moment");

  state = {
    activePage: 1,
    itensPerPage: 12,
    pageRangeDisplayed: 5,
    totalItemsCount: 450,
    sortField: null,
    registros: [],
    carregamentoInicial: false,
    interval: null,
    textoDigitado: "",
    tiposOrcamento: [],
  };

  camposParaOrdenacao = [
    { value: "Nome", label: "Nome" },
    { value: "Nome Desc", label: "Nome Decrescente" },
    { value: "Tipo", label: "Tipo de Orçamento" },
  ];

  getTipoOrcamento = (tipo) => {
    var filtro = null;
    var i = 0;
    if (tipo === "modal") {
      filtro = this.state.tiposOrcamento.filter(function (item) {
        return i++ !== 0;
      });
    } else {
      filtro = this.state.tiposOrcamento.filter(function (item) {
        return i++ !== 1;
      });
    }
    return filtro;
  };

  mapearTipoOrcamentoPeloId = (id) => {
    var tipoOrcamento = this.state.tiposOrcamento.filter(function (item) {
      return parseInt(item.value) === parseInt(id);
    });
    return tipoOrcamento && tipoOrcamento[0] ? tipoOrcamento[0].label : "";
  };

  montarDropDown = (itens, id, changeMethod, component, selectedValue) => (
    <Select
      id={id}
      onChange={changeMethod}
      className="select-component"
      options={itens}
      defaultValue={itens != null ? itens[0] : ""}
      component={component != null ? component : null}
      placeholder="Selecione..."
      value={
        itens != null && selectedValue != null
          ? itens.find((op) => {
              return (
                (op.value != null ? op.value.toString() : null) ===
                (selectedValue != null ? selectedValue.toString() : null)
              );
            })
          : null
      }
    />
  );

  mostrarCamposParaOrdenacao = (camposParaOrdenacao) =>
    this.montarDropDown(
      camposParaOrdenacao,
      "selecao-ordenacao",
      this.mudarOrdenacao,
      null,
      null
    );

  mudarOrdenacao = (selectedOption) => {
    const sortField = selectedOption.value;

    this.setState({ sortField }, function () {
      this.atualizarRegistros();
    });
  };

  mudarTipoOrcamentoFiltro = (selectedOption) => {
    const tipoOrcamentoFiltro = selectedOption.value;
    console.log(selectedOption);
    this.setState({ tipoOrcamentoFiltro }, function () {
      this.atualizarRegistros();
    });
  };

  mudarTipoOrcamento = (selectedOption) => {
    const tipoOrcamento = selectedOption.value;
    console.log(tipoOrcamento);
    this.setState({ tipoOrcamento });
  };

  mostrarLista = () => {
    return (
      <>
        <Card className="shadow list-entries">
          <Table>
            <th width="15"></th>
            <th width="15"></th>
            <th>Nome</th>
            <th>Tipo Orçamento</th>
            {this.state.registros != null &&
              this.mostrarDados(this.state.registros)}
          </Table>
        </Card>
      </>
    );
  };

  mostrarMuiDataTable = () => {
    const options = {
      textLabels: global.textLabels,
      filter: true,
      filterType: "dropdown",
      responsive: "stacked",
      print: false,
      download: false,
      rowsPerPage: 15,
      onRowsDelete: (rowsDeleted) => {
        this.removerRegistros(rowsDeleted);
        return false;
      },
      onRowClick: (rowData) => {
        this.editar(rowData[0]);
      },
    };
    const columns = [
      {
        name: "id",
        options: {
          display: false,
          viewColumns: false,
          filter: false,
        },
      },
      {
        name: "tipoOrcamento",
        label: "Tipo de Orçamento",
        options: {
          display: true,
          viewColumns: true,
          filter: true,
          customBodyRender: (value) => {
            if (value != null) {
              return <>{this.mapearTipoOrcamentoPeloId(value)}</>;
            }
            return <></>;
          },
        },
      },
      {
        name: "nome",
        label: "Nome",
        options: {
          display: true,
          viewColumns: true,
          filter: true,
        },
      },
    ];
    var data = [];
    if (this.state.registros != null) {
      data = this.state.registros.map((item, index) => {
        return {
          id: item.id,
          nome: item.nome,
          tipoOrcamento: item.tipoOrcamento,
        };
      });
    }
    return (
      <div style={{ width: "100%" }}>
        <MUIDataTable data={data} columns={columns} options={options} />
      </div>
    );
  };

  removerRegistros = (rowsDeleted) => {
    const options = {
      title: "Exclusão de Orçamentos",
      message:
        "Confirma procedimento de exclusão? Este procedimento é irreversível!",
      buttons: [
        {
          label: "Sim",
          onClick: () => {
            var ids = [];

            for (var key in rowsDeleted.data) {
              ids.push(
                this.state.registros[rowsDeleted.data[key].dataIndex].id
              );
            }

            if (ids.length > 0) {
              //var url = global.server_api + 'api/orcamento/bulk/' + ids.join(",");
              var url =
                global.server_api_new +
                global.apiToken +
                "/orcamento/bulk/" +
                ids.join(",");

              if (this.state.familiaId == null) {
                //url = global.server_api + 'api/orcamento/bulk/padrao/' + ids.join(",");
                url =
                  global.server_api_new +
                  global.apiToken +
                  "/orcamento/bulk/padrao/" +
                  ids.join(",");
              }

              var config = {
                headers: {
                  Authorization: "bearer " + this.state.accessToken,
                  "Access-Control-Allow-Origin": "*",
                  "Access-Control-Allow-Headers": "Authorization",
                  "Access-Control-Allow-Methods":
                    "GET, POST, OPTIONS, PUT, PATCH, DELETE",
                },
              };

              axios.delete(url, {}, config).then((res) => {
                const result = res.data;

                if (result.success && result.affectedResults > 0) {
                  this.atualizarRegistros();
                } else {
                  alert("Erro ao remover. " + res.data.exception.Message);
                  console.log(res.data.exception);
                }
              });
            }
          },
        },
        {
          label: "Não",
        },
      ],
    };

    confirmAlert(options);

    return false;
  };

  mostrarDados = (registros) => {
    if (registros == null) {
      return <></>;
    }

    return registros.map((item, key) => {
      return (
        <>
          <tr>
            <td>
              <div onClick={() => this.confirmaExcluir(item.id)}>
                <i className="far fa-trash-alt"></i>
              </div>
            </td>
            <td>
              <div onClick={() => this.editar(item.id)}>
                <i className="fas fa-info"></i>
              </div>
            </td>
            <td onClick={() => this.editar(item.id)}>{item.nome}</td>
            <td onClick={() => this.editar(item.id)}>
              {this.mapearTipoOrcamentoPeloId(item.tipoOrcamento)}
            </td>
          </tr>
        </>
      );
    });
  };

  atualizarRegistros = () => {
    if (this.state.interval != null) {
      clearInterval(this.state.interval);
    }

    //var url = global.server_api + 'api/orcamento/filtro';
    var url = global.server_api_new + global.apiToken + "/orcamento/filtro";

    if (this.state.familiaId != null) {
      //url = global.server_api + 'api/orcamento/familia/'+this.state.familiaId+'/filtro';
      url =
        global.server_api_new +
        global.apiToken +
        "/orcamento/familia/" +
        this.state.familiaId +
        "/filtro";
    }

    // if(this.state.planejadorId != null ){
    //   url = global.server_api + 'api/orcamento/planejador/'+this.state.planejadorId+'/filtro';
    // }

    console.log(url);

    var filtro = {
      Filtro: this.state.textoDigitado,
      Ordenacao: this.state.sortField,
      TipoOrcamento: this.state.tipoOrcamentoFiltro,
    };

    var config = {
      headers: {
        Authorization: "bearer " + this.state.accessToken,
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Headers": "Authorization",
        "Access-Control-Allow-Methods":
          "GET, POST, OPTIONS, PUT, PATCH, DELETE",
      },
    };

    axios.post(url, filtro, config).then((res) => {
      var registros = [];

      if (res.data.results != null) {
        registros = res.data.results.filter(function (item) {
          //Apenas receitas e despesas
          return item.tipoOrcamento === 0 || item.tipoOrcamento === 1;
        });
      }

      this.setState({ registros });
    });
  };

  confirmaExcluir = (id) => {
    const options = {
      title: "Exclusão de Registro",
      message:
        "Tem certeza que deseja excluir este Registro? Este procedimento é irreversível!",
      buttons: [
        {
          label: "Sim",
          onClick: () => this.excluir(id),
        },
        {
          label: "Não",
        },
      ],
    };

    confirmAlert(options);
  };

  excluir = (id) => {
    //var url = global.server_api + 'api/orcamento/' + id;
    var url = global.server_api_new + global.apiToken + "/orcamento/" + id;

    if (this.state.familiaId == null) {
      //url = global.server_api + 'api/orcamento/padrao/' + id;
      url = global.server_api_new + global.apiToken + "/orcamento/padrao/" + id;
    }

    var config = {
      headers: {
        Authorization: "bearer " + this.state.accessToken,
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Headers": "Authorization",
        "Access-Control-Allow-Methods":
          "GET, POST, OPTIONS, PUT, PATCH, DELETE",
      },
    };

    axios.delete(url, {}, config).then((res) => {
      const result = res.data;

      if (result.success && result.affectedResults > 0) {
        this.atualizarRegistros();
      } else {
        alert("Erro ao remover registro. " + res.data.exception.Message);
        console.log(res.data.exception);
      }
    });
  };

  editar = (id) => {
    var registroEdicao = this.state.registros.filter(function (item) {
      return item.id === id;
    });

    if (registroEdicao != null && registroEdicao.length > 0) {
      registroEdicao = registroEdicao[0];
      //Seta item edicao
      this.setState(
        {
          nome: registroEdicao.nome,
          tipoOrcamento: registroEdicao.tipoOrcamento,
          id: registroEdicao.id,
        },
        function () {
          //Abre modal
          global.showModal(this.modal);
        }
      );
    }
  };

  filtroPorTexto = (valorDigitado) => {
    const textoDigitado = valorDigitado.target.value;

    this.setState({ textoDigitado }, function () {
      if (this.state.interval != null) {
        clearInterval(this.state.interval);
      }

      const $this = this;

      const interval = setInterval(function () {
        $this.atualizarRegistros();
      }, 1000);

      this.setState({ interval });
    });
  };

  limparSelecao = () => {
    this.setState({
      nome: "",
      tipoOrcamento: null,
      id: null,
    });
  };

  salvar = () => {
    var item = {
      Nome: this.state.nome,
      TipoOrcamento: parseInt(this.state.tipoOrcamento),
      FamiliaId:
        this.state.familiaId != null
          ? parseInt(this.state.familiaId)
          : this.state.planejadorId != null
          ? this.state.familiaId2
          : null,
      EmpresaId:
        this.state.empresaId != null ? parseInt(this.state.empresaId) : null,
    };

    //var url = global.server_api + 'api/orcamento' + (this.state.id != null ? "/" + this.state.id: "");
    var url =
      global.server_api_new +
      global.apiToken +
      "/orcamento" +
      (this.state.id != null ? "/" + this.state.id : "");

    var config = {
      headers: {
        Authorization: "bearer " + this.state.accessToken,
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Headers": "Authorization",
        "Access-Control-Allow-Methods":
          "GET, POST, OPTIONS, PUT, PATCH, DELETE",
      },
    };

    axios.post(url, item, config).then((res) => {
      console.log(res.data);
      if (res.data.success === true) {
        this.limparSelecao();
        window.location.reload();
      } else {
        console.log(res.data.exception);
        const options = {
          title: "Erro ao Salvar Registro",
          message: res.data.exception.Message,
          buttons: [
            {
              label: "Ok",
            },
          ],
        };

        confirmAlert(options);
      }
    });
  };

  componentDidMount() {
    var tiposOrcamento = [{ value: null, label: "Selecione..." }];

    global.tipoOrcamento.forEach(function (value, index) {
      tiposOrcamento.push({
        value: index,
        label: value,
      });
    });

    this.setState(
      {
        accessToken: localStorage.getItem("access-token"),
        empresaId: localStorage.getItem("empresa-id"),
        familiaId: localStorage.getItem("familia-id"),
        planejadorId: localStorage.getItem("planejador-id"),
        tiposOrcamento: tiposOrcamento,
      },
      function () {
        this.atualizarRegistros();
        if (this.state.familiaId == null && this.state.planejadorId != null)
          this.atualizarFamilias();
        global.mostrarAjudaDaPaginaAutomaticamente(this);
      }
    );
  }

  atualizarFamilias = () => {};

  mostrarPopup = () => (
    <div>
      <SkyLight
        ref={(ref) => (this.modal = ref)}
        transitionDuration={0}
        beforeOpen={this.preCarregarParaEdicao}
        afterClose={this.limparSelecao}
        title={
          <>
            <div className="pop-up-title">
              <i className="ni ni-check-bold"></i>{" "}
              <h2>Cadastro de Orçamento</h2>
            </div>
          </>
        }
      >
        <Row>
          <Col lg="6" xl="6">
            <div>
              <label>Tipo de Orçamento</label>
              {this.montarDropDown(
                this.getTipoOrcamento("modal"),
                "select-tipo-orcamento",
                this.mudarTipoOrcamento,
                null,
                this.state.tipoOrcamento
              )}
            </div>
          </Col>
          <Col lg="6" xl="6">
            <div>
              <label>Nome</label>
              <input
                type="text"
                id="nome"
                value={this.state.nome}
                onChange={(e) => this.setState({ nome: e.target.value })}
              ></input>
            </div>
          </Col>
          <Col lg="12" xl="12">
            <button
              id="salvar-lancamento"
              className="featured-button"
              onClick={() => this.salvar()}
              style={{
                backgroundImage:
                  "url(" +
                  require("../../assets/img/theme/botao-destaque.png") +
                  ")",
              }}
            >
              Salvar
            </button>
          </Col>
        </Row>
      </SkyLight>
    </div>
  );

  render() {
    return (
      <>
        <Container fluid>
          <div className="home-summary-top">
            <Row>
              <Col lg="12" xl="12" style={{ marginBottom: "15px" }}>
                <div>
                  <Card>
                    <CardHeader className="border-0">
                      <Row className="align-items-center">
                        <div className="col">
                          <img
                            src={require("../../assets/img/theme/icone-calendario.png")}
                            alt="Orçamentos"
                            className="before-title-img"
                          />
                          <h3 className="mb-0 chart-title">Orçamentos</h3>
                        </div>
                        <div className="new-entry" style={{ float: "right" }}>
                          <div
                            className="nav-link-icon featured-button"
                            style={{
                              backgroundImage:
                                "url(" +
                                require("../../assets/img/theme/botao-destaque.png") +
                                ")",
                            }}
                            onClick={() => global.showModal(this.modal)}
                          >
                            <span className="nav-link-inner--text">
                              Novo Orçamento
                            </span>
                          </div>
                        </div>
                      </Row>
                    </CardHeader>
                  </Card>
                </div>
              </Col>
              {/* <Col lg="6" md="6" className="filterAndOnder">
                <label>Tipo de Orçamento </label>
                {this.montarDropDown(this.getTipoOrcamento('filtro'), "select-tipo-orcamento-filtro", this.mudarTipoOrcamentoFiltro, null, null)}
              </Col>
              <Col lg="6" md="6" className="filterAndOnder">
                <label>Ordenar </label>{this.mostrarCamposParaOrdenacao(this.camposParaOrdenacao)}
              </Col>
              <Col lg="6" md="6" className="filterAndOnder text-field">
                <label>Filtrar </label><input type="text" onKeyUp={this.filtroPorTexto}/>
              </Col> */}
            </Row>
            <Row className="entry-list">{this.mostrarMuiDataTable()}</Row>
          </div>
        </Container>
        {this.mostrarPopup()}
      </>
    );
  }
}

export default Orcamentos;
