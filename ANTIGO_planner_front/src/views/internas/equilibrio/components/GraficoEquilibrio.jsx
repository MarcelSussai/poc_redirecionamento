import React from "react";
import ApexChart from 'react-apexcharts';
import axios from 'axios';
import { registerLocale } from "react-datepicker";
import br from "date-fns/locale/pt-BR";

registerLocale("br", br);

class GraficoEquilibrio extends React.Component {

    moment = require('moment');

    state = {
        series : [
            {
                name: "Investimentos",
                data: []
            },
            {
                name: "Gastos",
                data: []
            }
        ],
        options: {
            legend: {
                show: false
            },
            dataLabels: {
                enabled: false
            },              
            stroke: {
                curve: 'smooth'
            },
            chart: {
                id: "basic-bar"
            },
            xaxis: {
                categories: []
            }
        }
    };

    componentDidMount() 
    {
        var $this = this;
        this.setState({
          accessToken : localStorage.getItem('access-token'),
          familiaId: localStorage.getItem('familia-id')
        }, $this.refreshData());


    }

    getData = () => {
        //var url = global.server_api + "api/EquilibrioFinanceiro/familia/"+ this.state.familiaId + "/grafico";
        var url = global.server_api_new + global.apiToken + "/EquilibrioFinanceiro/familia/"+ this.state.familiaId + "/grafico";
        var config = {
            headers: {
        'Authorization': "bearer " + this.state.accessToken,
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Headers": "Authorization", 
        "Access-Control-Allow-Methods": "GET, POST, OPTIONS, PUT, PATCH, DELETE" ,
      }
        };
    
        var filtro = {
            DataInicial: this.state.startDate,
            DataFinal: this.state.endDate
        }
    
        axios.post(url, filtro, config).then(res => {
            var despesas = [];
            var investimentos = [];
            var meses = [];

            var formatoMoeda = new Intl.NumberFormat([], {
                            style: 'currency',
                            currency: 'BRL'
                            })

            if(res.data.results != null){
                for(var i = 0; i < res.data.results[0].length; i++){
                    var despesa = res.data.results[0][i];
                    var investimento = res.data.results[1][i];

                    var mes = despesa.mes + "/" + despesa.ano;
                    
                    despesas.push(despesa.total.toLocaleString('pt-BR', { style: 'currency', currency: 'BRL' }).replace('R$', ''));
                    investimentos.push(investimento.total.toLocaleString('pt-BR', { style: 'currency', currency: 'BRL' }).replace('R$', ''));
                    meses.push(mes);
                }
            }

            var series = [
                {
                    name: "Investimentos R$",
                    data: investimentos
                },
                {
                    name: "Gastos R$",
                    data: despesas
                }
            ]

            var options = {
                legend: {
                    show: false
                },
                dataLabels: {
                    enabled: false
                },              
                stroke: {
                    curve: 'smooth'
                },
                chart: {
                    id: "basic-bar"
                },
                xaxis: {
                    categories: meses
                },
                colors:['#28ADC0', '#fa0000']
            }
            if(despesas !== undefined && investimentos !== undefined && 
                meses !== undefined)
            this.setState({ series: series,  options: options });
        });
    }

    setStartDate = (date, callback) => {

        if(date != null){
            date.setHours(0);
            date.setMinutes(0, 0, 0);
        }
    
        const startDate = date;
    
        this.setState({startDate} , function() {

            if(callback != null){
                callback();
            }
            else {
                this.getData();
            }
        });
    }
    
    setEndDate = (date, callback) => {

        if(date != null){
            date.setHours(20);
            date.setMinutes(59, 59, 0);
        }
    
        const endDate = date;
    
        this.setState({endDate} , function() {
            if(callback != null){
                callback();
            }
            else {
                this.getData();
            }
        });
    }

    refreshData = () => {
        var $this = this;

        var startDate   = localStorage.getItem('dataInicio') ? new Date( JSON.parse(localStorage.getItem('dataInicio')) ) : this.moment().startOf('month').toDate()
        var endDate     = localStorage.getItem('dataFim') ? new Date ( JSON.parse(localStorage.getItem('dataFim')) ) : this.moment().endOf('month').toDate() 

        this.setStartDate(startDate, function(){
            $this.setEndDate(endDate, function(){
                // console.log('RefreshData 4');
                $this.getData();
            });
        });
    }
    
    render() {
        return (
            <>
                <button id="grafico" onClick={ () => this.refreshData() } style={{position: "fixed", left: "-5000px"}}/>
                <ApexChart 
                    options={this.state.options} 
                    series={this.state.series} 
                    type="line" 
                    className="home-estimate-realized" />
            </>
        );
    }
}

export default GraficoEquilibrio;