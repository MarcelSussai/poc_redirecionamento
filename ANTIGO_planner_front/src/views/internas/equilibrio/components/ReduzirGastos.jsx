import React from "react";
import MUIDataTable from "mui-datatables";
import CurrencyInput from 'react-currency-input';
import axios from 'axios';
import { registerLocale } from "react-datepicker";
import br from "date-fns/locale/pt-BR";
import { createMuiTheme, MuiThemeProvider } from '@material-ui/core/styles';

registerLocale("br", br);

class ReduzirGastos extends React.Component {

    moment = require('moment');

    componentDidMount() 
    {
        var $this = this;

        this.setState({
          accessToken : localStorage.getItem('access-token'),
          familiaId: localStorage.getItem('familia-id')
        }, $this.refreshData())
    }

    getData = () => {
        //var url = global.server_api + "api/EquilibrioFinanceiro/familia/"+this.state.familiaId+"/orcamento";
        var url = global.server_api_new + global.apiToken + "/EquilibrioFinanceiro/familia/"+this.state.familiaId+"/orcamento";

        var config = {
            headers: {
        'Authorization': "bearer " + this.state.accessToken,
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Headers": "Authorization", 
        "Access-Control-Allow-Methods": "GET, POST, OPTIONS, PUT, PATCH, DELETE" ,
      }
        };

        var filtro = {
            DataInicial: this.state.startDate,
            DataFinal: this.state.endDate
        }
    
        axios.post(url, filtro, config).then(res => {
            var data = [];
            var despesasTotal = 0;
            var totalObjetivo = 0;
            var totalMesAtual = 0;

            if(res.data.results != null){
                res.data.results.forEach((item) => {
                    if(item.tipoOrcamento === 1) {
                        data.push({
                            id: item.id,
                            orcamentoId: item.orcamentoId,
                            nome: item.nome,
                            estimativa: item.estimado,
                            meta: item.meta,
                            objetivo: item.objetivo,
                            mesAtual: item.mesAtual,
                            ehTotal: 0
                        });
                        despesasTotal += item.estimado;
                        totalObjetivo += item.objetivo;
                        totalMesAtual += item.mesAtual;
                    }
                });
                data.push({
                    nome: 'Total',
                    estimativa: despesasTotal,
                    meta: null,
                    objetivo: totalObjetivo,
                    mesAtual: totalMesAtual,
                    ehTotal: 1
                  });
            }

            this.setState({ data });
        });
    }

    setStartDate = (date, callback) => {

        if(date != null){
            date.setHours(0);
            date.setMinutes(0, 0, 0);
        }
    
        const startDate = date;
    
        this.setState({startDate} , function() {
            if(callback != null){
                callback();
            }
            else {
                this.getData();
            }
        });
    }
    
    setEndDate = (date, callback) => {
        if(date != null){
            date.setHours(20);
            date.setMinutes(59, 59, 0);
        }
    
        const endDate = date;
    
        this.setState({endDate} , function() {
            if(callback != null){
                callback();
            }
            else {
                this.getData();
            }
        });
    }

    columns = [
        {
            name: "ehTotal",
            options: {
                display: false,
                viewColumns: false,
                filter: false
            }
        },
        {
            name: "id",
            options: {
                display: false,
                viewColumns: false,
                filter: false
            }
        },
        {
            name: "orcamentoId",
            options: {
                display: false,
                viewColumns: false,
                filter: false
            }
        },
        {
            name: "nome",
            label: "Nome",
            options: {
                display: true,
                viewColumns: true,
                filter: false
            }
        },
        {
            name: "estimativa",
            label: "Estimativa",
            options: {
                display: true,
                viewColumns: true,
                filter: false,
                customBodyRender: (value, tableMeta, updateValue) => {
                    
                    if(value == null)
                        value = 0;

                    return (
                        <div>
                            {value.toLocaleString('pt-BR', { style: 'currency', currency: 'BRL' }).replace('R$', '')}
                        </div>
                    );
                }
            }
        },
        {
            name: "meta",
            label: "Meta",
            options: {
                display: true,
                viewColumns: true,
                filter: false,  
                customBodyRender: (value, tableMeta, updateValue) => {

                    var $this = this;

                    if(value == null)
                        value = 0;

                    if(tableMeta.rowData[0] === 1){
                        return (
                            <></>
                        );
                    }

                    return (
                        <div style={{cursor: 'pointer'}}>
                            <CurrencyInput 
                                style={{width: "60px"}}
                                value={value} 
                                thousandSeparator={'.'}
                                suffix={' %'}
                                precision="2"
                                onChangeEvent={(event, formattedValue, value) => {
                                    updateValue(value);

                                    if(this.state.timeout != null){
                                        clearTimeout(this.state.timeout);
                                    }

                                    var timeout = setTimeout(function(){
                                        var orcamentoId = tableMeta.rowData[2];
                                        var familia = localStorage.getItem('familia-id');

                                        //var service = 'api/EquilibrioFinanceiro/familia/' +$this.state.familiaId+ '/orcamento/'+orcamentoId;
                                        var service =  `${global.apiToken}/EquilibrioFinanceiro/familia/${familia}/orcamento/${orcamentoId}`;
                                        var url = global.server_api_new + service;

                                        var config = {
                                            headers: {'Authorization': "bearer " + $this.state.accessToken}
                                        };
                                        
                                        var dados = {
                                            meta: value
                                        };

                                        axios.post(url, dados, config).then(res => {
                                            if(res.data.success){
                                                $this.getData();
                                            }
                                        });
                                    },1000);

                                    this.setState({timeout});

                                }}  
                            />
                        </div>
                    );
                }
            }
        },
        {
            name: "objetivo",
            label: "Objetivo",
            options: {
                display: true,
                viewColumns: true,
                filter: false,
                customBodyRender: (value, tableMeta, updateValue) => {

                    if(value == null)
                        value = 0;

                    return (
                        <div>
                            {value.toLocaleString('pt-BR', { style: 'currency', currency: 'BRL' }).replace('R$', '')}
                        </div>
                    );
                }
            }
        },
        {
            name: "mesAtual",
            label: "Realizado",
            options: {
                display: true,
                viewColumns: true,
                filter: false,
                customBodyRender: (value, tableMeta, updateValue) => {

                    if(value == null)
                        value = 0;

                    return (
                        <div>
                            {value.toLocaleString('pt-BR', { style: 'currency', currency: 'BRL' }).replace('R$', '')}
                        </div>
                    );
                }
            }
        }
    ]

    options = {
        filter: true,
        filterType: 'dropdown',
        responsive: 'stacked', 
        print: false, 
        download: false,
        sort: true,
        pagination: false,
        selectableRows: 'none',
        textLabels: global.textLabels
    };
    
    state = {
        data : []
    };

    refreshData = () => {
        var $this = this;

        var startDate   = localStorage.getItem('dataInicio') ? new Date( JSON.parse(localStorage.getItem('dataInicio')) ) : this.moment().startOf('month').toDate()
        var endDate     = localStorage.getItem('dataFim') ? new Date ( JSON.parse(localStorage.getItem('dataFim')) ) : this.moment().endOf('month').toDate() 
 
        this.setStartDate(startDate, function(){
            $this.setEndDate(endDate, function(){
                // console.log('RefreshData 1');
                $this.getData();
            });
        });
    }

    getMuiTheme = () => createMuiTheme({
        overrides: {
            MuiTableRow: {
                root: {
                    '&:nth-of-type(odd)': {
                        backgroundColor: "#f8f9fe"
                    },
                },
            },
            MuiTableCell: {
                root: {
                    fontFamily: "unset",
                    borderTop: "none",
                    padding: "16px 10px" 
                }
            },
            MUIDataTableHeadCell: {
                root: {
                    fontWeight: 700
                }
            },
            MuiPaper: {
                root: {
                    overflow: "hidden",
                    margin: 0,

                },
                elevation4: {
                    boxShadow: "none",
                    border: "0.5px solid rgb(228, 234, 244)"
                }
            },
            MUIDataTableToolbar: {
                left: {
                    margin: 10
                },
            },
            MuiToolbar: {
                regular: {
                    marginTop: 16
                }
            }
            }
        })

    render() {
        return (
            <>
                <button id="reduzirGastos" onClick={ () => this.refreshData() } style={{position: "fixed", left: "-1000px"}}/>
                <MuiThemeProvider theme={this.getMuiTheme()}>
                    <MUIDataTable
                        //title={'Reduzir gastos'} 
                        data={this.state.data} 
                        columns={this.columns} 
                        options={this.options}
                    />
                </MuiThemeProvider>
            </>
        );
    }
}
export default ReduzirGastos;