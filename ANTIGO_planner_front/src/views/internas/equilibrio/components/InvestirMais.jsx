import React from "react";
import MUIDataTable from "mui-datatables";
import CurrencyInput from 'react-currency-input';
import axios from 'axios';
import { registerLocale } from "react-datepicker";
import br from "date-fns/locale/pt-BR";
import { createMuiTheme, MuiThemeProvider } from '@material-ui/core/styles';

registerLocale("br", br);

class InvestirMais extends React.Component {

    moment = require('moment');
   
    componentDidMount() 
    {
        var $this = this;

        this.setState({
          accessToken : localStorage.getItem('access-token'),
          familiaId: localStorage.getItem('familia-id')
        }, $this.refreshData());                    
    }

    getData = () => {
        //var url = global.server_api + "api/EquilibrioFinanceiro/familia/"+this.state.familiaId+"/plano";
        var url = global.server_api_new + global.apiToken + "/EquilibrioFinanceiro/familia/"+this.state.familiaId+"/plano";

        var config = {
            headers: {
        'Authorization': "bearer " + this.state.accessToken,
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Headers": "Authorization", 
        "Access-Control-Allow-Methods": "GET, POST, OPTIONS, PUT, PATCH, DELETE" ,
      }
        };
    
        var filtro = {
            DataInicial: this.state.startDate,
            DataFinal: this.state.endDate
        }
    
        axios.post(url, filtro, config).then(res => {
            var data = [];

            var totalMeta = 0;
            var totalAtual = 0;
            var totalParcela = 0;

            if(res.data.results != null){
                res.data.results.forEach((item) => {
                    data.push({
                        id: item.id,
                        planoId: item.planoId,
                        nome: item.nome,
                        ideal: item.ideal,
                        mesAtual: item.mesAtual,
                        ehTotal: 0
                    });
                    totalAtual += item.mesAtual != null ? item.mesAtual : 0;
                    totalMeta += item.ideal != null ? item.ideal : 0;
                });
            }

            //var urlFamilia = global.server_api + 'api/sonho/planos/familia/' + this.state.familiaId + "/emAndamento";
            var urlFamilia = global.server_api_new + global.apiToken + '/sonho/planos/familia/' + this.state.familiaId + "/emAndamento";
            axios.get(urlFamilia, filtro, config).then(res => {
                res.data.results.forEach(first => {
                    data.forEach(second => {
                        if(first.planoId === second.planoId) {
                            second.parcela = first.parcela;
                        }
                    });
                    totalParcela +=  first.parcela;
                });
                //console.log('Hello world!', data);
                data.push({
                    nome: "Total",
                    ideal: totalMeta,
                    mesAtual: totalAtual,
                    parcela: totalParcela,
                    ehTotal: 1
                });
                this.setState({ data });
            });
        });
    }

    setStartDate = (date, callback) => {

        if(date != null){
            date.setHours(0);
            date.setMinutes(0, 0, 0);
        }
    
        const startDate = date;
    
        this.setState({startDate} , function() {

            if(callback != null){
                callback();
            }
            else {
                this.getData();
            }
        });
    }
    
    setEndDate = (date, callback) => {
        if(date != null){
            date.setHours(20);
            date.setMinutes(59, 59, 0);
        }
    
        const endDate = date;
    
        this.setState({endDate} , function() {
            if(callback != null){
                callback();
            }
            else {
                this.getData();
            }
        });
    }

    state = {
        data : []
    };

    columns = [
        {
            name: "ehTotal",
            options: {
                display: false,
                viewColumns: false,
                filter: false
            }
        },
        {
            name: "id",
            options: {
                display: false,
                viewColumns: false,
                filter: false
            }
        },
        {
            name: "planoId",
            options: {
                display: false,
                viewColumns: false,
                filter: false
            }
        },
        {
            name: "nome",
            label: "Nome",
            options: {
                display: true,
                viewColumns: true,
                filter: false
            }
        },
        {
            name: "parcela",
            label: "Estimado",
            options: {
                display: true,
                viewColumns: true,
                filter: false,
                customBodyRender: (value, tableMeta, updateValue) => {

                    if(value == null)
                        value = 0;

                    return (
                        <div>
                            {value.toLocaleString('pt-BR', { style: 'currency', currency: 'BRL' }).replace('R$', '')}
                        </div>
                    );
                }
            }
        },
        {
            name: "ideal",
            label: "Ideal",
            options: {
                display: true,
                viewColumns: true,
                filter: false,
                customBodyRender: (value, tableMeta, updateValue) => {

                    if(value == null)
                        value = 0;

                    if(tableMeta.rowData[0] === 1){
                        return (
                            <div>
                                {value.toLocaleString('pt-BR', { style: 'currency', currency: 'BRL' }).replace('R$', '')}
                            </div>
                        );
                    }

                    return (
                        <div>
                            <CurrencyInput 
                                style={{width: "100px"}}
                                value={value} 
                                thousandSeparator={'.'}
                                precision="2"
                                onChangeEvent={(event, formattedValue, value) => {
                                    updateValue(value);

                                    if(this.state.timeout != null){
                                        clearTimeout(this.state.timeout);
                                    }

                                    var $this = this;

                                    var timeout = setTimeout(function(){
                                        var planoId = tableMeta.rowData[2];
                                        //var service = 'api/EquilibrioFinanceiro/familia/' +$this.state.familiaId+ '/plano/'+planoId;
                                        var service =  `${global.apiToken}/EquilibrioFinanceiro/familia/${$this.state.familiaId}/plano/${planoId}`;
                                        var url = global.server_api_new + service;

                                        var config = {
                                            headers: {'Authorization': "bearer " + $this.state.accessToken}
                                        };
                                        
                                        var dados = {
                                            ideal: value
                                        };

                                        axios.post(url, dados, config).then(res => {
                                            if(res.data.success){
                                                $this.getData();
                                            }
                                        });
                                    },1000);

                                    this.setState({timeout});
                                }}  
                            />
                        </div>
                    );
                }
            }
        },
        {
            name: "mesAtual",
            label: "Realizado",
            options: {
                display: true,
                viewColumns: true,
                filter: false,
                customBodyRender: (value, tableMeta, updateValue) => {

                    if(value == null)
                        value = 0;

                    return (
                        <div>
                            {value.toLocaleString('pt-BR', { style: 'currency', currency: 'BRL' }).replace('R$', '')}
                        </div>
                    );
                }
            }
        },
        {
            name: "diferenca",
            label: "Diferença",
            options: {
                display: true,
                viewColumns: true,
                filter: false,
                customBodyRender: (value, tableMeta, updateValue) => {

                    var atual = tableMeta.rowData[4];
                    var meta = tableMeta.rowData[5];

                    if(atual == null)
                    {
                        atual = 0;
                    }
                    
                    if(meta == null)
                    {
                        meta = 0;
                    }
                    
                    value = atual - meta;

                    return (
                        <div>
                            {value.toLocaleString('pt-BR', { style: 'currency', currency: 'BRL' }).replace('R$', '')}
                        </div>
                    );
                }
            }
        }
    ]
    
    options = {
        filter: true,
        filterType: 'dropdown',
        responsive: 'stacked', 
        print: false, 
        download: false,
        sort: true,
        pagination: false,
        selectableRows: 'none',
        textLabels: global.textLabels
    };

    refreshData = () => {
        var $this = this;

        var startDate   = localStorage.getItem('dataInicio') ? new Date( JSON.parse(localStorage.getItem('dataInicio')) ) : this.moment().startOf('month').toDate()
        var endDate     = localStorage.getItem('dataFim') ? new Date ( JSON.parse(localStorage.getItem('dataFim')) ) : this.moment().endOf('month').toDate() 
 
        this.setStartDate(startDate, function(){
            $this.setEndDate(endDate, function(){
                // console.log('RefreshData 2');
                $this.getData();
            });
        });
    }

    getMuiTheme = () => createMuiTheme({
        overrides: {
            MuiTableRow: {
                root: {
                    '&:nth-of-type(odd)': {
                        backgroundColor: "#f8f9fe"
                    }
                },
            },
            MuiTableCell: {
                root: {
                    fontFamily: "unset",
                    borderTop: "none",
                    padding: "16px 10px" 
                }
            },
            MUIDataTableHeadCell: {
                root: {
                    fontWeight: 700
                }
            },
            MuiPaper: {
                root: {
                    overflow: "hidden",
                    margin: 0,

                },
                elevation4: {
                    boxShadow: "none",
                    border: "0.5px solid rgb(228, 234, 244)"
                }
            },
            MUIDataTableToolbar: {
                left: {
                    margin: 10
                },
            },
            MuiToolbar: {
                regular: {
                    marginTop: 16
                }
            }
            }
        })

    render() {
        return (
            <>
                <button id="investirMais" onClick={ () => this.refreshData() } style={{position: "fixed", left: "-1000px"}}/>
                <MuiThemeProvider theme={this.getMuiTheme()}>
                    <MUIDataTable
                        //title={'Investir mais'} 
                        data={this.state.data} 
                        columns={this.columns} 
                        options={this.options}
                    />
                </MuiThemeProvider>
            </>
        );
    }
}
export default InvestirMais;