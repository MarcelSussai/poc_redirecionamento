import React from "react";
import MUIDataTable from "mui-datatables";
import CurrencyInput from 'react-currency-input';
import axios from 'axios';
import MaskedInput from 'react-maskedinput';
import { registerLocale } from "react-datepicker";
import br from "date-fns/locale/pt-BR";
import { createMuiTheme, MuiThemeProvider } from '@material-ui/core/styles';

registerLocale("br", br);

class Geral extends React.Component {
    state = {
        accessToken : localStorage.getItem('access-token'),
        familiaId: localStorage.getItem("familia-id"),
        data : []
    }
    moment = require('moment');

    componentDidMount() 
    {
        var $this = this;

        this.setState({
            accessToken : localStorage.getItem('access-token'),
            familiaId: localStorage.getItem('familia-id')
          }, $this.refreshData());
        console.log(this.state.familiaId, localStorage.getItem("familia-id"));
    }

    getData = () => {
        //var url = global.server_api + "api/EquilibrioFinanceiro/familia/"+this.state.familiaId+"/tipoOrcamento";
        var url = global.server_api_new + global.apiToken + "/EquilibrioFinanceiro/familia/"+this.state.familiaId+"/tipoOrcamento";

        var config = {
            headers: {
        'Authorization': "bearer " + this.state.accessToken,
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Headers": "Authorization", 
        "Access-Control-Allow-Methods": "GET, POST, OPTIONS, PUT, PATCH, DELETE" ,
      }
        };
    
        var filtro = {
            DataInicial: this.state.startDate,
            DataFinal: this.state.endDate
        }
    
        axios.post(url, filtro, config).then(res => {
            var data = [];

            var totalMeta = 0;
            var totalAtual = 0;

            if(res.data.results != null){
                res.data.results.forEach((item) => {
                    data.push({
                        id: item.id,
                        tipoOrcamento: item.tipoOrcamento,
                        nome: item.nome,
                        mesAtual: item.mesAtual,
                        meta: item.meta,
                        data: (item.mes != null && item.ano != null) ? item.mes + "/" + item.ano : "",
                        ehTotal: 0
                    });

                    if(item.tipoOrcamento === 0){
                        totalAtual += item.mesAtual;
                        totalMeta += item.meta;
                    }
                    else {
                        totalAtual -= item.mesAtual;
                        totalMeta -= item.meta;
                    }
                });

                data.push({
                    nome: "Total",
                    mesAtual: totalAtual,
                    meta: totalMeta,
                    ehTotal: 1
                });
            }

            this.setState({ data });
        });
    }

    setStartDate = (date, callback) => {

        if(date != null){
            date.setHours(0);
            date.setMinutes(0, 0, 0);
        }
    
        const startDate = date;
    
        this.setState({startDate} , function() {

            if(callback != null){
                callback();
            }
            else {
                this.getData();
            }
        });
    }
    
    setEndDate = (date, callback) => {
        if(date != null){
            date.setHours(20);
            date.setMinutes(59, 59, 0);
        }
    
        const endDate = date;
    
        this.setState({endDate} , function() {
            if(callback != null){
                callback();
            }
            else {
                this.getData();
            }
        });
    }

    columns = [
        {
            name: "ehTotal",
            options: {
                display: false,
                viewColumns: false,
                filter: false
            }
        },
        {
            name: "id",
            options: {
                display: false,
                viewColumns: false,
                filter: false
            }
        },
        {
            name: "tipoOrcamento",
            options: {
                display: false,
                viewColumns: false,
                filter: false
            }
        },
        {
            name: "nome",
            label: "Nome",
            options: {
                display: true,
                viewColumns: true,
                filter: false
            }
        },
        {
            name: "mesAtual",
            label: "Realizado",
            options: {
                display: true,
                viewColumns: true,
                filter: false,
                customBodyRender: (value, tableMeta, updateValue) => {
                    
                    if(value == null)
                        value = 0;

                    return (
                        <div>
                            {value.toLocaleString('pt-BR', { style: 'currency', currency: 'BRL' }).replace('R$', '')}
                        </div>
                    );
                }
            }
        },
        {
            name: "meta",
            label: "Meta",
            options: {
                display: true,
                viewColumns: true,
                filter: false,
                customBodyRender: (value, tableMeta, updateValue) => {

                    if(value == null)
                        value = 0;

                    if(tableMeta.rowData[0] === 0){
                        return (
                            <div>
                                <CurrencyInput 
                                    value={value} 
                                    thousandSeparator={'.'}
                                    precision="2"
                                    onChangeEvent={(event, formattedValue, value) => {
                                        updateValue(value);

                                        if(this.state.timeout != null){
                                            clearTimeout(this.state.timeout);
                                        }

                                        var $this = this;

                                        var timeout = setTimeout(function(){
                                            var tipoOrcamento = tableMeta.rowData[2];
                                            //var service = 'api/EquilibrioFinanceiro/familia/' +$this.state.familiaId+ '/tipoOrcamento/'+tipoOrcamento;
                                            var service =  `${global.apiToken}/EquilibrioFinanceiro/familia/${localStorage.getItem('familia-id')}/tipoOrcamento/${tipoOrcamento}`;
                                            var url = global.server_api_new + service;

                                            var config = {
                                                headers: {'Authorization': "bearer " + $this.state.accessToken}
                                            };

                                            var data = null;

                                            if(tableMeta.rowData[6] != null && tableMeta.rowData[6].indexOf('/') === -1 && tableMeta.rowData[6].length === 6){
                                                data = "01" + tableMeta.rowData[6];
                                                data = data.substr(0, 2) + "/" + data.substr(2, 2) + "/" + data.substr(4, 4);
                                            }
                                            else if (tableMeta.rowData[6] != null && tableMeta.rowData[6].indexOf('/') >= 0 && tableMeta.rowData[6].length === 7){
                                                data = "01/" + tableMeta.rowData[6];
                                            }

                                            data = (data != null) ? $this.moment(data, "DD/MM/YYYY").toDate() : null;
                                            
                                            var dados = {
                                                meta: value,
                                                data: data
                                            };

                                            axios.post(url, dados, config).then(res => {
                                                if(res.data.success){
                                                    $this.getData();
                                                }
                                            });
                                        },1000);

                                        this.setState({timeout});
                                    }}  
                                />
                            </div>
                        );
                    }
                    else {
                        return (
                            <>
                                {value.toLocaleString('pt-BR', { style: 'currency', currency: 'BRL' }).replace('R$', '')}
                            </>
                        );
                    }
                }
            }
        },
        {
            name: "data",
            label: "Data da Meta",
            options: {
                display: true,
                viewColumns: true,
                filter: false,
                customBodyRender: (value, tableMeta, updateValue) => {
                    
                    var $this = this;

                    if(value == null)
                        value = "";

                    if(tableMeta.rowData[0] === 1){
                        return (
                            <>
                            </>
                        );
                    }

                    return (
                        <div>
                            <MaskedInput
                                mask={'11/1111'}
                                value={value}
                                onChange={(e) => { 
                                    updateValue(e.target.value);

                                    var novoValor = e.target.value;
                                    var data = null;

                                    // console.log('novadata antes', novoValor);

                                    if(novoValor == null || novoValor.indexOf('_') > -1){
                                        return;
                                    }

                                    if(novoValor != null && novoValor.indexOf('/') === -1 && novoValor.length === 6){
                                        data = "01" + novoValor;
                                        data = data.substr(0, 2) + "/" + data.substr(2, 2) + "/" + data.substr(4, 4);
                                    }
                                    else if (novoValor != null && novoValor.indexOf('/') >= 0 && novoValor.length === 7){
                                        data = "01/" + novoValor;
                                    }

                                   // console.log('novadata depois', data);

                                    if(data != null)
                                    {
                                        if(this.state.timeout != null){
                                            clearTimeout(this.state.timeout);
                                        }

                                        var timeout = setTimeout(function(){
                                            var id = tableMeta.rowData[2];
                                            //var service = 'api/EquilibrioFinanceiro/familia/' +$this.state.familiaId+ '/tipoOrcamento/'+id;
                                            var service =  `${global.apiToken}/EquilibrioFinanceiro/familia/${localStorage.getItem('familia-id')}/tipoOrcamento/${id}`;
                                            
                                            var url = global.server_api_new + service;

                                            var config = {
                                                headers: {'Authorization': "bearer " + $this.state.accessToken}
                                            };

                                            data = (data != null) ? $this.moment(data, "DD/MM/YYYY").toDate() : null;
                                            
                                            var dados = {
                                                meta: tableMeta.rowData[5] != null && tableMeta.rowData[5] !== "" ? tableMeta.rowData[5] : null,
                                                data: data
                                            };

                                            axios.post(url, dados, config).then(res => {
                                                if(res.data.success){
                                                    $this.getData();
                                                }
                                            });
                                        },1000);

                                        this.setState({timeout});
                                    }
                                }}/>
                        </div>
                    );
                }
            }
        }
    ]
    
    options = {
        filter: true,
        filterType: 'dropdown',
        responsive: 'stacked', 
        print: false, 
        download: false,
        sort: true,
        pagination: false,
        selectableRows: 'none',
        textLabels: global.textLabels
    };

    refreshData = () => {
        var $this = this;

        var startDate   = localStorage.getItem('dataInicio') ? new Date( JSON.parse(localStorage.getItem('dataInicio')) ) : this.moment().startOf('month').toDate()
        var endDate     = localStorage.getItem('dataFim') ? new Date ( JSON.parse(localStorage.getItem('dataFim')) ) : this.moment().endOf('month').toDate() 
 
        this.setStartDate(startDate, function(){
            $this.setEndDate(endDate, function(){
                // console.log('RefreshData 3');
                $this.getData();
            });
        });
    }

    getMuiTheme = () => createMuiTheme({
        overrides: {
            MuiTableRow: {
                root: {
                    '&:nth-of-type(odd)': {
                        backgroundColor: "#f8f9fe"
                    },
                    '& input': {
                        background: "transparent"
                    }
                },
            },
            MuiTableCell: {
                root: {
                    fontFamily: "unset",
                    borderTop: "none",
                    padding: "16px 10px" 
                }
            },
            MUIDataTableHeadCell: {
                root: {
                    fontWeight: 700
                }
            },
            MuiPaper: {
                root: {
                    overflow: "hidden",
                    margin: 0,

                },
                elevation4: {
                    boxShadow: "none",
                    border: "0.5px solid rgb(228, 234, 244)"
                }
            },
            MUIDataTableToolbar: {
                left: {
                    margin: 10
                },
            },
            MuiToolbar: {
                regular: {
                    marginTop: 16
                }
            }
            }
        })

    render() {
        return (
            <>
                <button id="geral" onClick={ () => this.refreshData() } style={{position: "fixed", left: "-1000px"}}/>
                <MuiThemeProvider theme={this.getMuiTheme()}>
                    <MUIDataTable
                        //title={'Geral'} 
                        data={this.state.data} 
                        columns={this.columns} 
                        options={this.options}
                    />
                </MuiThemeProvider>
            </>
        );
    }
}

export default Geral;
