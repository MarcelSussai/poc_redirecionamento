import React from "react";
import { Redirect } from "react-router-dom";
import { Row } from "reactstrap";

class TermosUso extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      accessToken: localStorage.getItem("access-token"),
      empresaId: localStorage.getItem("empresa-id"),
      planejadorId: localStorage.getItem("planejador-id"),
      familiaId: localStorage.getItem("familia-id"),
      tipoUsuario: parseInt(localStorage.getItem("tipo-usuario")),

      selectedVideo: "cadastro",
    };
  }

  componentDidMount() {
    if (this.state.accessToken == null) {
      this.setRedirect();
      return;
    }
  }

  setRedirect = () => {
    this.setState({
      redirect: true,
    });
  };

  renderRedirect = () => {
    if (this.state.redirect) {
      return <Redirect to="/auth/login" />;
    }
  };

  render() {
    return (
      <div id="leads" style={{ padding: "0 5%", marginTop: 20 }}>
        <Row>
          <div className="modal_pesquisa">
            <h2 className="privacy_title text-center">Termos de Uso</h2>
            <p className=" pt-3 text-justify">
              A plataforma digital, por aplicativo (App) ou site (SaaS) são
              serviços ofertados por VISTA® SISTEMAS DE SOLUCOES EM TECNOLOGIA
              FINANCEIRA LTDA, sociedade empresária limitada com sede na Cidade
              de Brasília, Distrito Federal, Q SHIN QL 9 CONJUNTO 3, CASA, n° 6,
              CEP 71.515-235, devidamente inscrita no CNPJ sob o número
              36.312.248/0001-35.
            </p>
            <br />

            <h2 className="privacy_title">CONSIDERANDO QUE:</h2>

            <ul>
              <li>
                A VISTA® é empresa que tem por objeto SISTEMAS DE SOLUÇÕES EM
                TECNOLOGIA FINANCEIRA;
              </li>
            </ul>

            <ul>
              <li>
                A utilização da plataforma busca auxiliar ao usuário em relação
                às finanças pessoais, utilizando metodologia para isso;
              </li>
            </ul>

            <ul>
              <li>
                A VISTA® não se responsabiliza pelos resultados da utilização da
                plataforma, considerando que não possui gerência na condução dos
                recursos disponibilizados, sendo uma plataforma de meio e não de
                fim, não se responsabilizando pela utilização e condução da
                gestão entre USUÁRIO e seus CLIENTES;
              </li>
            </ul>
            <br />

            <p className="text-justify">
              <strong>DEFINIÇÕES DESTE TERMO</strong>
            </p>
            <br />

            <ul>
              <li>
                <strong>USUÁRIO: </strong>PLANEJADOR FINANCEIRO INDEPENDENTE,
                pessoa física, com capacidade legal para contratar, o qual não
                possui vínculo ou relação de subordinação com a plataforma
                VISTA®, sendo esta uma mera ferramenta a ser utilizada pelo
                USUÁRIO, realizando o seu cadastro e de seus clientes por meio
                do cadastro disponibilizado para utilizar as funcionalidades da
                plataforma, aceitando o presente Termos e Condições de Uso, bem
                como a Política de Privavidade de forma expressa;
              </li>
            </ul>

            <ul>
              <li>
                <strong>CLIENTE: </strong>pessoa física cadastrada pelo USUÁRIO,
                a qual possui relação direta com este, não havendo qualquer tipo
                de relação com a plataforma VISTA®;
              </li>
            </ul>

            <ul>
              <li>
                <strong>POLÍTICA DE PRIVACIDADE: </strong>política de
                privacidade que rege as disposições sobre a utilização dos dados
                do Usuário pelo VISTA®, disponível no link
                https://meuvista.com/politica-de-privacidade/
              </li>
            </ul>

            <ul>
              <li>
                <strong>GATEWAY DE PAGAMENTO: </strong>O gateway de pagamento é
                uma interface que viabiliza a transmissão de dados entre os
                clientes, comerciantes e os bancos durante os pagamentos da loja
                virtual. Em outras palavras, podemos descrevê-lo como uma parte
                integrante da estrutura do e-commerce que o permite realizar
                transações financeiras;
              </li>
            </ul>

            <ul>
              <li>
                <strong>“Aplicativo” ou “App”: </strong>significa o aplicativo
                adaptado e desenvolvido para operação em telefone celular,
                tablet ou qualquer outro dispositivo móvel, mas com ferramentas
                não idênticas e opções de uso limitadas em relação à Plataforma
                Web;
              </li>
            </ul>

            <ul>
              <li>
                <strong>PLATAFORMA WEB SaaS: </strong>O SaaS (Software como
                Serviço) permite aos usuários se conectar e usar aplicativos
                baseados em nuvem pela Internet – plataforma e-commerce. Com
                esses serviços, você se conecta à sua conta pela Internet,
                normalmente por um navegador da Web;
              </li>
            </ul>

            <ul>
              <li>
                <strong>ÓRGÃOS REGULADORES: </strong>Agência reguladora é uma
                pessoa jurídica de direito público interno, geralmente
                constituída sob a forma de autarquia especial ou outro ente da
                administração indireta, cuja finalidade é regular e/ou
                fiscalizar a atividade de determinado setor da economia de um
                país;
              </li>
            </ul>

            <p className="text-justify">
              O usuário deve proceder com a leitura atenta e integralmente o
              nosso CONTRATO DE PLANO PRIVADO DE UTILIZAÇÃO DE PLATAFORMA
              DIGITAL, ou simplesmente, TERMOS E CONDIÇÕES DE USO, além de
              demais documentos que compõem a utilização da plataforma, como
              Política de Privacidade, Política de Segurança, Política Comercial
              e outros que se fizerem necessários, a fim de se familiarizar com
              o VISTA® antes de iniciar sua efetiva utilização e, caso esteja de
              acordo, manifeste-se nesse sentido quando do seu cadastro na
              Plataforma, por meio do botão disponibilizado para tanto. Ao fazer
              isso, o usuário demonstra sua aceitação livre, inequívoca,
              expressa e informada, desprovida de quaisquer reservas, acerca de
              todas as disposições dos nossos Termos, bem como declara ser maior
              de idade e estar apto para utilizar o VISTA®. Contudo, caso o
              usuário não esteja de acordo com o conteúdo abaixo apresentado,
              deverá se abster de utilizar a VISTA®
            </p>

            <p className="text-justify">
              Usuário poderá reler e verificar estes Termos a qualquer momento,
              tendo em vista que estes estão disponíveis no VISTA® no link{" "}
              <a href="https://meuvista.com/termos-de-uso/"></a>
            </p>

            <p className="text-justify">
              Estes Termos estão sujeitos a mudanças, podendo ser alterados a
              qualquer tempo, quando não houver vedação legal nesse sentido.
              Desse modo, recomendamos que o usuário acesse estes Termos
              periodicamente e certifique-se de ter verificado sua versão mais
              atualizada, com base na data indicada ao término do documento.
            </p>
            <br />

            <h2 className="privacy_title">1. NOSSOS SERVIÇOS</h2>
            <br />

            <p className="text-justify">
              <strong>Plataforma:</strong> Usuário poderá acessar gratuitamente
              Plataforma através de smartphones, tablets, e computadores após o
              download do aplicativo na App Store e Google Play, ou por meio do
              website www.meuvista.com.br. Por meio deste Aplicativo,
              disponibilizaremos meios para que o usuário possa preencher e
              imprimir seu documento oficial de forma, rápida, segura e sem
              erros, evitando rasuras e a necessidade de solicitar uma segunda
              via.
            </p>

            <p className="text-justify">
              <strong>Cadastro:</strong> Para que nós possamos oferecer o melhor
              de nossas funcionalidades, software e website (“Serviços”), o
              usuário terá que se cadastrar no VISTA®, fornecendo-nos
              informações precisas de identificação pessoal e/ou da empresa,
              conforme indicado nos campos exibidos na Plataforma, definindo,
              ainda, suas credenciais de acesso. A partir do cadastro, o usuário
              nos autoriza a encaminhar um e-mail para confirmação de cadastro,
              com orientações para a efetiva ativação de sua conta. Seguindo
              nossas orientações, o usuário poderá utilizar o VISTA® e usufruir
              de nossos Serviços.
            </p>

            <p className="text-justify">
              Caso algum dado utilizado para cadastro no VISTA® venha a sofrer
              alguma alteração, deve o usuário utilizar ferramenta de
              atualização para que seus dados informados estejam sempre
              atualizados.
            </p>

            <p className="text-justify">
              <strong>Tipos de Planos</strong>. Ao usuário é facultado escolher
              dentre os planos ofertados pelo VISTA®, devendo observar as
              funcionalidades disponibilizadas em cada um deles, estando ao
              exclusivo critério do usuário aquele que pretende utilizar.
            </p>

            <p className="text-justify">
              <strong>
                Processo de Preenchimento de Clientes dentro da Plataforma
              </strong>
              . O usuário é livre para preencher quantos cadastros de clientes
              que desejar, no qual cada cadastro representa uma contratação,
              razão pela qual cada um será cobrado de forma individual no valor
              do plano escolhido, na sua forma pro rata die.
            </p>

            <p className="text-justify">
              <strong>Processo de Gestão Financeira</strong>.O VISTA® não
              participa, não se responsabiliza e nem gerencia o processo de
              gestão realizado pelo usuário frente ao cliente cadastrado por
              este, sendo responsável pela veracidade informações cadastrais ali
              lançadas, assim como o uso e gerenciamento da plataforma frente
              aos seus clientes, responsabilizando-se, inclusive, pelas
              informações de pagamentos e pelos próprios valores que devem ser
              efetivados mensalmente. Ainda, o VISTA® não se responsabiliza
              pelos equipamentos utilizados pelo usuário e seus clientes,
              incluindo o sistema operacional e navegadores de internet e suas
              versões, ferramentas de controle de acesso, bem como demais
              equipamentos utilizados pelo usuário e clientes deste para
              realizar o acesso e uso da plataforma. Desta forma, dificuldades
              de acessos realizadas de maneira indevida, que gerem lentidão,
              suspensão e/ou interrupção dos serviços ou não conformidades com a
              plataforma são de responsabilidade única do usuário.
            </p>
            <br />

            <h2 className="privacy_title">
              2. REGRAS DE UTILIZAÇÃO DE NOSSOS SERVIÇOS
            </h2>
            <br />

            <p className="text-justify">
              <strong>Responsabilidade</strong>.Para utilização dos nossos
              Serviços, o usuário declara (i) ser considerado civilmente a
              aceitar estes Termos e utilizar o VISTA®; (ii) ser o único e
              exclusivo responsável por todas as informações fornecidas ao
              VISTA® e pelo uso deste, inclusive perante terceiros, estando
              sujeito à responsabilização em âmbito civil, penal e
              administrativo em razão de eventual fornecimento de informações
              inverídicas, incompletas e/ou imprecisas, eximindo o Provedor de
              quaisquer responsabilidades decorrentes de eventual dano ou
              prejuízo experimentado por usuário ou por terceiro.
            </p>

            <p className="text-justify">
              <strong>Licitude e Aceitabilidade</strong>. Os Serviços referentes
              ao VISTA® devem ser utilizados única e exclusivamente para fins
              lícitos e aceitáveis. Assim, o usuário não utilizará e não
              concorrerá para a utilização desta Plataforma de modo a: (i)
              inserir no VISTA® qualquer conteúdo ilícito, obsceno, difamatório,
              ameaçador, ofensivo, relacionado à questões raciais, sexuais e/ou
              étnicas, capaz de encorajar e incentivar condutas ilícitas,
              inadequadas e/ou violentas, ou de qualquer forma contrário à moral
              e aos bons costumes; (ii) violar direitos do VISTA® e demais
              usuários e/ou de terceiros; (iii) incorporar, ou permitir que seja
              incorporado, vírus ou outros elementos nocivos na Plataforma,
              capazes de prejudicar os sistemas do VISTA®, dos demais usuários
              ou de terceiros; (iv) permitir a cópia, alteração, modificação,
              elaboração de trabalhos derivados, distribuição, licenciamento,
              transferência ou qualquer forma de exploração destes Serviços,
              além do implemento de engenharia reversa ou extração de códigos do
              VISTA®; (v) comprometer a integridade ou o desempenho do VISTA®;
              (vi) criar e manter contas na VISTA® utilizando-se de meios
              automatizados e/ou não autorizados pelo Provedor; e (vii) coletar
              dados sobre os usuários para utilização diversa daquela à que se
              dispõe a Plataforma.
            </p>

            <p className="text-justify">
              <strong>Meios de Acesso e Utilização</strong>.O acesso ao VISTA®
              deve ocorrer a partir de dispositivos de sua propriedade, providos
              de software compatível e acesso à internet, cuja responsabilidade
              pela disponibilização e manutenção é exclusivamente sua. O correto
              funcionamento do VISTA® depende de conexão estável à internet,
              sendo que todos os custos e tarifas decorrentes de sua utilização
              são de responsabilidade integral do usuário.
            </p>

            <p className="text-justify">
              <strong>Credenciais de Acesso</strong>.As credenciais de acesso
              definidas pelo usuário quando do seu cadastro no VISTA® são de uso
              exclusivo deste e não devem, em nenhuma hipótese, ser
              compartilhadas com terceiros. Portanto, ao aceitar estes Termos, o
              usuário se declara ciente de que é o único responsável pela
              manutenção do sigilo de suas credenciais e por toda e qualquer
              utilização destas, bem como de sua conta na Plataforma, não
              possuindo o Provedor qualquer responsabilidade por eventuais danos
              e/ou prejuízos causados por terceiros em posse de tais
              credenciais.
            </p>

            <p className="text-justify">
              Caso o usuário constate o uso e/ou acesso não autorizado de suas
              credenciais, o usuário deverá imediatamente notificar o Provedor
              para que sejam adotadas as medidas cabíveis, oportunidade na qual
              este poderá, a seu exclusivo critério, suspender ou cancelar a sua
              conta.
            </p>

            <p className="text-justify">
              O usuário somente terá ao acesso da plataforma de acordo com o
              tipo de PLANO escolhido por ele próprio, conforme opções descritas
              na Política Comercial do VISTA® e termos do item 3 deste TERMO.
            </p>

            <p className="text-justify">
              O cadastro preliminar consiste no preenchimento de todas as
              informações pessoais, entrega de documentos exigidos pela Área de
              Compliance da VISTA, e a anuência expressa ao contrato de adesão
              aos serviços prestados pela VISTA
            </p>

            <p className="text-justify">
              <strong>Instruções de Utilização </strong>.As instruções de
              utilização do VISTA® estão disponíveis através do link de Ajuda
              disponível na Plataforma, bem como suporte aos usuários em horário
              comercial (das 9hs às 12hs e das 14hs às 18hs), e por meio do
              e-mail (suporte@meuvista.com). De modo a garantir a melhor
              experiência para o usuário, nossa reposta deverá demorar no máximo
              48 (quarenta e oito) horas.
            </p>
            <br />

            <h2 className="privacy_title">
              3. PAGAMENTO, CANCELAMENTO E RENOVAÇÃO
            </h2>
            <br />

            <p className="text-justify">
              <strong>Política Comercial </strong>.A política comercial são as
              condições de utilização da plataforma ofertadas ao tempo da
              contratação, a qual contém as informações de relativas ao preço,
              número de usuários, espaço em disco, dentre outras, que podem
              variar de acordo com as necessidades do usuário. Ao concordar com
              os presentes Termos, o usuário também concorda e aceita a Política
              comercial do VISTA®.
            </p>

            <p className="text-justify">
              É importante que usuário saiba que a política comercial poderá ser
              alterada. Quando isso acontecer o usuário será avisado através da
              Plataforma ou por qualquer canal de comunicação disponível, com 30
              (trinta) dias de antecedência.
            </p>

            <p className="text-justify">
              <strong>Faturamento</strong>.O usuário/cliente pagará para a
              utilização da plataforma VISTA® os valores ajustados de acordo com
              o tipo de PLANO ora contratado, conforme abaixo indicado:
            </p>
            <br />

            <ul>
              <li>
                PLANO VISTA® INDIVIDUAL + PAGAMENTO – Hipótese na qual o usuário
                terá: acesso completo a plataforma VISTA®; consolidação e
                atualização automática de investimentos; aplicativo para
                clientes; sistema de gerenciamento de clientes. integração com
                gateway de pagamentos para cobrar dos seus clientes; cobrança
                adicional por cobrança (valor opcional e pagos para a
                operadora).Valores disponíveis na própria plataforma, sujeita à
                alterações sem aviso prévio;
              </li>
            </ul>

            <ul>
              <li>
                PLANO VISTA® FAMÍLIA + PAGAMENTO – Hipótese na qual o
                INTERMEDIADOR terá: todos os itens do plano VISTA® PLANEJAMENTO;
                integração com gateway de pagamentos para cobrar dos seus
                clientes; cobrança adicional por cobrança (valor opcional e
                pagos para a operadora). Valores disponíveis na própria
                plataforma, sujeita à alterações sem aviso prévio.; Terá direito
                a 2 carteiras de investimentos (para utilização com 2 usuários).
              </li>
            </ul>

            <ul>
              <li>
                PLANO VISTA® BUSINESS – Hipótese na qual o usuário/cliente terá:
                acesso completo a plataforma; consolidação e atualização
                automática de investimentos; aplicativo para clientes; sistema
                de gerenciamento de clientes; sistema de cobrança de clientes;
                gestão de usuários e planejadores. Valores negociados caso a
                caso, situação a qual anexos/aditivos com essa informação será
                parte integrante deste instrumento.
              </li>
            </ul>
            <br />

            <p className="text-justify">
              Os pagamentos poderão ser realizados por meio de plataformas
              parceiras sugeridas, não sendo obrigatória a contratação dessas
              pelo usuário e seus clientes.
            </p>

            <p className="text-justify">
              A cobrança ocorrerá todo dia 1o de cada mês subsequente à
              contratação, calculada pro-rata die, de acordo com a data do
              cadastro do realizado pelo usuário e de seu cliente.
            </p>

            <p className="text-justify">
              O pagamento também se dará pro-rata die quando do cancelamento da
              utilização da plataforma, sendo apurada no mês subsequente à
              rescisão, conforme cláusula 11 deste TERMO.
            </p>

            <p className="text-justify">
              A cobrança será realizada mediante cartão de crédito devidamente
              informado quando do cadastro na plataforma, conforme
              responsabilização e veracidade já dispostas neste TERMOS.
            </p>

            <p className="text-justify">
              Entende-se como pro-rata die o pagamento proporcional ao dia de
              utilização, e para efeito de cálculo de apuração de pagamento,
              será contabilizado sobre 30 dias/mês, não obstante meses com
              números de dias distintos.
            </p>

            <p className="text-justify">
              <strong>Inadimplência</strong> O provedor se reserva o direito de
              tomar as medidas judiciais e extrajudiciais pertinentes para o
              recebimento dos valores devidos pelos usuários, inclusive de levar
              à protesto o título ou documento que comprove a dívida existente.
            </p>

            <p className="text-justify">
              <strong>Atraso no pagamento</strong> Havendo atraso no pagamento
              superior a 5 (cinco) dias, será bloqueado o acesso do Usuário na
              Plataforma e serão suspensos quaisquer serviços prestados pelo
              Provedor. Sendo o atraso superior a 30 (trinta) dias, poderão ser
              excluídos permanentemente todos os dados e informações dos
              Usuários.
            </p>

            <p className="text-justify">
              <strong>Cancelamento: </strong>Os Usuários poderão fazer o
              cancelamento dos serviços contratados diretamente pela Plataforma
              ou entrando em contato com o VISTA®, mediante requerimento nesse
              sentido. Os serviços oferecidos pelo VISTA® são pós-pagos e, em
              nenhuma hipótese, haverá devolução dos valores pagos anteriormente
              em virtude do cancelamento.
            </p>
            <br />

            <h2 className="privacy_title">4. PROPRIEDADE INTELECTUAL</h2>
            <br />

            <p className="text-justify">
              <strong>Direitos do VISTA®:</strong> todos os textos, imagens,
              fotografias, desenhos, ícones, tecnologias, links e demais
              conteúdos audiovisuais ou sonoros, incluindo o software da
              Plataforma, designs, frames e códigos-fonte associados aos nossos
              Serviços são de propriedade exclusiva do VISTA® ou de terceiros
              que a ele permitiram o uso para fins de funcionamento,
              operacionalização e/ou divulgação da plataforma. Ainda, todas as
              marcas, patentes, domínios, nomes comerciais ou logotipos de
              qualquer espécie disponibilizados na Plataforma são de propriedade
              do VISTA® ou de terceiros que autorizaram sua utilização, sem que
              o uso da Plataforma signifique qualquer forma de permissão para
              que o usuário possa citar as tais marcas, nomes comerciais e
              logotipos.
            </p>

            <p className="text-justify">
              <strong>Licença Concedida ao Usuário.</strong> Por meio da
              utilização da Plataforma, é concedida ao usuário uma licença (i)
              pessoal, (ii) não transferível (iii) não exclusiva, (iv) limitada,
              (v) não comercial e (vi) revogável, para uso da Plataforma em seu
              smartphone, tablete e/ou computador em conformidade com as
              condições previstas nestes Termos, que não pode ser sublicenciada
              ou transferida.
            </p>

            <p className="text-justify">
              O VISTA® reserva-se todos os direitos à Plataforma que não estejam
              expressamente concedidos aqui.
            </p>
            <br />

            <h2 className="privacy_title">5. RESPONSABILIDADES DO VISTA®</h2>
            <br />

            <p className="text-justify">
              <strong>Plataforma.</strong> O VISTA®, apesar de se preocupar com
              a segurança e estabilidade de sua plataforma, não se
              responsabiliza por quaisquer indisponibilidades, erros ou falhas
              eventualmente apresentadas bem como por qualquer defraudação da
              utilidade que o usuário possa ter atribuído a este, pela
              falibilidade do mesmo, nem por qualquer dificuldade de acesso.
            </p>

            <p className="text-justify">
              <strong>Internet.</strong> Não possui o VISTA® quaisquer
              responsabilidades por eventuais erros ou inconsistências na
              transmissão de dados, bem como pela qualidade ou disponibilidade
              da conexão de Internet.
            </p>

            <p className="text-justify">
              <strong>Conteúdo.</strong> O VISTA® não poderá ser
              responsabilizado por quaisquer materiais ou conteúdos inseridos na
              Plataforma pelos usuários, bem como por informações
              desatualizadas, incompletas ou inverídicas que venham a ser
              fornecidas por meio do VISTA® sendo tal conteúdo considerado, a
              luz da legislação aplicável, como “conteúdo de terceiros”.
            </p>

            <p className="text-justify">
              <strong>Uso do VISTA®.</strong> O VISTA® não se responsabiliza
              pela utilização da Plataforma por seus usuários, vez que,
              tão-somente, promove o fornecimento de plataforma de gestão
              financeira para organização de gastos e que facilitem a relação do
              usuário com seu dinheiro. Nesse sentido, o VISTA® não se
              responsabiliza por qualquer falha nas informações e resultados
              advindos da utilização da plataforma pelo usuário, bem como não
              garante a confiabilidade dos dados e das informações inseridas
              pelos usuários na Plataforma. Portanto, a utilização do VISTA®
              ocorre por conta e risco dos usuários, não mantendo qualquer
              controle sobre os usuários e seus atos, nem qualquer vínculo com
              eventuais Órgãos e/ou Entidades eventualmente cadastradas na
              Plataforma.
            </p>

            <p className="text-justify">
              <strong>Elementos nocivos.</strong> Pela própria natureza e
              arquitetura da internet, qualquer uso de funcionalidades (sites,
              aplicativos, portais, serviços etc.) está sujeita a riscos
              cibernéticos. O VISTA® adota medidas para tentar evitar que
              pessoas maliciosas cometam algum tipo de ato ilícito ou moralmente
              reprovável em prejuízo de seus usuários, mas não consegue garantir
              que a Plataforma estará livre de atos de terceiros
              mal-intencionados. Portanto, o VISTA® não se responsabiliza pela
              existência de vírus ou outros elementos nocivos na plataforma,
              capazes de ensejar alterações nos seus sistemas informáticos
              (software e hardware) ou nos documentos eletrônicos neles
              armazenados, isentando-se de qualquer responsabilidade pelos danos
              e prejuízos eventualmente causados por tais elemento. O usuário
              não poderá empregar softwares, técnicas e/ou artifícios com o
              intuito de utilizar indevidamente o Site, o Aplicativo e/ou o
              Software para práticas nocivas ao VISTA® ou a terceiros, tais como
              exploits, spamming, flooding, spoofing, crashing, root kits, etc.;
            </p>

            <p className="text-justify">
              <strong>Terceiros.</strong> Não possui o VISTA® quaisquer
              responsabilidades pelos danos e prejuízos de toda ordem que possam
              decorrer do conhecimento que terceiros não autorizados possam ter
              de quaisquer das informações contidas na plataforma, em
              decorrência de falha exclusivamente atribuível ao usuário ou a
              terceiros que fujam a qualquer controle razoável.
            </p>

            <p className="text-justify">
              <strong>Alterações nas funcionalidades.</strong> O VISTA® poderá,
              a seu exclusivo critério, implementar novas funcionalidades aos
              Serviços prestados por meio da plataforma, bem como descontinuar
              aqueles que entender pertinente, sem necessidade de prévio aviso
              ao usuário, contudo podendo apresentar termos e condições de uso
              adicionais, caso entenda pertinente, não sendo devida qualquer
              forma de indenização ao usuário em razão de tais modificações.
            </p>
            <br />

            <h2 className="privacy_title">
              6. FUNCIONALIDADES DO SITE/APLICATIVO
            </h2>
            <br />

            <p className="text-justify">
              O VISTA® oferece diversas funcionalidades ao usuário como, por
              exemplo, Cadastro de usuários, lançamento de despesas, receitas,
              investimentos e dívidas, organização do orçamento pessoal com
              diversas visões e gráficos para auxílio, organização de patrimônio
              financeiro e imobilizado, visualização de extrato e histórico de
              lançamentos.
            </p>

            <p className="text-justify">
              Tais funcionalidades podem vir a sofrer alterações, a qualquer
              momento, sem aviso prévio ao usuário. Da mesma forma, o VISTA®
              poderá incluir novas funcionalidades ou desativar as já existentes
              a seu critério.
            </p>
            <br />

            <h2 className="privacy_title">7. DISPOSIÇÕES GERAIS</h2>
            <br />

            <ul>
              <li>
                Salvo disposição em contrário, este documento representa o total
                entendimento entre usuário e o provedor, bem como revoga
                qualquer outro que por ventura tenha sido firmado anteriormente.
              </li>
            </ul>

            <ul>
              <li>
                Os casos fortuitos ou de força maior serão excludentes de
                responsabilidades das partes, na forma da Legislação Brasileira.
              </li>
            </ul>

            <ul>
              <li>
                Estes Termos poderão ser alterados a qualquer momento, sendo que
                a nova versão deste documento entrará em vigor no dia seguinte
                da publicação no Portal. O usuário somente será comunicado da
                alteração dos Termos se houver obrigação legal em tal sentido.
              </li>
            </ul>

            <ul>
              <li>
                O usuário não poderá transferir os direitos ou obrigações de
                acordo com estes termos para outra pessoa sem consentimento
                prévio por escrito do Provedor.
              </li>
            </ul>

            <ul>
              <li>
                Caso qualquer disposição, obrigação ou restrição destes Termos
                seja, por juízo competente ou outra autoridade, considerada
                inválida, nula ou inexequível, todos os demais termos,
                disposições, obrigações e restrições deverão permanecer em pleno
                vigor e efeito, não podendo ser de forma nenhuma afetados,
                prejudicados ou invalidados.
              </li>
            </ul>

            <ul>
              <li>
                A aceitação por qualquer uma das partes com relação a qualquer
                violação dos presentes Termos ou sua omissão no exercício de
                qualquer direito outorgado pelos mesmos, não será considerado
                como novação ou renúncia em relação a qualquer violação futura,
                seja semelhante ou não, ou ao exercício por qualquer uma das
                partes de qualquer direito futuro conferido por estes Termos.
              </li>
            </ul>

            <ul>
              <li>
                Seus comentários ou outras sugestões sobre o VISTA® e nossos
                Serviços são sempre bem-vindos, mas entenda que podemos usar os
                seus comentários ou sugestões sem qualquer obrigação de
                remunerá-lo por eles (assim como usuário não tem obrigação de
                fazê-los). Quaisquer dúvidas, sugestões ou reclamações a
                respeito dos presentes Termos e Condições de Uso poderão ser
                dirigidas ao e-mail (suporte@meuvista.com).
              </li>
            </ul>

            <ul>
              <li>
                Fica estabelecido que qualquer evento que afete ou envolva o
                usuário e seus clientes, e que possa prejudicar o cumprimento
                das obrigações assumidas por essa parte, deverá ser
                imediatamente comunicada, por escrito, ao VISTA®.
              </li>
            </ul>
            <br />

            <h2 className="privacy_title">
              8. DIREITOS E OBRIGAÇÕES DO USUÁRIO
            </h2>
            <br />

            <ul>
              <li>
                Após cadastrar-se na plataforma, o usuário estará habilitado
                para proceder com o cadastro de seus próprios clientes, devendo
                informar a forma de pagamento descrita nestes termos,
                responsabilizando-se solidariamente pelo mesmo, inclusive,
                prestar ao seu próprio cliente, espontaneamente, todas as
                informações sobre as características e riscos do negócio
                desenvolvido pelo VISTA®;
              </li>
            </ul>

            <ul>
              <li>
                O usuário se compromete e deverá ser devidamente capacitado para
                exercer a atividade, não violando regras e regulamentações da
                CVM (notadamente Instrução 593), ANBIMA e demais órgãos
                reguladores;
              </li>
            </ul>

            <ul>
              <li>
                O usuário exercerá sua função com autonomia em relação à VISTA,
                declarando que se encontra devidamente habilitado para tal
                exercício, reconhecendo que a sua forma ilegal implica na
                apuração das tipificações prescritas em lei;
              </li>
            </ul>

            <ul>
              <li>
                O usuário declara e garante possuir recursos e meios próprios,
                incluindo estruturas administrativa, jurídica e operacional,
                para utilizar a plataforma VISTA®;
              </li>
            </ul>

            <ul>
              <li>
                O usuário se responsabiliza integralmente pela utilização dos
                serviços da plataforma e de todas suas funcionalidades,
                incluindo sua própria responsabilização por quaisquer danos
                diretos ou indiretos no caso de utilização do seu login por
                terceiros, com ou sem a sua ciência. Cada login e senha
                representa um cadastro único, e quando devidamente cadastrado, o
                usuário poderá utilizar as ferramentas e dispositivos postos à
                sua disposição, inclusive cadastrar dentro da plataforma seus
                próprios clientes para utilização dos serviços, advertindo-os
                deste TERMO e demais Políticas do VISTA®, reconhecendo de forma
                expressa que as informações prestadas no cadastro são fidedignas
                e isentas de erro, dolo e/ou coação, confirmando que o cadastro
                mencionado representa meio idôneo e válido de comprovação de
                autoria e autenticidade dentro da plataforma VISTA®, sendo que
                este não responde pela autenticidade, validade e precisão dos
                dados fornecidos pelos Usuários e/ou coletadas na plataforma;
              </li>
            </ul>

            <ul>
              <li>
                Manter o VISTA® devidamente informada a respeito das informações
                dos clientes apresentados. Ainda, o VISTA® não será
                responsabilizado de nenhuma forma, mesmo que de maneira
                subsidiária, por quaisquer desacordos que ocorram entre o
                usuário e seus clientes e, eventualmente seja demandado
                judicialmente por tais razões, o usuário se comprometerá, desde
                já, a solicitar a exclusão do VISTA® do polo passivo da ação,
                bem como arcar com todos os custos necessários para exclusão
                definitiva, inclusive honorários advocatícios de profissional de
                confiança do o VISTA® para defesa dos interesses deste, até o
                termo do processo, seja com a decisão que excluiu sua
                legitimidade passiva, ou com o consequente trânsito em julgado;
              </li>
            </ul>

            <ul>
              <li>
                Responder isolada e integralmente pelas obrigações derivadas de
                sua atividade em favor do VISTA® descritas nestes Termos,
                inclusive pela contratação de terceiros colaboradores;
              </li>
            </ul>

            <ul>
              <li>
                Deixar claro aos clientes que não possui qualquer relação
                jurídica com o VISTA®, além do presente Termo. Ainda, estando
                claro que o VISTA® não possui qualquer relação com os clientes
                do usuário ou com a sua atividade comercial, este deverá cumprir
                todas as disposições legais que envolvam a sua atividade, bem
                como todos os prazos de fornecimento e garantia dos produtos ou
                serviços ofertados;
              </li>
            </ul>

            <ul>
              <li>
                O VISTA® não compartilha dado pessoal ou financeiro do usuário
                com terceiros sem o devido consentimento, afirmando que mantém a
                transparência sobre como tais dados são utilizados. Ainda,
                reconhece a segurança da coleta, uso, armazenamento e tratamento
                dos dados;
              </li>
            </ul>

            <ul>
              <li>
                Ao usuário, seus clientes e terceiros que utilizem a plataforma,
                cumpre informar que o VISTA® não se responsabiliza por eventuais
                serviços ou produtos oferecidos por Parceiros ou quaisquer
                terceiros, inclusive no que diz respeito a sua disponibilidade,
                qualidade, quantidade, características essenciais, ofertas,
                preços, validade da oferta, formas de pagamento ou quaisquer
                outros elementos a eles referentes. Ainda, que não responde por
                eventuais prejuízos sofridos pelos Usuários em razão da tomada
                de decisões com base nas informações disponibilizadas na
                plataforma (Site/Aplicativo);
              </li>
            </ul>

            <ul>
              <li>
                Em nenhum caso, o VISTA® será responsável por danos diretos ou
                indiretos sofridos pelo usuário, seus clientes e eventuais
                terceiros, incluindo, sem limitação, prejuízos por perda de
                lucro, falha de transmissão ou recepção de dados, não
                continuidade do negócio ou qualquer outra perda comercial,
                decorrentes ou relacionados ao uso do Aplicativo, Site, Software
                ou Conteúdo pelo Usuário.
              </li>
            </ul>
            <br />

            <h2 className="privacy_title">9. DO SIGILO</h2>
            <br />

            <ul>
              <li>
                O usuário/cliente se obriga a manter sigilo sobre todas e
                quaisquer informações transmitidas pelo VISTA® em função deste
                TERMO, não podendo revelar a terceiros, e, no caso de seus
                empregados e prepostos, compromete-se a revelar tão somente os
                dados estritamente essenciais ao cumprimento deste.
              </li>
            </ul>

            <ul>
              <li>
                O usuário/cliente obriga-se a manter como confidenciais todas as
                informações recebidas da VISTA® ou demais empresas componentes
                de seu grupo econômico, ficando expressamente proibida a
                divulgação ou disponibilização das informações a terceiros sem o
                prévio assentimento da VISTA®, por meio de seus representantes
                legais.
              </li>
            </ul>

            <ul>
              <li>
                A obrigação da confidencialidade persistirá pelo prazo de 10
                (dez) anos após a rescisão ou término do presente contrato, sob
                pena de incorrer na apuração dos danos morais, materiais e
                eventuais lucros cessantes, inclusive arcando com as despesas
                judiciais, extrajudiciais e honorários advocatícios.
              </li>
            </ul>
            <br />

            <h2 className="privacy_title">10. DA VIGÊNCIA</h2>
            <br />

            <p className="text-justify">
              O prazo de duração do presente contrato é indeterminado, vigorando
              a partir de sua assinatura.
            </p>
            <br />

            <h2 className="privacy_title">11. DA RESCISÃO</h2>
            <br />

            <p className="text-justify">
              O presente Contrato poderá ser resolvido, a qualquer tempo, pelo
              VISTA® ou pelo usuário, de forma imotivada e sem indenização,
              mediante aviso prévio por escrito à outra parte com pelo menos 30
              (trinta) dias de antecedência, responsabilizando-se as partes,
              pelo cumprimento de suas obrigações realizadas ou assumidas até a
              data do efetivo término da prestação dos serviços, sem prejuízo
              das obrigações que, por sua natureza, devam sobreviver ao término
              desta relação, notadamente saldo de pagamentos a serem efetivados
              pelo usuário.
            </p>

            <p className="text-justify">
              Este instrumento será rescindido de imediato frente ao
              usuário/clientes, independentemente de notificação, interpelação
              judicial ou extrajudicial, na hipótese de falência, recuperação
              judicial ou extrajudicial ou insolvência, decretada ou requerida,
              ou dissolução da sociedade VISTA®, ou instalação de qualquer outra
              forma de concurso de credores em face desta.
            </p>
            <br />

            <h2 className="privacy_title">12. LEGISLAÇÃO E FORO</h2>
            <br />

            <p className="text-justify">
              Todos os itens destes Termos são regidos pelas leis vigentes na
              República Federativa do Brasil. Para todos os assuntos referentes
              à interpretação, ao cumprimento ou a qualquer outro questionamento
              relacionado a estes Termos, as partes concordam em se submeter ao
              Foro da Comarca de Brasília, Distrito Federal.
            </p>
            <br />

            <div
              className="justify-content-center pb-3"
              style={{ fontSize: "20px" }}
            ></div>
          </div>
        </Row>
        {this.renderRedirect()}
      </div>
    );
  }
}

export default TermosUso;
