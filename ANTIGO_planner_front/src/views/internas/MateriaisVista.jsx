import React from "react";
import { Redirect } from "react-router-dom";
import { Col, Row, Card, CardHeader } from "reactstrap";

// import apresentacao from '../../assets/docs/Vista_apresentacao_vendas.pdf';
import entrevista from "../../assets/docs/Vista_entrevista_checklist.pdf";
import checklist from "../../assets/docs/Vista_checklist_leads.docx";
import ButtonBackPage from "../../components/Utils/ButtonBackPage.jsx";

class MateriaisVista extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      accessToken: localStorage.getItem("access-token"),
      empresaId: localStorage.getItem("empresa-id"),
      planejadorId: localStorage.getItem("planejador-id"),
      familiaId: localStorage.getItem("familia-id"),
      tipoUsuario: parseInt(localStorage.getItem("tipo-usuario")),

      selectedVideo: "cadastro",
    };
  }

  componentDidMount() {
    if (this.state.accessToken == null) {
      this.setRedirect();
      return;
    }
  }

  setRedirect = () => {
    this.setState({
      redirect: true,
    });
  };

  renderRedirect = () => {
    if (this.state.redirect) {
      return <Redirect to="/auth/login" />;
    }
  };

  render() {
    const item = {
      lineHeight: "2rem",
      paddingLeft: 0,
      cursor: "pointer",
    };

    return (
      <div id="tutoriais" style={{ padding: "0 5%", marginTop: 20 }}>
        <Row>
          <Col>
            <Card style={{ border: "none" }}>
              <CardHeader
                className="border-0"
                style={{ padding: "30px 15px 30px 0" }}
              >
                <Row className="align-items-center" style={{ paddingLeft: 0 }}>
                  <div className="col">
                    <img
                      src={require("../../assets/img/theme/icone-calendario.png")}
                      alt="Meios de Pagamento"
                      className="before-title-img"
                    />
                    <h3 className="mb-0 chart-title">Materiais Vista</h3>
                  </div>
                </Row>
              </CardHeader>
            </Card>
          </Col>
        </Row>
        <Row>
          <Col lg="6">
            <h3>Comercial</h3>
            <ul style={{ paddingLeft: 0 }}>
              {/* <li className="nav-link" style={item}>
                    <a href={apresentacao} target="_blank" rel="noreferrer noopener">
                    <i class="fas fa-file-download pr-2"></i>Apresentação para Vendas 
                    </a>
                  </li>                  
                  <p>O que é planejamento financeiro, passo a passo e implementação da plataforma Vista</p> */}
              {/* ITEM EM DISCUSSÃO SE DEVE ESTAR NA PÁGINA OU NÃO */}

              <li className="nav-link" style={item}>
                <a href={entrevista} target="_blank" rel="noreferrer noopener">
                  <i class="fas fa-file-download pr-2"></i>Entrevista e
                  checklist para leads
                </a>
              </li>
              <p>Perguntas-chave durante a conversa com o cliente</p>

              <li className="nav-link" style={item}>
                <a href={checklist} target="_blank" rel="noreferrer noopener">
                  <i class="fas fa-file-download pr-2"></i>Cheklist para leads
                </a>
              </li>
              <p>Principais questões para entender seu potencial cliente</p>
            </ul>
          </Col>
          <Col lg="6">
            <h3>Institucional</h3>
            <ul style={{ paddingLeft: 0 }}>
              <li className="nav-link" style={item}>
                <a
                  href="https://meuvista.com/manual-vista/"
                  target="_blank"
                  rel="noreferrer noopener"
                >
                  <i class="fas fa-external-link-alt pr-2"></i>Manual
                </a>
              </li>
              <p>Guia prático da plataforma e melhores práticas</p>

              <li className="nav-link" style={item}>
                <a
                  href="https://meuvista.com"
                  target="_blank"
                  rel="noreferrer noopener"
                >
                  <i class="fas fa-external-link-alt pr-2"></i>Site
                </a>
              </li>
              <p>Página institucional da Plataforma Vista</p>
            </ul>
          </Col>
        </Row>
        <ButtonBackPage />
        {this.renderRedirect()}
      </div>
    );
  }
}

export default MateriaisVista;
