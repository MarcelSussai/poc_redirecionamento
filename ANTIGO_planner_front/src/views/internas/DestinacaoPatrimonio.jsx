import React from "react";
import Select from "react-select";
import { CircularProgress, Typography } from "@material-ui/core";
import TableRow from "@material-ui/core/TableRow";
import TableCell from "@material-ui/core/TableCell";
import MUIDataTable from "mui-datatables";
import { Redirect, Link } from "react-router-dom";
import axios from "axios";
import $ from "jquery";
import { confirmAlert } from "react-confirm-alert";
import SkyLight from "react-skylight";
import CurrencyInput from "react-currency-input";

import {
  Col,
  Row,
  Container,
  Card,
  CardHeader,
  NavLink,
  NavItem,
} from "reactstrap";
import { createMuiTheme, MuiThemeProvider } from "@material-ui/core/styles";

class DestinacaoPatrimonio extends React.Component {
  moment = require("moment");

  state = {
    patrimonio: [],
    destinacao: [],
    plano: [],
    item: {},
    valorDestinadoPatrimonioNaoFinanceiro: {},
    valorDestinadoPorPlano: {},
    patrimoniosPlano: [],
  };

  obterPlanosComPatrimonios = async () => {
    var url =
      global.server_api_new +
      global.apiToken +
      "/patrimonio/DevolvePatrimonioPlano/" +
      localStorage.getItem("familia-id");

    var config = {
      headers: {
        Authorization: "bearer " + this.state.accessToken,
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Headers": "Authorization",
        "Access-Control-Allow-Methods":
          "GET, POST, OPTIONS, PUT, PATCH, DELETE",
      },
    };

    axios.get(url, config).then((res) => {
      const planos = [];
      console.debug(res.data, 'DEBUG - DestinacaoPatrimonio.jsx:59')
      if (res.data !== null && res.data.length > 0) {
        res.data.forEach((item) => {
          planos.push({
            id: item.planoId,
            nome: item.plano.descricao,
            patrimonios: item.patrimonios,
            valorTotal: item.valorTotal,
            progresso: item.progresso,
            valorFuturo: item.valorFuturo,
          });
        });
      }

      if (planos.length > this.state.patrimoniosPlano.length) {
        this.setState({ patrimoniosPlano: planos });
      }
    });
  };

  tabelaPlanos = () => {
    if (
      this.state.valorDestinadoPorPlano == null ||
      this.state.valorDestinadoPorPlano.length === 0
    ) {
      return [];
    }

    var idsPlanos = [];
    for (var planoId in this.state.valorDestinadoPorPlano) {
      if (this.state.valorDestinadoPorPlano.hasOwnProperty(planoId)) {
        idsPlanos.push(planoId);
      }
    }

    //this.obterPlanosComPatrimonios(idsPlanos);

    const theme = createMuiTheme({
      overrides: {
        MuiTableRow: {
          root: {
            "&:nth-of-type(odd)": {
              backgroundColor: "#f8f9fe",
            },
            "& input": {
              background: "transparent",
            },
          },
        },
        MUIDataTableSelectCell: {
          fixedHeaderCommon: {
            backgroundColor: "unset",
          },
        },
        MuiTableCell: {
          root: {
            fontFamily: "unset",
            borderTop: "none",
            padding: "16px 10px",
            textAlign: "center",
            lineHeight: "1! important",
            fontSize: ".8rem !important",
          },
          footer: {
            color: "#FFF",
            fontWeight: 700,
          },
        },
        MUIDataTableHeadCell: {
          fixedHeaderYAxis: {
            fontWeight: 700,
          },
          fixedHeaderCommon: {
            zIndex: 0,
          },
        },
        MuiPaper: {
          root: {
            overflow: "hidden",
            margin: 0,
          },
        },
        MuiToolbar: {
          regular: {
            backgroundColor: "#e4eaf4",
            minHeight: "0 !important",
          },
        },
      },
    });

    const customRender = {
      customHeadRender: ({ index, ...column }) => (
        <TableCell
          key={index}
          style={{
            textAlign: "center",
            fontWeight: "bold",
            backgroundColor: "#ffffff",
          }}
        >
          {column.label}
        </TableCell>
      ),
      customBodyRenderFormatCurrency: (value) => (
        <span style={{ color: value < 0 ? "#c21515" : "#000000de" }}>
          {value.toLocaleString("pt-BR", {
            style: "currency",
            currency: "BRL",
          })}
        </span>
      ),
      customBodyRenderFormatPercent: (value) => (
        <span>
          {value.toLocaleString("pt-BR", {
            style: "percent",
            minimumFractionDigits: 2,
            maximumFractionDigits: 2,
          })}
        </span>
      ),
    };

    const columns = {
      root: [
        {
          name: "id",
          options: {
            display: false,
            filter: false,
          },
        },
        {
          name: "patrimonios",
          options: {
            display: false,
            filter: false,
          },
        },
        {
          name: "nome",
          label: "Nome",
          options: {
            filter: true,
            customHeadRender: (props) => customRender.customHeadRender(props),
          },
        },
        {
          name: "valorTotal",
          label: "Valor Destinado",
          options: {
            filter: true,
            customHeadRender: (props) => customRender.customHeadRender(props),
            customBodyRender: (props) =>
              customRender.customBodyRenderFormatCurrency(props),
          },
        },
        {
          name: "progresso",
          label: "Progresso",
          options: {
            filter: true,
            customHeadRender: (props) => customRender.customHeadRender(props),
            customBodyRender: (props) =>
              customRender.customBodyRenderFormatPercent(props),
          },
        },
        {
          name: "valorFuturo",
          label: "Valor Final Esperado",
          options: {
            filter: true,
            customHeadRender: (props) => customRender.customHeadRender(props),
            customBodyRender: (props) =>
              customRender.customBodyRenderFormatCurrency(props),
          },
        },
      ],
      expandable: [
        {
          name: "patrimonio.descricao",
          label: "Patrimonio",
          options: {
            filter: true,
            customHeadRender: (props) => customRender.customHeadRender(props),
          },
        },
        {
          name: "percentualPlano",
          label: "Distribuição",
          options: {
            filter: true,
            customHeadRender: (props) => customRender.customHeadRender(props),
            customBodyRender: (props) =>
              customRender.customBodyRenderFormatPercent(props),
          },
        },
        {
          name: "valor",
          label: "Valor",
          options: {
            filter: true,
            customHeadRender: (props) => customRender.customHeadRender(props),
            customBodyRender: (props) =>
              customRender.customBodyRenderFormatCurrency(props),
          },
        },
      ],
    };

    const options = {
      root: {
        rowsPerPage: 100,
        textLabels: global.textLabels,
        filter: true,
        print: false,
        download: false,
        filterType: "dropdown",
        responsive: "standard",
        selectableRows: false,
        expandableRows: true,
        expandableRowsHeader: false,
        expandableRowsOnClick: true,
        customFooter: () => {},
        renderExpandableRow: (rowData) => {
          const colSpan = rowData.length + 1;
          return (
            <TableRow>
              <TableCell colSpan={colSpan}>
                <MuiThemeProvider theme={theme}>
                  <MUIDataTable
                    data={rowData[1]}
                    columns={columns.expandable}
                    options={options.expandable}
                  />
                </MuiThemeProvider>
              </TableCell>
            </TableRow>
          );
        },
      },
      expandable: {
        search: false,
        filter: false,
        print: false,
        download: false,
        viewColumns: false,
        responsive: "standard",
        selectableRows: false,
        rowsPerPage: 100,
        customFooter: () => {},
      },
    };

    return (
      <MuiThemeProvider theme={theme}>
        <MUIDataTable
          title={
            <h2 style={{ paddingTop: 15, paddingBottom: 10 }}>
              Investimentos por planos
            </h2>
          }
          data={this.state.patrimoniosPlano}
          columns={columns.root}
          options={options.root}
        />
      </MuiThemeProvider>
    );
  };

  getMuiTheme = () =>
    createMuiTheme({
      overrides: {
        MuiTableRow: {
          root: {
            "&:nth-of-type(odd)": {
              backgroundColor: "#f8f9fe",
            },
          },
        },
        MUIDataTableSelectCell: {
          fixedHeaderCommon: {
            backgroundColor: "unset",
          },
        },
        MuiTableCell: {
          root: {
            fontFamily: "unset",
            borderTop: "none",
            padding: "16px 10px",
          },
        },
        MUIDataTableHeadCell: {
          fixedHeaderYAxis: {
            fontWeight: 700,
          },
        },
        MuiPaper: {
          root: {
            overflow: "hidden",
          },
        },
        MuiToolbar: {
          regular: {
            backgroundColor: "#e4eaf4",
          },
        },
        MUIDataTableBodyCell: {
          stackedCommon: {
            height: "fit-content!important",
          },
        },
        MUIDataTable: {
          responsiveStacked: {
            overflowY: "hidden",
            overflowX: "hidden",
          },
        },
        MuiPopover: {
          root: {
            left: "0px !important",
          },
        },
      },
    });

  componentDidMount() {
    this.setState(
      {
        accessToken: localStorage.getItem("access-token"),
        empresaId: localStorage.getItem("empresa-id"),
        familiaId: localStorage.getItem("familia-id"),
        planejadorId: localStorage.getItem("planejador-id"),
      },
      function () {
        if (this.state.accessToken == null) {
          this.setRedirect();
          return;
        }

        this.atualizarPatrimonios();
        this.atualizarDestinacoes();
        this.atualizarPlanos();
        global.mostrarAjudaDaPaginaAutomaticamente(this);
      }
    );
    this.obterPlanosComPatrimonios();
  }

  setRedirect = () => {
    this.setState({
      redirect: true,
    });
  };

  renderRedirect = () => {
    if (this.state.redirect) {
      return <Redirect to="/auth/login" />;
    }
  };

  montarDropDown = (itens, id, changeMethod, selectedValue) => (
    <Select
      id={id}
      onChange={changeMethod}
      className="select-component"
      options={itens}
      defaultValue={itens != null ? itens[0] : ""}
      placeholder="Selecione..."
      value={
        itens != null && selectedValue != null
          ? itens.find((op) => {
              if (
                op.value != null &&
                selectedValue["id"] != null &&
                op.value["id"] != null
              ) {
                return op.value.id === selectedValue.id;
              } else {
                return op.value != null && op.value === selectedValue;
              }
            })
          : null
      }
    />
  );

  atualizarPlanos = () => {
    //var url = global.server_api + 'api/sonho/planos/familia/' + this.state.familiaId + "/emAndamento";
    var url =
      global.server_api_new +
      global.apiToken +
      "/sonho/planos/familia/" +
      this.state.familiaId +
      "/emAndamento";

    var config = {
      headers: {
        Authorization: "bearer " + this.state.accessToken,
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Headers": "Authorization",
        "Access-Control-Allow-Methods":
          "GET, POST, OPTIONS, PUT, PATCH, DELETE",
      },
    };

    axios.get(url, config).then((res) => {
      var data = [];
      var dataCombo = [];

      if (res.data.results != null) {
        data = res.data.results;
        data.forEach((mp) => {
          dataCombo.push({
            value: mp,
            label: mp.descricao,
          });
        });
      }
      this.setState({ plano: data, planoCombo: dataCombo });
    });
  };

  atualizarPatrimonios = () => {
    //var url = global.server_api + 'api/patrimonio/familia/' + this.state.familiaId + '/filtro';
    var url =
      global.server_api_new +
      global.apiToken +
      "/patrimonio/familia/" +
      this.state.familiaId +
      "/filtro";

    var config = {
      headers: {
        Authorization: "bearer " + this.state.accessToken,
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Headers": "Authorization",
        "Access-Control-Allow-Methods":
          "GET, POST, OPTIONS, PUT, PATCH, DELETE",
      },
    };

    var currentScroll = [
      document.documentElement.scrollLeft || document.body.scrollLeft,
      document.documentElement.scrollTop || document.body.scrollTop,
    ];

    global.spinnerShow($);

    axios.post(url, {}, config).then((res) => {
      var patrimonio = [];
      var dataCombo = [];

      if (res.data.results != null) {
        patrimonio = res.data.results;

        patrimonio.forEach((item) => {
          dataCombo.push({
            value: item,
            label: item.descricao,
          });

          if (item.tipoPatrimonio === 3) {
            this.buscarValorTotalEDestinadoDoPatrimonioFinanceiro(item.id);
          }
        });
      }

      this.setState(
        { patrimonio: patrimonio, patrimonioCombo: dataCombo },
        function () {
          global.spinnerHide($, currentScroll);
        }
      );
    });
  };

  atualizarDestinacoes = () => {
    //var url = global.server_api + 'api/patrimonio/destinacao/familia/' + this.state.familiaId;
    var url =
      global.server_api_new +
      global.apiToken +
      "/patrimonio/destinacao/familia/" +
      this.state.familiaId;

    var config = {
      headers: {
        Authorization: "bearer " + this.state.accessToken,
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Headers": "Authorization",
        "Access-Control-Allow-Methods":
          "GET, POST, OPTIONS, PUT, PATCH, DELETE",
      },
    };

    var currentScroll = [
      document.documentElement.scrollLeft || document.body.scrollLeft,
      document.documentElement.scrollTop || document.body.scrollTop,
    ];

    global.spinnerShow($);

    axios.get(url, config).then((res) => {
      var destinacao = [];
      var valorDestinadoPatrimonioNaoFinanceiro = {};
      var valorDestinadoPorPlano = this.state.valorDestinadoPorPlano;

      if (res.data.results != null) {
        destinacao = res.data.results;

        destinacao.forEach((item) => {
          if (item.patrimonio != null && item.patrimonio.tipoPatrimonio !== 3) {
            if (
              valorDestinadoPatrimonioNaoFinanceiro[item.patrimonio.id] == null
            ) {
              valorDestinadoPatrimonioNaoFinanceiro[item.patrimonio.id] = 0;
            }

            valorDestinadoPatrimonioNaoFinanceiro[item.patrimonio.id] +=
              (item.percentualDestinado / 100) * item.patrimonio.valorAtual;

            if (item.plano != null) {
              var valor = valorDestinadoPorPlano[item.plano.id];

              if (valor == null) valor = 0;

              valor +=
                (item.percentualDestinado / 100) * item.patrimonio.valorAtual;

              valorDestinadoPorPlano[item.plano.id] = valor;
            }
          }
        });
      }

      this.setState(
        {
          destinacao: destinacao,
          valorDestinadoPatrimonioNaoFinanceiro: valorDestinadoPatrimonioNaoFinanceiro,
          valorDestinadoPorPlano: valorDestinadoPorPlano,
        },
        function () {
          global.spinnerHide($, currentScroll);
        }
      );
    });
  };

  buscarValorTotalEDestinadoDoPatrimonioFinanceiro = (patrimonioId) => {
    //var url = global.server_api + 'api/patrimonio/destinacao/familia/' + this.state.familiaId + '/compras/' + patrimonioId;
    var url =
      global.server_api_new +
      global.apiToken +
      "/patrimonio/destinacao/familia/" +
      this.state.familiaId +
      "/compras/" +
      patrimonioId;

    var config = {
      headers: {
        Authorization: "bearer " + this.state.accessToken,
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Headers": "Authorization",
        "Access-Control-Allow-Methods":
          "GET, POST, OPTIONS, PUT, PATCH, DELETE",
      },
    };

    axios.get(url, config).then((res) => {
      if (res.data.success === true) {
        if (res.data.singleResult == null) {
          console.log("Não veio nenhum resultado");
          return;
        }

        var patrimonio = this.state.patrimonio;

        var rowIndex = -1;

        var patrimonioFiltrado = this.state.patrimonio.filter(function (
          it,
          index
        ) {
          var result = it.id === patrimonioId;

          if (result) {
            rowIndex = index;
          }

          return result;
        });

        if (
          patrimonio != null &&
          patrimonio[rowIndex] != null &&
          patrimonioFiltrado != null &&
          patrimonioFiltrado.length === 1
        ) {
          if (patrimonio[rowIndex].id !== patrimonioId) {
            console.log(
              "Pegamos o rowIndex errado para o patrimonioId",
              patrimonioId,
              patrimonio[rowIndex].id,
              rowIndex
            );
          } else {
            patrimonio[rowIndex].valorAplicado =
              res.data.singleResult.totalPatrimonio != null
                ? res.data.singleResult.totalPatrimonio
                : 0;

            patrimonio[rowIndex].valorAtual =
              res.data.singleResult.totalPatrimonioCotizado != null
                ? res.data.singleResult.totalPatrimonioCotizado
                : 0;

            patrimonio[rowIndex].valorDestinado =
              res.data.singleResult.totalDestinado != null
                ? res.data.singleResult.totalDestinado
                : 0;

            patrimonio[rowIndex].destinacaoPorPlano =
              res.data.singleResult.destinacaoAtual != null
                ? res.data.singleResult.destinacaoAtual
                : [];

            var valorDestinadoPorPlano = this.state.valorDestinadoPorPlano;

            if (res.data.singleResult.destinacaoAtual != null) {
              res.data.singleResult.destinacaoAtual.forEach((it) => {
                var valor = valorDestinadoPorPlano[it.planoId];

                if (valor == null) valor = 0;

                // valor += it.valorDestinado;
                valor += it.valorDestinadoCotizado;

                valorDestinadoPorPlano[it.planoId] = valor;
              });
            }

            this.setState(
              {
                patrimonio: patrimonio,
                valorDestinadoPorPlano: valorDestinadoPorPlano,
              },
              function () {
                //console.log('Patrimonio Atualizado com Destinacoes', patrimonioId, rowIndex, res.data.singleResult.destinacaoAtual);
              }
            );
          }
        }
      } else {
        console.log(res.data.exception);
      }
    });
  };

  salvar = () => {
    //var url = global.server_api + 'api/patrimonio/destinacao';
    var url =
      global.server_api_new + global.apiToken + "/patrimonio/destinacao";

    var config = {
      headers: {
        Authorization: "bearer " + this.state.accessToken,
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Headers": "Authorization",
        "Access-Control-Allow-Methods":
          "GET, POST, OPTIONS, PUT, PATCH, DELETE",
      },
    };

    var currentScroll = [
      document.documentElement.scrollLeft || document.body.scrollLeft,
      document.documentElement.scrollTop || document.body.scrollTop,
    ];

    global.spinnerShow($);

    var item = this.state.item;

    item.empresaId = this.state.empresaId;
    item.familiaId = this.state.familiaId;

    delete item.patrimonio;
    delete item.plano;

    if (item.id == null || item.id === "") {
      delete item.id;
    }

    axios.post(url, item, config).then((res) => {
      global.spinnerHide($, currentScroll);

      if (res.data.success === true) {
        window.location.reload();
      } else {
        console.log(res.data.exception);
        const options = {
          title: "Erro ao aplicar mudanças na destinação",
          message: res.data.exception.Message,
          buttons: [
            {
              label: "Ok",
            },
          ],
        };

        confirmAlert(options);
      }
    });
  };

  confirmaExcluir = (planoId, patrimonioId) => {
    const options = {
      title: "Exclusão de Destinação",
      message:
        "Tem certeza que deseja excluir? Este procedimento é irreversível!",
      buttons: [
        {
          label: "Sim",
          onClick: () => this.excluir(planoId, patrimonioId),
        },
        {
          label: "Não",
        },
      ],
    };

    confirmAlert(options);
  };

  excluir = (planoId, patrimonioId) => {
    //var url = global.server_api + 'api/patrimonio/destinacao/investimento/' + patrimonioId + "/" + planoId;
    var url =
      global.server_api_new +
      global.apiToken +
      "/patrimonio/destinacao/investimento/" +
      patrimonioId +
      "/" +
      planoId;

    var config = {
      headers: {
        Authorization: "bearer " + this.state.accessToken,
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Headers": "Authorization",
        "Access-Control-Allow-Methods":
          "GET, POST, OPTIONS, PUT, PATCH, DELETE",
      },
    };

    axios.delete(url, {}, config).then((res) => {
      const result = res.data;

      if (result.success && result.affectedResults > 0) {
        window.location.reload();
      } else {
        console.log(res.data.exception);
        alert("Erro ao remover destinação. " + res.data.exception.Message);
      }
    });
  };

  mostrarPatrimonios = () => {
    if (this.state.patrimonio == null || this.state.patrimonio.length === 0) {
      return (
        <Container>
          <Row>
            <Col>Nenhum patrimônio cadastrado ainda.</Col>
          </Row>
        </Container>
      );
    } else {
      var patrimoniosOrdenados = this.state.patrimonio.sort(function (a, b) {
        var aTipoPatrimonio = a.tipoPatrimonio;
        var bTipoPatrimonio = b.tipoPatrimonio;
        var aDescricao = a.descricao;
        var bDescricao = b.descricao;
        if (aTipoPatrimonio === bTipoPatrimonio) {
          return aDescricao < bDescricao ? -1 : aDescricao > bDescricao ? 1 : 0;
        } else {
          return aTipoPatrimonio !== 3 && bTipoPatrimonio === 3 ? -1 : 1;
        }
      });

      const options = {
        selectableRows: false,
        expandableRows: true,
        expandableRowsOnClick: false,
        isRowExpandable: (dataIndex, expandedRows) => {
          return true;
        },
        onRowsExpand: (a, b) => {},
        renderExpandableRow: (rowData, rowMeta) => {
          return this.mostrarDestinacoes(rowData[0]);
        },
        textLabels: global.textLabels,
        filter: true,
        filterType: "dropdown",
        responsive: "stacked",
        print: false,
        download: false,
        serverSide: false,
        pagination: false,
      };
      const columns = [
        {
          name: "id",
          label: "id",
          options: {
            display: false,
            viewColumns: false,
            filter: false,
          },
        },
        {
          name: "descricao",
          label: "Descrição",
          options: {
            display: true,
            viewColumns: true,
            filter: false,
          },
        },
        {
          name: "valorAplicado",
          label: "Valor Aplicado",
          options: {
            display: false,
            viewColumns: true,
            filter: false,
            customBodyRender: (value) => {
              return (
                <>
                  {value != null
                    ? value
                        .toLocaleString("pt-BR", {
                          style: "currency",
                          currency: "BRL",
                        })
                        .replace("R$", "")
                    : "-"}
                </>
              );
            },
          },
        },
        {
          name: "valorAtual",
          label: "Valor Atual",
          options: {
            display: true,
            viewColumns: true,
            filter: false,
            customBodyRender: (value) => {
              return (
                <>
                  {value != null
                    ? value
                        .toLocaleString("pt-BR", {
                          style: "currency",
                          currency: "BRL",
                        })
                        .replace("R$", "")
                    : "-"}
                </>
              );
            },
          },
        },
        {
          name: "tipoPatrimonio",
          label: "Patrimônio",
          options: {
            display: false,
            viewColumns: true,
            filter: false,
            customBodyRender: (value) => {
              return <>{value != null ? global.tipoPatrimonio[value] : ""}</>;
            },
          },
        },
        {
          name: "percentualDestinado",
          label: "Percentual destinado",
          options: {
            display: true,
            viewColumns: true,
            filter: false,
            customBodyRender: (value, tableMeta) => {
              var patrimonioId = tableMeta.rowData[0];
              var percentualTotalDestinado = 0;
              var destinacoes = [];

              var patrimonioFiltrado = this.state.patrimonio.filter(function (
                it,
                index
              ) {
                var result = it.id === patrimonioId;
                return result;
              });

              var patrimonio = patrimonioFiltrado[0];
              var tipoPatrimonio = patrimonio.tipoPatrimonio;

              destinacoes =
                tipoPatrimonio !== 3
                  ? this.state.destinacao.filter(function (item) {
                      return (
                        item.patrimonio != null &&
                        item.patrimonio.id === patrimonioId
                      );
                    })
                  : patrimonio.destinacaoPorPlano;

              if (destinacoes) {
                destinacoes.forEach((destinacao) => {
                  percentualTotalDestinado += this.percentualDestinadoPlano(
                    destinacao,
                    patrimonio,
                    tipoPatrimonio
                  );
                });
              }

              return (
                <div style={{ marginLeft: 10 }}>
                  {percentualTotalDestinado !== 0
                    ? `${parseFloat(percentualTotalDestinado.toFixed(2))} %`
                    : "-"}
                </div>
              );
            },
          },
        },
        {
          name: "tipoPatrimonio",
          label: "Nova Destinação",
          options: {
            display: true,
            viewColumns: true,
            filter: false,
            customBodyRender: (value, tableMeta) => {
              var patrimonioId = tableMeta.rowData[0];

              var patrimonioFiltrado = this.state.patrimonio.filter(function (
                it,
                index
              ) {
                var result = it.id === patrimonioId;
                return result;
              });

              var patrimonio = null;

              if (patrimonioFiltrado != null && patrimonioFiltrado.length > 0) {
                patrimonio = patrimonioFiltrado[0];
              }

              return (
                <div style={{ cursor: "pointer", textAlign: "left" }}>
                  <div
                    onClick={(e) => {
                      this.setItemOnState("patrimonio", patrimonio);
                      this.setItemOnState("patrimonioId", patrimonioId);
                      global.showModal(this.modal);
                    }}
                    style={{
                      padding: "0.5rem 0.5rem",
                      float: "left",
                      color: "white",
                      borderRadius: "30px",
                      background: "#9947F4",
                      fontWeight: "100",
                      minWidth: "90px",
                    }}
                  >
                    <i
                      style={{ marginRight: ".45rem", fontSize: ".6rem" }}
                      className="fas fa-plus"
                    ></i>
                    Adicionar
                  </div>
                </div>
              );
            },
          },
        },
      ];
      return (
        <div>
          <MuiThemeProvider theme={this.getMuiTheme()}>
            <MUIDataTable
              id="orcamento-table"
              title={
                <>
                  <h2 style={{ paddingTop: 15, paddingBottom: 10 }}>
                    Destine seu patrimônio atual para os seus objetivos
                  </h2>
                  <Typography>
                    {false && (
                      <CircularProgress
                        size={24}
                        style={{ marginLeft: 15, position: "relative", top: 4 }}
                      />
                    )}
                  </Typography>
                </>
              }
              data={patrimoniosOrdenados}
              columns={columns}
              options={options}
            />
          </MuiThemeProvider>
        </div>
      );
    }
  };

  mostrarDestinacoes = (patrimonioId) => {
    var patrimonioFiltrado = this.state.patrimonio.filter(function (it, index) {
      var result = it.id === patrimonioId;
      return result;
    });

    var patrimonio = patrimonioFiltrado[0];
    var tipoPatrimonio = patrimonio.tipoPatrimonio;

    var destinacoes =
      tipoPatrimonio !== 3
        ? this.state.destinacao.filter(function (item) {
            return (
              item.patrimonio != null && item.patrimonio.id === patrimonioId
            );
          })
        : patrimonio.destinacaoPorPlano;

    if (destinacoes == null || destinacoes.length === 0) {
      return (
        <TableRow style={{ border: "1px solid rgba(224, 224, 224, 1)" }}>
          <TableCell
            colSpan="7"
            style={{ paddingLeft: "3rem", background: "#FFF" }}
          >
            <div>Nenhuma destinação ainda.</div>
          </TableCell>
        </TableRow>
      );
    } else {
      return destinacoes.map((item, key) => {
        return (
          <TableRow
            key={key}
            style={{ border: "1px solid rgba(224, 224, 224, 1)" }}
          >
            <TableCell
              colSpan="7"
              style={{ paddingLeft: "3rem", background: "#FFF" }}
            >
              <div style={{ width: "100%" }}>
                <div style={{ width: "15px", display: "inline-block" }}>
                  <div
                    onClick={() =>
                      this.confirmaExcluir(
                        tipoPatrimonio !== 3 ? item.plano.id : item.planoId,
                        patrimonioId
                      )
                    }
                  >
                    <i
                      className="far fa-trash-alt"
                      style={{ cursor: "pointer" }}
                    ></i>
                  </div>
                </div>
                <div style={{ width: "45%", display: "inline-block" }}>
                  {tipoPatrimonio !== 3
                    ? item.plano.sonho.descricao
                    : item.nomePlano}
                </div>
                <div style={{ width: "30%", display: "inline-block" }}>
                  {this.mostraValorDestinadoPlano(
                    item,
                    patrimonio,
                    tipoPatrimonio
                  )}
                </div>
                <div style={{ width: "20%", display: "inline-block" }}>
                  {this.mostraPercentualDestinadoPlano(
                    item,
                    patrimonio,
                    tipoPatrimonio
                  )}
                </div>
              </div>
            </TableCell>
          </TableRow>
        );
      });
    }
  };

  mostraValorDestinadoPlano = (item, patrimonio, tipoPatrimonio) => {
    if (tipoPatrimonio !== 3) {
      return (
        <>
          {((item.percentualDestinado / 100) * item.patrimonio.valorAtual)
            .toLocaleString("pt-BR", { style: "currency", currency: "BRL" })
            .replace("R$", "")}
        </>
      );
    }

    if (
      patrimonio != null &&
      patrimonio.destinacaoPorPlano != null &&
      patrimonio.destinacaoPorPlano.length > 0
    ) {
      var compras = patrimonio.destinacaoPorPlano;

      if (compras != null) {
        var destinacao = compras.filter(function (dest) {
          return dest.planoId === item.planoId;
        });

        if (destinacao != null && destinacao.length > 0) {
          return (
            <>
              {/* { destinacao[0].valorDestinado.toLocaleString('pt-BR', { style: 'currency', currency: 'BRL' }).replace('R$', '') } */}
              {destinacao[0].valorDestinadoCotizado
                .toLocaleString("pt-BR", { style: "currency", currency: "BRL" })
                .replace("R$", "")}
            </>
          );
        }
      }
    }

    return <>0,00</>;
  };

  percentualDestinadoPlano = (item, patrimonio, tipoPatrimonio) => {
    if (item == null) {
      return null;
    }

    if (tipoPatrimonio !== 3) {
      return item.percentualDestinado;
    }

    if (patrimonio != null) {
      var compras = patrimonio.destinacaoPorPlano;

      if (compras != null) {
        var destinacao = compras.find(function (dest) {
          return dest.planoId === item.planoId;
        });

        if (destinacao != null) {
          return parseFloat(destinacao.percentualDestinado * 100);
        }
      }
    }

    return null;
  };

  mostraPercentualDestinadoPlano = (item, patrimonio, tipoPatrimonio) => {
    var percentualDestinado = this.percentualDestinadoPlano(
      item,
      patrimonio,
      tipoPatrimonio
    );

    if (percentualDestinado !== null) {
      return (
        <>
          {percentualDestinado.toLocaleString("pt-BR", {
            minimumFractionDigits: 0,
            maximumFractionDigits: 4,
          })}{" "}
          %
        </>
      );
    }

    return <>0,0000 %</>;
  };

  setItemOnState = (key, value, callback) => {
    var item = this.state.item;

    item[key] = value;

    if (callback) this.setState({ item: item }, callback);
    else this.setState({ item: item });
  };

  mudandoPlano = (selectedOption) => {
    const plano = selectedOption.value;
    this.setItemOnState("plano", plano);
    this.setItemOnState("planoId", plano != null ? plano.planoId : null);

    this.verificarDestinacaoExistente(
      plano.planoId,
      this.state.item.patrimonioId
    );
  };

  mudandoPatrimonio = (selectedOption) => {
    const patrimonio = selectedOption.value;
    this.setItemOnState("patrimonio", patrimonio);
    this.setItemOnState(
      "patrimonioId",
      patrimonio != null ? patrimonio.id : null
    );

    this.verificarDestinacaoExistente(this.state.item.planoId, patrimonio.id);
  };

  verificarDestinacaoExistente = (planoId, patrimonioId) => {
    if (planoId == null || patrimonioId == null) {
      this.setItemOnState("id", null);
      return;
    }

    console.log("patrimonios", this.state.patrimonio);

    var patrimonioFiltrado = this.state.patrimonio.filter(function (it, index) {
      var result = it.id === patrimonioId;
      return result;
    });

    var patrimonio = patrimonioFiltrado[0];
    var tipoPatrimonio = patrimonio.tipoPatrimonio;

    console.log("patrimonio", patrimonio, tipoPatrimonio);

    var destinacao =
      tipoPatrimonio !== 3
        ? this.state.destinacao.find(function (item) {
            return (
              item.patrimonio != null &&
              item.patrimonio.id === patrimonioId &&
              item.plano != null &&
              item.plano.id === planoId
            );
          })
        : patrimonio.destinacaoPorPlano.find(function (item) {
            return item.planoId === planoId;
          });

    console.log("destinacao", destinacao);

    var valorDestinado = this.percentualDestinadoPlano(
      destinacao,
      patrimonio,
      tipoPatrimonio
    );

    console.log("valorDestinado", valorDestinado);

    //Se tem destinacao do investimento e nao foi preenchida a destinacao do patrimonio
    if (valorDestinado != null) {
      this.setItemOnState("percentualDestinado", valorDestinado);
      this.setItemOnState(
        "percentualDestinadoFormatado",
        valorDestinado.toLocaleString("pt-BR", {
          minimumFractionDigits: 0,
          maximumFractionDigits: 10,
        }) + "%"
      );
    }
  };

  limparSelecao = () => {
    var item = {};
    this.setState({
      item: item,
    });
  };

  mostrarPopup = () => (
    <div className="planosSonhos">
      <SkyLight
        ref={(ref) => (this.modal = ref)}
        transitionDuration={0}
        beforeOpen={this.preCarregarParaEdicao}
        afterClose={this.limparSelecao}
        title={
          <>
            <div className="pop-up-title">
              <i className="ni ni-check-bold"></i>
              <h2>Cadastro de Destinação</h2>
            </div>
          </>
        }
      >
        <Row>
          <Col lg="4" xl="4">
            <label>Patrimômio *</label>
            {this.montarDropDown(
              this.state.patrimonioCombo,
              "patrimonio",
              this.mudandoPatrimonio,
              this.state.item.patrimonio
            )}
          </Col>
          <Col lg="4" xl="4">
            <label>Plano *</label>
            {this.montarDropDown(
              this.state.planoCombo,
              "plano",
              this.mudandoPlano,
              this.state.item.plano
            )}
          </Col>
          <Col lg="4" xl="4">
            <div>
              <label>Percentual *</label>
              <CurrencyInput
                value={
                  this.state.item.percentualDestinadoFormatado != null
                    ? this.state.item.percentualDestinadoFormatado
                    : this.state.item.percentualDestinado
                }
                thousandSeparator={"."}
                decimalSeparator={","}
                suffix={"%"}
                onChangeEvent={(event, formattedValue, value) => {
                  console.log(value, formattedValue);
                  this.setItemOnState("percentualDestinado", value);
                  this.setItemOnState(
                    "percentualDestinadoFormatado",
                    formattedValue
                  );
                }}
              />
            </div>
          </Col>
        </Row>
        <Row>
          <Col lg="12" xl="12">
            <button
              onClick={() => this.salvar()}
              className="featured-button"
              style={{
                float: "right",
                backgroundImage:
                  "url(" +
                  require("../../assets/img/theme/botao-destaque.png") +
                  ")",
              }}
            >
              Salvar
            </button>
          </Col>
        </Row>
      </SkyLight>
    </div>
  );

  valorTotalPlano = (planoId) => {
    if (this.state.plano != null) {
      var plano = this.state.plano.filter(function (it) {
        return it.planoId === parseInt(planoId);
      });

      if (plano != null && plano.length > 0) {
        return plano[0].valorEstimadoFuturo;
      }
    }

    return 0;
  };

  descricaoPlano = (planoId) => {
    if (this.state.plano != null) {
      var plano = this.state.plano.filter(function (it) {
        return it.planoId === parseInt(planoId);
      });

      if (plano != null && plano.length > 0) {
        return plano[0].descricao;
      }
    }

    return 0;
  };

  render() {
    return (
      <div style={{ padding: "0 5%", marginTop: 20 }}>
        {this.renderRedirect()}
        {this.mostrarPopup()}

        <div className="home-summary-top">
          <Row>
            <Col lg="12" xl="12" style={{ marginBottom: "15px" }}>
              <div>
                <Card>
                  <CardHeader
                    className="border-0"
                    style={{ padding: "30px 15px 30px 0" }}
                  >
                    <Row className="align-items-center">
                      <Col xl="3">
                        <img
                          src={require("../../assets/img/theme/icone-calendario.png")}
                          alt="Destinação de Patrimônio"
                          className="before-title-img"
                        />
                        <h3 className="mb-0 chart-title">
                          Destinação de Patrimônio
                        </h3>
                      </Col>
                      <Col xl="9" className="mt-2">
                        <Row className="justify-content-center">
                          <NavItem
                            className="general-button dark with-left-margin"
                            style={{ float: "right" }}
                          >
                            <NavLink
                              className="nav-link-icon aux-button"
                              to="/admin/planos-sonhos"
                              tag={Link}
                            >
                              <span className="nav-link-inner--text">
                                Planos & Sonhos
                              </span>
                            </NavLink>
                          </NavItem>
                          <NavItem
                            className="general-button dark with-left-margin"
                            style={{ float: "right" }}
                          >
                            <NavLink
                              className="nav-link-icon aux-button"
                              to="/admin/gestao-planos-sonhos"
                              tag={Link}
                            >
                              <span className="nav-link-inner--text inner">
                                Gestão de Planos
                              </span>
                            </NavLink>
                          </NavItem>
                          <NavItem
                            className="general-button dark with-left-margin"
                            style={{ float: "right" }}
                          >
                            <NavLink
                              className="nav-link-icon aux-button"
                              to="/admin/destinacao-investimentos"
                              tag={Link}
                            >
                              <span className="nav-link-inner--text inner">
                                Dest. de Investimentos
                              </span>
                            </NavLink>
                          </NavItem>
                          <NavItem
                            className="general-button dark with-left-margin"
                            style={{ float: "right" }}
                          >
                            <NavLink
                              className="nav-link-icon featured-button"
                              to="/admin/patrimonio/1"
                              tag={Link}
                              style={{
                                backgroundImage:
                                  "url(" +
                                  require("../../assets/img/theme/botao-destaque.png") +
                                  ")",
                              }}
                            >
                              <span className="nav-link-inner--text inner">
                                Novo Patrimônio
                              </span>
                            </NavLink>
                          </NavItem>
                        </Row>
                      </Col>
                    </Row>
                  </CardHeader>
                </Card>
              </div>
            </Col>
          </Row>
        </div>
        <Row>
          <Col xl="6" lg="12" className="p-0">
            {this.mostrarPatrimonios()}
          </Col>
          <Col xl="6" lg="12" className="p-0 pl-3">
            {this.tabelaPlanos()}
          </Col>
        </Row>

        {/* <Card style={{marginBottom:"10px"}} className="plano-card-invert">
              <CardHeader className="plano-title-invert">
                <Row style={{
                  borderBottom: "1px solid white",
                  paddingBottom: "15px", 
                  marginBottom: "15px",
                  fontWeight: "bold"}}>
                  <Col lg="12" xl="12">
                    <center>Destinação por Planos</center>
                  </Col>
                </Row>
                <Row>
                  <Col lg="5" xl="5">
                    <b>Nome do Plano</b>
                  </Col>
                  <Col lg="2" xl="2">
                    <b>Valor Destinado</b>
                  </Col>
                  <Col lg="2" xl="2">
                    <b>% Concluído</b>
                  </Col>
                  <Col lg="3" xl="3">
                    <b>Valor Final Esperado</b>
                  </Col>
                </Row>
              </CardHeader>
              <CardBody>
                {this.mostrarValorAcumuladoPlanos()}
              </CardBody>
          </Card> */}
      </div>
    );
  }
}

export default DestinacaoPatrimonio;
