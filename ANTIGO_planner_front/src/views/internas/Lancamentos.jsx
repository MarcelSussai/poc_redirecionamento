import React from "react";
import Datepicker, { registerLocale } from "react-datepicker";
import Switch from "react-switch";
import MaskedInput from "react-maskedinput";
import br from "date-fns/locale/pt-BR";
import "react-datepicker/dist/react-datepicker.css";
import { Redirect } from "react-router-dom";
import { CircularProgress } from "@material-ui/core";
import { confirmAlert } from "react-confirm-alert";
import MUIDataTable from "mui-datatables";
import axios from "axios";
import Rightbar from "../../components/Sidebar/RightBar.jsx";
import CustomFooter from "./CustomFooter.jsx";
import { Col, Card, CardHeader, Row } from "reactstrap";
import { createMuiTheme, MuiThemeProvider } from "@material-ui/core/styles";
import RestoreDate from "@material-ui/icons/Restore";
import Tooltip from "@material-ui/core/Tooltip";
import { withStyles } from "@material-ui/core/styles";
import Restore3Months from "@material-ui/icons/Looks3Outlined";
import Select from "react-select";
import "../../assets/css/aposentadoria.css";

registerLocale("br", br);

class Lancamentos extends React.Component {
  moment = require("moment");

  state = {
    page: 0,
    count: 1,
    data: [["Carregando Dados..."]],
    isLoading: false,
    startDate: localStorage.getItem("dataInicio")
      ? new Date(JSON.parse(localStorage.getItem("dataInicio")))
      : this.moment().startOf("month").toDate(),
    endDate: localStorage.getItem("dataFim")
      ? new Date(JSON.parse(localStorage.getItem("dataFim")))
      : this.moment().endOf("month").toDate(),
    fieldList: {
      data: true,
      descricao: true,
      valor: true,
      nomeMeioPagamento: true,
      nomeCategoria: true,
      nomeOrcamento: true,
      agrupamento: true,
      tipoOperacao: false,
    },
    Sumario: {
      Orcamento: {
        Receitas: 0,
        Despesas: 0,
        Investimentos: 0,
        Dividas: 0,
      },
    },
    dropdown: {
      orcamentos: [],
      categorias: [],
    },
    checkedNotCategorized: sessionStorage.getItem("checkedNotCategorized")
      ? sessionStorage.getItem("checkedNotCategorized") === "true"
      : false,
  };

  getMuiTheme = () =>
    createMuiTheme({
      overrides: {
        MuiTableRow: {
          root: {
            "&:nth-of-type(odd)": {
              backgroundColor: "#f8f9fe",
            },
            "&.Mui-selected": {
              backgroundColor: "#f5005714",
            },
          },
        },
        MUIDataTableSelectCell: {
          fixedHeaderCommon: {
            backgroundColor: "unset",
          },
        },
        MuiTableCell: {
          root: {
            fontFamily: "unset",
          },
        },
        MUIDataTableHeadCell: {
          fixedHeaderYAxis: {
            fontWeight: 700,
          },
          fixedHeaderCommon: {
            zIndex: 0,
          },
        },
        MuiPaper: {
          root: {
            overflow: "hidden",
            margin: 0,
          },
        },
        MuiToolbar: {
          regular: {
            backgroundColor: "#e4eaf4",
          },
        },
      },
    });

  componentDidMount() {
    this.setState(
      {
        accessToken: localStorage.getItem("access-token"),
        empresaId: localStorage.getItem("empresa-id"),
        familiaId: localStorage.getItem("familia-id"),
      },
      function () {
        var $this = this;
        this.setStartDate(this.state.startDate, function () {
          $this.setEndDate($this.state.endDate, function () {
            $this.getData();
            $this.atualizarMeiosPagamento();
            $this.atualizarCategorias();
            $this.atualizarOrcamentos();
            $this.obterDropdownOrcamentos();
          });
        });

        global.mostrarAjudaDaPaginaAutomaticamente(this);
      }
    );
  }

  setRedirect = () => {
    this.setState({
      redirect: true,
    });
  };

  renderRedirect = () => {
    if (this.state.redirect) {
      return <Redirect to="/auth/login" />;
    }
  };

  preparaDataParaGrid = (dataString) => {
    var data = this.moment(dataString, "YYYY-MM-DDThh:mm:ss");
    return data.format("DD/MM/YYYY");
  };

  atualizarMeiosPagamento = () => {
    //var urlMeioPagamento = global.server_api + 'api/meioPagamento/familia/' + this.state.familiaId + "/filtro";
    var urlMeioPagamento =
      global.server_api_new +
      global.apiToken +
      "/meioPagamento/familia/" +
      this.state.familiaId +
      "/filtro";

    var config = {
      headers: {
        Authorization: "bearer " + this.state.accessToken,
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Headers": "Authorization",
        "Access-Control-Allow-Methods":
          "GET, POST, OPTIONS, PUT, PATCH, DELETE",
      },
    };

    axios.post(urlMeioPagamento, {}, config).then((res) => {
      const meiosPagamento = res.data.results;
      this.setState({ meiosPagamento });
    });
  };

  atualizarOrcamentos = () => {
    //var url = global.server_api + 'api/orcamento/familia/' + this.state.familiaId + "/filtro";
    var url =
      global.server_api_new +
      global.apiToken +
      "/orcamento/familia/" +
      this.state.familiaId +
      "/filtro";

    var config = {
      headers: {
        Authorization: "bearer " + this.state.accessToken,
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Headers": "Authorization",
        "Access-Control-Allow-Methods":
          "GET, POST, OPTIONS, PUT, PATCH, DELETE",
      },
    };

    axios.post(url, {}, config).then((res) => {
      const orcamentos = res.data.results;
      this.setState({ orcamentos });
    });
  };

  atualizarCategorias = () => {
    //var urlCategoria = global.server_api + 'api/categoria/familia/' + this.state.familiaId + "/filtro";
    var urlCategoria =
      global.server_api_new +
      global.apiToken +
      "/categoria/familia/" +
      this.state.familiaId +
      "/filtro";

    var config = {
      headers: {
        Authorization: "bearer " + this.state.accessToken,
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Headers": "Authorization",
        "Access-Control-Allow-Methods":
          "GET, POST, OPTIONS, PUT, PATCH, DELETE",
      },
    };

    axios.post(urlCategoria, {}, config).then((res) => {
      const categorias = res.data.results;
      this.setState({ categorias });
    });
  };

  getData = () => {
    if (this.state.interval != null) {
      clearInterval(this.state.interval);
    }

    if (this.state.accessToken === null) {
      this.setRedirect();
      return;
    }

    this.setState({ isLoading: true });

    //var url = global.server_api + 'api/lancamento/familia/' + this.state.familiaId + '/filtro';
    var url =
      global.server_api_new +
      global.apiToken +
      "/lancamento/familia/" +
      this.state.familiaId +
      "/filtro";
    //console.log(this.state);
    var filtro = {
      CategoriaId:
        this.state.categoryId != null ? this.state.categoryId.id : null,
      MeioPagamentoId:
        this.state.meioPagamentoId != null
          ? this.state.meioPagamentoId.id
          : null,
      OrcamentoId:
        this.state.orcamentoId != null ? this.state.orcamentoId.id : null,
      DataInicial: this.state.startDate,
      DataFinal: this.state.endDate,
      Filtro: this.state.textoDigitado,
      Ordenacao:
        this.state.sortField != null ? this.state.sortField : "Data Desc",
      NumeroPagina:
        this.state.numeroPagina != null ? this.state.numeroPagina : null,
      NumeroItensPorPagina:
        this.state.numeroItensPorPagina != null
          ? this.state.numeroItensPorPagina
          : null,
    };

    var config = {
      headers: {
        Authorization: "bearer " + this.state.accessToken,
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Headers": "Authorization",
        "Access-Control-Allow-Methods":
          "GET, POST, OPTIONS, PUT, PATCH, DELETE",
      },
    };

    // console.log(filtro);

    axios.post(url, filtro, config).then((res) => {
      var totalReceitas = 0;
      var totalDespesas = 0;
      var totalInvestimentos = 0;
      var totalDividas = 0;

      if (res.data.results != null) {
        // console.log(res.data);

        res.data.results.forEach(function (item) {
          if (item.tipoOrcamento === 1) {
            totalDespesas +=
              item.valorParcela == null ? item.valor : item.valorParcela;
          } else if (item.tipoOrcamento === 2) {
            totalInvestimentos += item.valor;
          } else if (item.tipoOrcamento === 3) {
            totalDividas += item.valor;
          } else {
            totalReceitas += item.valor;
          }
        });
      }

      totalReceitas =
        "Total Receitas: " +
        totalReceitas
          .toLocaleString("pt-BR", { style: "currency", currency: "BRL" })
          .replace("R$", "");
      totalInvestimentos =
        "Total Investimentos: " +
        totalInvestimentos
          .toLocaleString("pt-BR", { style: "currency", currency: "BRL" })
          .replace("R$", "");
      totalDespesas =
        "Total Despesas: " +
        totalDespesas
          .toLocaleString("pt-BR", { style: "currency", currency: "BRL" })
          .replace("R$", "");
      totalDividas =
        "Total Dívidas: " +
        totalDividas
          .toLocaleString("pt-BR", { style: "currency", currency: "BRL" })
          .replace("R$", "");

      var sumarioExtrato = [
        totalReceitas,
        totalInvestimentos,
        totalDespesas,
        totalDividas,
      ];

      this.setState({
        sumarioExtrato: sumarioExtrato,
      });

      var data = [];

      //var $this = this;

      if (res.data.results != null) {
        res.data.results.forEach(function (item) {
          var temp = item;

          //temp.data = $this.preparaDataParaGrid(item.data);
          temp.tipoOperacao = global.operacao2[item.operacao];

          data.push(temp);
        });
      }

      const total = res.data.totalResults;
      this.setState({
        originalData: res.data.results,
        // data: data,
        isLoading: false,
        count: total,
      });

      if (sessionStorage.getItem("checkedNotCategorized") === "true") {
        let newData = res.data.results.filter((item) => {
          let itemData = `${item.statusCategorizacaoBob}`;
          return itemData.indexOf(1) > -1;
        });
        this.setState({ data: newData });
      } else {
        this.setState({ data: res.data.results });
      }
    });
  };

  editarRegistro = (lancamentoId) => {
    //ultimo campo do table é o id neste caso

    var array = this.state.originalData.filter(function (item) {
      return item.Id === lancamentoId || item.id === lancamentoId;
    });

    if (array != null && array.length === 1) {
      var itemSelecionado = array[0];
      itemSelecionado.data = this.moment(
        itemSelecionado.data,
        "YYYY-MM-DDThh:mm:ss"
      ).format("DD/MM/YYYY");

      //Especifico para lancamento (usar o storage local)
      localStorage.setItem("lancamentoEdicao", JSON.stringify(itemSelecionado));

      document.getElementById("novo-lancamento").click();
    } else {
      console.log(
        "Ou o id está no index errado do grid ou não existe o id selecionado no this.state.data"
      );
    }
  };

  Pesquisa_Filtros(tableState) {
    var Sumario = {
      Orcamento: {
        Receitas: 0,
        Despesas: 0,
        Investimentos: 0,
        Dividas: 0,
      },
    };

    tableState.displayData.forEach((x) => {
      switch (x.data[8]) {
        case 0:
          Sumario.Orcamento.Receitas += parseFloat(
            global.convertToFloat(x.data[4].props.children)
          );
          break;
        case 1:
          Sumario.Orcamento.Despesas += parseFloat(
            global.convertToFloat(x.data[4].props.children)
          );
          break;
        case 2:
          Sumario.Orcamento.Investimentos += parseFloat(
            global.convertToFloat(x.data[4].props.children)
          );
          break;
        case 3:
          Sumario.Orcamento.Dividas += parseFloat(
            global.convertToFloat(x.data[4].props.children)
          );
          break;
        default:
          break;
      }
    });

    if (
      Sumario.Orcamento.Receitas !== this.state.Sumario.Orcamento.Receitas ||
      Sumario.Orcamento.Despesas !== this.state.Sumario.Orcamento.Despesas ||
      Sumario.Orcamento.Investimentos !==
        this.state.Sumario.Orcamento.Investimentos ||
      Sumario.Orcamento.Dividas !== this.state.Sumario.Orcamento.Dividas
    ) {
      this.setState({ Sumario });
    }
  }

  removerRegistros = (rowsDeleted) => {
    var ids = [];
    var lancamentoId = [];
    var agrupamento = null;
    var temParcelado = false;
    var apenasParcelado = true;

    if (typeof rowsDeleted === "object") {
      for (var key in rowsDeleted.data) {
        lancamentoId = this.state.data[rowsDeleted.data[key].dataIndex]
          ? this.state.data[rowsDeleted.data[key].dataIndex].id
          : null;
        ids.push(lancamentoId);

        agrupamento = this.state.data[rowsDeleted.data[key].dataIndex]
          .agrupamento;

        if (!temParcelado && agrupamento != null && agrupamento !== "") {
          temParcelado = true;
        } else if (
          apenasParcelado &&
          (agrupamento === null || agrupamento === "")
        ) {
          apenasParcelado = false;
        }
      }
    } else {
      //se o lançamento foi apagado pela lixeira (é enviado o id diretamente como parâmetro)
      ids.push(rowsDeleted);

      var lancamento = this.state.data.filter(
        (lancamento) => lancamento.id === rowsDeleted
      );

      agrupamento = lancamento[0].agrupamento;

      if (!temParcelado && agrupamento != null && agrupamento !== "") {
        temParcelado = true;
      } else if (
        apenasParcelado &&
        (agrupamento === null || agrupamento === "")
      ) {
        apenasParcelado = false;
      }
    }

    // console.log(temParcelado, apenasParcelado);

    var mensagem =
      "A exclusão é um procedimento irreversível! Tem certeza que deseja excluir estes lançamentos?";

    if (apenasParcelado) {
      mensagem +=
        " A exclusão total apaga todos os lançamentos envolvidos no parcelamento/repetição. A exclusão parcial remove o lançamento marcado para exclusão e os lançamentos futuros do mesmo parcelamento/repetição";
    } else if (temParcelado) {
      mensagem +=
        " Os lançamentos parcelados / repetidos serão TOTALMENTE excluídos, incluindo os meses passados ou futuros!";
    }

    var buttons = [];

    buttons.push({
      label: apenasParcelado ? "Exclusão Total" : "Sim",
      onClick: () => {
        if (ids.length > 0) {
          //var url = global.server_api + 'api/lancamento/bulk/' + ids.join(",");
          var url =
            global.server_api_new +
            global.apiToken +
            "/lancamento/bulk/" +
            ids.join(",");

          var config = {
            headers: {
              Authorization: "bearer " + this.state.accessToken,
              "Access-Control-Allow-Origin": "*",
              "Access-Control-Allow-Headers": "Authorization",
              "Access-Control-Allow-Methods":
                "GET, POST, OPTIONS, PUT, PATCH, DELETE",
            },
          };

          axios.delete(url, {}, config).then((res) => {
            const result = res.data;

            if (result.success) {
              confirmAlert({
                title: "Informação",
                message:
                  "Por favor, aguarde enquanto o lançamento é deletado. Isso pode durar alguns segundos",
                buttons: [
                  {
                    label: "Ok",
                  },
                ],
              });
              this.getData();
            } else {
              console.log(res.data.exception);
              alert(
                "Erro ao remover lançamentos. " + res.data.exception.Message
              );
            }
          });
        }
      },
    });

    //Primeiro Botao
    if (apenasParcelado) {
      buttons.push({
        label: "Exclusão parcial",
        onClick: () => {
          if (ids.length > 0) {
            //var url = global.server_api + 'api/lancamento/bulk/currentAndFutureOnly/' + ids.join(",");
            var url =
              global.server_api_new +
              global.apiToken +
              "/lancamento/bulk/currentAndFutureOnly/" +
              ids.join(",");

            var config = {
              headers: {
                Authorization: "bearer " + this.state.accessToken,
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Headers": "Authorization",
                "Access-Control-Allow-Methods":
                  "GET, POST, OPTIONS, PUT, PATCH, DELETE",
              },
            };

            axios.delete(url, {}, config).then((res) => {
              const result = res.data;

              if (result.success) {
                this.getData();
              } else {
                console.log(res.data.exception);
                alert(
                  "Erro ao remover lançamentos. " + res.data.exception.Message
                );
              }
            });
          }
        },
      });
    }

    buttons.push({
      label: "Não",
    });

    const options = {
      title: "Exclusão de lançamentos",
      message: mensagem,
      buttons: buttons,
    };

    confirmAlert(options);

    return false;
  };

  gerarDatePicker = (id, callback, defaultDate, type) => {
    var currentDate = null;

    if (!defaultDate) {
      currentDate = new Date();
    } else {
      if (type === "filtro-inicio" && this.state.startDate != null) {
        currentDate = this.state.startDate;
      } else if (type === "filtro-fim" && this.state.endDate != null) {
        currentDate = this.state.endDate;
      } else {
        currentDate = defaultDate.toDate();
      }
    }

    return (
      <Datepicker
        locale="br"
        id={id}
        dateFormat="dd/MM/yyyy"
        selected={currentDate}
        showYearDropdown
        dateFormatCalendar="MMMM"
        scrollableYearDropdown
        yearDropdownItemNumber={5}
        onChange={(date) => callback(date)}
        customInput={<MaskedInput mask="11/11/1111" />}
      />
    );
  };

  setStartDate = (date, callback) => {
    if (date != null) {
      date.setHours(0);
      date.setMinutes(0, 0, 0);
      localStorage.setItem("dataInicio", JSON.stringify(date));
    }

    const startDate = date;

    this.setState({ startDate: startDate }, function () {
      document.getElementById("rightbar-startdate-refresh").click();

      if (callback != null) {
        callback();
      } else {
        this.getData();
      }
    });
  };

  setEndDate = (date, callback) => {
    if (date != null) {
      date.setHours(20);
      date.setMinutes(59, 59, 0);
      localStorage.setItem(
        "dataFim",
        JSON.stringify(
          date
        ) /*JSON.stringify(zonedTimeToUtc(parseISO(date), 'America/Sao_Paulo'))*/
      );
      // console.log(date);
    }

    const endDate = date;

    this.setState({ endDate }, function () {
      document.getElementById("rightbar-enddate-refresh").click();

      if (callback != null) {
        callback();
      } else {
        this.getData();
      }
    });
  };

  obterDropdownOrcamentos = () => {
    var url = `${global.server_api_new}${global.apiToken}/orcamento/familia/${this.state.familiaId}`;

    var config = {
      headers: {
        Authorization: "bearer " + this.state.accessToken,
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Headers": "Authorization",
        "Access-Control-Allow-Methods":
          "GET, POST, OPTIONS, PUT, PATCH, DELETE",
      },
    };

    axios.post(url, {}, config).then((res) => {
      let dropdown = this.state.dropdown;
      dropdown.orcamentos = res.data.results;

      this.setState({ dropdown });
    });
  };

  obterDropdownCategorias = (orcamentoId, lancamentoId) => {
    var url = `${global.server_api_new}${global.apiToken}/categoria/pororcamento/${orcamentoId}`;

    var config = {
      headers: {
        Authorization: "bearer " + this.state.accessToken,
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Headers": "Authorization",
        "Access-Control-Allow-Methods":
          "GET, POST, OPTIONS, PUT, PATCH, DELETE",
      },
    };

    axios.get(url, config).then((res) => {
      let dropdown = this.state.dropdown;
      dropdown.categorias[lancamentoId] = res.data.results;

      this.setState({ dropdown });
    });
  };

  handleOnChangeOrcamento = (item, lancamentoId) => {
    this.obterDropdownCategorias(item.value, lancamentoId);
  };

  handleOnChangeCategoria = (lancamentoId, categoriaId) => {
    var url = `${global.server_api_new}${global.apiToken}/lancamento/setacategorizacao/${lancamentoId}/categoria/${categoriaId}`;

    var config = {
      headers: {
        Authorization: "bearer " + this.state.accessToken,
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Headers": "Authorization",
        "Access-Control-Allow-Methods":
          "GET, POST, OPTIONS, PUT, PATCH, DELETE",
      },
    };

    axios.get(url, config).then((res) => {
      if (res.data.success === true) {
        this.getData();
      }
    });
  };

  handleShowNotCategorized = (value) => {
    if (this.state.originalData === undefined) return;

    this.setState({ checkedNotCategorized: value });
    sessionStorage.setItem("checkedNotCategorized", value);

    let statusFilter = 1;

    if (value) {
      let newData = this.state.originalData.filter((item) => {
        let itemData = `${item.statusCategorizacaoBob}`;
        return itemData.indexOf(statusFilter) > -1;
      });
      this.setState({ data: newData });
    } else {
      this.setState({ data: this.state.originalData });
    }
  };

  render() {
    const columns = [
      {
        name: "Controle",
        label: "Controle",
        options: {
          display: false,
          viewColumns: false,
          filter: false,
          sort: false,
          customBodyRender: (value, tableMeta) => {
            console.log('value', value)
            console.log('tableMeta', tableMeta)
            var lancamentoId = tableMeta.rowData[1];
            return (
              <div>
                <i
                  style={{ cursor: "pointer", marginLeft: 15 }}
                  onClick={(e) => {
                    this.removerRegistros(lancamentoId);
                  }}
                  className="far fa-trash-alt"
                ></i>
              </div>
            );
          },
        },
      },
      {
        name: "id",
        options: {
          display: false,
          viewColumns: false,
          filter: false,
        },
      },
      {
        name: "data",
        label: "Data",
        options: {
          display: this.state.fieldList.data,
          viewColumns: true,
          filter: false,
          sort: true,
          customBodyRender: (value, tableMeta) => {
            var dataFormatada = this.moment(
              value,
              "YYYY-MM-DDThh:mm:ss"
            ).format("DD/MM/YYYY");
            var lancamentoId = tableMeta.rowData[1];
            return (
              <div
                style={{ cursor: "pointer" }}
                onClick={() => {
                  this.editarRegistro(lancamentoId);
                }}
              >
                {dataFormatada}
              </div>
            );
          },
        },
      },
      {
        name: "descricao",
        label: "Descrição",
        options: {
          display: this.state.fieldList.descricao,
          viewColumns: true,
          filter: false,
          customBodyRender: (value, tableMeta) => {
            var lancamentoId = tableMeta.rowData[1];
            return (
              <div
                style={{ cursor: "pointer" }}
                onClick={() => {
                  this.editarRegistro(lancamentoId);
                }}
              >
                {value}
              </div>
            );
          },
        },
      },
      {
        name: "valor",
        label: "Valor",
        options: {
          display: this.state.fieldList.valor,
          viewColumns: true,
          filter: false,
          customBodyRender: (value, tableMeta) => {
            var valorReal = value;

            var lancamentoId = tableMeta.rowData[1];

            if (tableMeta.rowData[9] != null && tableMeta.rowData[9] !== "") {
              valorReal = tableMeta.rowData[9];
            }

            return (
              <span
                onClick={() => {
                  this.editarRegistro(lancamentoId);
                }}
                style={
                  tableMeta.rowData[8] === 3
                    ? { color: "#e80331", cursor: "pointer" }
                    : tableMeta.rowData[8] !== 1 && tableMeta.rowData[11] !== 1
                    ? { color: "#0e8730", cursor: "pointer" }
                    : { color: "#e80331", cursor: "pointer" }
                }
              >
                {valorReal != null
                  ? valorReal
                      .toLocaleString("pt-BR", {
                        style: "currency",
                        currency: "BRL",
                      })
                      .replace("R$", "")
                  : "0,00"}
              </span>
            );
          },
        },
      },
      {
        name: "nomeMeioPagamento",
        label: "Meio de Pagamento",
        options: {
          display: this.state.fieldList.nomeMeioPagamento,
          viewColumns: true,
          filter: true,
          customBodyRender: (value, tableMeta) => {
            var lancamentoId = tableMeta.rowData[1];
            return (
              <div
                style={{ cursor: "pointer" }}
                onClick={() => {
                  this.editarRegistro(lancamentoId);
                }}
              >
                {value}
              </div>
            );
          },
        },
      },
      {
        name: "nomeOrcamento",
        label: "Orçamento",
        options: {
          display: this.state.fieldList.nomeOrcamento,
          viewColumns: true,
          filter: true,
          customBodyRender: (value, tableMeta) => {
            const lancamentoId = tableMeta.rowData[1];
            const orcamentoPadrao = tableMeta.rowData[14];

            if (orcamentoPadrao) {
              return (
                <Select
                  placeholder="Selecione..."
                  options={this.state.dropdown.orcamentos}
                  onChange={(value) =>
                    this.handleOnChangeOrcamento(value, tableMeta.rowData[1])
                  }
                  menuPortalTarget={document.body}
                  menuPosition={"fixed"}
                />
              );
            } else {
              return (
                <div
                  style={{ cursor: "pointer" }}
                  onClick={() => {
                    this.editarRegistro(lancamentoId);
                  }}
                >
                  {value}
                </div>
              );
            }
          },
        },
      },
      {
        name: "nomeCategoria",
        label: "Categoria",
        options: {
          display: this.state.fieldList.nomeCategoria,
          viewColumns: true,
          filter: true,
          customBodyRender: (value, tableMeta) => {
            var lancamentoId = tableMeta.rowData[1];
            const categoriaPadrao = tableMeta.rowData[15];

            if (categoriaPadrao) {
              return (
                <Select
                  placeholder="Selecione..."
                  options={this.state.dropdown.categorias[tableMeta.rowData[1]]}
                  onChange={(categoria) =>
                    this.handleOnChangeCategoria(lancamentoId, categoria.value)
                  }
                  menuPortalTarget={document.body}
                  menuPosition={"fixed"}
                />
              );
            } else {
              return (
                <div
                  style={{ cursor: "pointer" }}
                  onClick={() => {
                    this.editarRegistro(lancamentoId);
                  }}
                >
                  {value}
                </div>
              );
            }
          },
        },
      },
      {
        name: "tipoOrcamento",
        options: {
          display: false,
          viewColumns: false,
          filter: false,
        },
      },
      {
        name: "valorParcela",
        options: {
          display: false,
          viewColumns: false,
          filter: false,
        },
      },
      {
        name: "agrupamento",
        label: "Parcelado?",
        options: {
          display: this.state.fieldList.agrupamento,
          viewColumns: true,
          filter: false,
          customBodyRender: (value, tableMeta) => {
            var lancamentoId = tableMeta.rowData[1];

            if (value != null && value !== "") {
              return (
                <div
                  style={{ cursor: "pointer" }}
                  onClick={() => {
                    this.editarRegistro(lancamentoId);
                  }}
                >
                  Sim<span style={{ display: "none" }}>{value}</span>
                </div>
              );
            } else {
              return (
                <span
                  style={{ cursor: "pointer" }}
                  onClick={() => {
                    this.editarRegistro(lancamentoId);
                  }}
                >
                  Não
                </span>
              );
            }
          },
        },
      },
      {
        name: "operacao",
        options: {
          display: false,
          viewColumns: false,
          filter: false,
        },
      },
      {
        name: "tipoOperacao",
        label: "Tipo de Operação",
        options: {
          display: this.state.fieldList.tipoOperacao,
          viewColumns: true,
          filter: true,
          customBodyRender: (value, tableMeta) => {
            var lancamentoId = tableMeta.rowData[1];
            return (
              <div
                style={{ cursor: "pointer" }}
                onClick={() => {
                  this.editarRegistro(lancamentoId);
                }}
              >
                {value}
              </div>
            );
          },
        },
      },
      {
        name: "statusCategorizacaoBob",
        options: {
          display: false,
          viewColumns: false,
          filter: false,
        },
      },
      {
        name: "orcamentoPadrao",
        options: {
          display: false,
          viewColumns: false,
          filter: false,
        },
      },
      {
        name: "categoriaPadrao",
        options: {
          display: false,
          viewColumns: false,
          filter: false,
        },
      }
    ];

    const { data, isLoading } = this.state;
    console.log(data)

    const FixarDesfixarColuna = (coluna, valor) => {
      switch (coluna) {
        case "Data":
          this.setState({ fieldList: { Data: valor } });
          break;
        case "Descricao":
          this.setState({ fieldList: { Descricao: valor } });
          break;
        case "Valor":
          this.setState({ fieldList: { Valor: valor } });
          break;
        case "NomeMeioPagamento":
          this.setState({ fieldList: { NomeMeioPagamento: valor } });
          break;
        case "NomeCategoria":
          this.setState({ fieldList: { NomeCategoria: valor } });
          break;
        case "NomeOrcamento":
          this.setState({ fieldList: { NomeOrcamento: valor } });
          break;
        case "Agrupamento":
          this.setState({ fieldList: { Agrupamento: valor } });
          break;
        case "TipoOperacao":
          this.setState({ fieldList: { TipoOperacao: valor } });
          break;
        default:
          break;
      }
    };

    const optionsFooter = {
      onColumnViewChange: (changedColumn, action) => {
        switch (action) {
          case "add":
            console.log("FIXAR");
            FixarDesfixarColuna(changedColumn, true);
            break;
          case "remove":
            console.log("DESFIXAR");
            FixarDesfixarColuna(changedColumn, false);
            break;
          default:
            break;
        }
      },
      onTableChange: (action, tableState) => {
        switch (action) {
          case "expandRow":
            var expandedIndexes = tableState.expandedRows.data.map(function (
              value
            ) {
              return value.dataIndex;
            });
            this.setState({ expandedIndexes: expandedIndexes });
            break;
          default:
            break;
        }

        this.Pesquisa_Filtros(tableState);
      },
      textLabels: global.textLabels,
      filter: true,
      filterType: "dropdown",
      responsive: "stacked",
      print: false,
      download: false,
      searchText: this.state.textoDigitado,
      sort: true,
      pagination: false,
      onRowsDelete: (rowsDeleted) => {
        this.removerRegistros(rowsDeleted);
        return false;
      },
      customFooter: (
        count,
        page,
        rowsPerPage,
        changeRowsPerPage,
        changePage,
        textLabels
      ) => {
        var sumarioLinha1 = [
          `Total Receitas: ${this.state.Sumario.Orcamento.Receitas.toLocaleString(
            "pt-BR",
            { style: "currency", currency: "BRL" }
          )}`,
          `Total Despesas: ${this.state.Sumario.Orcamento.Despesas.toLocaleString(
            "pt-BR",
            { style: "currency", currency: "BRL" }
          )}`,
        ];

        var sumarioLinha2 = [
          `Total Investimentos: ${this.state.Sumario.Orcamento.Investimentos.toLocaleString(
            "pt-BR",
            { style: "currency", currency: "BRL" }
          )}`,
          `Total Dívidas: ${this.state.Sumario.Orcamento.Dividas.toLocaleString(
            "pt-BR",
            { style: "currency", currency: "BRL" }
          )}`,
        ];

        return (
          <CustomFooter
            count={count}
            page={page}
            rowsPerPage={rowsPerPage}
            changeRowsPerPage={changeRowsPerPage}
            changePage={changePage}
            textLabels={textLabels}
            data7={sumarioLinha1}
            data8={sumarioLinha2}
            hasPagination={false}
          />
        );
      },
    };

    const TooltipRestore = withStyles((theme) => ({
      tooltip: {
        backgroundColor: "#888888",
        color: "white",
        fontSize: 12,
        fontWeight: "bold",
        fontFamily: "Open Sans",
      },
    }))(Tooltip);

    return (
      <div
        id="tela-cadastro-lancamento"
        style={{ padding: "0 5%", marginTop: 20 }}
      >
        <div className="home-summary-top">
          <Row>
            <Col lg="12" xl="12" style={{ marginBottom: "15px" }}>
              <Card>
                <CardHeader
                  className="border-0"
                  style={{ padding: "30px 15px 30px 0" }}
                >
                  <Row className="align-items-center">
                    <Col lg="2" xl="2">
                      <img
                        src={require("../../assets/img/theme/icone-calendario.png")}
                        alt="Meios de Pagamento"
                        className="before-title-img"
                      />
                      <h3 className="mb-0 chart-title">Extrato</h3>
                    </Col>
                    <Col lg="10" xl="10">
                      <div
                        className="nav-link-icon"
                        style={{ display: "flex" }}
                      >
                        <h3
                          className="mb-0 chart-title"
                          style={{ marginRight: "10px" }}
                        >
                          Período
                        </h3>
                        {this.gerarDatePicker(
                          "data-inicial",
                          this.setStartDate,
                          this.moment().startOf("month"),
                          "filtro-inicio"
                        )}
                        {this.gerarDatePicker(
                          "data-final",
                          this.setEndDate,
                          this.moment().endOf("month"),
                          "filtro-fim"
                        )}
                        <TooltipRestore
                          disableFocusListener
                          disableTouchListener
                          title="Clique para ver apenas os lançamentos do mês atual"
                        >
                          <RestoreDate
                            style={{ cursor: "pointer" }}
                            onClick={() => {
                              this.setStartDate(
                                this.moment().startOf("month").toDate()
                              );
                              this.setEndDate(
                                this.moment().endOf("month").toDate()
                              );
                            }}
                          />
                        </TooltipRestore>
                        <TooltipRestore
                          disableFocusListener
                          disableTouchListener
                          title="Clique para ver os lançamentos dos últimos três meses"
                        >
                          <Restore3Months
                            style={{ cursor: "pointer" }}
                            onClick={() => {
                              this.setStartDate(
                                this.moment()
                                  .subtract(3, "months")
                                  .startOf("month")
                                  .toDate()
                              );
                              this.setEndDate(
                                this.moment()
                                  .subtract(1, "months")
                                  .endOf("month")
                                  .toDate()
                              );
                            }}
                          />
                        </TooltipRestore>
                      </div>
                    </Col>
                  </Row>
                </CardHeader>
              </Card>
            </Col>
          </Row>
        </div>

        <MuiThemeProvider theme={this.getMuiTheme()}>
          <MUIDataTable
            title={
              <div
                style={{ float: "left" }}
                className="gestao-planos-grid-title"
              >
                <div
                  style={{
                    display: "flex",
                    justifyContent: "center",
                    alignContent: "center",
                  }}
                >
                  {/* <span style={{ fontWeight: "600" }}>
                    Mostrar não categorizados?
                  </span>
                  &nbsp;
                  <Switch
                    onChange={this.handleShowNotCategorized}
                    checked={this.state.checkedNotCategorized}
                  /> */}
                </div>
              </div>
            }
            data={data}
            columns={columns}
            options={optionsFooter}
          />
        </MuiThemeProvider>

        <Rightbar
          {...this.props}
          logo={{
            innerLink: "/admin/index",
            imgSrc: require("../../assets/img/brand/logo-nova-branca.png"),
            imgAlt: "...",
          }}
          id="right-sidenav-main"
          position="fixed-right"
          menuItens={[
            "sumarioPrevistoRealizadoGeral",
            "gastosPorOrcamento",
            "gastosPorCategoria",
          ]}
        />

        {this.renderRedirect()}
      </div>
    );
  }
}

export default Lancamentos;
