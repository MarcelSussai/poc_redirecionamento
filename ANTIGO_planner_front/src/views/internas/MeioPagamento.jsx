import React from "react";
import Select from "react-select";
import axios from "axios";
import { confirmAlert } from "react-confirm-alert";
import "react-confirm-alert/src/react-confirm-alert.css";
import SkyLight from "react-skylight";
import $ from "jquery";
import { RadioGroup, Radio } from "react-radio-group";
import MUIDataTable from "mui-datatables";

// reactstrap components
import { Card, CardHeader, Container, Row, Col, Table } from "reactstrap";
import { createMuiTheme, MuiThemeProvider } from "@material-ui/core/styles";

class MeioPagamento extends React.Component {
  moment = require("moment");
  ready = {};

  state = {
    activePage: 1,
    itensPerPage: 12,
    pageRangeDisplayed: 5,
    totalItemsCount: 450,
    sortField: null,
    registros: [],
    carregamentoInicial: false,
    interval: null,
    textoDigitado: "",
    donos: [],
    donosCombo: [],
    contasBob: [],
    contasBobCombo: [],
    bancos: [],
    bancosCombo: [],
    tiposMeioPagamento: [],
    bandeiras: [],
  };

  camposParaOrdenacao = [
    { value: "Nome", label: "Nome" },
    { value: "Nome Desc", label: "Nome Decrescente" },
    { value: "Tipo", label: "Tipo de Meio de Pagamento" },
    { value: "Dono", label: "Responsável pelo Meio de Pagamento" },
    { value: "Banco", label: "Banco da Conta Corrente" },
    { value: "Bandeira", label: "Bandeira do Cartão" },
  ];

  getMuiTheme = () =>
    createMuiTheme({
      overrides: {
        MuiTableRow: {
          root: {
            "&:nth-of-type(odd)": {
              backgroundColor: "#f8f9fe",
            },
          },
        },
        MUIDataTableSelectCell: {
          fixedHeaderCommon: {
            backgroundColor: "unset",
          },
        },
        MuiTableCell: {
          root: {
            fontFamily: "unset",
          },
          footer: {
            color: "#FFF",
            fontWeight: 700,
          },
        },
        MUIDataTableHeadCell: {
          fixedHeaderYAxis: {
            fontWeight: 700,
          },
        },
        MuiPaper: {
          root: {
            overflow: "hidden",
            margin: "10px 40px",
          },
        },
        MuiToolbar: {
          regular: {
            backgroundColor: "#e4eaf4",
          },
        },
        MUIDataTablePagination: {
          root: {
            "&:last-child": {
              margin: "0px 24px 0px 24px",
              padding: 0,
            },
          },
        },
      },
    });

  atualizarDonos = () => {
    //var url = global.server_api + 'api/pessoa/familia/' + this.state.familiaId+'/filtro';
    var url =
      global.server_api_new +
      global.apiToken +
      "/pessoa/familia/" +
      this.state.familiaId +
      "/filtro";

    var config = {
      headers: {
        Authorization: "bearer " + this.state.accessToken,
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Headers": "Authorization",
        "Access-Control-Allow-Methods":
          "GET, POST, OPTIONS, PUT, PATCH, DELETE",
      },
    };

    axios.post(url, {}, config).then((res) => {
      const donos = res.data.results;
      var donosCombo = [
        {
          label: "Selecione...",
          value: null,
        },
      ];

      if (donos != null) {
        donos.forEach((mp) => {
          donosCombo.push({
            value: mp,
            label: mp.nome + " " + mp.sobrenome,
          });
        });
      }

      this.setState({ donos });
      this.setState({ donosCombo });
    });
  };

  atualizarBancos = () => {
    //var url = global.server_api + 'api/banco/filtro';
    var url = global.server_api_new + global.apiToken + "/banco/filtro";

    var config = {
      headers: {
        Authorization: "bearer " + this.state.accessToken,
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Headers": "Authorization",
        "Access-Control-Allow-Methods":
          "GET, POST, OPTIONS, PUT, PATCH, DELETE",
      },
    };

    axios.post(url, {}, config).then((res) => {
      const bancos = res.data.results;
      var bancosCombo = [
        {
          label: "Selecione...",
          value: null,
        },
      ];

      if (bancos != null) {
        bancos.forEach((mp) => {
          bancosCombo.push({
            value: mp,
            label: mp.nome,
          });
        });
      }

      this.setState({ bancos });
      this.setState({ bancosCombo });
    });
  };

  atualizarContasBob = () => {
    if (
      this.state.tipoMeioPagamento === undefined ||
      this.state.tipoMeioPagamento == null
    ) {
      return;
    }

    //var url = global.server_api + 'api/banco/filtro';
    var url = `${global.server_api_new}${global.apiToken}/bob/mpgtoctbob/obtercontas/${this.state.familiaId}/tipo/${this.state.tipoMeioPagamento}`;

    var config = {
      headers: {
        Authorization: "bearer " + this.state.accessToken,
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Headers": "Authorization",
        "Access-Control-Allow-Methods":
          "GET, POST, OPTIONS, PUT, PATCH, DELETE",
      },
    };

    axios.get(url, config).then((res) => {
      const contasBob = res.data.result;

      var contasBobCombo = [
        {
          label: "Selecione...",
          value: null,
        },
      ];

      if (contasBob != null) {
        contasBob.forEach((mp) => {
          contasBobCombo.push({
            value: mp,
            label: mp.nomeConta,
          });
        });
      }

      this.setState({ contasBob });
      this.setState({ contasBobCombo });
    });
  };

  mapearTipoPeloId = (id) => {
    var tipoEncontrado = this.state.tiposMeioPagamento.filter(function (item) {
      return item.value === id;
    });
    return tipoEncontrado != null && tipoEncontrado.length > 0
      ? tipoEncontrado[0].label
      : "";
  };

  montarDropDown = (itens, id, changeMethod, selectedValue, type) => (
    <Select
      id={id}
      onChange={changeMethod}
      className="select-component"
      options={itens}
      defaultValue={itens != null ? itens[0] : ""}
      placeholder="Selecione..."
      value={
        itens != null && selectedValue != null
          ? itens.find((op) => {
              if (type === "enum") {
                return (
                  (op.value != null ? op.value.toString() : null) ===
                  (selectedValue != null ? selectedValue.toString() : null)
                );
              } else if (op.value != null && selectedValue != null) {
                return op.value.id === selectedValue.id;
              }
              return false;
            })
          : null
      }
    />
  );

  mostrarCamposParaOrdenacao = (camposParaOrdenacao) =>
    this.montarDropDown(
      camposParaOrdenacao,
      "selecao-ordenacao",
      this.mudarOrdenacao,
      null,
      null
    );

  mudarOrdenacao = (selectedOption) => {
    const sortField = selectedOption.value;

    this.setState({ sortField }, function () {
      this.atualizarRegistros();
    });
  };

  atualizarRegistros = () => {
    if (this.state.interval != null) {
      clearInterval(this.state.interval);
    }

    //var url = global.server_api + 'api/meioPagamento/familia/'+this.state.familiaId+'/filtro';
    var url =
      global.server_api_new +
      global.apiToken +
      "/meioPagamento/familia/" +
      this.state.familiaId +
      "/filtro";

    var filtro = {
      Filtro: this.state.textoDigitado,
      Ordenacao: this.state.sortField,
      TipoMeioPagamento:
        this.state.tipoMeioPagamentoFiltro != null
          ? this.state.tipoMeioPagamentoFiltro.value
          : null,
      Banco:
        this.state.bancoFiltro != null
          ? parseInt(this.state.bancoFiltro.id)
          : null,
      Dono:
        this.state.donoFiltro != null
          ? parseInt(this.state.donoFiltro.id)
          : null,
    };

    var config = {
      headers: {
        Authorization: "bearer " + this.state.accessToken,
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Headers": "Authorization",
        "Access-Control-Allow-Methods":
          "GET, POST, OPTIONS, PUT, PATCH, DELETE",
      },
    };

    var currentScroll = [
      document.documentElement.scrollLeft || document.body.scrollLeft,
      document.documentElement.scrollTop || document.body.scrollTop,
    ];

    global.spinnerShow($);

    axios.post(url, filtro, config).then((res) => {
      const registros = res.data.results;
      global.spinnerHide($, currentScroll);
      this.setState({ registros });
    });
  };

  confirmaExcluir = (id) => {
    const options = {
      title: "Exclusão de Registro",
      message:
        "Tem certeza que deseja excluir este Registro? Este procedimento é irreversível!",
      buttons: [
        {
          label: "Sim",
          onClick: () => this.excluir(id),
        },
        {
          label: "Não",
        },
      ],
    };

    confirmAlert(options);
  };

  excluir = (id) => {
    //var url = global.server_api + 'api/meioPagamento/' + id;
    var url = global.server_api_new + global.apiToken + "/meioPagamento/" + id;

    var config = {
      headers: {
        Authorization: "bearer " + this.state.accessToken,
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Headers": "Authorization",
        "Access-Control-Allow-Methods":
          "GET, POST, OPTIONS, PUT, PATCH, DELETE",
      },
    };

    axios.delete(url, {}, config).then((res) => {
      const result = res.data;

      if (result.success && result.affectedResults > 0) {
        window.location.reload();
      } else {
        console.log(res.data.exception);
        alert("Erro ao remover registro. " + res.data.exception.Message);
      }
    });
  };

  editar = (id) => {
    var registroEdicao = this.state.registros.filter(function (item) {
      return item.id === id;
    });

    if (registroEdicao != null && registroEdicao.length > 0) {
      registroEdicao = registroEdicao[0];
      //Seta item edicao
      this.setState(
        {
          nome: registroEdicao.nome,
          tipoMeioPagamento: registroEdicao.tipoMeioPagamento,
          banco: registroEdicao.banco,
          dono: registroEdicao.dono,
          bandeira: registroEdicao.bandeira,
          bandeiraPrePago: registroEdicao.bandeiraPrePago,
          diaVencimento: registroEdicao.diaVencimento,
          agencia: registroEdicao.agencia,
          conta: registroEdicao.conta,
          id: registroEdicao.id,
        },
        function () {
          //Abre modal
          this.camposMeioPagamento(registroEdicao.tipoMeioPagamento + "");
          global.showModal(this.modal);
        }
      );
    }
  };

  filtroPorTexto = (valorDigitado) => {
    const textoDigitado = valorDigitado.target.value;

    this.setState({ textoDigitado }, function () {
      if (this.state.interval != null) {
        clearInterval(this.state.interval);
      }

      const $this = this;

      const interval = setInterval(function () {
        $this.atualizarRegistros();
      }, 1000);

      this.setState({ interval });
    });
  };

  limparSelecao = () => {
    console.log("limpando");
    this.setState({
      id: null,
      nome: "",
      tipoMeioPagamento: null,
      banco: null,
      dono: null,
      bandeira: null,
      bandeiraPrePago: "",
      diaVencimento: "",
      agencia: "",
      conta: "",
    });
  };

  salvar = () => {

    let contaBob = this.state.contaBob == "" ? "": this.state.contaBob.contaBobId;

    var item = {
      nome: this.state.nome,
      tipoMeioPagamento: this.state.tipoMeioPagamento,
      bancoId: this.state.banco != null ? this.state.banco.id : null,
      pessoaId: this.state.dono != null ? this.state.dono.id : null,
      bandeira:
        this.state.bandeira != null ? parseInt(this.state.bandeira) : null,
      bandeiraPrePago: this.state.bandeiraPrePago,
      diaVencimento: this.state.diaVencimento,
      agencia: this.state.agencia,
      conta: this.state.conta,
      familiaId:
        this.state.familiaId != null ? parseInt(this.state.familiaId) : null,
      empresaId:
        this.state.empresaId != null ? parseInt(this.state.empresaId) : null,
      bobConta: this.state.tipoMeioPagamento !== 0 ? contaBob : "",
    };
    //console.log(JSON.stringify(this.state.contaBob));

    //var url = global.server_api + 'api/meioPagamento' + (this.state.id != null ? "/" + this.state.id: "");
    var url =
      global.server_api_new +
      global.apiToken +
      "/meioPagamento" +
      (this.state.id != null ? "/" + this.state.id : "");

    var config = {
      headers: {
        Authorization: "bearer " + this.state.accessToken,
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Headers": "Authorization",
        "Access-Control-Allow-Methods":
          "GET, POST, OPTIONS, PUT, PATCH, DELETE",
      },
    };

    var currentScroll = [
      document.documentElement.scrollLeft || document.body.scrollLeft,
      document.documentElement.scrollTop || document.body.scrollTop,
    ];

    global.spinnerShow($);

    axios.post(url, item, config).then((res) => {
      global.spinnerHide($, currentScroll);
      if (res.data.success === true) {
        this.limparSelecao();
        window.location.reload();
      } else {
        console.log(res.data.exception);
        const options = {
          title: "Erro ao Salvar Registro",
          message: res.data.exception.Message,
          buttons: [
            {
              label: "Ok",
            },
          ],
        };

        confirmAlert(options);
      }
    });
  };

  componentDidMount() {
    var bandeiras = [{ value: null, label: "Selecione..." }];

    global.bandeira.forEach(function (value, index) {
      bandeiras.push({
        value: index,
        label: value,
        image: (value + ".png").toLowerCase(),
      });
    });

    var tiposMeioPagamento = [{ value: null, label: "Selecione..." }];

    global.tipoMeioPagamento.forEach(function (value, index) {
      tiposMeioPagamento.push({
        value: index,
        label: value,
      });
    });

    this.setState(
      {
        accessToken: localStorage.getItem("access-token"),
        empresaId: localStorage.getItem("empresa-id"),
        familiaId: localStorage.getItem("familia-id"),
        bandeiras: bandeiras,
        tiposMeioPagamento: tiposMeioPagamento,
        contaBob: "",
      },
      function () {
        this.atualizarRegistros();
        this.atualizarDonos();
        this.atualizarBancos();
        this.atualizarContasBob();

        global.mostrarAjudaDaPaginaAutomaticamente(this);
      }
    );
  }

  camposMeioPagamento = (tipo) => {
    if (tipo === null || tipo === "0" || tipo === 0) {
      $(".contacorrente").hide();
      $(".cartaocredito").hide();
      $(".cartaoprepago").hide();
    } else if (tipo === "1" || tipo === 1) {
      $(".contacorrente").hide();
      $(".cartaocredito").hide();
      $(".cartaoprepago").show();
    } else if (tipo === "2" || tipo === 2) {
      $(".contacorrente").hide();
      $(".cartaoprepago").hide();
      $(".cartaocredito").show();
    } else if (tipo === "3" || tipo === 3) {
      $(".cartaocredito").hide();
      $(".cartaoprepago").hide();
      $(".contacorrente").show();
    }
  };

  mudarTipoMeioPagamento = async (tipoSelecionado) => {
    this.camposMeioPagamento(
      tipoSelecionado != null ? tipoSelecionado.value : null
    );
    await this.setState({
      tipoMeioPagamento: tipoSelecionado != null ? tipoSelecionado.value : null,
    });

    this.atualizarContasBob();
  };

  mudarTipoMeioPagamentoFiltro = (tipoSelecionado) => {
    this.setState({ tipoMeioPagamentoFiltro: tipoSelecionado }, function () {
      this.atualizarRegistros();
    });
  };

  mostrarLista = () => {
    return (
      <>
        <Card className="shadow list-entries">
          <Table>
            <th width="15"></th>
            <th width="15"></th>
            <th>Nome</th>
            <th>Tipo</th>
            <th>Dono</th>
            {this.state.registros != null &&
              this.mostrarDados(this.state.registros)}
          </Table>
        </Card>
      </>
    );
  };

  mostrarDados = (registros) => {
    if (registros == null) {
      return <></>;
    }

    return registros.map((item, key) => {
      return (
        <>
          <tr key={key}>
            <td>
              <div onClick={() => this.confirmaExcluir(item.id)}>
                <i className="far fa-trash-alt"></i>
              </div>
            </td>
            <td>
              <div onClick={() => this.editar(item.id)}>
                <i className="fas fa-info"></i>
              </div>
            </td>
            <td onClick={() => this.editar(item.id)}>{item.nome}</td>
            <td>{this.mapearTipoPeloId(item.tipoMeioPagamento)}</td>
            <td onClick={() => this.editar(item.id)}>
              {item.dono.nome} {item.dono.sobrenome}
            </td>
          </tr>
        </>
      );
    });
  };

  mudarDono = (selectedOption) => {
    const dono = selectedOption.value;
    this.setState({ dono });
  };

  mudarContaBob = (selectedOption) => {
    const contaBob = selectedOption.value;
    this.setState({ contaBob });
  };

  mudarDonoFiltro = (selectedOption) => {
    const donoFiltro = selectedOption.value;

    this.setState({ donoFiltro }, function () {
      this.atualizarRegistros();
    });
  };

  mudarBanco = (selectedOption) => {
    const banco = selectedOption.value;
    this.setState({ banco });
  };

  mudarBancoFiltro = (selectedOption) => {
    const bancoFiltro = selectedOption.value;

    this.setState({ bancoFiltro }, function () {
      this.atualizarRegistros();
    });
  };

  mudarBandeira = (selectedOption) => {
    const bandeira = selectedOption;
    this.setState({ bandeira });
  };

  mudarBandeiraFiltro = (selectedOption) => {
    const bandeiraFiltro = selectedOption.value;

    this.setState({ bandeiraFiltro }, function () {
      this.atualizarRegistros();
    });
  };

  preCarregarParaEdicao = () => {
    if (this.state.id == null) {
      this.mudarTipoMeioPagamento(null);
    }
  };

  mostraOpcoesBandeiras = () => {
    return this.state.bandeiras.map((item, key) => {
      if (item.image == null) return <></>;

      return (
        <div
          key={key}
          style={{ float: "left", marginRight: "5px", marginBottom: "10px" }}
        >
          <Radio value={item.value} />
          <img
            src={require("../../assets/img/theme/" + item.image)}
            alt={item.label}
            style={{ width: "30px", marginLeft: "5px" }}
          />
        </div>
      );
    });
  };

  mostrarPopup = () => (
    <div>
      <SkyLight
        ref={(ref) => (this.modal = ref)}
        transitionDuration={0}
        beforeOpen={this.preCarregarParaEdicao}
        afterClose={this.limparSelecao}
        title={
          <>
            <div className="pop-up-title">
              <i className="ni ni-check-bold"></i>{" "}
              <h2>Cadastro de Meio de Pagamento</h2>
            </div>
          </>
        }
      >
        <Row>
          <Col lg="6" xl="6">
            <div>
              <label>Tipo *</label>
              {this.montarDropDown(
                this.state.tiposMeioPagamento,
                "select-tipo",
                this.mudarTipoMeioPagamento,
                this.state.tipoMeioPagamento,
                "enum"
              )}
            </div>
          </Col>
          <Col lg="6" xl="6">
            <div>
              <label>Nome *</label>
              <input
                type="text"
                id="nome"
                value={this.state.nome}
                onChange={(e) => this.setState({ nome: e.target.value })}
              ></input>
            </div>
          </Col>
          <Col lg="6" xl="6">
            <div>
              <label>Dono *</label>
              {this.montarDropDown(
                this.state.donosCombo,
                "select-dono",
                this.mudarDono,
                this.state.dono,
                "model"
              )}
            </div>
          </Col>
          <Col lg="6" xl="6" className="contacorrente">
            <div>
              <label>Banco *</label>
              {this.montarDropDown(
                this.state.bancosCombo,
                "select-banco",
                this.mudarBanco,
                this.state.banco,
                "model"
              )}
            </div>
          </Col>
          <Col lg="6" xl="6" className="contacorrente">
            <div>
              <label>Agência</label>
              <input
                type="text"
                id="agencia"
                value={this.state.agencia}
                onChange={(e) => this.setState({ agencia: e.target.value })}
              ></input>
            </div>
          </Col>
          <Col lg="6" xl="6" className="contacorrente">
            <div>
              <label>Conta</label>
              <input
                type="text"
                id="agencia"
                value={this.state.conta}
                onChange={(e) => this.setState({ conta: e.target.value })}
              ></input>
            </div>
          </Col>
          <Col lg="6" xl="6" className="cartaocredito">
            <div>
              <label>Bandeira *</label>
              <RadioGroup
                name="bandeiras"
                selectedValue={this.state.bandeira}
                onChange={this.mudarBandeira}
              >
                {this.mostraOpcoesBandeiras()}
              </RadioGroup>
            </div>
          </Col>
          <Col lg="6" xl="6" className="cartaocredito">
            <div>
              <label>Banco Emissor</label>
              {this.montarDropDown(
                this.state.bancosCombo,
                "select-banco",
                this.mudarBanco,
                this.state.banco,
                "model"
              )}
            </div>
          </Col>
          <Col lg="6" xl="6" className="cartaocredito">
            <div>
              <label>Dia Vencimento *</label>
              <input
                type="number"
                id="vencimento"
                value={this.state.diaVencimento}
                onChange={(e) =>
                  this.setState({ diaVencimento: e.target.value })
                }
              ></input>
            </div>
          </Col>
          <Col lg="6" xl="6" className="cartaoprepago">
            <div>
              <label>Bandeira Pré-Pago *</label>
              <input
                type="text"
                id="bandeira-pre-pago"
                value={this.state.bandeiraPrePago}
                onChange={(e) =>
                  this.setState({ bandeiraPrePago: e.target.value })
                }
              ></input>
            </div>
          </Col>
          {/* <Col lg="6" xl="6" className="contacorrente cartaocredito">
            <div>
              <label>Vista Open Banking</label>
              {this.montarDropDown(
                this.state.contasBobCombo,
                "select-contaBob",
                this.mudarContaBob,
                this.state.contaBob,
                "model"
              )}
            </div>
          </Col> */}
          <Col lg="12" xl="12">
            <button
              id="salvar-lancamento"
              className="featured-button"
              onClick={() => this.salvar()}
              style={{
                backgroundImage:
                  "url(" +
                  require("../../assets/img/theme/botao-destaque.png") +
                  ")",
              }}
            >
              Salvar
            </button>
          </Col>
        </Row>
      </SkyLight>
    </div>
  );

  removerRegistros = (rowsDeleted) => {
    const options = {
      title: "Exclusão de Meios de Pagamento",
      message:
        "Confirma procedimento de exclusão? Este procedimento é irreversível!",
      buttons: [
        {
          label: "Sim",
          onClick: () => {
            var ids = [];

            for (var key in rowsDeleted.data) {
              ids.push(
                this.state.registros[rowsDeleted.data[key].dataIndex].id
              );
            }

            if (ids.length > 0) {
              //var url = global.server_api + 'api/meiopagamento/bulk/' + ids.join(",");
              var url =
                global.server_api_new +
                global.apiToken +
                "/meiopagamento/bulk/" +
                ids.join(",");

              var config = {
                headers: {
                  Authorization: "bearer " + this.state.accessToken,
                  "Access-Control-Allow-Origin": "*",
                  "Access-Control-Allow-Headers": "Authorization",
                  "Access-Control-Allow-Methods":
                    "GET, POST, OPTIONS, PUT, PATCH, DELETE",
                },
              };

              axios.delete(url, {}, config).then((res) => {
                const result = res.data;

                if (result.success && result.affectedResults > 0) {
                  this.atualizarBancos();
                } else {
                  alert("Erro ao remover. " + res.data.exception.Message);
                  console.log(res.data.exception);
                }
              });
            }
          },
        },
        {
          label: "Não",
        },
      ],
    };

    confirmAlert(options);

    return false;
  };

  mostrarListaMUI = () => {
    console.log(this.state.registros);
    const options = {
      textLabels: global.textLabels,
      filter: true,
      filterType: "dropdown",
      responsive: "stacked",
      print: false,
      download: false,
      rowsPerPage: 15,
      onRowsDelete: (rowsDeleted) => {
        this.removerRegistros(rowsDeleted);
        return false;
      },
      onRowClick: (rowData) => {
        this.editar(rowData[0]);
      },
    };
    const columns = [
      {
        name: "id",
        options: {
          display: false,
          viewColumns: false,
          filter: false,
        },
      },
      {
        name: "nome",
        label: "Nome",
      },
      {
        name: "tipoMeioPagamento",
        label: "Tipo",
        options: {
          display: true,
          viewColumns: true,
          filter: true,
          customBodyRender: (value) => {
            if (value != null) {
              return <>{this.mapearTipoPeloId(value)}</>;
            }
            return <></>;
          },
        },
      },
      {
        name: "nomeDono",
        label: "Dono",
      },
    ];

    return (
      <div style={{ width: "100%" }}>
        <MuiThemeProvider theme={this.getMuiTheme()}>
          <MUIDataTable
            columns={columns}
            data={this.gerarDados(this.state.registros)}
            options={options}
          />
        </MuiThemeProvider>
      </div>
    );
  };

  gerarDados = (dados) => {
    if (dados == null) return [];
    return dados.map((item) => {
      return {
        id: item.id,
        nome: item.nome,
        tipoMeioPagamento: item.tipoMeioPagamento,
        nomeDono: `${item.dono.nome} ${item.dono.sobrenome}`,
      };
    });
  };

  render() {
    return (
      <>
        <Container fluid>
          <div className="home-summary-top">
            <Row>
              <Col lg="12" xl="12" style={{ marginBottom: "15px" }}>
                <div>
                  <Card>
                    <CardHeader className="border-0">
                      <Row className="align-items-center">
                        <div className="col">
                          <img
                            src={require("../../assets/img/theme/icone-calendario.png")}
                            alt="Meios de Pagamento"
                            className="before-title-img"
                          />
                          <h3 className="mb-0 chart-title">
                            Meios de Pagamento
                          </h3>
                        </div>
                        <div className="new-entry" style={{ float: "right" }}>
                          <div
                            className="nav-link-icon featured-button"
                            style={{
                              backgroundImage:
                                "url(" +
                                require("../../assets/img/theme/botao-destaque.png") +
                                ")",
                            }}
                            onClick={() => global.showModal(this.modal)}
                          >
                            <span className="nav-link-inner--text">
                              Novo Meio de Pagto
                            </span>
                          </div>
                        </div>
                      </Row>
                    </CardHeader>
                  </Card>
                </div>
              </Col>
              {/* <Col lg="6" md="6" className="filterAndOnder">
                <label>Tipo </label>
                {this.montarDropDown(this.state.tiposMeioPagamento, "select-tipo-filtro", this.mudarTipoMeioPagamentoFiltro, null, null)}
              </Col>
              <Col lg="6" md="6" className="filterAndOnder">
                <label>Ordenar </label>
                {this.mostrarCamposParaOrdenacao(this.camposParaOrdenacao)}
              </Col>
              <Col lg="6" md="6" className="filterAndOnder text-field">
                <label>Filtrar </label><input type="text" onKeyUp={this.filtroPorTexto}/>
              </Col> */}
            </Row>
            <Row className="entry-list">{this.mostrarListaMUI()}</Row>
          </div>
        </Container>
        {this.mostrarPopup()}
      </>
    );
  }
}

export default MeioPagamento;
