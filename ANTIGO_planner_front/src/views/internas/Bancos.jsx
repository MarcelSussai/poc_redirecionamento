import React from "react";
import Select from "react-select";
import axios from "axios";
import MUIDataTable from "mui-datatables";
import { confirmAlert } from "react-confirm-alert";
import "react-confirm-alert/src/react-confirm-alert.css";
import SkyLight from "react-skylight";

// reactstrap components
import { Card, CardHeader, Container, Row, Col, Table } from "reactstrap";

class Bancos extends React.Component {
  moment = require("moment");

  state = {
    activePage: 1,
    itensPerPage: 12,
    pageRangeDisplayed: 5,
    totalItemsCount: 450,
    sortField: null,
    bancos: [],
    carregamentoInicial: false,
    interval: null,
    textoDigitado: "",
  };

  camposParaOrdenacao = [
    { value: "Nome", label: "Nome" },
    { value: "Nome Desc", label: "Nome Decrescente" },
  ];

  montarDropDown = (itens, id, changeMethod, component) => (
    <Select
      id={id}
      onChange={() => changeMethod}
      className="select-component"
      options={itens}
      defaultValue={itens != null ? itens[0] : ""}
      component={component != null ? component : null}
    />
  );

  mostrarCamposParaOrdenacao = (camposParaOrdenacao) =>
    this.montarDropDown(
      camposParaOrdenacao,
      "selecao-ordenacao",
      this.mudarOrdenacao,
      null
    );

  mudarOrdenacao = (selectedOption) => {
    const sortField = selectedOption.value;

    this.setState({ sortField }, function () {
      this.atualizarBancos();
    });
  };

  mostrarLista = () => {
    return (
      <>
        <Card className="shadow list-entries">
          <Table>
            <th width="15"></th>
            <th width="15"></th>
            <th>Nome</th>
            <th>CódigoComp</th>
            <th>ISPB</th>
            {this.state.bancos != null && this.mostrarDados(this.state.bancos)}
          </Table>
        </Card>
      </>
    );
  };

  mostrarDados = (bancos) => {
    if (bancos == null) {
      return <></>;
    }

    return bancos.map((item, key) => {
      return (
        <>
          <tr>
            <td>
              <div onClick={() => this.confirmaExcluir(item.id)}>
                <i className="far fa-trash-alt"></i>
              </div>
            </td>
            <td>
              <div onClick={() => this.editar(item.id)}>
                <i className="fas fa-info"></i>
              </div>
            </td>
            <td onClick={() => this.editar(item.id)}>{item.nome}</td>
            <td onClick={() => this.editar(item.id)}>{item.codigoComp}</td>
            <td onClick={() => this.editar(item.id)}>{item.ispb}</td>
          </tr>
        </>
      );
    });
  };

  mostrarListaMUI = () => {
    const options = {
      textLabels: global.textLabels,
      filter: true,
      filterType: "dropdown",
      responsive: "stacked",
      print: false,
      download: false,
      rowsPerPage: 15,
      onRowsDelete: (rowsDeleted) => {
        this.removerRegistros(rowsDeleted);
        return false;
      },
      onRowClick: (rowData) => {
        this.editar(rowData[0]);
      },
    };
    const columns = [
      {
        name: "id",
        options: {
          display: false,
          viewColumns: false,
          filter: false,
        },
      },
      {
        name: "nome",
        label: "Nome",
      },
      {
        name: "codigoComp",
        label: "Código Comp",
      },
      {
        name: "ispb",
        label: "ISPB",
      },
    ];
    return (
      <div style={{ width: "100%" }}>
        <MUIDataTable
          columns={columns}
          data={this.gerarDados(this.state.bancos)}
          options={options}
        />
      </div>
    );
  };

  removerRegistros = (rowsDeleted) => {
    const options = {
      title: "Exclusão de Bancos",
      message:
        "Confirma procedimento de exclusão? Este procedimento é irreversível!",
      buttons: [
        {
          label: "Sim",
          onClick: () => {
            var ids = [];

            for (var key in rowsDeleted.data) {
              ids.push(this.state.bancos[rowsDeleted.data[key].dataIndex].id);
            }

            if (ids.length > 0) {
              //var url = global.server_api + 'api/banco/bulk/' + ids.join(",");
              var url =
                global.server_api_new +
                global.apiToken +
                "/banco/bulk/" +
                ids.join(",");

              var config = {
                headers: {
                  Authorization: "bearer " + this.state.accessToken,
                  "Access-Control-Allow-Origin": "*",
                  "Access-Control-Allow-Headers": "Authorization",
                  "Access-Control-Allow-Methods":
                    "GET, POST, OPTIONS, PUT, PATCH, DELETE",
                },
              };

              axios.delete(url, {}, config).then((res) => {
                const result = res.data;

                if (result.success && result.affectedResults > 0) {
                  this.atualizarBancos();
                } else {
                  alert("Erro ao remover. " + res.data.exception.Message);
                  console.log(res.data.exception);
                }
              });
            }
          },
        },
        {
          label: "Não",
        },
      ],
    };

    confirmAlert(options);

    return false;
  };

  gerarDados = (bancos) => {
    if (bancos == null) return [];
    return bancos.map((item) => {
      return {
        id: item.id,
        nome: item.nome,
        codigoComp: item.codigoComp,
        ispb: item.ispb,
      };
    });
  };

  montarDropDown = (itens, id, changeMethod) => (
    <Select
      id={id}
      onChange={changeMethod}
      className="select-component"
      options={itens}
      defaultValue={itens != null ? itens[0] : ""}
      placeholder="Selecione..."
    />
  );

  atualizarBancos = () => {
    if (this.state.interval != null) {
      clearInterval(this.state.interval);
    }

    //var url = global.server_api + 'api/banco/filtro';
    var url = global.server_api_new + global.apiToken + "/banco/filtro";

    var filtro = {
      Filtro: this.state.textoDigitado,
      Ordenacao: this.state.sortField,
    };

    var config = {
      headers: {
        Authorization: "bearer " + this.state.accessToken,
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Headers": "Authorization",
        "Access-Control-Allow-Methods":
          "GET, POST, OPTIONS, PUT, PATCH, DELETE",
      },
    };

    axios.post(url, filtro, config).then((res) => {
      const bancos = res.data.results;
      this.setState({ bancos });
    });
  };

  confirmaExcluir = (id) => {
    const options = {
      title: "Exclusão de Banco",
      message:
        "Tem certeza que deseja excluir este Banco? Este procedimento é irreversível!",
      buttons: [
        {
          label: "Sim",
          onClick: () => this.excluir(id),
        },
        {
          label: "Não",
        },
      ],
    };

    confirmAlert(options);
  };

  excluir = (id) => {
    //var url = global.server_api + 'api/banco/' + id;
    var url = global.server_api_new + global.apiToken + "/banco/" + id;

    var config = {
      headers: {
        Authorization: "bearer " + this.state.accessToken,
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Headers": "Authorization",
        "Access-Control-Allow-Methods":
          "GET, POST, OPTIONS, PUT, PATCH, DELETE",
      },
    };

    axios.delete(url, {}, config).then((res) => {
      const result = res.data;

      if (result.success && result.affectedResults > 0) {
        this.atualizarBancos();
      } else {
        alert("Erro ao remover banco. " + res.data.exception.Message);
        console.log(res.data.exception);
      }
    });
  };

  editar = (id) => {
    var bancoEdicao = this.state.bancos.filter(function (item) {
      return item.id === id;
    });

    if (bancoEdicao != null && bancoEdicao.length > 0) {
      bancoEdicao = bancoEdicao[0];
      //Seta item edicao
      this.setState(
        {
          nome: bancoEdicao.nome,
          ispb: bancoEdicao.ispb,
          codigoComp: bancoEdicao.codigoComp,
          id: bancoEdicao.id,
        },
        function () {
          //Abre modal
          global.showModal(this.modal);
        }
      );
    }
  };

  filtroPorTexto = (valorDigitado) => {
    const textoDigitado = valorDigitado.target.value;

    this.setState({ textoDigitado }, function () {
      if (this.state.interval != null) {
        clearInterval(this.state.interval);
      }

      const $this = this;

      const interval = setInterval(function () {
        $this.atualizarBancos();
      }, 1000);

      this.setState({ interval });
    });
  };

  limparSelecao = () => {
    this.setState({
      nome: null,
      ispb: null,
      codigoComp: null,
      id: null,
    });
  };

  salvar = () => {
    var item = {
      nome: this.state.nome,
      ispb: this.state.ispb,
      codigoComp: this.state.codigoComp,
      id: this.state.id,
    };

    //var url = global.server_api + 'api/banco' + (this.state.id != null ? "/" + this.state.id: "");
    var url =
      global.server_api_new +
      global.apiToken +
      "/banco" +
      (this.state.id != null ? "/" + this.state.id : "");

    var config = {
      headers: {
        Authorization: "bearer " + this.state.accessToken,
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Headers": "Authorization",
        "Access-Control-Allow-Methods":
          "GET, POST, OPTIONS, PUT, PATCH, DELETE",
      },
    };

    axios.post(url, item, config).then((res) => {
      if (res.data.success === true) {
        this.limparSelecao();
        window.location.reload();
      } else {
        console.log(res.data.exception);
        const options = {
          title: "Erro ao Salvar Banco",
          message: res.data.exception.Message,
          buttons: [
            {
              label: "Ok",
            },
          ],
        };

        confirmAlert(options);
      }
    });
  };

  componentDidMount() {
    this.setState(
      {
        accessToken: localStorage.getItem("access-token"),
        empresaId: localStorage.getItem("empresa-id"),
        familiaId: localStorage.getItem("familia-id"),
      },
      function () {
        this.atualizarBancos();
        global.mostrarAjudaDaPaginaAutomaticamente(this);
      }
    );
  }

  mostrarPopup = () => (
    <div>
      <SkyLight
        ref={(ref) => (this.modal = ref)}
        transitionDuration={0}
        beforeOpen={this.preCarregarParaEdicao}
        afterClose={this.limparSelecao}
        title={
          <>
            <div className="pop-up-title">
              <i className="ni ni-check-bold"></i> <h2>Cadastro de Banco</h2>
            </div>
          </>
        }
      >
        <Row>
          <Col lg="12" xl="12">
            <div>
              <label>Nome</label>
              <input
                type="text"
                id="nome"
                value={this.state.nome}
                onChange={(e) => this.setState({ nome: e.target.value })}
              ></input>
            </div>
          </Col>
          <Col lg="6" xl="6">
            <div>
              <label>Código COMPE</label>
              <input
                type="text"
                id="codigoComp"
                value={this.state.codigoComp}
                onChange={(e) => this.setState({ codigoComp: e.target.value })}
              ></input>
            </div>
          </Col>
          <Col lg="6" xl="6">
            <label>ISPB</label>
            <div>
              <input
                type="text"
                id="ispb"
                value={this.state.ispb}
                onChange={(e) => this.setState({ ispb: e.target.value })}
              ></input>
            </div>
          </Col>
          <Col lg="12" xl="12">
            <button
              id="salvar-lancamento"
              className="featured-button"
              onClick={() => this.salvar()}
              style={{
                backgroundImage:
                  "url(" +
                  require("../../assets/img/theme/botao-destaque.png") +
                  ")",
              }}
            >
              Salvar
            </button>
          </Col>
        </Row>
      </SkyLight>
    </div>
  );

  render() {
    return (
      <>
        <Container fluid>
          <div className="home-summary-top">
            <Row>
              <Col lg="12" xl="12" style={{ marginBottom: "15px" }}>
                <div>
                  <Card>
                    <CardHeader className="border-0">
                      <Row className="align-items-center">
                        <div className="col">
                          <img
                            src={require("../../assets/img/theme/icone-calendario.png")}
                            alt="Bancos"
                            className="before-title-img"
                          />
                          <h3 className="mb-0 chart-title">Bancos</h3>
                        </div>
                        <div className="new-entry" style={{ float: "right" }}>
                          <div
                            className="nav-link-icon featured-button"
                            style={{
                              backgroundImage:
                                "url(" +
                                require("../../assets/img/theme/botao-destaque.png") +
                                ")",
                            }}
                            onClick={() => global.showModal(this.modal)}
                          >
                            <span className="nav-link-inner--text">
                              Novo Banco
                            </span>
                          </div>
                        </div>
                      </Row>
                    </CardHeader>
                  </Card>
                </div>
              </Col>
            </Row>
            <Row className="entry-list">
              {/* {this.mostrarLista()} */}
              {this.mostrarListaMUI()}
            </Row>
          </div>
        </Container>
        {this.mostrarPopup()}
      </>
    );
  }
}

export default Bancos;
