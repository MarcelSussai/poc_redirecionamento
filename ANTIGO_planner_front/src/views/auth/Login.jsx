import React, { useEffect } from "react";

// http://localhost:3001/admin/planos-sonhos

const Login = () => {
  useEffect(() => {
    localStorage.removeItem("access-token");
    localStorage.removeItem("dataInicio");
    localStorage.removeItem("dataFim");
    localStorage.removeItem("empresa-id");
    localStorage.removeItem("familia-id");
    localStorage.removeItem("pessoa-id");
    localStorage.removeItem("planejador-id");
    localStorage.removeItem("planejador-principal");
    localStorage.removeItem("tipo-usuario");
    localStorage.removeItem("apiToken");
    localStorage.removeItem("usuario-id");
    localStorage.removeItem("usuario-email");
    localStorage.setItem("erroLogin", "false");
    window.location.href = 'http://localhost:3001/auth/login'
  })
  return ( <> </> )
}

export default Login;


// import React from "react";
// import { Link, Redirect } from "react-router-dom";
// import { confirmAlert } from "react-confirm-alert";
// import "react-confirm-alert/src/react-confirm-alert.css";
// import axios from "axios";
// import {
//   Button,
//   Card,
//   CardBody,
//   FormGroup,
//   Form,
//   Label,
//   Input,
//   InputGroup,
//   Row,
//   Col,
// } from "reactstrap";

// class Login extends React.Component {
//   componentDidMount() {
//     localStorage.removeItem("access-token");
//     localStorage.removeItem("dataInicio");
//     localStorage.removeItem("dataFim");
//     localStorage.removeItem("empresa-id");
//     localStorage.removeItem("familia-id");
//     localStorage.removeItem("pessoa-id");
//     localStorage.removeItem("planejador-id");
//     localStorage.removeItem("planejador-principal");
//     localStorage.removeItem("tipo-usuario");
//     localStorage.removeItem("apiToken");
//     localStorage.removeItem("usuario-id");
//     localStorage.removeItem("usuario-email");
//     localStorage.setItem("erroLogin", "false");

//     document.addEventListener("keypress", this.handleKeyPress);

//     if (global.apiToken === "") {
//       global.getCredential();
//     }
//   }

//   state = {
//     nomeUsuario: null,
//     email: null,
//     senha: null,
//     redirect: false,
//     redirectTermo: false,
//   };

//   setRedirect = () => {
//     this.setState({
//       redirect: true,
//     });
//   };

//   renderRedirect = () => {
//     if (this.state.redirect) {
//       return <Redirect to="/admin/index" />;
//     }
//   };

//   login = () => {
//     localStorage.setItem("erroLogin", "false");
//     var dados = {
//       senha: this.state.senha,
//       email: this.state.email,
//       nomeUsuario: this.state.nomeUsuario,
//     };

//     global.email = dados.email;
//     global.senha = dados.senha;
//     global.getCredential();
//     console.log(localStorage.getItem("erroLogin"));
//     if (localStorage.getItem("erroLogin") === "false") {
//       setTimeout(this.realizaLogin, 1500);
//     }
//   };

//   realizaLogin = () => {
//     var dados = {
//       senha: this.state.senha,
//       email: this.state.email,
//       nomeUsuario: this.state.nomeUsuario,
//     };

//     var url = global.server_api_new + global.apiToken + "/auth";

//     axios.post(url, dados).then((res) => {
//       if (res.data.success === true) {
//         localStorage.setItem("usuario-email", this.state.email);

//         localStorage.setItem("access-token", res.data.accessToken);

//         if (res.data.empresaId != null) {
//           localStorage.setItem("empresa-id", res.data.empresaId);
//         }
//         if (res.data.familiaId != null) {
//           localStorage.setItem("familia-id", res.data.familiaId);
//         }
//         if (res.data.pessoaId != null) {
//           localStorage.setItem("pessoa-id", res.data.pessoaId);
//         }
//         if (res.data.planejadorId != null) {
//           localStorage.setItem("planejador-id", res.data.planejadorId);
//         }
//         if (res.data.usuarioId != null) {
//           localStorage.setItem("usuario-id", res.data.usuarioId);
//         }
//         if (
//           res.data.planejadorPrincipal != null &&
//           res.data.planejadorPrincipal === true
//         ) {
//           localStorage.setItem("planejador-principal", "1");
//         }

//         localStorage.setItem("tipo-usuario", res.data.tipoUsuario);
//         localStorage.setItem("isFlaggedToUseBankIntegration", res.data.isFlaggedToUseBankIntegration);

//         this.setRedirect();
//       } else {
//         console.log(res.data.exception);
//         const options = {
//           title: "Falha no Login",
//           message:
//             res.data.exception.Message != null
//               ? res.data.exception.Message
//               : res.data.exception,
//           buttons: [
//             {
//               label: "Ok",
//             },
//           ],
//         };

//         confirmAlert(options);
//       }
//     });
//   };

//   handleKeyPress = (event) => {
//     if (event.key === "Enter") {
//       this.login();
//     }
//   };

//   render() {
//     const input = {
//       borderRadius: 25,
//       paddingLeft: 20,
//       border: "none",
//       backgroundColor: "#f7faff",
//     };

//     const label = {
//       marginLeft: 15,
//       fontSize: "0.9rem",
//       fontWeight: 600,
//     };

//     return (
//       <>
//         {this.renderRedirect()}
//         <Col
//           lg="6"
//           md="12"
//           style={{
//             backgroundColor: "#10a4c3",
//             display: "flex",
//             flexDirection: "column",
//             justifyContent: "center",
//             alignItems: "center",
//             position: "relative",
//             minHeight: 240,
//           }}
//         >
//           <a
//             href="https://meuvista.com/"
//             target="_blank"
//             rel="noreferrer noopener"
//             style={{ textAlign: "center" }}
//           >
//             <img
//               alt="Vista"
//               src={require("../../assets/img/brand/logo-nova-branca.png")}
//               style={{ width: "70%" }}
//               className="mt-3 mb-3"
//             />
//           </a>
//           <div
//             style={{ position: "absolute", bottom: 0, width: "100%" }}
//             className="pb-3 mt-3"
//           >
//             <div className="copyright text-center text-muted">
//               © 2020 Todos os direitos reservados
//               <span className="font-weight-bold ml-1">Vista</span>
//             </div>
//           </div>
//         </Col>
//         <Col
//           lg="6"
//           md="12"
//           style={{
//             display: "flex",
//             justifyContent: "center",
//             flexDirection: "column",
//             alignItems: "center",
//             backgroundColor: "#f7faff",
//             padding: "30px 0px 30px 0px",
//           }}
//         >
//           <Row className="mt-3">
//             <Link
//               to="/auth/registrar"
//               className="alert-link mb-3"
//               style={{ color: "#9947F4" }}
//             >
//               Cadastro exclusivo para planejadores financeiros
//             </Link>
//           </Row>

//           <Card
//             className="bg-secondary shadow border-0"
//             style={{ width: "90%", borderRadius: 15, maxWidth: 500 }}
//           >
//             <CardBody
//               className="px-lg-5 py-lg-5"
//               style={{ backgroundColor: "#FFF", borderRadius: 15 }}
//             >
//               <div className="text-center text-muted mb-4">
//                 <h2 style={{ fontWeight: 700 }}>Acesse sua conta no Vista</h2>
//               </div>

//               <Form role="form">
//                 <FormGroup className="mb-3">
//                   <Label for="email" style={label}>
//                     E-mail
//                   </Label>
//                   <InputGroup>
//                     <Input
//                       type="email"
//                       value={this.state.email}
//                       onChange={(e) => this.setState({ email: e.target.value.trim() })}
//                       style={input}
//                     />
//                   </InputGroup>
//                 </FormGroup>
//                 <FormGroup>
//                   <Label for="senha" style={label}>
//                     Senha
//                   </Label>
//                   <InputGroup>
//                     <Input
//                       type="password"
//                       value={this.state.senha}
//                       onChange={(e) => this.setState({ senha: e.target.value.trim() })}
//                       style={input}
//                     />
//                   </InputGroup>
//                   <Row>
//                     <Col className="text-right mt-2">
//                       <Link
//                         to="/auth/reset"
//                         style={{ fontSize: "0.9rem", fontWeight: 600 }}
//                       >
//                         Esqueci minha senha
//                       </Link>
//                     </Col>
//                   </Row>
//                 </FormGroup>
//                 <div className="text-center">
//                   <Button
//                     className="featured-button my-4"
//                     color="primary"
//                     type="button"
//                     onKeyPress={this.handleKeyPress}
//                     style={{
//                       backgroundImage:
//                         "url(" +
//                         require("../../assets/img/theme/botao-destaque.png") +
//                         ")",
//                       minWidth: 150,
//                     }}
//                     onClick={() => this.login()}
//                   >
//                     ENTRAR
//                   </Button>
//                   <a href="http://localhost:3001/">TESTE</a>
//                 </div>
//               </Form>
//             </CardBody>
//           </Card>
//         </Col>
//       </>
//     );
//   }
// }

// export default Login;
