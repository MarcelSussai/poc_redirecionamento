import React from "react";
import { Link, Redirect } from "react-router-dom";
import { confirmAlert } from "react-confirm-alert";
import "react-confirm-alert/src/react-confirm-alert.css";
import axios from "axios";

// reactstrap components
import {
  Button,
  Card,
  CardBody,
  FormGroup,
  Form,
  Input,
  InputGroup,
  Row,
  Col,
} from "reactstrap";

class Reset extends React.Component {
  state = {
    email: null,
    nomeUsuario: null,
    redirect: false,
  };

  setRedirect = () => {
    this.setState({
      redirect: true,
    });
  };

  renderRedirect = () => {
    if (this.state.redirect) {
      return <Redirect to="/auth/login" />;
    }
  };

  componentDidMount() {
    const { token } = this.props.match.params;

    //console.log(this.props, this.props.match, this.props.match.params);

    this.setState({ token: token });
  }

  resetarSenha = () => {
    var dados = {
      email: this.state.email,
      nomeUsuario: this.state.nomeUsuario,
      senha: this.state.senha,
      novaSenha: this.state.novaSenha,
      token: this.state.token,
    };

    var url =
      global.server_api_new +
      "a0285a3d-21a0-4efd-8f3f-8808c6a25c68" +
      "/auth/resetsenha";

    if (this.state.token != null) {
      url =
        global.server_api_new +
        "a0285a3d-21a0-4efd-8f3f-8808c6a25c68" +
        "/auth/executarresetsenha";
    }

    axios.post(url, dados).then((res) => {
      if (res.data.success === true) {
        var msg =
          "Instruções para o procedimento de reset enviadas para o seu e-mail";

        if (this.state.token != null) {
          msg = "Senha alterada com sucesso";
        }

        const options = {
          title: "Resetar Senha",
          message: msg,
          buttons: [
            {
              label: "Ok",
              onClick: () => {
                this.setRedirect();
              },
            },
          ],
        };
        confirmAlert(options);
      } else {
        console.log(res.data.exception);
        const options = {
          title: "Falha ao Resetar Senha",
          message: res.data.exception.Message,
          buttons: [
            {
              label: "Ok",
            },
          ],
        };
        confirmAlert(options);
      }
    });
  };

  render() {
    const input = {
      borderRadius: 25,
      paddingLeft: 20,
      border: "none",
      backgroundColor: "#f7faff",
    };

    return (
      <>
        {this.renderRedirect()}
        <Col
          lg="6"
          md="12"
          style={{
            backgroundColor: "#10a4c3",
            display: "flex",
            flexDirection: "column",
            justifyContent: "center",
            alignItems: "center",
            position: "relative",
            minHeight: 240,
          }}
        >
          <a
            href="https://meuvista.com/"
            target="_blank"
            rel="noreferrer noopener"
            style={{ textAlign: "center" }}
          >
            <img
              alt="Vista"
              src={require("../../assets/img/brand/logo-nova-branca.png")}
              style={{ width: "70%" }}
              className="mt-3 mb-3"
            />
          </a>
          <div
            style={{ position: "absolute", bottom: 0, width: "100%" }}
            className="pb-3 mt-3"
          >
            <div className="copyright text-center text-muted">
              © 2020 Todos os direitos reservados
              <span className="font-weight-bold ml-1">Vista</span>
            </div>
          </div>
        </Col>
        <Col
          lg="6"
          md="12"
          style={{
            display: "flex",
            justifyContent: "center",
            flexDirection: "column",
            alignItems: "center",
            backgroundColor: "#f7faff",
            padding: "30px 0px 30px 0px",
          }}
        >
          <Row className="mt-3">
            <Link
              to="/auth/login"
              className="alert-link mb-3"
              style={{ color: "#9947F4" }}
            >
              Voltar para a página de login
            </Link>
          </Row>

          <Card
            className="bg-secondary shadow border-0"
            style={{ width: "90%", borderRadius: 15, maxWidth: 500 }}
          >
            <CardBody
              className="px-lg-5 py-lg-5"
              style={{ backgroundColor: "#FFF", borderRadius: 15 }}
            >
              <div className="text-center text-muted mb-4">
                <h2>Por favor, informe seu e-mail</h2>
              </div>
              <Form role="form">
                <FormGroup className="mb-3">
                  <InputGroup>
                    <Input
                      placeholder="E-mail"
                      type="email"
                      value={this.state.email}
                      onChange={(e) => this.setState({ email: e.target.value })}
                      style={input}
                    />
                  </InputGroup>
                </FormGroup>
                {this.state.token != null ? (
                  <>
                    <FormGroup>
                      <InputGroup>
                        <Input
                          placeholder="Senha"
                          type="password"
                          value={this.state.senha}
                          onChange={(e) =>
                            this.setState({ senha: e.target.value })
                          }
                          style={input}
                        />
                      </InputGroup>
                    </FormGroup>
                    <FormGroup>
                      <InputGroup>
                        <Input
                          placeholder="Confirmação de Senha"
                          type="password"
                          value={this.state.novaSenha}
                          onChange={(e) =>
                            this.setState({ novaSenha: e.target.value })
                          }
                          style={input}
                        />
                      </InputGroup>
                    </FormGroup>
                  </>
                ) : (
                  <></>
                )}
                <div className="text-center">
                  <Button
                    className="my-4 featured-button"
                    color="primary"
                    type="button"
                    style={{
                      backgroundImage:
                        "url(" +
                        require("../../assets/img/theme/botao-destaque.png") +
                        ")",
                      minWidth: 150,
                    }}
                    onClick={() => this.resetarSenha()}
                  >
                    Resetar senha
                  </Button>
                </div>
              </Form>
            </CardBody>
          </Card>
        </Col>
      </>
    );
  }
}

export default Reset;
