import  React from "react";
import { Redirect } from "react-router-dom";
import 'react-confirm-alert/src/react-confirm-alert.css';
import axios from 'axios';

// reactstrap components
import {
  Card,
  CardHeader,
  CardBody,
  Row,
  Col,
} from "reactstrap";

class HabilitarUsuario extends React.Component {

    state = {
        mensagem: "Iniciando processo de habilitação do usuário",
        redirect: false
    }

    componentDidMount() {
        const { id } = this.props.match.params
        this.habilitar(id);
    }

    setRedirect = () => {
        this.setState({
            redirect: true
        })
    }
    renderRedirect = () => {
        if (this.state.redirect) {
            return <Redirect to='/auth/login' />
        }
    }
  
    habilitar = (id) => {

        this.setState({
            mensagem: "Enviando requisição de habilitação aos servidores Vista",
        });
    
        //var url = global.server_api + 'api/Usuario/activate/' + id;
        var url = global.server_api_new + 'a0285a3d-21a0-4efd-8f3f-8808c6a25c68' + '/Usuario/activate/' + id;

        axios.get(url).then(res => {
            if(res.data.success === true){
                this.setState({
                    mensagem: "Usuário ativado com sucesso. Você será redirecionado em 5 segundos",
                });

                var $this = this;

                setTimeout(function(){
                    $this.setRedirect();
                }, 5000);
            }
            else 
            {
                var motivo = res.data.exception.Message != null ? res.data.exception.Message : res.data.exception;

                this.setState({
                    mensagem: "Falha na habilitação do usuário: " + motivo,
                });

                console.log(res.data.exception);
            }
        });
    }

  render() {
    return (
      <>
        {this.renderRedirect()}
        <Row>
            <Col>
                <Card>
                    <CardHeader>
                        <center><b>Habilitação do Usuário</b></center>
                    </CardHeader>
                    <CardBody>
                        <b>Status:</b> {this.state.mensagem}
                    </CardBody>
                </Card>
            </Col>
        </Row>
      </>
    );
  }
}

export default HabilitarUsuario;
