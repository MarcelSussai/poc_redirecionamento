import React from "react";
import MaskedInput from "react-maskedinput";
import axios from "axios";
import Select from "react-select";
import "../../assets/css/register.css";
import { confirmAlert } from "react-confirm-alert";
import "react-confirm-alert/src/react-confirm-alert.css";
import { Link, Redirect } from "react-router-dom";
import $ from "jquery";

// reactstrap components
import {
  Button,
  Card,
  CardBody,
  FormGroup,
  Form,
  Label,
  Input,
  InputGroup,
  Row,
  Col,
  Container
} from "reactstrap";

const urlParams = new URLSearchParams(window.location.search);

class Register extends React.Component {
  constructor(props) {
    super(props);

    this.mascaras = {
      telefone: "(11) 1111-1111",
      celular: "(11) 1 1111-1111",
      cpf: "111.111.111-11",
      cnpj: "11.111.111/1111-11",
    };

    this.estadoCivil = [
      { value: 0, label: "Solteiro" },
      { value: 1, label: "Casado" },
      { value: 2, label: "Divorciado" },
      { value: 3, label: "Viúvo" },
    ];

    this.state = {
      key: urlParams.get("key"),
      termosCondicoes: false,
      isPlanjeadorCheckbox: false,
      pessoaFisicaAtivada: false, //PJ
      tipoEmpresa: 1, //PJ
      passoAtual: 1,
      pais: "Brasil",
      CEP: "",
      telefone: "",
      documentoEmpresa: "",
      enderecoEmpresa: "",
      cidadeEmpresa: "",
      estadoEmpresa: "",
      cpfPlanejador: "",
      labels: {
        nome: "Nome da Empresa *",
        documento: "CNPJ *",
        telefone: "Telefone *",
        enderecoTitulo: "Endereço da Empresa *",
        passo1: "Passo 1 - Dados de Login",
        passo2: "Passo 2 - Dados da Empresa",
        passo3: "Passo 3 - Endereço da Empresa",
        passo4: "Passo 4 - Dados do Responsável",
        passo5: "Passo 5 - Endereço do Responsável",
        telefoneEmpresaMascara: this.mascaras.celular,
        documentoEmpresaMascara: this.mascaras.cnpj,
      },
      tooltipOpen: false,
      confirmaSenha: "",
      email: "",
      senha: "",
      nomeEmpresa: "",
      nomePlanejador: "",
      sobrenomePlanejador: "",
      estadoCivilPlanejador: "",
      celularPlanejador: "",
      docExists: false,
      cepPlanejador: "",
      cidadePlanejador: "",
      estadoPlanejador: "",
      enderecoPlanejador: "",
    };
  }

  validarUsuarioSenha = () => {
    return (
      !this.validarSenha() ||
      this.state.isPlanjeadorCheckbox === false ||
      this.state.termosCondicoes === false ||
      this.state.email === "" ||
      this.state.senha === "" ||
      this.state.confirmaSenha !== this.state.senha
    );
  };

  validarCNPJ = () => {
    return (
      this.state.docExists === true ||
      this.state.nomeEmpresa === "" ||
      this.state.documentoEmpresa === "" ||
      this.state.telefone === ""
    );
  };

  validarCPF = () => {
    return (
      this.state.pessoaFisicaAtivada === true &&
      (this.state.docExists === true ||
        this.state.nomePlanejador === "" ||
        this.state.sobrenomePlanejador === "" ||
        this.state.documentoEmpresa === "" ||
        this.state.telefone === "" ||
        this.state.estadoCivilPlanejador === "")
    );
  };

  validarResponsavel = () => {
    return (
      this.state.pessoaFisicaAtivada === false &&
      (this.state.docExists === true ||
        this.state.nomePlanejador === "" ||
        this.state.sobrenomePlanejador === "" ||
        this.state.cpfPlanejador === "" ||
        this.state.estadoCivilPlanejador === "" ||
        this.state.celularPlanejador === "")
    );
  };

  validarEndereco = () => {
    if (this.state.passoAtual === 3) {
      return (
        this.state.CEP === "" ||
        this.state.cidadeEmpresa === "" ||
        this.state.estadoEmpresa === "" ||
        this.state.enderecoEmpresa === ""
      );
    } else if (this.state.passoAtual === 5) {
      return (
        this.state.cepPlanejador === "" ||
        this.state.cidadePlanejador === "" ||
        this.state.estadoPlanejador === "" ||
        this.state.enderecoPlanejador === ""
      );
    }
  };

  _next = () => {
    if (!this.validarSenha()) {
      confirmAlert({
        title: "Senha Fraca",
        message:
          "A senha deve conter pelo menos 8 dígitos, uma letra em maiúsculo, um número e um caracter especial (Ex: !@#$%¨*)",
        buttons: [
          {
            label: "Ok",
          },
        ],
      });
      return;
    }

    let passoAtual = this.state.passoAtual;
    passoAtual += 1;

    // if(this.state.pessoaFisicaAtivada && passoAtual === 2) //É PF e está saindo do passo 2
    // {
    //   passoAtual += 3;
    // }
    // else {
    //   passoAtual += 1;
    // }
    this.setState({
      passoAtual: passoAtual,
    });
  };

  _prev = () => {
    let passoAtual = this.state.passoAtual;
    passoAtual -= 1;
    // if(this.state.pessoaFisicaAtivada && passoAtual === 5) //É PF e está voltando do ultimo passo (passo 5)
    // {
    //   passoAtual -= 3;
    // }
    // else {
    //   passoAtual -= 1;
    // }

    this.setState({
      passoAtual: passoAtual,
    });
  };

  atualizarDadosEnderecoEmpresa = (json) => {
    if (json != null) {
      this.setState({
        enderecoEmpresa: json.logradouro,
        cidadeEmpresa: json.localidade,
        estadoEmpresa: json.uf,
      });
    }
  };

  atualizarDadosEnderecoPlanejador = (json) => {
    if (json != null) {
      this.setState({
        enderecoPlanejador: json.logradouro,
        cidadePlanejador: json.localidade,
        estadoPlanejador: json.uf,
      });
    }
  };

  _onChangeSelection = (selectedOption) => {
    var valor = selectedOption.value;
    this.setState({ estadoCivilPlanejador: valor });
  };

  _onChange = (e) => {
    if (e.target.value.indexOf("_") > -1) {
      return;
    }

    var nomeCampo = e.target.name;

    this.setState({ [nomeCampo]: e.target.value });

    if (nomeCampo === "documentoEmpresa" || nomeCampo === "cpfPlanejador") {
      axios
        .post(`${global.server_api_new}credencial/cadplanexistdoc/`, {
          documento: e.target.value,
        })
        .then((res) => {
          if (res.data.code === "200") {
            this.setState({ docExists: true });
          } else {
            this.setState({ docExists: false });
          }
        });
    }

    if (nomeCampo === "CEP" || nomeCampo === "cepPlanejador") {
      var cep = e.target.value;
      cep = cep.replace(".", "").replace("-", "");

      var cepUrl = "https://viacep.com.br/ws/" + cep + "/json";

      axios.get(cepUrl).then((res) => {
        if (nomeCampo === "CEP") {
          this.atualizarDadosEnderecoEmpresa(res.data);
        } else {
          this.atualizarDadosEnderecoPlanejador(res.data);
        }
      });
    }
  };

  trocarTipoPessoa = () => {
    var labels = this.state.labels;
    var pessoaFisicaAtivada = !this.state.pessoaFisicaAtivada;

    labels.documento = !pessoaFisicaAtivada ? "CNPJ *" : "CPF *";
    labels.nome = !pessoaFisicaAtivada
      ? "Nome da Empresa *"
      : "Nome Completo *";
    labels.telefone = !pessoaFisicaAtivada ? "Telefone Fixo *" : "Celular *";
    labels.enderecoTitulo = !pessoaFisicaAtivada
      ? "Endereço da Empresa *"
      : "Endereço *";
    labels.passo2 = !pessoaFisicaAtivada
      ? "Passo 2 - Dados da Empresa"
      : "Passo 2 - Dados Pessoais";
    labels.passo3 = !pessoaFisicaAtivada
      ? "Passo 3 - Endereço da Empresa"
      : "Passo 3 - Endereço";
    labels.passo1 = !pessoaFisicaAtivada
      ? "Passo 1 - Dados de Login"
      : "Passo 1 - Dados de Login";
    labels.passo5 = !pessoaFisicaAtivada
      ? "Passo 5 - Endereço do Responsável"
      : "Passo 3 - Endereço do Responsável";
    labels.telefoneEmpresaMascara = this.mascaras.celular; //!pessoaFisicaAtivada ? this.mascaras.telefone : this.mascaras.celular;
    labels.documentoEmpresaMascara = !pessoaFisicaAtivada
      ? this.mascaras.cnpj
      : this.mascaras.cpf;

    var tipoEmpresa = !pessoaFisicaAtivada ? 1 : 0;

    this.setState({
      pessoaFisicaAtivada: pessoaFisicaAtivada,
      tipoEmpresa: tipoEmpresa,
      labels: labels,
    });
  };

  setRedirect = () => {
    this.setState({
      redirect: true,
    });
  };

  renderRedirect = () => {
    if (this.state.redirect) {
      return <Redirect to="/auth/login" />;
    }
  };

  validarSenha = () => {
    let regex = RegExp("^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#$%^&*])(?=.{8,})");

    if (regex.test(this.state.senha)) {
      return true;
    } else {
      return false;
    }
  };

  criarEmpresa = () => {
    var isPJ = !this.state.pessoaFisicaAtivada;

    var currentScroll = [
      document.documentElement.scrollLeft || document.body.scrollLeft,
      document.documentElement.scrollTop || document.body.scrollTop,
    ];

    global.spinnerShow($);

    // console.log(this.state);

    var empresa = {
      Nome: isPJ
        ? this.state.nomeEmpresa
        : this.state.nomePlanejador + " " + this.state.sobrenomePlanejador,
      Documento: this.state.documentoEmpresa,
      Telefone: this.state.telefone,
      Endereco: this.state.enderecoEmpresa,
      Complemento: this.state.complementoEmpresa,
      Pais: this.state.pais,
      Cidade: this.state.cidadeEmpresa,
      Estado: this.state.estadoEmpresa,
      CEP: this.state.CEP,
      TipoEmpresa: this.state.tipoEmpresa,
      PlanejadorDTO: {
        Nome: this.state.nomePlanejador,
        Sobrenome: this.state.sobrenomePlanejador,
        Documento: isPJ
          ? this.state.cpfPlanejador
          : this.state.documentoEmpresa,
        EstadoCivil: this.state.estadoCivilPlanejador,
        Endereco: isPJ
          ? this.state.enderecoPlanejador
          : this.state.enderecoEmpresa,
        Complemento: isPJ
          ? this.state.complementoPlanejador
          : this.state.complementoEmpresa,
        Pais: this.state.pais,
        Cidade: isPJ ? this.state.cidadePlanejador : this.state.cidadeEmpresa,
        Estado: isPJ ? this.state.estadoPlanejador : this.state.estadoEmpresa,
        CEP: isPJ ? this.state.cepPlanejador : this.state.CEP,
        Celular: isPJ ? this.state.celularPlanejador : this.state.telefone,
        Telefone: this.state.telefonePlanejador,
        UsuarioDTO: {
          NomeUsuario: this.state.nomeUsuario,
          Senha: this.state.senha,
          NovaSenha: this.state.senha,
          Email: this.state.email,
          Tipo: 1, //0 = Admin , 1 = Planejador, 2 = Pessoa
        },
      },
      Indicacao: this.state.key,
    };

    //var url = global.server_api + 'api/Empresa';
    var url =
      global.server_api_new +
      "a0285a3d-21a0-4efd-8f3f-8808c6a25c68" +
      "/Empresa/";

    axios
      .post(url, empresa)
      .then((res) => {
        global.spinnerHide($, currentScroll);

        if (res.data.success === true) {
          const options = {
            title: "Novo Cadastro",
            message:
              "Seu cadastro foi efetuado com sucesso. Acesse seu e-mail para mais informações.",
            buttons: [
              {
                label: "Ok",
                onClick: () => {
                  this.setRedirect();
                },
              },
            ],
          };

          confirmAlert(options);
        } else {
          // console.log(res.data.exception);
          const options = {
            title: "Falha no Cadastro",
            message: res.data.exception.Message,
            buttons: [
              {
                label: "Ok",
              },
            ],
          };

          confirmAlert(options);
        }
      })
      .catch((error) => {
        global.spinnerHide($, currentScroll);
        global.logarErroDeRequisicao(error);
      });
  };

  render() {
    const input = {
      borderRadius: 25,
      paddingLeft: 20,
      border: "none",
      backgroundColor: "#f7faff",
      height: "2.5rem",
    };

    const label = {
      marginLeft: 15,
      fontSize: "0.9rem",
      fontWeight: 600,
    };

    return (
      <>
        {this.renderRedirect()}
        <Col
          lg="6"
          md="12"
          style={{
            backgroundColor: "#10a4c3",
            display: "flex",
            flexDirection: "column",
            justifyContent: "center",
            alignItems: "center",
            position: "relative",
            minHeight: 240,
          }}
        >
          <a
            href="https://meuvista.com/"
            target="_blank"
            rel="noreferrer noopener"
            style={{ textAlign: "center" }}
          >
            <img
              alt="Vista"
              src={require("../../assets/img/brand/logo-nova-branca.png")}
              style={{ width: "70%" }}
              className="mt-3 mb-3"
            />
          </a>
          <div
            style={{ position: "absolute", bottom: 0, width: "100%" }}
            className="pb-3 mt-3"
          >
            <div className="copyright text-center text-muted">
              © 2020 Todos os direitos reservados
              <span className="font-weight-bold ml-1">Vista</span>
            </div>
          </div>
        </Col>
        <Col
          lg="6"
          md="12"
          style={{
            display: "flex",
            justifyContent: "center",
            flexDirection: "column",
            alignItems: "center",
            backgroundColor: "#f7faff",
            padding: "30px 0px 30px 0px",
          }}
        >
          <Row className="mt-3">
            <Link
              to="/auth/login"
              className="alert-link mb-3"
              style={{ color: "#9947F4" }}
            >
              Já possui cadastro no Vista? Entrar
            </Link>
          </Row>

          <Card
            className="bg-secondary shadow border-0"
            style={{ width: "90%", borderRadius: 15, maxWidth: 500 }}
          >
            <CardBody
              className="px-lg-5 py-lg-5"
              style={{ backgroundColor: "#FFF", borderRadius: 15 }}
            >
              <div className="text-center text-muted">
                <h2 style={{ fontWeight: 700 }}>Crie sua conta no Vista</h2>
              </div>
              <Form role="form">
                {this.state.passoAtual === 1 ? (
                  <>
                    <Row
                      style={{
                        marginBottom: "1.5rem",
                        justifyContent: "center",
                      }}
                    >
                      <h4>{this.state.labels.passo1}</h4>
                    </Row>
                    <FormGroup>
                      <Label style={label}>E-mail</Label>
                      <InputGroup>
                        <Input
                          type="email"
                          value={this.state.email}
                          onChange={(e) =>
                            this.setState({ email: e.target.value })
                          }
                          style={input}
                        />
                      </InputGroup>
                    </FormGroup>

                    <Row form>
                      <Col md={6}>
                        <FormGroup>
                          <Label style={label}>Senha</Label>
                          <InputGroup>
                            <Input
                              type="password"
                              value={this.state.senha}
                              onChange={(e) =>
                                this.setState({ senha: e.target.value })
                              }
                              onLoad={this.handleChange}
                              style={input}
                            />
                          </InputGroup>
                          <span
                            style={
                              this.validarSenha()
                                ? { display: "none" }
                                : {
                                    display: "block",
                                    color: "red",
                                    fontSize: "0.65rem",
                                    marginLeft: 10,
                                    position: "absolute",
                                    marginTop: "2px",
                                    marginBottom: "2px",
                                  }
                            }
                          >
                            A senha deve conter pelo menos 8 dígitos, uma letra
                            em maiúsculo, um número e um caracter especial (Ex:
                            !@#$%¨*)
                          </span>
                        </FormGroup>
                      </Col>
                      <Col md={6}>
                        <FormGroup>
                          <Label style={label}>Confirmar senha</Label>
                          <InputGroup>
                            <Input
                              type="password"
                              value={this.state.confirmaSenha}
                              onChange={(e) =>
                                this.setState({ confirmaSenha: e.target.value })
                              }
                              style={input}
                            />
                          </InputGroup>
                          <span
                            style={
                              this.state.confirmaSenha === this.state.senha
                                ? { display: "none" }
                                : {
                                    display: "block",
                                    color: "red",
                                    fontSize: "0.65rem",
                                    marginLeft: 10,
                                    position: "absolute",
                                  }
                            }
                          >
                            Senha não confere
                          </span>
                        </FormGroup>
                      </Col>
                    </Row>

                    {/* <FormGroup>
                      <Label style={label}>Código do convite para cadastro</Label>
                      <InputGroup>
                        <Input type="text" value={this.state.codigoliberacao} onChange={e => this.setState({ codigoliberacao: e.target.value })} style={input}/>
                      </InputGroup>
                      <p style={{fontSize: "0.75rem", fontWeight: 400, paddingTop: 5, textAlign: "center"}}>
                        Caso não tenha um convite, escreva para <a href="mailto:suporte@meuvista.com" style={{color: "#9947F4"}}>suporte@meuvista.com</a>
                      </p>
                    </FormGroup> */}
                  </>
                ) : (
                  <></>
                )}
                {this.state.passoAtual === 1 ? (
                  <Container>

                    <Row className="mt-5">
                      <Col xs="12" className="ml-1">
                        <div className="custom-control custom-control-alternative custom-checkbox mt-2">
                          <input
                            className="custom-control-input"
                            id="termosCondicoes"
                            type="checkbox"
                            checked={this.state.termosCondicoes}
                            onChange={() =>
                              this.setState({
                                termosCondicoes: !this.state.termosCondicoes,
                              })
                            }
                          />
                          <label
                            className="custom-control-label"
                            htmlFor="termosCondicoes"
                          >
                            <span
                              className="text-muted"
                              style={{ fontSize: "0.8rem" }}
                            >
                              Declaro que li e concordo com os{" "}
                              <a
                                href="https://meuvista.com/termos-de-uso/"
                                rel="noopener noreferrer"
                                target="_blank"
                              >
                                Termos & Condições
                              </a>{" "}
                              e a{" "}
                              <a
                                href="https://meuvista.com/politica-de-privacidade/"
                                rel="noopener noreferrer"
                                target="_blank"
                              >
                                Política de Privacidade
                              </a>
                            </span>
                          </label>
                        </div>
                      </Col>
                    </Row>
                    <Row className="mt-5">
                        <Col xs="12" className="ml-1">
                        <div className="custom-control custom-control-alternative custom-checkbox mt-2">
                          <input
                            className="custom-control-input"
                            id="isPlanjeadorCheckbox"
                            type="checkbox"
                            checked={this.state.isPlanjeadorCheckbox}
                            onChange={() =>
                              this.setState({
                                isPlanjeadorCheckbox: !this.state.isPlanjeadorCheckbox,
                              })
                            }
                          />
                          <label
                            className="custom-control-label"
                            htmlFor="isPlanjeadorCheckbox"
                          >
                            <span
                              className="text-muted"
                              style={{ fontSize: "0.8rem" }}
                            >Eu sou um planejador financeiro pessoal</span>
                          </label>
                          </div>
                        </Col>
                    </Row>
                  </Container>
                  
                ) : (
                  <></>
                )}
                {this.state.passoAtual === 2 ? (
                  <div>
                    <Row
                      style={{
                        marginBottom: "1.5rem",
                        justifyContent: "center",
                      }}
                    >
                      <h4>{this.state.labels.passo2}</h4>
                    </Row>
                    <Row className="my-4">
                      <Col xs="12">
                        <div className="custom-control custom-control-alternative custom-checkbox">
                          <input
                            className="custom-control-input"
                            type="checkbox"
                            id="pessoaFisicaAtivada"
                            checked={this.state.pessoaFisicaAtivada}
                            onChange={() => this.trocarTipoPessoa()}
                          />
                          <label
                            className="custom-control-label"
                            for="pessoaFisicaAtivada"
                          >
                            <span className="text-muted">
                              Desejo me cadastrar como Pessoa Física
                            </span>
                          </label>
                        </div>
                      </Col>
                    </Row>
                    {this.state.pessoaFisicaAtivada === false ? (
                      <FormGroup>
                        <Label style={label}>Razão Social *</Label>
                        <InputGroup>
                          <Input
                            type="text"
                            value={this.state.nomeEmpresa}
                            onChange={(e) =>
                              this.setState({ nomeEmpresa: e.target.value })
                            }
                            style={input}
                          />
                        </InputGroup>
                      </FormGroup>
                    ) : (
                      <>
                        <FormGroup>
                          <Label style={label}>Nome *</Label>
                          <InputGroup>
                            <Input
                              type="text"
                              value={this.state.nomePlanejador}
                              onChange={(e) =>
                                this.setState({
                                  nomePlanejador: e.target.value,
                                })
                              }
                              style={input}
                            />
                          </InputGroup>
                        </FormGroup>
                        <FormGroup>
                          <Label style={label}>Sobrenome *</Label>
                          <InputGroup>
                            <Input
                              type="text"
                              value={this.state.sobrenomePlanejador}
                              onChange={(e) =>
                                this.setState({
                                  sobrenomePlanejador: e.target.value,
                                })
                              }
                              style={input}
                            />
                          </InputGroup>
                        </FormGroup>
                      </>
                    )}
                    <FormGroup>
                      <Label style={label}>{this.state.labels.documento}</Label>
                      <InputGroup>
                        <MaskedInput
                          name="documentoEmpresa"
                          className="form-control"
                          mask={this.state.labels.documentoEmpresaMascara}
                          value={this.state.documentoEmpresa}
                          onChange={this._onChange}
                          style={input}
                        />
                      </InputGroup>
                      <span
                        style={
                          !this.state.docExists
                            ? { display: "none" }
                            : {
                                display: "block",
                                color: "red",
                                fontSize: "0.65rem",
                                marginLeft: 10,
                                position: "absolute",
                                marginTop: "2px",
                                marginBottom: "2px",
                              }
                        }
                      >
                        Documento já cadastrado.
                      </span>
                    </FormGroup>
                    {this.state.pessoaFisicaAtivada === true ? (
                      <FormGroup>
                        <Label style={label}>Estado civil *</Label>
                        <InputGroup className="mb-3">
                          <Select
                            className="select-component"
                            placeholder="Selecione..."
                            value={this.estadoCivil.find((item) => {
                              return (
                                item.value === this.state.estadoCivilPlanejador
                              );
                            })}
                            onChange={this._onChangeSelection}
                            options={this.estadoCivil}
                            styles={{
                              control: (provided, state) => ({
                                ...provided,
                                backgroundColor: "#f7faff",
                                borderRadius: 25,
                                borderStyle: "none",
                                display: "flex",
                              }),
                              container: (provided, state) => ({
                                ...provided,
                                width: "100%",
                              }),
                              singleValue: (provided, state) => ({
                                ...provided,
                                color: "#8898aa",
                                paddingLeft: 10,
                              }),
                              placeholder: (provided, state) => ({
                                ...provided,
                                color: "#8898aa",
                                paddingLeft: 10,
                              }),
                            }}
                          />
                        </InputGroup>
                      </FormGroup>
                    ) : (
                      <></>
                    )}
                    <FormGroup>
                      <Label style={label}>{this.state.labels.telefone}</Label>
                      <InputGroup>
                        <MaskedInput
                          name="telefone"
                          className="form-control"
                          mask={this.state.labels.telefoneEmpresaMascara}
                          value={this.state.telefone}
                          onChange={this._onChange}
                          style={input}
                        />
                      </InputGroup>
                    </FormGroup>
                  </div>
                ) : (
                  <></>
                )}
                {this.state.passoAtual === 3 ? (
                  <>
                    <Row
                      style={{
                        marginBottom: "1.5rem",
                        justifyContent: "center",
                      }}
                    >
                      <h4>{this.state.labels.passo3}</h4>
                    </Row>
                    <FormGroup>
                      <Label style={label}>CEP *</Label>
                      <InputGroup className="mb-3">
                        <MaskedInput
                          name="CEP"
                          className="form-control"
                          value={this.state.CEP}
                          mask="11.111-111"
                          onChange={this._onChange}
                          style={input}
                        />
                      </InputGroup>
                    </FormGroup>

                    <Row form>
                      <Col md={8}>
                        <FormGroup>
                          <Label style={label}>Cidade *</Label>
                          <InputGroup className="mb-3">
                            <Input
                              type="text"
                              value={this.state.cidadeEmpresa}
                              onChange={(e) =>
                                this.setState({ cidadeEmpresa: e.target.value })
                              }
                              style={input}
                            />
                          </InputGroup>
                        </FormGroup>
                      </Col>
                      <Col md={4}>
                        <FormGroup>
                          <Label style={label}>Estado *</Label>
                          <InputGroup className="mb-3">
                            <Input
                              type="text"
                              value={this.state.estadoEmpresa}
                              onChange={(e) =>
                                this.setState({ estadoEmpresa: e.target.value })
                              }
                              style={input}
                            />
                          </InputGroup>
                        </FormGroup>
                      </Col>
                    </Row>

                    <FormGroup>
                      <Label style={label}>Endereço *</Label>
                      <InputGroup className="mb-3">
                        <Input
                          type="text"
                          value={this.state.enderecoEmpresa}
                          onChange={(e) =>
                            this.setState({ enderecoEmpresa: e.target.value })
                          }
                          style={input}
                        />
                      </InputGroup>
                    </FormGroup>
                    <FormGroup>
                      <Label style={label}>Complemento</Label>
                      <InputGroup className="mb-3">
                        <Input
                          type="text"
                          value={this.state.complementoEmpresa}
                          onChange={(e) =>
                            this.setState({
                              complementoEmpresa: e.target.value,
                            })
                          }
                          style={input}
                        />
                      </InputGroup>
                    </FormGroup>
                  </>
                ) : (
                  <></>
                )}
                {this.state.passoAtual === 4 ? (
                  <>
                    <Row
                      style={{
                        marginBottom: "1.5rem",
                        justifyContent: "center",
                      }}
                    >
                      <h4>{this.state.labels.passo4}</h4>
                    </Row>
                    <FormGroup>
                      <Label style={label}>Nome *</Label>
                      <InputGroup className="mb-3">
                        <Input
                          type="text"
                          value={this.state.nomePlanejador}
                          onChange={(e) =>
                            this.setState({ nomePlanejador: e.target.value })
                          }
                          style={input}
                        />
                      </InputGroup>
                    </FormGroup>
                    <FormGroup>
                      <Label style={label}>Sobrenome *</Label>
                      <InputGroup className="mb-3">
                        <Input
                          type="text"
                          value={this.state.sobrenomePlanejador}
                          onChange={(e) =>
                            this.setState({
                              sobrenomePlanejador: e.target.value,
                            })
                          }
                          style={input}
                        />
                      </InputGroup>
                    </FormGroup>
                    <FormGroup>
                      <Label style={label}>CPF *</Label>
                      <InputGroup>
                        <MaskedInput
                          name="cpfPlanejador"
                          className="form-control"
                          mask={this.mascaras.cpf}
                          value={this.state.cpfPlanejador}
                          onChange={this._onChange}
                          style={input}
                        />
                      </InputGroup>
                      <span
                        style={
                          !this.state.docExists
                            ? { display: "none" }
                            : {
                                display: "block",
                                color: "red",
                                fontSize: "0.65rem",
                                marginLeft: 10,
                                position: "absolute",
                                marginTop: "2px",
                                marginBottom: "2px",
                              }
                        }
                      >
                        Documento já cadastrado.
                      </span>
                    </FormGroup>
                    <FormGroup>
                      <Label style={label}>Estado Civil *</Label>
                      <InputGroup className="mb-3">
                        <Select
                          className="select-component"
                          placeholder="Selecione..."
                          value={this.estadoCivil.find((item) => {
                            return (
                              item.value === this.state.estadoCivilPlanejador
                            );
                          })}
                          onChange={this._onChangeSelection}
                          options={this.estadoCivil}
                          styles={{
                            control: (provided, state) => ({
                              ...provided,
                              backgroundColor: "#f7faff",
                              borderRadius: 25,
                              borderStyle: "none",
                              display: "flex",
                            }),
                            container: (provided, state) => ({
                              ...provided,
                              width: "100%",
                            }),
                            singleValue: (provided, state) => ({
                              ...provided,
                              color: "#8898aa",
                              paddingLeft: 10,
                            }),
                            placeholder: (provided, state) => ({
                              ...provided,
                              color: "#8898aa",
                              paddingLeft: 10,
                            }),
                          }}
                        />
                      </InputGroup>
                    </FormGroup>

                    <Row form>
                      <Col md={6}>
                        <FormGroup>
                          <Label style={label}>Celular *</Label>
                          <InputGroup className="mb-3">
                            <MaskedInput
                              name="celularPlanejador"
                              className="form-control"
                              mask={this.mascaras.celular}
                              value={this.state.celularPlanejador}
                              onChange={this._onChange}
                              style={input}
                            />
                          </InputGroup>
                        </FormGroup>
                      </Col>
                      <Col md={6}>
                        <FormGroup>
                          <Label style={label}>Telefone</Label>
                          <InputGroup className="mb-3">
                            <MaskedInput
                              name="telefonePlanejador"
                              className="form-control"
                              mask={this.mascaras.telefone}
                              value={this.state.telefonePlanejador}
                              onChange={this._onChange}
                              style={input}
                            />
                          </InputGroup>
                        </FormGroup>
                      </Col>
                    </Row>
                  </>
                ) : (
                  <></>
                )}
                {this.state.passoAtual === 5 ? (
                  <>
                    <Row
                      style={{
                        marginBottom: "1.5rem",
                        justifyContent: "center",
                      }}
                    >
                      <h4>{this.state.labels.passo5}</h4>
                    </Row>
                    <FormGroup>
                      <Label style={label}>CEP *</Label>
                      <InputGroup className="mb-3">
                        <MaskedInput
                          name="cepPlanejador"
                          className="form-control"
                          mask="11.111-111"
                          onChange={this._onChange}
                          style={input}
                        />
                      </InputGroup>
                    </FormGroup>

                    <Row form>
                      <Col md={8}>
                        <FormGroup>
                          <Label style={label}>Cidade *</Label>
                          <InputGroup className="mb-3">
                            <Input
                              type="text"
                              value={this.state.cidadePlanejador}
                              onChange={(e) =>
                                this.setState({
                                  cidadePlanejador: e.target.value,
                                })
                              }
                              style={input}
                            />
                          </InputGroup>
                        </FormGroup>
                      </Col>
                      <Col md={4}>
                        <FormGroup>
                          <Label style={label}>Estado *</Label>
                          <InputGroup className="mb-3">
                            <Input
                              type="text"
                              value={this.state.estadoPlanejador}
                              onChange={(e) =>
                                this.setState({
                                  estadoPlanejador: e.target.value,
                                })
                              }
                              style={input}
                            />
                          </InputGroup>
                        </FormGroup>
                      </Col>
                    </Row>

                    <FormGroup>
                      <Label style={label}>Endereço *</Label>
                      <InputGroup className="mb-3">
                        <Input
                          type="text"
                          value={this.state.enderecoPlanejador}
                          onChange={(e) =>
                            this.setState({
                              enderecoPlanejador: e.target.value,
                            })
                          }
                          style={input}
                        />
                      </InputGroup>
                    </FormGroup>
                    <FormGroup>
                      <Label style={label}>Complemento</Label>
                      <InputGroup className="mb-3">
                        <Input
                          type="text"
                          value={this.state.complementoPlanejador}
                          onChange={(e) =>
                            this.setState({
                              complementoPlanejador: e.target.value,
                            })
                          }
                          style={input}
                        />
                      </InputGroup>
                    </FormGroup>
                  </>
                ) : (
                  <></>
                )}
                <Row
                  style={{
                    justifyContent: "center",
                    flexDirection: "row-reverse",
                  }}
                >
                  {this.state.passoAtual === 1 && this.validarUsuarioSenha() ? (
                    <div className="text-center">
                      <Button
                        className="mt-4 featured-button"
                        color="light"
                        type="button"
                        id="checkConditions"
                        style={{ minWidth: 150 }}
                      >
                        Próximo
                      </Button>
                    </div>
                  ) : this.state.passoAtual === 1 &&
                    !this.validarUsuarioSenha() ? (
                    <div className="text-center">
                      <Button
                        className="mt-4 featured-button"
                        color="primary"
                        type="button"
                        style={{
                          backgroundImage:
                            "url(" +
                            require("../../assets/img/theme/botao-destaque.png") +
                            ")",
                          minWidth: 150,
                        }}
                        onClick={() => this._next()}
                      >
                        Próximo
                      </Button>
                    </div>
                  ) : (
                    <></>
                  )}
                  {this.state.passoAtual === 2 &&
                  !this.state.pessoaFisicaAtivada ? (
                    this.validarCNPJ() ? (
                      <div className="text-center">
                        <Button
                          className="mt-4 featured-button"
                          color="light"
                          type="button"
                          id="checkConditions"
                          style={{ minWidth: 150 }}
                        >
                          Próximo
                        </Button>
                      </div>
                    ) : (
                      <div className="text-center">
                        <Button
                          className="mt-4 featured-button"
                          color="primary"
                          type="button"
                          style={{
                            backgroundImage:
                              "url(" +
                              require("../../assets/img/theme/botao-destaque.png") +
                              ")",
                            minWidth: 150,
                          }}
                          onClick={() => this._next()}
                        >
                          Próximo
                        </Button>
                      </div>
                    )
                  ) : (
                    <></>
                  )}
                  {this.state.passoAtual === 2 &&
                  this.state.pessoaFisicaAtivada ? (
                    this.validarCPF() ? (
                      <div className="text-center">
                        <Button
                          className="mt-4 featured-button"
                          color="light"
                          type="button"
                          id="checkConditions"
                          style={{ minWidth: 150 }}
                        >
                          Próximo
                        </Button>
                      </div>
                    ) : (
                      <div className="text-center">
                        <Button
                          className="mt-4 featured-button"
                          color="primary"
                          type="button"
                          style={{
                            backgroundImage:
                              "url(" +
                              require("../../assets/img/theme/botao-destaque.png") +
                              ")",
                            minWidth: 150,
                          }}
                          onClick={() => this._next()}
                        >
                          Próximo
                        </Button>
                      </div>
                    )
                  ) : (
                    <></>
                  )}
                  {this.state.passoAtual === 4 ? (
                    this.validarResponsavel() ? (
                      <div className="text-center">
                        <Button
                          className="mt-4 featured-button"
                          color="light"
                          type="button"
                          id="checkConditions"
                          style={{ minWidth: 150 }}
                        >
                          Próximo
                        </Button>
                      </div>
                    ) : (
                      <div className="text-center">
                        <Button
                          className="mt-4 featured-button"
                          color="primary"
                          type="button"
                          style={{
                            backgroundImage:
                              "url(" +
                              require("../../assets/img/theme/botao-destaque.png") +
                              ")",
                            minWidth: 150,
                          }}
                          onClick={() => this._next()}
                        >
                          Próximo
                        </Button>
                      </div>
                    )
                  ) : (
                    <></>
                  )}
                  {this.state.passoAtual < 5 &&
                  this.state.passoAtual !== 1 &&
                  this.state.passoAtual !== 2 &&
                  this.state.passoAtual !== 4 &&
                  !this.state.pessoaFisicaAtivada ? (
                    this.validarEndereco() ? (
                      <div className="text-center">
                        <Button
                          className="mt-4 featured-button"
                          color="light"
                          type="button"
                          id="checkConditions"
                          style={{ minWidth: 150 }}
                        >
                          Próximo
                        </Button>
                      </div>
                    ) : (
                      <div className="text-center">
                        <Button
                          className="mt-4 featured-button"
                          color="primary"
                          type="button"
                          style={{
                            backgroundImage:
                              "url(" +
                              require("../../assets/img/theme/botao-destaque.png") +
                              ")",
                            minWidth: 150,
                          }}
                          onClick={() => this._next()}
                        >
                          Próximo
                        </Button>
                      </div>
                    )
                  ) : (
                    <></>
                  )}
                  {(this.state.passoAtual === 5 &&
                    this.state.termosCondicoes) ||
                  (this.state.passoAtual === 3 &&
                    this.state.pessoaFisicaAtivada &&
                    this.state.termosCondicoes) ? (
                    this.validarEndereco() ? (
                      <div className="text-center">
                        <Button
                          className="mt-4 featured-button"
                          color="light"
                          type="button"
                          id="checkConditions"
                          style={{ minWidth: 150 }}
                        >
                          CRIAR CONTA
                        </Button>
                      </div>
                    ) : (
                      <div className="text-center">
                        <Button
                          className="mt-4 featured-button"
                          color="primary"
                          type="button"
                          style={{
                            backgroundImage:
                              "url(" +
                              require("../../assets/img/theme/botao-destaque.png") +
                              ")",
                            minWidth: 150,
                          }}
                          onClick={() => this.criarEmpresa()}
                        >
                          CRIAR CONTA
                        </Button>
                      </div>
                    )
                  ) : (
                    <></>
                  )}
                  {this.state.passoAtual > 1 ? (
                    <div
                      className="text-center"
                      style={{ float: "right", marginRight: "8px" }}
                    >
                      <Button
                        className="mt-4 featured-button"
                        color="primary"
                        type="button"
                        style={{
                          backgroundImage:
                            "url(" +
                            require("../../assets/img/theme/botao-destaque.png") +
                            ")",
                          minWidth: 150,
                        }}
                        onClick={() => this._prev()}
                      >
                        Anterior
                      </Button>
                    </div>
                  ) : (
                    <></>
                  )}
                </Row>
              </Form>
            </CardBody>
          </Card>
          {/* <Row className="mt-3">
            <Col xs="6">
              <Link to="/auth/login" className="text-light">Voltar para Login</Link>
            </Col>
            <Col className="text-right" xs="6">
              <Link to="/auth/reset" className="text-light">Esqueci Minha Senha</Link>
            </Col>
          </Row> */}
        </Col>
      </>
    );
  }
}

export default Register;
