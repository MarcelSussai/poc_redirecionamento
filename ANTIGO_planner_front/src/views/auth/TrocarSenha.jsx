import React from "react";
import { Redirect } from "react-router-dom";
import { confirmAlert } from "react-confirm-alert";
import "react-confirm-alert/src/react-confirm-alert.css";
import axios from "axios";

// reactstrap components
import {
  Button,
  Card,
  CardHeader,
  FormGroup,
  Form,
  Input,
  InputGroupAddon,
  InputGroupText,
  InputGroup,
  Row,
  Col,
} from "reactstrap";

class TrocarSenha extends React.Component {
  state = {
    email: null,
    senha: null,
    novaSenha: null,
    confirmacaoNovaSenha: null,
    redirect: false,
    confirmacaoNovaSenhaBorder: "none",
  };

  componentDidMount() {
    this.setState(
      {
        accessToken: localStorage.getItem("access-token"),
        empresaId: localStorage.getItem("empresa-id"),
        familiaId: localStorage.getItem("familia-id"),
      },
      function () {
        //Fazer algo
      }
    );
  }

  validaConfirmacaoDeSenha = (confirmacaoNovaSenha) => {
    var border = "none";

    if (this.state.novaSenha !== confirmacaoNovaSenha) {
      border = "1px solid red";
    }

    this.setState({
      confirmacaoNovaSenhaBorder: border,
      confirmacaoNovaSenha: confirmacaoNovaSenha,
    });
  };

  setRedirect = () => {
    this.setState({
      redirect: true,
    });
  };
  renderRedirect = () => {
    if (this.state.redirect) {
      return <Redirect to="/admin/index" />;
    }
  };

  trocarSenha = () => {
    var dados = {
      email: this.state.email,
      senha: this.state.senha,
      novaSenha: this.state.novaSenha,
    };

    var url =
      global.server_api_new +
      "a0285a3d-21a0-4efd-8f3f-8808c6a25c68" +
      "/auth/alterarsenha";

    var config = {
      headers: {
        Authorization: "bearer " + this.state.accessToken,
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Headers": "Authorization",
        "Access-Control-Allow-Methods":
          "GET, POST, OPTIONS, PUT, PATCH, DELETE",
      },
    };

    axios.post(url, dados, config).then((res) => {
      var options = null;

      if (res.data.success === true) {
        options = {
          title: "Alterar Senha",
          message: "Senha alterada com sucesso",
          buttons: [
            {
              label: "Ok",
              onClick: () => {
                this.setRedirect();
              },
            },
          ],
        };
      } else {
        console.log(res.data.exception);
        options = {
          title: "Falha ao Alterar Senha",
          message: res.data.exception.Message,
          buttons: [
            {
              label: "Ok",
            },
          ],
        };
      }

      confirmAlert(options);
    });
  };

  render() {
    return (
      <>
        {this.renderRedirect()}
        <Row>
          <Col lg="4" md="3"></Col>
          <Col lg="4" md="6">
            <Card className="bg-secondary shadow border-0">
              <CardHeader className="bg-transparent pb-2">
                <div className="text-muted text-center mt-2 mb-3">
                  <small>Informe seu e-mail, senha atual e nova senha</small>
                </div>
                <Form role="form">
                  <FormGroup className="mb-3">
                    <InputGroup className="input-group-alternative">
                      <InputGroupAddon addonType="prepend">
                        <InputGroupText>
                          <i className="ni ni-email-83" />
                        </InputGroupText>
                      </InputGroupAddon>
                      <Input
                        placeholder="E-mail"
                        type="email"
                        value={this.state.email}
                        onChange={(e) =>
                          this.setState({ email: e.target.value })
                        }
                      />
                    </InputGroup>
                  </FormGroup>
                  <FormGroup>
                    <InputGroup className="input-group-alternative">
                      <InputGroupAddon addonType="prepend">
                        <InputGroupText>
                          <i className="ni ni-lock-circle-open" />
                        </InputGroupText>
                      </InputGroupAddon>
                      <Input
                        placeholder="Senha Atual"
                        type="password"
                        value={this.state.senha}
                        onChange={(e) =>
                          this.setState({ senha: e.target.value })
                        }
                      />
                    </InputGroup>
                  </FormGroup>
                  <FormGroup>
                    <InputGroup className="input-group-alternative">
                      <InputGroupAddon addonType="prepend">
                        <InputGroupText>
                          <i className="ni ni-lock-circle-open" />
                        </InputGroupText>
                      </InputGroupAddon>
                      <Input
                        placeholder="Nova Senha"
                        type="password"
                        value={this.state.novaSenha}
                        onChange={(e) =>
                          this.setState({ novaSenha: e.target.value })
                        }
                      />
                    </InputGroup>
                  </FormGroup>
                  <FormGroup>
                    <InputGroup className="input-group-alternative">
                      <InputGroupAddon addonType="prepend">
                        <InputGroupText>
                          <i className="ni ni-lock-circle-open" />
                        </InputGroupText>
                      </InputGroupAddon>
                      <Input
                        style={{
                          border: this.state.confirmacaoNovaSenhaBorder,
                        }}
                        placeholder="Confirmar Nova Senha"
                        type="password"
                        value={this.state.confirmacaoNovaSenha}
                        onChange={(e) =>
                          this.validaConfirmacaoDeSenha(e.target.value)
                        }
                      />
                    </InputGroup>
                  </FormGroup>
                  <div className="text-center">
                    <Button
                      className="my-4"
                      color="primary"
                      type="button"
                      onClick={() => this.trocarSenha()}
                    >
                      Trocar Senha
                    </Button>
                  </div>
                </Form>
              </CardHeader>
            </Card>
          </Col>
          <Col lg="4" md="3"></Col>
        </Row>
      </>
    );
  }
}

export default TrocarSenha;
