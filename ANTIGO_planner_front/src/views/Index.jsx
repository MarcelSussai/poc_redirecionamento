import React from "react";
import { CircularProgress, Typography } from "@material-ui/core";
import { Redirect } from "react-router-dom";
import ApexChart from "react-apexcharts";
import SkyLight from "react-skylight";
import Rightbar from "../components/Sidebar/RightBar.jsx";
import axios from "axios";
import { confirmAlert } from "react-confirm-alert";
import Datepicker, { registerLocale } from "react-datepicker";
import MaskedInput from "react-maskedinput";
import br from "date-fns/locale/pt-BR";
import "react-datepicker/dist/react-datepicker.css";
import $ from "jquery";
import RestoreDate from "@material-ui/icons/Restore";
import Tooltip from "@material-ui/core/Tooltip";
import { withStyles } from "@material-ui/core/styles";
import Restore3Months from "@material-ui/icons/Looks3Outlined";
import WizardService from "../services/WizardService";
import "../assets/css/modal_index.css";
import { Button } from "reactstrap";
import PlanejadorView from './PlanejadorView'
import { Card, CardHeader, CardBody, Row, Col } from "reactstrap";

registerLocale("br", br);

class Index extends React.Component {
  moment = require("moment");

  constructor(props) {
    super(props);
    this.state = {
      activeNav: 1,
      estimateRealizedAreaChart: {
        options: {},
        series: [],
      },
      dadosOrcamento: {
        options: {
          labels: [],
        },
        series: [],
      },
      accessToken: localStorage.getItem("access-token"),
      empresaId: localStorage.getItem("empresa-id"),
      familiaId: localStorage.getItem("familia-id"),
      planejadorId: localStorage.getItem("planejador-id"),
      usuarioEmail: localStorage.getItem("usuario-email"),
      tipoUsuario: parseInt(localStorage.getItem("tipo-usuario")),
      startDate: localStorage.getItem("dataInicio")
        ? new Date(JSON.parse(localStorage.getItem("dataInicio")))
        : this.moment().startOf("month").toDate(),
      endDate: localStorage.getItem("dataFim")
        ? new Date(JSON.parse(localStorage.getItem("dataFim")))
        : this.moment().endOf("month").toDate(),
      //naoLembrar = localStorage.getItem('naoLembrarAtzApp')
      mostrarModalAposentadoria:
        localStorage.getItem("mostrarModalAposentadoria") == null
          ? true
          : false,
    };

    this.wizardService = new WizardService();
  }

  componentDidMount() {
    if (this.state.accessToken === null) {
      this.setRedirect();
      return;
    }
    // console.log(this.state.accessToken)
    this.verifyToken();
    this.verifyTermo();

    if (this.state.familiaId != null) {
      this.getData(true);
      this.getWizard();
    } else {
      this.encontrarFamiliasTotal();
      this.getAllUsuarios();
    }
    this.renderRedirectModalAposentadoria();

    global.mostrarAjudaDaPaginaAutomaticamente(this);
  }

  setRedirect = () => {
    this.setState({
      redirect: true,
    });
  };

  renderRedirect = () => {
    if (this.state.redirect) {
      return <Redirect to="/auth/login" />;
    }
  };

  setRedirectTermo = () => {
    this.setState({
      redirectTermo: true,
    });
  };

  renderRedirectTermo = () => {
    if (this.state.redirectTermo) {
      global.showModal(this.modalTermosPrivacidade);
    }
  };

  renderRedirectModalAposentadoria = () => {
    if (
      this.state.mostrarModalAposentadoria == true &&
      this.modalLibFinanceira != undefined
    ) {
      global.showModal(this.modalLibFinanceira);
    }
  };

  verifyToken = () => {
    if (global.email === "") {
      this.renderRedirect();
    }
  };

  verifyTermo = () => {
    //Obter dados para consulta, consultar
    // redirecionar  ou não
    var url =
      global.server_api_new +
      global.apiToken +
      "/Termo/verificacaoaceite/" +
      global.termoId +
      "/" +
      this.state.usuarioEmail;
    //https://localhost:5002/api/APIKEY/Termo/verificacaoaceite/TERMO-ID/EMAIL;
    //console.log(url);
    var config = {
      headers: {
        Authorization: "bearer " + this.state.accessToken,
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Headers": "Authorization",
        "Access-Control-Allow-Methods":
          "GET, POST, OPTIONS, PUT, PATCH, DELETE",
      },
    };

    axios.get(url, config).then((res) => {
      try {
        // console.log(res.data);
        if (res.data.code === 200) {
          //
        } else {
          //
          this.setRedirectTermo();
        }
      } catch (error) {
        /*console.log("Entrou catch"); */
        console.log(error);
      }
    });
  };

  updateTermo = () => {
    //console.log(url);
    let checkprivac = document.getElementById("chkprivac");

    if (!checkprivac.checked) {
      alert("É necessário preencher a caixa de aceite para continuar");
      return;
    }

    var url =
      global.server_api_new +
      global.apiToken +
      "/Termo/cadastroaceite/" +
      global.termoId +
      "/" +
      this.state.usuarioEmail;
    //https://localhost:5002/api/{apiKey}/Termo/cadastroaceite/{termo-Id}/{emailUser};

    var config = {
      headers: {
        Authorization: "bearer " + this.state.accessToken,
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Headers": "Authorization",
        "Access-Control-Allow-Methods":
          "GET, POST, OPTIONS, PUT, PATCH, DELETE",
      },
    };
    try {
      axios.get(url, config).then((res) => {
        //console.log(url, res);
        if (res.data.code === 200) {
          alert("Obrigado! Você será redirecionado..");
          //window.history.back(2);
          this.naoLembrar();
          window.location = "/admin/index";
        } else {
          alert(res.data.message);
        }
      });
    } catch (error) {
      console.log(error);
    }
  };

  getData = (start) => {
    if (this.state.accessToken === null) {
      this.setRedirect();
      return;
    }

    if (start === true) {
      var $this = this;
      this.setStartDate(this.state.startDate, function () {
        $this.setEndDate($this.state.endDate, function () {
          $this.atualizarDadosTipoOrcamento(true);
        });
      });
    }
  };

  getWizard = () => {
    let $this = this;

    this.wizardService.buscarStatusDeConfiguracao(
      this.state.accessToken,
      this.state.familiaId,
      function (data) {
        if (data.success && data.singleResult === null) {
          const options = {
            title: "Primeiro Acesso?",
            message:
              "Verificamos que você não executou a Configuração Inicial. Deseja fazer isso agora? Recomendamos que a configuração inicial seja realizada para maior facilidade no uso da ferramenta.",
            buttons: [
              {
                label: "Lembrar mais tarde",
              },
              {
                label: "Não perguntar mais",
                onClick: () => {
                  //Marcado para não visualizar mais mensagem na home
                  $this.wizardService.configuracaoIniciada(
                    $this.state.accessToken,
                    $this.state.familiaId
                  );
                },
              },
              {
                label: "Sim",
                onClick: () => {
                  window.location.href = "/admin/wizard";
                },
              },
            ],
          };

          confirmAlert(options);
        }
      }
    );
  };

  naoLembrar = () => {
    localStorage.setItem("naoLembrarAtzApp", true);
    window.location = "/admin/index";
  };

  naoLembrarAposentadoria = () => {
    localStorage.setItem("mostrarModalAposentadoria", false);
    this.modalLibFinanceira.hide();
  };

  getAllUsuarios = () => {
    //var url = global.server_api + 'api/Pessoa/planejador/' + this.state.planejadorId + "/filtro/";
    var url =
      global.server_api_new +
      global.apiToken +
      "/Pessoa/planejador/" +
      this.state.planejadorId +
      "/filtro/";
    var config = {
      headers: {
        Authorization: "bearer " + this.state.accessToken,
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Headers": "Authorization",
        "Access-Control-Allow-Methods":
          "GET, POST, OPTIONS, PUT, PATCH, DELETE",
      },
    };
    //console.log(url);
    axios.post(url, { ApenasNome: true }, config).then((res) => {
      this.setState({ totalUsuarios: res.data.results });
    });
  };

  atualizarDadosOrcamento = () => {
    //var url = global.server_api + 'api/dashboard/orcamento-previsto-realizado/' + this.state.familiaId;
    var url =
      global.server_api_new +
      global.apiToken +
      "/dashboard/orcamento-previsto-realizado/" +
      this.state.familiaId;

    var filtro = {
      DataInicial: this.state.startDate,
      DataFinal: this.state.endDate,
    };

    var config = {
      headers: {
        Authorization: "bearer " + this.state.accessToken,
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Headers": "Authorization",
        "Access-Control-Allow-Methods":
          "GET, POST, OPTIONS, PUT, PATCH, DELETE",
      },
    };

    axios.post(url, filtro, config).then((res) => {
      // console.log(res.data);
      var dadosOrcamento = res.data.results;

      var labels = [];
      var series = [];
      var totalMedio = 0;

      if (dadosOrcamento != null && dadosOrcamento.length > 0) {
        dadosOrcamento.forEach((item) => {
          if (item.totalMedio > 0 && item.tipoOrcamento !== 0) {
            labels.push(item.nome);
            totalMedio = Number(item.totalMedio.toFixed(2));
            series.push(totalMedio);
          }
        });
      }

      dadosOrcamento = {
        options: {
          labels: labels,
          dataLabels: {
            style: {
              fontFamily: "Open Sans, sans-serif",
            },
          },
          legend: {
            position: "right",
            fontFamily: "Open Sans, sans-serif",
            fontSize: "11px",
            markers: {
              offsetY: "1.5px",
            },
          },
          chart: {
            background: "#ffffff",
          },
        },
        series: series,
      };

      this.setState({ dadosOrcamento });
    });
  };

  encontrarFamiliasTotal = () => {
    var config = {
      headers: {
        Authorization: "bearer " + this.state.accessToken,
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Headers": "Authorization",
        "Access-Control-Allow-Methods":
          "GET, POST, OPTIONS, PUT, PATCH, DELETE",
      },
    };

    if (this.state.planejadorId != null) {
      //var urlPlanejador = global.server_api + 'api/familia/planejador/filtro/' + this.state.planejadorId;
      var urlPlanejador =
        global.server_api_new +
        global.apiToken +
        "/familia/planejador/filtro/" +
        this.state.planejadorId;
      axios
        .post(urlPlanejador, {}, config)
        .then((res) => this.setState({ familias: res.data.results }));
    }
  };

  atualizarDadosTipoOrcamento = (startScreen) => {
    //var url = global.server_api + 'api/dashboard/tipo-orcamento-previsto-realizado/' + this.state.familiaId;
    var url =
      global.server_api_new +
      global.apiToken +
      "/dashboard/tipo-orcamento-previsto-realizado/" +
      this.state.familiaId;

    var filtro = {
      DataInicial: this.state.startDate,
      DataFinal: this.state.endDate,
    };

    var config = {
      headers: {
        Authorization: "bearer " + this.state.accessToken,
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Headers": "Authorization",
        "Access-Control-Allow-Methods":
          "GET, POST, OPTIONS, PUT, PATCH, DELETE",
      },
    };

    axios.post(url, filtro, config).then((res) => {
      // console.log(res.data.results);
      if (res.data.exception) {
        console.log(res.data.exception);
        const options = {
          title: "Erro",
          message: res.data.exception.Message,
          buttons: [
            {
              label: "Ok",
            },
          ],
        };

        confirmAlert(options);

        return;
      }

      if (startScreen) {
        this.abrirMenuLateral();
      }

      const dados = res.data.results;

      var nomesTipoOrcamento = [];
      var tiposOrcamentoEstimados = [];
      var tiposOrcamentoRealizados = [];

      if (dados != null) {
        dados.forEach((item) => {
          nomesTipoOrcamento.push(item.nome);
          tiposOrcamentoEstimados.push(
            item.estimado != null ? item.estimado : 0
          );
          if (item.estimado === item.totalMedio) {
            tiposOrcamentoRealizados.push(
              item.totalMedio != null ? item.totalMedio : 0
            );
          } else {
            tiposOrcamentoRealizados.push(
              item.totalMedio != null ? item.totalMedio : 0
            );
          }
        });
      }

      // this.setState({
      //   colors: colors
      // })

      // console.log(this.state.colors);

      this.setState(
        {
          nomesTipoOrcamento: nomesTipoOrcamento,
          tiposOrcamentoEstimados: tiposOrcamentoEstimados,
          tiposOrcamentoRealizados: tiposOrcamentoRealizados,
        },
        function () {
          // console.log(this.state);
          this.atualizarDadosOrcamento();

          this.setState(
            {
              estimateRealizedAreaChart: {
                options: {
                  legend: {
                    show: false,
                  },
                  plotOptions: {
                    bar: {
                      horizontal: false,
                      endingShape: "rounded",
                      dataLabels: {
                        position: "top",
                      },
                    },
                  },
                  dataLabels: {
                    enabled: true,
                    textAnchor: "middle",
                    formatter: function (val) {
                      return val != null
                        ? val
                            .toLocaleString("pt-BR", {
                              style: "currency",
                              currency: "BRL",
                            })
                            .replace("R$", "")
                        : "";
                    },
                    hideOverflowingLabels: true,
                    offsetY: -20,
                    style: {
                      fontSize: "11px",
                      fontFamily: "Open Sans, sans-serif",
                      colors: ["#57657D"],
                      textAlign: "center",
                    },
                  },
                  stroke: {
                    curve: "smooth",
                  },
                  xaxis: {
                    categories: this.state.nomesTipoOrcamento,
                    labels: {
                      style: {
                        fontFamily: "Open Sans, sans-serif",
                        fontWeight: 800,
                        fontSize: 13,
                      },
                    },
                  },
                  yaxis: {
                    labels: {
                      formatter: function (value) {
                        return value != null
                          ? value
                              .toLocaleString("pt-BR", {
                                style: "currency",
                                currency: "BRL",
                              })
                              .replace("R$", "")
                          : "";
                      },
                      style: {
                        fontWeight: 600,
                      },
                    },
                  },
                  colors: ["#9A3DFF", "#28ADC0", "#fa0000"],
                },
                series: [
                  {
                    name: "Estimativa",
                    data: this.state.tiposOrcamentoEstimados,
                  },
                  {
                    name: "Realizado",
                    data: this.state.tiposOrcamentoRealizados,
                  },
                ],
              },
            },
            function () {
              //console.log(this.state.estimateRealizedAreaChart);
              $(
                ".home-estimate-realized > div > svg > foreignObject > div"
              ).removeClass("center");
            }
          );
        }
      );
    });
  };

  setStartDate = (date, callback) => {
    if (date != null) {
      date.setHours(0);
      date.setMinutes(0, 0, 0);
      localStorage.setItem("dataInicio", JSON.stringify(date));
    }

    const startDate = date;

    this.setState({ startDate: startDate }, function () {
      //console.log('Gravado: ' + JSON.stringify(startDate));
      document.getElementById("rightbar-startdate-refresh").click();

      if (callback != null) {
        callback();
      } else {
        this.atualizarDadosTipoOrcamento();
      }
    });
  };

  setEndDate = (date, callback) => {
    if (date != null) {
      date.setHours(20);
      date.setMinutes(59, 59, 0);
      localStorage.setItem("dataFim", JSON.stringify(date));
    }

    const endDate = date;

    this.setState({ endDate: endDate }, function () {
      //console.log('Gravado: ' + JSON.stringify(endDate));
      document.getElementById("rightbar-enddate-refresh").click();

      if (callback != null) {
        callback();
      } else {
        this.atualizarDadosTipoOrcamento();
      }
    });
  };

  gerarDatePicker = (id, callback, defaultDate, type) => {
    var currentDate = null;

    if (!defaultDate) {
      currentDate = new Date();
    } else {
      if (type === "filtro-inicio" && this.state.startDate != null) {
        currentDate = this.state.startDate;
      } else if (type === "filtro-fim" && this.state.endDate != null) {
        currentDate = this.state.endDate;
      } else {
        currentDate = defaultDate.toDate();
      }
    }

    return (
      <Datepicker
        locale="br"
        id={id}
        dateFormat="dd/MM/yyyy"
        selected={currentDate}
        showYearDropdown
        dateFormatCalendar="MMMM"
        scrollableYearDropdown
        yearDropdownItemNumber={5}
        onChange={(date) => callback(date)}
        customInput={<MaskedInput mask="11/11/1111" />}
      />
    );
  };

  abrirMenuLateral = () => {
    $("#right-sidenav-main").click();
  };

  renderModalClientesEmpty = () => {
    return (
      <div>
        <SkyLight
          ref={(ref) => (this.modalCliente = ref)}
          transitionDuration={0}
          beforeOpen={this.preCarregarParaEdicao}
          afterClose={this.limparSelecao}
          title={
            <>
              <div className="pop-up-title">
                <i className="ni ni-check-bold"></i> <h2>Comece agora</h2>
              </div>
            </>
          }
        >
          <p>
            Você ainda não tem nenhum cliente cadastrado, deseja fazer isso
            agora?
          </p>
          <button
            className="featured-button"
            onClick={() => {
              let path = "/admin/clientes/1";
              this.props.history.push(path);
            }}
            style={{
              backgroundImage:
                "url(" +
                require("../assets/img/theme/botao-destaque.png") +
                ")",
            }}
          >
            Começar
          </button>
        </SkyLight>
      </div>
    );
  };

  renderModalAvisoClientes = () => {
    return (
      <div>
        <SkyLight
          ref={(ref) => (this.modalCliente = ref)}
          transitionDuration={0}
          beforeOpen={this.preCarregarParaEdicao}
          afterClose={this.limparSelecao}
          title={
            <>
              <div className="pop-up-title">
                <i className="ni ni-check-bold"></i> <h2>Comece agora</h2>
              </div>
            </>
          }
        >
          <p>
            “Seus clientes cadastrados na plataforma representa uma família e
            cada usuário, são os membros desta família que terão acesso ao
            VISTA. Ex: Cliente: Família Silva, Usuário: José; Usuário: Maria”
          </p>
          <button
            className="featured-button"
            onClick={() => {
              let path = "/admin/index";
              this.props.history.push(path);
            }}
            style={{
              backgroundImage:
                "url(" +
                require("../assets/img/theme/botao-destaque.png") +
                ")",
            }}
          >
            Entendí
          </button>
        </SkyLight>
      </div>
    );
  };

  renderModalUsuariosEmpty = () => {
    return (
      <div>
        <SkyLight
          ref={(ref) => (this.modal = ref)}
          transitionDuration={0}
          beforeOpen={this.preCarregarParaEdicao}
          afterClose={this.limparSelecao}
          title={
            <>
              <div className="pop-up-title">
                <i className="ni ni-check-bold"></i> <h2>Comece agora</h2>
              </div>
            </>
          }
        >
          <p>
            Você ainda não possui usuários cadastrados para seus clientes,
            deseja realizar este cadastro agora?
          </p>
          <button
            className="featured-button"
            onClick={() => {
              let path = "/admin/usuarios/1";
              this.props.history.push(path);
            }}
            style={{
              backgroundImage:
                "url(" +
                require("../assets/img/theme/botao-destaque.png") +
                ")",
            }}
          >
            Começar
          </button>
        </SkyLight>
      </div>
    );
  };

  mostrarCardGastosPorOrcamento = () => {
    return (
      <>
        <Card
          className="mt-5 p-4 shadow"
          style={{ borderRadius: 15, overflow: "overlay" }}
        >
          <CardHeader className="border-0">
            <Row className="align-items-center">
              <div className="col">
                <img
                  src={require("../assets/img/theme/icone-analytics.png")}
                  alt="Orçamentos Realizados"
                  className="before-title-img before-title-img-2"
                />
                <h3 className="mb-0 chart-title">
                  Orçamentos{" "}
                  <span className="chart-title-span">Realizados</span>
                </h3>
              </div>
            </Row>
          </CardHeader>
          <div style={{ display: "flex", justifyContent: "center" }}>
            <div className="chart-expense-adjust" style={{ width: 500 }}>
              <ApexChart
                options={this.state.dadosOrcamento.options}
                series={this.state.dadosOrcamento.series}
                className="chart-expense-2"
                type="donut"
              />
            </div>
          </div>
        </Card>
      </>
    );
  };

  renderModalPrivacidade = () => {
    return (
      <div className="botaotermos">
        <SkyLight
          closeOnEsc={false}
          ref={(ref) => (this.modalTermosPrivacidade = ref)}
          transitionDuration={500}
          title={
            <>
              <div>
                <h2 className="modal_title text-center">
                  Atualização de Termos de Uso e Política de Privacidade
                </h2>
              </div>
            </>
          }
        >
          <div className="modal_pesquisa">
            <p className="modal_text ajuda-titulo pb-3">
              Caro cliente, <br />
              <br />
              Estamos atualizando nossos termos de uso e políticas de
              privacidade, proporcionando mais segurança e confiabilidade, por
              favor leia com atenção os termos e condições. Eles dizem respeito
              à relação entre o VISTA e você, usuário do serviço.
              <br />
              <br />
              Obrigado!
            </p>

            <div className="form-check">
              <input
                className="form-check-input check-termos"
                type="checkbox"
                value=""
                id="chkprivac"
              />
              <label>
                {" "}
                Aceitar a{" "}
                <a
                  href="termosprivacidade"
                  target="_blank"
                  className="link_pol"
                >
                  Política de Privacidade{" "}
                </a>{" "}
                e{" "}
                <a href="termosuso" target="_blank" className="link_pol">
                  {" "}
                  Termos de Uso
                </a>
                .
              </label>
            </div>
            <p className="modal_text_footer py-4">
              Atensiosamente,
              <br />
              Equipe Vista
            </p>
            <Button
              className="featured-button my-4"
              color="primary"
              type="button"
              style={{
                backgroundImage:
                  "url(" +
                  require("../assets/img/theme/botao-destaque.png") +
                  ")",
                minWidth: 150,
              }}
              onClick={() => this.updateTermo()}
            >
              Assinar
            </Button>
          </div>
        </SkyLight>
      </div>
    );
  };

  renderModalPesquisaPlanejador = () => {
    return (
      <div>
        <SkyLight
          ref={(ref) => (this.modalPesquisaPlanejador = ref)}
          transitionDuration={500}
          title={
            <>
              <div>
                <h2 className="modal_title"> Atualização do App Meu Vista</h2>
              </div>
            </>
          }
        >
          <div className="modal_pesquisa">
            <p className="modal_text ajuda-titulo">
              O VISTA está cada vez mais preocupado com a experiência de todos
              os seus usuários no uso da Plataforma.
            </p>
            <p className="modal_text pt-3 text-justify">
              Pensando nisso, realizamos uma atualização de melhoria no sistema
              e por conta disso o APP Meu Vista, Versão iOS, encontra-se
              temporariamente indisponível. Mas não se preocupe, assim que a
              Apple o atualizar na loja, enviaremos um e-mail informando.
              <br />
              <br />A nova versão Android já está disponível na Play Store.
            </p>
            <p className="modal_text_footer pt-4">
              Qualquer dúvida estamos à disposição,
            </p>
            <p className="modal_text_footer">Equipe Vista</p>
            <div></div>
            <Button
              className="featured-button my-4"
              color="primary"
              type="button"
              style={{
                backgroundImage:
                  "url(" +
                  require("../assets/img/theme/botao-destaque.png") +
                  ")",
                minWidth: 150,
              }}
              onClick={() => this.naoLembrar()}
            >
              Não Lembrar
            </Button>
          </div>
        </SkyLight>
      </div>
    );
  };

  renderColMateriaisVista = () => {
    return (
      <Col
        md="6"
        lg="4"
        xl="4"
        style={{ cursor: "pointer" }}
        onClick={() => {
          let path = "/admin/materiais";
          this.props.history.push(path);
        }}
      >
        <Card
          style={{
            background: "#9A3DFF",
            borderRadius: "1rem",
            color: "white",
            padding: 10,
            minWidth: 200,
            marginBottom: 30,
          }}
        >
          <CardHeader
            style={{
              background: "#9A3DFF",
              borderBottomWidth: "0px",
              borderRadius: "1rem 1rem 0 0",
              paddingBottom: 0,
            }}
          >
            <center style={{ fontWeight: "100" }}>
              <b>MATERIAIS VISTA</b>
            </center>
            <div
              style={{
                width: "100%",
                height: "1px",
                background: "white",
                marginTop: "1rem",
              }}
            ></div>
          </CardHeader>
          <CardBody
            style={{
              paddingTop: "0rem",
              paddingBottom: "10px",
            }}
          >
            <center>
              <p style={{ margin: "0", paddingTop: 10, paddingBottom: 10 }}>
                Acesse os materiais sobre a plataforma
              </p>
            </center>
            <div style={{ textAlign: "center" }}>
              <i
                className="fas fa-book-open"
                style={{
                  margin: ".5rem",
                  textAlign: "center",
                  borderRadius: "50%",
                  background: "white",
                  padding: ".5rem",
                  color: "#9A3DFF",
                }}
              ></i>
            </div>
          </CardBody>
        </Card>
      </Col>
    );
  };

  renderColTreinamento = () => {
    return (
      <Col
        md="6"
        lg="4"
        xl="4"
        style={{ cursor: "pointer" }}
        onClick={() => {
          var win = window.open("https://meuvista.com/treinamentos", "_blank");
          win.focus();
        }}
      >
        <Card
          style={{
            background: "#9A3DFF",
            borderRadius: "1rem",
            color: "white",
            padding: 10,
            minWidth: 200,
            marginBottom: 30,
          }}
        >
          <CardHeader
            style={{
              background: "#9A3DFF",
              borderBottomWidth: "0px",
              borderRadius: "1rem 1rem 0 0",
              paddingBottom: 0,
            }}
          >
            <center style={{ fontWeight: "100" }}>
              <b>TREINAMENTOS</b>
            </center>
            <div
              style={{
                width: "100%",
                height: "1px",
                background: "white",
                marginTop: "1rem",
              }}
            ></div>
          </CardHeader>
          <CardBody
            style={{
              paddingTop: "0rem",
              paddingBottom: "10px",
            }}
          >
            <center>
              <p style={{ margin: "0", paddingTop: 10, paddingBottom: 10 }}>
                Conheça os treinamentos do Vista
              </p>
            </center>
            <div style={{ textAlign: "center" }}>
              <i
                className="fas fa-dumbbell"
                style={{
                  margin: ".5rem",
                  textAlign: "center",
                  borderRadius: "50%",
                  background: "white",
                  padding: ".5rem",
                  color: "#9A3DFF",
                }}
              ></i>
            </div>
          </CardBody>
        </Card>
      </Col>
    );
  };

  renderColTutoriais = () => {
    return (
      <Col
        md="6"
        lg="4"
        xl="4"
        style={{ cursor: "pointer", color: "white" }}
        onClick={() => {
          let path = "/admin/tutoriais";
          this.props.history.push(path);
        }}
      >
        <Card
          style={{
            background: "#9A3DFF",
            borderRadius: "1rem",
            color: "white",
            padding: 10,
            minWidth: 200,
            marginBottom: 30,
          }}
        >
          <CardHeader
            style={{
              background: "#9A3DFF",
              borderBottomWidth: "0px",
              borderRadius: "1rem 1rem 0 0",
              paddingBottom: 0,
            }}
          >
            <center style={{ fontWeight: "100" }}>
              <b>TUTORIAIS</b>
            </center>
            <div
              style={{
                width: "100%",
                height: "1px",
                background: "white",
                marginTop: "1rem",
              }}
            ></div>
          </CardHeader>
          <CardBody
            style={{
              paddingTop: "0rem",
              paddingBottom: "10px",
            }}
          >
            <center>
              <p style={{ margin: "0", paddingTop: 10, paddingBottom: 10 }}>
                Tire as dúvidas com nossos vídeos
              </p>
            </center>
            <div style={{ textAlign: "center" }}>
              <i
                className="fas fa-play"
                style={{
                  margin: ".5rem",
                  textAlign: "center",
                  borderRadius: "50%",
                  background: "white",
                  padding: ".5rem",
                  color: "#9A3DFF",
                }}
              ></i>
            </div>
          </CardBody>
        </Card>
      </Col>
    );
  };

  renderColEventos = () => {
    return (
      <Col
        md="6"
        lg="4"
        xl="4"
        style={{ cursor: "pointer" }}
        onClick={() => {
          let path = "/admin/eventos";
          this.props.history.push(path);
        }}
      >
        <Card
          style={{
            background: "#feae00",
            borderRadius: "1rem",
            color: "black",
            padding: 10,
            minWidth: 200,
            marginBottom: 30,
          }}
        >
          <CardHeader
            style={{
              background: "#feae00",
              borderBottomWidth: "0px",
              borderRadius: "1rem 1rem 0 0",
              paddingBottom: 0,
            }}
          >
            <center style={{ fontWeight: "100" }}>
              <b>EVENTOS</b>
            </center>
            <div
              style={{
                width: "100%",
                height: "1px",
                background: "black",
                marginTop: "1rem",
              }}
            ></div>
          </CardHeader>
          <CardBody
            style={{
              paddingTop: "0rem",
              paddingBottom: "10px",
            }}
          >
            <center>
              <p style={{ margin: "0", paddingTop: 10, paddingBottom: 10 }}>
                Veja aqui os próximos eventos para planejadores Vista
              </p>
            </center>
          </CardBody>
        </Card>
      </Col>
    );
  };

  renderColLeads = () => {
    return (
      <Col
        md="6"
        lg="4"
        xl="4"
        style={{ cursor: "pointer" }}
        onClick={() => {
          let path = "/admin/IndexLeads";
          this.props.history.push(path);
        }}
      >
        <Card
          style={{
            background: "#feae00",
            borderRadius: "1rem",
            color: "black",
            padding: 10,
            minWidth: 200,
            marginBottom: 30,
          }}
        >
          <CardHeader
            style={{
              background: "#feae00",
              borderBottomWidth: "0px",
              borderRadius: "1rem 1rem 0 0",
              paddingBottom: 0,
            }}
          >
            <center style={{ fontWeight: "100" }}>
              <b>LEADS</b>
            </center>
            <div
              style={{
                width: "100%",
                height: "1px",
                background: "black",
                marginTop: "1rem",
              }}
            ></div>
          </CardHeader>
          <CardBody
            style={{
              paddingTop: "0rem",
              paddingBottom: "10px",
            }}
          >
            <center>
              <p style={{ margin: "0", paddingTop: 10, paddingBottom: 10 }}>
                Se cadastre aqui para receber Leads
              </p>
            </center>
          </CardBody>
        </Card>
      </Col>
    );
  };

  renderColPesquisa = () => {
    return (
      <Col
        md="6"
        lg="4"
        xl="4"
        style={{ cursor: "pointer" }}
        onClick={() => {
          global.showModal(this.modalPesquisaPlanejador);
        }}
      >
        <Card
          style={{
            background: "#feae00",
            borderRadius: "1rem",
            color: "black",
            padding: 10,
            minWidth: 200,
            marginBottom: 30,
          }}
        >
          <CardHeader
            style={{
              background: "#feae00",
              borderBottomWidth: "0px",
              borderRadius: "1rem 1rem 0 0",
              paddingBottom: 0,
            }}
          >
            <center style={{ fontWeight: "100" }}>
              <b>AVISOS</b>
            </center>
            <div
              style={{
                width: "100%",
                height: "1px",
                background: "black",
                marginTop: "1rem",
              }}
            ></div>
          </CardHeader>
          <CardBody
            style={{
              paddingTop: "0rem",
              paddingBottom: "10px",
            }}
          >
            <center>
              <p style={{ margin: "0", paddingTop: 10, paddingBottom: 10 }}>
                Clique aqui e responda a nossa pesquisa
              </p>
            </center>
          </CardBody>
        </Card>
      </Col>
    );
  };

  render() {
    const TooltipRestore = withStyles((theme) => ({
      tooltip: {
        backgroundColor: "#888888",
        color: "white",
        fontSize: 12,
        fontWeight: "bold",
        fontFamily: "Open Sans",
      },
    }))(Tooltip);

    if (this.state.familiaId != null) {
      return (
        <>
          {this.renderRedirectTermo()}
          {this.renderRedirectModalAposentadoria()}
          {this.renderModalPesquisaPlanejador()}
          {this.renderModalPrivacidade()}
          {this.renderModalAvisoClientes()}
          {this.renderModalUsuariosEmpty()}
          {this.renderModalClientesEmpty()}
          {/* <Header /> */}
          {/* Page content */}
          <div id="tela-cadastro-lancamento"></div>
          <div className="home-index" style={{ padding: "1% 3%" }}>
            <div className="home-summary-top">
              <Row>
                <Col lg="12" xl="12" style={{ marginBottom: "15px" }}>
                  <div>
                    <Card>
                      <CardHeader
                        className="border-0"
                        style={{ paddingLeft: "0px", paddingRight: 5 }}
                      >
                        <Row>
                          <div className="col">
                            <img
                              src={require("../assets/img/theme/icone-analytics.png")}
                              className="before-title-img"
                              alt=""
                            />
                            <h2 className="mb-0 chart-title">Home</h2>
                          </div>
                        </Row>

                        <Row className="mt-5">
                          <Col
                            md="12"
                            lg="7"
                            xl="7"
                            className="filterAndOnder period mt-2"
                            style={{ float: "left", paddingRight: 0 }}
                          >
                            <label>Período </label>
                            {this.gerarDatePicker(
                              "data-inicial",
                              this.setStartDate,
                              this.moment().startOf("month"),
                              "filtro-inicio"
                            )}
                            {this.gerarDatePicker(
                              "data-final",
                              this.setEndDate,
                              this.moment().endOf("month"),
                              "filtro-fim"
                            )}
                            <TooltipRestore
                              disableFocusListener
                              disableTouchListener
                              title="Clique para ver apenas o orçamento do mês atual"
                            >
                              <RestoreDate
                                style={{ cursor: "pointer" }}
                                onClick={() => {
                                  this.setStartDate(
                                    this.moment().startOf("month").toDate()
                                  );
                                  this.setEndDate(
                                    this.moment().endOf("month").toDate()
                                  );
                                }}
                              />
                            </TooltipRestore>
                            <TooltipRestore
                              disableFocusListener
                              disableTouchListener
                              title="Clique para ver o orçamento dos últimos três meses"
                            >
                              <Restore3Months
                                style={{ cursor: "pointer" }}
                                onClick={() => {
                                  this.setStartDate(
                                    this.moment()
                                      .subtract(3, "months")
                                      .startOf("month")
                                      .toDate()
                                  );
                                  this.setEndDate(
                                    this.moment()
                                      .subtract(1, "months")
                                      .endOf("month")
                                      .toDate()
                                  );
                                }}
                              />
                            </TooltipRestore>
                          </Col>

                          <Col
                            md="12"
                            lg="5"
                            xl="5"
                            style={{ float: "left" }}
                            className="mt-2"
                          >
                            <Row>
                              <i
                                className="fas fa-chart-bar before-title-img ml-3"
                                style={{ color: "#9A3DFF" }}
                              ></i>
                              <h4 className="mb-0 chart-title mr-3">
                                Estimativa
                              </h4>

                              <i
                                className="fas fa-chart-bar before-title-img"
                                style={{ color: "#28ADC0" }}
                              ></i>
                              <h4 className="mb-0 chart-title mr-3">
                                Realizado
                              </h4>
                            </Row>
                          </Col>
                        </Row>
                      </CardHeader>
                    </Card>
                  </div>
                </Col>
              </Row>
            </div>
            <ApexChart
              options={this.state.estimateRealizedAreaChart.options}
              series={this.state.estimateRealizedAreaChart.series}
              type="bar"
              width="100%"
              height="350px"
              className="home-estimate-realized"
              style={{ marginLeft: "-20px" }}
            />
            <div>
              <Row>
                <Col lg="12" xl="12">
                  {this.mostrarCardGastosPorOrcamento()}
                </Col>
              </Row>
            </div>
          </div>
          <Rightbar
            {...this.props}
            logo={{
              innerLink: "/admin/index",
              imgSrc: require("../assets/img/brand/logo-nova-branca.png"),
              imgAlt: "...",
            }}
            collapseOpen={true}
            id="right-sidenav-main"
            style={this.state.familiaId === null ? { display: "none" } : {}}
            position="fixed-right"
            menuItens={[
              "sumarioPrevistoRealizadoGeral",
              "ultimosLancamentos",
              "gastosPorCategoria",
            ]}
          />
          {this.renderRedirect()}
        </>
      );
    } else {
      return (
        <div>
            {this.renderRedirectTermo()}
            {this.renderModalAvisoClientes()}
            {this.renderModalUsuariosEmpty()}
            {this.renderModalClientesEmpty()}
            {this.renderRedirect()}
            {this.renderModalPesquisaPlanejador()}
            {this.renderModalPrivacidade()}
            <PlanejadorView 
              qtdFamilias={this.state?.familias?.length ?? 0} 
              qtdUsuarios={this.state?.totalUsuarios?.length ?? 0}
            />
        </div>
      );
    }
  }
}

export default Index;
