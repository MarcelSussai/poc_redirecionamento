import React from 'react'
import styled from 'styled-components'

const TitleText = styled.p`
  color: rgb(141, 55, 248);
  margin-bottom: 0.15vw;
  font-size: x-large;
`

export default () => {
  return (
    <TitleText>Bem-vindo ao Meu Vista</TitleText>
  )
}