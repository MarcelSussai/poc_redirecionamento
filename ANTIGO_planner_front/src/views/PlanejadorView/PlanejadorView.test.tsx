import React from 'react'
import PlanejadorView from './PlanejadorView'
import { screen, render } from '@testing-library/react';

describe('PlanejadorView', () => {
    it('Should contain the title', () => {
        render(<PlanejadorView qtdFamilias={1} qtdUsuarios={2} />)
        const title = screen.getByText('Bem-vindo ao Meu Vista')
        expect(title).not.toBeNull()
    })
})