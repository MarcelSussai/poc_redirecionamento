import React from 'react'
import Titulo from './Titulo'
import { Card, IconCard } from './Card'
import styled from 'styled-components'
import { getRoute, getRoutePath } from '../../routes'
import communityButtonIcon from './Card/images/community.png'
import ebookIcon from './Card/images/ebook.png'
import aceleracaoButtonIcon from './Card/images/aceleracaoIcone.png'
import assistenteButtonIcon from './Card/images/assistente.png'
import nordIcon from './Card/images/nord.svg'
import UserIcon from './Card/images/user.png'
import FamilyIcon from './Card/images/family.png'
import Aceleracao from './Aceleracao'

interface Props {
  qtdFamilias: number
  qtdUsuarios: number
}

const Container = styled.div`
  margin-left: 5vw;
  margin-right: 5vw;
  margin-top: 2vw;
  display: flex;
  flex-direction: column;
`

const Row = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  margin-top: 2.5vw;
`

export default ({qtdFamilias, qtdUsuarios}: Props) => {
  return (
    <Container>
      <Titulo />
      <Row>
        <Card
          title={`${qtdFamilias} Clientes`}
          icon={FamilyIcon}
          description="Veja e edite todos os clientes cadastrados"
          color="cyan"
          goToButton={{ link: getRoutePath(getRoute("Clientes")) }}
          plusButton={{ link: getRoutePath(getRoute("Clientes - Novo")) }}
        />
        <Card
          title={`${qtdUsuarios} Usuários`}
          icon={UserIcon}
          description="Veja e edite todos seus usuários"
          color="lightGreen"
          goToButton={{ link: getRoutePath(getRoute("Usuários")) }}
          plusButton={{ link: getRoutePath(getRoute("Usuários - Novo")) }}
        />
        <Card
          title="Faturas Vista"
          description="Veja aqui suas faturas do Vista"
          color="lightBlue"
          goToButton={{ link: getRoutePath(getRoute("Fatura Vista")) }}
        />
        <Card
          title="Tutoriais"
          description="Assista aos tutoriais do Vista"
          goToButton={{ link: 'https://vimeo.com/showcase/tutoriaisvista', newTab: true }}
        />
      </ Row>
      <Row style={{ justifyContent: 'flex-end' }}>
        <IconCard
          title="Nord"
          iconSource={nordIcon}
          goToButton={{ link: 'https://lps.nordresearch.com.br/nord-selecao-parceiro-vista' }}
        />
        <Aceleracao />
      </Row>
      <Row>
        <IconCard
          title="E-BOOK"
          iconSource={ebookIcon}
          goToButton={{ link: 'https://materiais.meuvista.com/ebook-plano-de-negocios-2' }}
        />
        <IconCard
          title="ACELERAÇÃO VISTA"
          iconSource={aceleracaoButtonIcon}
          goToButton={{ link: 'https://meuvista.com/aceleracao/' }}
        />
        <IconCard
          title="ASSISTENTE VIRTUAL"
          iconSource={assistenteButtonIcon}
          goToButton={{ link: 'https://meuvista.com/configurando-o-vista/' }}
        />
        <IconCard
          title="COMUNIDADE"
          iconSource={communityButtonIcon}
          goToButton={{ link: 'https://comunidadedeplanejadores.com' }}
        />
      </Row>
    </Container>
  )
}