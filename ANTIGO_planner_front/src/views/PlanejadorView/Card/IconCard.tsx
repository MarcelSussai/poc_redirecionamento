import React from 'react'
import styled from 'styled-components'
import Footer from './Footer'
import Colors from '../../../domain/Colors'
import { ButtonImage, ButtonProps, ButtonsContainer, IconButton } from './Buttons'
import { CardContentContainer  } from './CardContentContainer'
import goToButtonIcon from './images/arrowRight.png'

const TitleContainer = styled.h2`
  color: rgb(${Colors["darkBlue"]});
  font-weight: bold;
  margin-top: 1vw;
  margin-left: 0.3vw;
  margin-right: 0.3vw;
`

const Icon = styled.img`
  height: 10vw;
  width: auto;
  cursor: pointer;
`

interface Props {
  title: string
  iconSource: string
  color?: keyof typeof Colors
  goToButton: ButtonProps
}

export default ({title, iconSource, goToButton, color = "blue"}: Props) => {
  const redirect = () => window.open(goToButton.link, '_blank', 'noopener,noreferrer')

  return (
    <CardContentContainer>
      <TitleContainer>{title}</TitleContainer>
      <Icon src={iconSource} onClick={redirect} />
      <ButtonsContainer>
        <IconButton onClick={redirect} > 
          <ButtonImage src={goToButtonIcon} />
        </IconButton>
      </ButtonsContainer>
      <Footer color={color}/>
    </CardContentContainer>
  )
}