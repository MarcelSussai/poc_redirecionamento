import React from 'react'
import styled from 'styled-components'
import { ButtonImage, ButtonProps, ButtonsContainer, IconButton } from './Buttons'
import { CardContentContainer } from './CardContentContainer'
import Footer from './Footer'
import Colors from '../../../domain/Colors'
import taskIcon from './images/task.png'
import goToButtonIcon from './images/arrowRight.png'
import plusButtonIcon from './images/plus.png'

const Icon = styled.img`
  width: 4vw;
  height: auto;
  padding-top: 1vw;
  padding-bottom: 1vw;
`

const TitleContainer = styled.h2`
  font-weight: bold;
  margin-left: 0.3vw;
  margin-right: 0.3vw;
`

const DescriptionContainer = styled.p`
  margin-left: 0.3vw;
  margin-right: 0.3vw;
  margin-bottom: 0.3vw;
`

interface Props {
  title: string
  description: string
  color?: keyof typeof Colors
  goToButton?: ButtonProps
  plusButton?: ButtonProps
  icon?: string
}

export default ({ title, description, color = "blue", goToButton, plusButton, icon = taskIcon }: Props) => {
  const redirect = (link: string, newTab: boolean = false) => {
    if (newTab) window.open(link, '_blank', 'noopener,noreferrer')
    else window.open(link, '_self')
  }
  return (
    <CardContentContainer>
      <Icon src={icon} />
      <TitleContainer>{title.toUpperCase()}</TitleContainer>
      <DescriptionContainer>{description}</DescriptionContainer>
      <ButtonsContainer>
        {goToButton &&
          <IconButton onClick={() => redirect(goToButton.link, goToButton.newTab ?? false)} >
            <ButtonImage src={goToButtonIcon} />
          </IconButton>
        }
        {plusButton &&
          <IconButton onClick={() => redirect(plusButton.link, plusButton.newTab ?? false)} >
            <ButtonImage src={plusButtonIcon} />
          </IconButton>
        }
      </ButtonsContainer>
      <Footer color={color} />
    </CardContentContainer>
  )
}