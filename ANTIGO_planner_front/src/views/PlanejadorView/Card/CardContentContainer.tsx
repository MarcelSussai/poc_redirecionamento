import styled from 'styled-components'
import Colors from '../../../domain/Colors'

interface Props {
  color?: keyof typeof Colors
}

const Container = styled.div<Props>`
  background-color: rgb(${({color}) => color ? Colors[color] : Colors.gray});
  display: flex;
  justify-content: space-between;
  flex-direction: column;
  align-items: center;
  width: 16.5vw;
  min-width: 16.5vw;
  text-align: center;
  border-top-left-radius: 8%;
  border-top-right-radius: 8%;
`

export { Container as CardContentContainer }