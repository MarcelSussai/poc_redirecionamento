import styled from 'styled-components'
import RawIconButton from '@material-ui/core/IconButton'

export interface ButtonProps {
  link: string
  newTab?: boolean
}

export const ButtonsContainer = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-evenly;
  padding: 1vw;
`

export const IconButton = styled(RawIconButton)`
  margin-left: 0.5vw;
  margin-right: 0.5vw;
`

export const ButtonImage = styled.img`
  width: 2.2vw;
  height: auto;
  object-fit: contain;
`