import React from 'react'
import styled from 'styled-components'
import aceleracaoBackground from './Card/images/aceleracao.png'

const AceleracaoContainer = styled.div`
  display: flex;
  min-height: 100%;
  width: 100%;
  background-image: url(${aceleracaoBackground});
  background-position: right;
  background-size: contain;
  background-repeat: no-repeat;
  cursor: pointer;
`

export default () => {
  const redirect = () => window.open("https://materiais.meuvista.com/licenca-vista", '_blank', 'noopener,noreferrer')
  return ( <AceleracaoContainer onClick={redirect} /> )
}