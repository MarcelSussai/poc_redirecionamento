import { QueryKey, useQuery, UseQueryOptions } from 'react-query'
import { getPessoaById } from "../../domain/pessoa/api/getPessoaById";
import { Pessoa } from '../../domain/pessoa/Pessoa';

export const cacheBaseKey = 'pessoa'

export const useCurrentPessoaQuery = (
  personId: number,
  options?: Omit<UseQueryOptions<unknown, Error, Pessoa, QueryKey>, "queryKey" | "queryFn">
) =>
  useQuery<unknown, Error, Pessoa>(
    [cacheBaseKey, personId],
    () => getPessoaById({ personId }),
    options
  )
