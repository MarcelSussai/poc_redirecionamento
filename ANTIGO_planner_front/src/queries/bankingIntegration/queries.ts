import { QueryKey, useQuery, UseQueryOptions } from 'react-query'
import { getConnectionsAndAccounts } from '../../domain/bankingIntegration/api/getConnectionsAndAccounts'
import { Connection } from '../../domain/bankingIntegration/Connection'

export interface UseBankIntegrationConnectionsResponse {
  isLoadingConnections: boolean,
  isErrorConnections: boolean,
  connections: Connection[],
  connectionsError: Error | null
}

export const cacheBaseKey = ['bank-integration']
export const accountCacheKey = [...cacheBaseKey, 'accounts']

export const useBankIntegrationConnections = (
  personId: number,
  options?: Omit<UseQueryOptions<unknown, Error, Connection[], QueryKey>, "queryKey" | "queryFn">
): UseBankIntegrationConnectionsResponse => {
  const {
    isLoading: isLoadingConnections,
    isError: isErrorConnections,
    data: connections = [],
    error: connectionsError
  } = useQuery<unknown, Error, Connection[]>(
    [...accountCacheKey, personId],
    () => getConnectionsAndAccounts(personId),
    options
  )
  return { isLoadingConnections, isErrorConnections, connections, connectionsError }
}
