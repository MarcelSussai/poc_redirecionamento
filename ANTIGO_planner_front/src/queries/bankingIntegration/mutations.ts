import { useMutation, useQueryClient } from "react-query";
import { accountCacheKey } from "./queries";
import { deleteConnection } from "../../domain/bankingIntegration/api/deleteConnection";

export const useDeleteConnectionMutation = () => {
  const client = useQueryClient()
  return useMutation(deleteConnection, {
    onSuccess: () => {
      client.invalidateQueries(accountCacheKey)
    }
  })
}