import { useMutation, useQueryClient } from "react-query";
import { cacheBaseKey } from "./queries";
import { accountCacheKey } from "../bankingIntegration/queries";
import { associateBankIntegrationAccountToMeioPagamento } from '../../domain/meioPagamento/api/bankIntegrationAccountAssociation'

export const useAssociateToBankIntegrationAccount = () => {
  const client = useQueryClient()
  return useMutation(associateBankIntegrationAccountToMeioPagamento, {
    onSuccess: () => {
      client.invalidateQueries(cacheBaseKey)
      client.invalidateQueries(accountCacheKey)
    }
  })
}
  