import { QueryKey, useQuery, UseQueryOptions } from 'react-query'
import { fetchAllByFamily } from '../../domain/meioPagamento/api/fetchByFamily'
import { MeioPagamento } from '../../domain/meioPagamento/meioPagamento'

export interface UseFamilyMeiosPagamentosResponse {
  isLoadingMeios: boolean
  isErrorMeios: boolean,
  meiosPagamento: MeioPagamento[] | undefined,
  meiosPagamentoError: Error | null
}

export const cacheBaseKey = 'meio-pagamento'

export const useFamilyMeiosPagamentos = (
  familyId: number,
  options?: Omit<UseQueryOptions<unknown, Error, MeioPagamento[], QueryKey>, "queryKey" | "queryFn">
): UseFamilyMeiosPagamentosResponse => {
  const {
    isLoading: isLoadingMeios,
    isError: isErrorMeios,
    data: meiosPagamento,
    error: meiosPagamentoError
  } = useQuery<unknown, Error, MeioPagamento[]>(
    [cacheBaseKey, familyId],
    () => fetchAllByFamily(familyId),
    options
  )
  return {
    isLoadingMeios,
    isErrorMeios,
    meiosPagamento,
    meiosPagamentoError
  }
}
