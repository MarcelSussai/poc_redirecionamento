import { QueryKey, useQuery, UseQueryOptions } from 'react-query'
import { getFamilyById } from "../../domain/family/api/getFamilyById";
import { Family } from '../../domain/family/Family';

export const cacheBaseKey = 'family'

export const useCurrentFamilyQuery = (
  familyId: number, 
  options?: Omit<UseQueryOptions<unknown, Error, Family, QueryKey>, "queryKey" | "queryFn">) => 
  useQuery<unknown, Error, Family>(
    [cacheBaseKey, familyId],
    () => getFamilyById({familyId}),
    options
  )