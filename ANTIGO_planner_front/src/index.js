import React from "react";
import ReactDOM from "react-dom";
import { BrowserRouter, Route, Switch, Redirect } from "react-router-dom";
import { disableReactDevTools } from "@fvilers/disable-react-devtools";
import { QueryClient, QueryClientProvider } from 'react-query'
import { ReactQueryDevtools } from 'react-query/devtools'
import Monitoring from './services/Monitoring'

import "./global.js";

import "./assets/vendor/nucleo/css/nucleo.css";
import "./assets/vendor/@fortawesome/fontawesome-free/css/all.min.css";
import "./assets/scss/argon-dashboard-react.scss";

import AdminLayout from "./layouts/Admin.jsx";
import AuthLayout from "./layouts/Auth.jsx";

global.apiToken = localStorage.getItem("tokenAcesso");
const queryClient = new QueryClient()

const MaybeReactQueryDevTools = () => {
  const isReactQueryDebuggingOn = process.env.REACT_APP_QUERY_DEBUG
  console.log(`React query debugging is ${isReactQueryDebuggingOn}`)
  return isReactQueryDebuggingOn === 'true'
    ? <ReactQueryDevtools />
    : <></>;
}

const reactDevToolsIsEnabled = process.env.REACT_APP_DEV_TOOLS === 'true'
if (!reactDevToolsIsEnabled) {
  disableReactDevTools();
}

ReactDOM.render(
  <QueryClientProvider client={queryClient}>
    <BrowserRouter>
      <Switch>
        <Route path="/admin" render={(props) => <AdminLayout {...props} />} />
        <Route path="/auth" render={(props) => <AuthLayout {...props} />} />
        <Redirect from="/" to="/admin/index" />
      </Switch>
    </BrowserRouter>
    <MaybeReactQueryDevTools />
  </QueryClientProvider>,
  document.getElementById("root")
);