import React from 'react'
import Index from "./views/Index.jsx";

import Register from "./views/auth/Register.jsx";
import Login from "./views/auth/Login.jsx";
import Reset from "./views/auth/Reset.jsx";
import AlterarSenha from "./views/auth/TrocarSenha.jsx";
import HabilitarUsuario from "./views/auth/HabilitarUsuario.jsx";

import Bancos from "./views/internas/Bancos.jsx";
import Orcamentos from "./views/internas/Orcamentos.jsx";
import OrcamentoGeral from "./views/internas/Orcamento.jsx";
import Categorias from "./views/internas/Categorias.jsx";
import MeioPagamento from "./views/internas/MeioPagamento.jsx";
import BucketList from "./views/internas/BucketList.jsx";
import Lancamentos from "./views/internas/Lancamentos.jsx";
import PlanosSonhos from "./views/internas/PlanosSonhos.jsx";
import GestaoPlanos from "./views/internas/GestaoPlanos.jsx";
import Aposentadoria from "./views/internas/Aposentadoria";
import IntegracaoBancaria from "./views/internas/IntegracaoBancaria";
import Familias from "./views/internas/Familias.jsx";
import Pessoas from "./views/internas/Pessoas.jsx";
import Planejadores from "./views/internas/Planejadores.jsx";
import MesEmpresas from "./views/internas/empresas/Empresas.jsx";
import Empresas from "./views/internas/Empresas.jsx";
import ClientesCancelados from "./views/internas/ClientesCancelados.jsx";
import Equilibrio from "./views/internas/equilibrio/Equilibrio.jsx";
import Wizard from "./views/internas/wizard/Wizard.jsx";

import Dividas from "./views/internas/Dividas.jsx";
import Patrimonios from "./views/internas/Patrimonios.jsx";
import DestinacaoPatrimonio from "./views/internas/DestinacaoPatrimonio.jsx";
import DestinacaoInvestimento from "./views/internas/DestinacaoInvestimentos.jsx";

import Tutoriais from "./views/internas/Tutoriais.jsx";
import MateriaisVista from "./views/internas/MateriaisVista.jsx";
import Eventos from "./views/internas/Eventos.jsx";
import Leads from "./views/internas/IndexLeads.jsx";
import TermosPrivacidade from "./views/internas/TermosPrivacidade.jsx";
import TermosUso from "./views/internas/TermosUso.jsx";
import { AuthProvider } from './hooks/contexts/authContext';
import { useCurrentPersonId } from './hooks/useCurrentPersonId';
import { CurrentPersonProvider } from './hooks/contexts/currentPersonContext';
import { useCurrentFamilyId } from './hooks/useCurrentFamilyId';
import { CurrentFamilyProvider } from './hooks/contexts/currentFamilyContext';

// TODO: Improve these wrappers for easier usage
const WithAuth = (Children: () => JSX.Element) => {
  return (
    <AuthProvider>
      <Children />
    </AuthProvider>
  )
}

const WithCurrentPessoa = (Children: () => JSX.Element) => {
  const { personId } = useCurrentPersonId()
  return (
    <CurrentPersonProvider personId={personId} >
      <Children />
    </CurrentPersonProvider>
  )
}

const WithCurrentFamily = (Children: () => JSX.Element) => {
  const { familyId } = useCurrentFamilyId()
  return (
    <CurrentFamilyProvider familyId={familyId}>
      <Children />
    </CurrentFamilyProvider>
  )
}

interface Route {
  name?: string
  icon: string
  perfis?: number[]
  layout?: string
  path?: string
  header?: string
  component?: any
  hideOnSidebar?: boolean
  configurationMenu?: boolean
  planejadorPrincipal?: boolean
  verificacaoFamiliaSelecionada?: boolean
}

const getRoutePath = (route) => route.layout + route.path
const getRoute = (name: string) => routes.find(r => r.name === name)

const loginPath = "/login"
const routes: Route[] = [
  {
    header: "VISTA",
    icon: "fas fa-tv",
    perfis: [0, 1, 2],
  },
  {
    path: "/index",
    name: "Home",
    icon: "fas fa-angle-right",
    component: Index,
    layout: "/admin",
    perfis: [0, 1, 2],
  },
  {
    path: "/equilibrio-financeiro",
    name: "Equilíbrio Financeiro",
    icon: "fas fa-angle-right",
    component: Equilibrio,
    layout: "/admin",
    perfis: [0, 1, 2],
  },
  {
    header: "OBJETIVOS",
    icon: "fas fa-star",
    perfis: [0, 1, 2],
  },
  {
    path: "/planos-sonhos",
    name: "Planos & Sonhos",
    icon: "fas fa-angle-right",
    component: PlanosSonhos,
    layout: "/admin",
    perfis: [0, 1, 2],
  },
  {
    path: "/gestao-planos-sonhos",
    name: "Gestão de Planos",
    icon: "fas fa-angle-right",
    component: GestaoPlanos,
    layout: "/admin",
    perfis: [0, 1, 2],
  },
  {
    path: "/aposentadoria",
    name: "Independência Financeira",
    icon: "fas fa-angle-right",
    component: Aposentadoria,
    layout: "/admin",
    perfis: [0, 1, 2],
  },
  {
    header: "ORÇAMENTO",
    icon: "fas fa-coins",
    perfis: [0, 1, 2],
  },
  {
    path: "/orcamento",
    name: "Gestão de Orçamento",
    icon: "fas fa-angle-right",
    component: OrcamentoGeral,
    layout: "/admin",
    perfis: [0, 1, 2],
  },
  {
    path: "/dividas",
    name: "Dívidas",
    icon: "fas fa-angle-right",
    component: Dividas,
    layout: "/admin",
    perfis: [0, 1, 2],
  },
  {
    path: "/extrato",
    name: "Extrato",
    icon: "fas fa-angle-right",
    component: Lancamentos,
    layout: "/admin",
    perfis: [0, 1, 2],
  },
  {
    path: "/integracao-bancaria",
    name: "Integração Bancária",
    icon: "fas fa-angle-right",
    component: () => WithCurrentFamily(() => WithCurrentPessoa(() => WithAuth(IntegracaoBancaria))),
    layout: "/admin",
    perfis: [2],
  },
  {
    header: "FINANCEIRO",
    icon: "fas fa-chart-line",
    perfis: [0, 1, 2],
  },
  {
    path: "/patrimonio/:open",
    name: "Patrimônio",
    icon: "fas fa-angle-right",
    component: Patrimonios,
    layout: "/admin",
    hideOnSidebar: true,
  },
  {
    path: "/patrimonio",
    name: "Patrimônio",
    icon: "fas fa-angle-right",
    component: Patrimonios,
    layout: "/admin",
    perfis: [0, 1, 2],
  },
  {
    path: "/destinacao-patrimonio",
    name: "Destinação de Patrimônio",
    icon: "fas fa-angle-right",
    component: DestinacaoPatrimonio,
    layout: "/admin",
    perfis: [0, 1, 2],
  },
  {
    path: "/destinacao-investimentos",
    name: "Destinação de Investimentos",
    icon: "fas fa-angle-right",
    component: DestinacaoInvestimento,
    layout: "/admin",
    perfis: [0, 1, 2],
  },
  // {
  //   path: "/estimativas",
  //   name: "Estimativas",
  //   icon: "ni ni-key-25 text-info",
  //   component: Index,
  //   layout: "/admin",
  //   perfis: [1,2]
  // },
  // {
  //   path: "/plano-investimento",
  //   name: "Plano de Investimento",
  //   icon: "ni ni-circle-08 text-pink",
  //   component: Index,
  //   layout: "/admin",
  //   perfis: [1,2]
  // },
  {
    path: "/bucketlist",
    name: "Lista de Sonhos",
    icon: "ni ni-bullet-list-67 text-red",
    component: BucketList,
    layout: "/admin",
    configurationMenu: true,
    perfis: [0],
  },
  {
    path: "/empresas",
    name: "Empresas",
    icon: "ni ni-bullet-list-67 text-red",
    component: Empresas,
    layout: "/admin",
    configurationMenu: true,
    perfis: [0],
    planejadorPrincipal: true,
  },
  {
    path: "/minha-empresa",
    name: "Empresa",
    icon: "ni ni-bullet-list-67 text-red",
    component: Empresas,
    layout: "/admin",
    configurationMenu: true,
    perfis: [1],
    planejadorPrincipal: true,
  },
  {
    path: "/planejadores",
    name: "Planejadores",
    icon: "ni ni-bullet-list-67 text-red",
    component: Planejadores,
    layout: "/admin",
    configurationMenu: true,
    perfis: [0],
  },
  {
    path: "/planejadores",
    name: "Planejador",
    icon: "ni ni-bullet-list-67 text-red",
    component: Planejadores,
    layout: "/admin",
    configurationMenu: true,
    perfis: [1],
    planejadorPrincipal: true,
  },
  {
    path: "/mes-empresas",
    name: "Fatura Vista",
    icon: "ni ni-bullet-list-67 text-red",
    component: MesEmpresas,
    layout: "/admin",
    configurationMenu: true,
    perfis: [0, 1],
  },
  {
    path: "/clientes/:open",
    name: "Clientes - Novo",
    icon: "ni ni-bullet-list-67 text-red",
    component: Familias,
    layout: "/admin",
    configurationMenu: true,
    perfis: [0, 1],
    hideOnSidebar: true,
  },
  {
    path: "/clientes",
    name: "Clientes",
    icon: "ni ni-bullet-list-67 text-red",
    component: Familias,
    layout: "/admin",
    configurationMenu: true,
    perfis: [0, 1],
  },
  {
    path: "/usuarios/:open",
    name: "Usuários - Novo",
    icon: "ni ni-bullet-list-67 text-red",
    component: Pessoas,
    layout: "/admin",
    configurationMenu: true,
    perfis: [0, 1, 2],
    hideOnSidebar: true,
  },
  {
    path: "/cancelados",
    name: "Clientes Cancelados",
    icon: "ni ni-bullet-list-67 text-red",
    component: ClientesCancelados,
    layout: "/admin",
    configurationMenu: true,
    perfis: [0],
  },
  {
    path: "/usuarios",
    name: "Usuários",
    icon: "ni ni-bullet-list-67 text-red",
    component: Pessoas,
    layout: "/admin",
    configurationMenu: true,
    verificacaoFamiliaSelecionada: true,
    perfis: [0, 1, 2],
  },
  {
    path: "/orcamentos",
    name: "Orçamentos",
    icon: "ni ni-bullet-list-67 text-red",
    component: Orcamentos,
    layout: "/admin",
    configurationMenu: true,
    perfis: [0],
  },
  {
    path: "/categorias",
    name: "Categorias",
    icon: "ni ni-bullet-list-67 text-red",
    component: Categorias,
    layout: "/admin",
    configurationMenu: true,
    perfis: [0],
  },
  {
    path: "/bancos",
    name: "Bancos",
    icon: "ni ni-bullet-list-67 text-red",
    component: Bancos,
    layout: "/admin",
    configurationMenu: true,
    perfis: [0],
  },
  {
    path: "/meios-pagamento",
    name: "Meios de Pagamento",
    icon: "ni ni-bullet-list-67 text-red",
    component: MeioPagamento,
    layout: "/admin",
    configurationMenu: true,
    verificacaoFamiliaSelecionada: true,
    perfis: [0, 1, 2],
  },
  {
    path: "/wizard",
    name: "Configuração Inicial",
    icon: "ni ni-tv-2 text-primary",
    component: Wizard,
    layout: "/admin",
    configurationMenu: true,
    verificacaoFamiliaSelecionada: true,
    perfis: [0, 1, 2],
  },
  {
    path: "/tutoriais",
    name: "Tutoriais",
    icon: "ni ni-button-play text-primary",
    component: Tutoriais,
    layout: "/admin",
    configurationMenu: true,
    verificacaoFamiliaSelecionada: true,
    perfis: [0, 1],
  },
  {
    path: "/materiais",
    name: "Materiais Vista",
    icon: "ni ni-collection text-primary",
    component: MateriaisVista,
    layout: "/admin",
    configurationMenu: true,
    perfis: [0, 1],
  },
  {
    path: "/eventos",
    name: "Eventos Vista",
    icon: "ni ni-collection text-primary",
    component: Eventos,
    layout: "/admin",
    configurationMenu: true,
    perfis: [0, 1],
  },
  {
    path: "/indexleads",
    name: "Leads Vista",
    icon: "ni ni-collection text-primary",
    component: Leads,
    layout: "/admin",
    configurationMenu: true,
    perfis: [0, 1],
  },
  {
    path: "/termosprivacidade",
    name: "Termos Privacidade Vista",
    icon: "ni ni-collection text-primary",
    component: TermosPrivacidade,
    layout: "/admin",
    configurationMenu: true,
    perfis: [0, 1, 2],
  },
  {
    path: "/termosuso",
    name: "Termos Uso Vista",
    icon: "ni ni-collection text-primary",
    component: TermosUso,
    layout: "/admin",
    configurationMenu: true,
    perfis: [0, 1, 2],
  },
  {
    path: "/alterar-senha",
    name: "Alterar Senha",
    icon: "ni ni-key-25 text-info",
    component: AlterarSenha,
    layout: "/admin",
    configurationMenu: true,
    perfis: [0, 1, 2],
  },
  {
    path: "/registrar",
    name: "Cadastro",
    icon: "ni ni-key-25 text-info",
    component: Register,
    layout: "/auth",
    hideOnSidebar: true,
  },
  {
    path: loginPath,
    name: "Login",
    icon: "ni ni-key-25 text-info",
    component: Login,
    layout: "/auth",
    hideOnSidebar: true,
  },
  {
    path: "/reset/:token",
    name: "Reset Senha",
    icon: "ni ni-key-25 text-info",
    component: Reset,
    layout: "/auth",
    hideOnSidebar: true,
  },
  {
    path: "/reset",
    name: "Reset Senha",
    icon: "ni ni-key-25 text-info",
    component: Reset,
    layout: "/auth",
    hideOnSidebar: true,
  },
  {
    path: "/habilitar-usuario/:id",
    name: "Habilitar Usuário",
    icon: "ni ni-key-25 text-info",
    component: HabilitarUsuario,
    layout: "/auth",
    hideOnSidebar: true,
  },
];
export default routes;
export { loginPath, getRoute, getRoutePath };
