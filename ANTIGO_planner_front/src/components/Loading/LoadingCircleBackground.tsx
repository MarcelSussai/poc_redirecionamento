import React from "react";
import styled from 'styled-components'
import Colors from '../../domain/Colors'
import { Oval } from 'react-loader-spinner'

interface Props {
    text?: string
}

const Background = styled.div`
    display: flex;
    justify-content: center;
    width: 100%;
    height: 100%;
    background-color: ${Colors.gray};
    opacity: 0.3;
    z-index: -999;
`

const IconAndTextContainer = styled.div`
    display: flex;
    flex-direction: column;
`

const LoadingCircleBackground = ({ text }: Props) => {
    const displayText = text ?? "Carregando..."
    return (
        <Background >
            <IconAndTextContainer>
                <Oval />
                <p>{displayText}</p>
            </IconAndTextContainer>
        </Background>
    )
}

export default LoadingCircleBackground