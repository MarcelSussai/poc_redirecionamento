import React from "react";

class ButtonBackPage extends React.Component {
    render() {
      return (
        <>
            <a className="" href="javascript:history.back()" >
              <i className="far fa-arrow-alt-circle-left" style={
                {
                  textAlign: "start", 
                  background: "white", 
                  color: "#9A3DFF",
                  fontSize: '50px',
                  paddingTop: '50px'
                }}>
              </i>
          </a>
        </>
      );
    }
}

export default ButtonBackPage;