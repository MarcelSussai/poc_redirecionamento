import React from "react";
import { NavLink as NavLinkRRD, Link, Redirect } from "react-router-dom";
import SkyLight from "react-skylight";
import Datepicker, { registerLocale } from "react-datepicker";
import MaskedInput from "react-maskedinput";
import br from "date-fns/locale/pt-BR";
import "react-datepicker/dist/react-datepicker.css";
import Select from "react-select";
import axios from "axios";
import $ from "jquery";
import { confirmAlert } from "react-confirm-alert";
import "react-confirm-alert/src/react-confirm-alert.css";
import CurrencyInput from "react-currency-input";
import { RadioGroup, Radio } from "react-radio-group";
import Tooltip from "@material-ui/core/Tooltip";
import { withStyles } from "@material-ui/core/styles";
import ResgatarTudo from "@material-ui/icons/MonetizationOn";
import PlanejadorService from "../../services/PlanejadorService";
import {
  Navbar,
  NavLink,
  NavItem,
  Container,
  Input,
  Nav,
  Row,
  Col,
} from "reactstrap";

registerLocale("br", br);

class AdminNavbar extends React.Component {
  moment = require("moment");
  reportar = [
    {
      label: "Financeiro",
      value: "Financeiro",
    },
    {
      label: "Bug",
      value: "Bug",
    },
    {
      label: "Uso da plataforma",
      value: "Uso da plataforma",
    },
    {
      label: "Integração",
      value: "Integração",
    },
    {
      label: "Outros",
      value: "Outros",
    },
  ];

  paginaBug = [
    {
      label: "Home",
      value: "Home",
    },
    {
      label: "Equilíbrio Financeiro",
      value: "Equilíbrio Financeiro",
    },
    {
      label: "Planos & Sonhos",
      value: "Planos & Sonhos",
    },
    {
      label: "Gestão de Planos",
      value: "Gestão de Planos",
    },
    {
      label: "Gestão de Orçamento",
      value: "Gestão de Orçamento",
    },
    {
      label: "Dívidas",
      value: "Dívidas",
    },
    {
      label: "Extrato",
      value: "Extrato",
    },
    {
      label: "Patrimônio",
      value: "Patrimônio",
    },
    {
      label: "Destinação de Patrimônio",
      value: "Destinação de Patrimônio",
    },
    {
      label: "Destinação de Investimentos",
      value: "Destinação de Investimentos",
    },
    {
      label: "Outra",
      value: "Outra",
    },
  ];

  state = {
    categorias: [],
    meiosPagamento: [],
    patrimonio: [],
    patrimonioAtivos: {},
    patrimonioAtivosDados: {},
    patrimonioAtivosCombo: [],
    passoAtual: 1,
    edicao: false,
    alertEnvioForm: false,
    item: {
      descricao: "",
      categoria: null,
      orcamento: null,
      data: null,
      meioPagamento: null,
      valor: "",
      valorFormatado: "",
      operacao: null,
      ativo: null,
      quantidade: "",
      quantidadeFormatado: "",
      parcelas: 1,
      agrupamento: null,
      tipoDeParcela: null,

      patrimonio: null,
      idInstituicaoFinanceira: null,
      custoOperacional: "",
      taxaCustomizada: "",
      taxaCustomizadaFormatada: "",
      taxaAnual: "",
      taxaAnualFormatada: "",
      iof: "",
      iofFormatado: "",
      impostoRenda: "",
      impostoRendaFormatado: "",
      tipoRetirada: null,
      idMovimentacao: null,
      videoURL: "https://player.vimeo.com/video/518358268",
    },
  };

  // creates the links that appear in the Rightbar
  createLinks = (routes) => {
    if (routes == null) return <></>;

    return routes.map((prop, key) => {
      return (
        <NavItem key={key}>
          <NavLink
            to={prop.layout + prop.path}
            tag={NavLinkRRD}
            onClick={this.closeCollapse}
            activeClassName="active"
          >
            {prop.name}
            <i className={prop.icon} />
          </NavLink>
        </NavItem>
      );
    });
  };

  setItemPropertyOnState = (key, value, callback) => {
    var item = this.state.item;

    item[key] = value;

    if (callback) this.setState({ item: item }, callback());
    else this.setState({ item: item });
  };

  setItemOnState = (item, callback) => {
    if (callback) this.setState({ item: item }, callback());
    else this.setState({ item: item });
  };

  setTodayDate = (fieldKey, date) => {
    this.setItemPropertyOnState(fieldKey, date);
  };

  gerarDatePicker = (id, callback, defaultDate) => {
    var currentDate = null;

    //date type
    if (this.state.item != null && this.state.item[id] != null) {
      //string vinda da edicao
      if (typeof this.state.item[id] === "string") {
        var temp = this.moment(this.state.item[id], "YYYY-MM-DDThh:mm:ss");
        this.setItemPropertyOnState(id, temp);
        currentDate = temp.toDate();
      }
      //data vinda da selecao atraves do calendario
      else {
        currentDate = this.state.item[id];
      }
    }
    //momentjs date
    else if (defaultDate != null) {
      currentDate = defaultDate.toDate();
      this.setItemPropertyOnState(id, currentDate);
    }
    //now (date)
    else {
      currentDate = new Date();
      this.setItemPropertyOnState(id, currentDate);
    }

    return (
      <Datepicker
        locale="br"
        id={id}
        dateFormat="dd/MM/yyyy"
        selected={currentDate}
        showYearDropdown
        dateFormatCalendar="MMMM"
        scrollableYearDropdown
        yearDropdownItemNumber={5}
        onChange={(date) => callback(id, date)}
        customInput={<MaskedInput mask="11/11/1111" />}
      />
    );
  };

  buscarPatrimonioAtivosRendaFixa = () => {
    //var url = global.server_api + 'api/patrimonio/patrimonioAtivo/' + this.state.familiaId;
    var url =
      global.server_api_new +
      global.apiToken +
      "/patrimonio/patrimonioAtivo/" +
      this.state.familiaId;

    var config = {
      headers: {
        Authorization: "bearer " + this.state.accessToken,
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Headers": "Authorization",
        "Access-Control-Allow-Methods":
          "GET, POST, OPTIONS, PUT, PATCH, DELETE",
      },
    };

    axios.get(url, config).then((res) => {
      var itens = res.data;

      var patrimonioAtivos = {};
      var patrimonioAtivosDados = {};

      if (itens != null && itens.length > 0) {
        itens.forEach((item) => {
          if (patrimonioAtivos[item.patrimonioId] == null) {
            patrimonioAtivos[item.patrimonioId] = [];
          }

          patrimonioAtivosDados[item.id] = [];
          patrimonioAtivosDados[item.id].push(item.quantidade);
          patrimonioAtivosDados[item.id].push(item.total);

          patrimonioAtivos[item.patrimonioId].push({
            value: item.id,
            label: item.descricao,
          });
        });
      }

      this.setState({ patrimonioAtivosDados: patrimonioAtivosDados });
      this.setState({ patrimonioAtivos: patrimonioAtivos });
    });
  };

  atualizarEntidadesDeDominio = (fieldKey, identificador) => {
    //var url = global.server_api + 'api/patrimonio/metodosAuxiliares/porIdentificador';
    var url =
      global.server_api_new +
      global.apiToken +
      "/patrimonio/metodosAuxiliares/porIdentificador";

    var config = {
      headers: {
        Authorization: "bearer " + this.state.accessToken,
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Headers": "Authorization",
        "Access-Control-Allow-Methods":
          "GET, POST, OPTIONS, PUT, PATCH, DELETE",
      },
    };

    var data = {
      filtro: identificador,
    };

    axios.post(url, data, config).then((res) => {
      const itens = res.data.data;

      var combo = [];

      if (res.data.success === true && itens != null) {
        itens.forEach((item) => {
          combo.push({
            value: item.key,
            label: item.value,
          });
        });
      }

      this.setState({ [fieldKey]: combo });
    });
  };

  atualizarOrcamentos = () => {
    //var url = global.server_api + 'api/orcamento/familia/'+this.state.familiaId+'/filtro';
    var url =
      global.server_api_new +
      global.apiToken +
      "/orcamento/familia/" +
      this.state.familiaId +
      "/filtro";

    var config = {
      headers: {
        Authorization: "bearer " + this.state.accessToken,
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Headers": "Authorization",
        "Access-Control-Allow-Methods":
          "GET, POST, OPTIONS, PUT, PATCH, DELETE",
      },
    };

    axios.post(url, {}, config).then((res) => {
      const orcamentos = res.data.results;
      var orcamentoDeDespesaCombo = [
        {
          label: "Selecione...",
          value: null,
        },
      ];

      var orcamentoDeReceitaCombo = [
        {
          label: "Selecione...",
          value: null,
        },
      ];

      if (orcamentos != null) {
        orcamentos.forEach((item) => {
          //Apenas orcamentos de receitas
          if (item.tipoOrcamento === 0 && item.suspenso === 0) {
            orcamentoDeReceitaCombo.push({
              value: item,
              label: item.nome,
            });
          }
          //Apenas orcamentos de despesas
          else if (item.tipoOrcamento === 1 && item.suspenso === 0) {
            orcamentoDeDespesaCombo.push({
              value: item,
              label: item.nome,
            });
          }
        });
      }

      if (orcamentoDeReceitaCombo.length === 1) {
        orcamentoDeReceitaCombo[0].label =
          "Nenhuma categoria de receita ativa encontrada";
      }

      if (orcamentoDeDespesaCombo.length === 1) {
        orcamentoDeDespesaCombo[0].label =
          "Nenhuma categoria de despesa ativa encontrada";
      }

      this.setState({ orcamentos });
      this.setState({ orcamentoDeDespesaCombo });
      this.setState({ orcamentoDeReceitaCombo });
    });
  };

  atualizaMeiosPagamento = () => {
    //var urlMeioPagamento = global.server_api + 'api/meioPagamento/familia/' + this.state.familiaId + "/filtro";
    var urlMeioPagamento =
      global.server_api_new +
      global.apiToken +
      "/meioPagamento/familia/" +
      this.state.familiaId +
      "/filtro";

    var config = {
      headers: {
        Authorization: "bearer " + this.state.accessToken,
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Headers": "Authorization",
        "Access-Control-Allow-Methods":
          "GET, POST, OPTIONS, PUT, PATCH, DELETE",
      },
    };

    axios.post(urlMeioPagamento, {}, config).then((res) => {
      const meiosPagamento = res.data.results;
      var meiosPagamentoCombo = [];

      if (meiosPagamento != null) {
        meiosPagamento.forEach((mp) => {
          meiosPagamentoCombo.push({
            value: mp,
            label: mp.nome,
          });
        });
      }

      this.setState({ meiosPagamento });
      this.setState({ meiosPagamentoCombo });
    });
  };

  atualizaCategorias = () => {
    //var urlCategoria = global.server_api + 'api/categoria/familia/' + this.state.familiaId + "/filtro";
    var urlCategoria =
      global.server_api_new +
      global.apiToken +
      "/categoria/familia/" +
      this.state.familiaId +
      "/filtro";

    var config = {
      headers: {
        Authorization: "bearer " + this.state.accessToken,
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Headers": "Authorization",
        "Access-Control-Allow-Methods":
          "GET, POST, OPTIONS, PUT, PATCH, DELETE",
      },
    };

    axios
      .post(urlCategoria, { IncluirOrcamentos: true }, config)
      .then((res) => {
        const categorias = res.data.results;
        var categoriasDeReceitasCombo = [];
        var categoriasDeDespesasCombo = [];
        var categoriasDeInvestimentoCombo = [];
        var categoriasDeDividaCombo = [];

        if (categorias != null) {
          categorias.forEach((c) => {
            if (
              c.orcamento.tipoOrcamento === 2 &&
              c.suspenso === 0 &&
              c.orcamento.suspenso === 0
            ) {
              categoriasDeInvestimentoCombo.push({
                value: c,
                label: c.nome,
              });
            }
            //Se orcamento de divida
            else if (
              c.orcamento.tipoOrcamento === 3 &&
              c.suspenso === 0 &&
              c.orcamento.suspenso === 0
            ) {
              categoriasDeDividaCombo.push({
                value: c,
                label: c.nome,
              });
            }
          });

          if (categoriasDeReceitasCombo.length === 0) {
            categoriasDeReceitasCombo.push({
              label: "Selecione um orçamento...",
              value: null,
            });
          }

          if (categoriasDeDespesasCombo.length === 0) {
            categoriasDeDespesasCombo.push({
              label: "Selecione um orçamento...",
              value: null,
            });
          }

          if (categoriasDeInvestimentoCombo.length === 0) {
            categoriasDeInvestimentoCombo.push({
              label:
                "Nenhuma categoria de investimento cadastrada e/ou ativa...",
              value: null,
            });
          }

          if (categoriasDeDividaCombo.length === 0) {
            categoriasDeDividaCombo.push({
              label: "Nenhuma categoria de dívida cadastrada e/ou ativa...",
              value: null,
            });
          }

          this.setState({ categorias });
          this.setState({ categoriasDeReceitasCombo });
          this.setState({ categoriasDeDespesasCombo });
          this.setState({ categoriasDeInvestimentoCombo });
          this.setState({ categoriasDeDividaCombo });
        }
      });
  };

  atualizarPatrimonios = () => {
    //var url = global.server_api + 'api/patrimonio/familia/' + this.state.familiaId + '/filtro';
    var url =
      global.server_api_new +
      global.apiToken +
      "/patrimonio/familia/" +
      this.state.familiaId +
      "/filtro";

    var config = {
      headers: {
        Authorization: "bearer " + this.state.accessToken,
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Headers": "Authorization",
        "Access-Control-Allow-Methods":
          "GET, POST, OPTIONS, PUT, PATCH, DELETE",
      },
    };

    axios.post(url, { ApenasNome: true }, config).then((res) => {
      var patrimonio = [];
      var dataCombo = [];

      if (res.data.results != null) {
        patrimonio = res.data.results;

        patrimonio.forEach((item) => {
          if (item.tipoPatrimonio === 3) {
            dataCombo.push({
              value: item,
              label: item.descricao,
            });
          }
        });
      }

      this.setState({ patrimonio: patrimonio, patrimonioCombo: dataCombo });
    });
  };

  componentDidMount() {
    this.PlanejadorService = new PlanejadorService();
    this.fileInput = React.createRef();
    this.setState(
      {
        accessToken: localStorage.getItem("access-token"),
        empresaId: localStorage.getItem("empresa-id"),
        familiaId: localStorage.getItem("familia-id"),
        planejadorPrincipal: localStorage.getItem("planejador-principal"),
        planejadorId: localStorage.getItem("planejador-id"),
        tipoUsuario: localStorage.getItem("tipo-usuario"),
      },
      function () {
        this.atualizarDados(false);
      }
    );
    this.lancamentosCampos("inicio");

    //alert(window.location.href);
    //Correção de bug de navegação
    if(window.location.href == "http://localhost:3000/admin" 
    || window.location.href == "https://app.meuvista.com/admin"){
      window.location.href = "/";
    }
  }

  componentDidUpdate() {
    if (this.state.accessToken === null) {
      this.setRedirect();
      return;
    }
  }

  setRedirect = () => {
    this.setState({
      redirect: true,
    });
  };

  renderRedirect = () => {
    if (this.state.redirect) {
      return <Redirect to="/auth/login" />;
    }
  };

  atualizarDados = (apenasVista) => {
    if (this.state.familiaId != null) {
      this.atualizarPatrimonios();
      this.atualizarOrcamentos();
      this.atualizaCategorias();
      this.atualizaMeiosPagamento();
      this.buscarPatrimonioAtivosRendaFixa();
      // this.atualizarAtivos();

      if (apenasVista !== true) {
        this.atualizarEntidadesDeDominio(
          "instituicoesFinanceiras",
          "instituicoesFinanceiras"
        );
      }
    }
  };

  lancamentosCampos = (tipo) => {
    if (tipo !== "inicio") {
      $(".lancamento-opcoes").removeClass("active");
    }

    $(".lancamento").hide();

    if (tipo === "receita") {
      $(".receita").show();
      if (tipo !== "inicio") {
        $($(".lancamento-opcoes")[0]).addClass("active");
      }
    } else if (tipo === "despesa" || tipo === "inicio") {
      $(".despesa").show();
      $($(".lancamento-opcoes")[1]).addClass("active");
    } else if (tipo === "investimento") {
      $(".investimento").show();
      $($(".lancamento-opcoes")[2]).addClass("active");
    } else if (tipo === "divida") {
      $(".divida").show();
      $($(".lancamento-opcoes")[3]).addClass("active");
    }
  };

  montarDropDown = (itens, id, changeMethod, selectedValue) => (
    <Select
      id={id}
      onChange={changeMethod}
      className="select-component"
      options={itens}
      defaultValue={itens != null ? itens[0] : ""}
      placeholder="Selecione..."
      value={
        itens != null && selectedValue != null
          ? itens.find((op) => {
              if (
                op.value != null &&
                op.value["id"] != null &&
                selectedValue["id"] != null
              ) {
                return (
                  parseInt(op.value["id"]) === parseInt(selectedValue["id"])
                );
              } else {
                //ToString para a comparacao nao dar erro e pq pode ter string
                return (
                  op.value != null &&
                  op.value.toString() === selectedValue.toString()
                );
              }
            })
          : null
      }
    />
  );

  preCarregarParaEdicao = () => {
    try {
      var lancamentoEdicao = localStorage.getItem("lancamentoEdicao");

      if (lancamentoEdicao != null) {
        lancamentoEdicao = JSON.parse(lancamentoEdicao);

        this.setState({ edicao: true });

        var categoriaId = lancamentoEdicao.categoriaId; //lancamentoEdicao.categoria.id;
        var categorias = this.state.categorias.filter(function (categoria) {
          return categoriaId === categoria.id;
        });

        //Preenche combo de categorias de receita ou despesa
        if (
          categorias != null &&
          categorias[0].orcamento != null &&
          categorias[0].orcamento.tipoOrcamento < 2
        ) {
          this.handleChangeOrcamento({ value: categorias[0].orcamento });
        }

        var meioPagamentoId = lancamentoEdicao.meioPagamentoId; //lancamentoEdicao.meioPagamento.id;
        var meioPagamentos = this.state.meiosPagamento.filter(function (
          meioPagamento
        ) {
          return meioPagamentoId === meioPagamento.id;
        });

        var patrimonioId = lancamentoEdicao.PatrimonioId; //lancamentoEdicao.ativoReferencia != null ? lancamentoEdicao.ativoReferencia.id : null;
        var patrimonios = this.state.patrimonio.filter(function (patrimonio) {
          return patrimonioId === patrimonio.id;
        });

        var data = this.moment(lancamentoEdicao.data, "DD/MM/YYYY").toDate();

        var dataVencimento =
          lancamentoEdicao.dataVencimento != null
            ? this.moment(
                lancamentoEdicao.dataVencimento,
                "YYYY-MM-DDThh:mm:ss"
              ).toDate()
            : null;

        if (patrimonios != null && patrimonios.length > 0) {
          var tipoInvestimento = patrimonios[0].tipoInvestimento;

          if (
            tipoInvestimento === 8 ||
            tipoInvestimento === 10 ||
            tipoInvestimento === 99
          ) {
            lancamentoEdicao.valor =
              lancamentoEdicao.valor != null && lancamentoEdicao.valor > 0
                ? lancamentoEdicao.valor /
                  (lancamentoEdicao.quantidade != null &&
                  lancamentoEdicao.quantidade > 0
                    ? lancamentoEdicao.quantidade
                    : 1)
                : 0;
          }
        }

        lancamentoEdicao.categoria =
          categorias != null && categorias.length > 0 ? categorias[0] : null;
        lancamentoEdicao.orcamento =
          categorias != null && categorias.length > 0
            ? categorias[0].orcamento
            : null;
        lancamentoEdicao.data = data;
        lancamentoEdicao.meioPagamento =
          meioPagamentos != null && meioPagamentos.length > 0
            ? meioPagamentos[0]
            : null;
        lancamentoEdicao.valorFormatado =
          lancamentoEdicao.valor != null
            ? lancamentoEdicao.valor.toLocaleString("pt-BR", {
                minimumFractionDigits: 2,
                maximumFractionDigits: 2,
              })
            : null;
        lancamentoEdicao.patrimonio =
          patrimonios != null && patrimonios.length > 0 ? patrimonios[0] : null;
        lancamentoEdicao.dataVencimento = dataVencimento;
        lancamentoEdicao.tipoDeParcela =
          lancamentoEdicao.parcelas > 1 ? lancamentoEdicao.tipoDeParcela : null;

        var $this = this;

        this.setItemOnState(lancamentoEdicao, function () {
          //var url = global.server_api + 'api/lancamento/familia/' + $this.state.familiaId + '/data-inicial/' + lancamentoEdicao.id;
          var url =
            global.server_api_new +
            global.apiToken +
            "/lancamento/familia/" +
            $this.state.familiaId +
            "/data-inicial/" +
            lancamentoEdicao.id;

          var config = {
            headers: { Authorization: "bearer " + $this.state.accessToken },
          };

          //Se é um lançamento agrupado com mais de uma parcela
          if (
            lancamentoEdicao.agrupamento != null &&
            lancamentoEdicao.parcelas > 1
          ) {
            //Busca data inicial do lancamento
            axios.get(url, config).then((res) => {
              if (res.data.success && res.data.singleResult != null) {
                var temp2 = $this.moment(
                  res.data.singleResult,
                  "YYYY-MM-DDThh:mm:ss"
                );
                $this.setItemPropertyOnState("data", temp2.toDate());
              }
            });
          }
        });
      } else {
        this.atualizarDados(true);
      }
    } catch (error) {
      // console.log("error: ", error);
    }
  };

  limparSelecao = (logar) => {
    this.setState({ edicao: false });
    this.setItemOnState({
      descricao: "",
      categoria: null,
      orcamento: null,
      data: null,
      meioPagamento: null,
      valor: "",
      valorFormatado: "",
      operacao: null,
      ativo: null,
      quantidade: "",
      quantidadeFormatado: "",
      parcelas: 1,
      agrupamento: null,
      tipoDeParcela: null,

      patrimonio: null,
      idInstituicaoFinanceira: null,
      custoOperacional: "",
      taxaCustomizada: "",
      taxaCustomizadaFormatada: "",
      taxaAnual: "",
      taxaAnualFormatada: "",
      iof: "",
      iofFormatado: "",
      impostoRenda: "",
      impostoRendaFormatado: "",
      tipoRetirada: null,
      idMovimentacao: null,
    });
  };

  limparSelecaoEmail = () => {
    this.setState({
      atualNome: "",
      atualEmail: "",
      atualTelefone: "",
      atualMotivo: "",
      atualAssunto: "",
      paginaBug: "",
      familiaBug: "",
    });

    $("#arquivoEmail").val(null);
    document.getElementById("atualMensagem").value = "";
  };

  selecaoTipoLancamento = () => {
    var lancamentoEdicao = localStorage.getItem("lancamentoEdicao");

    if (lancamentoEdicao != null) {
      lancamentoEdicao = JSON.parse(lancamentoEdicao);
      localStorage.removeItem("lancamentoEdicao");

      //if(lancamentoEdicao && lancamentoEdicao.categoria && lancamentoEdicao.categoria.orcamento && lancamentoEdicao.categoria.orcamento.tipoOrcamento === 1){
      if (lancamentoEdicao && lancamentoEdicao.tipoOrcamento === 1) {
        document.getElementById("lancamento-despesa").click();
      } else if (lancamentoEdicao && lancamentoEdicao.tipoOrcamento === 2) {
        document.getElementById("lancamento-investimento").click();
      } else if (lancamentoEdicao && lancamentoEdicao.tipoOrcamento === 3) {
        document.getElementById("lancamento-divida").click();
      } else {
        document.getElementById("lancamento-receita").click();
      }
    }
  };

  mudarTipoDeParcela = (selectedOption) => {
    this.setItemPropertyOnState("tipoDeParcela", selectedOption);
  };

  NovoLancamentoModal = () => (
    <div className="novo-lancamento-popup">
      <SkyLight
        ref={(ref) => (this.cadastroLancamento = ref)}
        transitionDuration={0}
        beforeOpen={this.preCarregarParaEdicao}
        afterOpen={this.selecaoTipoLancamento}
        afterClose={this.limparSelecao}
        dialogStyles={{ borderRadius: "1rem", padding: "2rem" }}
        title={
          <>
            <div className="pop-up-title">
              <i className="ni ni-check-bold"></i>
              <h2>
                Cadastro de Lançamento
                <span style={{ fontSize: "14px" }}>
                  {this.state.item != null && this.state.item.patrimonio != null
                    ? " - " +
                      global.tipoInvestimento[
                        this.state.item.patrimonio.tipoInvestimento
                      ]
                    : ""}
                </span>
                <span
                  style={
                    this.state.item.operacao === 1 &&
                    this.state.item.patrimonioAtivoId != null
                      ? { fontSize: "14px" }
                      : { display: "none" }
                  }
                >
                  {" "}
                  - Qtd. Atual:{" "}
                  {this.state.patrimonioAtivosDados[
                    this.state.item.patrimonioAtivoId
                  ] != null
                    ? this.state.patrimonioAtivosDados[
                        this.state.item.patrimonioAtivoId
                      ][0]
                    : ""}{" "}
                  - Valor Atual: R${" "}
                  {this.state.patrimonioAtivosDados[
                    this.state.item.patrimonioAtivoId
                  ] != null &&
                  this.state.patrimonioAtivosDados[
                    this.state.item.patrimonioAtivoId
                  ][1] != null
                    ? this.state.patrimonioAtivosDados[
                        this.state.item.patrimonioAtivoId
                      ][1].toLocaleString("pt-BR", {
                        minimumFractionDigits: 2,
                        maximumFractionDigits: 2,
                      })
                    : ""}
                </span>
              </h2>
              <Navbar>
                <Container>
                  <NavItem
                    id="lancamento-receita"
                    className="lancamento-opcoes"
                    onClick={this.lancamentosCampos.bind(this, "receita")}
                  >
                    <NavLink className="nav-link-icon">
                      <span className="nav-link-inner--text">Receita</span>
                    </NavLink>
                  </NavItem>
                  <NavItem
                    id="lancamento-despesa"
                    className="lancamento-opcoes active"
                    onClick={this.lancamentosCampos.bind(this, "despesa")}
                  >
                    <NavLink className="nav-link-icon">
                      <span className="nav-link-inner--text">Despesa</span>
                    </NavLink>
                  </NavItem>
                  <NavItem
                    id="lancamento-investimento"
                    className="lancamento-opcoes"
                    onClick={this.lancamentosCampos.bind(this, "investimento")}
                  >
                    <NavLink className="nav-link-icon">
                      <span className="nav-link-inner--text">Investimento</span>
                    </NavLink>
                  </NavItem>
                  <NavItem
                    id="lancamento-divida"
                    className="lancamento-opcoes"
                    onClick={this.lancamentosCampos.bind(this, "divida")}
                  >
                    <NavLink className="nav-link-icon">
                      <span className="nav-link-inner--text">Dívida</span>
                    </NavLink>
                  </NavItem>
                </Container>
              </Navbar>
            </div>
          </>
        }
      >
        <Row>
          <Col lg="6" xl="6" className="lancamento investimento">
            <label>Operação *</label>
            {this.montarDropDown(
              global.operacao,
              "lancamentoOperacao",
              (selectedOption) => {
                this.setItemPropertyOnState("operacao", selectedOption.value);
              },
              this.state.item.operacao
            )}
          </Col>
          <Col lg="6" xl="6" className="lancamento investimento">
            <label>Patrimônio Financeiro *</label>
            {this.montarDropDown(
              this.state.patrimonioCombo,
              "lancamentoPatrimonio",
              this.handleChangePatrimonio,
              this.state.item.patrimonio
            )}
          </Col>
          <Col lg="6" xl="6" className="lancamento receita">
            <label>Orçamento</label>
            {this.montarDropDown(
              this.state.orcamentoDeReceitaCombo,
              "lancamentoOrcamento",
              this.handleChangeOrcamento,
              this.state.item.orcamento
            )}
          </Col>
          <Col lg="6" xl="6" className="lancamento despesa">
            <label>Orçamento</label>
            {this.montarDropDown(
              this.state.orcamentoDeDespesaCombo,
              "lancamentoOrcamento",
              this.handleChangeOrcamento,
              this.state.item.orcamento
            )}
          </Col>
          <Col lg="6" xl="6" className="lancamento receita despesa divida">
            <div className="lancamento receita">
              <label>Categoria *</label>
              {this.montarDropDown(
                this.state.categoriasDeReceitasCombo,
                "lancamentoCategoria",
                this.handleChangeCategoria,
                this.state.item.categoria
              )}
            </div>
            <div className="lancamento despesa">
              <label>Categoria *</label>
              {this.montarDropDown(
                this.state.categoriasDeDespesasCombo,
                "lancamentoCategoria",
                this.handleChangeCategoria,
                this.state.item.categoria
              )}
            </div>
            <div className="lancamento divida">
              <label>Dívida *</label>
              {this.montarDropDown(
                this.state.categoriasDeDividaCombo,
                "lancamentoCategoria",
                this.handleChangeCategoria,
                this.state.item.categoria
              )}
            </div>
          </Col>
          <Col
            lg="6"
            xl="6"
            style={
              this.state.item.patrimonio != null &&
              this.state.item.patrimonio.tipoInvestimento === 3 &&
              this.state.item.operacao !== 1
                ? {}
                : { display: "none" }
            }
          >
            <div>
              <label>Taxa *</label>
              <CurrencyInput
                id="lancamentoTaxaCustomizada"
                value={
                  this.state.item.taxaCustomizadaFormatada != null
                    ? this.state.item.taxaCustomizadaFormatada
                    : this.state.item.taxaCustomizada
                }
                thousandSeparator={"."}
                decimalSeparator={","}
                precision="2"
                suffix=" %"
                onChangeEvent={(event, formattedValue, value) => {
                  this.setItemPropertyOnState("taxaCustomizada", value);
                  this.setItemPropertyOnState(
                    "taxaCustomizadaFormatada",
                    formattedValue
                  );
                }}
              />
            </div>
          </Col>
          <Col
            lg="6"
            xl="6"
            style={
              this.state.item.patrimonio != null &&
              this.state.item.patrimonio.tipoInvestimento === 6 &&
              this.state.item.operacao !== 1
                ? {}
                : { display: "none" }
            }
          >
            <div>
              <label>Taxa Anual *</label>
              <CurrencyInput
                id="lancamentoTaxaAnual"
                value={
                  this.state.item.taxaAnualFormatada != null
                    ? this.state.item.taxaAnualFormatada
                    : this.state.item.taxaAnual
                }
                thousandSeparator={"."}
                decimalSeparator={","}
                precision="2"
                suffix=" %"
                onChangeEvent={(event, formattedValue, value) => {
                  this.setItemPropertyOnState("taxaAnual", value);
                  this.setItemPropertyOnState(
                    "taxaAnualFormatada",
                    formattedValue
                  );
                }}
              />
            </div>
          </Col>
          <Col
            lg="6"
            xl="6"
            style={
              this.state.item.patrimonio != null &&
              (this.state.item.patrimonio.tipoInvestimento === 3 ||
                this.state.item.patrimonio.tipoInvestimento === 6) &&
              this.state.item.operacao !== 1
                ? {}
                : { display: "none" }
            }
          >
            <div>
              <label>Data de Vencimento *</label>
              {this.gerarDatePicker(
                "dataVencimento",
                this.setTodayDate,
                this.moment()
              )}
            </div>
          </Col>
          <Col
            lg="6"
            xl="6"
            style={
              this.state.item.patrimonio != null &&
              (this.state.item.patrimonio.tipoInvestimento === 3 ||
                this.state.item.patrimonio.tipoInvestimento === 6) &&
              this.state.item.operacao === 1
                ? {}
                : { display: "none" }
            }
          >
            <div>
              <label>Ativo *</label>
              {this.montarDropDown(
                this.state.patrimonioAtivosCombo,
                "lancamentoPatrimonioAtivo",
                this.handleChangePatrimonioAtivo,
                this.state.item.patrimonioAtivoId
              )}
            </div>
          </Col>
          <Col lg="6" xl="6" className="lancamento despesa receita divida">
            <div>
              <label className="lancamento receita">Conta *</label>
              <label className="lancamento despesa divida">
                Forma de Pagamento *
              </label>
              <Select
                id="lancamentoMeioPagamento"
                onChange={this.handleChangeMeioPagamento}
                className="select-component"
                options={this.state.meiosPagamentoCombo}
                defaultValue={
                  this.state.meiosPagamentoCombo != null
                    ? this.state.meiosPagamentoCombo[0]
                    : ""
                }
                placeholder="Selecione..."
                value={
                  this.state.meiosPagamentoCombo != null &&
                  this.state.item.meioPagamento != null
                    ? this.state.meiosPagamentoCombo.find((op) => {
                        return op.label === this.state.item.meioPagamento.nome;
                      })
                    : this.state.meiosPagamentoCombo != null &&
                      this.state.item.categoria != null &&
                      this.state.item.categoria.meioPagamentoPadrao != null
                    ? this.state.meiosPagamentoCombo.find((op) => {
                        return (
                          op.label ===
                          this.state.item.categoria.meioPagamentoPadrao.nome
                        );
                      })
                    : null
                }
              />
            </div>
          </Col>
          <Col
            lg="6"
            xl="6"
            style={
              this.state.item.patrimonio != null &&
              this.state.item.patrimonio.tipoPatrimonio != null &&
              this.state.item.patrimonio.tipoPatrimonio !== 5 &&
              this.state.item.operacao !== 1
                ? {}
                : { display: "none" }
            }
          >
            <div>
              <label>Origem do Recurso *</label>
              {this.montarDropDown(
                this.state.meiosPagamentoCombo,
                "lancamentoMeioPagamento",
                this.handleChangeMeioPagamento,
                this.state.item.meioPagamento
              )}
            </div>
          </Col>
          <Col lg="6" xl="6" className="lancamento investimento">
            <div>
              <label>Instituição Financeira da Transação *</label>
              {this.montarDropDown(
                this.state.instituicoesFinanceiras,
                "lancamentoIdInstituicaoFinanceira",
                (selectedOption) => {
                  this.setItemPropertyOnState(
                    "idInstituicaoFinanceira",
                    selectedOption.value
                  );
                },
                this.state.item.idInstituicaoFinanceira
              )}
            </div>
          </Col>
          <Col
            lg="6"
            xl="6"
            className="lancamento despesa receita investimento divida"
          >
            {/* TESOURO DIRETO, ACOES, MOEDA, PROD CUSTOMIZADO */}
            <div>
              <label className="lancamento despesa receita">Valor *</label>
              <label className="lancamento investimento">
                {this.state.item.patrimonio != null &&
                (this.state.item.patrimonio.tipoInvestimento === 8 ||
                  this.state.item.patrimonio.tipoInvestimento === 10 ||
                  this.state.item.patrimonio.tipoInvestimento === 99)
                  ? "Preço Unitário"
                  : this.state.item.operacao !== 1
                  ? "Valor Total Aplicado"
                  : "Valor resgatado"}{" "}
                *
                {this.state.item.operacao === 1 && (
                  <button
                    style={{
                      borderRadius: 25,
                      border: "solid 1px #09a8c2",
                      fontSize: 12,
                      cursor: "pointer",
                      marginLeft: 5,
                      background: "none",
                    }}
                    onClick={() => this.handleResgateTotal()}
                  >
                    <ResgatarTudo
                      fontSize="small"
                      style={{ color: "#09a8c2" }}
                    />
                    <span style={{ color: "#5b5c5d" }}>Resgate total</span>
                  </button>
                )}
              </label>
              <label className="lancamento divida">Valor Pago *</label>
              <CurrencyInput
                id="lancamentoValor"
                style={{ height: 38, paddingLeft: 25 }}
                value={
                  this.state.item.valorFormatado
                    ? this.state.item.valorFormatado
                    : this.state.item.patrimonio != null &&
                      this.state.item.patrimonio.tipoInvestimento === 4 &&
                      this.state.item.operacao === 1
                    ? this.state.item.valor * this.state.item.quantidade
                    : this.state.item.valor
                }
                thousandSeparator={"."}
                decimalSeparator={","}
                precision="2" //prefix={'R$ '}
                onChangeEvent={(event, formattedValue, value) => {
                  this.setItemPropertyOnState("valor", value);
                  this.setItemPropertyOnState("valorFormatado", formattedValue);
                }}
              />
            </div>
          </Col>
          <Col
            lg="6"
            xl="6"
            style={
              this.state.item.patrimonio != null &&
              (this.state.item.patrimonio.tipoInvestimento === 4 ||
                this.state.item.patrimonio.tipoInvestimento === 8 ||
                this.state.item.patrimonio.tipoInvestimento === 10 ||
                this.state.item.patrimonio.tipoInvestimento === 99)
                ? {}
                : { display: "none" }
            }
          >
            {/* TESOURO DIRETO, ACOES, MOEDA, PROD CUSTOMIZADO */}
            <div>
              <label>Quantidade *</label>
              <CurrencyInput
                id="lancamentoQuantidade"
                style={{ paddingLeft: 25 }}
                value={
                  this.state.item.quantidadeFormatado
                    ? this.state.item.quantidadeFormatado
                    : this.state.item.quantidade
                }
                thousandSeparator={"."}
                decimalSeparator={","}
                onChangeEvent={(event, formattedValue, value) => {
                  this.setItemPropertyOnState("quantidade", value);
                  this.setItemPropertyOnState(
                    "quantidadeFormatado",
                    formattedValue
                  );
                }}
              />
            </div>
          </Col>
          <Col
            lg="6"
            xl="6"
            style={
              this.state.item.patrimonio != null &&
              (this.state.item.patrimonio.tipoInvestimento === 4 ||
                this.state.item.patrimonio.tipoInvestimento === 8 ||
                this.state.item.patrimonio.tipoInvestimento === 10 ||
                this.state.item.patrimonio.tipoInvestimento === 99)
                ? {}
                : { display: "none" }
            }
          >
            {/* TESOURO DIRETO, ACOES, MOEDA, PROD CUSTOMIZADO */}
            <div>
              <label>
                {this.state.item.patrimonio != null &&
                (this.state.item.patrimonio.tipoInvestimento === 8 ||
                  this.state.item.patrimonio.tipoInvestimento === 10 ||
                  this.state.item.patrimonio.tipoInvestimento === 99)
                  ? this.state.item.operacao !== 1
                    ? "Valor Total Aplicado"
                    : "Valor Total Resgatado"
                  : "Preço Unitário"}
              </label>
              {this.state.item.patrimonio != null &&
              (this.state.item.patrimonio.tipoInvestimento === 8 ||
                this.state.item.patrimonio.tipoInvestimento === 10 ||
                this.state.item.patrimonio.tipoInvestimento === 99)
                ? (
                    (this.state.item.valor != null
                      ? this.state.item.valor
                      : 0) *
                    (this.state.item.quantidade != null &&
                    this.state.item.quantidade >= 0
                      ? this.state.item.quantidade
                      : 0)
                  ).toLocaleString("pt-BR", {
                    minimumFractionDigits: 2,
                    maximumFractionDigits: 2,
                  })
                : this.state.item.patrimonio != null &&
                  this.state.item.patrimonio.tipoInvestimento === 4 &&
                  this.state.item.operacao === 1
                ? (this.state.item.valor != null
                    ? this.state.item.valor
                    : 0
                  ).toLocaleString("pt-BR", {
                    minimumFractionDigits: 2,
                    maximumFractionDigits: 2,
                  })
                : (
                    (this.state.item.valor != null
                      ? this.state.item.valor
                      : 0) /
                    (this.state.item.quantidade != null &&
                    this.state.item.quantidade > 0
                      ? this.state.item.quantidade
                      : 1)
                  ).toLocaleString("pt-BR", {
                    minimumFractionDigits: 2,
                    maximumFractionDigits: 6,
                  })}
            </div>
          </Col>
          <Col
            lg="6"
            xl="6"
            className="lancamento despesa receita investimento divida"
          >
            <div>
              <label className="lancamento despesa receita divida">
                Data *
              </label>
              <label className="lancamento investimento">
                Data da Compra / Venda *
              </label>
              {this.gerarDatePicker("data", this.setTodayDate, this.moment())}
            </div>
          </Col>
          <Col lg="6" xl="6" className="lancamento despesa receita divida">
            <div>
              <label>Quantidade de Parcelas</label>
              <div
                style={{ float: "left", marginRight: "10px", maxWidth: "45%" }}
              >
                <Input
                  type="number"
                  id="lancamentoParcelas"
                  value={this.state.item.parcelas}
                  onChange={(e) =>
                    this.setItemPropertyOnState("parcelas", e.target.value)
                  }
                  style={{ height: 38, paddingLeft: 25 }}
                />
              </div>
              <div style={{ float: "left" }}>
                <RadioGroup
                  name="tipoDeParcela"
                  selectedValue={this.state.item.tipoDeParcela}
                  onChange={this.mudarTipoDeParcela}
                >
                  <div
                    className="lancamento despesa receita"
                    style={{
                      float: "left",
                      marginRight: "10px",
                      marginBottom: "10px",
                    }}
                  >
                    <Radio value={0} /> Parcelado
                  </div>
                  <div
                    className="lancamento despesa receita"
                    style={{ float: "left", marginBottom: "10px" }}
                  >
                    <Radio value={1} /> Repetido
                  </div>
                </RadioGroup>
              </div>
            </div>
          </Col>
          <Col
            lg="6"
            xl="6"
            style={
              this.state.item.patrimonio != null &&
              (this.state.item.patrimonio.tipoInvestimento === 1 ||
                this.state.item.patrimonio.tipoInvestimento === 2) &&
              this.state.item.operacao === 1
                ? {}
                : { display: "none" }
            }
          >
            <div>
              <label>Tipo de Resgate *</label>
              {this.montarDropDown(
                global.tipoRetirada,
                "lancamentoTipoRetirada",
                (selectedOption) => {
                  this.setItemPropertyOnState(
                    "tipoRetirada",
                    selectedOption.value
                  );
                },
                this.state.item.tipoRetirada
              )}
            </div>
          </Col>
          <Col
            lg="6"
            xl="6"
            style={
              this.state.item.patrimonio != null &&
              this.state.item.operacao !== 1 &&
              this.state.item.patrimonio.tipoInvestimento === 8
                ? {}
                : { display: "none" }
            }
          >
            <div>
              <label>Custo Operacional</label>
              <input
                type="number"
                id="lancamentoCustoOperacional"
                value={this.state.item.custoOperacional}
                onChange={(e) =>
                  this.setItemPropertyOnState(
                    "custoOperacional",
                    e.target.value
                  )
                }
              ></input>
            </div>
          </Col>
          <Col
            lg="6"
            xl="6"
            style={
              this.state.item.patrimonio != null &&
              (this.state.item.patrimonio.tipoInvestimento === 1 || //Fundos
                this.state.item.patrimonio.tipoInvestimento === 2 || //Previdencia
                this.state.item.patrimonio.tipoInvestimento === 3 || //Renda Fixa Pós
                this.state.item.patrimonio.tipoInvestimento === 4 || //Tesouro Direto
                this.state.item.patrimonio.tipoInvestimento === 6 || //Renda Fixa Pré
                this.state.item.patrimonio.tipoInvestimento === 8 || //Ações
                this.state.item.patrimonio.tipoInvestimento === 9) && //Debentures
              this.state.item.operacao === 1
                ? {}
                : { display: "none" }
            }
          >
            <div>
              <label>Imposto de Renda *</label>
              <CurrencyInput
                id="lancamentoImpostoRenda"
                style={{ paddingLeft: 25 }}
                value={
                  this.state.item.impostoRendaFormatado != null
                    ? this.state.item.impostoRendaFormatado
                    : this.state.item.impostoRenda
                }
                thousandSeparator={"."}
                decimalSeparator={","}
                precision="2"
                onChangeEvent={(event, formattedValue, value) => {
                  this.setItemPropertyOnState("impostoRenda", value);
                  this.setItemPropertyOnState(
                    "impostoRendaFormatado",
                    formattedValue
                  );
                }}
              />
            </div>
          </Col>
          <Col
            lg="6"
            xl="6"
            style={
              this.state.item.patrimonio != null &&
              (this.state.item.patrimonio.tipoInvestimento === 1 || //Fundos
                this.state.item.patrimonio.tipoInvestimento === 2 || //Previdencia
                this.state.item.patrimonio.tipoInvestimento === 3 || //Renda Fixa Pós
                this.state.item.patrimonio.tipoInvestimento === 4 || //Tesouro Direto
                this.state.item.patrimonio.tipoInvestimento === 6 || //Renda Fixa Pré
                this.state.item.patrimonio.tipoInvestimento === 8 || //Ações
                this.state.item.patrimonio.tipoInvestimento === 9) && //Debentures
              this.state.item.operacao === 1
                ? {}
                : { display: "none" }
            }
          >
            <div>
              <label>IOF *</label>
              <CurrencyInput
                id="lancamentoIOF"
                style={{ paddingLeft: 25 }}
                value={
                  this.state.item.iofFormatado != null
                    ? this.state.item.iofFormatado
                    : this.state.item.iof
                }
                thousandSeparator={"."}
                decimalSeparator={","}
                precision="2"
                onChangeEvent={(event, formattedValue, value) => {
                  this.setItemPropertyOnState("iof", value);
                  this.setItemPropertyOnState("iofFormatado", formattedValue);
                }}
              />
            </div>
          </Col>
          <Col
            lg="6"
            xl="6"
            className="lancamento despesa receita investimento divida"
          >
            <div>
              <label>Descrição</label>
              <textarea
                id="lancamentoOQue"
                onChange={(e) =>
                  this.setItemPropertyOnState("descricao", e.target.value)
                }
                value={this.state.item.descricao}
                style={{ padding: 20 }}
              ></textarea>
            </div>
          </Col>
          <Col
            lg="12"
            xl="12"
            style={{ display: "flex", justifyContent: "center" }}
          >
            <button
              id="salvar-lancamento"
              className="featured-button"
              onClick={() => this.salvarLancamento()}
              style={{
                backgroundImage:
                  "url(" +
                  require("../../assets/img/theme/botao-destaque.png") +
                  ")",
                backgroundSize: "cover",
              }}
            >
              SALVAR LANÇAMENTO
            </button>
          </Col>
        </Row>
      </SkyLight>
    </div>
  );

  handleChangeOrcamento = (selectedOption) => {
    const lancamentoOrcamento = selectedOption.value;

    this.setItemPropertyOnState("orcamento", lancamentoOrcamento);

    var categorias = [];

    if (this.state.categorias != null && lancamentoOrcamento != null) {
      this.state.categorias.forEach((c) => {
        if (
          c.orcamento.id === lancamentoOrcamento.id &&
          c.suspenso === 0 &&
          c.orcamento.suspenso === 0
        ) {
          categorias.push({
            value: c,
            label: c.nome,
          });
        }
      });
    }

    if (categorias != null && categorias.length === 0) {
      categorias.push({
        label: "Nenhuma categoria cadastrada e/ou ativa neste orçamento",
        value: null,
      });
    }

    //Sem selecao de orcamento
    if (lancamentoOrcamento == null) {
      this.setState({
        categoriasDeReceitasCombo: [
          {
            label: "Selecione um orçamento...",
            value: null,
          },
        ],
        categoriasDeDespesasCombo: [
          {
            label: "Selecione um orçamento...",
            value: null,
          },
        ],
      });
    }
    //Receita
    else if (lancamentoOrcamento.tipoOrcamento === 0) {
      this.setState({
        categoriasDeReceitasCombo: categorias,
        categoriasDeDespesasCombo: [
          {
            label: "Selecione um orçamento...",
            value: null,
          },
        ],
      });
    }
    //Despesa
    else if (lancamentoOrcamento.tipoOrcamento === 1) {
      this.setState({
        categoriasDeReceitasCombo: [
          {
            label: "Selecione um orçamento...",
            value: null,
          },
        ],
        categoriasDeDespesasCombo: categorias,
      });
    }
  };

  getBase64 = (file, callback) => {
    var reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => {
      callback(reader.result);
    };
  };

  handleChangePatrimonio = (selectedOption) => {
    const lancamentoPatrimonio = selectedOption.value;

    var lancamentoCategoria = null;

    var patrimonioAtivosCombo = [];

    var patrimonioAtivoId = null;

    if (lancamentoPatrimonio != null) {
      var nomeCategoria = lancamentoPatrimonio.descricao;

      var categoria = this.state.categorias.find(function (categoria) {
        return (
          nomeCategoria.toLowerCase() === categoria.nome.toLowerCase() &&
          categoria.orcamento != null &&
          categoria.orcamento.tipoOrcamento === 2
        );
      });

      if (categoria != null) {
        lancamentoCategoria = categoria;
      } else {
        console.log(
          "Temos uma inconsistência aqui. Deveria ter a categoria mas não tem",
          nomeCategoria
        );
      }

      patrimonioAtivosCombo = this.state.patrimonioAtivos[
        lancamentoPatrimonio.id
      ];

      if (patrimonioAtivosCombo != null && patrimonioAtivosCombo.length === 1) {
        patrimonioAtivoId = patrimonioAtivosCombo[0].value;
      }
    }

    this.setItemPropertyOnState("patrimonio", lancamentoPatrimonio);
    this.setItemPropertyOnState("categoria", lancamentoCategoria);
    this.setItemPropertyOnState("patrimonioAtivoId", patrimonioAtivoId);
    this.setState({ patrimonioAtivosCombo: patrimonioAtivosCombo });
    this.setState({ item: { ...this.state.item, quantidade: 0, valor: 0 } });
  };

  handleChangeCategoria = (selectedOption) => {
    this.setItemPropertyOnState("categoria", selectedOption.value);
  };

  handleChangeMeioPagamento = (selectedOption) => {
    this.setItemPropertyOnState("meioPagamento", selectedOption.value);
  };

  handleChangePatrimonioAtivo = (selectedOption) => {
    this.setItemPropertyOnState("patrimonioAtivoId", selectedOption.value);
  };

  handleResgateTotal = () => {
    var idPatrimonioSelecionado;

    if (this.state.item && this.state.item.patrimonio) {
      //validar se não tiver
      idPatrimonioSelecionado = this.state.item.patrimonio.id;
    } else {
      return null;
    }
    var valuePatrimonio = this.state.patrimonioAtivos[idPatrimonioSelecionado]
      ? this.state.patrimonioAtivos[idPatrimonioSelecionado][0]["value"]
      : null;

    var patrimonioSelecionado = this.state.patrimonioAtivosDados[
      valuePatrimonio
    ];

    if (patrimonioSelecionado) {
      this.setState({
        item: {
          ...this.state.item,
          quantidade: patrimonioSelecionado[0],
          valor: patrimonioSelecionado[1] / patrimonioSelecionado[0],
          valorFormatado: "",
        },
      });
    }
  };

  salvarLancamento = () => {
    var lancamento = this.state.item;
    console.log("\n\n\n\Lancamento", lancamento);

    lancamento.patrimonioId =
      lancamento.patrimonio != null ? lancamento.patrimonio.id : null;
    lancamento.categoriaId =
      lancamento.categoria != null ? lancamento.categoria.id : null;
    lancamento.meioPagamentoId =
      lancamento.meioPagamento != null
        ? lancamento.meioPagamento.id
        : lancamento.categoria &&
          lancamento.categoria.meioPagamentoPadrao != null
        ? lancamento.categoria.meioPagamentoPadrao.id
        : null;

    if(lancamento.categoria != null){
      if (
        lancamento.categoria.orcamento.tipoOrcamento === 3 &&
        lancamento.parcelas > 1
      ) {
        lancamento.tipoDeParcela = 1;
      } else {
        lancamento.tipoDeParcela =
          lancamento.parcelas != null ? lancamento.tipoDeParcela : null;
      }
    }
    

    lancamento.parcelas = lancamento.parcelas != null ? lancamento.parcelas : 1;

    lancamento.empresaId =
      this.state.empresaId != null
        ? this.state.empresaId
        : localStorage.getItem("empresa-id");
    lancamento.familiaId = this.state.familiaId;

    //SE VENDA OU NAO RENDA PRE VALOR DA DATA DE VENCIMENTO É NULO
    if (
      this.state.item.operacao === 1 ||
      this.state.item.patrimonio == null ||
      (this.state.item.patrimonio.tipoInvestimento !== 3 &&
        this.state.item.patrimonio.tipoInvestimento !== 6)
    ) {
      lancamento.dataVencimento = null;
    }

    if (lancamento.quantidade != null && lancamento.valor != null) {
      if (this.state.item.patrimonio != null) {
        if (
          this.state.item.patrimonio.tipoInvestimento === 8 ||
          this.state.item.patrimonio.tipoInvestimento === 10 ||
          this.state.item.patrimonio.tipoInvestimento === 99
        ) {
          lancamento.valorCalculado =
            (this.state.item.valor != null ? this.state.item.valor : 0) *
            (this.state.item.quantidade != null &&
            this.state.item.quantidade >= 0
              ? this.state.item.quantidade
              : 0);
        } else if (this.state.item.patrimonio.tipoInvestimento === 4) {
          lancamento.valorCalculado =
            (this.state.item.valor != null ? this.state.item.valor : 0) /
            (this.state.item.quantidade != null &&
            this.state.item.quantidade > 0
              ? this.state.item.quantidade
              : 1);
        }
      }
    }

    //var url = global.server_api + 'api/lancamento' + (this.state.item.id != null ? "/" + this.state.item.id: "");
    var url =
      global.server_api_new +
      global.apiToken +
      "/lancamento" +
      (this.state.item.id != null ? "/" + this.state.item.id : "");

    var config = {
      headers: {
        Authorization: "bearer " + this.state.accessToken,
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Headers": "Authorization",
        "Access-Control-Allow-Methods":
          "GET, POST, OPTIONS, PUT, PATCH, DELETE",
      },
    };

    var currentScroll = [
      document.documentElement.scrollLeft || document.body.scrollLeft,
      document.documentElement.scrollTop || document.body.scrollTop,
    ];

    global.spinnerShow($);

    axios.post(url, lancamento, config).then((res) => {
      //console.log(res.data);

      if (res.data.success === true) {
        this.limparSelecao();

        const options = {
          title: "Cadastro de Lançamento",
          message: "Lançamento salvo com sucesso!",
          buttons: [
            {
              label: "Ok",
              className: "cadastro-lancamento-popup-ok",
              onClick: () => {
                this.cadastroLancamento.hide();

                //Esta na tela de lançamento
                if (
                  $("#tela-cadastro-lancamento").length === 1 ||
                  $("#tela-patrimonios").length === 1
                ) {
                  window.location.reload();
                }
              },
            },
          ],
        };

        confirmAlert(options);

        setTimeout(function () {
          $(
            "#react-confirm-alert > div > div > div > div > button:nth-child(1)"
          ).click();
        }, 2000);
      } else {
        var msg = null;

        switch (res.data.exception.Message) {
          case "Este ativo ainda não possui cotação nessa data. Tente novamente no próximo dia útil.":
            msg =
              "Esse ativo ainda não possui cotação pois os dados são atualizados no fechamento do dia anterior. Tente no próximo dia útil lançar este investimento.";
            break;
          default:
            msg = res.data.exception.Message;
            break;
        }

        const options = {
          title: "Erro ao Salvar Lançamento",
          message: msg,
          buttons: [
            {
              label: "Ok",
            },
          ],
        };

        confirmAlert(options);
      }

      global.spinnerHide($, currentScroll);
    });
  };

  preparaEnviarProblema = () => {
    if (
      !document.getElementById("atualMensagem").value ||
      !this.state.atualAssunto
    ) {
      this.setState({ alertEnvioForm: true });
      return;
    } else if (
      this.state.atualAssunto === "Bug" &&
      (this.state.paginaBug == undefined || this.state.familiaBug == undefined)
    ) {
      this.setState({ alertEnvioForm: true });
      return;
    }

    if (
      this.fileInput != null &&
      this.fileInput.current != null &&
      this.fileInput.current.files.length > 0
    ) {
      this.getBase64(this.fileInput.current.files[0], (result) => {
        // console.log(result.split(",")[1]); // Foto base64
        this.enviarProblema(
          this.fileInput.current.files[0].name,
          result.split(",")[1]
        );
      });
    } else {
      this.enviarProblema(null, null);
    }
  };

  enviarProblema = (nome, anexo) => {
    //var url = global.server_api + '/duvida';
    var url = global.server_api_new + global.apiToken + "/planejador/duvida";

    var config = {
      headers: {
        Authorization: "bearer " + this.state.accessToken,
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Headers": "Authorization",
        "Access-Control-Allow-Methods":
          "GET, POST, OPTIONS, PUT, PATCH, DELETE",
      },
    };

    var currentScroll = [
      document.documentElement.scrollLeft || document.body.scrollLeft,
      document.documentElement.scrollTop || document.body.scrollTop,
    ];

    global.spinnerShow($);

    var envio = {
      Nome: this.state.atualNome,
      Email: this.state.atualEmail,
      Telefone: this.state.atualTelefone,
      Assunto: this.state.atualAssunto,
      PaginaBug: this.state.paginaBug,
      FamiliaBug: this.state.familiaBug,
      Mensagem: document.getElementById("atualMensagem").value,
      ArquivoBase64: anexo,
      Filename: nome,
    };
    console.log(envio);

    axios.post(url, envio, config).then((res) => {
      global.spinnerHide($, currentScroll);

      if (res.data.success === true) {
        this.limparSelecaoEmail();
        this.modalReportar.hide();
      } else {
        console.log("error!!!", res.data.exception);

        const options = {
          title: "Erro ao enviar pedido de ajuda",
          message: res.data.exception.Message,
          buttons: [
            {
              label: "Ok",
            },
          ],
        };

        confirmAlert(options);
      }
    });
  };

  ReportarModal = () => {
    const inputStyle = {
      paddingLeft: 25,
      height: 38,
    };

    return (
      <SkyLight
        ref={(ref) => (this.modalReportar = ref)}
        transitionDuration={0}
        dialogStyles={{ borderRadius: "1rem", padding: "2rem" }}
        title={
          <>
            <div className="pop-up-title">
              <i className="ni ni-check-bold"></i> <h2>Reportar problema</h2>
            </div>
          </>
        }
      >
        <Row>
          <Col lg="6" xl="6">
            <div>
              <label>Nome *</label>
              <input
                type="text"
                id="nome"
                style={inputStyle}
                defaultValue={this.state.atualNome}
                disabled
              />
            </div>
          </Col>
          <Col lg="6" xl="6">
            <div>
              <label>Email *</label>
              <input
                type="text"
                id="email"
                style={inputStyle}
                defaultValue={this.state.atualEmail}
                disabled
              />
            </div>
          </Col>
          <Col lg="6" xl="6">
            <div>
              <label>Telefone *</label>
              <input
                type="text"
                id="telefone"
                style={inputStyle}
                defaultValue={this.state.atualTelefone}
                disabled
              />
            </div>
          </Col>
          <Col lg="6" xl="6">
            <div>
              <label>Motivo *</label>
              <Select
                options={this.reportar}
                className="select-component"
                placeholder="Selecione..."
                onChange={(e) => {
                  this.setState({
                    atualAssunto: e.value,
                    alertEnvioForm: false,
                  });
                }}
              />
            </div>
          </Col>

          {this.state.atualAssunto === "Bug" && (
            <>
              <Col lg="6" xl="6">
                <div>
                  <label>Página do bug *</label>
                  <Select
                    options={this.paginaBug}
                    className="select-component"
                    placeholder="Selecione..."
                    onChange={(e) => {
                      this.setState({
                        paginaBug: e.value,
                        alertEnvioForm: false,
                      });
                    }}
                  />
                </div>
              </Col>
              <Col lg="6" xl="6">
                <div>
                  <label>Família/Cliente *</label>
                  <input
                    type="text"
                    id="nome"
                    style={inputStyle}
                    onChange={(e) => {
                      this.setState({
                        familiaBug: e.target.value,
                        alertEnvioForm: false,
                      });
                    }}
                  />
                </div>
              </Col>
            </>
          )}

          <Col lg="6" xl="6">
            <div>
              <label>Arquivo</label>
              <input id="arquivoEmail" type="file" ref={this.fileInput} />
              <p style={{ fontSize: 13 }}>
                Se desejar, anexe um print da tela para entenderemos melhor o
                problema encontrado na plataforma
              </p>
            </div>
          </Col>
          <Col lg="6" xl="6">
            <div>
              <label>Mensagem *</label>
              <textarea
                id="atualMensagem"
                style={{ padding: "5px 15px", minHeight: 110 }}
                placeholder={
                  this.state.atualAssunto === "Bug"
                    ? "Descreva o problema encontrado e, se for o caso, a operação que estava tentando fazer ao encontrar o erro e qual era o comportamento esperado"
                    : "Descreva o problema encontrado"
                }
                onChange={() => this.setState({ alertEnvioForm: false })}
              />
            </div>
          </Col>
          <div
            style={{ display: "flex", justifyContent: "center", width: "100%" }}
          >
            <button
              id="salvar-lancamento"
              className="featured-button"
              onClick={() => {
                this.preparaEnviarProblema();
              }}
              style={{
                backgroundImage:
                  "url(" +
                  require("../../assets/img/theme/botao-destaque.png") +
                  ")",
              }}
            >
              Enviar
            </button>
          </div>
          <span
            style={{
              fontSize: 11,
              color: "red",
              display: this.state.alertEnvioForm ? "block" : "none",
            }}
          >
            Por favor, preencha todos os campos obrigatórios antes de enviar
          </span>
        </Row>
      </SkyLight>
    );
  };

  setUrlVideoFechar = () => {
    this.setState({
      videoURL: "",
    });
  };

  setUrlVideoAbrir = () => {
    this.setState({
      videoURL: "https://player.vimeo.com/video/518358268",
    });
  };

  AjudaModal = () => {
    // console.log(window.location.pathname);
    if (window.location.pathname != "/admin/aposentadoria") {
      return (
        <div id="ajuda-modal">
          <SkyLight
            ref={(ref) => (this.ajudaModal = ref)}
            transitionDuration={0}
            dialogStyles={{ borderRadius: "1rem", padding: "2rem" }}
            title={
              <>
                <div className="pop-up-title">
                  <h2 id="ajuda-titulo">Titulo</h2>
                </div>
              </>
            }
          >
            <Row id="ajuda-conteudo"></Row>
            <Row id="ajuda-botoes">
              <Col lg="12" xl="12">
                <button
                  onClick={() => this._ajudanext()}
                  className="aux-button"
                  style={
                    this.state.passoAtual !== this.state.qtdPassos
                      ? { float: "right", marginRight: "10px" }
                      : { display: "none" }
                  }
                >
                  Próximo
                </button>
                <button
                  onClick={() => this._ajudaprev()}
                  className="aux-button"
                  style={
                    this.state.passoAtual !== 1
                      ? { float: "right", marginRight: "10px" }
                      : { display: "none" }
                  }
                >
                  Anterior
                </button>
              </Col>
            </Row>
          </SkyLight>
        </div>
      );
    } else {
      return (
        <div id="ajuda-modal">
          <SkyLight
            ref={(ref) => (this.ajudaModal = ref)}
            transitionDuration={0}
            hideOnOverlayClicked
            afterOpen={this.setUrlVideoAbrir}
            beforeClose={this.setUrlVideoFechar}
            dialogStyles={{ borderRadius: "1rem", padding: "2rem" }}
            title={
              <>
                <div className="pop-up-title">
                  <h2 id="ajuda-titulo">Titulo</h2>
                </div>
              </>
            }
          >
            <Row id="ajuda-conteudo"></Row>
            <Row>
              <div class="container py-5" style={{ minHeight: 600 }} id="video">
                <div
                  class="row justify-content-center"
                  style={{ width: "100%", height: "100%" }}
                >
                  <iframe
                    src={this.state.videoURL}
                    ref="vidRef"
                    style={{ width: "100%", height: "100%" }}
                    allow="fullscreen"
                    allowFullScreen
                    title="vídeo selecionado"
                  ></iframe>
                </div>
              </div>
            </Row>
            <Row id="ajuda-botoes">
              <Col lg="12" xl="12">
                <button
                  onClick={() => this._ajudanext()}
                  className="aux-button"
                  style={
                    this.state.passoAtual !== this.state.qtdPassos
                      ? { float: "right", marginRight: "10px" }
                      : { display: "none" }
                  }
                >
                  Fechar
                </button>
              </Col>
            </Row>
          </SkyLight>
        </div>
      );
    }
  };

  _ajudanext = () => {
    let passoAtual = this.state.passoAtual;

    passoAtual += 1;

    this.setState(
      {
        passoAtual: passoAtual,
      },
      function () {
        $(".passos").hide();
        $("#passo_" + passoAtual).show();
      }
    );
  };

  _ajudaprev = () => {
    let passoAtual = this.state.passoAtual;

    passoAtual -= 1;

    this.setState(
      {
        passoAtual: passoAtual,
      },
      function () {
        $(".passos").hide();
        $("#passo_" + passoAtual).show();
      }
    );
  };

  render() {
    const { rightBarTopRoutes } = this.props;

    const HelpTooltip = withStyles((theme) => ({
      tooltip: {
        backgroundColor: "#0FA4C3",
        color: "white",
        fontSize: 12,
        fontWeight: "bold",
        fontFamily: "Open Sans",
      },
    }))(Tooltip);

    const ProblemTooltip = withStyles((theme) => ({
      tooltip: {
        backgroundColor: "#ec97a8",
        color: "white",
        fontSize: 12,
        fontWeight: "bold",
        fontFamily: "Open Sans",
      },
    }))(Tooltip);

    return (
      <>
        <Navbar className="navbar-top navbar-dark" expand="md" id="navbar-main">
          <Container fluid>
            <NavItem
              style={
                parseInt(this.state.tipoUsuario) !== 2 &&
                this.state.familiaId == null
                  ? {}
                  : { display: "none" }
              }
            >
              <NavLink
                className="nav-link-icon"
                to="/admin/clientes"
                tag={Link}
              >
                <span className="nav-link-inner--text">Clientes</span>
              </NavLink>
              <div className="navigation-pipe">&nbsp;</div>
            </NavItem>
            <a style={{
              cursor: 'pointer',
              fontWeight: 800,
            }}
              href="http://localhost:3001/admin/planos-sonhos"
            >
              {`Planos & Sonhos`}
            </a>
            <NavItem
              style={
                parseInt(this.state.tipoUsuario) !== 2 &&
                this.state.familiaId == null
                  ? {}
                  : { display: "none" }
              }
            >
              <NavLink
                className="nav-link-icon"
                to="/admin/usuarios"
                tag={Link}
              >
                <span className="nav-link-inner--text">Usuários</span>
              </NavLink>
              <div
                className="navigation-pipe double-pipe"
                style={{ width: "2.5px !important" }}
              >
                &nbsp;
              </div>
            </NavItem>
            <NavItem
              style={
                parseInt(this.state.tipoUsuario) === 1 &&
                this.state.familiaId == null
                  ? { marginLeft: "7px" }
                  : { display: "none" }
              }
            >
              <a
                className="nav-link-icon"
                href="https://meuvista.com/manual-vista/"
                rel="noopener noreferrer"
                target="_blank"
              >
                <span className="nav-link-inner--text">Manual</span>
              </a>
              <div
                className="navigation-pipe double-pipe"
                style={{ width: "2.5px !important" }}
              >
                &nbsp;
              </div>
            </NavItem>
            <NavItem
              style={
                parseInt(this.state.tipoUsuario) !== 2 &&
                this.state.familiaId == null
                  ? {}
                  : { display: "none" }
              }
            >
              <NavLink className="nav-link-icon" to="/index" tag={Link}>
                <span className="nav-link-inner--text">Vista Planejador</span>
              </NavLink>
            </NavItem>
            {/* <NavItem style={parseInt(this.state.tipoUsuario) === 1 && this.state.familiaId == null ? {marginLeft: "7px"} : {display:"none"}}>
              <NavLink className="nav-link-icon" onClick={() => {
                if(this.state.planejadorId) {
                  var currentScroll = [
                    document.documentElement.scrollLeft || document.body.scrollLeft,
                    document.documentElement.scrollTop  || document.body.scrollTop
                  ];
              
                  global.spinnerShow($);
                  this.PlanejadorService.getDadosPlanejador(this.state.accessToken, this.state.planejadorId, 
                    res => {
                      global.spinnerHide($, currentScroll);
                      if(res.singleResult) {
                        this.setState({
                          atualTelefone: res.singleResult != null && res.singleResult.celular ? res.singleResult.celular : "",
                          atualNome: res.singleResult != null && res.singleResult.nome && res.singleResult.sobrenome ? `${res.singleResult.nome} ${res.singleResult.sobrenome}` : "",
                          atualEmail: res.singleResult != null && res.singleResult.usuario != null ? res.singleResult.usuario.email : ""
                        });
                        global.showModal(this.modalReportar);
                      }
                  });
                }
              }}>
                <span className="nav-link-inner--text">Vista Planejador</span>
              </NavLink>
            </NavItem> */}
            <NavItem
              style={
                parseInt(this.state.tipoUsuario) === 2 ||
                this.state.familiaId != null
                  ? {}
                  : { display: "none" }
              }
            >
              <NavLink
                className="nav-link-icon"
                to="/admin/patrimonio"
                tag={Link}
              >
                <span className="nav-link-inner--text">Patrimônio</span>
              </NavLink>
              <div className="navigation-pipe">&nbsp;</div>
            </NavItem>
            <NavItem
              style={
                parseInt(this.state.tipoUsuario) === 2 ||
                this.state.familiaId != null
                  ? {}
                  : { display: "none" }
              }
            >
              <NavLink
                className="nav-link-icon"
                to="/admin/gestao-planos-sonhos"
                tag={Link}
              >
                <span className="nav-link-inner--text">Gestão de Planos</span>
              </NavLink>
              <div
                className="navigation-pipe double-pipe"
                style={{ width: "2.5px !important" }}
              >
                &nbsp;
              </div>
            </NavItem>
            <NavItem
              style={
                parseInt(this.state.tipoUsuario) === 2 ||
                this.state.familiaId != null
                  ? {}
                  : { display: "none" }
              }
            >
              <NavLink
                className="nav-link-icon"
                to="/admin/orcamento"
                tag={Link}
              >
                <span className="nav-link-inner--text">
                  Gestão de Orçamento
                </span>
              </NavLink>
              <div className="navigation-pipe">&nbsp;</div>
            </NavItem>
            <NavItem
              className="new-entry"
              style={
                parseInt(this.state.tipoUsuario) === 2 ||
                this.state.familiaId != null
                  ? {}
                  : { display: "none" }
              }
            >
              <NavLink
                id="novo-lancamento"
                className="nav-link-icon"
                style={{
                  marginLeft: "10px",
                  padding: "10px",
                  backgroundImage:
                    "url(" +
                    require("../../assets/img/theme/botao-destaque.png") +
                    ")",
                }}
                onClick={() => global.showModal(this.cadastroLancamento)}
              >
                <i className="fas fa-star" style={{ color: "#FDD916" }}></i>
                <span className="nav-link-inner--text">Novo Lançamento</span>
              </NavLink>
            </NavItem>
            <NavItem style={{ marginLeft: 20 }}>
              <HelpTooltip title="Ajuda" placement="right">
                <NavLink
                  id="abrir-ajuda"
                  className="nav-link-icon"
                  style={{
                    width: 40,
                    height: 40,
                    borderRadius: "50%",
                    background: "#0FA4C3",
                    textAlign: "center",
                  }}
                  onClick={() =>
                    global.mostrarAjudaDaPagina(
                      this,
                      parseInt(localStorage.getItem("tipo-usuario"))
                    )
                  }
                >
                  <span
                    style={{
                      fontSize: "20px",
                      fontWeight: "bolder",
                      color: "white",
                    }}
                  >
                    ?
                  </span>
                </NavLink>
              </HelpTooltip>
            </NavItem>
            {parseInt(this.state.tipoUsuario) === 1 && (
              <NavItem>
                <ProblemTooltip title="Reportar problema" placement="right">
                  <NavLink
                    id="abrir-reportar-problema"
                    className="nav-link-icon"
                    style={{
                      width: 40,
                      height: 40,
                      borderRadius: "50%",
                      background: "#ec97a8",
                      textAlign: "center",
                    }}
                    onClick={() => {
                      if (this.state.planejadorId) {
                        var currentScroll = [
                          document.documentElement.scrollLeft ||
                            document.body.scrollLeft,
                          document.documentElement.scrollTop ||
                            document.body.scrollTop,
                        ];

                        global.spinnerShow($);
                        this.PlanejadorService.getDadosPlanejador(
                          this.state.accessToken,
                          this.state.planejadorId,
                          (res) => {
                            global.spinnerHide($, currentScroll);
                            if (res.singleResult) {
                              this.setState({
                                atualTelefone:
                                  res.singleResult != null &&
                                  res.singleResult.celular
                                    ? res.singleResult.celular
                                    : "",
                                atualNome:
                                  res.singleResult != null &&
                                  res.singleResult.nome &&
                                  res.singleResult.sobrenome
                                    ? `${res.singleResult.nome} ${res.singleResult.sobrenome}`
                                    : "",
                                atualEmail:
                                  res.singleResult != null &&
                                  res.singleResult.usuario != null
                                    ? res.singleResult.usuario.email
                                    : "",
                              });
                              global.showModal(this.modalReportar);
                            }
                          }
                        );
                      }
                    }}
                  >
                    <span
                      style={{
                        fontSize: "20px",
                        fontWeight: "bolder",
                        color: "white",
                      }}
                    >
                      !
                    </span>
                  </NavLink>
                </ProblemTooltip>
              </NavItem>
            )}
            <NavItem>
              <NavLink
                id="atualizar-dados"
                className="nav-link-icon"
                style={{ visibility: "hidden" }}
                onClick={() => this.atualizarDados(true)}
              >
                <i className="fas fa-question-circle"></i>
              </NavLink>
            </NavItem>
            <NavItem className="admin-right">
              <div style={{ paddingHorizontal: "1.5rem" }}>
                {/* Navigation */}
                <Nav navbar>{this.createLinks(rightBarTopRoutes)}</Nav>
              </div>
            </NavItem>
          </Container>
        </Navbar>
        {this.NovoLancamentoModal()}
        {this.AjudaModal()}
        {this.ReportarModal()}
      </>
    );
  }
}

export default AdminNavbar;
