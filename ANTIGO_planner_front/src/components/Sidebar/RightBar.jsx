/*!

=========================================================
* Argon Dashboard React - v1.0.0
=========================================================

* Product Page: https://www.creative-tim.com/product/argon-dashboard-react
* Copyright 2019 Creative Tim (https://www.creative-tim.com)
* Licensed under MIT (https://github.com/creativetimofficial/argon-dashboard-react/blob/master/LICENSE.md)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/

import React from "react";
import { PropTypes } from "prop-types";
import Chart from "react-apexcharts";
import { MDBProgress } from "mdbreact";
import $ from "jquery";
import axios from "axios";

// reactstrap components
import { Button, Card, CardHeader, Navbar, Table, Row } from "reactstrap";

class Rightbar extends React.Component {
  moment = require("moment");

  state = {
    collapseOpen: false,
    maisLancamentosPorCategoria: false,
    lancamentos: [],
    dadosOrcamento: {
      options: {
        labels: [],
      },
      series: [],
    },
    dadosCategorias: [],
  };

  componentDidMount() {
    $("#right-sidenav-main").click(function (e) {
      if (e.offsetX > 0) {
        //alert('not before');
      } else {
        $("#right-sidenav-main").toggleClass("closed", 5000);

        if (
          $("#right-sidenav-main").hasClass("closed") ||
          $(window).width() < 1200
        ) {
          $("#root > div.main-content > div.home-index").css("width", "98%");
          $("#root > div.main-content > div#tela-cadastro-lancamento").css(
            "width",
            "100%"
          );
          $("#root > div.main-content > div#tela-gestao-orcamento").css(
            "width",
            "100%"
          );
        } else {
          $("#root > div.main-content > div.home-index").css(
            "width",
            "calc(98% - 352px)"
          );
          $("#root > div.main-content > div#tela-cadastro-lancamento").css(
            "width",
            "calc(100% - 352px)"
          );
          $("#root > div.main-content > div#tela-gestao-orcamento").css(
            "width",
            "calc(100% - 352px)"
          );
          $("#right-sidenav-main").css("background", "#f7faff");
        }
      }
    });

    this.setState(
      {
        accessToken: localStorage.getItem("access-token"),
        empresaId: localStorage.getItem("empresa-id"),
        familiaId: localStorage.getItem("familia-id"),
      },
      function () {
        this.getData();
      }
    );
  }

  clearData = () => {
    this.setState({
      lancamentos: [],
      dadosOrcamento: {
        options: {
          labels: [],
        },
        series: [],
      },
      dadosCategorias: [],
    });
  };

  getData = () => {
    //console.log('GetData Inicio');

    //Nao logado
    if (this.state.accessToken == null) {
      //console.log('GetData Token Nulo');

      var token = localStorage.getItem("access-token");

      //Seta token e tenta novamente
      if (token != null) {
        //console.log('GetData Token Nova Tentativa');

        this.setState(
          {
            accessToken: token,
            empresaId: localStorage.getItem("empresa-id"),
            familiaId: localStorage.getItem("familia-id"),
          },
          function () {
            this.getData();
          }
        );
      }

      return;
    }

    //Datas nao preenchidas
    if (this.state.startDate == null || this.state.endDate == null) {
      //console.log('GetData Datas Nulas');
      this.clearData();
    }
    //Datas preenchidas (refresh)
    else {
      this.atualizarDadosTipoOrcamento();
      this.atualizarDadosOrcamento();
      this.atualizarDadosCategorias();
      this.atualizarUltimosLancamentos();
    }
  };

  setDate = (date, tipo, callback) => {
    if (date != null) {
      if (tipo === "inicio") {
        date.setHours(0);
        date.setMinutes(0, 0, 0);

        const startDate = date;

        this.setState({ startDate }, function () {
          if (callback) {
            callback();
          }
        });
      } else if (tipo === "fim") {
        date.setHours(20);
        date.setMinutes(59, 59, 0);

        const endDate = date;

        this.setState({ endDate }, function () {
          if (callback) {
            callback();
          }
        });
      }
    }
  };

  atualizarUltimosLancamentos = () => {
    //Ultimos 3 lancamentos da familia
    var quantidade = 3;
    //var url = global.server_api + 'api/dashboard/ultimos-lancamentos/' + this.state.familiaId + "/" + quantidade;
    var url =
      global.server_api_new +
      global.apiToken +
      "/dashboard/ultimos-lancamentos/" +
      this.state.familiaId +
      "/" +
      quantidade;

    var config = {
      headers: {
        Authorization: "bearer " + this.state.accessToken,
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Headers": "Authorization",
        "Access-Control-Allow-Methods":
          "GET, POST, OPTIONS, PUT, PATCH, DELETE",
      },
    };

    axios.get(url /*, filtro*/, config).then((res) => {
      const lancamentos = res.data.results;
      this.setState({ lancamentos });
    });
  };

  preparaDataParaGrid = (dataString) => {
    var data = this.moment(dataString, "YYYY-MM-DDThh:mm:ss");
    return data.format("DD/MM/YYYY");
  };

  atualizarDadosTipoOrcamento = () => {
    //var url = global.server_api + 'api/dashboard/tipo-orcamento-previsto-realizado/' + this.state.familiaId;
    var url =
      global.server_api_new +
      global.apiToken +
      "/dashboard/tipo-orcamento-previsto-realizado/" +
      this.state.familiaId;

    var filtro = {
      DataInicial: this.state.startDate,
      DataFinal: this.state.endDate,
    };

    var config = {
      headers: {
        Authorization: "bearer " + this.state.accessToken,
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Headers": "Authorization",
        "Access-Control-Allow-Methods":
          "GET, POST, OPTIONS, PUT, PATCH, DELETE",
      },
    };

    axios.post(url, filtro, config).then((res) => {
      const dadosTipoOrcamento = res.data.results;
      this.reordenacaoListaPorNome(dadosTipoOrcamento, [
        "Receitas",
        "Despesas",
        "Investimentos",
        "Dívidas",
      ]);
      var estimadoTotal = 0;
      var realizadoTotal = 0;
      dadosTipoOrcamento.forEach(function (item) {
        estimadoTotal +=
          item.nome === "Receitas" ? item.estimado : -item.estimado;
        realizadoTotal +=
          item.nome === "Receitas" ? item.totalMedio : -item.totalMedio;
      });
      this.setState({ dadosTipoOrcamento, estimadoTotal, realizadoTotal });
    });
  };

  reordenacaoListaPorNome = (array, nomes) => {
    nomes.forEach(function (nome, i) {
      for (var j = 0; j < array.length; j++) {
        var item = array[j];
        if (item.nome === nome) {
          var aux = array[i];
          array[i] = item;
          array[j] = aux;
        }
      }
    });
  };

  atualizarDadosOrcamento = () => {
    //var url = global.server_api + 'api/dashboard/orcamento-previsto-realizado/' + this.state.familiaId;
    var url =
      global.server_api_new +
      global.apiToken +
      "/dashboard/orcamento-previsto-realizado/" +
      this.state.familiaId;

    var filtro = {
      DataInicial: this.state.startDate,
      DataFinal: this.state.endDate,
    };

    var config = {
      headers: {
        Authorization: "bearer " + this.state.accessToken,
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Headers": "Authorization",
        "Access-Control-Allow-Methods":
          "GET, POST, OPTIONS, PUT, PATCH, DELETE",
      },
    };

    axios.post(url, filtro, config).then((res) => {
      var dadosOrcamento = res.data.results;

      var labels = [];
      var series = [];

      if (dadosOrcamento != null && dadosOrcamento.length > 0) {
        dadosOrcamento.forEach((item) => {
          if (item.totalMedio > 0 && item.tipoOrcamento !== 0) {
            labels.push(item.nome);
            series.push(item.totalMedio);
          }
        });
      }

      dadosOrcamento = {
        options: {
          labels: labels,
          legend: {
            show: false,
          },
          chart: {
            background: "#ffffff",
          },
        },
        series: series,
      };

      this.setState({ dadosOrcamento });
    });
  };

  atualizarDadosCategorias = () => {
    //var url = global.server_api + 'api/dashboard/categoria-previsto-realizado/' + this.state.familiaId;
    var url =
      global.server_api_new +
      global.apiToken +
      "/dashboard/categoria-previsto-realizado/" +
      this.state.familiaId;

    var filtro = {
      DataInicial: this.state.startDate,
      DataFinal: this.state.endDate,
      Ordenacao: "Previsto Desc",
    };

    var config = {
      headers: {
        Authorization: "bearer " + this.state.accessToken,
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Headers": "Authorization",
        "Access-Control-Allow-Methods":
          "GET, POST, OPTIONS, PUT, PATCH, DELETE",
      },
    };

    axios.post(url, filtro, config).then((res) => {
      var results = res.data.results;

      var dadosCategorias = [];

      if (results != null && results.length > 0) {
        results.forEach((item) => {
          dadosCategorias.push({
            nome: item.nome,
            percentual:
              item.estimado > 0 ? (item.totalMedio / item.estimado) * 100 : 0,
            estimado: item.estimado,
            realizado: item.totalMedio,
            tipoOrcamento: item.tipoOrcamento,
          });
        });
      }
      this.setState({ dadosCategorias });
    });
  };

  categoriesAreaChart = {
    options: {
      dataLabels: {
        enabled: false,
      },
      stroke: {
        curve: "smooth",
      },
      xaxis: {
        categories: [
          "Fam.",
          "Res.",
          "Adu.",
          "Fil.",
          "Inv.",
          "Div.",
          "Tra.",
          "Out.",
        ],
      },
      colors: ["#FA5C86"],
    },
    series: [
      {
        name: "Valor Gasto",
        data: [17, 14, 15, 3, 4, 4.5, 20],
      },
    ],
  };

  // toggles collapse between opened and closed (true/false)
  toggleCollapse = () => {
    this.setState({
      collapseOpen: !this.state.collapseOpen,
    });
  };

  // closes the collapse
  closeCollapse = () => {
    this.setState({
      collapseOpen: false,
    });
  };

  //Progress bar
  createProgressBar = () => {
    if (this.state.dadosCategorias == null) {
      return <></>;
    }

    return this.state.dadosCategorias.map((item, key) => {
      if (!this.state.maisLancamentosPorCategoria && key > 9) {
        return <></>;
      }
      return (
        <div
          className="progressLine"
          style={{ position: "relative" }}
          key={key}
        >
          <div className="progress-bar-title">{item.nome}</div>
          {this.dadosProgress(item, key)}
        </div>
      );
    });
  };

  dadosProgress = (item, key) => {
    var color;
    if (item.tipoOrcamento === 0) {
      color =
        item.estimado != null && item.estimado > 0
          ? item.percentual <= 100
            ? "success"
            : "info"
          : item.realizado != null && item.realizado > 0
          ? "info"
          : "success";
    } else {
      color =
        item.estimado != null && item.estimado > 0
          ? item.percentual <= 100
            ? "success"
            : "danger"
          : item.realizado != null && item.realizado > 0
          ? "danger"
          : "success";
    }
    return (
      <MDBProgress
        key={key}
        material
        value={
          item.estimado != null && item.estimado > 0
            ? item.percentual
            : item.realizado != null && item.realizado > 0
            ? 100
            : 0
        }
        color={color}
        height="25px"
      >
        <div style={{ position: "absolute", left: "12px" }}>
          <b>
            {`${
              item.realizado != null
                ? item.realizado
                    .toLocaleString("pt-BR", {
                      style: "currency",
                      currency: "BRL",
                    })
                    .replace("R$", "")
                : "0.00"
            } / ${
              item.estimado != null
                ? item.estimado
                    .toLocaleString("pt-BR", {
                      style: "currency",
                      currency: "BRL",
                    })
                    .replace("R$", "")
                : "0.00"
            } 
          (${item.percentual != null ? item.percentual.toFixed(1) : "0.00"}%)`}
          </b>
        </div>
      </MDBProgress>
    );
  };

  dadosDosUltimosLancamentos = () => {
    if (this.state.lancamentos == null || this.state.lancamentos.length === 0) {
      return (
        <tr style={{ fontSize: "12px", marginLeft: "22px" }}>
          <td colSpan="3">Nenhum lançamento realizado hoje!</td>
        </tr>
      );
    } else {
      return this.state.lancamentos.map((item, key) => {
        var valor =
          item.valorParcela != null && item.valorParcela !== ""
            ? item.valorParcela
            : item.valor;

        return (
          <tr key={key}>
            <td
              style={
                item.categoria.orcamento.tipoOrcamento === 0
                  ? { color: "#1e8d9d", whiteSpace: "normal", fontSize: "11px" }
                  : {
                      color: "rgb(250, 93, 134)",
                      whiteSpace: "normal",
                      fontSize: "11px",
                    }
              }
            >
              {item.categoria.nome}
            </td>
            <td style={{ fontSize: "11px" }}>
              {valor
                .toLocaleString("pt-BR", { style: "currency", currency: "BRL" })
                .replace("R$", "")}
            </td>
            <td style={{ fontSize: "11px" }}>
              {this.preparaDataParaGrid(item.data)}
            </td>
          </tr>
        );
      });
    }
  };

  //Last Entries
  lastEntriesCard = () => {
    return (
      <Card className="shadow" style={{ borderRadius: 15 }}>
        <CardHeader className="border-0" style={{ borderRadius: 15 }}>
          <Row className="align-items-center">
            <div className="col">
              <img
                src={require("../../assets/img/theme/icone-calendario.png")}
                alt="Últimos Lançamentos"
                className="before-title-img"
              />
              <h3 className="mb-0 chart-title">Lançamentos do dia</h3>
            </div>
            <div className="col text-right">
              <Button color="primary" href="/admin/extrato" size="sm">
                +
              </Button>
            </div>
          </Row>
        </CardHeader>
        <Table>
          <tbody>{this.dadosDosUltimosLancamentos()}</tbody>
        </Table>
      </Card>
    );
  };

  mostrarEsconderMaisLancamentos = (e) => {
    e.preventDefault();
    this.setState({
      maisLancamentosPorCategoria: !this.state.maisLancamentosPorCategoria,
    });
  };

  mostrarCardGastosPorCategoria = () => {
    return (
      <>
        <Card className="shadow" style={{ borderRadius: 15 }}>
          <CardHeader className="border-0" style={{ borderRadius: 15 }}>
            <Row className="align-items-center">
              <div className="col">
                <img
                  src={require("../../assets/img/theme/icone-progress.png")}
                  alt="Lançamentos por Categorias"
                  className="before-title-img before-title-img-2"
                />
                <h3 className="mb-0 chart-title">
                  Lançamentos por <span>categorias</span>
                </h3>
              </div>
              <div className="col text-right">
                <Button
                  color="primary"
                  onClick={(e) => this.mostrarEsconderMaisLancamentos(e)}
                  size="sm"
                >
                  {!this.state.maisLancamentosPorCategoria ? "+" : "-"}
                </Button>
              </div>
            </Row>
          </CardHeader>
          <div className="chart-expense-adjust">
            <div className="expense-progress-bar">
              {this.createProgressBar()}
            </div>
          </div>
        </Card>
      </>
    );
  };

  mostrarCardUltimosLancamentos = () => {
    return (
      <>
        <div className="entries-right">{this.lastEntriesCard()}</div>
      </>
    );
  };

  mostrarCardGastosPorOrcamento = () => {
    return (
      <>
        <Card className="shadow" style={{ borderRadius: 15, border: "none" }}>
          <CardHeader
            className="border-0"
            style={{ borderRadius: "15px 15px 0 0" }}
          >
            <Row className="align-items-center">
              <div className="col">
                <img
                  src={require("../../assets/img/theme/icone-analytics.png")}
                  alt="Orçamentos Realizados"
                  className="before-title-img before-title-img-2"
                />
                <h3 className="mb-0 chart-title">
                  Orçamentos <span>realizados</span>
                </h3>
              </div>
              {/*
              <div className="col text-right">
                <Button color="primary" href="#teste" onClick={e => e.preventDefault()} size="sm">+</Button>
              </div>
              */}
            </Row>
          </CardHeader>
        </Card>
        <div className="chart-expense-adjust">
          <Chart
            options={this.state.dadosOrcamento.options}
            series={this.state.dadosOrcamento.series}
            className="chart-expense-2"
            type="donut"
            width="100%"
          />
        </div>
      </>
    );
  };

  mostrarDadosSumarioPrevistoRealizado = () => {
    if (
      this.state.dadosTipoOrcamento == null ||
      this.state.dadosTipoOrcamento.length === 0
    ) {
      return (
        <tr style={{ fontSize: "12px", marginLeft: "22px" }}>
          <td colSpan="3">Nenhuma tipo de categoria encontrado!</td>
        </tr>
      );
    } else {
      return this.state.dadosTipoOrcamento.map((item, key) => {
        var estimado = item.estimado != null ? item.estimado : 0;
        var totalMedio = item.totalMedio != null ? item.totalMedio : 0;

        return (
          <tr key={key}>
            <td
              style={{
                whiteSpace: "normal",
                paddingLeft: "1rem",
                fontSize: 11,
              }}
            >
              <div>
                <b>{item.nome}</b>
              </div>
            </td>
            <td style={{ color: "#5AC9FB", fontSize: "11px" }}>
              {estimado
                .toLocaleString("pt-BR", { style: "currency", currency: "BRL" })
                .replace("R$", "")}
            </td>
            <td style={{ color: "#FA5D86", fontSize: "11px" }}>
              {totalMedio
                .toLocaleString("pt-BR", { style: "currency", currency: "BRL" })
                .replace("R$", "")}
            </td>
          </tr>
        );
      });
    }
  };

  mostrarSumarioPrevistoRealizadoGeral = () => {
    return (
      <>
        <Card
          className="shadow home-summary-estimated-realizaded-card-dark"
          style={{ borderRadius: 15 }}
        >
          <Table>
            <thead style={{ borderBottom: "1px solid #dddddd" }}>
              <tr>
                <th></th>
                <th>
                  <h3
                    className="mb-0 chart-title"
                    style={{
                      textTransform: "none",
                      letterSpacing: 0,
                      fontSize: "12px",
                      color: "#9A3DFF",
                    }}
                  >
                    Estimativa
                  </h3>
                </th>
                <th>
                  <h3
                    className="mb-0 chart-title"
                    style={{
                      textTransform: "none",
                      letterSpacing: 0,
                      fontSize: "12px",
                      color: "#28ADC0",
                    }}
                  >
                    Realizado
                  </h3>
                </th>
              </tr>
            </thead>
            <tbody>{this.mostrarDadosSumarioPrevistoRealizado()}</tbody>
            {this.mountFoot()}
          </Table>
        </Card>
      </>
    );
  };
  mountFoot = () => {
    if (this.state.estimadoTotal != null && this.state.realizadoTotal != null) {
      return (
        <tfoot>
          <tr style={{ borderTop: "1px solid #dddddd" }}>
            <td>Saldo</td>
            <td style={{ fontSize: "11px" }}>
              {this.state.estimadoTotal
                .toLocaleString("pt-BR", { style: "currency", currency: "BRL" })
                .replace("R$", "")}
            </td>
            <td style={{ fontSize: "11px" }}>
              {this.state.realizadoTotal
                .toLocaleString("pt-BR", { style: "currency", currency: "BRL" })
                .replace("R$", "")}
            </td>
          </tr>
        </tfoot>
      );
    } else {
      return <></>;
    }
  };
  montarBarraLateral = () => {
    const { menuItens } = this.props;

    if (menuItens == null) {
      return <></>;
    }

    return menuItens.map((item, key) => {
      return (
        <div key={key} style={{ background: "#FFF", borderRadius: 15 }}>
          {item === "ultimosLancamentos" &&
            this.mostrarCardUltimosLancamentos()}
          {item === "gastosPorOrcamento" &&
            this.mostrarCardGastosPorOrcamento()}
          {item === "gastosPorCategoria" &&
            this.mostrarCardGastosPorCategoria()}
          {item === "sumarioPrevistoRealizadoGeral" &&
            this.mostrarSumarioPrevistoRealizadoGeral()}
        </div>
      );
    });
  };

  rightBarBottom = () => {
    return (
      <div style={{ paddingHorizontal: "1.5rem" }}>
        <div>{this.montarBarraLateral()}</div>
      </div>
    );
  };

  updateStartDate = (callback) => {
    var startDate = localStorage.getItem("dataInicio");
    //console.log('Lido: ' + startDate);
    if (startDate != null) {
      try {
        startDate = JSON.parse(startDate);
        //console.log(startDate);
        this.setState(
          {
            startDate: startDate,
          },
          function () {
            if (callback != null) {
              callback();
            }
          }
        );
      } catch (ex) {
        console.log(ex);
      }
    }

    localStorage.removeItem("startDate");
  };

  updateEndDate = (callback) => {
    var endDate = localStorage.getItem("dataFim");
    //console.log('Lido: ' + endDate);
    if (endDate != null) {
      try {
        endDate = JSON.parse(endDate);
        //console.log(endDate);
        this.setState(
          {
            endDate: endDate,
          },
          function () {
            if (callback != null) {
              callback();
            }
          }
        );
      } catch (ex) {
        console.log(ex);
      }
    }

    localStorage.removeItem("endDate");
  };

  render() {
    const { id, position } = this.props;
    return (
      <>
        <button
          id="rightbar-startdate-refresh"
          onClick={() => this.updateStartDate(this.getData)}
          style={{ display: "none" }}
        ></button>
        <button
          id="rightbar-enddate-refresh"
          onClick={() => this.updateEndDate(this.getData)}
          style={{ display: "none" }}
        ></button>
        <Navbar
          className={"closed navbar-vertical " + position + " navbar-light"}
          expand="md"
          id={id}
        >
          <div></div>
          {this.rightBarBottom()}
        </Navbar>
      </>
    );
  }
}

Rightbar.defaultProps = {};

Rightbar.propTypes = {
  // links that will be displayed inside the component
  routes: PropTypes.arrayOf(PropTypes.object),
  logo: PropTypes.shape({
    // innerLink is for links that will direct the user within the app
    // it will be rendered as <Link to="...">...</Link> tag
    innerLink: PropTypes.string,
    // outterLink is for links that will direct the user outside the app
    // it will be rendered as simple <a href="...">...</a> tag
    outterLink: PropTypes.string,
    // the image src of the logo
    imgSrc: PropTypes.string.isRequired,
    // the alt for the img
    imgAlt: PropTypes.string.isRequired,
  }),
};

export default Rightbar;
