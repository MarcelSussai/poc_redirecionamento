import React from "react";
import { NavLink as NavLinkRRD, Link } from "react-router-dom";
import { PropTypes } from "prop-types";
import SkyLight from "react-skylight";
import Select from "react-select";
import axios from "axios";
import $ from "jquery";
import { CircularProgress } from "@material-ui/core";

// reactstrap components
import {
  Collapse,
  NavbarBrand,
  Navbar,
  NavItem,
  NavLink,
  Nav,
  Container,
  Row,
  Col,
} from "reactstrap";

class Sidebar extends React.Component {
  imagens = {};

  constructor(props) {
    super(props);
    this.activeRoute.bind(this);

    this.state = {
      collapseOpen: false,
      configuracoesCssDisplay: "none",
      configuracoesCssDisplay2: "none",
      configuracoesIcone: "ni ni-bold-right",
      configuracoesIcone2: "ni ni-bold-right",
      accessToken: localStorage.getItem("access-token"),
      empresaId: localStorage.getItem("empresa-id"),
      familiaId: localStorage.getItem("familia-id"),
      userId: localStorage.getItem("usuario-id"),
      planejadorId: localStorage.getItem("planejador-id"),
      tipoUsuario: parseInt(localStorage.getItem("tipo-usuario")),
      planejadorPrincipal: localStorage.getItem("planejador-principal"), 
      isFlaggedToUseBankIntegration: localStorage.getItem("isFlaggedToUseBankIntegration"),     
    };
  }

  async componentDidMount() {
    this.getDadosFamilia();
    this.getFamilias();
  }

  getDadosFamilia = () => {
    var config = {
      headers: {
        Authorization: "bearer " + this.state.accessToken,
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Headers": "Authorization",
        "Access-Control-Allow-Methods":
          "GET, POST, OPTIONS, PUT, PATCH, DELETE",
      },
    };

    if (this.state.familiaId != null) {
      //Se é o admin tem que setar a empresa da familia
      if (parseInt(this.state.tipoUsuario) === 0) {
        this.getEmpresaFamilia();
      }

      if (this.imagens != null && this.imagens[this.state.familiaId] != null) {
        $("#logo-familia").css(
          "background-image",
          "url(" + this.imagens[this.state.familiaId] + ")"
        );
        return;
      }

      //var url = global.server_api + 'api/familia/imagem/' + this.state.familiaId;
      var url =
        global.server_api_new +
        global.apiToken +
        "/familia/imagem/" +
        this.state.familiaId;

      axios.get(url, config).then((res) => {
        var imagem = res.data.singleResult;

        if (imagem == null || imagem === "") {
          imagem = require("../../assets/img/theme/familia.png");
        }

        this.imagens[this.state.familiaId] = imagem;

        try {
          $("#logo-familia").css("background-image", "url(" + imagem + ")");

          //var url = global.server_api + 'api/familia/' + this.state.familiaId;
          var url =
            global.server_api_new +
            global.apiToken +
            "/familia/" +
            this.state.familiaId;

          axios.get(url, config).then((res) => {
            var familia = res.data.singleResult;

            if (familia != null) {
              this.setState({ nomeFamilia: familia.nome });
            }
          });
        } catch (ex) {
          console.log(ex);
        }
      });
    }
  };

  getEmpresaFamilia = () => {
    var config = {
      headers: {
        Authorization: "bearer " + this.state.accessToken,
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Headers": "Authorization",
        "Access-Control-Allow-Methods":
          "GET, POST, OPTIONS, PUT, PATCH, DELETE",
      },
    };

    if (this.state.familiaId != null) {
      //var url = global.server_api + 'api/familia/' + this.state.familiaId + '/buscaidempresa';
      var url =
        global.server_api_new +
        global.apiToken +
        "/familia/" +
        this.state.familiaId +
        "/buscaidempresa";

      try {
        axios.get(url, config).then((res) => {
          var idEmpresa = res.data.singleResult;

          if (idEmpresa != null) {
            localStorage.setItem("empresa-id", idEmpresa);
          }
        });
      } catch (ex) {
        console.log(ex);
      }
    }
  };

  getFamilias = () => {
    if (this.state.tipoUsuario > 1) {
      return;
    }

    var config = {
      headers: {
        Authorization: "bearer " + this.state.accessToken,
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Headers": "Authorization",
        "Access-Control-Allow-Methods":
          "GET, POST, OPTIONS, PUT, PATCH, DELETE",
      },
    };

    var urlPlanejador = null;

    if (this.state.tipoUsuario === 1 && this.state.planejadorId != null) {
      //urlPlanejador = global.server_api + 'api/familia/planejador/filtro/' + this.state.planejadorId;
      urlPlanejador =
        global.server_api_new +
        global.apiToken +
        "/familia/planejador/filtro/" +
        this.state.planejadorId;
    } else {
      //urlPlanejador = global.server_api + 'api/familia/filtro/';
      urlPlanejador =
        global.server_api_new + global.apiToken + "/familia/filtro/";
    }

    axios.post(urlPlanejador, { ApenasNome: true }, config).then((res) => {
      var familias = [];

      familias.push({
        value: null,
        label: "Voltar para Vista Planejador",
      });

      if (res.data.results != null) {
        var data = res.data.results.sort((item1, item2) => {
          const name1 = item1.nome.toUpperCase();
          const name2 = item2.nome.toUpperCase();

          let comparison = 0;

          if (name1 > name2) {
            comparison = 1;
          } else if (name1 < name2) {
            comparison = -1;
          }

          return comparison;
        });

        data.forEach((item) => {
          familias.push({
            value: item.id,
            label: item.nome,
          });
        });
      }

      this.setState({ familias: familias });
    });
  };

  activeRoute(routeName) {
    return this.props.location.pathname.indexOf(routeName) > -1 ? "active" : "";
  }

  toggleCollapse = () => {
    this.setState({
      collapseOpen: !this.state.collapseOpen,
    });
  };

  closeCollapse = () => {
    this.setState({
      collapseOpen: false,
    });
  };

  mostrarPopup = () => (
    <div className="trocar-familia">
      <SkyLight
        ref={(ref) => (this.modal = ref)}
        transitionDuration={0}
        beforeOpen={this.preCarregarParaEdicao}
        afterClose={this.limparSelecao}
        dialogStyles={{ borderRadius: "1rem", padding: "2rem" }}
        title={
          <>
            <div className="pop-up-title">
              <i className="ni ni-check-bold"></i>
              <h2>Seleção de Cliente</h2>
            </div>
          </>
        }
      >
        <Row>
          <Col lg="12" xl="12">
            {this.montarDropDown(
              this.state.familias,
              "familia-select",
              this.mudarFamilia,
              this.state.familiaId
            )}
          </Col>
        </Row>
      </SkyLight>
    </div>
  );

  montarDropDown = (itens, id, changeMethod, selectedValue) => (
    <Select
      id={id}
      onChange={changeMethod}
      className="select-component"
      options={itens}
      defaultValue={itens != null ? itens[0] : ""}
      placeholder="Selecione..."
      value={
        itens != null && selectedValue != null
          ? itens.find((op) => {
              return op.value === selectedValue;
            })
          : null
      }
    />
  );

  mudarFamilia = (selectedOption) => {
    var familia = selectedOption.value;

    localStorage.removeItem("familia-id");
    localStorage.removeItem("dataInicio");
    localStorage.removeItem("dataFim");

    //Se é o admin tem que limpar a empresa da familia
    if (parseInt(this.state.tipoUsuario) === 0) {
      localStorage.removeItem("empresa-id");
    }

    if (familia != null) localStorage.setItem("familia-id", familia);

    this.setState(
      {
        familiaId: familia,
      },
      function () {
        //Recarrega após selecao
        window.location.href = "/admin/index";
      }
    );
  };

  temPermissao = (perfisPermitidos) => {
    if (perfisPermitidos == null) return true;

    for (var i = 0; i < perfisPermitidos.length; i++) {
      if (perfisPermitidos[i] === parseInt(this.state.tipoUsuario)) return true;
      
    }


    return false;
  };

  temPermissaoCliente = (perfisPermitidos, path) => {
    if (perfisPermitidos == null) return true;

    for (var i = 0; i < perfisPermitidos.length; i++) {
      if (perfisPermitidos[i] === parseInt(this.state.tipoUsuario)) return true;      
    }
    console.log("\n", path);
    if(path == "/integracao-bancaria"){
      alert("ola");
    }


    return false;
  };

  createLinks = (routes) => {
    if (routes == null) {
      return <></>;
    }

    return routes.map((prop, key) => {
      //console.log("prop: ", prop.path, "key", key);
      if (
        prop.hideOnSidebar === true || //Se é para esconder no sidebar
        prop.configurationMenu === true || //Se é menu de configuracao
        prop.userConfigurationMenu === true || //Se é menu de configuracao de usuario
        (prop.perfis != null && !this.temPermissao(prop.perfis)) || //se tipo usuario incompatível com perfil do menu
        (prop.planejadorPrincipal === true &&
          this.state.tipoUsuario === 1 &&
          parseInt(this.state.planejadorPrincipal) !== 1)
      ) {
        //Se necessita de planejador principal (caso planejador)
        return <div key={key}></div>;
      } 

      //let pessoasTeste = [22,82,187,193,237,415,749,1164,1165,1185,87,263,282,334,343,349,350,423,424,712,749,696,81, 761];
      //prod - id de usuario
      //let pessoasTeste = [22,82,187,193,232,237,415,749,1164,1165,1185,242,269,301,893,696,81, 761, 263, 347, 1375];
      //hml - id de usuario
      let pessoasTeste = [187,263,282,334,343,349,350,423,424,712,749,696,81,1378,709,1380,1382,1384,1387];


      let user = parseInt(localStorage.getItem("usuario-id"));

      if (!this.state.familiaId) {
        return <div key={key}></div>;
      } else if (prop.header) {
        return (
          <div style={{ paddingLeft: "1.4rem", fontSize: "1.25rem" }} key={key}>
            <h4 style={{ color: "white", marginBottom: "0.3rem" }}>
              <i
                style={{ marginRight: ".2rem", marginTop: 15 }}
                className={prop.icon}
              />{" "}
              {prop.header}
            </h4>
          </div>
        );
      }
      if (prop.path == "/integracao-bancaria")
      {
        if(this.state.isFlaggedToUseBankIntegration === 'true'){
          return (
            <NavItem
              key={key}
              style={
                this.state.familiaId != null ? { height: 28 } : { display: "none" }
              }
            >
              <NavLink
                to={prop.layout + prop.path}
                tag={NavLinkRRD}
                onClick={this.closeCollapse}
                activeClassName="active"
                style={{ paddingRight: 0 }}
              >
                <i className={prop.icon} style={{ fontSize: 10 }} />
                {prop.name}
              </NavLink>
            </NavItem>
          );
        }
      }
      else{
        return (
          <NavItem
            key={key}
            style={
              this.state.familiaId != null ? { height: 28 } : { display: "none" }
            }
          >
            <NavLink
              to={prop.layout + prop.path}
              tag={NavLinkRRD}
              onClick={this.closeCollapse}
              activeClassName="active"
              style={{ paddingRight: 0 }}
            >
              <i className={prop.icon} style={{ fontSize: 10 }} />
              {prop.name}
            </NavLink>
          </NavItem>
        );
      }
    });
  };

  createConfigLinks = (routes) => {
    if (routes == null) {
      return <></>;
    }

    return routes.map((prop, key) => {
      //console.log("prop: ", prop.path, "key", key);
      if (
        (prop.name === "Clientes" &&
          this.state.familiaId !== null &&
          this.state.familiaId !== undefined) ||
        prop.hideOnSidebar === true || //Se é para esconder no sidebar
        prop.configurationMenu !== true || //Se não é menu de configuracao
        (prop.perfis != null && !this.temPermissao(prop.perfis)) || //Se tipo usuario incompatível com perfil do menu
        (prop.planejadorPrincipal === true &&
          parseInt(this.state.tipoUsuario) === 1 &&
          parseInt(this.state.planejadorPrincipal) !== 1)
      ) {
        //Se necessita de planejador principal (caso planejador)
        return <div key={key}></div>;
      }      

      if (localStorage.getItem("familia-id") !== null) {
        if (prop.verificacaoFamiliaSelecionada === true) {
          return this.renderNavItem(prop, key);
        }
      } else {
        return this.renderNavItem(prop, key);
      }
    });
  };

  renderNavItem = (prop, key) => {
    return (
      <NavItem key={key}>
        <NavLink
          to={prop.layout + prop.path}
          tag={NavLinkRRD}
          onClick={this.closeCollapse}
          activeClassName="active"
        >
          <i className={prop.icon} style={{ paddingRight: 10 }} />
          {prop.name}
        </NavLink>
      </NavItem>
    );
  };

  createUserConfigLinks = (routes) => {
    if (routes == null) {
      return <></>;
    }
    

    return routes.map((prop, key) => {
      if (
        prop.hideOnSidebar === true || //Se é para esconder no sidebar
        prop.userConfigurationMenu !== true || //Se não é menu de configuracao
        (prop.perfis != null && !this.temPermissao(prop.perfis)) || //Se tipo usuario incompatível com perfil do menu
        (prop.planejadorPrincipal === true &&
          this.state.tipoUsuario === 1 &&
          parseInt(this.state.planejadorPrincipal) !== 1)
      ) {
        //Se necessita de planejador principal (caso planejador)
        return <div key={key}></div>;
      }

      return (
        <NavItem key={key}>
          <NavLink
            to={prop.layout + prop.path}
            tag={NavLinkRRD}
            onClick={this.closeCollapse}
            activeClassName="active"
          >
            <i className={prop.icon} />
            {prop.name}
          </NavLink>
        </NavItem>
      );
    });
  };

  trocarVisibilidadeConfiguracoes = () => {
    var novoValor = "block";
    var novoIcone = "ni ni-bold-down";

    if (this.state.configuracoesCssDisplay !== "none") {
      novoValor = "none";
      novoIcone = "ni ni-bold-right";
    }

    this.setState({
      configuracoesCssDisplay: novoValor,
      configuracoesIcone: novoIcone,
    });
  };

  trocarVisibilidadeConfiguracoes2 = () => {
    var novoValor = "block";
    var novoIcone = "ni ni-bold-down";

    if (this.state.configuracoesCssDisplay2 !== "none") {
      novoValor = "none";
      novoIcone = "ni ni-bold-right";
    }

    this.setState({
      configuracoesCssDisplay2: novoValor,
      configuracoesIcone2: novoIcone,
    });
  };

  render() {
    const { routes, logo, id, position } = this.props;
    let navbarBrandProps;
    if (logo && logo.innerLink) {
      navbarBrandProps = {
        to: logo.innerLink,
        tag: Link,
      };
    } else if (logo && logo.outterLink) {
      navbarBrandProps = {
        href: logo.outterLink,
        target: "_blank",
      };
    }

    return (
      <>
        {this.mostrarPopup()}
        <Navbar
          className={
            parseInt(this.state.tipoUsuario) <= 1
              ? "navbar-vertical " +
                position +
                " navbar-light bg-vista-planejador"
              : "navbar-vertical " + position + " navbar-light bg-vista"
          }
          expand="md"
          id={id}
          style={{ overflowX: "hidden" }}
        >
          <Container fluid>
            {/* Toggler */}
            <button
              className="navbar-toggler"
              type="button"
              onClick={this.toggleCollapse}
            >
              <span className="navbar-toggler-icon" />
            </button>
            {/* Brand */}
            {logo ? (
              <NavbarBrand className="pt-0" {...navbarBrandProps}>
                <img
                  alt={logo.imgAlt}
                  className="navbar-brand-img"
                  src={logo.imgSrc}
                />
              </NavbarBrand>
            ) : null}
            {/* Tipo Usuario */}
            <div
              className="fita fita-admin"
              style={
                parseInt(this.state.tipoUsuario) === 0
                  ? {}
                  : { display: "none" }
              }
            >
              Administrador
            </div>
            <div
              className="fita fita-planner"
              style={
                parseInt(this.state.tipoUsuario) === 1
                  ? {}
                  : { display: "none" }
              }
            >
              Planejador
            </div>
            {/*<div className="fita fita-cliente" style={parseInt(this.state.tipoUsuario) === 2 ? {} : {display: "none"}}>Cliente</div>*/}
            <div
              className="family-background"
              style={{
                position: "relative",
                display: "flex",
                width: "97.5%",
                height: 70,
                justifyContent: "center",
                paddingLeft: 45,
              }}
            >
              <CircularProgress
                size={24}
                style={
                  parseInt(this.state.tipoUsuario) <= 1 &&
                  this.state.familias == null &&
                  this.state.familiaId == null
                    ? {
                        float: "left",
                        position: "absolute",
                        left: "140px",
                        top: "20px",
                      }
                    : { display: "none" }
                }
              />
              <div
                className="family-change"
                style={{ position: "absolute", right: 0, zIndex: -1 }}
                onClick={() => {
                  if (
                    parseInt(this.state.tipoUsuario) <= 1 &&
                    (this.state.familias != null ||
                      this.state.familiaId != null)
                  )
                    global.showModal(this.modal);
                }}
              >
                <NavLink
                  style={
                    parseInt(this.state.tipoUsuario) <= 1
                      ? {}
                      : { display: "none" }
                  }
                >
                  <i
                    className="fas fa-chevron-right"
                    style={{ fontSize: 15, color: "#ffffff" }}
                  ></i>
                </NavLink>
                <NavLink
                  style={
                    parseInt(this.state.tipoUsuario) > 1
                      ? {}
                      : { display: "none" }
                  }
                >
                  <i
                    className="fa fa-refresh"
                    style={{ fontSize: "20px", color: "#ffffff" }}
                  ></i>
                </NavLink>
              </div>
              <div>
                <button
                  className="navbar-toggler"
                  type="button"
                  id="atualizar-familias-sidebar"
                  onClick={() => this.getFamilias()}
                >
                  Atualizar Famílias
                </button>
              </div>
              <div
                id="logo-familia"
                style={{
                  backgroundImage:
                    "url(" +
                    require("../../assets/img/theme/familia.png") +
                    ")",
                  position: "absolute",
                  left: 0,
                }}
              ></div>
              <div
                className="family-name"
                style={
                  this.state.nomeFamilia != null
                    ? { fontSize: ".9rem", paddingLeft: 0 }
                    : { fontSize: ".9rem", paddingLeft: 0 }
                }
              >
                <b>
                  {parseInt(this.state.tipoUsuario) <= 1 &&
                  this.state.nomeFamilia == null &&
                  this.state.familias != null ? (
                    <label style={{ marginBottom: 0, marginLeft: 10 }}>
                      Selecione um cliente
                    </label>
                  ) : (
                    this.state.nomeFamilia
                  )}
                </b>
              </div>
            </div>
            {/* Collapse */}
            <Collapse navbar isOpen={this.state.collapseOpen}>
              {/* Navigation */}
              <button
                className="navbar-toggler navbar-toggler-icon"
                type="button"
                onClick={this.toggleCollapse}
                style={{ backgroundColor: "#7E32D0", width: 30, height: 30 }}
              ></button>
              <Nav navbar>{this.createLinks(routes)}</Nav>
              {/* Divider */}
              <hr className="my-3" />
              {/* Heading */}
              <h6
                className="navbar-heading text-muted"
                onClick={() => this.trocarVisibilidadeConfiguracoes()}
                style={{ width: "100%", marginLeft: "-10px" }}
              >
                <label style={{ marginBottom: 0 }}>Configurações</label>
                <i
                  className={this.state.configuracoesIcone}
                  style={{
                    fontSize: "15px",
                    float: "left",
                    marginTop: "1px",
                    marginRight: "5px",
                  }}
                />
              </h6>
              {/* Navigation */}
              <Nav
                navbar
                style={{ display: this.state.configuracoesCssDisplay }}
              >
                {this.createConfigLinks(routes)}
              </Nav>
            </Collapse>
          </Container>
        </Navbar>
      </>
    );
  }
}

Sidebar.defaultProps = {
  routes: [{}],
};

Sidebar.propTypes = {
  // links that will be displayed inside the component
  routes: PropTypes.arrayOf(PropTypes.object),
  logo: PropTypes.shape({
    // innerLink is for links that will direct the user within the app
    // it will be rendered as <Link to="...">...</Link> tag
    innerLink: PropTypes.string,
    // outterLink is for links that will direct the user outside the app
    // it will be rendered as simple <a href="...">...</a> tag
    outterLink: PropTypes.string,
    // the image src of the logo
    imgSrc: PropTypes.string.isRequired,
    // the alt for the img
    imgAlt: PropTypes.string.isRequired,
  }),
};

export default Sidebar;
