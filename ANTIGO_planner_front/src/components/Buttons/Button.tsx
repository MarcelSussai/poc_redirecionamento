import React from 'react'
import styled from 'styled-components'
import Colors from '../../domain/Colors'
import backgroundImg from "../../assets/img/theme/botao-destaque.png"

interface Props {
    text: string
    onClick: () => void
}

const ButtonContainer = styled.div`
    color: rgb(${Colors.white});
    float: right;
    word-wrap: break-word;
    background-image: url(${backgroundImg});
    background-position: 50%;
    border-radius: 25px;
    padding: 7px 15px;
    cursor: pointer;
    margin-left: 1px;
    margin-right: 1px;

    &:hover {
        color: rgb(${Colors.highLightGray});
    }
`

export default ({ text, onClick }: Props) => {
    return (
        <ButtonContainer onClick={onClick}>
            <span>{text}</span>
        </ButtonContainer>
    )
}