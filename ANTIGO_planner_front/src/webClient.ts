import axios from 'axios'

const apiWebClient = axios.create({
  baseURL: (global as any).server_api_new,
  timeout: 5000,
})

export const postWithToken = <Response, Body>(url: string, body?: Body) => {
  const token = localStorage.getItem("tokenAcesso")
  return apiWebClient.post<Response>(`${token}/${url}`, body)
}

export const putWithToken = <Response, Body>(url: string, body?: Body) => {
  const token = localStorage.getItem("tokenAcesso")
  return apiWebClient.put<Response>(`${token}/${url}`, body)
}

export const deleteWithToken = <Response, Body>(url: string, body?: Body) => {
  const token = localStorage.getItem("tokenAcesso")
  return apiWebClient.delete<Response>(`${token}/${url}`, body)
}

export const getWithToken = <Response>(url: string) => {
  const token = localStorage.getItem("tokenAcesso")
  return apiWebClient.get<Response>(`${token}/${url}`)
}

